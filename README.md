# README #

This README would normally document whatever steps are necessary to get your application up and running.

# GIT SOP For Developer #

Clone and create a new branch from master to start development on your local.

Project setup:

- npm install
- composer install
- setup .env file 
- composer dump-autoload
- php artisan key:generate
- php artisan config:cache

Git steps before making pull request:

- git status
- git commit in your branch
- git checkout master
- git pull
- git checkout <yourbranch>
- git merge master (also make sure to resolveconflict, if any )
- git push origin <yourbranch>


then create a pull request source: <yourbranch>, target: master.


**Very important to ensure conflicts are resolved before pushing and making pull request**


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


# DOCUMENTATION: #

### How to add a setting?

The setting was handled using packaging [anlutro/l4-settings](https://github.com/anlutro/laravel-settings). It is to cache the value after saving to DB. If you save manually, make sure to re-cache or use the function/helper provided in this package documentation. There is a default setting value that can be referred to **database\\seeders\\SettingSeeder.php**. Additional default setting value can be added to that file and make sure to run **php artisan db:seed --class=SettingSeeder**. 

Another reference and example:

*   **app\\Services\\Admin\\Setting\\FileService.php@store** for store functions
*   **resources\\views\\admin\\setting\\file\\index\\form-file.blade.php** for handling the input

### How to validate a file setting?

There are two types of validation on the file, which are size and extension.

*   The size that was stored in DB is in format MB. However, Laravel validated that the file size is in kb. Therefore, the size needs to be times over 1024.
*   The extension that was stored is in format extension without the prefix dot (.) and comma (,) so that it is easy to convert for display and function usage.

Another reference and example:

*   **app\\Http\\Requests\\Profile\\StoreRequest.php** for handling the validation
*   **resources\\views\\profile\\index\\form-avatar.blade.php** for handling the extension display

Feel free to ask me if you confused with this. :))

### How to add custom email?
*   Create a blade in resources\views\notification\custom. Must be unique.
*   Add Student Ids in app\Console\Commands\SendCustomEmailNotification.php@getTemplateStudentID
