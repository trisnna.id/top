composer create-project laravel/laravel top
cd project folder
setup DB_DATABASE use collation utf8mb4_unicode_ci. e.g.: top
setup APP_URL without end slash
change default application name at app config
change timezone at app config with 
    'timezone' => env('APP_TIMEZONE', 'Asia/Kuala_Lumpur'),
add dateformat after timezone at app config
    'date_format' => [
        'php' => 'd/m/Y',
        'js' => 'DD/MM/YYYY',
    ],
    'time_format' => [
        'php' => 'g:i a',
        'js' => 'h:mm a',
    ],
change mail configuration at .env use account mailtrap
    MAIL_MAILER=smtp
    MAIL_HOST=smtp.mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME=d9addc57ab08c3
    MAIL_PASSWORD=1ddb5116665689
    MAIL_ENCRYPTION=tls
    MAIL_FROM_ADDRESS=noreply@noreply.test
    MAIL_FROM_NAME=No-Replay
php artisan config:cache
php artisan storage:link

composer require laravel/ui
php artisan ui bootstrap --auth
npm install && npm run dev
npm run dev (if error)
composer require tightenco/ziggy

// config/ziggy.php
return [
    'only' => ['home', 'posts.index', 'posts.show'],
];

composer require owen-it/laravel-auditing
php artisan vendor:publish --provider "OwenIt\Auditing\AuditingServiceProvider" --tag="config"
php artisan vendor:publish --provider "OwenIt\Auditing\AuditingServiceProvider" --tag="migrations"
composer require stevebauman/location
php artisan config:cache
replace $table->morphs('auditable'); at create_audits_table with
	$table->uuidMorphs('auditable');
add $table->string('location')->nullable(); at create_audits_table after ip
create ..\app\Vendors\OwenIt\Auditing\Resolvers\LocationResolver.php
	<?php

	namespace App\Vendors\OwenIt\Auditing\Resolvers;

	use Illuminate\Support\Facades\Request;
	use OwenIt\Auditing\Contracts\Auditable;
	use OwenIt\Auditing\Contracts\Resolver;
	use Stevebauman\Location\Facades\Location;

	class LocationResolver implements Resolver
	{
		public static function resolve(Auditable $auditable): string
		{
			return Location::get(Request::ip())->countryName ?? '';
		}
	}
change audit config
	'implementation' => App\Models\Audit::class,
	...
	'resolvers' => [
        'ip_address' => OwenIt\Auditing\Resolvers\IpAddressResolver::class,
        'user_agent' => OwenIt\Auditing\Resolvers\UserAgentResolver::class,
        'url'        => OwenIt\Auditing\Resolvers\UrlResolver::class,
        'location'   => App\Vendors\OwenIt\Auditing\Resolvers\LocationResolver::class,
    ],
create ..\app\Models\Audit.php with following code
	<?php

	namespace App\Models;

	use OwenIt\Auditing\Models\Audit as OwenItAudit;
	use App\Traits\Models\BaseModelTrait;

	class Audit extends OwenItAudit
	{

		use BaseModelTrait;
		protected $fillable = [
			'user_type',
			'user_id',
			'event',
			'auditable_type',
			'auditable_id',
			'old_values',
			'new_values',
			'url',
			'ip_address',
			'location',
			'user_agent',
			'tags'
		];
		protected $casts = [
			'old_values' => 'json',
			'new_values' => 'json',
		];

		/* =========================================================================
		 * Relationship
		 * =========================================================================
		 */


		/* =========================================================================
		 * Scope
		 * =========================================================================
		 */

		/* =========================================================================
		 * Accessor & Mutator
		 * =========================================================================
		 */


		/* =========================================================================
		 * Other
		 * =========================================================================
		 */
	}


composer require spatie/laravel-permission
add to middleware kernal protected $routeMiddleware
    'role' => \Spatie\Permission\Middlewares\RoleMiddleware::class,
    'permission' => \Spatie\Permission\Middlewares\PermissionMiddleware::class,
    'role_or_permission' => \Spatie\Permission\Middlewares\RoleOrPermissionMiddleware::class,
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"
php artisan config:cache
change migration create_permission_table date before create_users_table
add to create_permission_table for schema permission 
	after id
		$table->string('uuid')->unique();
		$table->unsignedBigInteger('parent_id')->nullable();		
		$table->foreign('parent_id')->references('id')->on('permissions')->onDelete('set null')->onUpdate('cascade');
	after name
		$table->string('label')->nullable();
		$table->text('description')->nullable();
add to create_permission_table for schema role
	after id
		$table->string('uuid')->unique();
	after name
		$table->string('label')->nullable();
		$table->text('description')->nullable();
		$table->boolean('is_active')->default(true);
edit ..\config\permission.php
	'permission' => App\Models\Permission::class,
	'role' => App\Models\Role::class,
Create ..\app\Models\BaseModel.php with following code
	<?php

	namespace App\Models;

	use App\Traits\Models\BaseModelTrait;
	use Illuminate\Database\Eloquent\Factories\HasFactory;
	use Illuminate\Database\Eloquent\Model;
	use OwenIt\Auditing\Auditable as AuditableTrait;
	use OwenIt\Auditing\Contracts\Auditable;

	class BaseModel extends Model implements Auditable
	{

		use AuditableTrait,
			BaseModelTrait,
			HasFactory;
	}
Create ..\app\Traits\Models\BaseModelTrait.php with following code
	<?php

	namespace App\Traits\Models;

	use App\Vendors\Illuminate\Database\Eloquent\Builder;
	use Carbon\Carbon;

	trait BaseModelTrait
	{
		/* =========================================================================
		 * Scope
		 * =========================================================================
		 */

		public function scopeWhenByArgs($query, $args, $table = null)
		{
			$table = !empty($table) ? "{$table}." : null;
			return $query->when(($args['created_at'] ?? null),
				function ($query) use ($args, $table) {
					if (is_array($args['created_at'])) {
						$query->whereBetween(
							"{$table}created_at",
							$args['created_at']
						);
					} else {
						$query->where("{$table}created_at", $args['created_at']);
					}
				}
			)->when(($args['is_active'] ?? null),
				function ($query) use ($args, $table) {
					if (is_array($args['is_active'])) {
						$query->whereIn("{$table}is_active", $args['is_active']);
					} else {
						$query->where("{$table}is_active", $args['is_active']);
					}
				}
			)->when(($args['group_id'] ?? null),
				function ($query) use ($args, $table) {
					if (is_array($args['group_id'])) {
						$query->whereIn("{$table}group_id", $args['group_id']);
					} else {
						$query->where("{$table}group_id", $args['group_id']);
					}
				}
			)->when(($args['approval'] ?? null),
				function ($query) use ($args, $table) {
					if (is_array($args['approval'])) {
						$query->whereIn("{$table}approval", $args['approval']);
					} else {
						$query->where("{$table}approval", $args['approval']);
					}
				}
			)->when(($args['subgroup_id'] ?? null),
				function ($query) use ($args, $table) {
					if (is_array($args['subgroup_id'])) {
						$query->whereIn(
							"{$table}subgroup_id",
							$args['subgroup_id']
						);
					} else {
						$query->where(
							"{$table}subgroup_id",
							$args['subgroup_id']
						);
					}
				}
			)->when(($args['uuid'] ?? null),
				function ($query) use ($args, $table) {
					if (is_array($args['uuid'])) {
						$query->whereIn("{$table}uuid", $args['uuid']);
					} else {
						$query->where("{$table}uuid", $args['uuid']);
					}
				}
			);
		}
		/* =========================================================================
		 * Accessor & Mutator
		 * =========================================================================
		 */

		public function getAttribute($key)
		{
			switch ($key) {
				case "updated_at_readable":
				case "created_at_readable":
					$key = explode("_readable", $key)[0];
					$value = $this->getAttributeValue($key);
					return !empty($value) ? $value->format(config('app.date_format.php') . ' ' . config('app.time_format.php'))
						: null;
				default:
					return parent::getAttribute($key);
			}
		}
		/* =========================================================================
		 * Other
		 * =========================================================================
		 */

		public function newEloquentBuilder($query)
		{
			return new Builder($query);
		}

		public function getUuidKey()
		{
			return $this->getAttribute('uuid');
		}

		public static function boot()
		{
			parent::boot();
			static::creating(function ($model) {
				if (($model->hasUuid ?? false) && empty($model->uuid)) {
					$model->uuid = uniqueUuidByModel($model);
				}
			});
		}
	}
Create ..\app\Vendors\Illuminate\Database\Eloquent\Builder.php with following code
	<?php

	namespace App\Vendors\Illuminate\Database\Eloquent;

	use Illuminate\Database\Eloquent\Builder as BaseBuilder;
	use Illuminate\Database\Eloquent\ModelNotFoundException;

	class Builder extends BaseBuilder
	{

		public function findOrFailByUuid($uuid, $columns = ['*'])
		{
			$result = $this->findByUuid($uuid, $columns);
			$uuid = $uuid instanceof Arrayable ? $uuid->toArray() : $uuid;
			if (is_array($uuid)) {
				if (count($result) === count(array_unique($uuid))) {
					return $result;
				}
			} elseif (!is_null($result)) {
				return $result;
			}
			throw (new ModelNotFoundException)->setModel(
				get_class($this->model),
				$uuid
			);
		}

		public function findByUuid($uuid, $columns = ['*'])
		{
			if (is_array($uuid) || $uuid instanceof Arrayable) {
				return $this->findMany($uuid, $columns);
			}
			return $this->whereUuidKey($uuid)->first($columns);
		}

		public function whereUuidKey($uuid)
		{
			if ($uuid instanceof Model) {
				$uuid = $uuid->getUuidKey();
			}
			if (is_array($uuid) || $uuid instanceof Arrayable) {
				$this->query->whereIn('uuid', $uuid);
				return $this;
			}
			if ($uuid !== null && $this->model->getKeyType() === 'string') {
				$uuid = (string) $uuid;
			}
			return $this->where('uuid', '=', $uuid);
		}

		/**
		 * Find or fail by name
		 * @param type $name
		 * @param type $columns
		 * @return type
		 * @throws type
		 */
		public function findOrFailByName($name, $columns = ['*'])
		{
			$result = $this->findByName($name, $columns);
			$name = $name instanceof Arrayable ? $name->toArray() : $name;
			if (is_array($name)) {
				if (count($result) === count(array_unique($name))) {
					return $result;
				}
			} elseif (!is_null($result)) {
				return $result;
			}
			throw (new ModelNotFoundException)->setModel(
				get_class($this->model),
				$name
			);
		}

		/**
		 * Find by name
		 * @param type $name
		 * @param type $columns
		 * @return type
		 */
		public function findByName($name, $columns = ['*'])
		{
			return $this->where('name', $name)->first($columns);
		}

		/**
		 * Find or fail by type and name
		 * @param type $type
		 * @param type $name
		 * @param type $columns
		 * @return type
		 * @throws type
		 */
		public function findOrFailByTypeName($type, $name, $columns = ['*'])
		{
			$result = $this->findByTypeName($type, $name, $columns);
			$type = $type instanceof Arrayable ? $type->toArray() : $type;
			$name = $name instanceof Arrayable ? $name->toArray() : $name;
			if (is_array($type) && is_array($name)) {
				if (count($result) === (count(array_unique($type)) && count(array_unique($name)))) {
					return $result;
				}
			} elseif (!is_null($result)) {
				return $result;
			}
			throw (new ModelNotFoundException)->setModel(
				get_class($this->model),
				$type
			);
		}

		/**
		 * Find by type and name
		 * @param type $type
		 * @param type $name
		 * @param type $columns
		 * @return type
		 */
		public function findByTypeName($type, $name, $columns = ['*'])
		{
			return $this->where(['type' => $type, 'name' => $name])->first($columns);
		}

		/**
		 * Upsert or ignore the existing data. Avoid the gap of auto increment keep increasing.
		 * @param array $inputs
		 * @param array $uniqueBy
		 * @return array
		 */
		public function upsertUniqueBy(
			array $inputs,
			array $uniqueBy,
			$update = null
		) {
			$chunkInputs = array_chunk($inputs, 2);
			foreach ($chunkInputs as $chunkInput) {
				$uniqueByInputs = [];
				$upsertInputs = [];
				$insertInputs = [];
				$existingInputs = $this->where(function ($query) use (
					$chunkInput,
					$uniqueBy,
					&$uniqueByInputs
				) {
					foreach ($chunkInput as $index => $input) {
						$uniqueByInputs[$index] = array_filter(
							$input,
							function ($uniqueByInput) use ($uniqueBy) {
								return in_array($uniqueByInput, $uniqueBy);
							},
							ARRAY_FILTER_USE_KEY
						);
						$query->orWhere(function ($query2) use (
							$index,
							$uniqueByInputs
						) {
							$query2->where($uniqueByInputs[$index]);
						});
					}
				})->get()->makeHidden(['created_at', 'updated_at']);
				foreach ($chunkInput as $chunkIndex => $chunkValue) {
					$countExistingInputs = null;
					foreach ($uniqueByInputs[$chunkIndex] as $keyInput => $valueInput) {
						if (empty($countExistingInputs)) {
							$countExistingInputs = $existingInputs->where(
								$keyInput,
								$valueInput
							);
						} else {
							$countExistingInputs = $countExistingInputs->where(
								$keyInput,
								$valueInput
							);
						}
					}
					if (
						!empty($countExistingInputs) && $countExistingInputs->count()
						> 0
					) {
						if (array_key_exists('uuid', $chunkValue)) {
							unset($chunkValue['uuid']);
						}
						$upsertInputs[$chunkIndex] = array_merge(
							$countExistingInputs->first()->toArray(),
							$chunkValue
						);
					} else {
						if (array_key_exists('id', $chunkValue)) {
							unset($chunkValue['id']);
						}
						$insertInputs[$chunkIndex] = $chunkValue;
					}
				}
				count($insertInputs) > 0 ? $this->upsert(
					$insertInputs,
					$uniqueBy,
					$update
				) : null;
				count($upsertInputs) > 0 ? $this->upsert(
					$upsertInputs,
					$uniqueBy,
					$update
				) : null;
			}
			return true;
		}
	}
Create ..\app\Models\Permission.php with following code
	<?php

	namespace App\Models;

	use App\Traits\Models\BaseModelTrait;
	use Spatie\Permission\Models\Permission as SpatiePermission;

	class Permission extends SpatiePermission
	{

		use BaseModelTrait;
		protected $fillable = [
			'name',
			'label',
			'description',
			'guard_name',
			'parent_id'
		];
		protected $casts = [
			'uuid' => 'string',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		];

		/* =========================================================================
		 * Relationship
		 * =========================================================================
		 */

		public function recursivePermissionsByParentId()
		{
			return $this->permissionsByParentId()->with('recursivePermissionsByParentId');
		}

		public function permissionsByParentId()
		{
			return $this->hasMany(Permission::class, 'parent_id', 'id');
		}
		/* =========================================================================
		 * Scope
		 * =========================================================================
		 */

		/* =========================================================================
		 * Accessor & Mutator
		 * =========================================================================
		 */

		/* =========================================================================
		 * Other
		 * =========================================================================
		 */
	}
Create ..\app\Models\Role.php with following code
	<?php

	namespace App\Models;

	use App\Traits\Models\BaseModelTrait;
	use Carbon\Carbon;
	use Spatie\Permission\Models\Role as SpatieRole;

	class Role extends SpatieRole
	{

		use BaseModelTrait;
		public $hasUuid = true;
		protected $fillable = [
			'name',
			'label',
			'description',
			'is_active',
			'guard_name',
			'created_by',
			'updated_by'
		];
		protected $casts = [
			'uuid' => 'string',
			'is_active' => 'boolean',
		];

		/* =========================================================================
		 * Relationship
		 * =========================================================================
		 */

		public function roleHasPermissions()
		{
			return $this->hasMany(RoleHasPermission::class);
		}
		/* =========================================================================
		 * Scope
		 * =========================================================================
		 */


		/* =========================================================================
		 * Accessor & Mutator
		 * =========================================================================
		 */

		public function getAttribute($key)
		{
			switch ($key) {
				case "created_at_readable":
					$key = explode("_readable", $key)[0];
					$value = $this->getAttributeValue($key);
					return !empty($value) ? Carbon::createFromFormat(
						'Y-m-d H:i:s',
						$value
					)->format(config('app.date_format.php') . ' ' . config('app.time_format.php'))
						: null;
				default:
					return parent::getAttribute($key);
			}
		}
		/* =========================================================================
		 * Other
		 * =========================================================================
		 */
	}
add to create_users_table
	after id
		$table->string('uuid')->unique();
	after password
		$table->unsignedBigInteger('role_id')->nullable();     
		$table->foreign('role_id')->references('id')->on('roles')->onDelete('set null')->onUpdate('cascade');
		$table->timestamp('last_login')->nullable();
create ..\app\Listeners\UserEventSubscriber.php with following code
	<?php

	namespace App\Listeners;

	use Carbon\Carbon;
	use Illuminate\Auth\Events\Login;
	use Illuminate\Auth\Events\Logout;

	class UserEventSubscriber
	{

		/**
		 * Handle user login events.
		 */
		public function handleUserLogin($event)
		{
			$user = $event->user;
			$user->last_login = Carbon::now();
			$user->save();
		}

		/**
		 * Handle user logout events.
		 */
		public function handleUserLogout($event)
		{
			//
		}

		/**
		 * Register the listeners for the subscriber.
		 *
		 * @param  \Illuminate\Events\Dispatcher  $events
		 * @return array
		 */
		public function subscribe($events)
		{
			return [
				Login::class => 'handleUserLogin',
				Logout::class => 'handleUserLogout',
			];
		}
	}
add to App\Providers\EventServiceProvider
	use App\Listeners\UserEventSubscriber;
	
	protected $subscribe = [
        UserEventSubscriber::class,
    ];
add to model User
	public $hasUuid = true;
	in use
		use App\Traits\Models\BaseModelTrait;
		use OwenIt\Auditing\Auditable as AuditableTrait;
		use OwenIt\Auditing\Contracts\Auditable;
		use Spatie\Permission\Traits\HasRoles;
	in use inside class
		use HasRoles, AuditableTrait, BaseModelTrait
	in fillable
		protected $fillable = [
			'uuid',
			'name',
			'email',
			'email_verified_at',
			'password',
			'role_id',
			'last_login',
		];
	in cast
		protected $casts = [
			'uuid' => 'string',
			'last_login' => 'datetime',
			'email_verified_at' => 'datetime',
		];
	in implements
		implements Auditable, MustVerifyEmail
		
	/* =========================================================================
     * Relationship
     * =========================================================================
     */

    /* =========================================================================
     * Scope
     * =========================================================================
     */

    /* =========================================================================
     * Accessor & Mutator
     * =========================================================================
     */

    /* =========================================================================
     * Other
     * =========================================================================
     */
	 
	public function generateTags(): array
    {
        if ($this->wasChanged('last_login')) {
            return [
                'login',
            ];
        }
        return [];
    }
replace Auth::routes(); at route with
	Auth::routes(['verify' => true]);
add to route
    use App\Http\Controllers;

	// Handle user path
	Route::group(['middleware' => ['auth','verified']], function () {
	
	});
	// Handle admin path
	Route::group(['middleware' => ['auth','role:admin','verified']], function () {
	
	});
php artisan config:cache

create helper Helpers\Helper.php
	<?php

	use Illuminate\Support\Str;

	if (!function_exists('uniqueUuidByModel')) {

		function uniqueUuidByModel($model, string $key = 'uuid', bool $string = true)
		{
			$exist = true;
			while ($exist) {
				$uuid = Str::orderedUuid();
				$exist = $model->where($key, $uuid)->exists();
			}
			return $string ? $uuid->toString() : $uuid;
		}
	}
add to composer.json
	"autoload": {
        "files": [
            "app/Helpers/Helper.php"
        ]
    },
composer dump-autoload
add to ..\database\seeders\DatabaseSeeder.php
	$this->call([
		RolePermissionSeeder::class,
		DefaultCredentialSeeder::class,
	]);
create ..\database\seeders\DefaultCredentialSeeder.php and add following code
	<?php

	namespace Database\Seeders;

	use App\Models\Role;
	use App\Models\User;
	use Carbon\Carbon;
	use Database\Seeders\RolePermissionSeeder;
	use Illuminate\Database\Seeder;
	use Illuminate\Support\Facades\Hash;
	use Spatie\Permission\PermissionRegistrar;

	class DefaultCredentialSeeder extends Seeder
	{
		protected $domainEmail = 'test';

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			(new RolePermissionSeeder())->roleSeeder();
			$this->userSeeder();
		}

		public function userSeeder()
		{
			$roles['user'] = Role::findByName('user');
            $roles['admin'] = Role::findByName('admin');
            $users = [
                [
                    'uuid' => uniqueUuidByModel(new User()), 'name' => 'admin', 'email' => "admin@{$this->domainEmail}",
                    'email_verified_at' => Carbon::now(), 'password' => Hash::make('password'),
                    'role_id' => $roles['admin']->id
                ],
                [
                    'uuid' => uniqueUuidByModel(new User()), 'name' => 'user', 'email' => "user@{$this->domainEmail}",
                    'email_verified_at' => Carbon::now(), 'password' => Hash::make('password'),
                    'role_id' => $roles['user']->id
                ],
            ];
            User::upsertUniqueBy($users, ['email']);
            $name['admin'] = User::where('name', 'admin')->first();
            $name['admin']->assignRole('admin');
            $name['user'] = User::where('name', 'user')->first();
            $name['user']->assignRole('user');
            app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
            return User::all();
		}
	}
create ..\database\seeders\RolePermissionSeeder.php and add following code
	<?php

	namespace Database\Seeders;

	use App\Models\Permission;
	use App\Models\Role;
	use Illuminate\Database\Seeder;
	use Spatie\Permission\PermissionRegistrar;

	class RolePermissionSeeder extends Seeder
	{

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$this->roleSeeder();
			$this->permissionSeeder();
		}

		public function permissionSeeder()
		{
			$permissions = [
				['uuid' => uniqueUuidByModel(new Permission()), 'name' => 'user', 'label' => 'user', 'guard_name' => 'web'],
				['uuid' => uniqueUuidByModel(new Permission()), 'name' => 'admin', 'label' => 'admin', 'guard_name' => 'web'],
			];
			Permission::upsertUniqueBy($permissions, ['name', 'guard_name']);
			app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
			$permissionNames['admin'] = [
				"admin",
			];
			$roles['admin'] = Role::findByName('admin')->syncPermissions($permissionNames['admin']);
			$permissionNames['user'] = [
				"user",
			];
			$roles['user'] = Role::findByName('user')->syncPermissions($permissionNames['user']);
		}

		public function roleSeeder()
		{
			$roles = [
				[
					'uuid' => uniqueUuidByModel(new Role()), 'name' => 'admin', 'label' => 'Admin',
					'is_active' => true, 'guard_name' => 'web'
				],
				[
					'uuid' => uniqueUuidByModel(new Role()), 'name' => 'user', 'label' => 'User',
					'is_active' => true, 'guard_name' => 'web'
				],
			];
			Role::upsertUniqueBy($roles, ['name', 'guard_name']);
			return Role::all();
		}
	}
php artisan migrate --seed

change ..\app\Providers\RouteServiceProvider.php '/home' to '/dashboard', route and controller
	// Handle user path
	Route::group(['middleware' => ['auth', 'verified']], function () {
		Route::get('dashboard', [Controllers\DashboardController::class, 'index'])->name('dashboard.index');
	});
	// Handle admin path
	Route::group(['middleware' => ['auth', 'role:admin', 'verified']], function () {
		Route::resource('user', Controllers\UserController::class);
	});
php artisan ziggy:generate ./resources/js/plugins/ziggy.js --url="//"

composer require yajra/laravel-datatables
php artisan vendor:publish --tag=datatables

Setup theme (directory suggestion)
- ..\public\app (any scripts/styles related to application)
- ..\public\portal (any scripts/styles related to portal/landing page)
- ..\public\{others} (such as media)
- ..\public\vendors (any plugin javascript)

Metronic Themes, Create components for
- php artisan make:component Layouts/App
- php artisan make:component Layouts/App/Body
- php artisan make:component Layouts/App/Body/Scripts
- php artisan make:component Layouts/App/Body/Footer
- php artisan make:component Layouts/App/Styles
- php artisan make:component Layouts/App/Meta
- php artisan make:component Layouts/App/Body/Header
- php artisan make:component Layouts/App/Body/Header/Menu

- php artisan make:component Layouts/Auth
- php artisan make:component Layouts/Auth/Body
- php artisan make:component Layouts/Auth/Body/Scripts
- php artisan make:component Layouts/Auth/Styles

- php artisan make:component Swal/Upsert
	
composer require kodeine/laravel-meta
php artisan make:migration create_users_meta_table

php artisan make:migration create_club_societies_table
php artisan make:migration create_club_society_categories_table
php artisan make:migration create_resource_types_table
php artisan make:migration create_resources_table

composer require spatie/laravel-medialibrary
php artisan vendor:publish --provider="Spatie\MediaLibrary\MediaLibraryServiceProvider" --tag="migrations"
php artisan migrate
php artisan vendor:publish --provider="Spatie\MediaLibrary\MediaLibraryServiceProvider" --tag="config"

php artisan make:migration create_activities_table
php artisan make:migration create_activity_types_table

php artisan make:migration create_programmes_table

php artisan make:migration create_students_table
php artisan make:migration create_staffs_table
php artisan make:migration create_attendances_table

php artisan make:migration create_registrations_table
php artisan make:migration create_mentors_table

php artisan make:migration create_surveys_table
php artisan make:migration create_survey_answers_table
php artisan make:migration create_survey_answer_statuses_table

composer update && npm update && npm run dev && php artisan ziggy:generate ./resources/js/plugins/ziggy.js


add to package.json at dependencies
	"dependencies": {
		"datatables.net": "^1.10.24",
		"datatables.net-autofill-bs4": "^2.3.5",
		"datatables.net-bs4": "^1.10.24",
		"datatables.net-buttons-bs4": "^1.7.0",
		"datatables.net-colreorder-bs4": "^1.5.3",
		"datatables.net-fixedcolumns-bs4": "^3.3.2",
		"datatables.net-fixedheader-bs4": "^3.1.8",
		"datatables.net-keytable-bs4": "^2.6.1",
		"datatables.net-responsive-bs4": "^2.2.7",
		"datatables.net-rowgroup-bs4": "^1.1.2",
		"datatables.net-rowreorder-bs4": "^1.2.7",
		"datatables.net-scroller-bs4": "^2.0.3",
		"datatables.net-searchbuilder-bs4": "^1.0.1",
		"datatables.net-searchpanes-bs4": "^1.2.2",
		"datatables.net-select-bs4": "^1.3.2",
		"select2": "^4.0.13",
		"sweetalert2": "^10.15.6",
		"selectize": "^0.12.6",
		"selectize-plugin-clear": "0.0.3",
		"moment": "^2.29.1",
		"pdfmake": "^0.1.70",
		"jszip": "^3.6.0",
	}
npm install
add to resources > js > app.js
	require('./plugins');
create plugins.js at resources > js
	// Moment - Parse, validate, manipulate, and display dates and times in JavaScript. Learn more: https://momentjs.com/
	window.moment = require('moment');
	// Selectize is an extensible jQuery-based custom <select> UI control: https://github.com/selectize/selectize.js
	window.Selectize = require('selectize/dist/js/standalone/selectize.min.js');
	require('selectize-plugin-clear/dist/selectize-plugin-clear.js');
	// Sweetalert2 - a beautiful, responsive, customizable and accessible (WAI-ARIA) replacement for JavaScript's popup boxes: https://sweetalert2.github.io/
	window.Swal = window.swal = require('sweetalert2/dist/sweetalert2.min.js');
	// Select2 - Select2 is a jQuery based replacement for select boxes: https://select2.org/
	require('select2/dist/js/select2.full.js');
	// Datatable.net
	require('datatables.net/js/jquery.dataTables.js');
	require('datatables.net-bs4/js/dataTables.bootstrap4.js');
	require('datatables.net-autofill/js/dataTables.autoFill.min.js');
	require('datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js');
	require('jszip/dist/jszip.min.js');
	require('pdfmake/build/pdfmake.min.js');
	require('pdfmake/build/vfs_fonts.js');
	require('datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js');
	require('datatables.net-buttons/js/buttons.colVis.js');
	require('datatables.net-buttons/js/buttons.flash.js');
	require('datatables.net-buttons/js/buttons.html5.js');
	require('datatables.net-buttons/js/buttons.print.js');
	require('datatables.net-colreorder/js/dataTables.colReorder.min.js');
	require('datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js');
	require('datatables.net-fixedheader/js/dataTables.fixedHeader.min.js');
	require('datatables.net-keytable/js/dataTables.keyTable.min.js');
	require('datatables.net-responsive/js/dataTables.responsive.min.js');
	require('datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js');
	require('datatables.net-rowgroup/js/dataTables.rowGroup.min.js');
	require('datatables.net-rowreorder/js/dataTables.rowReorder.min.js');
	require('datatables.net-scroller/js/dataTables.scroller.min.js');
	require('datatables.net-select/js/dataTables.select.min.js');
add to resources > sass > app.scss
	// Plugins
	@import 'plugins';
create _plugins.scss at resources > sass
	// Selectize is an extensible jQuery-based custom <select> UI control: https://github.com/selectize/selectize.js
	@import "~selectize/dist/css/selectize.bootstrap3.css";
	@import "~selectize-plugin-clear/dist/selectize-plugin-clear.css";
	// select2 - Select2 is a jQuery based replacement for select boxes: https://select2.org/
	@import "~select2/dist/css/select2.css";
	// Sweetalert2 - a beautiful, responsive, customizable and accessible (WAI-ARIA) replacement for JavaScript's popup boxes: https://sweetalert2.github.io/
	@import "~sweetalert2/dist/sweetalert2.css";

<-- Test for Laravel PWA -->
composer require silviolleite/laravelpwa --prefer-dist
php artisan vendor:publish --provider="LaravelPWA\Providers\LaravelPWAServiceProvider"
copy latest BaseService, YajraService, BaseRepository, BaseModel, layouts.components.modal_edit, services.yajra-service
add to layouts.app or equivalent
	@yield('content')
	{{-- Here is style --}}
        <link rel="stylesheet" href="{{mix('css/app.css')}}" >
        @laravelPWA
	@stack('styles')
	<style>
		@stack('style')
	</style>
	{{-- Here is script --}}
	<script>
		var AppConfig = {
			"timezone": "{{config('app.timezone')}}",
			"dateFormat": "{{config('app.date_format.js')}}",
			"timeFormat": "{{config('app.time_format.js')}}",
		}
	</script>
	<script type="text/javascript" src="{{mix('js/app.js')}}"></script>
	@stack('scripts')
	<script>
		jQuery(document).ready(function () {
			@stack('scriptDocumentReady')
		})
		@stack('script')
	</script>

<!--(.+?)-->

composer update && npm update && npm run prod
- php artisan make:component Layouts/App/Body/HeaderAdmin
- php artisan make:component Layouts/App/Body/Sidebar
- php artisan make:component Datatables/Search

php artisan make:migration create_faculties_table
php artisan make:migration create_schools_table

php artisan make:migration create_localities_table
php artisan make:migration create_campuses_table
php artisan make:migration create_mobilities_table
php artisan make:migration create_level_studies_table
php artisan make:migration create_intakes_table

php artisan make:migration create_orientations_table
php artisan make:migration create_orientation_types_table
php artisan make:migration create_orientation_filters_table
php artisan make:migration create_orientation_links_table

php artisan make:migration create_action_logs_table
php artisan make:migration create_orientation_rsvps_table
php artisan make:migration create_orientation_attendances_table

php artisan make:component Rating/FiveStar

php artisan make:migration create_student_points_table
php artisan make:component Modules/Admin/Student/Index/Filter

composer require barryvdh/laravel-snappy
php artisan vendor:publish --provider="Barryvdh\Snappy\ServiceProvider"

- php artisan make:component Datatables/Export
php artisan make:component Modules/Admin/Orientation/Index/Detail/Filter
php artisan make:component Modules/Orientation/Index/Calendar/Filter
php artisan make:component Modules/Admin/Dashboard/Index/Card/Filter
php artisan make:component Modules/Admin/Dashboard/Index/Graph/Filter

php artisan make:migration create_mails_table
php artisan make:migration create_mail_logs_table

php artisan make:migration create_google_accounts_table

git reset --hard b4e86406884398df82d38938a6b321526fe9fab9

php artisan make:migration create_orientation_filter_raws_table
php artisan make:migration create_faqs_table

php artisan make:migration add_notification_to_mails_table --table=mails

php artisan make:component Layouts/App/Body/Header/Notification
php artisan make:migration add_is_academic_to_orientations_table --table=orientations

php artisan make:migration create_student_views_table
php artisan make:migration create_course_intake_views_table
php artisan make:migration add_contact_no_to_students_table --table=students
php artisan make:migration add_unique_to_programmes_table --table=programmes
php artisan make:migration add_created_via_to_users_table --table=users
php artisan make:migration add_total_points_to_students_table --table=students

php artisan make:migration add_notes_to_orientations_table --table=orientations

php artisan make:migration create_faq_filters_table
php artisan make:migration create_faq_filter_values_table
php artisan make:migration add_student_id_to_audits_table --table=audits

php artisan make:migration add_point_rating_to_orientation_attendances_table --table=orientation_attendances

php artisan make:component Tooltip/Basic
php artisan make:migration create_survey_migrations_table
php artisan make:migration add_end_date_to_orientations_table --table=orientations

php artisan make:migration add_orientation_attendance_percentage_to_students_table --table=students
php artisan make:migration add_is_orientation_activities_active_to_intakes_table --table=intakes

php artisan make:migration add_is_default_to_mails_table --table=mails
php artisan make:migration create_mail_recipients_table