<?php

use App\Http\Controllers;
use Illuminate\Routing\Router;
use App\Http\Controllers\Settings;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\Student\PreSurveyController;
use App\Http\Controllers\Student\PostSurveyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Controllers\PortalController::class, 'index'])->name('portal.index');
Route::get('privacy-policy', [Controllers\PageController::class, 'indexPolicy'])->name('page.policy');
Route::get('disclaimer', [Controllers\PageController::class, 'indexDisclaimer'])->name('page.disclaimer');

Route::get('orientation-leader-activation/{remember_token}', [Controllers\Admin\UserController::class, 'orientationLeaderActivation'])->name('orientation-leader-activation');

Route::group(['prefix' => 'microsoft', 'as' => 'microsoft.'], function () {
    Route::get('login', [Controllers\Auth\Microsoft\LoginController::class, 'login'])->name('login');
    Route::get('login/callback', [Controllers\Auth\Microsoft\LoginController::class, 'callback'])->name('login.callback');
});

Auth::routes(['verify' => true]);

// Handle all user path
Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::get('dashboard', [Controllers\DashboardController::class, 'index'])->name('dashboard.index');
    Route::get('profile', [Controllers\ProfileController::class, 'index'])->name('profile.index');
    Route::post('profile', [Controllers\ProfileController::class, 'store'])->name('profile.store');
    Route::get('profile/notification', [Controllers\ProfileController::class, 'indexNotification'])->name('profile.notification.index');
    Route::post('profile/notification/read/{read}', [Controllers\ProfileController::class, 'updateNotificationRead'])->name('profile.notification-read.update');
    Route::get('profile/password', [Controllers\ProfileController::class, 'indexPassword'])->name('profile.password.index');
    Route::post('profile/password', [Controllers\ProfileController::class, 'storePassword'])->name('profile.password.store');
    Route::post('google-accounts/share-event', [Controllers\GoogleAccountController::class, 'shareEvent'])->name('google-accounts.share-event');
    Route::post('google-accounts/share-events', [Controllers\GoogleAccountController::class, 'shareEvents'])->name('google-accounts.share-events');
    Route::get('orientations/export-timetable', [Controllers\OrientationController::class, 'exportTimetable'])->name('orientations.export-timetable');
    Route::get('timetables', [Controllers\OrientationController::class, 'index'])->name('orientations.index');
});

// Handle student path
Route::group(['middleware' => ['auth', 'verified', 'student']], function () {
    Route::get('resources', [Controllers\ResourceController::class, 'index'])->name('resources.index');
    Route::get('resources/{resource}', [Controllers\ResourceController::class, 'show'])->name('resources.show');
    Route::get('arrivals', [Controllers\ArrivalController::class, 'index'])->name('arrivals.index');
    Route::get('arrivals/{arrival}', [Controllers\ArrivalController::class, 'show'])->name('arrivals.show');
    // Route::get('pre-orientations', [Controllers\PreOrientationController::class, 'index'])->name('pre-orientations.index');

    Route::prefix('announcements')->group(function (Router $router) {
        $router->get('/', [AnnouncementController::class, 'index'])->name('announcement.index');
        $router->get('/{slug}', [AnnouncementController::class, 'show'])->name('announcement.show');
    });

    Route::put('timetables/attendance', [Controllers\OrientationController::class, 'upsertAttendance'])->name('orientations.upsert-attendance');
    Route::put('timetables/{orientation}/rate', [Controllers\OrientationController::class, 'updateRate'])->name('orientations.update-rate');
    Route::put('timetables/{orientation}/rsvp', [Controllers\OrientationController::class, 'upsertRsvp'])->name('orientations.upsert-rsvp');
    Route::put('timetables/{orientation}/rsvp-yes', [Controllers\OrientationController::class, 'upsertRsvpYes'])->name('orientations.upsert-rsvp-yes');
    Route::put('timetables/{orientation}/rsvp-no', [Controllers\OrientationController::class, 'upsertRsvpNo'])->name('orientations.upsert-rsvp-no');

    Route::get('orientations', [Controllers\OrientationController::class, 'index2'])->name('orientations.index2');
    Route::get('orientations/post-survey', [PostSurveyController::class, 'index'])->name('orientations.post-survey');
    Route::post('orientations/post-survey', [PostSurveyController::class, 'store']);

    Route::resource('club-societies', Controllers\ClubSocietyController::class);
    Route::resource('attendances', Controllers\AttendanceController::class);
    Route::resource('surveys', Controllers\SurveyController::class);
    Route::resource('faqs', Controllers\FaqController::class);

    Route::prefix('preorientations')->name('student.preorientation.')->group(function (Router $router) {
        $router->get('', [Controllers\Student\PreorientationController::class, 'index'])->name('index');
        $router->get('survey', [PreSurveyController::class, 'index'])->name('survey');
        $router->post('survey', [PreSurveyController::class, 'store']);
    });
});

// Handle admin path
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'permission:admin', 'verified']], function () {
    Route::get('dashboard', [Controllers\DashboardController::class, 'indexAdmin'])->name('dashboard.index');
    Route::group(['prefix' => 'announcements', 'as' => 'announcements', 'middleware' => 'permission:manage_announcements'], function (Router $router) {
        $router->get('/', [Controllers\Admin\AnnouncementController::class, 'index'])->name('.index');
        $router->get('create', [Controllers\Admin\AnnouncementController::class, 'create'])->name('.create');
        $router->post('store', [Controllers\Admin\AnnouncementController::class, 'store'])->name('.store');
        $router->get('preview/{uuid}', [Controllers\Admin\AnnouncementController::class, 'show'])->name('.show');
        $router->get('edit/{uuid}', [Controllers\Admin\AnnouncementController::class, 'edit'])->name('.edit');
        $router->patch('update/{uuid}', [Controllers\Admin\AnnouncementController::class, 'update'])->name('.update');
        $router->delete('delete/{uuid}', [Controllers\Admin\AnnouncementController::class, 'destroy'])->name('.delete');
    });

    // Route::resource('attendances', Controllers\Admin\AttendanceController::class);
    Route::group(['middleware' => ['permission:manage_users']], function () {
        Route::post('users/bulk/role', [Controllers\Admin\UserController::class, 'updateBulkRole'])->name('users.update-bulk-role');
        Route::post('users/bulk/active', [Controllers\Admin\UserController::class, 'updateBulkActive'])->name('users.update-bulk-active');
        Route::post('users/bulk/inactive', [Controllers\Admin\UserController::class, 'updateBulkInactive'])->name('users.update-bulk-inactive');
        Route::post('users/admin', [Controllers\Admin\UserController::class, 'storeAdmin'])->name('users.store-admin');
        Route::put('users/{user}/active', [Controllers\Admin\UserController::class, 'updateActive'])->name('users.update-active');
        Route::resource('users', Controllers\Admin\UserController::class);
    });
    Route::group(['middleware' => ['permission:manage_roles']], function () {
        Route::post('roles/bulk/active', [Controllers\Admin\RoleController::class, 'updateBulkActive'])->name('roles.update-bulk-active');
        Route::post('roles/bulk/inactive', [Controllers\Admin\RoleController::class, 'updateBulkInactive'])->name('roles.update-bulk-inactive');
        Route::put('roles/{role}/active', [Controllers\Admin\RoleController::class, 'updateActive'])->name('roles.update-active');
        Route::resource('roles', Controllers\Admin\RoleController::class);
    });
    Route::resource('quizzes', Controllers\Admin\QuizController::class);
    Route::group(['middleware' => ['permission:manage_resources']], function () {
        Route::delete('resources/{resource}/file/{file}', [Controllers\Admin\ResourceController::class, 'destroyFile'])->name('resources.destroy-file');
        Route::delete('resources/{resource}/link/{link}', [Controllers\Admin\ResourceController::class, 'destroyLink'])->name('resources.destroy-link');
        Route::put('resources/{resource}/active', [Controllers\Admin\ResourceController::class, 'updateActive'])->name('resources.update-active');
        Route::put('resources/{resource}/file/{file}', [Controllers\Admin\ResourceController::class, 'updateFile'])->name('resources.update-file');
        Route::put('resources/{resource}/link/{link}', [Controllers\Admin\ResourceController::class, 'updateLink'])->name('resources.update-link');
        Route::post('arrivals/{arrival}/clone', [Controllers\Admin\ArrivalController::class, 'clone'])->name('arrivals.clone');
        Route::post('arrivals/bulk/active', [Controllers\Admin\ArrivalController::class, 'updateBulkActive'])->name('arrivals.update-bulk-active');
        Route::post('arrivals/bulk/inactive', [Controllers\Admin\ArrivalController::class, 'updateBulkInactive'])->name('arrivals.update-bulk-inactive');
        Route::resource('arrivals', Controllers\Admin\ArrivalController::class);
        Route::post('resources/bulk/active', [Controllers\Admin\ResourceController::class, 'updateBulkActive'])->name('resources.update-bulk-active');
        Route::post('resources/bulk/inactive', [Controllers\Admin\ResourceController::class, 'updateBulkInactive'])->name('resources.update-bulk-inactive');
        Route::resource('resources', Controllers\Admin\ResourceController::class);
        Route::put('faqs/{faq}/active', [Controllers\Admin\FaqController::class, 'updateActive'])->name('faqs.update-active');
        Route::post('faqs/bulk/active', [Controllers\Admin\FaqController::class, 'updateBulkActive'])->name('faqs.update-bulk-active');
        Route::post('faqs/bulk/inactive', [Controllers\Admin\FaqController::class, 'updateBulkInactive'])->name('faqs.update-bulk-inactive');
        Route::post('faqs/{faq}/clone', [Controllers\Admin\FaqController::class, 'clone'])->name('faqs.clone');
        Route::resource('faqs', Controllers\Admin\FaqController::class);
    });
    Route::resource('surveys', Controllers\Admin\SurveyController::class);
    Route::group(['prefix' => 'orientations', 'as' => 'orientations.', 'middleware' => ['permission:manage_activity_approval']], function () {
        Route::put('approvals/{approval}/publish', [Controllers\Admin\OrientationApprovalController::class, 'updatePublish'])->name('approvals.update-publish');
        Route::put('approvals/{approval}/correction', [Controllers\Admin\OrientationApprovalController::class, 'updateCorrection'])->name('approvals.update-correction');
        Route::put('approvals/{approval}/cancel', [Controllers\Admin\OrientationApprovalController::class, 'updateCancel'])->name('approvals.update-cancel');
        Route::put('approvals/{approval}/approve', [Controllers\Admin\OrientationApprovalController::class, 'updateApprove'])->name('approvals.update-approve');
        Route::post('approvals/{approval}/notify-student', [Controllers\Admin\OrientationApprovalController::class, 'notifyStudent'])->name('approvals.notify-student');
        Route::resource('approvals', Controllers\Admin\OrientationApprovalController::class);
    });
    Route::group(['middleware' => ['permission:manage_orientations']], function () {
        Route::delete('orientations/{orientation}/attendance/{attendance}', [Controllers\Admin\OrientationController::class, 'destroyAttendance'])->name('orientations.destroy-attendance');
        Route::delete('orientations/{orientation}/file/{file}', [Controllers\Admin\OrientationController::class, 'destroyFile'])->name('orientations.destroy-file');
        Route::delete('orientations/{orientation}/link/{link}', [Controllers\Admin\OrientationController::class, 'destroyLink'])->name('orientations.destroy-link');
        Route::delete('orientations/{orientation}/recording-file/{recording}', [Controllers\Admin\OrientationController::class, 'destroyRecordingFile'])->name('orientations.destroy-recording-file');
        Route::delete('orientations/{orientation}/recording-link/{recording}', [Controllers\Admin\OrientationController::class, 'destroyRecordingLink'])->name('orientations.destroy-recording-link');
        Route::get('orientations/calculate-student', [Controllers\Admin\OrientationController::class, 'calculateStudent'])->name('orientations.calculate-student');
        Route::post('orientations/{orientation}/attendance', [Controllers\Admin\OrientationController::class, 'storeAttendance'])->name('orientations.store-attendance');
        Route::post('orientations/{orientation}/clone', [Controllers\Admin\OrientationController::class, 'clone'])->name('orientations.clone');
        Route::post('orientations/{orientation}/recording', [Controllers\Admin\OrientationController::class, 'storeRecording'])->name('orientations.store-recording');
        Route::put('orientations/{orientation}/approval', [Controllers\Admin\OrientationController::class, 'updateApproval'])->name('orientations.update-approval');
        Route::put('orientations/{orientation}/link/{link}', [Controllers\Admin\OrientationController::class, 'updateLink'])->name('orientations.update-link');
        Route::resource('orientations', Controllers\Admin\OrientationController::class);
    });


    Route::group(['middleware' => ['permission:view_orientations']], function () {
        Route::get('view-orientations', [Controllers\Admin\OrientationController::class, 'index2'])->name('orientations.index2');
        Route::get('view-orientations/{orientation}', [Controllers\Admin\OrientationController::class, 'show'])->name('orientations.show2');
    });

    Route::group(['middleware' => ['permission:manage_student_lists']], function () {
        Route::get('students/{student}/orientation', [Controllers\Admin\StudentController::class, 'showOrientation'])->name('students.show-orientation');
        Route::get('point-stats/students', [Controllers\Admin\StudentPointController::class, 'index'])->name('point.students');
        Route::resource('students', Controllers\Admin\StudentController::class);
    });
    Route::group(['prefix' => 'reports', 'as' => 'reports.', 'middleware' => ['permission:access_report_login|access_report_attendance|access_report_registration|access_report_activity|access_report_survey|access_report_gamification|access_report_audit']], function () {
        Route::get('student-login', [Controllers\Admin\ReportController::class, 'indexStudentLogin'])->name('student-login.index');
        Route::get('student-attendance', [Controllers\Admin\ReportController::class, 'indexStudentAttendance'])->name('student-attendance.index');
        Route::get('student-registration', [Controllers\Admin\ReportController::class, 'indexStudentRegistration'])->name('student-registration.index');
        Route::get('student-activity', [Controllers\Admin\ReportController::class, 'indexStudentActivity'])->name('student-activity.index');

        Route::get('student-survey', [Controllers\Admin\ReportController::class, 'indexStudentSurvey'])->name('student-survey.index');
        Route::get('student-survey', [Controllers\Admin\ReportController::class, 'indexStudentSurvey'])->name('student-survey.index');
        Route::get('audit-trails', [Controllers\Admin\ReportController::class, 'indexAuditTrail'])->name('audit-trail.index');
        Route::get('view-page', [Controllers\Admin\ReportController::class, 'indexViewPage'])->name('view-page.index');
        Route::get('gamification', [Controllers\Admin\ReportController::class, 'indexGamification'])->name('gamification.index');
    });

    Route::group(['prefix' => 'pre-orientations', 'as' => 'preorientation', 'middleware' => 'permission:manage_pre_orientations'], function (Router $router) {
        $router->get('/', [Controllers\Admin\PreOrientationController::class, 'index'])->name('.index');
        $router->get('create', [Controllers\Admin\PreOrientationController::class, 'create'])->name('.create');
        $router->post('store', [Controllers\Admin\PreOrientationController::class, 'store'])->name('.store');
        $router->get('preview/{uuid}', [Controllers\Admin\PreOrientationController::class, 'show'])->name('.show');
        $router->get('edit/{uuid}', [Controllers\Admin\PreOrientationController::class, 'edit'])->name('.edit');
        $router->patch('update/{id}', [Controllers\Admin\PreOrientationController::class, 'update'])->name('.update');
        $router->delete('delete/{uuid}', [Controllers\Admin\PreOrientationController::class, 'destroy'])->name('.delete');
    });

    Route::group(['middleware' => ['permission:manage_settings'], 'prefix' => 'settings', 'as' => 'settings.'], function (Router $router) {
        Route::get('survey', [Controllers\Admin\Setting\SurveyController::class, 'index'])->name('survey.index');
        Route::post('survey', [Controllers\Admin\Setting\SurveyController::class, 'store'])->name('survey.store');
        Route::post('survey/upload', [Controllers\Admin\Setting\SurveyController::class, 'upload'])->name('survey.upload');


        Route::get('portal', [Controllers\Admin\Setting\PortalController::class, 'index'])->name('portal.index');
        Route::post('portal', [Controllers\Admin\Setting\PortalController::class, 'store'])->name('portal.store');
        Route::get('orientation', [Controllers\Admin\Setting\OrientationController::class, 'index'])->name('orientation.index');
        Route::post('orientation', [Controllers\Admin\Setting\OrientationController::class, 'store'])->name('orientation.store');
        Route::get('pages', [Controllers\Admin\Setting\PageController::class, 'index'])->name('pages.index');
        Route::put('pages/{page}', [Controllers\Admin\Setting\PageController::class, 'update'])->name('pages.update');

        Route::resource('mails', Controllers\Admin\Setting\MailController::class);
        
        Route::post('campuses/bulk/active', [Controllers\Admin\Setting\CampusController::class, 'updateBulkActive'])->name('campuses.update-bulk-active');
        Route::post('campuses/bulk/inactive', [Controllers\Admin\Setting\CampusController::class, 'updateBulkInactive'])->name('campuses.update-bulk-inactive');
        Route::put('campuses/{campus}/active', [Controllers\Admin\Setting\CampusController::class, 'updateActive'])->name('campuses.update-active');
        Route::get('campuses', [Controllers\Admin\Setting\CampusController::class, 'index'])->name('campuses.index');
        Route::post('faculties/bulk/active', [Controllers\Admin\Setting\FacultyController::class, 'updateBulkActive'])->name('faculties.update-bulk-active');
        Route::post('faculties/bulk/inactive', [Controllers\Admin\Setting\FacultyController::class, 'updateBulkInactive'])->name('faculties.update-bulk-inactive');
        Route::put('faculties/{faculty}/active', [Controllers\Admin\Setting\FacultyController::class, 'updateActive'])->name('faculties.update-active');
        Route::get('faculties', [Controllers\Admin\Setting\FacultyController::class, 'index'])->name('faculties.index');
        Route::post('intakes/bulk/active', [Controllers\Admin\Setting\IntakeController::class, 'updateBulkActive'])->name('intakes.update-bulk-active');
        Route::post('intakes/bulk/inactive', [Controllers\Admin\Setting\IntakeController::class, 'updateBulkInactive'])->name('intakes.update-bulk-inactive');
        
        Route::put('intakes/{intake}/orientation-activities-active', [Controllers\Admin\Setting\IntakeController::class, 'updateOrientationActivitiesActive'])->name('intakes.update-orientation-activities-active');
        Route::put('intakes/{intake}/timetable-active', [Controllers\Admin\Setting\IntakeController::class, 'updateTimetableActive'])->name('intakes.update-timetable-active');

        Route::put('intakes/{intake}', [Controllers\Admin\Setting\IntakeController::class, 'update'])->name('intakes.update');

        Route::put('intakes/{intake}/active', [Controllers\Admin\Setting\IntakeController::class, 'updateActive'])->name('intakes.update-active');
        Route::get('intakes', [Controllers\Admin\Setting\IntakeController::class, 'index'])->name('intakes.index');
        Route::post('level-studies/bulk/active', [Controllers\Admin\Setting\LevelStudyController::class, 'updateBulkActive'])->name('level-studies.update-bulk-active');
        Route::post('level-studies/bulk/inactive', [Controllers\Admin\Setting\LevelStudyController::class, 'updateBulkInactive'])->name('level-studies.update-bulk-inactive');
        Route::put('level-studies/{level_study}/active', [Controllers\Admin\Setting\LevelStudyController::class, 'updateActive'])->name('level-studies.update-active');
        Route::get('level-studies', [Controllers\Admin\Setting\LevelStudyController::class, 'index'])->name('level-studies.index');
        Route::post('localities/bulk/active', [Controllers\Admin\Setting\LocalityController::class, 'updateBulkActive'])->name('localities.update-bulk-active');
        Route::post('localities/bulk/inactive', [Controllers\Admin\Setting\LocalityController::class, 'updateBulkInactive'])->name('localities.update-bulk-inactive');
        Route::put('localities/{locality}/active', [Controllers\Admin\Setting\LocalityController::class, 'updateActive'])->name('localities.update-active');
        Route::get('localities', [Controllers\Admin\Setting\LocalityController::class, 'index'])->name('localities.index');
        Route::post('mobilities/bulk/active', [Controllers\Admin\Setting\MobilityController::class, 'updateBulkActive'])->name('mobilities.update-bulk-active');
        Route::post('mobilities/bulk/inactive', [Controllers\Admin\Setting\MobilityController::class, 'updateBulkInactive'])->name('mobilities.update-bulk-inactive');
        Route::put('mobilities/{mobility}/active', [Controllers\Admin\Setting\MobilityController::class, 'updateActive'])->name('mobilities.update-active');
        Route::get('mobilities', [Controllers\Admin\Setting\MobilityController::class, 'index'])->name('mobilities.index');
        Route::post('programmes/bulk/active', [Controllers\Admin\Setting\ProgrammeController::class, 'updateBulkActive'])->name('programmes.update-bulk-active');
        Route::post('programmes/bulk/inactive', [Controllers\Admin\Setting\ProgrammeController::class, 'updateBulkInactive'])->name('programmes.update-bulk-inactive');
        Route::put('programmes/{programme}/active', [Controllers\Admin\Setting\ProgrammeController::class, 'updateActive'])->name('programmes.update-active');
        Route::get('programmes', [Controllers\Admin\Setting\ProgrammeController::class, 'index'])->name('programmes.index');
        Route::post('schools/bulk/active', [Controllers\Admin\Setting\SchoolController::class, 'updateBulkActive'])->name('schools.update-bulk-active');
        Route::post('schools/bulk/inactive', [Controllers\Admin\Setting\SchoolController::class, 'updateBulkInactive'])->name('schools.update-bulk-inactive');
        Route::put('schools/{school}/active', [Controllers\Admin\Setting\SchoolController::class, 'updateActive'])->name('schools.update-active');
        Route::get('schools', [Controllers\Admin\Setting\SchoolController::class, 'index'])->name('schools.index');
        Route::post('files', [Controllers\Admin\Setting\FileController::class, 'store'])->name('files.store');
        Route::get('files', [Controllers\Admin\Setting\FileController::class, 'index'])->name('files.index');
        
        Route::get('quicklink', [Controllers\Admin\Setting\QuicklinkController::class, 'index'])->name('quicklink.index');
        Route::post('quicklink', [Controllers\Admin\Setting\QuicklinkController::class, 'store'])->name('quicklink.store');
    });
});

Route::group(['prefix' => 'devtool', 'as' => 'devtool.'], function () {
    Route::group(['middleware' => ['devtool']], function () {
        Route::get('login/{email}', [Controllers\Devtool\DebugController::class, 'login'])->name('login');
        Route::get('debug/{name}', [Controllers\Devtool\DebugController::class, 'index'])->name('debug');
        Route::get('debug/student/upsert', [Controllers\Devtool\DebugController::class, 'upsertStudent'])->name('debug.upsert-student');
        Route::get('optimize', [Controllers\Devtool\DebugController::class, 'optimize'])->name('optimize');
        Route::get('fix-label', [Controllers\Devtool\DebugController::class, 'fixLabel'])->name('fixLabel');
        //        Route::get('scaffolds', [Controllers\Devtool\ScaffoldController::class, 'index'])->name('scaffolds');
    });
});
