<?php

use App\Http\Controllers\Api\PreOrientation;
use App\Http\Controllers\Api\Announcement;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'api', 'middleware' => ['web']], function () {
    Route::prefix('announcements')->name('.announcements')->group(function (Router $router) {
        $router->post('publish', Announcement\PublishController::class)->name('.publish');
        $router->post('unpublish', Announcement\UnpublishController::class)->name('.unpublish');
        $router->post('bulk-publish', Announcement\BulkPublishController::class)->name('.bulk.publish');
        $router->post('bulk-unpublish', Announcement\BulkUnpublishController::class)->name('.bulk.unpublish');
    });

    Route::prefix('pre-orientations')->name('.preorientation')->group(function (Router $router) {
        $router->post('publish', PreOrientation\PublishController::class)->name('.publish');
        $router->post('unpublish', PreOrientation\UnpublishController::class)->name('.unpublish');
        $router->post('bulk-publish', PreOrientation\BulkPublishController::class)->name('.bulk.publish');
        $router->post('bulk-unpublish', PreOrientation\BulkUnpublishController::class)->name('.bulk.unpublish');
        $router->post('mark-as-complete', PreOrientation\MarkCompleteController::class)->name('.markcomplete');
        $router->post('submit-mcq', PreOrientation\SubmitMcqAnswerController::class)->name('.submitmcq');
        $router->post('clone', PreOrientation\CloneController::class)->name('.clone');

        $router->post('update-component', PreOrientation\UpdateComponentController::class)->name('.update-component');
    });
});
