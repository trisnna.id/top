<?php declare(strict_types=1);

namespace App\Constants;

use App\Models\Locality;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

final class NationalityConstant implements Arrayable
{
    /**
     * Define nationality type international
     * 
     * @var string
     */
    const NATIONALITY_INTER = 'International';

    /**
     * Define nationality type local
     * 
     * @var string
     */
    const NATIONALITY_LOCAL = 'Local';

    /**
     * The Singleton's instance is stored in a static field. This field is an
     * array, because we'll allow our Singleton to have subclasses. Each item in
     * this array will be an instance of a specific Singleton's subclass. You'll
     * see how this works in a moment.
     */
    private static $instances = [];

    /**
     * The Singleton's constructor should always be private to prevent direct
     * construction calls with the `new` operator.
     */
    protected function __construct() { }

    /**
     * Singletons should not be cloneable.
     */
    protected function __clone() { }

    /**
     * Singletons should not be restorable from strings.
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    /**
     * This is the static method that controls the access to the singleton
     * instance. On the first run, it creates a singleton object and places it
     * into the static field. On subsequent runs, it returns the client existing
     * object stored in the static field.
     *
     * This implementation lets you subclass the Singleton class while keeping
     * just one instance of each subclass around.
     */
    public static function getInstance(): self
    {
        $cls = static::class;

        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static();
        }

        return self::$instances[$cls];
    }

    /**
     * Get the locality records
     *
     * @param   array|string $ids
     * @param   array|string $columns
     * @return  Collection
     */
    public function get($ids = null, $columns = ['id', 'name']): Collection
    {
        $records = Locality::select($columns);

        if (is_string($ids)) {
            return $records->where('id', $ids)->get();
        }

        return $records->whereIn('id', $ids)->get();
    }

    /**
     * Get local nationality as an object
     *
     * @return Locality
     */
    public function local(): Locality
    {
        $locality = Locality::select(['id', 'name'])->local()->first();

        if (is_null($locality)) {
            $locality = Locality::create(['name' => self::NATIONALITY_LOCAL]);
            $locality = Locality::select(['id', 'name'])->local()->first();
        }

        return $locality;
    }

    /**
     * Get international nationality as an object
     *
     * @return Locality
     */
    public function international(): Locality
    {
        $locality = Locality::select(['id', 'name'])->inter()->first();

        if (is_null($locality)) {
            $locality = Locality::create(['name' => self::NATIONALITY_INTER]);
            $locality = Locality::select(['id', 'name'])->inter()->first();
        }

        return $locality;
    }

    /**
     * check if the current nationality is local
     *
     * @param   string|int $value
     * @return  bool
     */
    public function isInternational($value): bool
    {
        return $value === self::NATIONALITY_INTER ||
            $value == $this->local()->id;
    }

    /**
     * check if the current nationality is local
     *
     * @param   string $value
     * @return  bool
     */
    public function isLocal(string $value): bool
    {
        return $value === self::NATIONALITY_LOCAL ||
            $value == $this->international()->id;
    }

    /**
     * check if the specified nationalities contains all
     *
     * @param   array $values
     * @return  bool
     */
    public function itContainAll(array $values): bool
    {
        return Arr::has($values, 'all');
    }

    /**
     * Check if the specified nationalities is valid
     *
     * @param   array $values
     * @return  bool
     */
    public function isValid(array $values): bool
    {
        if (empty($values)) {
            return false;
        }

        $localities = Locality::all('id')->map(function (Locality $item) { return $item->id; })->toArray();
        array_push($localities, 'all');

        foreach ($values as $locality) {
            if (!in_array($locality, $localities)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the instance as an array.
     *
     * @return array<TKey, TValue>
     */
    public function toArray()
    {
        return [
            self::NATIONALITY_INTER,
            self::NATIONALITY_LOCAL       
        ];
    }

    /**
     * Convert constants as selectable options
     *
     * @return  Collection
     */
    public function toOptions(): Collection
    {
        return new Collection([
            $this->international(),
            $this->local(),
        ]);
    }

    /**
     * Get the nationalities as encoded json
     *
     * @return string
     */
    public function toString(): string
    {
        return json_encode($this->toOptions()->toArray());
    }
}
