<?php

namespace App\Constants;

class RoleConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const ROLE_ADMIN = 'admin';
    public const ROLE_STAFF = 'staff';
    public const ROLE_ACTIVITY_MANAGER = 'activity_manager';
    public const ROLE_STUDENT = 'student';
    public const ROLE_ORIENTATION_LEADER = 'orientation_leader';
    public const GUARD_WEB = 'web';
    public const STATUS_ACTIVE = true;
    public const STATUS_INACTIVE = false;

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $results = [];
        return $results;
    }
}
