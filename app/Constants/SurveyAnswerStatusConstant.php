<?php

namespace App\Constants;

class SurveyAnswerStatusConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const NOT_ANSWERED = 1;
    public const ANSWERING = 2;
    public const ANSWERED = 3;

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $results = [];
        switch ($option) {
            default:
                $results = [
                    self::NOT_ANSWERED => 'Not Answered',
                    self::ANSWERING => 'Answering',
                    self::ANSWERED => 'Answered',
                ];
                break;
        }
        return $results;
    }
}
