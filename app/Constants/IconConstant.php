<?php

namespace App\Constants;

use Illuminate\Support\Str;

class IconConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const ANNOUNCEMENT = 'announcement';
    public const BREAK = 'break';
    public const COMPLEMENTARY = 'complementary';
    public const COMPULSORY_CORE = 'compulsory_core';
    public const DASHBOARD = 'dashboard';
    public const FAQ = 'faq';
    public const CONTACT_US = 'contact_us';
    public const NEWS = 'news';
    public const NEXT = 'next';
    public const ORIENTATION = 'orientation';
    public const POINT = 'point';
    public const PREVIOUS = 'previous';
    public const PRE_ORIENTATION = 'pre_orientation';
    public const QUIZ = 'quiz';
    public const RESOURCE = 'resource';
    public const SEARCH = 'search';
    public const STAFF = 'staff';
    public const SAVE = 'save';
    public const STUDENT = 'student';
    public const SUBMIT = 'submit';
    public const TIMELINE = 'timeline';
    public const TIMETABLE = 'timetable';
    public const EXT_DOCX = 'ext_docx';
    public const EXT_FILE = 'ext_file';
    public const EXT_MP4 = 'ext_mp4';
    public const EXT_PDF = 'ext_pdf';
    public const EXT_PPTX = 'ext_pptx';
    public const EXT_TXT = 'ext_txt';
    public const EXT_XLSX = 'ext_xlsx';
    public const ACTION1 = 'action1'; // Refer to ActionLogConstant
    public const ACTION2 = 'action2'; // Refer to ActionLogConstant
    public const ACTION3 = 'action3'; // Refer to ActionLogConstant
    public const ACTION4 = 'action4'; // Refer to ActionLogConstant
    public const ACTION5 = 'action5'; // Refer to ActionLogConstant
    public const PUBLISH = 'publish';
    public const UNPUBLISH = 'unpublish';
    public const BTN_APPROVAL = 'btn_approval';
    public const BTN_EDIT = 'btn_edit';
    public const BTN_SHOW = 'btn_show';
    public const BTN_DELETE = 'btn_delete';
    public const BTN_CLONE = 'btn_clone';
    public const BTN_BANNED = 'btn_banned';
    public const BTN_PUBLISH = 'btn_publish';
    public const BTN_UNPUBLISH = 'btn_unpublish';
    public const BTN_SUBMIT = 'btn_submit';

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $colors = getConstant('ColorConstant');
        $icons = collect([
            self::SAVE => [
                'id' => self::SAVE,
                'color' => $colors['non-photo-blue'],
                'colors' => [$colors['non-photo-blue']],
                'icon_class' => 'fas fa-save',
            ],
            self::BTN_APPROVAL => [
                'id' => self::BTN_APPROVAL,
                'color' => $colors['bc687a'],
                'colors' => [$colors['bc687a']],
                'icon_class' => 'fa-solid fa-file-pen',
            ],
            self::BTN_EDIT => [
                'id' => self::BTN_EDIT,
                'color' => $colors['black'],
                'colors' => [$colors['black']],
                'icon_class' => 'fas fa-edit',
            ],
            self::BTN_SHOW => [
                'id' => self::BTN_SHOW,
                'color' => $colors['info'],
                'colors' => [$colors['info']],
                'icon_class' => 'fas fa-eye',
            ],
            self::BTN_DELETE => [
                'id' => self::BTN_DELETE,
                'color' => $colors['red'],
                'colors' => [$colors['red']],
                'icon_class' => 'fas fa-trash',
            ],
            self::BTN_CLONE => [
                'id' => self::BTN_CLONE,
                'color' => $colors['sanguine-brown'],
                'colors' => [$colors['sanguine-brown']],
                'icon_class' => 'fas fa-copy',
            ],
            self::BTN_BANNED => [
                'id' => self::BTN_BANNED,
                'color' => $colors['danger'],
                'colors' => [$colors['danger']],
                'icon_class' => 'fas fa-paper-plane',
            ],
            self::BTN_PUBLISH => [
                'id' => self::BTN_PUBLISH,
                'color' => $colors['success'],
                'colors' => [$colors['success']],
                'icon_class' => 'fas fa-cloud-upload',
            ],
            self::BTN_UNPUBLISH => [
                'id' => self::BTN_UNPUBLISH,
                'color' => $colors['poppy'],
                'colors' => [$colors['poppy']],
                'icon_class' => 'fas fa-cloud-download',
            ],
            self::BTN_SUBMIT => [
                'id' => self::BTN_SUBMIT,
                'color' => $colors['c2e7da'],
                'colors' => [$colors['c2e7da']],
                'icon_class' => 'fas fa-paper-plane',
            ],
            self::UNPUBLISH => [
                'id' => self::UNPUBLISH,
                'color' => $colors['danger'],
                'colors' => [$colors['danger']],
                'icon_class' => 'fas fa-cloud-download',
            ],
            self::PUBLISH => [
                'id' => self::PUBLISH,
                'color' => $colors['success'],
                'colors' => [$colors['success']],
                'icon_class' => 'fas fa-cloud-upload',
            ],
            self::ACTION5 => [
                'id' => self::ACTION5,
                'color' => $colors['primary'],
                'colors' => [$colors['primary']],
                'icon_class' => 'fas fa-share',
            ],
            self::ACTION4 => [
                'id' => self::ACTION4,
                'color' => $colors['warning'],
                'colors' => [$colors['warning']],
                'icon_class' => 'fas fa-reply',
            ],
            self::ACTION3 => [
                'id' => self::ACTION3,
                'color' => $colors['danger'],
                'colors' => [$colors['danger']],
                'icon_class' => 'fas fa-times',
            ],
            self::ACTION2 => [
                'id' => self::ACTION2,
                'color' => $colors['success'],
                'colors' => [$colors['success']],
                'icon_class' => 'fas fa-check',
            ],
            self::ACTION1 => [
                'id' => self::ACTION1,
                'color' => $colors['primary'],
                'colors' => [$colors['primary']],
                'icon_class' => 'fas fa-paper-plane',
            ],
            self::COMPULSORY_CORE => [
                'id' => self::COMPULSORY_CORE,
                'color' => $colors['e9b7ce'],
                'colors' => [$colors['e9b7ce']],
                'icon_class' => 'fa-solid fa-arrows-spin',
            ],
            self::COMPLEMENTARY => [
                'id' => self::COMPLEMENTARY,
                'color' => $colors['d3f3f1'],
                'colors' => [$colors['d3f3f1']],
                'icon_class' => 'fa-solid fa-code-compare',
            ],
            self::BREAK => [
                'id' => self::BREAK,
                'color' => $colors['pale-red'],
                'colors' => [$colors['pale-red']],
                'icon_class' => 'fa-solid fa-code-compare',
            ],
            self::PRE_ORIENTATION => [
                'id' => self::PRE_ORIENTATION,
                'color' => $colors['primary'],
                'colors' => [$colors['primary']],
                'icon_class' => 'fa-solid fa-calendar-days',
            ],
            self::NEWS => [
                'id' => self::NEWS,
                'color' => $colors['info'],
                'colors' => [$colors['info']],
                'icon_class' => 'fa-solid fa-newspaper',
            ],
            self::ANNOUNCEMENT => [
                'id' => self::ANNOUNCEMENT,
                'color' => $colors['success'],
                'colors' => [$colors['success']],
                'icon_class' => 'fa-solid fa-rss',
            ],
            self::DASHBOARD => [
                'id' => self::DASHBOARD,
                'color' => $colors['success'],
                'colors' => [$colors['success']],
                'icon_class' => 'fa-solid fa-rss',
            ],
            self::STUDENT => [
                'id' => self::STUDENT,
                'color' => $colors['info'],
                'colors' => [$colors['info']],
                'icon_class' => 'fa-solid fa-graduation-cap',
                'icon_img' => url('media/icons/taylors/student.png'),
            ],
            self::STAFF => [
                'id' => self::STAFF,
                'color' => $colors['primary'],
                'colors' => [$colors['primary']],
                'icon_class' => 'fa-solid fa-user',
                'icon_img' => url('media/icons/taylors/user.png'),
            ],
            self::TIMELINE => [
                'id' => self::TIMELINE,
                'color' => $colors['primary'],
                'colors' => [$colors['primary']],
                'icon_class' => 'fa-solid fa-timeline',
            ],
            self::TIMETABLE => [
                'id' => self::TIMETABLE,
                'color' => $colors['info'],
                'colors' => [$colors['info']],
                'icon_class' => 'fa-solid fa-table',
            ],
            self::ORIENTATION => [
                'id' => self::ORIENTATION,
                'color' => $colors['info'],
                'colors' => [$colors['info'], $colors['primary']],
                'icon_class' => 'fa-solid fa-calendar',
            ],
            self::POINT => [
                'id' => self::POINT,
                'color' => $colors['warning'],
                'colors' => [$colors['warning']],
                'icon_class' => 'fas fa-star',
            ],
            self::RESOURCE => [
                'id' => self::RESOURCE,
                'color' => $colors['info'],
                'colors' => [$colors['info']],
                'icon_class' => 'fas fa-file-alt',
            ],
            self::FAQ => [
                'id' => self::FAQ,
                'color' => $colors['info'],
                'colors' => [$colors['info']],
                'icon_class' => 'fas fa-question',
            ],
            self::CONTACT_US => [
                'id' => self::CONTACT_US,
                'color' => $colors['info'],
                'colors' => [$colors['info']],
                'icon_class' => 'fas fa-phone',
            ],
            self::SEARCH => [
                'id' => self::SEARCH,
                'color' => $colors['info'],
                'colors' => [$colors['info']],
                'icon_class' => 'fas fa-search',
            ],
            self::QUIZ => [
                'id' => self::QUIZ,
                'color' => $colors['info'],
                'colors' => [$colors['info']],
                'icon_class' => 'fas fa-poll-h',
            ],
            self::NEXT => [
                'id' => self::NEXT,
                'color' => $colors['primary'],
                'colors' => [$colors['primary']],
                'icon_class' => 'fa-solid fa-arrow-right',
            ],
            self::PREVIOUS => [
                'id' => self::PREVIOUS,
                'color' => $colors['primary'],
                'colors' => [$colors['primary']],
                'icon_class' => 'fa-solid fa-arrow-left',
            ],
            self::SUBMIT => [
                'id' => self::SUBMIT,
                'color' => $colors['warning'],
                'colors' => [$colors['warning']],
                'icon_class' => 'fa-solid fa-paper-plane',
            ],
            self::EXT_DOCX => [
                'id' => self::EXT_DOCX,
                'color' => $colors['info'],
                'icon_class' => 'fa-solid fa-file-word',
            ],
            self::EXT_FILE => [
                'id' => self::EXT_FILE,
                'color' => $colors['info'],
                'icon_class' => 'fa-solid fa-file',
            ],
            self::EXT_MP4 => [
                'id' => self::EXT_MP4,
                'color' => $colors['info'],
                'icon_class' => 'fa-solid fa-file-video',
            ],
            self::EXT_PDF => [
                'id' => self::EXT_PDF,
                'color' => $colors['info'],
                'icon_class' => 'fa-solid fa-file-pdf',
            ],
            self::EXT_PPTX => [
                'id' => self::EXT_PPTX,
                'color' => $colors['info'],
                'icon_class' => 'fa-solid fa-file-powerpoint',
            ],
            self::EXT_TXT => [
                'id' => self::EXT_TXT,
                'color' => $colors['info'],
                'icon_class' => 'fa-solid fa-file-lines',
            ],
            self::EXT_XLSX => [
                'id' => self::EXT_XLSX,
                'color' => $colors['info'],
                'icon_class' => 'fa-solid fa-file-excel',
            ],
        ]);
        $results = [];
        switch ($option) {
            case 'icon_class':
            case 'icon_img':
            case 'color':
            case 'colors':
                $results = $icons->pluck($option, 'id')->toArray();
                break;
            default:
                $results = $icons->toArray();
                break;
        }
        return $results;
    }
}
