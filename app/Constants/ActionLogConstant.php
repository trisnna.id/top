<?php

namespace App\Constants;

class ActionLogConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const USER_SUBMIT_ORIENTATION = 1;
    public const ORIENTATION_APPROVED = 2;
    public const ORIENTATION_CANCELLED = 3;
    public const ORIENTATION_REQUIRE_CORRECTION = 4;
    public const USER_SUBMIT_CORRECTION = 5;

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $icons = getConstant('IconConstant');
        $actions = collect([
            self::USER_SUBMIT_CORRECTION => [
                'id' => self::USER_SUBMIT_CORRECTION,
                'title' => 'User Submit Correction Orientation Activity',
                'log' => [
                    'label' => 'submit the correction orientation activity',
                    'title' => 'Orientation activity corrected at'
                ],
                'icon' => $icons[IconConstant::ACTION5],
            ],
            self::USER_SUBMIT_ORIENTATION => [
                'id' => self::USER_SUBMIT_ORIENTATION,
                'title' => 'User Submit Orientation Activity',
                'log' => [
                    'label' => 'submit the orientation activity',
                    'title' => 'Orientation activity created at'
                ],
                'icon' => $icons[IconConstant::ACTION1],
            ],
            self::ORIENTATION_APPROVED => [
                'id' => self::ORIENTATION_APPROVED,
                'title' => 'Orientation Activity Approved',
                'log' => [
                    'label' => 'approve the orientation activity',
                    'title' => 'Orientation activity approved at'
                ],
                'icon' => $icons[IconConstant::ACTION2],
            ],
            self::ORIENTATION_CANCELLED => [
                'id' => self::ORIENTATION_CANCELLED,
                'title' => 'Orientation Activity Cancelled',
                'log' => [
                    'label' => 'cancel the orientation activity',
                    'title' => 'Orientation activity cancelled at'
                ],
                'icon' => $icons[IconConstant::ACTION3],
            ],
            self::ORIENTATION_REQUIRE_CORRECTION => [
                'id' => self::ORIENTATION_REQUIRE_CORRECTION,
                'title' => 'Orientation Activity Require Correction',
                'log' => [
                    'label' => 'return back the orientation activity for correction',
                    'title' => 'Orientation activity need a correction at'
                ],
                'icon' => $icons[IconConstant::ACTION4],
            ],
        ]);
        $results = [];
        switch ($option) {
            case 'name':
            case 'title':
            case 'log':
            case 'icon':
                $results = $actions->pluck($option, 'id')->toArray();
                break;
            default:
                $results = $actions->toArray();
                break;
        }
        return $results;
    }
}
