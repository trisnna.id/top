<?php

namespace App\Constants;

class OrientationTypeConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const COMPULSORY_CORE = 1;
    public const COMPLEMENTARY = 2;
    public const BREAK = 3;

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $icons = getConstant('IconConstant');
        $types = collect([
            self::BREAK => [
                'id' => self::BREAK,
                'title' => 'Break',
                'css_calendar' => 'bg-pale-red text-black border-white-fc-daygrid-event-dott',
                'color' => $icons['break']['color'],
                'icon' => $icons['break'],
            ],
            self::COMPULSORY_CORE => [
                'id' => self::COMPULSORY_CORE,
                'title' => 'Compulsory Core',
                'css_calendar' => 'bg-e9b7ce text-black border-white-fc-daygrid-event-dott',
                'color' => $icons['compulsory_core']['color'],
                'icon' => $icons['compulsory_core'],
            ],
            self::COMPLEMENTARY => [
                'id' => self::COMPLEMENTARY,
                'title' => 'Complementary',
                'css_calendar' => 'bg-d3f3f1 text-black border-white-fc-daygrid-event-dot',
                'color' => $icons['complementary']['color'],
                'icon' => $icons['complementary'],
            ],
        ]);
        $results = [];
        switch ($option) {
            case 'title':
            case 'css_calendar':
            case 'color':
            case 'icon':
                $results = $types->pluck($option, 'id')->toArray();
                break;
            default:
                $results = $types->toArray();
                break;
        }
        return $results;
    }
}
