<?php

namespace App\Constants;

class ColorConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    | Reference: resources\sass\_variables.scss
    |--------------------------------------------------------------------------
    */

    public const BLACK = 'black';
    public const BLUE = 'blue';
    public const DELFT_BLUE = 'delft-blue';
    public const CHOCOLATE = 'chocolate';
    public const DANGER = 'danger';
    public const GOLD = 'gold';
    public const DARK = 'dark';
    public const INFO = 'info';
    public const LIGHT = 'light';
    public const LIGHT_GRAYISH_GREEN = 'light-grayish-green';
    public const LIME = 'lime';
    public const NON_PHOTO_BLUE = 'non-photo-blue';
    public const ORANGE = 'orange';
    public const PALE_RED = 'pale-red';
    public const POPPY = 'poppy';
    public const PRIMARY = 'primary';
    public const RED = 'red';
    public const SECONDARY = 'secondary';
    public const SUCCESS = 'success';
    public const WARNING = 'warning';
    public const WHITE = 'white';
    public const SANGUINE_BROWN = 'sanguine-brown';
    public const E9B7CE = 'e9b7ce';
    public const D3F3F1 = 'd3f3f1';
    public const BC687A = 'bc687a';
    public const C2E7DA = 'c2e7da';

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $colors = collect([
            self::D3F3F1 => [
                'id' => self::D3F3F1,
                'class' => 'd3f3f1',
                'code' => '#d3f3f1',
            ],
            self::BC687A => [
                'id' => self::BC687A,
                'class' => 'bc687a',
                'code' => '#bc687a',
            ],
            self::C2E7DA => [
                'id' => self::C2E7DA,
                'class' => 'c2e7da',
                'code' => '#c2e7da',
            ],
            self::E9B7CE => [
                'id' => self::E9B7CE,
                'class' => 'e9b7ce',
                'code' => '#e9b7ce',
            ],
            self::SANGUINE_BROWN => [
                'id' => self::SANGUINE_BROWN,
                'class' => 'sanguine-brown',
                'code' => '#8b3232',
            ],
            self::NON_PHOTO_BLUE => [
                'id' => self::NON_PHOTO_BLUE,
                'class' => 'non-photo-blue',
                'code' => '#9ed8db',
            ],
            self::POPPY => [
                'id' => self::POPPY,
                'class' => 'poppy',
                'code' => '#d64045',
            ],
            self::PALE_RED => [
                'id' => self::PALE_RED,
                'class' => 'pale-red',
                'code' => '#FCB9AA',
            ],
            self::LIGHT_GRAYISH_GREEN => [
                'id' => self::LIGHT_GRAYISH_GREEN,
                'class' => 'light-grayish-green',
                'code' => '#CCE2CB',
            ],
            self::GOLD => [
                'id' => self::GOLD,
                'class' => 'gold',
                'code' => '#FFD700',
            ],
            self::DELFT_BLUE => [
                'id' => self::DELFT_BLUE,
                'class' => 'delft-blue',
                'code' => '#1d3354',
            ],
            self::BLUE => [
                'id' => self::BLUE,
                'class' => 'blue',
                'code' => '#0000FF',
            ],
            self::CHOCOLATE => [
                'id' => self::CHOCOLATE,
                'class' => 'chocolate',
                'code' => '#d2691e',
            ],
            self::LIME => [
                'id' => self::LIME,
                'class' => 'lime',
                'code' => '#00ff00',
            ],
            self::ORANGE => [
                'id' => self::ORANGE,
                'class' => 'orange',
                'code' => '#d98c00',
            ],
            self::BLACK => [
                'id' => self::BLACK,
                'class' => 'black',
                'code' => '#000000',
            ],
            self::DANGER => [
                'id' => self::DANGER,
                'class' => 'danger',
                'code' => '#f1416c',
            ],
            self::DARK => [
                'id' => self::DARK,
                'class' => 'dark',
                'code' => '#181C32',
            ],
            self::INFO => [
                'id' => self::INFO,
                'class' => 'info',
                'code' => '#7239ea',
            ],
            self::LIGHT => [
                'id' => self::LIGHT,
                'class' => 'light',
                'code' => '#f5f8fa',
            ],
            self::PRIMARY => [
                'id' => self::PRIMARY,
                'class' => 'primary',
                'code' => '#009ef7',
            ],
            self::RED => [
                'id' => self::RED,
                'class' => 'red',
                'code' => '#ff0000',
            ],
            self::SECONDARY => [
                'id' => self::SECONDARY,
                'class' => 'secondary',
                'code' => '#E4E6EF',
            ],
            self::SUCCESS => [
                'id' => self::SUCCESS,
                'class' => 'success',
                'code' => '#50cd89',
            ],
            self::WARNING => [
                'id' => self::WARNING,
                'class' => 'warning',
                'code' => '#ffc700',
            ],
            self::WHITE => [
                'id' => self::WHITE,
                'class' => 'white',
                'code' => '#ffffff',
            ],
        ]);
        $results = [];
        switch ($option) {
            case 'class':
            case 'code':
                $results = $colors->pluck($option, 'id')->toArray();
                break;
            default:
                $results = $colors->toArray();
                break;
        }
        return $results;
    }
}
