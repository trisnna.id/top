<?php

namespace App\Constants;

use Illuminate\Support\Str;

class ActiveInactiveConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const INACTIVE = 0;
    public const ACTIVE = 1;

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getActiveOption(): array
    {
        return [
            'id' => self::ACTIVE,
            'name' => Str::slug('active'),
            'title' => 'Active',
            'title-publish' => 'Publish',
            'title-yes' => 'Yes',
            'color' => ColorConstant::getAvailableOptions()[ColorConstant::NON_PHOTO_BLUE],
            'icon' => IconConstant::getAvailableOptions()[IconConstant::PUBLISH],
        ];
    }

    public static function getInActiveOption(): array
    {
        return [
            'id' => self::INACTIVE,
            'name' => Str::slug('Inactive'),
            'title' => 'Inactive',
            'title-publish' => 'Unpublish',
            'title-yes' => 'No',
            'color' => ColorConstant::getAvailableOptions()[ColorConstant::POPPY],
            'icon' => IconConstant::getAvailableOptions()[IconConstant::UNPUBLISH],
        ];
    }

    public static function getAvailableOptions($option = null)
    {
        $options = collect([
            self::INACTIVE => self::getInActiveOption(),
            self::ACTIVE => self::getActiveOption(),
        ]);
        $results = [];
        switch ($option) {
            case 'name':
            case 'title':
            case 'title-publish':
            case 'title-yes':
            case 'color':
                $results = $options->pluck($option, 'id')->toArray();
                break;
            default:
                $results = $options->toArray();
                break;
        }
        return $results;
    }
}
