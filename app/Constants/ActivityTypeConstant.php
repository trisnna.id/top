<?php

namespace App\Constants;

class ActivityTypeConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const PRE_ENGAGEMENT = 1;
    public const COMPULSORY_CORE = 2;
    public const COMPLEMENTARY = 3;

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $icons = getConstant('IconConstant');
        $types = collect([
            self::PRE_ENGAGEMENT => [
                'id' => self::PRE_ENGAGEMENT,
                'title' => 'Pre-engagement',
                'css_calendar' => '',
                'color' => 'primary',
                'color_class' => '',
                'color_code' => '',
                'icon_class' => '',
            ],
            self::COMPULSORY_CORE => [
                'id' => self::COMPULSORY_CORE,
                'title' => 'Compulsory Core',
                'css_calendar' => 'bg-danger text-black border-white-fc-daygrid-event-dott',
                'color' => $icons['compulsory_core']['color'],
                'color_class' => $icons['compulsory_core']['color']['class'],
                'color_code' => $icons['compulsory_core']['color']['code'],
                'icon_class' => $icons['compulsory_core']['icon_class'],
            ],
            self::COMPLEMENTARY => [
                'id' => self::COMPLEMENTARY,
                'title' => 'Complementary',
                'css_calendar' => 'bg-info text-black border-white-fc-daygrid-event-dot',
                'color' => $icons['complementary']['color'],
                'color_class' => $icons['complementary']['color']['class'],
                'color_code' => $icons['complementary']['color']['code'],
                'icon_class' => $icons['complementary']['icon_class'],
            ],
        ]);
        $results = [];
        switch ($option) {
            case 'icon_class':
            case 'css_calendar':
            case 'color':
                $results = $types->pluck($option, 'id')->toArray();
                break;
            case 'all':
                $results = $types->toArray();
                break;
            default:
                $results = $types->pluck('title', 'id')->toArray();
                break;
        }
        return $results;
    }
}
