<?php

namespace App\Constants;

class ResourceTypeConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const ALL = 1;
    public const RESOURCE = 2;
    public const ARRIVAL = 3;

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $icons = getConstant('IconConstant');
        $types = collect([
            self::ALL => [
                'id' => self::ALL,
                'name' => 'all',
                'title' => 'Both',
                'color' => $icons['compulsory_core']['color'],
                'icon' => $icons['compulsory_core'],
            ],
            self::RESOURCE => [
                'id' => self::RESOURCE,
                'name' => 'resource',
                'title' => 'Resource',
                'color' => $icons['compulsory_core']['color'],
                'icon' => $icons['compulsory_core'],
            ],
            self::ARRIVAL => [
                'id' => self::ARRIVAL,
                'name' => 'arrival',
                'title' => 'Arrival',
                'color' => $icons['complementary']['color'],
                'icon' => $icons['complementary'],
            ],
        ]);
        $results = [];
        switch ($option) {
            case 'name':
            case 'title':
            case 'css_calendar':
            case 'color':
            case 'icon':
                $results = $types->pluck($option, 'id')->toArray();
                break;
            default:
                $results = $types->toArray();
                break;
        }
        return $results;
    }
}
