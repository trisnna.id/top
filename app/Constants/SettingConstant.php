<?php

namespace App\Constants;

class SettingConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const FILE = [
        'arrival_resource_size' => 5,
        'arrival_resource_extension' => 'png jpg jpeg mp4 txt pdf docx',
        'orientation_attachment_size' => 5,
        'orientation_attachment_extension' => 'png jpg jpeg mp4 txt pdf docx',
        'orientation_recording_size' => 5,
        'orientation_recording_extension' => 'png jpg jpeg mp4 txt pdf docx',
        'profile_avatar_size' => 2,
        'profile_avatar_width' => 250,
        'profile_avatar_height' => 250,
        'profile_avatar_fallback_url' => 'media/size/250x250.png',
        'profile_avatar_extension' => 'png jpg jpeg',
        'thumbnail_size' => 2,
        'thumbnail_width' => 600,
        'thumbnail_height' => 400,
        'thumbnail_fallback_url' => 'media/size/600x400.png',
        'thumbnail_extension' => 'png jpg jpeg',
    ];
    public const ORIENTATION = [
        'extension_scan_attendance' => 30,
        'point_attendance' => 5,
        'point_rating' => 1,
        'skip_completed_pre_orientation' => 0,
    ];
    public const PORTAL = [
        'link_mobile' => '#',
        'link_times' => '#',
        'link_campus' => '#',
        'link_virtual' => '#',
        'sosmed_youtube' => 'https://www.youtube.com',
        'sosmed_twitter' => 'https://twitter.com',
        'sosmed_instagram' => 'https://www.instagram.com',
        'sosmed_facebook' => 'https://www.facebook.com',
    ];
    public const PREORIENTATION = [
        'point_quiz' => 2,
    ];
    public const SURVEY = [
        'link_post_orientation_survey' => 'https://forms.office.com/Pages/ResponsePage.aspx?id=E-45CidcDEKwr45lxpKQVTtxbPPS2AtCqjeyHy1jajtURTczTFhFODNUSFE5TkVCQjZOMjlDU1VHVS4u',
        'point_post_orientation_survey' => 1,
        'link_pre_orientation_survey' => 'https://forms.office.com/pages/responsepage.aspx?id=E-45CidcDEKwr45lxpKQVTtxbPPS2AtCqjeyHy1jajtUQVNTNEpMUTgzVlZaVlYxNzNWQ0FYMFZBMC4u',
        'point_pre_orientation_survey' => 1,
        'upload_extension' => 'csv xlsx',
        'upload_size' => 5,
    ];
    public const QUICKLINK = [
        'flame' => '.',
        'clubs_n_societies' => '.',
        'student_id' => '.',
        'campus_central' => '.',
        'international_students' => '.',
        'unigym' => '.',
        'missed_orientation' => '.',
    ];


    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $results = [];
        return $results;
    }
}
