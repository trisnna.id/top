<?php

namespace App\Constants;

class SurveyMigrationTypeConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const PRE_ORIENTATION = 1;
    public const POST_ORIENTATION = 2;

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $types = collect([
            self::PRE_ORIENTATION => [
                'id' => self::PRE_ORIENTATION,
                'name' => 'pre',
                'title' => 'Pre Orientation',
            ],
            self::POST_ORIENTATION => [
                'id' => self::POST_ORIENTATION,
                'name' => 'post',
                'title' => 'Post Orientation',
            ],
        ]);
        $results = [];
        switch ($option) {
            case 'name':
            case 'title':
            case 'css_calendar':
            case 'color':
            case 'icon':
                $results = $types->pluck($option, 'id')->toArray();
                break;
            default:
                $results = $types->toArray();
                break;
        }
        return $results;
    }
}
