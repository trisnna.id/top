<?php

namespace App\Constants;

class OrientationStatusConstant
{
    /*
    |--------------------------------------------------------------------------
    | Init constants
    |--------------------------------------------------------------------------
    */

    public const DRAFT = 1;
    public const PENDING_APPROVAL = 2;
    public const REQUIRE_CORRECTION = 3;
    public const APPROVED = 4;
    public const CANCELLED = 5;

    /*
    |--------------------------------------------------------------------------
    | Setup static functions
    |--------------------------------------------------------------------------
    */

    public static function getAvailableOptions($option = null)
    {
        $icons = getConstant('IconConstant');
        $colors = getConstant('ColorConstant');
        $types = collect([
            self::DRAFT => [
                'id' => self::DRAFT,
                'title' => 'Draft',
                'color' => $colors['primary'],
            ],
            self::PENDING_APPROVAL => [
                'id' => self::PENDING_APPROVAL,
                'title' => 'Pending Approval',
                'color' => $colors['warning'],
            ],
            self::REQUIRE_CORRECTION => [
                'id' => self::REQUIRE_CORRECTION,
                'title' => 'Require Correction',
                'color' => $colors['warning'],
            ],
            self::APPROVED => [
                'id' => self::APPROVED,
                'title' => 'Approved',
                'color' => $colors['success'],
                'colors' => [
                    $colors['poppy'], $colors['success'] //publish and unpublish
                ]
            ],
            self::CANCELLED => [
                'id' => self::CANCELLED,
                'title' => 'Cancelled',
                'color' => $colors['danger'],
            ],
        ]);
        $results = [];
        switch ($option) {
            case 'title':
            case 'color':
                $results = $types->pluck($option, 'id')->toArray();
                break;
            default:
                $results = $types->toArray();
                break;
        }
        return $results;
    }
}
