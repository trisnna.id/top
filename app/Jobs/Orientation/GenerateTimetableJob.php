<?php

namespace App\Jobs\Orientation;

use App\Services\OrientationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateTimetableJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $request;
    public $auth;
    public $student;
    /**
     * Create a new job instance.
     */
    public function __construct($request, $auth, $student)
    {
        $this->request = $request;
        $this->auth = $auth;
        $this->student = $student;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        (new OrientationService)->generateTimetable($this->request, $this->auth, $this->student);
    }
}
