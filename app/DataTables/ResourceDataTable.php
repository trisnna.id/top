<?php

namespace App\DataTables;

use App\Models\Resource;
use App\View\Components\Datatables\Status;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ResourceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        $dt = (new EloquentDataTable($query))
            ->setRowId('id')
            ->addIndexColumn()
            ->editColumn('is_active', function (Resource $item) {
                return (new Status($item->is_active))->render();
            })
            ->editColumn('value', function (Resource $item) {
                return view('datatables.admin.resource.video', [
                    'resource' => $item
                ]);
            });
        
        if (auth()->user()->hasRole('admin')) {
            return $dt->editColumn('action', function (Resource $item) {
                $edit = route('admin.resources.edit', $item->id);
                $show = route('admin.resources.show', ['resource' => $item->uuid]);
                return sprintf("
                    <div class=\"btn-group\" role=\"group\">
                        <a href=\"$edit\" class=\"btn btn-icon btn-primary btn-sm\" data-target=\"%s\">
                            <i class=\"fas fa-edit\"></i>
                        </a>
                        <a href=\"$show\" class=\"btn btn-icon btn-success btn-sm\" data-target=\"%s\">
                            <i class=\"fas fa-eye\"></i>
                        </a>
                        <a href=\"#\" class=\"btn btn-icon btn-warning btn-sm\" data-target=\"%s\">
                            <i class=\"fas fa-ban\"></i>
                        </a>
                        <a href=\"#\" class=\"btn btn-icon btn-danger btn-sm\" data-target=\"%s\">
                            <i class=\"fas fa-trash\"></i>
                        </a>
                    </div>
                ", $item->id, $item->id, $item->id, $item->id);
            })->rawColumns(['action', 'is_active']);
        }

        return $dt->editColumn('updated_at',  function (Resource $item) {
            return $item->updated_at->format('d-m-y H:i');
        })->editColumn('action', function (Resource $item) {
            $route = route('resources.show', ['uuid' => $item->uuid]);
            return sprintf("
                <div class=\"btn-group\" role=\"group\">
                    <a href=\"$route\" class=\"btn btn-icon btn-primary btn-sm\" data-target=\"%s\">
                        <i class=\"fas fa-eye\"></i>
                    </a>
                </div>
            ", $item->id, $item->id, $item->id);
        })->rawColumns(['action', 'is_active']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Resource $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Resource $model): QueryBuilder
    {
        return $model->newQuery()->select([
            'id', 'type', 'uuid', 'name', 'description', 'value', 'is_active', 'updated_at'
        ]);
    }

    protected function makeDataScript(array $data)
    {
        $script = '';
        foreach ($data as $key => $value) {
            $script .= PHP_EOL."data.{$key} = {$value};";
        }
        return $script;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        $appendData = $this->makeDataScript([
            'table' => "'resourceTable'",
            'search.value' => "$('input#resourceTable_search').val()",
        ]);

        return $this->builder()
                    ->setTableId('resourceTable')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->postAjax([
                        'url' => '',
                        'data' => "function(data) {{$appendData}}",
                    ])
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->responsive(true)
                    ->searching(true)
                    ->serverSide(true)
                    ->parameters([
                        'drawCallback' => "function () {
                            $('.resourceEdit').click(function () {
                                renderSwalDialog({}, 'Edit Resource')
                            })
                        }"
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        $no = Column::computed('DT_RowIndex', 'No.')->addClass('text-center');
        $name = Column::make('name')->title('Title');
        $description = Column::make('description');
        $type = Column::make('type')->title('Source Type')->addClass('text-center');
        $source = Column::make('value')->title('Source')->addClass('text-center');
        $status = Column::make('is_active')->title('Status')->orderable(false)->addClass('text-center');

        if (auth()->user()->hasRole('admin')) {
            return [
                $no->width('5%'),
                $name->width('15%'),
                $description->width('20%'),
                $type->width('10%'),
                Column::computed('action')->addClass('text-center')->width('5%'),
            ];
        }

        return [
            $no->width('10%'),
            $name->width('30%'),
            Column::make('updated_at')->title('Date Updated')->addClass('text-center')->width('30%'),
            Column::computed('action')->addClass('text-center')->width('30%'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Resource_' . date('YmdHis');
    }
}
