<?php

namespace App\DataTables\Arrival;

use App\Constants\ResourceTypeConstant;
use App\DataTables\Resource\ResourceDt;
use App\Models\Resource;

class ArrivalDt extends ResourceDt
{

    protected $tableId = 'arrivalDt';

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('title', function ($instance) {
                $instance['show'] = [
                    'redirect' => route('arrivals.show', $instance->uuid)
                ];
                return $this->renderColumn('title', compact('instance'));
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['title']);
    }

    public function query()
    {
        return Resource::query()
            ->select([
                'resources.*',
            ])
            ->where('is_active', true)
            ->whereIn('resource_type_id', [ResourceTypeConstant::ALL, ResourceTypeConstant::ARRIVAL]);
    }
}
