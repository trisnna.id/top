<?php

namespace App\DataTables\ClubSociety;

use Illuminate\Http\Request;
use App\Models\ClubSociety;
use App\Models\ClubSocietyCategory;
use App\Vendors\Yajra\BaseDataTable;
use Yajra\DataTables\Html\Column;

class ClubSocietyDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'clubSocietyDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('name', function ($instance) {
                return view('datatables.clubsociety.clubsocietydt.name', compact('instance'))->render();
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    public function query()
    {
        $clubSocietyCategorySubQuery = ClubSocietyCategory::query()
            ->select([
                'club_society_categories.id',
                'club_society_categories.name',
            ]);
        return ClubSociety::select([
            'club_societies.*',
            'club_society_categories.name as club_society_category_name',
        ])->joinSub($clubSocietyCategorySubQuery, 'club_society_categories', function ($query) {
            $query->on('club_societies.club_society_category_id', '=', 'club_society_categories.id');
        });
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'ordering' => false,
                'responsive' => false,
                'searching' => false,
                'lengthMenu' => [12,24,48,96],
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::make('name')->title('')->width('100%'),
        ];
    }
}
