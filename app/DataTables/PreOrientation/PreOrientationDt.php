<?php

namespace App\DataTables\PreOrientation;

use App\Models\ClubSociety;
use App\Models\ClubSocietyCategory;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Yajra\DataTables\CollectionDataTable;
use Yajra\DataTables\Html\Column;

class PreOrientationDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'preOrientationDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable()
    {
        return (new CollectionDataTable($this->collection()))
            ->editColumn('name', function ($instance) {
                $instance['detail'] = [
                    'data' => [
                        'id' => $instance->get('id'),
                        'name' => $instance->get('name'),
                    ],
                    'swal' => [
                        'id' => "detail",
                        'axios' => [
                            'url' => '#',
                        ]
                    ]
                ];
                return view(columnDataTablesView('name', get_class()), compact('instance'))->render();
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    public function collection()
    {
        $checkpoint = $this->httpRequest->get('checkpoint');

        if (!(bool) $checkpoint) {
            return collect();
        }

        $data = ClubSociety::proto(5);
        return $data->filter(function ($item) use ($checkpoint) {
            return Str::contains($checkpoint, $item->checkpoint);
        });
    }

    public function query()
    {
        $clubSocietyCategorySubQuery = ClubSocietyCategory::query()
            ->select([
                'club_society_categories.id',
                'club_society_categories.name',
            ]);
        return ClubSociety::select([
            'club_societies.*',
            'club_society_categories.name as club_society_category_name',
        ])->joinSub($clubSocietyCategorySubQuery, 'club_society_categories', function ($query) {
            $query->on('club_societies.club_society_category_id', '=', 'club_society_categories.id');
        });
    }

    protected function getHtmlData()
    {
        return [
            'checkpoint' => "localStorage.getItem('checkpoint')",
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'ordering' => false,
                'responsive' => false,
                'searching' => false,
                'lengthMenu' => [12,24,48,96],
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                'drawCallback' => "function (settings) {
                    dtx.drawCallback(settings,'{$this->tableId}');
                }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::make('name')->title('')->width('100%'),
        ];
    }
}
