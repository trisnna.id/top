<?php

namespace App\DataTables\Profile\Notification;

use App\Models\Mail;
use App\Models\Notification;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Column;

class NotificationDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'notificationDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        $mails = Mail::all();
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('data', function ($instance) use ($mails) {
                $notification = new $instance->type();
                $template = $notification->template ?? null;
                if (!empty($template ?? null)) {
                    $notificationTemplate = $mails->where('name', $template)->first()->notification ?? null;
                    if (!empty($notificationTemplate ?? null)) {
                        $content = $this->parse(trim($notificationTemplate), $instance->data);
                        return $this->renderColumn('data', compact('instance','content'));
                    }
                }
            })
            ->editColumn('created_at', function ($instance) {
                return $instance->created_at_readable;
            })
            ->editColumn('action', function ($instance) {
                $url = ($instance->data['link'] ?? ($instance->data['orientationUrl'] ?? '#'));
                $instance['show'] = [
                    'redirect' => $url
                ];
                return $this->renderColumn('action', compact('instance'));
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['data','action']);
    }

    public function parse($stub, $data)
    {
        $replace = $this->replacements($data);
        return str_replace(array_keys($replace), array_values($replace), $stub);
    }

    public function replacements($data)
    {
        $replace = [];
        foreach ($data as $key => $value) {
            $replace['{{ ' . $key . ' }}'] = $value;
            $replace['{{' . $key . '}}'] = $value;
        }
        return $replace;
    }

    public function query()
    {
        $auth = $this->auth;
        return Notification::query()
            ->select(['notifications.*'])
            ->where([
                'notifiable_type' => $auth::class,
                'notifiable_id' => $auth->id,
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('created_at')->title('Datetime'),
            Column::computed('data')->title('Notification'),
            Column::computed('action')->addClass('text-center')->exportable(false),
        ];
    }
}
