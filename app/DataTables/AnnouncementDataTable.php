<?php

namespace App\DataTables;

use App\Contracts\Entities\AnnouncementEntity;
use App\Models\Campus;
use App\Models\Intake;
use App\Models\Announcement;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\View\Components\Badges\Localities;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Arr;

class AnnouncementDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        $dataTable = (new EloquentDataTable($query))
            ->editColumn('checkbox', function ($item) {
                // return "<input type='checkbox' class='announcement-checkbox' name='bulkSelect' data-id='{$item->id}'>";
                return '
                    <label class="form-check form-check-sm form-check-custom form-check-solid form-checkbox-label">
                        <input class="form-check-input form-checkbox-input checkbox-input-single announcement-checkbox" type="checkbox" name="bulkSelect" data-id="' . $item->id . '">
                    </label>
                ';
            })
            ->addIndexColumn()
            ->setRowId('id')
            ->editColumn(Announcement::FIELD_INTAKES, function (Announcement $item) {
                $item = $item->intakes['original'];
                $intakes = Intake::where('is_active', 1)->get()->count();

                if (count($item) === $intakes) {
                    return __('All');
                } else {
                    $intakeNames = Intake::whereIn('id', $item)->pluck('name')->toArray();
                    return implode(', ', $intakeNames);
                }
            })
            ->editColumn(Announcement::FIELD_LOCALITIES, function (Announcement $item) {
                return (new Localities($item->localities['original']))->render();
            })
            ->editColumn(Announcement::FIELD_CAMPUSES, function (Announcement $item) {
                $item = $item->campuses['original'];
                $campuses = Campus::where('is_active', 1)->get()->count();
                $campusNames = [];

                if (count($item) === $campuses) {
                    return __('All');
                } else {
                    $campusNames = Campus::whereIn('id', $item)->pluck('name');
                }

                $text = '';
                foreach ($campusNames as $campusName) {
                    $text .= sprintf('<span class="badge badge-sm px-4 badge-danger">%s</span>', $campusName);
                }
                return $text;
            })
            ->editColumn(Announcement::FIELD_STATUS, function (Announcement $item) {
                return ucfirst($item->status);
            })
            ->editColumn('action', function ($item) {
                return view('admin.announcement.action', [
                    Announcement::FIELD_UUID => $item->uuid,
                    'isPublished' => $item->status === Announcement::STATUS_PUBLISHED,
                    'isUnpublished' => $item->status === Announcement::STATUS_UNPUBLISHED,
                ]);
            })
            ->rawColumns(['checkbox', Announcement::FIELD_CATEGORY, Announcement::FIELD_INTAKES, Announcement::FIELD_LOCALITIES, Announcement::FIELD_CAMPUSES, 'action']);
        
        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Announcement $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Announcement $model): QueryBuilder
    {
        return $model->dataTable()
            ->when(request()->has('intake') && request()->get('intake') !== 'null', function ($q) {
                $intake = request()->get('intake');
                $intake = explode(',', $intake);
                $q->where(function ($subquery) use ($intake) {
                    foreach ($intake as $c) {
                        if (!in_array($c, [null, '', 'null'])) {
                            $subquery->orWhereJsonContains(Announcement::FIELD_INTAKES, $c);
                        }
                    }
                });
            })
            ->when(request()->has('programme') && request()->get('programme') !== 'null', function ($q) {
                $programme = request()->get('programme');
                $programme = explode(',', $programme);
                $q->where(function ($subquery) use ($programme) {
                    foreach ($programme as $c) {
                        if (!in_array($c, [null, '', 'null'])) {
                            $subquery->orWhereJsonContains(Announcement::FIELD_PROGRAMMES, $c);
                        }
                    }
                });
            })
            ->when(request()->has('nationality') && request()->get('nationality') !== 'null', function ($q) {
                $locality = request()->get('nationality');
                $locality = explode(',', $locality);
                $q->where(function ($subquery) use ($locality) {
                    foreach ($locality as $c) {
                        if (!in_array($c, [null, '', 'null'])) {
                            $subquery->orWhereJsonContains(Announcement::FIELD_LOCALITIES, $c);
                        }
                    }
                });
            })
            ->when(request()->has('campus') && request()->get('campus') !== 'null', function ($q) {
                $campus = request()->get('campus');
                $campus = explode(',', $campus);
                $q->where(function ($subquery) use ($campus) {
                    foreach ($campus as $c) {
                        if (!in_array($c, [null, '', 'null'])) {
                            $subquery->orWhereJsonContains(Announcement::FIELD_CAMPUSES, $c);
                        }
                    }
                });
            })
            ->when(request()->has('status') && request()->get('status') !== 'null', function ($q) {
                $status = request()->get('status');
                $status = explode(',', $status);
                $q->whereIn(Announcement::FIELD_STATUS, $status);
            });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('announcementTable')
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
            ])
            ->orderBy(1)
            ->orderBy(2, 'asc')
            ->parameters([
                'searchDelay' => 1000,
            ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        $columns =  [
            // Checkbox
            // Column::computed('checkbox', '')->exportable(false)->printable(false)->width('1%')->addClass('text-center')->title('<input type="checkbox" id="selectAll" name="selectAll">')->titleAttr('Select All'),
            Column::computed('checkbox', '')->exportable(false)->printable(false)->width('1%')->addClass('text-center')->title('
                <label class="form-check form-check-sm form-check-custom form-check-solid form-checkbox-label">
                    <input class="form-check-input form-checkbox-input checkbox-input-all" id="selectAll" name="selectAll" type="checkbox">
                </label>
            ')->titleAttr('Select All'),
            Column::computed('DT_RowIndex', 'No.')->width('5%')->addClass('text-center'),
            Column::make(Announcement::FIELD_SEQUENCE)->title(__('description.labels.sequence'))->orderable(),
            Column::make(Announcement::FIELD_NAME)->title(__('description.labels.title')),
            Column::make(Announcement::FIELD_INTAKES)->title('Intakes'),
            Column::make(Announcement::FIELD_LOCALITIES)->title('Nationality'),
            Column::make(Announcement::FIELD_CAMPUSES)->title('Campus'),
            Column::make(Announcement::FIELD_STATUS)->title('Status'),
            Column::computed('action')->addClass('text-center')->exportable(false)->printable(false)
        ];

        
        if (request()->has('action') && request()->get('action') === 'pdf') {
            $columns = array_slice($columns, 0, 7);
        }

        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Announcement_' . date('YmdHis');
    }
}
