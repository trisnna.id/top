<?php

namespace App\DataTables\Faq;

use App\Models\Faq;
use App\Models\FaqFilter;
use App\Models\Student;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class FaqDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'faqDt';
    protected $auth;
    protected $tableClass = 'fs-7 thead-none w-100';

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('question', function ($instance) {
                return $this->renderColumn('question', compact('instance'));
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['question']);
    }

    public function query()
    {
        $request = $this->httpRequest;
        $student = $this->auth->student;
        if (empty($student ?? null)) {
            $student = Student::findOrFailByUuid($request->student_id);
        }
        $faqFilterSubQueries['intake'] = FaqFilter::query()
            ->selectRaw(DB::raw('COUNT(faq_filters.id)'))
            ->whereRaw(DB::raw('faq_filters.faq_id = faqs.id'))
            ->where('faq_filters.name', 'intake')->where(function ($query) use ($student) {
                $query->whereJsonContains('value', (string) $student->intake_id);
            });
        $faqFilterSubQueries['programme'] = FaqFilter::query()
            ->selectRaw(DB::raw('COUNT(faq_filters.id)'))
            ->whereRaw(DB::raw('faq_filters.faq_id = faqs.id'))
            ->where('faq_filters.name', 'programme')->where(function ($query) use ($student) {
                $query->whereJsonContains('value', (string) $student->programme_id);
            });
        $faqFilterSubQueries['campus'] = FaqFilter::query()
            ->selectRaw(DB::raw('COUNT(faq_filters.id)'))
            ->whereRaw(DB::raw('faq_filters.faq_id = faqs.id'))
            ->where('faq_filters.name', 'campus')->where(function ($query) use ($student) {
                $query->whereJsonContains('value', (string) $student->campus_id);
            });
        return Faq::query()
            ->select(['faqs.*'])
            ->where(function ($query) use ($faqFilterSubQueries) {
                foreach ($faqFilterSubQueries as $faqFilterSubQuery) {
                    $query->where($faqFilterSubQuery, '>', 0);
                }
            })
            ->when(!empty($request->defaultFaqDtEmpty ?? null), function ($query) {
                $query->where('id', -1);
            })
            ->where([
                'is_active' => true
            ]);
    }

    protected function getParameters()
    {
        return [
            "ordering" => false,
            "paging" => false,
            'responsive' => false,
            'searching' => true,
            'bInfo' => false,
            "language" => [
                "emptyTable" => "No FAQ available"
            ],
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::make('question'),
        ];
    }
}
