<?php

namespace App\DataTables\Survey;

use Illuminate\Http\Request;
use App\Models\Attendance;
use App\Models\Student;
use App\Models\Activity;
use App\Models\Survey;
use App\Models\SurveyAnswer;
use App\Models\SurveyAnswerStatus;
use App\Vendors\Yajra\BaseDataTable;
use Yajra\DataTables\Html\Column;

class SurveyAnswerDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'surveyAnswerDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('attendance_at', function ($instance) {
                return !empty($instance->attendance_at ?? null) ? $instance->attendance_at->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    public function query()
    {

        $q1SubQuery = Survey::query()
            ->select([
                'surveys.id',
                'surveys.name',
            ]);
        $q2SubQuery = SurveyAnswerStatus::query()
            ->select([
                'survey_answer_statuses.id',
                'survey_answer_statuses.title',
            ]);
        return SurveyAnswer::select([
            'survey_answers.*',
            'q1SubQuery.name as survey_name',
            'q2SubQuery.title as survey_answer_status_title',
        ])
            ->joinSub($q1SubQuery, 'q1SubQuery', function ($query) {
                $query->on('survey_answers.survey_id', '=', 'q1SubQuery.id');
            })
            ->joinSub($q2SubQuery, 'q2SubQuery', function ($query) {
                $query->on('survey_answers.survey_answer_status_id', '=', 'q2SubQuery.id');
            })
            ->where('survey_answers.student_id', $this->auth->student->id ?? 0);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', '')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('survey_name'),
            Column::make('survey_answer_status_title')->title('status'),
            Column::computed('actions')->addClass('text-center'),
        ];
    }
}
