<?php

namespace App\DataTables\Orientation;

use App\Constants\OrientationTypeConstant;
use App\DataTables\Orientation\CompulsoryCoreDt;
use Carbon\Carbon;

class ComplementaryDt extends CompulsoryCoreDt
{

    protected $tableId = 'complementaryDt';

    public function query()
    {
        $request = $this->httpRequest;
        $auth = $this->auth;
        return (new TimelineDt($request, $auth))->query()
            ->where('orientations.orientation_type_id', OrientationTypeConstant::COMPLEMENTARY)
            ->whereDate('start_date','<=', Carbon::today())
            ->whereDate('end_date','>=', Carbon::today());
    }
}
