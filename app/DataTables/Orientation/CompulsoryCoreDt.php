<?php

namespace App\DataTables\Orientation;

use App\Constants\OrientationTypeConstant;
use App\Traits\Services\BaseServiceTrait;
use App\Vendors\Yajra\BaseDataTable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Column;

class CompulsoryCoreDt extends BaseDataTable
{
    use BaseServiceTrait;

    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'compulsoryCoreDt';
    protected $auth;
    protected $tableClass = 'table thead-none table-bordered table-striped align-middle table-row-dashed fs-6 gy-5 th-text-uppercase th-fw-bold w-100';

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        $student = $this->auth->student;
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('name', function ($instance) use ($student) {
                $instance['orientation'] = [
                    'swal' => [
                        'id' => 'orientation',
                    ],
                    'data' => $this->dataSwalOrientation($instance, $student)
                ];
                $instance['attendance'] = [
                    'swal' => [
                        'id' => 'attendance',
                        'settings' => [
                            'title' => "Scan Attendance",
                            'showConfirmButton' => false,
                        ],
                        'axios' => [
                            'url' => route('orientations.upsert-attendance'),
                            'method' => 'put'
                        ]
                    ],
                    'data' => [
                        'id' => $instance['uuid'],
                        'student_id' => $this->auth->student->uuid ?? null,
                        'name' => $instance['name'],
                        'synopsis' => $instance['synopsis'],
                        'livestream' => $instance['livestream'],
                        'presenter' => $instance['presenter'],
                        'agenda' => $instance['agenda'],
                        'rsvp' => $instance['rsvp'],
                        'attendance' => $instance->orientation_attendance_updated_at,
                        'rule_can_scan_attendance' => $instance->rule_can_scan_attendance,
                        'event' => $instance->venue . '<br>' . $instance->start_date . ", {$instance->start_end_time_readable}",
                    ]
                ];

                if ($instance->rule_can_rsvp && empty($instance->orientation_attendance_updated_at ?? null)) {
                    if (is_null($instance->orientation_rsvp_is_go) || !is_null($instance->orientation_rsvp_is_go)) {
                        $instance['rsvp'] = [
                            'swal' => [
                                'id' => 'rsvp',
                                'settings' => [
                                    'title' => "RSVP",
                                    'confirmButtonText' => 'Confirm',
                                ],
                                'axios' => [
                                    'url' => route('orientations.upsert-rsvp', $instance->uuid),
                                    'method' => 'put'
                                ]
                            ],
                            'data' => [
                                'id' => $instance['uuid'],
                                'orientation_rsvp_is_go' => $instance['orientation_rsvp_is_go'],
                            ]
                        ];
                    }
                }
                return $this->renderColumn('name', compact('instance'), 'datatables.orientation.compulsorycoredt');
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    public function query()
    {
        $request = $this->httpRequest;
        $auth = $this->auth;
        return (new TimelineDt($request, $auth))->query()
            ->where('orientations.orientation_type_id', OrientationTypeConstant::COMPULSORY_CORE)
            ->whereDate('start_date', Carbon::today());
    }

    protected function getParameters()
    {
        return [
            "ordering" => false,
            'responsive' => false,
            'searching' => false,
            'paging' => false,
            'bInfo' => false,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::make('name'),
        ];
    }
}
