<?php

namespace App\DataTables\Orientation;

use App\Models\Orientation;
use App\Models\OrientationAttendance;
use App\Models\OrientationFilter;
use App\Models\OrientationRsvp;
use App\Models\Student;
use App\Traits\Services\BaseServiceTrait;
use App\Vendors\Yajra\BaseDataTable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class TimelineDt extends BaseDataTable
{
    use BaseServiceTrait;

    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'timelineDt';
    protected $auth;
    protected $tableClass = 'thead-none table-timeline';
    protected $student;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
        $student = $auth->student ?? null;
        if (empty($student ?? null)) {
            $student = Student::findOrFailByUuid($request->student_id ?? $request->get('student_id'));
        }
        $this->student = $student;
    }

    public function dataTable($query)
    {
        $orientationTypeConstants = getConstant('OrientationTypeConstant');
        $student = $this->student;
        $weight = [];
        return datatables()
            ->of($query)
            ->editColumn('name', function ($instance) use ($orientationTypeConstants, $student) {
                $instance->icon =  $orientationTypeConstants[$instance->orientation_type_id];
                $instance->orientationTypeConstant =  $orientationTypeConstants[$instance->orientation_type_id];
                $instance['orientation'] = [
                    'swal' => [
                        'id' => 'orientation',
                        'settings' => [
                            'title' => "{$instance->orientation_type_constant['title']} Activity",
                        ],
                    ],
                    'data' => $this->dataSwalOrientation($instance, $student)
                ];
                if (!is_null($instance->orientation_attendance_updated_at) && empty($instance->orientation_attendance_rating)) {
                    $instance['rate'] = [
                        'swal' => [
                            'id' => 'rate',
                            'settings' => [
                                'title' => "Please Rate",
                                'showCancelButton' => false,
                                'showConfirmButton' => true,
                                'confirmButtonText' => 'Submit',
                            ],
                            'axios' => [
                                'url' => route('orientations.update-rate', $instance['uuid']),
                                'method' => 'put'
                            ]
                        ],
                        'data' => [
                            'id' => $instance['uuid'],
                            'name' => $instance['name'],
                        ]
                    ];
                }
                $attachments = collect();
                foreach ($instance->getMedia('recordings') as $file) {
                    $attachment = collect([
                        'type' => 'file',
                        'name' => $file->file_name,
                        'updated_at' => $file->updated_at,
                        'url' => $file->original_url,
                        'instance' => $file,
                        'media_type' => extensionTypeMedia($file->extension)
                    ]);
                    $attachments = $attachments->push($attachment);
                }
                foreach ($instance->orientationLinks->where('collection_name', 'recordings') as $link) {
                    $attachment = collect([
                        'type' => 'link',
                        'name' => $link->name,
                        'updated_at' => $link->updated_at,
                        'url' => $link->url,
                        'instance' => $link,
                        'media_type' => urlTypeMedia($link->url)
                    ]);
                    $attachments = $attachments->push($attachment);
                }
                if (count($attachments)) {
                    $attachments->sortBy('name');
                    $firstVideoAttachment = $attachments->filter(function ($attachment) {
                        return in_array($attachment['media_type'], ['youtube', 'video']);
                    })->first();
                    $instance->recordings = $attachments;
                    $instance->first_video_attachment = $firstVideoAttachment;
                    $instance['video'] = [
                        'swal' => [
                            'id' => 'video',
                            'settings' => [
                                'title' => 'Recording Video',
                                'showCancelButton' => false,
                                'showConfirmButton' => false,
                            ],
                        ],
                        'data' => [
                            'id' => $instance['uuid'],
                            'name' => $instance['name'],
                            'video_name' => $firstVideoAttachment['name'] ?? null,
                            'video_url' => $firstVideoAttachment['url'] ?? null,
                            'video_media_type' => $firstVideoAttachment['media_type'] ?? null,
                            'video_type' => $firstVideoAttachment['type'] ?? null,
                        ]
                    ];
                }
                if ($instance->rule_can_rsvp && empty($instance->orientation_attendance_updated_at ?? null)) {
                    if (is_null($instance->orientation_rsvp_is_go) || !is_null($instance->orientation_rsvp_is_go)) {
                        $instance['rsvp'] = [
                            'swal' => [
                                'id' => 'rsvp',
                                'settings' => [
                                    'title' => "RSVP",
                                    'confirmButtonText' => 'Confirm',
                                ],
                                'axios' => [
                                    'url' => route('orientations.upsert-rsvp', $instance->uuid),
                                    'method' => 'put'
                                ]
                            ],
                            'data' => [
                                'id' => $instance['uuid'],
                                'orientation_rsvp_is_go' => $instance['orientation_rsvp_is_go'],
                            ]
                        ];
                    }
                }
                return $this->renderColumn('name', compact('instance'));
            })
            ->setRowAttr([
                'data-weight' => function ($instance) {
                    return $instance->weight_timeline;
                },
                'data-title' => function ($instance) {
                    return $instance->name;
                },
                'data-date-day' => function ($instance) {
                    // return $instance->start_date_j;
                    return '';
                },
                'data-date-month' => function ($instance) {
                    return $instance->start_time_readable;
                },
                'data-same-date' => function ($instance) {
                    return $instance->start_date == $instance->end_date;
                },
                'data-end-date-day' => function ($instance) {
                    return $instance->end_date_j;
                },
                'data-end-date-month' => function ($instance) {
                    return $instance->end_date_M;
                },
                'data-color' => function ($instance) {
                    return $instance->orientation_type_constant['color']['class'];
                },

                'data-weight-name' => function ($instance) use (&$weight) {
                    if ($instance->weight_timeline == 1) {
                        $name = 'Today';
                    } elseif ($instance->weight_timeline == 2) {
                        $name = 'Tomorrow';
                    } else {
                        $name = $instance->start_date_dFY;
                    }
                    array_push($weight, $name);
                    return $name;
                },
            ])
            ->setRowClass('weight')
            ->only(array_column($this->getColumns(), 'data'))
            ->with('weight', $weight)
            ->rawColumns(['name']);
    }

    public function query()
    {
        $request = $this->httpRequest;
        $filter = $request->filter ?? [];
        $student = $this->student;
        $orientationFilterSubQueries['intake'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'intake')->where(function ($query) use ($student) {
                $query->whereJsonContains('value', 'all')->orWhereJsonContains('value', (string) $student->intake_id);
            });
        $orientationFilterSubQueries['faculty'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'faculty')->where(function ($query) use ($student) {
                $query->whereJsonContains('value', 'all')->orWhereJsonContains('value', (string) $student->faculty_id);
            });
        $orientationFilterSubQueries['school'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'school')->where(function ($query) use ($student) {
                $query->whereJsonContains('value', 'all')->orWhereJsonContains('value', (string) $student->school_id);
            });
        $orientationFilterSubQueries['programme'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'programme')->where(function ($query) use ($student) {
                $query->whereJsonContains('value', 'all')->orWhereJsonContains('value', (string) $student->programme_id);
            });
        $orientationFilterSubQueries['level_study'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'level_study')->where(function ($query) use ($student) {
                $query->whereJsonContains('value', 'all')->orWhereJsonContains('value', (string) $student->level_study_id);
            });
        $orientationFilterSubQueries['mobility'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'mobility')->where(function ($query) use ($student) {
                $query->whereJsonContains('value', 'all')->orWhereJsonContains('value', (string) $student->mobility_id);
            });
        $orientationRsvpSubQuery = OrientationRsvp::query()
            ->select([
                'orientation_rsvps.id',
                'orientation_rsvps.orientation_id',
                'orientation_rsvps.student_id',
                'orientation_rsvps.is_go',
            ])
            ->where('orientation_rsvps.student_id', $student->id);
        $orientationAttendanceSubQuery = OrientationAttendance::query()
            ->select([
                'orientation_attendances.id',
                'orientation_attendances.orientation_id',
                'orientation_attendances.student_id',
                'orientation_attendances.rating',
                'orientation_attendances.point',
                'orientation_attendances.updated_at',
            ])
            ->where('orientation_attendances.student_id', $student->id);
        $today = Carbon::today()->format('Y-m-d');
        $tomorrow = Carbon::tomorrow()->format('Y-m-d');
        return Orientation::select([
            'orientations.*',
            'orientation_rsvps.is_go as orientation_rsvp_is_go',
            'orientation_attendances.updated_at as orientation_attendance_updated_at',
            'orientation_attendances.rating as orientation_attendance_rating',
            'orientation_attendances.point as orientation_attendance_point',
            DB::raw("CASE WHEN start_date <= '{$today}' AND end_date >= '{$today}' THEN 1 WHEN start_date = '{$tomorrow}' THEN 2 ELSE 3 END AS weight_timeline")
        ])
            ->where(function ($query) use ($orientationFilterSubQueries) {
                foreach ($orientationFilterSubQueries as $orientationFilterSubQuery) {
                    $query->where($orientationFilterSubQuery, '>', 0);
                }
            })
            ->leftJoinSub($orientationRsvpSubQuery, 'orientation_rsvps', function ($query) {
                $query->on('orientations.id', '=', 'orientation_rsvps.orientation_id');
            })
            ->leftJoinSub($orientationAttendanceSubQuery, 'orientation_attendances', function ($query) {
                $query->on('orientations.id', '=', 'orientation_attendances.orientation_id');
            })
            ->when(!empty($filter['orientation_type'] ?? null), function ($query) use ($filter) {
                $query->where('orientation_type_id', $filter['orientation_type']);
            })
            ->when(!is_null($filter['is_rsvp'] ?? null), function ($query) use ($filter) {
                $query->where('is_rsvp', $filter['is_rsvp']);
            })
            ->when(!empty($request->start_date ?? null) && !empty($request->end_date ?? null), function ($query) use ($request) {
                $query->whereBetween('start_date', [Carbon::parse($request->start_date), Carbon::parse($request->end_date)]);
            })
            ->where('is_publish', true)
            ->orderBy('weight_timeline', 'asc')
            ->orderBy('start_date', 'asc')
            ->orderBy('start_time', 'asc');
    }

    protected function getParameters()
    {
        return [
            'ordering' => false,
            'responsive' => false,
            'searching' => true,
            'paging' => false,
            'bInfo' => false,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}');$('table#timelineDt tbody').prepend('<tr></tr>');KTMenu.createInstances();modules.orientation.index.timeline(settings,'{$this->tableId}') }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::make('name'),
        ];
    }
}
