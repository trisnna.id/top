<?php

namespace App\DataTables\Orientation;

use Yajra\DataTables\Html\Column;
use App\DataTables\Admin\Orientation\Show\OrientationRecordingDt as AdminOrientationRecordingDt;

class OrientationRecordingDt extends AdminOrientationRecordingDt
{

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'orientation_id' => "$('form#video input#orientation_id').val()",
        ];
    }

    protected function getColumns(): array
    {
        return [
            Column::make('name'),
        ];
    }
}
