<?php

namespace App\DataTables\Orientation;

use Illuminate\Http\Request;
use Yajra\DataTables\Html\Column;

class OrientationDt extends TimelineDt
{

    public function __construct(Request $request, $auth = null)
    {
        parent::__construct($request, $auth);
        $this->tableId = 'orientationDt';
        $this->tableClass = 'fs-7 thead-none w-100 table tbody-row tr-col-md-4 td-d-block tr-mb-3 td-p-0 td-h-100';
    }

    public function dataTable($query)
    {
        $student = $this->auth->student;
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('name', function ($instance) use ($student) {
                $instance['orientation'] = [
                    'swal' => [
                        'id' => 'orientation',
                    ],
                    'data' => $this->dataSwalOrientation($instance, $student)
                ];
                return $this->renderColumn('name', compact('instance'));
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'filter' => "{}",
            'filter.orientation_type' => "$('#{$this->tableId}_filter_orientation_type').val()",
        ];
    }

    protected function getParameters()
    {
        return [
            'ordering' => false,
            'lengthMenu' => [
                [3, 12, 24, 48, -1],
                [3, 12, 24, 48, 'All'],
            ],
            "dom"=>"<tr><'row'<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'li><'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>>",
            "pageLength" => 12,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::make('name'),
        ];
    }
}
