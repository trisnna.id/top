<?php

namespace App\DataTables\Admin\Resource\Edit;

use App\Models\Orientation;
use App\Models\Resource;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Yajra\DataTables\CollectionDataTable;
use Yajra\DataTables\Html\Column;

class AttachmentDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'attachmentDt';
    protected $auth;
    protected $resource;
    protected $tableClass = 'table thead-none table-bordered table-striped align-middle table-row-dashed fs-7 gy-5 th-text-uppercase th-fw-bold w-100';

    public function __construct(Request $request, $auth = null, Resource $resource)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
        $this->resource = $resource;
    }

    public function dataTable(): CollectionDataTable
    {
        return (new CollectionDataTable($this->collection()))
            ->setRowId('id')
            ->editColumn('name', function ($instance) {
                if ($instance['type'] == 'file') {
                    $instance = $instance['instance'];
                    return $this->renderColumn('filename', compact('instance'), 'components.datatables');
                } elseif ($instance['type'] == 'link') {
                    $instance = $instance['instance'];
                    return $this->renderColumn('link', compact('instance'), 'components.datatables');
                }
            })
            ->editColumn('updated_at', function ($instance) {
                return !empty($instance['updated_at'] ?? null) ? $instance['updated_at']->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
            })
            ->addColumn('action', function ($instance) {
                if ($instance['type'] == 'file') {
                    $file = $instance['instance'];
                    $instance['upsert'] = [
                        'data' => [
                            'id' => $file->uuid,
                            'name' => $file->getCustomProperty('name') ?? $file->file_name,
                            'description' => $file->getCustomProperty('description') ?? '',
                        ],
                        'swal' => [
                            'id' => "upsertFile",
                            'axios' => [
                                'url' => route('admin.resources.update-file', [$this->resource->uuid, $file->uuid]),
                                'method' => 'put'
                            ]
                        ]
                    ];
                    $instance['delete'] = [
                        'swal' => [
                            'id' => "swal-blank",
                            'settings' => [
                                'icon' => 'warning',
                                'title' => 'Are you sure to DELETE?',
                                'html' => "You won't be able to revert this!",
                                'confirmButtonText' => 'Confirm',
                            ],
                            'axios' => [
                                'url' => route(
                                    'admin.resources.destroy-file',
                                    [$this->resource->uuid, $file->uuid]
                                ),
                                'method' => 'delete'
                            ]
                        ]
                    ];
                } elseif ($instance['type'] == 'link') {
                    $link = $instance['instance'];
                    $instance['upsert'] = [
                        'data' => [
                            'id' => $link->uuid,
                            'name' => $link->name,
                            'url' => $link->url,
                            'description' => $link->description,
                        ],
                        'swal' => [
                            'id' => "upsertLink",
                            'axios' => [
                                'url' => route('admin.resources.update-link', [$this->resource->uuid, $link->uuid]),
                                'method' => 'put'
                            ]
                        ]
                    ];
                    $instance['delete'] = [
                        'swal' => [
                            'id' => "swal-blank",
                            'settings' => [
                                'icon' => 'warning',
                                'title' => 'Are you sure to DELETE?',
                                'html' => "You won't be able to revert this!",
                                'confirmButtonText' => 'Confirm',
                            ],
                            'axios' => [
                                'url' => route('admin.resources.destroy-link', [$this->resource->uuid, $link->uuid]),
                                'method' => 'delete'
                            ]
                        ]
                    ];
                }
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            })
            ->rawColumns(['name', 'action'])
            ->only(array_column($this->getColumns(), 'data'));
    }

    public function collection()
    {
        $attachments = collect();
        $resource = $this->resource;
        foreach ($resource->getMedia('media') as $file) {
            $attachment = collect([
                'type' => 'file',
                'name' => $file->file_name,
                'source' => 'media',
                'updated_at' => $file->updated_at,
                'url' => $file->original_url,
                'instance' => $file
            ]);
            $attachments = $attachments->push($attachment);
        }
        foreach ($resource->getMedia('documents') as $file) {
            $attachment = collect([
                'type' => 'file',
                'name' => $file->file_name,
                'source' => 'documents',
                'updated_at' => $file->updated_at,
                'url' => $file->original_url,
                'instance' => $file
            ]);
            $attachments = $attachments->push($attachment);
        }
        foreach ($resource->resourceLinks->where('collection_name', 'media') as $link) {
            $attachment = collect([
                'type' => 'link',
                'name' => $link->name,
                'source' => 'media',
                'updated_at' => $link->updated_at,
                'url' => $link->url,
                'instance' => $link
            ]);
            $attachments = $attachments->push($attachment);
        }
        foreach ($resource->resourceLinks->where('collection_name', 'links') as $link) {
            $attachment = collect([
                'type' => 'link',
                'name' => $link->name,
                'source' => 'links',
                'updated_at' => $link->updated_at,
                'url' => $link->url,
                'instance' => $link
            ]);
            $attachments = $attachments->push($attachment);
        }
        return $attachments->sortBy('title');
    }

    protected function getParameters()
    {
        return [
            'responsive' => false,
            'searching' => true,
            'paging' => false,
            'bInfo' => false,
            'ordering' => false,
            "language" => [
                "emptyTable" => "No data files & links available"
            ],
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns(): array
    {
        return [
            Column::make('name'),
            Column::make('updated_at')->title('Last Modified'),
            Column::make('source')->title('Source'),
            Column::computed('action')->title('Action')->addClass('text-center px-4'),
        ];
    }
}
