<?php

namespace App\DataTables\Admin\Resource;

use App\Constants\ResourceTypeConstant;
use App\Models\Resource;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class ResourceDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'resourceDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->addColumn('control', function ($instance) {
                $request = $this->httpRequest;
                $checkboxId = $this->checkboxId;
                return $this->renderColumnControl($request, $instance, $checkboxId);
            })
            ->editColumn('active', function ($instance) {
                $url = route('admin.resources.update-active', $instance->uuid);
                return $this->renderColumnIsActive($instance, 'is_active', $url);
            })
            ->filterColumn('active', function ($query, $keyword) {
                $query->whereRaw("IF(resources.is_active = 1, 'active', 'inactive') LIKE '%{$keyword}%'");
            })
            ->addColumn('action', function ($instance) {
                $instance['upsert'] = [
                    'redirect' => route('admin.resources.edit', $instance->uuid)
                ];
                $instance['show'] = [
                    'redirect' => route('admin.resources.show', $instance->uuid)
                ];
                if ($instance->rule_can_delete) {
                    $instance['delete'] = [
                        'swal' => [
                            'id' => "swal-blank",
                            'settings' => [
                                'icon' => 'warning',
                                'title' => 'Are you sure to DELETE?',
                                'html' => "You won't be able to revert this!",
                                'confirmButtonText' => 'Confirm',
                            ],
                            'axios' => [
                                'url' => route('admin.resources.destroy', $instance->uuid),
                                'method' => 'delete'
                            ]
                        ]
                    ];
                }
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['control', 'active', 'action']);
    }

    public function query()
    {
        return Resource::query()
            ->select([
                'resources.*',
            ])
            ->addSelect(DB::raw("IF(resources.is_active = 1, 'active', 'inactive')  AS `active`"))
            ->whereIn('resource_type_id', [ResourceTypeConstant::ALL, ResourceTypeConstant::RESOURCE]);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            $this->checkboxId => "$('input#{$this->checkboxId}').val()",
        ];
    }

    protected function getParameters()
    {
        return [
            'order' => [
                2, 'asc'
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', $this->renderCheckboxAll($this->checkboxId))->titleAttr('')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('title')->title('Title'),
            Column::make('active')->title('Status'),
            Column::computed('action')->addClass('text-center'),
        ];
    }
}
