<?php

namespace App\DataTables\Admin\Quiz;

use App\Models\Activity;
use App\Models\Attendance;
use App\Models\Survey;
use App\Models\SurveyAnswer;
use App\Models\Student;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class QuizDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'quizDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()


            ->editColumn('action', function ($item) {
                $instance['upsert'] = [
                    'data' => $item,
                    'swal' => [
                        'id' => "upsert",
                    ]
                ];
                $instance['show'] = [
                    'data' => $item,
                    'swal' => [
                        'id' => "swal-blank",
                        'settings' => [
                            'title' => $item['title'],
                        ],
                        // 'axios' => [
                        //     'url' => '#',
                        // ]
                    ]
                ];
                $instance['delete'] = [
                    'swal' => [
                        'id' => "swal-blank",
                        'settings' => [
                            'icon' => 'warning',
                            'title' => 'Are you sure to DELETE?',
                            'html' => "You won't be able to revert this!",
                            'confirmButtonText' => 'Confirm',
                        ],
                    ]
                ];
                return view('components.datatables.action', compact('instance'))->render();
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['action']);
    }

    public function query()
    {
        return Survey::select([
            'surveys.*',
        ]);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', '')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name')->title('Title'),
            Column::computed('action')->addClass('text-center'),
        ];
    }
}
