<?php

namespace App\DataTables\Admin\Role;

use App\Models\Role;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class RoleDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'roleDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->addColumn('control', function ($instance) {
                $request = $this->httpRequest;
                $checkboxId = $this->checkboxId;
                return $this->renderColumnControl($request, $instance, $checkboxId);
            })
            ->editColumn('active', function ($instance) {
                $url = route('admin.roles.update-active', $instance->uuid);
                return $this->renderColumnIsActive($instance, 'is_active', $url);
            })
            ->filterColumn('active', function ($query, $keyword) {
                $query->whereRaw("IF(roles.is_active = 1, 'active', 'inactive') LIKE '%{$keyword}%'");
            })
            ->addColumn('action', function ($instance) {
                $id = $instance->uuid;
                $instance['upsert'] = [
                    'data' => [
                        'id' => $id,
                        'title' => $instance->title,
                        'is_active' => $instance->is_active,
                        'description' => $instance->description,
                        'permissions' => $instance->permissions->pluck('name')
                    ],
                    'swal' => [
                        'id' => "upsert",
                        'axios' => [
                            'url' => route('admin.roles.update', $id),
                        ]
                    ]
                ];
                if ($instance->rule_can_delete) {
                    $instance['delete'] = [
                        'swal' => [
                            'id' => "swal-blank",
                            'settings' => [
                                'icon' => 'warning',
                                'title' => 'Are you sure to DELETE?',
                                'html' => "You won't be able to revert this!",
                                'confirmButtonText' => 'Confirm',
                            ],
                            'axios' => [
                                'url' => route('admin.roles.destroy', $id),
                                'method' => 'delete'
                            ]
                        ]
                    ];
                }
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['control','active', 'action']);
    }

    public function query(Role $model)
    {
        return $model
            ->with(['permissions'])
            ->select('roles.*')
            ->addSelect(DB::raw("IF(roles.is_active = 1, 'active', 'inactive')  AS `active`"))
            ->whereNotIn('name', ['student']);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            $this->checkboxId => "$('input#{$this->checkboxId}').val()",
        ];
    }

    protected function getParameters()
    {
        return [
            'order' => [
                2, 'asc'
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', $this->renderCheckboxAll($this->checkboxId))->titleAttr('')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('title')->title('Role'),
            Column::make('description')->title('Description'),
            Column::make('active')->title('Status'),
            Column::computed('action')->title('Action')->addClass('text-center'),
        ];
    }
}
