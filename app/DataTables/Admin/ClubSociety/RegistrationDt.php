<?php

namespace App\DataTables\Admin\ClubSociety;

use Illuminate\Http\Request;
use App\Models\Attendance;
use App\Models\Student;
use App\Models\Activity;
use App\Models\ClubSociety;
use App\Models\Registration;
use App\Vendors\Yajra\BaseDataTable;
use Yajra\DataTables\Html\Column;

class RegistrationDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'registrationDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    public function query()
    {

        $studentSubQuery = Student::query()
            ->select([
                'students.id',
                'students.name',
            ]);
        $clubSocietySubQuery = ClubSociety::query()
            ->select([
                'club_societies.id',
                'club_societies.name',
            ]);
        return Registration::select([
            'registrations.*',
            'students.name as student_name',
            'club_societies.name as club_society_name',
        ])
            ->joinSub($studentSubQuery, 'students', function ($query) {
                $query->on('registrations.student_id', '=', 'students.id');
            })
            ->joinSub($clubSocietySubQuery, 'club_societies', function ($query) {
                $query->on('registrations.model_id', '=', 'club_societies.id');
            })
            ->where('model_type', ClubSociety::class);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', '')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('student_name'),
            Column::make('club_society_name'),
            Column::computed('actions')->addClass('text-center'),
        ];
    }
}
