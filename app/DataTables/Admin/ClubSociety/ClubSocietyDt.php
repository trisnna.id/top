<?php

namespace App\DataTables\Admin\ClubSociety;

use App\Models\ClubSociety;
use App\Models\ClubSocietyCategory;
use App\Models\Registration;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class ClubSocietyDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'clubSocietyDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    public function query()
    {
        $registrationSubQuery = Registration::query()
            ->select([
                'registrations.model_id',
                DB::raw("COUNT(registrations.student_id) as count_student")
            ])
            ->where('model_type', ClubSociety::class)
            ->groupBy('registrations.model_id');
        return ClubSociety::select([
            'club_societies.*',
            DB::raw("COALESCE(registrations.count_student,0) as count_student"),
        ])->leftJoinSub($registrationSubQuery, 'registrations', function ($query) {
            $query->on('club_societies.id', '=', 'registrations.model_id');
        });
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', '')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name'),
            Column::make('count_student')->title('No. Student Register'),
            Column::computed('actions')->addClass('text-center'),
        ];
    }
}
