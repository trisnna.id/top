<?php

namespace App\DataTables\Admin\Attendance;

use Illuminate\Http\Request;
use App\Models\Attendance;
use App\Models\Student;
use App\Models\Activity;
use App\Vendors\Yajra\BaseDataTable;
use Yajra\DataTables\Html\Column;

class AttendanceDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'attendanceDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('attendance_at', function ($instance) {
                return !empty($instance->attendance_at ?? null) ? $instance->attendance_at->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    public function query()
    {

        $q1SubQuery = Student::query()
            ->select([
                'students.id',
                'students.name',
            ]);
        $q2SubQuery = Activity::query()
            ->select([
                'activities.id',
                'activities.name',
            ]);
        return Attendance::select([
            'attendances.*',
            'q1SubQuery.name as student_name',
            'q2SubQuery.name as activity_name',
        ])
            ->joinSub($q1SubQuery, 'q1SubQuery', function ($query) {
                $query->on('attendances.student_id', '=', 'q1SubQuery.id');
            })
            ->joinSub($q2SubQuery, 'q2SubQuery', function ($query) {
                $query->on('attendances.activity_id', '=', 'q2SubQuery.id');
            });
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'rowGroup' => [
                    'dataSrc' => ['student_name'],
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', '')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('student_name')->visible(false),
            Column::make('activity_name'),
            Column::make('attendance_at'),
            Column::computed('actions')->addClass('text-center'),
        ];
    }
}
