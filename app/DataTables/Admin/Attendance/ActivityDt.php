<?php

namespace App\DataTables\Admin\Attendance;

use App\Models\Activity;
use App\Models\Attendance;
use App\Models\Student;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class ActivityDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'activityDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('start_at', function ($instance) {
                return $instance->location . '<br>' . $instance->start_end_at_readable;
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['start_at']);
    }

    public function query()
    {
        $q1SubQuery = Attendance::query()
            ->select([
                'attendances.activity_id',
                DB::raw("COUNT(attendances.student_id) as count_student")
            ])
            ->groupBy('attendances.activity_id');
        return Activity::select([
            'activities.*',
            DB::raw("COALESCE(q1SubQuery.count_student,0) as count_student"),
        ])
            ->leftJoinSub($q1SubQuery, 'q1SubQuery', function ($query) {
                $query->on('activities.id', '=', 'q1SubQuery.activity_id');
            });
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', '')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name'),
            Column::make('start_at')->title('Event'),
            Column::make('count_student')->title('No. Student Attend'),
            Column::computed('actions')->addClass('text-center'),
        ];
    }
}
