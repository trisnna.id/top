<?php

namespace App\DataTables\Admin\Dashboard;

use App\Constants\OrientationStatusConstant;
use App\Constants\OrientationTypeConstant;
use App\Models\Orientation;
use App\Models\OrientationFilterValue;
use App\Models\Programme;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class OrientationProgrammeDt extends BaseDataTable
{

    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'orientationProgrammeDt';
    protected $auth;
    protected $tableClass = 'thead-none';

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    /**
     * @inheritDoc
     * @param type $query
     * @return type
     */
    public function dataTable($query)
    {
        $orientationTypeConstants = OrientationTypeConstant::getAvailableOptions();
        return datatables()
            ->eloquent($query)
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([]);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'filter' => "{}",
            'filter.intake' => "$('#orientationTypeDt_filter_intake').val()",
        ];
    }

    /**
     * @inheritDoc
     * @param Form $model
     * @return type
     */
    public function query()
    {
        $filters = $this->httpRequest->filter ?? [];
        $orientationIds = [];
        if (!empty($filters['intake'] ?? [])) {
            $orientationIds = OrientationFilterValue::where('name', 'intake')->whereIn('value', $filters['intake'])
                ->get()
                ->pluck('orientation_id')
                ->toArray();
        }
        $programmeSubQuery = Programme::query()
            ->select([
                'id', 'name'
            ]);
        $orientationSubQuery = Orientation::query()
            ->select([
                'id', 'status_id'
            ]);
        return OrientationFilterValue::query()
            ->select([
                'programmes.name',
                DB::raw("COUNT(orientation_filter_values.value) as total")
            ])
            ->leftJoinSub($orientationSubQuery, 'orientations', function ($query) {
                $query->on('orientation_filter_values.orientation_id', '=', 'orientations.id');
            })
            ->leftJoinSub($programmeSubQuery, 'programmes', function ($query) {
                $query->on('orientation_filter_values.value', '=', 'programmes.id');
            })
            ->where('orientation_filter_values.name', 'programme')
            ->where('orientations.status_id', OrientationStatusConstant::APPROVED)
            ->when(!empty($filters['intake'] ?? []), function ($query) use ($orientationIds) {
                $query->whereIn('orientation_filter_values.orientation_id', $orientationIds);
            })
            ->groupBy('orientation_filter_values.value');
    }

    /**
     * @inheritDoc
     * @param Form $model
     * @return type
     */
    protected function getColumns()
    {
        return [
            Column::computed('name'),
            Column::make('total'),
            Column::computed('color_code'),
        ];
    }

    protected function getParameters()
    {
        return [
            'dom' => "<'d-flex justify-content-between align-items-center mx-0 row'<'col-sm-12 col-md-3'><'col-sm-12 col-md-6'f><'col-sm-12 col-md-3'<'dataTables_button'>>><'custom-dt-loader'<'custom-table't>r><'d-flex justify-content-between mx-0 row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            'ordering' => false,
            'responsive' => true,
            'searching' => false,
            'drawCallback' => "function (settings) { modules.dashboard.admin.index.{$this->tableId}RenderChart(settings,'{$this->tableId}','{$this->tableId}Chart','{$this->filename()}'); }",
        ];
    }
}
