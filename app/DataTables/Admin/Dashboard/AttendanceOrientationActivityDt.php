<?php

namespace App\DataTables\Admin\Dashboard;

use App\Constants\OrientationStatusConstant;
use App\Constants\OrientationTypeConstant;
use App\Models\Orientation;
use App\Models\OrientationAttendance;
use App\Models\OrientationFilterValue;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class AttendanceOrientationActivityDt extends BaseDataTable
{

    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'attendanceOrientationActivityDt';
    protected $auth;
    protected $tableClass = 'thead-none';

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    /**
     * @inheritDoc
     * @param type $query
     * @return type
     */
    public function dataTable($query)
    {
        $orientationTypeConstants = OrientationTypeConstant::getAvailableOptions();
        return datatables()
            ->eloquent($query)
            ->addColumn('color_code', function ($instance) use ($orientationTypeConstants) {
                return $orientationTypeConstants[$instance->orientation_type_id]['color']['code'];
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([]);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'filter' => "{}",
            'filter.intake' => "$('#{$this->tableId}_filter_intake').val()",
        ];
    }

    /**
     * @inheritDoc
     * @param Form $model
     * @return type
     */
    public function query(Orientation $model)
    {
        $filters = $this->httpRequest->filter ?? [];
        $orientationIds = [];
        if (!empty($filters['intake'] ?? [])) {
            $orientationIds = OrientationFilterValue::where('name', 'intake')->whereIn('value', $filters['intake'])
                ->get()
                ->pluck('orientation_id')
                ->toArray();
        }
        $orientationAttendanceSubQuery = OrientationAttendance::query()
            ->select([
                'orientation_attendances.orientation_id',
                DB::raw("COUNT(orientation_attendances.orientation_id) as total")
            ])
            ->when(!empty($filters['intake'] ?? []), function ($query) use ($orientationIds) {
                $query->whereIn('orientation_id', $orientationIds);
            })
            ->groupBy('orientation_attendances.orientation_id');
        return $model->select([
            'orientations.*',
            DB::raw("COALESCE(orientation_attendances.total,0) as total")
        ])
            ->leftJoinSub($orientationAttendanceSubQuery, 'orientation_attendances', function ($query) {
                $query->on('orientations.id', '=', 'orientation_attendances.orientation_id');
            })
            ->where('orientations.status_id', OrientationStatusConstant::APPROVED)
            ->when(!empty($filters['intake'] ?? []), function ($query) use ($orientationIds) {
                $query->whereIn('id', $orientationIds);
            });
    }

    /**
     * @inheritDoc
     * @param Form $model
     * @return type
     */
    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('total'),
            Column::computed('color_code'),
        ];
    }

    protected function getParameters()
    {
        return [
            'dom' => "<'d-flex justify-content-between align-items-center mx-0 row'<'col-sm-12 col-md-3'><'col-sm-12 col-md-6'f><'col-sm-12 col-md-3'<'dataTables_button'>>><'custom-dt-loader'<'custom-table't>r><'d-flex justify-content-between mx-0 row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            'ordering' => false,
            'responsive' => true,
            'searching' => false,
            'drawCallback' => "function (settings) { modules.dashboard.admin.index.renderAttendanceOrientationActivityChart(settings,'{$this->tableId}','chart3','{$this->filename()}'); }",
        ];
    }
}
