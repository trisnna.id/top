<?php

namespace App\DataTables\Admin\Dashboard;

use App\Constants\OrientationStatusConstant;
use App\Models\Orientation;
use App\Models\OrientationFilterValue;
use App\Models\OrientationRsvp;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class RsvpOrientationActivityDt extends BaseDataTable
{

    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'rsvpOrientationActivityDt';
    protected $auth;
    protected $tableClass = 'thead-none';

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    /**
     * @inheritDoc
     * @param type $query
     * @return type
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('total_no', function ($instance) {
                return ($instance->rsvp_capacity ?? 0) - $instance->total_yes;
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([]);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'filter' => "{}",
            'filter.intake' => "$('#{$this->tableId}_filter_intake').val()",
        ];
    }

    /**
     * @inheritDoc
     * @param Form $model
     * @return type
     */
    public function query(Orientation $model)
    {
        $filters = $this->httpRequest->filter ?? [];
        $orientationIds = [];
        if (!empty($filters['intake'] ?? [])) {
            $orientationIds = OrientationFilterValue::where('name', 'intake')->whereIn('value', $filters['intake'])
                ->get()
                ->pluck('orientation_id')
                ->toArray();
        }
        $orientationRsvpSubQuery = OrientationRsvp::query()
            ->select([
                'orientation_rsvps.orientation_id',
                DB::raw("COUNT(orientation_rsvps.orientation_id) as total_yes")
            ])
            ->when(!empty($filters['intake'] ?? []), function ($query) use ($orientationIds) {
                $query->whereIn('orientation_id', $orientationIds);
            })
            ->where('is_go', true)
            ->groupBy('orientation_rsvps.orientation_id');
        return $model->select([
            'orientations.*',
            DB::raw("COALESCE(orientation_rsvps.total_yes,0) as total_yes")
        ])
            ->leftJoinSub($orientationRsvpSubQuery, 'orientation_rsvps', function ($query) {
                $query->on('orientations.id', '=', 'orientation_rsvps.orientation_id');
            })
            ->where('orientations.status_id', OrientationStatusConstant::APPROVED)
            ->where('is_rsvp', true)
            ->when(!empty($filters['intake'] ?? []), function ($query) use ($orientationIds) {
                $query->whereIn('id', $orientationIds);
            });
    }

    /**
     * @inheritDoc
     * @param Form $model
     * @return type
     */
    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('rsvp_capacity'),
            Column::make('total_yes'),
            Column::computed('total_no'),
        ];
    }


    protected function getParameters()
    {
        return [
            'dom' => "<'d-flex justify-content-between align-items-center mx-0 row'<'col-sm-12 col-md-3'><'col-sm-12 col-md-6'f><'col-sm-12 col-md-3'<'dataTables_button'>>><'custom-dt-loader'<'custom-table't>r><'d-flex justify-content-between mx-0 row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            'ordering' => false,
            'responsive' => true,
            'searching' => false,
            'drawCallback' => "function (settings) { modules.dashboard.admin.index.renderRsvpOrientationActivityChart(settings,'{$this->tableId}','chart4','{$this->filename()}'); }",
        ];
    }
}
