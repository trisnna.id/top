<?php

namespace App\DataTables\Admin\Dashboard;

use App\Models\PreOrientation;
use App\Models\Student;
use App\Models\StudentPreOrientation;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\CollectionDataTable;
use Yajra\DataTables\Html\Column;

class PreOrientationCompletionDt extends BaseDataTable
{

    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'preOrientationCompletionDt';
    protected $auth;
    protected $tableClass = 'thead-none';

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return (new CollectionDataTable($this->collection()))
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([]);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'filter' => "{}",
            'filter.intake' => "$('#{$this->tableId}_filter_intake').val()",
        ];
    }

    public function collection()
    {
        $filters = $this->httpRequest->filter ?? [];
        $totalStudent = Student::query()->when(!empty($filters['intake'] ?? []), function ($query) use ($filters) {
            $query->whereIn('students.intake_id', $filters['intake']);
        })->count();
        $checkpoints = [1, 2, 3, 4, 5, 6];
        $q1SubQuery = PreOrientation::select([
            'pre_orientations.id',
            'pre_orientations.checkpoint_id',
            'pre_orientations.intakes',
            'pre_orientations.faculties',
            'pre_orientations.schools',
            'pre_orientations.programmes',
            'pre_orientations.mobilities',
            'pre_orientations.localities',
            'pre_orientations.status',
        ]);
        $student = Student::query()
            ->select([
                'id',
                'intake_id',
                DB::raw("(CONCAT('[','\"', intake_id,'\"',']')) as intake_char"),
                'faculty_id',
                DB::raw("(CONCAT('[','\"', faculty_id,'\"',']')) as faculty_char"),
                'school_id',
                DB::raw("(CONCAT('[','\"', school_id,'\"',']')) as school_char"),
                'programme_id',
                DB::raw("(CONCAT('[','\"', programme_id,'\"',']')) as programme_char"),
                'mobility_id',
                DB::raw("(CONCAT('[','\"', mobility_id,'\"',']')) as mobility_char"),
                'locality_id',
                DB::raw("(CONCAT('[','\"', locality_id,'\"',']')) as locality_char"),
            ])->when(!empty($filters['intake'] ?? []), function ($query) use ($filters) {
                $query->whereIn('students.intake_id', $filters['intake']);
            });
        foreach ($checkpoints as $key => $checkpoint) {
            $preOrientationSubQuery = PreOrientation::select(DB::raw('count(*)'))
                ->whereRaw(DB::raw("pre_orientations.checkpoint_id = {$checkpoint} and pre_orientations.status != 'draft' and json_contains(pre_orientations.intakes, intake_char) and json_contains(pre_orientations.faculties, faculty_char) and json_contains(pre_orientations.schools, school_char) and json_contains(pre_orientations.programmes, programme_char) and json_contains(pre_orientations.mobilities, mobility_char) and json_contains(pre_orientations.localities, locality_char)"));
            $q2SubQuery = StudentPreOrientation::select(DB::raw('count(*)'))
                ->leftJoinSub($q1SubQuery, 'q1SubQuery', function ($query) {
                    $query->on('students_preorientations.pre_orientation_id', '=', 'q1SubQuery.id');
                })
                ->whereRaw(DB::raw("students_preorientations.completed = 1 and q1SubQuery.status != 'draft' and q1SubQuery.checkpoint_id = {$checkpoint} and students_preorientations.student_id = students.id and json_contains(q1SubQuery.intakes, intake_char) and json_contains(q1SubQuery.faculties, faculty_char) and json_contains(q1SubQuery.schools, school_char) and json_contains(q1SubQuery.programmes, programme_char) and json_contains(q1SubQuery.mobilities, mobility_char) and json_contains(q1SubQuery.localities, locality_char)"));
            $student->addSelect(DB::raw("(convert(({$q2SubQuery->toSql()})/({$preOrientationSubQuery->toSql()}),double)) as checkpoint_complete{$checkpoint}"));
            // ->addSelect(DB::raw("({$preOrientationSubQuery->toSql()}) as total_checkpoint{$checkpoint}"))
            // ->addSelect(DB::raw("({$q2SubQuery->toSql()}) as complete_checkpoint{$checkpoint}"))
        }
        $student = $student->get();
        foreach ($checkpoints as $key => $checkpoint) {
            $percentageMilestones[$key]['name'] = "Checkpoint {$checkpoint}";
            $percentageMilestones[$key]['complete'] = $student->where("checkpoint_complete{$checkpoint}",'>=', 1)->count() ?? 0;
            $percentageMilestones[$key]['total'] = $totalStudent ?? 0;
            $percentageMilestones[$key]['percentage'] = $totalStudent != 0 ? number_format($percentageMilestones[$key]['complete'] / $totalStudent * 100, 2) : 0;
        }
        return collect($percentageMilestones);
    }

    protected function getColumns()
    {
        return [
            Column::make('name'),
            Column::make('percentage'),
            Column::make('complete'),
        ];
    }

    protected function getParameters()
    {
        return [
            'dom' => "<'d-flex justify-content-between align-items-center mx-0 row'<'col-sm-12 col-md-3'><'col-sm-12 col-md-6'f><'col-sm-12 col-md-3'<'dataTables_button'>>><'custom-dt-loader'<'custom-table't>r><'d-flex justify-content-between mx-0 row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            'ordering' => false,
            'responsive' => true,
            'searching' => false,
            'bInfo' => false,
            'drawCallback' => "function (settings) { modules.dashboard.admin.index.renderPreOrientationCompletionChart(settings,'{$this->tableId}','chart5','{$this->filename()}'); }",
        ];
    }
}
