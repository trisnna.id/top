<?php

namespace App\DataTables\Admin\Dashboard;

use App\Constants\OrientationStatusConstant;
use App\Constants\OrientationTypeConstant;
use App\Models\Orientation;
use App\Models\OrientationFilterValue;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class OrientationTypeDt extends BaseDataTable
{

    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'orientationTypeDt';
    protected $auth;
    protected $tableClass = 'thead-none';

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    /**
     * @inheritDoc
     * @param type $query
     * @return type
     */
    public function dataTable($query)
    {
        $orientationTypeConstants = OrientationTypeConstant::getAvailableOptions();
        return datatables()
            ->eloquent($query)
            ->addColumn('name', function ($instance) use ($orientationTypeConstants) {
                return $orientationTypeConstants[$instance->orientation_type_id]['title'];
            })
            ->addColumn('color_code', function ($instance) use ($orientationTypeConstants) {
                return $orientationTypeConstants[$instance->orientation_type_id]['color']['code'];
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([]);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'filter' => "{}",
            'filter.intake' => "$('#{$this->tableId}_filter_intake').val()",
        ];
    }

    /**
     * @inheritDoc
     * @param Form $model
     * @return type
     */
    public function query()
    {
        $filters = $this->httpRequest->filter ?? [];
        $orientationIds = [];
        if (!empty($filters['intake'] ?? [])) {
            $orientationIds = OrientationFilterValue::where('name', 'intake')->whereIn('value', $filters['intake'])
                ->get()
                ->pluck('orientation_id')
                ->toArray();
        }
        return Orientation::query()
            ->select([
                'orientation_type_id',
                DB::raw("COUNT(orientations.orientation_type_id) as total")
            ])
            ->where('orientations.status_id', OrientationStatusConstant::APPROVED)
            ->when(!empty($filters['intake'] ?? []), function ($query) use ($orientationIds) {
                $query->whereIn('id', $orientationIds);
            })
            ->groupBy('orientation_type_id');
    }

    /**
     * @inheritDoc
     * @param Form $model
     * @return type
     */
    protected function getColumns()
    {
        return [
            Column::computed('name'),
            Column::make('total'),
            Column::computed('color_code'),
        ];
    }

    protected function getParameters()
    {
        return [
            'dom' => "<'d-flex justify-content-between align-items-center mx-0 row'<'col-sm-12 col-md-3'><'col-sm-12 col-md-6'f><'col-sm-12 col-md-3'<'dataTables_button'>>><'custom-dt-loader'<'custom-table't>r><'d-flex justify-content-between mx-0 row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            'ordering' => false,
            'responsive' => true,
            'searching' => false,
            'bInfo' => false,
            'paging' => false,
            'drawCallback' => "function (settings) { modules.dashboard.admin.index.{$this->tableId}RenderChart(settings,'{$this->tableId}','{$this->tableId}Chart','{$this->filename()}'); }",
        ];
    }
}
