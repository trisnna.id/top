<?php

namespace App\DataTables\Admin\Dashboard;

use App\Constants\OrientationTypeConstant;
use App\DataTables\Admin\Dashboard\CompulsoryCoreDt;
use App\Models\Orientation;
use Carbon\Carbon;

class ComplementaryDt extends CompulsoryCoreDt
{

    protected $tableId = 'complementaryDt';

    public function query()
    {
        $request = $this->httpRequest;
        $auth = $this->auth;
        return (new Orientation())->query()
            ->where('orientations.orientation_type_id', OrientationTypeConstant::COMPLEMENTARY)
            ->whereDate('start_date', Carbon::today());
    }
}
