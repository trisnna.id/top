<?php

namespace App\DataTables\Admin\Mentor;

use App\Models\Activity;
use App\Models\Attendance;
use App\Models\Mentor;
use App\Models\Student;
use App\Models\Staff;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class MentorDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'mentorDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['']);
    }

    public function query()
    {
        $staffSubQuery = Staff::query()
            ->select([
                'staffs.id',
                'staffs.name',
            ]);
        $studentSubQuery = Student::query()
            ->select([
                'students.id',
                'students.name',
            ]);
        $countStudentSubQuery = Student::query()
            ->select([
                'students.mentor_id',
                DB::raw("COUNT(students.mentor_id) as value")
            ])
            ->groupBy('students.mentor_id');
        return Mentor::select([
            'mentors.*',
            DB::raw("COALESCE(staffs.name,students.name) as mentor_name"),
            DB::raw("COALESCE(count_students.value,0) as count_student"),
        ])
            ->leftJoinSub($countStudentSubQuery, 'count_students', function ($query) {
                $query->on('mentors.id', '=', 'count_students.mentor_id');
            })
            ->leftJoinSub($staffSubQuery, 'staffs', function ($query) {
                $query->on('mentors.model_id', '=', 'staffs.id')->where('model_type', Staff::class);
            })
            ->leftJoinSub($studentSubQuery, 'students', function ($query) {
                $query->on('mentors.model_id', '=', 'students.id')->where('model_type', Student::class);
            });
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', '')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('mentor_name'),
            Column::make('count_student')->title('No. Mentee'),
            Column::computed('actions')->addClass('text-center'),
        ];
    }
}
