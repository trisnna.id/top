<?php

namespace App\DataTables\Admin\Mentor;

use App\Models\Activity;
use App\Models\Attendance;
use App\Models\Mentor;
use App\Models\Student;
use App\Models\Staff;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class StudentDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'studentDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['']);
    }

    public function query()
    {
        $staffSubQuery = Staff::query()
            ->select([
                'staffs.id',
                'staffs.name',
            ]);
        $studentSubQuery = Student::query()
            ->select([
                'students.id',
                'students.name',
            ]);
        $mentorSubQuery = Mentor::query()
            ->select([
                'mentors.id',
                'mentors.model_type',
                'mentors.model_id',
            ]);
        return Student::select([
            'students.*',
            DB::raw("COALESCE(q2.name,q3.name) as mentor_name"),
        ])
            ->leftJoinSub($mentorSubQuery, 'q1', function ($query) {
                $query->on('students.mentor_id', '=', 'q1.id');
            })
            ->leftJoinSub($staffSubQuery, 'q2', function ($query) {
                $query->on('q1.model_id', '=', 'q2.id')->where('q1.model_type', Staff::class);
            })
            ->leftJoinSub($studentSubQuery, 'q3', function ($query) {
                $query->on('q1.model_id', '=', 'q3.id')->where('q1.model_type', Student::class);
            });
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', '')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name')->title('Mentee Name'),
            Column::make('mentor_name'),
            Column::computed('actions')->addClass('text-center'),
        ];
    }
}
