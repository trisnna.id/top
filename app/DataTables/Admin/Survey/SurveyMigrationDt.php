<?php

namespace App\DataTables\Admin\Survey;

use App\Models\Survey;
use App\Models\SurveyMigration;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Column;

class SurveyMigrationDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'surveyMigrationDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('file_name', function ($instance) {
                $media = $instance->getFirstMedia('migration');
                return $this->renderColumn('file_name',compact('instance','media'));
            })
            ->addColumn('status', function ($instance) {
                if($instance->is_complete){
                    return 'Completed';
                }else{
                    if($instance->expired_at->isPast()){
                        return 'Error';
                    }else{
                        return 'In-progress';
                    }
                }
                return $instance->start_end_at_readable;
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['file_name','status']);
    }

    public function query()
    {
        return SurveyMigration::query();
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('file_name'),
            Column::make('total_record'),
            Column::computed('status'),
            Column::computed('created_at')->title('Datetime')
        ];
    }
}
