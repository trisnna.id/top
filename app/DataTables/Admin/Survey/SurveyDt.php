<?php

namespace App\DataTables\Admin\Survey;

use App\Models\Activity;
use App\Models\Attendance;
use App\Models\Survey;
use App\Models\SurveyAnswer;
use App\Models\Student;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class SurveyDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'surveyDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('start_at', function ($instance) {
                return $instance->start_end_at_readable;
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['start_at']);
    }

    public function query()
    {

        $q1SubQuery = SurveyAnswer::query()
            ->select([
                'survey_answers.survey_id',
                DB::raw("COUNT(survey_answers.student_id) as count_student")
            ])
            ->groupBy('survey_answers.survey_id');
        return Survey::select([
            'surveys.*',
            DB::raw("COALESCE(q1SubQuery.count_student,0) as count_student"),
        ])
            ->leftJoinSub($q1SubQuery, 'q1SubQuery', function ($query) {
                $query->on('surveys.id', '=', 'q1SubQuery.survey_id');
            });
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', '')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name'),
            Column::make('start_at')->title('Survey Datetime'),
            Column::make('count_student')->title('No. Student Survey'),
            Column::computed('actions')->addClass('text-center'),
        ];
    }
}
