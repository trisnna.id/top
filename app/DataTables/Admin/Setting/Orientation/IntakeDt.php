<?php

namespace App\DataTables\Admin\Setting\Orientation;

use App\Models\Intake;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class IntakeDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'intakeDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('orientation_activities_active', function ($instance) {
                $url = route('admin.settings.intakes.update-orientation-activities-active', $instance->uuid);
                return $this->renderColumnIsActive($instance, 'is_orientation_activities_active', $url);
            })
            ->filterColumn('orientation_activities_active', function ($query, $keyword) {
                $query->whereRaw("IF(intakes.is_orientation_activities_active = 1, 'active', 'inactive') LIKE '%{$keyword}%'");
            })
            ->editColumn('timetable_active', function ($instance) {
                $url = route('admin.settings.intakes.update-timetable-active', $instance->uuid);
                return $this->renderColumnIsActive($instance, 'is_timetable_active', $url);
            })
            ->filterColumn('timetable_active', function ($query, $keyword) {
                $query->whereRaw("IF(intakes.is_timetable_active = 1, 'active', 'inactive') LIKE '%{$keyword}%'");
            })
            ->addColumn('action', function ($instance) {
                $instance['upsert'] = [
                    'data' => [
                        'id' => $instance->uuid,
                        'is_orientation_activities_active' => $instance->is_orientation_activities_active,
                        'orientation_activities_message' => $instance->orientation_activities_message,
                        'is_timetable_active' => $instance->is_timetable_active,
                        'timetable_message' => $instance->timetable_message,
                    ],
                    'swal' => [
                        'id' => "upsertIntake",
                        'axios' => [
                            'url' => route('admin.settings.intakes.update', [$instance->uuid]),
                        ]
                    ]
                ];
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['control', 'action', 'orientation_activities_active', 'timetable_active']);
    }

    public function query()
    {
        $name = Carbon::today()->subYear()->format('Y') . '00';
        return Intake::query()
            ->select([
                'intakes.*',
            ])
            ->addSelect(DB::raw("IF(intakes.is_orientation_activities_active = 1, 'active', 'inactive')  AS `orientation_activities_active`"))
            ->addSelect(DB::raw("IF(intakes.is_timetable_active = 1, 'active', 'inactive')  AS `timetable_active`"))
            ->where('name', '>', $name);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            $this->checkboxId => "$('input#{$this->checkboxId}').val()",
        ];
    }

    protected function getParameters()
    {
        return [
            'order' => [
                [1, 'desc']
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name')->title('Intake')->addClass('text-center'),
            Column::make('orientation_activities_active')->title('Orientation Activities')->addClass('text-center'),
            Column::make('timetable_active')->title('Timetable')->addClass('text-center'),
            Column::computed('action')->addClass('text-center'),
        ];
    }
}
