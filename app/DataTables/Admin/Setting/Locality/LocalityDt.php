<?php

namespace App\DataTables\Admin\Setting\Locality;

use App\Models\Locality;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class LocalityDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'localityDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->addColumn('control', function ($instance) {
                $request = $this->httpRequest;
                $checkboxId = $this->checkboxId;
                return $this->renderColumnControl($request, $instance, $checkboxId);
            })
            ->editColumn('active', function ($instance) {
                $url = route('admin.settings.localities.update-active', $instance->uuid);
                return $this->renderColumnIsActive($instance, 'is_active', $url);
            })
            ->filterColumn('active', function ($query, $keyword) {
                $query->whereRaw("IF(localities.is_active = 1, 'active', 'inactive') LIKE '%{$keyword}%'");
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['control', 'active']);
    }

    public function query()
    {
        return Locality::query()
            ->select([
                'localities.*',
            ])
            ->addSelect(DB::raw("IF(localities.is_active = 1, 'active', 'inactive')  AS `active`"));
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            $this->checkboxId => "$('input#{$this->checkboxId}').val()",
        ];
    }

    protected function getParameters()
    {
        return [
            'order' => [
                2, 'asc'
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', $this->renderCheckboxAll($this->checkboxId))->titleAttr('')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name')->title('Locality'),
            Column::make('active')->title('Status'),
        ];
    }
}
