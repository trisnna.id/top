<?php

namespace App\DataTables\Admin\Setting\Mail;

use App\Mail\NotificationMail;
use App\Models\Mail;
use App\Notifications\Orientation\OrientationApprovalNotification;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Column;

class MailDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'mailDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($instance) {
                $id = $instance->uuid;
                $arrayPlacements = [];
                if ($instance->name == 'orientation-approval') {
                    $arrayPlacements = (new OrientationApprovalNotification())->arrayReplacements();
                }
                $mail = new NotificationMail($instance->subject, json_decode($instance->content));
                $instance['upsert'] = [
                    'redirect' => route('admin.settings.mails.edit', $id),
                ];
                $instance['show'] = [
                    'data' => [
                        'body' => $mail->render()
                    ],
                    'swal' => [
                        'id' => "show",
                        'setting' => [
                            'title' => 'Preview Content'
                        ],
                        'axios' => [
                            'url' => '#',
                        ]
                    ]
                ];
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['action']);
    }

    public function query()
    {
        return Mail::query()
            ->select([
                'mails.*',
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('title')->title('Name'),
            Column::computed('action')->addClass('text-center'),
        ];
    }
}
