<?php

namespace App\DataTables\Admin\Setting\Page;

use App\Models\Page;
use App\Notifications\Orientation\OrientationApprovalNotification;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Column;

class PageDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'pageDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($instance) {
                $instance['upsert'] = [
                    'data' => [
                        'id' => $instance->uuid,
                        'title' => $instance->title,
                        'content' => json_decode($instance->content_encode),
                    ],
                    'swal' => [
                        'id' => "upsert",
                        'axios' => [
                            'url' => route('admin.settings.pages.update', $instance->uuid),
                        ]
                    ]
                ];
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['action']);
    }

    public function query()
    {
        return Page::query()
            ->select([
                'pages.*',
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('title')->title('Name'),
            Column::computed('action')->addClass('text-center'),
        ];
    }
}
