<?php

namespace App\DataTables\Admin\Setting\LevelStudy;

use App\Models\LevelStudy;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class LevelStudyDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'levelStudyDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->addColumn('control', function ($instance) {
                $request = $this->httpRequest;
                $checkboxId = $this->checkboxId;
                return $this->renderColumnControl($request, $instance, $checkboxId);
            })
            ->editColumn('active', function ($instance) {
                if ($instance->is_cms_active) {
                    $url = route('admin.settings.level-studies.update-active', $instance->uuid);
                    return $this->renderColumnIsActive($instance, 'is_active', $url);
                } else {
                    return $this->renderColumnIsActive($instance, 'is_active');
                }
            })
            ->filterColumn('active', function ($query, $keyword) {
                $query->whereRaw("IF(level_studies.is_active = 1, 'active', 'inactive') LIKE '%{$keyword}%'");
            })
            ->editColumn('cms_active', function ($instance) {
                return $this->renderColumnIsActive($instance, 'is_cms_active');
            })
            ->filterColumn('cms_active', function ($query, $keyword) {
                $query->whereRaw("IF(level_studies.is_cms_active = 1, 'active', 'inactive') LIKE '%{$keyword}%'");
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['control', 'active', 'cms_active']);
    }

    public function query()
    {
        return LevelStudy::query()
            ->select([
                'level_studies.*',
            ])
            ->addSelect(DB::raw("IF(level_studies.is_active = 1, 'active', 'inactive')  AS `active`"))
            ->addSelect(DB::raw("IF(level_studies.is_cms_active = 1, 'active', 'inactive')  AS `cms_active`"));
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            $this->checkboxId => "$('input#{$this->checkboxId}').val()",
        ];
    }

    protected function getParameters()
    {
        return [
            'order' => [
                [3, 'asc'], [2, 'asc']
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', $this->renderCheckboxAll($this->checkboxId))->titleAttr('')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name')->title('Level of Study'),
            Column::make('active')->title('Status'),
            Column::make('cms_active')->title('Sync CMS Status'),
        ];
    }
}
