<?php

namespace App\DataTables\Admin\Announcement;

use App\Models\Resource;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Yajra\DataTables\CollectionDataTable;
use Yajra\DataTables\Html\Column;

class AnnouncementDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'announcementDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable()
    {
        return (new CollectionDataTable($this->collection()))
            ->addIndexColumn()
            ->editColumn('action', function ($item) {
                $instance['upsert'] = [
                    'data' => $item,
                    'swal' => [
                        'id' => "upsert",
                    ]
                ];
                $instance['publish'] = [
                    'swal' => [
                        'id' => "swal-blank",
                        'settings' => [
                            'icon' => 'warning',
                            'title' => 'Are you sure to PUBLISH?',
                            'html' => "You still allowed to unpublish this item.",
                            'confirmButtonText' => 'Confirm',
                        ],
                    ],
                    // 'axios' => [
                    //     'url' => '#',
                    //     'method' => 'post'
                    // ]
                ];
                $instance['unpublish'] = [
                    'swal' => [
                        'id' => "swal-blank",
                        'settings' => [
                            'icon' => 'warning',
                            'title' => 'Are you sure to UNPUBLISH?',
                            'html' => "You won't be able to revert this!",
                            'confirmButtonText' => 'Confirm',
                        ],
                    ],
                    // 'axios' => [
                    //     'url' => '#',
                    //     'method' => 'post'
                    // ]
                ];
                $instance['show'] = [
                    'data' => $item,
                    'swal' => [
                        'id' => "swal-blank",
                        'settings' => [
                            'title' => $item['title'],
                        ],
                        // 'axios' => [
                        //     'url' => '#',
                        // ]
                    ]
                ];
                $instance['delete'] = [
                    'swal' => [
                        'id' => "swal-blank",
                        'settings' => [
                            'icon' => 'warning',
                            'title' => 'Are you sure to DELETE?',
                            'html' => "You won't be able to revert this!",
                            'confirmButtonText' => 'Confirm',
                        ],
                        // 'axios' => [
                        //     'url' => '#',
                        //     'method' => 'delete'
                        // ]
                    ]
                ];
                return view('components.datatables.action', compact('instance'))->render();
            })
            ->editColumn('student_programmes', function ($item) {
                return implode(', ', $item['student_programmes']);
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name', 'action']);
    }

    public function collection()
    {
        $faker = fake();
        $dummies = [];
        for ($i = 0; $i < 5; $i++) {
            array_push($dummies,[
                'id'=> rand(1, 100),
                'title' => $faker->sentence,
                'content' => $faker->text,
                'week' => $faker->randomElement([1, 2, 3, 4, 5, 6]),
                'nationality' => $faker->country,
                'intakes' => 'All',
                'student_programmes' => $faker->words(),
                'campus' => $faker->randomElement(['TC', 'TU', 'Both'])
            ]);
        }
        return collect($dummies);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('title'),
            Column::make('content'),
            Column::make('week'),
            Column::make('nationality'),
            Column::make('intakes'),
            Column::make('student_programmes'),
            Column::make('campus')->title('Institution'),
            Column::computed('action')->addClass('text-center'),
        ];
    }
}
