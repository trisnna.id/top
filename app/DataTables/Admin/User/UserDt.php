<?php

namespace App\DataTables\Admin\User;

use App\Models\Role;
use App\Models\User;
use App\Models\UserMeta;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class UserDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'userDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->addColumn('control', function ($instance) {
                $request = $this->httpRequest;
                $checkboxId = $this->checkboxId;
                return $this->renderColumnControl($request, $instance, $checkboxId);
            })
            ->editColumn('name', function ($instance) {
                return $this->renderColumn('name', compact('instance'));
            })
            ->editColumn('last_login', function ($instance) {
                return !empty($instance->last_login ?? null) ? $instance->last_login->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
            })
            ->editColumn('email_verified_at', function ($instance) {
                return !empty($instance->email_verified_at ?? null) ? $instance->email_verified_at->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
            })
            ->filterColumn('role_title', function ($query, $keyword) {
                $query->where('roles.title', 'LIKE', "%{$keyword}%");
            })
            ->editColumn('active', function ($instance) {
                $url = route('admin.users.update-active', $instance->uuid);
                return $this->renderColumnIsActive($instance, 'is_active', $url);
            })
            ->filterColumn('active', function ($query, $keyword) {
                $query->whereRaw("IF(users.is_active = 1, 'active', 'inactive') LIKE '%{$keyword}%'");
            })
            ->addColumn('action', function ($instance) {
                $instance['upsert'] = [
                    'data' => [
                        'id' => $instance->uuid,
                        'name' => $instance->name,
                        'id_number' => $instance->model->id_number ?? $instance->email,
                        'role_name' => $instance->role_name,
                        'role_type' => 'admin',
                        'is_active' => $instance->is_active,
                    ],
                    'swal' => [
                        'id' => "updateAdmin",
                        'axios' => [
                            'url' => route(
                                'admin.users.update',
                                [$instance->uuid]
                            ),
                        ]
                    ]
                ];
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['control', 'name', 'last_login', 'action', 'active']);
    }

    public function query()
    {
        $userMetaColorQuery = UserMeta::query()
            ->select([
                'user_id',
                'key',
                'value',
            ])
            ->where('key', 'color');
        $roleSubQuery = Role::query()
            ->select([
                'roles.id',
                'roles.title',
                'roles.name',
            ])
            ->whereNotIn('name', ['student', 'orientation_leader']);
        return User::select([
            'users.*',
            'roles.title as role_title',
            'roles.name as role_name',
            'users_meta_color.value as users_meta_color',
        ])
            ->addSelect(DB::raw("IF(users.is_active = 1, 'active', 'inactive')  AS `active`"))
            ->joinSub($roleSubQuery, 'roles', function ($query) {
                $query->on('users.role_id', '=', 'roles.id');
            })
            ->joinSub($userMetaColorQuery, 'users_meta_color', function ($query) {
                $query->on('users.id', '=', 'users_meta_color.user_id');
            });
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            $this->checkboxId => "$('input#{$this->checkboxId}').val()",
        ];
    }

    protected function getParameters()
    {
        return [
            'order' => [
                2, 'asc'
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('control', $this->renderCheckboxAll($this->checkboxId))->titleAttr('')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name')->title('Name')->addClass('td-d-flex td-align-items-center'),
            Column::make('email')->title('Email'),
            Column::make('role_title')->title('Role')->searchable(true),
            Column::make('last_login')->title('Last Login'),
            Column::make('email_verified_at')->title('Joined Date'),
            Column::make('active')->title('Status'),
            Column::computed('action')->addClass('text-center'),
        ];
    }
}
