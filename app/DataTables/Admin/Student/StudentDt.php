<?php

namespace App\DataTables\Admin\Student;

use App\Models\Student;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class StudentDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'studentDt';
    protected $auth;
    public $tableName = 'Student List';

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        $user = auth()->user();

        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('contact_number', function ($student) use ($user) {
                if ($user->can('student') && $user->hasRole('orientation_leader')) {
                    return '';
                }

                return $student->contact_number;
            })
            ->editColumn('updated_at', function ($student) {
                return $student->updated_at->format('d-m-Y');
            })
            ->addColumn('action', function ($instance) use ($user) {
                if ($user->can('student') && $user->hasRole('orientation_leader')) {
                    $contactNumber = '';
                } else {
                    $contactNumber = $instance->contact_number;
                }

                $instance['profile'] = [
                    'data' => [
                        'name' => $instance->name,
                        'id_number' => $instance->id_number,
                        'taylors_email' => $instance->taylors_email,
                        'personal_email' => $instance->personal_email,
                        'nationality' => $instance->nationality,
                        'intake_number' => $instance->intake_number,
                        'faculty_name' => $instance->faculty_name,
                        'school_name' => $instance->school_name,
                        'programme_name' => $instance->programme_name,
                        'level_of_study' => $instance->level_of_study,
                        'campus' => $instance->campus,
                        'mobility' => $instance->mobility,
                        'contact_number' => $contactNumber,
                        'flame_mentor_name' => $instance->flame_mentor_name,
                        'avatar_url' => $instance->user->getFirstMediaUrl('avatar'),
                    ],
                    'swal' => [
                        'id' => "profile",
                        'settings' => [
                            'title' => "Profile",
                            'showCancelButton' => false,
                            'showConfirmButton' => false,
                        ],
                        'axios' => [
                            'url' => '#',
                        ]
                    ]
                ];
                $instance['orientation'] = [
                    'url' => route('admin.students.show-orientation', $instance->uuid),
                ];
                $instance['point'] = [
                    'data' => [
                        'id' => $instance->id,
                        'orientation_attendance' => $instance->studentPoints->where('name', 'orientation_attendance')->first()->point ?? 0,
                        'orientation_rating' => $instance->studentPoints->where('name', 'orientation_rating')->first()->point ?? 0,
                        'pre_orientation_quiz' => $instance->studentPoints->where('name', 'pre_orientation_quiz')->first()->point ?? 0,
                        'pre_orientation_survey' => $instance->studentPoints->where('name', 'pre_orientation_survey')->first()->point ?? 0,
                        'post_orientation_survey' => $instance->studentPoints->where('name', 'post_orientation_survey')->first()->point ?? 0,
                    ],
                    'swal' => [
                        'id' => "point",
                        'settings' => [
                            'title' => 'Point Stats',
                            'showCancelButton' => false,
                            'showConfirmButton' => false,
                        ],
                        'axios' => [
                            'url' => '#',
                        ]
                    ]
                ];
                return $this->renderColumn('action', compact('instance'));
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['action']);
    }

    public function query()
    {
        $filter = $this->httpRequest->filter ?? [];
        return Student::with(['studentPoints'])
            ->when(!empty($filter['faculty'] ?? null), function ($query) use ($filter) {
                $query->whereIn('faculty_id', $filter['faculty']);
            })
            ->when(!empty($filter['school'] ?? null), function ($query) use ($filter) {
                $query->whereIn('school_id', $filter['school']);
            })
            ->when(!empty($filter['programme'] ?? null), function ($query) use ($filter) {
                $query->whereIn('programme_id', $filter['programme']);
            })
            ->when(!empty($filter['intake'] ?? null), function ($query) use ($filter) {
                $query->whereIn('intake_id', $filter['intake']);
            })
            ->when(!empty($filter['level_study'] ?? null), function ($query) use ($filter) {
                $query->whereIn('level_study_id', $filter['level_study']);
            })
            ->when(!empty($filter['campus'] ?? null), function ($query) use ($filter) {
                $query->where('campus_id', $filter['campus']);
            })
            ->when(!empty($filter['mobility'] ?? null), function ($query) use ($filter) {
                $query->whereIn(DB::raw("COALESCE(mobility_id,0)"), $filter['mobility']);
            })
            ->when(!empty($filter['locality'] ?? null), function ($query) use ($filter) {
                $query->where('locality_id', $filter['locality']);
            })
            ->select([
                'students.*',
            ]);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'filter' => "{}",
            'filter.faculty' => "$('#{$this->tableId}_filter_faculty').val()",
            'filter.school' => "$('#{$this->tableId}_filter_school').val()",
            'filter.programme' => "$('#{$this->tableId}_filter_programme').val()",
            'filter.intake' => "$('#{$this->tableId}_filter_intake').val()",
            'filter.level_study' => "$('#{$this->tableId}_filter_level_study').val()",
            'filter.campus' => "$('#{$this->tableId}_filter_campus').val()",
            'filter.mobility' => "$('#{$this->tableId}_filter_mobility').val()",
            'filter.locality' => "$('#{$this->tableId}_filter_locality').val()",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::make('id_number')->title('Student ID')->addClass('min-w-90px text-center'),
            Column::make('name')->title('Name')->addClass('min-w-100px'),
            Column::make('locality')->title('Locality'),
            Column::make('level_of_study')->title('Student Level')->addClass('min-w-100px'),
            Column::make('school_name')->title('School')->addClass('min-w-90px'),
            Column::make('faculty_name')->title('Faculty')->addClass('min-w-90px'),
            Column::make('taylors_email')->title('Taylor’s Email')->addClass('min-w-100px'),
            Column::make('personal_email')->title('Personal Email')->addClass('min-w-100px'),
            Column::make('contact_number')->title('Contact No'),
            Column::make('updated_at')->title('Last Updated'),
            Column::computed('action')->addClass('text-center'),
        ];
    }

    protected function getParameters()
    {
        $params = parent::getParameters();
        $params['lengthMenu'] =  [
            [5, 10, 25],
            [5, 10, 25],
        ];

        return $params;
    }
}
