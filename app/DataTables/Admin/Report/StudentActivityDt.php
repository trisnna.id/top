<?php

namespace App\DataTables\Admin\Report;

use App\Models\Audit;
use App\Models\Orientation;
use App\Models\OrientationAttendance;
use App\Models\Role;
use App\Models\Student;
use App\Models\User;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Html\Column;

class StudentActivityDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'studentActivityDt';
    protected $auth;
    public $tableName = "Student Activity";

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->filterColumn('user_email', function ($query, $keyword) {
                $query->where('users.email', 'LIKE', "%{$keyword}%");
            })
            ->filterColumn('role_title', function ($query, $keyword) {
                $query->where('roles.title', 'LIKE', "%{$keyword}%");
            })
            ->filterColumn('user_name', function ($query, $keyword) {
                $query->where('users.name', 'LIKE', "%{$keyword}%");
            })
            ->editColumn('auditable_type', function ($instance) {
                $module = trim(preg_replace('/(?<!\ )[A-Z]/', ' $0', Str::replaceFirst("App\\Models\\", '', $instance->auditable_type)));
                switch ($module) {
                    case 'Orientation Attendance':
                        return 'Orientation';
                }
                return $module;
            })
            ->editColumn('event', function ($instance) {
                if ($instance->tags == 'completed' && $instance->auditable_type == OrientationAttendance::class) {
                    return 'Completed Orientation Activity';
                } else {
                    return Str::title($instance->event);
                }
            })
            ->editColumn('old_values', function ($instance) {
                if($instance->auditable_type == OrientationAttendance::class){
                    return $instance->auditable->load('orientation')->orientation->name ?? null;
                }else{
                    return $this->getInstanceName($instance, 'name') ?? ($this->getInstanceName($instance, 'activity_name') ?? null);
                }
            })
            ->editColumn('updated_at', function ($instance) {
                return $instance->updated_at->format(config('app.date_format.php_report').' '.config('app.time_format.php_report'));
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([]);
    }

    protected function getInstanceName($instance, $value)
    {
        $name = $instance->auditable->{$value} ?? null;
        if (!empty($name)) {
            return $name;
        } else {
            $name = $instance->old_values[$value] ?? null;
            if (!empty($name)) {
                return $name;
            } else {
                return $instance->new_values[$value] ?? null;
            }
        }
    }

    protected function onlyModules()
    {
        return [OrientationAttendance::class];
    }

    public function query()
    {
        $roleSubQuery = Role::query()
            ->select([
                'roles.id',
                'roles.title',
                'roles.name',
            ]);
        $studentSubQuery = Student::query()
            ->select([
                'students.id',
                'students.name',
                'students.user_id',
            ]);
        $userSubQuery = User::query()
            ->select([
                'users.id',
                'users.name',
                'users.email',
                'users.role_id',
            ]);
        return Audit::query()
            ->select([
                'audits.*',
                'users.name as user_name',
                'users.email as user_email',
                'roles.title as role_title',
            ])
            ->with(['auditable'])
            ->whereIn('auditable_type', $this->onlyModules())
            ->whereNotNull('student_id')
            ->whereNotNull('tags')
            ->joinSub($studentSubQuery, 'students', function ($query) {
                $query->on('audits.student_id', '=', 'students.id');
            })
            ->joinSub($userSubQuery, 'users', function ($query) {
                $query->on('students.user_id', '=', 'users.id');
            })
            ->joinSub($roleSubQuery, 'roles', function ($query) {
                $query->on('users.role_id', '=', 'roles.id');
            });
    }

    protected function getParameters()
    {
        return [
            'order' => [
                1, 'desc'
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('user_email')->title('Email'),
            Column::make('role_title')->title('Role'),
            Column::make('user_name')->title('User'),
            Column::make('auditable_type')->title('Module'),
            Column::make('event')->title('Event'),
            Column::make('old_values')->title('Activity'),
            Column::make('updated_at')->title('Datetime'),
            Column::make('new_values')->title('')->visible(false),
            Column::make('tags')->title('')->visible(false),
        ];
    }
}
