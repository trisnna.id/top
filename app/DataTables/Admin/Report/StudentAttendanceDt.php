<?php

namespace App\DataTables\Admin\Report;

use App\Models\Orientation;
use App\Models\OrientationAttendance;
use App\Models\OrientationFilter;
use App\Models\Student;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class StudentAttendanceDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'studentAttendanceDt';
    protected $auth;
    public $tableName = "Student Attendance";

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->filterColumn('student_name', function ($query, $keyword) {
                $query->whereRaw("students.name LIKE '%{$keyword}%'");
            })
            ->filterColumn('student_id_number', function ($query, $keyword) {
                $query->whereRaw("students.id_number LIKE '%{$keyword}%'");
            })
            ->filterColumn('student_faculty_name', function ($query, $keyword) {
                $query->whereRaw("students.faculty_name LIKE '%{$keyword}%'");
            })
            ->filterColumn('student_school_name', function ($query, $keyword) {
                $query->whereRaw("students.school_name LIKE '%{$keyword}%'");
            })
            ->filterColumn('student_programme_name', function ($query, $keyword) {
                $query->whereRaw("students.programme_name LIKE '%{$keyword}%'");
            })
            ->filterColumn('student_intake_number', function ($query, $keyword) {
                $query->whereRaw("students.intake_number LIKE '%{$keyword}%'");
            })
            ->filterColumn('orientation_name', function ($query, $keyword) {
                $query->whereRaw("orientations.name LIKE '%{$keyword}%'");
            })
            ->editColumn('updated_at', function ($instance) {
                return $instance->updated_at->format(config('app.date_format.php_report').' '.config('app.time_format.php_report'));
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([]);
    }

    public function query()
    {
        $request = $this->httpRequest;
        $filter = $request->filter ?? [];
        $orientationFilterSubQueries['intake'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'intake')
            ->where(function ($query) use ($filter) {
                $query->when(!empty($filter['orientation_intake'] ?? null), function ($query) use ($filter) {
                    $query->whereJsonContains('value', 'all')->orWhere(function ($query) use ($filter) {
                        foreach ($filter['orientation_intake'] as $value) {
                            $query->orWhereJsonContains('value', (string) $value);
                        }
                    });
                });
            });
        $studentSubQuery = Student::query()
            ->select([
                'students.id',
                'students.name',
                'students.id_number',
                'students.faculty_id',
                'students.faculty_name',
                'students.school_id',
                'students.school_name',
                'students.programme_id',
                'students.programme_name',
                'students.intake_id',
                'students.intake_number',
            ]);
        $orientationSubQuery = Orientation::query()
            ->select([
                'orientations.id',
                'orientations.name',
            ]);
        return OrientationAttendance::query()
            ->select([
                'orientation_attendances.*',
                'students.name as student_name',
                'students.id_number as student_id_number',
                'students.faculty_name as student_faculty_name',
                'students.school_name as student_school_name',
                'students.programme_name as student_programme_name',
                'students.intake_number as student_intake_number',
                'orientations.name as orientation_name',
            ])
            ->joinSub($studentSubQuery, 'students', function ($query) {
                $query->on('orientation_attendances.student_id', '=', 'students.id');
            })
            ->joinSub($orientationSubQuery, 'orientations', function ($query) {
                $query->on('orientation_attendances.orientation_id', '=', 'orientations.id');
            })
            ->where(function ($query) use ($orientationFilterSubQueries) {
                foreach ($orientationFilterSubQueries as $orientationFilterSubQuery) {
                    $query->where($orientationFilterSubQuery, '>', 0);
                }
            })
            ->when(!empty($filter['updated_at'] ?? []), function ($query) use ($filter) {
                $dates = explode(' - ', $filter['updated_at']);
                if ($dates[0] == $dates[1]) {
                    $date = Carbon::createFromFormat(config('app.date_format.php_report') . ' ' . config('app.time_format.php_report'), $dates[0]);
                    $query->whereDate('orientation_attendances.updated_at', $date);
                } else {
                    $from = Carbon::createFromFormat(config('app.date_format.php_report') . ' ' . config('app.time_format.php_report'), $dates[0]);
                    $to = Carbon::createFromFormat(config('app.date_format.php_report') . ' ' . config('app.time_format.php_report'), $dates[1]);
                    $query->whereBetween('orientation_attendances.updated_at', [$from, $to]);
                }
            })
            ->when(!empty($filter['faculty'] ?? null), function ($query) use ($filter) {
                $query->whereIn('students.faculty_id', $filter['faculty']);
            })
            ->when(!empty($filter['school'] ?? null), function ($query) use ($filter) {
                $query->whereIn('students.school_id', $filter['school']);
            })
            ->when(!empty($filter['programme'] ?? null), function ($query) use ($filter) {
                $query->whereIn('students.programme_id', $filter['programme']);
            })
            ->when(!empty($filter['intake'] ?? null), function ($query) use ($filter) {
                $query->whereIn('students.intake_id', $filter['intake']);
            });
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'filter' => "{}",
            'filter.faculty' => "$('#{$this->tableId}_filter_faculty').val()",
            'filter.school' => "$('#{$this->tableId}_filter_school').val()",
            'filter.programme' => "$('#{$this->tableId}_filter_programme').val()",
            'filter.intake' => "$('#{$this->tableId}_filter_intake').val()",
            'filter.orientation_intake' => "$('#{$this->tableId}_filter_orientation_intake').val()",
            'filter.updated_at' => "$('#{$this->tableId}_filter_updated_at').val()",
        ];
    }

    protected function getParameters()
    {
        return [
            'order' => [
                1, 'desc'
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('updated_at')->title('Attend Datetime'),
            Column::make('student_name')->title('Name'),
            Column::make('student_id_number')->title('Student ID'),
            Column::make('student_faculty_name')->title('Faculty'),
            Column::make('student_school_name')->title('School'),
            Column::make('student_programme_name')->title('Programme'),
            Column::make('student_intake_number')->title('Intake (Student)'),
            Column::make('orientation_name')->title('Activity'),
        ];
    }
}
