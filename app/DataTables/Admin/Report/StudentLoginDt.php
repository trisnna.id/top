<?php

namespace App\DataTables\Admin\Report;

use App\Models\Audit;
use App\Models\Student;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Column;

class StudentLoginDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'studentLoginDt';
    protected $auth;
    public $tableName = "Student Login Details";

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->filterColumn('student_name', function ($query, $keyword) {
                $query->whereRaw("students.name LIKE '%{$keyword}%'");
            })
            ->filterColumn('student_id_number', function ($query, $keyword) {
                $query->whereRaw("students.id_number LIKE '%{$keyword}%'");
            })
            ->editColumn('created_at', function ($instance) {
                return $instance->created_at->format(config('app.date_format.php_report').' '.config('app.time_format.php_report'));
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([]);
    }

    public function query()
    {
        $studentSubQuery = Student::query()
            ->select([
                'students.user_id',
                'students.name',
                'students.id_number',
            ]);
        return Audit::query()
            ->select([
                'audits.*',
                'students.name as student_name',
                'students.id_number as student_id_number',
            ])
            ->joinSub($studentSubQuery, 'students', function ($query) {
                $query->on('audits.auditable_id', '=', 'students.user_id');
            })
            ->where('audits.tags', 'login');
    }

    protected function getParameters()
    {
        return [
            'order' => [
                1, 'desc'
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('created_at')->title('Login Datetime'),
            Column::make('student_name')->title('Name'),
            Column::make('student_id_number')->title('Student ID'),
            Column::make('ip_address')->title('IP Address'),
            Column::make('location')->title('Location'),
            Column::make('user_agent')->title('User Agent'),
        ];
    }
}
