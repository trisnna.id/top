<?php

namespace App\DataTables\Admin\Report;

use App\Models\Orientation;
use App\Models\OrientationAttendance;
use App\Models\Student;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class StudentSurveyDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'studentSurveyDt';
    protected $auth;
    public $tableName = "Student Survey";

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('is_pre_orientation_survey_complete', function ($instance) {
                return $instance->is_pre_orientation_survey_complete_yes_no;
            })
            ->editColumn('is_post_orientation_survey_complete', function ($instance) {
                return $instance->is_post_orientation_survey_complete_yes_no;
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([]);
    }

    public function query()
    {
        $filter = $this->httpRequest->filter ?? [];
        return Student::query()
            ->when(!empty($filter['faculty'] ?? null), function ($query) use ($filter) {
                $query->whereIn('faculty_id', $filter['faculty']);
            })
            ->when(!empty($filter['school'] ?? null), function ($query) use ($filter) {
                $query->whereIn('school_id', $filter['school']);
            })
            ->when(!empty($filter['programme'] ?? null), function ($query) use ($filter) {
                $query->whereIn('programme_id', $filter['programme']);
            })
            ->when(!empty($filter['intake'] ?? null), function ($query) use ($filter) {
                $query->whereIn('intake_id', $filter['intake']);
            })
            ->when(!empty($filter['level_study'] ?? null), function ($query) use ($filter) {
                $query->whereIn('level_study_id', $filter['level_study']);
            })
            ->when(!empty($filter['campus'] ?? null), function ($query) use ($filter) {
                $query->where('campus_id', $filter['campus']);
            })
            ->when(!empty($filter['mobility'] ?? null), function ($query) use ($filter) {
                $query->whereIn(DB::raw("COALESCE(mobility_id,0)"), $filter['mobility']);
            })
            ->when(!empty($filter['locality'] ?? null), function ($query) use ($filter) {
                $query->where('locality_id', $filter['locality']);
            })
            ->select([
                'students.*',
            ]);
    }

    protected function getParameters()
    {
        return [
            'order' => [
                1, 'desc'
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::make('id_number')->title('Student ID')->addClass('min-w-90px text-center'),
            Column::make('name')->title('Name')->addClass('min-w-100px'),
            Column::make('programme_name')->title('Programme'),
            Column::make('intake_number')->title('Intake'),
            Column::make('taylors_email')->title('Taylor’s Email'),
            Column::make('personal_email')->title('Personal Email'),
            Column::make('is_pre_orientation_survey_complete')->title('Pre Orientation Survey Completed'),
            Column::make('is_post_orientation_survey_complete')->title('Post Orientation Survey Completed'),

        ];
    }
}
