<?php

namespace App\DataTables\Admin\StudentPoint;

use App\Models\Student;
use App\Models\StudentPoint;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class StudentPointDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'studentPointDt';
    protected $auth;
    public $tableName = 'Student Points';

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        $user = auth()->user();
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->filterColumn('orient_attend_point', function ($query, $keyword) {
                $query->whereRaw("(orientation_attendance.point) LIKE '%{$keyword}%'");
            })
            ->filterColumn('preor_survey_point', function ($query, $keyword) {
                $query->whereRaw("(pre_orientation_survey.point) LIKE '%{$keyword}%'");
            })
            ->filterColumn('preor_quiz_point', function ($query, $keyword) {
                $query->whereRaw("(pre_orientation_quiz.point) LIKE '%{$keyword}%'");
            })
            ->filterColumn('orient_rating_point', function ($query, $keyword) {
                $query->whereRaw("(orientation_rating.point) LIKE '%{$keyword}%'");
            })
            ->filterColumn('post_survey_point', function ($query, $keyword) {
                $query->whereRaw("(post_orientation_survey.point) LIKE '%{$keyword}%'");
            })
            ->filterColumn('total_point', function ($query, $keyword) {
                $query->whereRaw("(countStudentPointSubQuery.total_point) LIKE '%{$keyword}%'");
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([]);
    }

    public function query()
    {
        $filter = $this->httpRequest->filter ?? [];
        $studentPointSubQuery = StudentPoint::query()
            ->select([
                'student_id',
                'name',
                'point'
            ]);
        $countStudentPointSubQuery = StudentPoint::query()
            ->select([
                'student_id',
                DB::raw('sum(point) as total_point'),
            ])
            ->groupBy('student_id');
        return Student::query()
            ->select([
                'students.*',
                'orientation_attendance.point as orient_attend_point',
                'orientation_rating.point as orient_rating_point',
                'pre_orientation_quiz.point as preor_quiz_point',
                'pre_orientation_survey.point as preor_survey_point',
                'post_orientation_survey.point as post_survey_point',
                'countStudentPointSubQuery.total_point as total_point',
            ])
            ->leftJoinSub($studentPointSubQuery, 'orientation_attendance', function ($query) {
                $query->on('students.id', '=', 'orientation_attendance.student_id')->where('orientation_attendance.name', 'orientation_attendance');
            })
            ->leftJoinSub($studentPointSubQuery, 'orientation_rating', function ($query) {
                $query->on('students.id', '=', 'orientation_rating.student_id')->where('orientation_rating.name', 'orientation_rating');
            })
            ->leftJoinSub($studentPointSubQuery, 'pre_orientation_quiz', function ($query) {
                $query->on('students.id', '=', 'pre_orientation_quiz.student_id')->where('pre_orientation_quiz.name', 'pre_orientation_quiz');
            })
            ->leftJoinSub($studentPointSubQuery, 'pre_orientation_survey', function ($query) {
                $query->on('students.id', '=', 'pre_orientation_survey.student_id')->where('pre_orientation_survey.name', 'pre_orientation_survey');
            })
            ->leftJoinSub($studentPointSubQuery, 'post_orientation_survey', function ($query) {
                $query->on('students.id', '=', 'post_orientation_survey.student_id')->where('post_orientation_survey.name', 'post_orientation_survey');
            })
            ->leftJoinSub($countStudentPointSubQuery, 'countStudentPointSubQuery', function ($query) {
                $query->on('students.id', '=', 'countStudentPointSubQuery.student_id');
            })
            ->when(!empty($filter['faculty'] ?? null), function ($query) use ($filter) {
                $query->whereIn('faculty_id', $filter['faculty']);
            })
            ->when(!empty($filter['school'] ?? null), function ($query) use ($filter) {
                $query->whereIn('school_id', $filter['school']);
            })
            ->when(!empty($filter['programme'] ?? null), function ($query) use ($filter) {
                $query->whereIn('programme_id', $filter['programme']);
            })
            ->when(!empty($filter['intake'] ?? null), function ($query) use ($filter) {
                $query->whereIn('intake_id', $filter['intake']);
            })
            ->when(!empty($filter['level_study'] ?? null), function ($query) use ($filter) {
                $query->whereIn('level_study_id', $filter['level_study']);
            })
            ->when(!empty($filter['campus'] ?? null), function ($query) use ($filter) {
                $query->where('campus_id', $filter['campus']);
            })
            ->when(!empty($filter['mobility'] ?? null), function ($query) use ($filter) {
                $query->whereIn(DB::raw("COALESCE(mobility_id,0)"), $filter['mobility']);
            })
            ->when(!empty($filter['locality'] ?? null), function ($query) use ($filter) {
                $query->where('locality_id', $filter['locality']);
            });
        dd($a->get());
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'filter' => "{}",
            'filter.faculty' => "$('#{$this->tableId}_filter_faculty').val()",
            'filter.school' => "$('#{$this->tableId}_filter_school').val()",
            'filter.programme' => "$('#{$this->tableId}_filter_programme').val()",
            'filter.intake' => "$('#{$this->tableId}_filter_intake').val()",
            'filter.level_study' => "$('#{$this->tableId}_filter_level_study').val()",
            'filter.campus' => "$('#{$this->tableId}_filter_campus').val()",
            'filter.mobility' => "$('#{$this->tableId}_filter_mobility').val()",
            'filter.locality' => "$('#{$this->tableId}_filter_locality').val()",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::make('id_number')->title('Student ID')->addClass('min-w-90px text-center')->orderable(true),
            Column::make('name')->title('Name')->addClass('min-w-100px')->orderable(true),
            Column::make('locality')->title('Locality')->orderable(true),
            Column::make('level_of_study')->title('Student Level')->addClass('min-w-100px')->orderable(true),
            Column::make('school_name')->title('School')->addClass('min-w-90px')->orderable(true),
            Column::make('faculty_name')->title('Faculty')->addClass('min-w-90px')->orderable(true),
            Column::make('taylors_email')->title('Taylor’s Email')->addClass('min-w-100px')->orderable(true),
            Column::make('personal_email')->addClass('min-w-100px')->orderable(true),
            Column::make('contact_number')->addClass('min-w-100px')->orderable(true),
            Column::make('intake_number')->title('Intake')->addClass('min-w-100px')->orderable(true),
            Column::make('programme_name')->title('Programme')->addClass('min-w-100px')->orderable(true),
            Column::make('campus')->title('Institution')->addClass('min-w-100px')->orderable(true),
            Column::make('preor_survey_point')->title('Pre-Orientation Survey')->orderable(true),
            Column::make('preor_quiz_point')->title('Pre-Orientation Quiz')->orderable(true),
            Column::make('orient_attend_point')->title('Orientation Attendance')->orderable(true),
            Column::make('orient_rating_point')->title('Orientation Ratings')->orderable(true),
            Column::make('post_survey_point')->title('Post Orientation Survey')->orderable(true),
            Column::make('total_point')->title('Total Points')->orderable(true),
        ];
    }

    protected function getParameters()
    {
        return [
            'order' => [
                17, 'desc'
            ],
            'lengthMenu' => [
                [5, 10, 25, 50],
                [5, 10, 25, 50],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }
}
