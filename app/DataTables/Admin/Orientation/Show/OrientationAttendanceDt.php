<?php

namespace App\DataTables\Admin\Orientation\Show;

use App\Models\Orientation;
use App\Models\OrientationAttendance;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\Request;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class OrientationAttendanceDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'orientationAttendanceDt';
    protected $auth;
    protected $orientation;

    public function __construct(Request $request, $auth = null, Orientation $orientation)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
        $this->orientation = $orientation;
    }

    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        $orientation = $this->orientation;
        return (new EloquentDataTable($query))
            ->setRowId('id')
            ->addIndexColumn()
            ->editColumn('rating', function ($instance) {
                return $this->renderColumn('rating', compact('instance'));
            })
            ->editColumn('updated_at', function ($instance) {
                return $instance->updated_at_readable;
            })
            ->editColumn('action', function ($instance) use ($orientation) {
                $instance['delete'] = [
                    'swal' => [
                        'id' => "swal-blank",
                        'settings' => [
                            'icon' => 'warning',
                            'title' => 'Are you sure to DELETE?',
                            'html' => "You won't be able to revert this!",
                            'confirmButtonText' => 'Confirm',
                        ],
                        'axios' => [
                            'url' => route('admin.orientations.destroy-attendance', [$orientation->uuid, $instance->uuid]),
                            'method' => 'delete'
                        ]
                    ]
                ];
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['rating','action']);
    }

    public function query(): QueryBuilder
    {
        $orientation = $this->orientation;
        return OrientationAttendance::with(['student'])
            ->select([
                'orientation_attendances.*'
            ])
            ->where('orientation_id', $orientation->id);
    }

    protected function getColumns(): array
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('student.name')->title('Name'),
            Column::make('student.id_number')->title('Student ID'),
            Column::make('rating'),
            Column::make('point'),
            Column::make('updated_at')->title('Scan Attendance'),
            Column::computed('action')->addClass('text-center'),
        ];
    }
}
