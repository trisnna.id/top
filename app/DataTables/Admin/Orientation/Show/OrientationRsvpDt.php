<?php

namespace App\DataTables\Admin\Orientation\Show;

use App\Models\Orientation;
use App\Models\OrientationAttendance;
use App\Models\OrientationRsvp;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class OrientationRsvpDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'orientationRsvpDt';
    protected $auth;
    protected $orientation;

    public function __construct(Request $request, $auth = null, Orientation $orientation)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
        $this->orientation = $orientation;
    }

    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->setRowId('id')
            ->addIndexColumn()
            ->filterColumn('rsvp', function ($query, $keyword) {
                $query->whereRaw("IF(orientation_rsvps.is_go = 1, 'yes', 'no') LIKE '%{$keyword}%'");
            })
            ->editColumn('rsvp', function ($instance) {
                return $this->renderColumnIsActive($instance, 'is_go', null, 'title-yes');
            })
            ->filterColumn('orientation_attendance_updated_at', function ($query, $keyword) {
                $query->whereRaw("orientation_attendances.updated_at LIKE '%{$keyword}%'");
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['rsvp']);
    }

    public function query(): QueryBuilder
    {
        $orientation = $this->orientation;
        $orientationAttendanceSubQuery = OrientationAttendance::query()
            ->select([
                'orientation_attendances.orientation_id',
                'orientation_attendances.student_id',
                'orientation_attendances.updated_at',
            ])
            ->where('orientation_attendances.orientation_id', $orientation->id);

        return OrientationRsvp::with(['student'])
            ->select([
                'orientation_rsvps.*',
                'orientation_attendances.updated_at as orientation_attendance_updated_at'
            ])
            ->addSelect(DB::raw("IF(orientation_rsvps.is_go = 1, 'yes', 'no')  AS `rsvp`"))
            ->leftJoinSub($orientationAttendanceSubQuery, 'orientation_attendances', function ($query) {
                $query->on('orientation_rsvps.student_id', '=', 'orientation_attendances.student_id');
            })
            ->where('orientation_rsvps.orientation_id', $orientation->id);
    }

    protected function getColumns(): array
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('student.name')->title('Name'),
            Column::make('student.id_number')->title('Student ID'),
            Column::make('rsvp')->title('RSVP?'),
            Column::make('orientation_attendance_updated_at')->title('Scan Attendance'),
        ];
    }
}
