<?php

namespace App\DataTables\Admin\Orientation\Show;

use Yajra\DataTables\Html\Column;
use App\DataTables\Admin\Orientation\Edit\AttachmentDt as EditAttachmentDt;

class AttachmentDt extends EditAttachmentDt
{

    protected function getColumns(): array
    {
        return [
            Column::make('name'),
        ];
    }
}
