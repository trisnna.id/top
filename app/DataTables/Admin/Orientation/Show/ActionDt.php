<?php

namespace App\DataTables\Admin\Orientation\Show;

use App\Constants\ActionLogConstant;
use App\Models\ActionLog;
use App\Models\Orientation;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\Request;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ActionDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'actionDt';
    protected $auth;
    protected $orientation;
    protected $tableClass = 'thead-none timeline w-100';

    public function __construct(Request $request, $auth = null, Orientation $orientation)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
        $this->orientation = $orientation;
    }

    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        $actionLogConstants = ActionLogConstant::getAvailableOptions();
        return (new EloquentDataTable($query))
            ->setRowId('id')
            ->editColumn('created_at', function ($instance) {
                return null;
            })
            ->addColumn('icon', function ($instance) use ($actionLogConstants) {
                $icon = $actionLogConstants[$instance->action_id]['icon'];
                return $this->renderColumn('icon', compact('instance', 'icon'));
            })
            ->addColumn('content', function ($instance) use ($actionLogConstants) {
                $actionLogConstant = $actionLogConstants[$instance->action_id];
                return $this->renderColumn('content', compact('instance', 'actionLogConstant'));
            })
            ->rawColumns(['created_at', 'icon', 'content'])
            ->only(array_column($this->getColumns(), 'data'));
    }

    public function query(): QueryBuilder
    {
        $orientation = $this->orientation;
        return ActionLog::select([
            'action_logs.*'
        ])
            ->with(['user'])
            ->where(function ($query) use ($orientation) {
                $query->where([
                    'model_type' => $orientation::class,
                    'model_id' => $orientation->id,
                ]);
            })
            ->orderBy('created_at','desc');
    }

    protected function getParameters()
    {
        return [
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "dom" => 'tp',
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'ordering' => false,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns(): array
    {
        return [
            Column::make('created_at')->addClass('timeline-line w-40px'),
            Column::computed('icon')->addClass('timeline-icon symbol symbol-circle symbol-40px me-4'),
            Column::computed('content')->addClass('timeline-content mb-10 mt-n1'),
        ];
    }
}
