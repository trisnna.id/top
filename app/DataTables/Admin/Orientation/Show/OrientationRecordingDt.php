<?php

namespace App\DataTables\Admin\Orientation\Show;

use App\Models\Orientation;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Yajra\DataTables\CollectionDataTable;
use Yajra\DataTables\Html\Column;

class OrientationRecordingDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'orientationRecordingDt';
    protected $auth;
    protected $orientation;
    protected $tableClass = 'table thead-none table-bordered table-striped align-middle table-row-dashed fs-7 gy-5 th-text-uppercase th-fw-bold w-100';

    public function __construct(Request $request, $auth = null, Orientation $orientation)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
        $this->orientation = $orientation;
    }

    public function dataTable(): CollectionDataTable
    {
        return (new CollectionDataTable($this->collection()))
            ->setRowId('id')
            ->editColumn('name', function ($instance) {
                if ($instance['type'] == 'file') {
                    $instance = $instance['instance'];
                    return $this->renderColumn('filename', compact('instance'), 'components.datatables');
                } elseif ($instance['type'] == 'link') {
                    $instance = $instance['instance'];
                    return $this->renderColumn('link', compact('instance'), 'components.datatables');
                }
            })
            ->editColumn('updated_at', function ($instance) {
                return !empty($instance['updated_at'] ?? null) ? $instance['updated_at']->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
            })
            ->addColumn('action', function ($instance) {
                if ($instance['type'] == 'file') {
                    $file = $instance['instance'];
                    $instance['delete'] = [
                        'swal' => [
                            'id' => "swal-blank",
                            'settings' => [
                                'icon' => 'warning',
                                'title' => 'Are you sure to DELETE?',
                                'html' => "You won't be able to revert this!",
                                'confirmButtonText' => 'Confirm',
                            ],
                            'axios' => [
                                'url' => route(
                                    'admin.orientations.destroy-recording-file',
                                    [$this->orientation->uuid, $file->uuid]
                                ),
                                'method' => 'delete'
                            ]
                        ]
                    ];
                } elseif ($instance['type'] == 'link') {
                    $link = $instance['instance'];
                    $instance['upsert'] = [
                        'data' => [
                            'id' => $link->uuid,
                            'name' => $link->name,
                            'url' => $link->url,
                        ],
                        'swal' => [
                            'id' => "upsertLink",
                            'axios' => [
                                'url' => route('admin.orientations.update-link', [$this->orientation->uuid, $link->uuid]),
                                'method' => 'put'
                            ]
                        ]
                    ];
                    $instance['delete'] = [
                        'swal' => [
                            'id' => "swal-blank",
                            'settings' => [
                                'icon' => 'warning',
                                'title' => 'Are you sure to DELETE?',
                                'html' => "You won't be able to revert this!",
                                'confirmButtonText' => 'Confirm',
                            ],
                            'axios' => [
                                'url' => route('admin.orientations.destroy-recording-link', [$this->orientation->uuid, $link->uuid]),
                                'method' => 'delete'
                            ]
                        ]
                    ];
                }
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            })
            ->rawColumns(['name', 'action'])
            ->only(array_column($this->getColumns(), 'data'));
    }

    public function collection()
    {
        $attachments = collect();
        $orientation = $this->orientation;
        foreach ($orientation->getMedia('recordings') as $file) {
            $attachment = collect([
                'type' => 'file',
                'name' => $file->file_name,
                'updated_at' => $file->updated_at,
                'url' => $file->original_url,
                'instance' => $file
            ]);
            $attachments = $attachments->push($attachment);
        }
        foreach ($orientation->orientationLinks->where('collection_name', 'recordings') as $link) {
            $attachment = collect([
                'type' => 'link',
                'name' => $link->name,
                'updated_at' => $link->updated_at,
                'url' => $link->url,
                'instance' => $link
            ]);
            $attachments = $attachments->push($attachment);
        }
        return $attachments->sortBy('name');
    }

    protected function getParameters()
    {
        return [
            'responsive' => false,
            'searching' => true,
            'paging' => false,
            'bInfo' => false,
            'ordering' => false,
            "language" => [
                "emptyTable" => "No data files & links available"
            ],
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns(): array
    {
        return [
            Column::make('name'),
            Column::make('updated_at')->title('Last Modified'),
            Column::computed('action')->title('Action')->addClass('text-center px-4'),
        ];
    }
}
