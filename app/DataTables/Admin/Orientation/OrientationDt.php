<?php

namespace App\DataTables\Admin\Orientation;

use App\Constants\ActiveInactiveConstant;
use App\Constants\OrientationStatusConstant;
use App\Models\Activity;
use App\Models\Intake;
use App\Models\LevelStudy;
use App\Models\Mobility;
use App\Models\Orientation;
use App\Models\OrientationAttendance;
use App\Models\OrientationFilter;
use App\Models\OrientationFilterValue;
use App\Models\OrientationRsvp;
use App\Models\Programme;
use App\Models\School;
use App\Vendors\Yajra\BaseDataTable;
use App\View\Components\Datatables\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Column;

class OrientationDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'orientationDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function labelMore($instance, $name): string
    {
        $orientationFilters = $instance->orientationFilters->where('name', $name)->first()->value ?? [];
        if (!empty($orientationFilters)) {
            if (count($orientationFilters) == 1 && $orientationFilters[0] == 'all') {
                $label = 'All';
            } else {
                if ($name == 'school') {
                    $names = School::whereIn('id', $orientationFilters)->pluck('name')->toArray();
                } elseif ($name == 'intake') {
                    $names = Intake::whereIn('id', $orientationFilters)->pluck('name')->toArray();
                } elseif ($name == 'programme') {
                    $names = Programme::whereIn('id', $orientationFilters)->pluck('name')->toArray();
                } elseif ($name == 'mobility') {
                    $names = Mobility::whereIn('id', $orientationFilters)->pluck('name')->toArray();
                    $zeroExist = array_filter($orientationFilters, function ($value) {
                        return $value == 0;
                    });
                    if (!empty($zeroExist ?? null)) {
                        $names = [...['None'], ...$names];
                    }
                }

                $count = count($names);
                if ($count > 1) {
                    $countMore = $count - 1;
                    $label = "{$names[0]} <strong>({$countMore} More)<strong>";
                } else {
                    $label = $names[0];
                }
            }
            return $label;
        }
        return null;
    }

    public function dataTable($query)
    {
        return $this->mainDataTable($query);
    }

    public function mainDataTable($query)
    {
        $auth = $this->auth;
        $activeInactiveConstants = ActiveInactiveConstant::getAvailableOptions();
        $orientationStatusConstants = OrientationStatusConstant::getAvailableOptions();
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->filterColumn('orientation_point', function ($query, $keyword) {
                $query->whereRaw("(orientations.point_attendance + orientations.point_rating) LIKE '%{$keyword}%'");
            })
            ->filterColumn('orientation_attendance_count', function ($query, $keyword) {
                $query->whereRaw("orientation_attendances.count LIKE '%{$keyword}%'")
                    ->orWhereRaw("orientations.total_student LIKE '%{$keyword}%'");
            })
            ->editColumn('orientation_attendance_count', function ($instance) {
                return "{$instance->orientation_attendance_count}/{$instance->total_student}";
            })
            ->filterColumn('orientation_rsvp_count', function ($query, $keyword) {
                $query->whereRaw("orientation_rsvps.count LIKE '%{$keyword}%'")
                    ->orWhereRaw("orientations.rsvp_capacity LIKE '%{$keyword}%'");
            })
            ->editColumn('orientation_rsvp_count', function ($instance) {
                if ($instance->is_rsvp) {
                    return "{$instance->orientation_rsvp_count}/{$instance->rsvp_capacity}";
                } else {
                    return 'N/A';
                }
            })
            ->filterColumn('is_rsvp', function ($query, $keyword) {
                $query->whereRaw("IF(orientations.is_rsvp = 1, 'yes', 'no') LIKE '%{$keyword}%'");
            })
            ->editColumn('is_rsvp', function ($instance) {
                return $instance->is_rsvp_yes_no ?? null;
            })
            ->addColumn('point', function ($instance) {
                if ($instance->is_point) {
                    return "{$instance->point_min} | {$instance->point_max}";
                } else {
                    return 'N/A';
                }
            })
            ->editColumn('start_date', function ($instance) {
                return $instance->start_date_readable ?? null;
            })
            ->editColumn('status_name', function ($instance) use ($activeInactiveConstants, $orientationStatusConstants) {
                if ($instance->status_id == OrientationStatusConstant::APPROVED) {
                    $label = $instance->status_name . ": " . $activeInactiveConstants[$instance->is_publish]['title-publish'];
                } else {
                    $label = $instance->status_name;
                }
                if ($instance->status_id == OrientationStatusConstant::APPROVED) {
                    $colorClass = $orientationStatusConstants[$instance->status_id]['colors'][$instance->is_publish]['class'];
                } else {
                    $colorClass = $orientationStatusConstants[$instance->status_id]['color']['class'];
                }
                return $this->renderColumn('status', compact('colorClass', 'label'), 'components.datatables');
            })
            ->editColumn('action', function ($instance) use ($activeInactiveConstants, $auth) {
                if ($instance->rule_can_view) {
                    $instance['show'] = [
                        'redirect' => route('admin.orientations.show', $instance->uuid)
                    ];
                }
                if ($instance->rule_can_approval) {
                    $instance['approval'] = [
                        'redirect' => route('admin.orientations.approvals.edit', $instance->uuid)
                    ];
                }
                if ($instance->rule_can_edit) {
                    $instance['upsert'] = [
                        'redirect' => route('admin.orientations.edit', $instance->uuid)
                    ];
                }
                if ($instance->rule_can_delete) {
                    $instance['delete'] = [
                        'swal' => [
                            'id' => "swal-blank",
                            'settings' => [
                                'icon' => 'warning',
                                'title' => 'Are you sure to DELETE?',
                                'html' => "You won't be able to revert this!",
                                'confirmButtonText' => 'Confirm',
                            ],
                            'axios' => [
                                'url' => route('admin.orientations.destroy', $instance->uuid),
                                'method' => 'delete'
                            ]
                        ]
                    ];
                }
                if ($instance->rule_can_submit) {
                    $instance['submit'] = [
                        'swal' => [
                            'id' => "swal-blank",
                            'settings' => [
                                'icon' => 'success',
                                'title' => 'Are you sure you want to SUBMIT?',
                                'html' => "Make sure to fill all Required fields before SUBMIT. You won't be able to revert this!",
                                'confirmButtonText' => 'Submit Approval',
                            ],
                            'axios' => [
                                'url' => route('admin.orientations.update-approval', [$instance->uuid, 'action' => 'no-update']),
                                'method' => 'put'
                            ]
                        ]
                    ];
                }
                if ($instance->rule_can_publish) {
                    if ($instance->is_publish == ActiveInactiveConstant::ACTIVE) {
                        $titlePublish = strtoupper($activeInactiveConstants[ActiveInactiveConstant::INACTIVE]['title-publish']);
                    } elseif ($instance->is_publish == ActiveInactiveConstant::INACTIVE) {
                        $titlePublish = strtoupper($activeInactiveConstants[ActiveInactiveConstant::ACTIVE]['title-publish']);
                    }
                    $instance['publish'] = [
                        'data' => [
                            'status' => $activeInactiveConstants[!(int) $instance->is_publish]['title-publish'],
                            'color_class' => $activeInactiveConstants[!(int) $instance->is_publish]['color']['class'],
                            'icon_class' => $activeInactiveConstants[!(int) $instance->is_publish]['icon']['icon_class'],
                        ],
                        'swal' => [
                            'id' => "swal-blank",
                            'settings' => [
                                'icon' => 'warning',
                                'title' => "Are you sure to {$titlePublish}?",
                                'html' => "You can change it back later!",
                                'confirmButtonText' => 'Confirm',
                            ],
                            'axios' => [
                                'url' => route('admin.orientations.approvals.update-publish', [$instance->uuid, 'reload' => 'no']),
                                'method' => 'put'
                            ]
                        ]
                    ];
                }
                $instance['clone'] = [
                    'swal' => [
                        'id' => "swal-blank",
                        'settings' => [
                            'icon' => 'warning',
                            'title' => 'Are you sure to CLONE?',
                            'html' => "You won't be able to revert this!",
                            'confirmButtonText' => 'Confirm',
                        ],
                        'axios' => [
                            'url' => route('admin.orientations.clone', $instance->uuid),
                            'method' => 'post'
                        ]
                    ],
                ];
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            })
            ->orderColumn('status_name', function ($query, $order) {
                $query->orderBy('status_name', $order)->orderBy('is_publish', 'desc')->orderBy('updated_at', 'desc');
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['start_at', 'intake', 'school', 'programme', 'mobility', 'status_name', 'action']);
    }

    public function query()
    {
        $request = $this->httpRequest;
        $auth = $this->auth;
        $filter = $request->filter ?? [];
        return $this->mainQuery($request, $auth, $filter)
            ->when($auth->can('manage_activity_approval'), function ($query) use ($auth) {
                $query->where(function ($query) use ($auth) {
                    $query->where('orientations.created_by', $auth->id)->orWhere(function ($query) use ($auth) {
                        $query->where('orientations.created_by', '<>', $auth->id)->where('orientations.status_id', '<>', OrientationStatusConstant::DRAFT);
                    });
                });
            }, function ($query) use ($auth) {
                $query->when($auth->can('manage_orientations'), function ($query) use ($auth) {
                    $query->where('orientations.created_by', $auth->id);
                }, function ($query) {
                    $query->whereNull('orientations.created_by');
                });
            });
    }

    public function mainQuery($request, $auth, $filter)
    {
        $orientationFilterSubQueries['intake'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'intake')
            ->where(function ($query) use ($filter) {
                $query->when(!empty($filter['intake'] ?? null), function ($query) use ($filter) {
                    $query->whereJsonContains('value', 'all')->orWhere(function ($query) use ($filter) {
                        foreach ($filter['intake'] as $value) {
                            $query->orWhereJsonContains('value', (string) $value);
                        }
                    });
                });
            });
        $orientationFilterSubQueries['faculty'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'faculty')
            ->where(function ($query) use ($filter) {
                $query->when(!empty($filter['faculty'] ?? null), function ($query) use ($filter) {
                    $query->whereJsonContains('value', 'all')->orWhere(function ($query) use ($filter) {
                        foreach ($filter['faculty'] as $value) {
                            $query->orWhereJsonContains('value', (string) $value);
                        }
                    });
                });
            });
        $orientationFilterSubQueries['school'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'school')
            ->where(function ($query) use ($filter) {
                $query->when(!empty($filter['school'] ?? null), function ($query) use ($filter) {
                    $query->whereJsonContains('value', 'all')->orWhere(function ($query) use ($filter) {
                        foreach ($filter['school'] as $value) {
                            $query->orWhereJsonContains('value', (string) $value);
                        }
                    });
                });
            });
        $orientationFilterSubQueries['programme'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'programme')
            ->where(function ($query) use ($filter) {
                $query->when(!empty($filter['programme'] ?? null), function ($query) use ($filter) {
                    $query->whereJsonContains('value', 'all')->orWhere(function ($query) use ($filter) {
                        foreach ($filter['programme'] as $value) {
                            $query->orWhereJsonContains('value', (string) $value);
                        }
                    });
                });
            });
        $orientationFilterSubQueries['level_study'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'level_study')
            ->where(function ($query) use ($filter) {
                $query->when(!empty($filter['level_study'] ?? null), function ($query) use ($filter) {
                    $query->whereJsonContains('value', 'all')->orWhere(function ($query) use ($filter) {
                        foreach ($filter['level_study'] as $value) {
                            $query->orWhereJsonContains('value', (string) $value);
                        }
                    });
                });
            });
        $orientationFilterSubQueries['mobility'] = OrientationFilter::query()
            ->selectRaw(DB::raw('COUNT(orientation_filters.id)'))
            ->whereRaw(DB::raw('orientation_filters.orientation_id = orientations.id'))
            ->where('orientation_filters.name', 'mobility')
            ->where(function ($query) use ($filter) {
                $query->when(!is_null($filter['mobility']), function ($query) use ($filter) {
                    $query->whereJsonContains('value', 'all')->orWhere(function ($query) use ($filter) {
                        $query->orWhereJsonContains('value', (string) $filter['mobility']);
                    });
                });
            });
        $orientationAttendanceSubQuery = OrientationAttendance::query()
            ->select([
                'orientation_attendances.orientation_id',
                DB::raw('count(orientation_attendances.orientation_id) as count')
            ])
            ->groupBy('orientation_attendances.orientation_id');
        $orientationRsvpSubQuery = OrientationRsvp::query()
            ->select([
                'orientation_rsvps.orientation_id',
                DB::raw('count(orientation_rsvps.orientation_id) as count')
            ])
            ->where('is_go', true)
            ->groupBy('orientation_rsvps.orientation_id');
        // add query level study
        $levelStudySubQuery = LevelStudy::query()
            ->select(['id', 'name']);
        $orientationFilterValueSubQueryByLevelStudy = OrientationFilterValue::query()
            ->select([
                'orientation_filter_values.orientation_id',
                DB::raw('GROUP_CONCAT(levelStudySubQuery.name ORDER BY levelStudySubQuery.name ASC SEPARATOR ", ") as level_study'),
            ])
            ->where('orientation_filter_values.name', 'level_study')
            ->leftJoinSub($levelStudySubQuery, 'levelStudySubQuery', function ($query) {
                $query->on('orientation_filter_values.value', '=', 'levelStudySubQuery.id');
            })
            ->groupBy('orientation_filter_values.orientation_id');
        // add query average rating
        $orientationAttendanceSubQueryByRating = OrientationAttendance::query()
            ->select([
                'orientation_attendances.orientation_id',
                DB::raw('count(orientation_attendances.orientation_id) as count'),
                DB::raw('sum(orientation_attendances.rating) as sum'),
                DB::raw('sum(orientation_attendances.rating)/count(orientation_attendances.orientation_id) as avg_rating'),
            ])
            ->whereNotNull('rating')
            ->groupBy('orientation_attendances.orientation_id');
        return Orientation::with(['orientationType', 'orientationFilters'])
            ->select([
                'orientations.*',
                DB::raw("COALESCE(orientation_attendances.count,0) as orientation_attendance_count"),
                DB::raw("COALESCE(orientation_rsvps.count,0) as orientation_rsvp_count"),
                DB::raw("(orientations.point_attendance + orientations.point_rating) as orientation_point"),
            ])
            ->addSelect(DB::raw("IF(orientations.is_academic = 1, 'Academic', 'Non-academic')  AS `academic`"))
            ->addSelect('orientationFilterValueSubQueryByLevelStudy.level_study')
            ->addSelect(DB::raw("(ROUND(CAST(orientationAttendanceSubQueryByRating.avg_rating AS DOUBLE),2)) as avg_rating"))
            ->where(function ($query) use ($orientationFilterSubQueries) {
                foreach ($orientationFilterSubQueries as $orientationFilterSubQuery) {
                    $query->where($orientationFilterSubQuery, '>', 0);
                }
            })
            ->leftJoinSub($orientationAttendanceSubQueryByRating, 'orientationAttendanceSubQueryByRating', function ($query) {
                $query->on('orientations.id', '=', 'orientationAttendanceSubQueryByRating.orientation_id');
            })
            ->leftJoinSub($orientationAttendanceSubQuery, 'orientation_attendances', function ($query) {
                $query->on('orientations.id', '=', 'orientation_attendances.orientation_id');
            })
            ->leftJoinSub($orientationRsvpSubQuery, 'orientation_rsvps', function ($query) {
                $query->on('orientations.id', '=', 'orientation_rsvps.orientation_id');
            })
            ->leftJoinSub($orientationFilterValueSubQueryByLevelStudy, 'orientationFilterValueSubQueryByLevelStudy', function ($query) {
                $query->on('orientations.id', '=', 'orientationFilterValueSubQueryByLevelStudy.orientation_id');
            })
            ->when(!empty($filter['status'] ?? null), function ($query) use ($filter) {
                $query->where('orientations.status_id', $filter['status']);
            })
            ->when(!empty($filter['orientation_type'] ?? null), function ($query) use ($filter) {
                $query->where('orientations.orientation_type_id', $filter['orientation_type']);
            })
            ->when(!is_null($filter['is_publish'] ?? null), function ($query) use ($filter) {
                $query->where('orientations.is_publish', $filter['is_publish']);
            })
            ->when(!is_null($filter['is_rsvp'] ?? null), function ($query) use ($filter) {
                $query->where('orientations.is_rsvp', $filter['is_rsvp']);
            })
            ->when(!empty($request->start_date ?? null) && !empty($request->end_date ?? null), function ($query) use ($request) {
                $query->whereBetween('orientations.start_date', [Carbon::parse($request->start_date), Carbon::parse($request->end_date)]);
            });
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
            'filter' => "{}",
            'filter.faculty' => "$('#{$this->tableId}_filter_faculty').val()",
            'filter.school' => "$('#{$this->tableId}_filter_school').val()",
            'filter.programme' => "$('#{$this->tableId}_filter_programme').val()",
            'filter.intake' => "$('#{$this->tableId}_filter_intake').val()",
            'filter.level_study' => "$('#{$this->tableId}_filter_level_study').val()",
            'filter.mobility' => "$('#{$this->tableId}_filter_mobility').val()",
            'filter.orientation_type' => "$('#{$this->tableId}_filter_orientation_type').val()",
            'filter.is_rsvp' => "$('#{$this->tableId}_filter_is_rsvp').val()",
            'filter.status' => "$('#{$this->tableId}_filter_status').val()",
            'filter.is_publish' => "$('#{$this->tableId}_filter_is_publish').val()",
        ];
    }

    protected function getParameters()
    {
        return [
            'order' => [
                [6, 'desc'], [1, 'asc']
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name')->title('Activity'),
            Column::make('orientation_type.title')->name('orientationType.title')->title('Type'),
            Column::make('is_rsvp')->title('RSVP?'),
            Column::make('orientation_attendance_count')->title('Attendance'),
            Column::make('orientation_point')->title('Point')->exportable(false),
            // Column::make('point_min')->title('Point Min')->visible(false),
            // Column::make('point_max')->title('Point Max')->visible(false),
            Column::make('status_name')->title('Status'),
            Column::computed('action')->addClass('text-center')->exportable(false),
        ];
    }
}
