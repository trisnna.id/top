<?php

namespace App\DataTables\Admin\Orientation;

use App\Constants\OrientationStatusConstant;
use App\DataTables\Admin\Orientation\OrientationDt;
use App\Models\Orientation;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Column;

class ViewOrientationDt extends OrientationDt
{
    protected $checkboxId;
    protected $tableId = 'viewOrientationDt';

    public function __construct(Request $request, $auth = null)
    {
        parent::__construct($request, $auth);
        $this->tableId = 'viewOrientationDt';
        $this->checkboxId = "{$this->tableId}Cbx";
    }

    public function dataTable($query)
    {
        return $this->mainDataTable($query)
            ->editColumn('avg_rating', function ($instance) {
                if (!empty($instance->avg_rating ?? null)) {
                    return "{$instance->avg_rating}/5";
                } else {
                    return '0/5';
                }
            })
            ->editColumn('action', function ($instance) {
                $instance['show'] = [
                    'redirect' => route('admin.orientations.show2', $instance->uuid)
                ];
                $instance['clone'] = [];
                $instance['publish'] = [];
                $instance['approval'] = [];
                $instance['upsert'] = [];
                $instance['delete'] = [];
                $instance['submit'] = [];
                return $this->renderColumn('action', compact('instance'), 'components.datatables');
            });
    }

    public function query()
    {
        $request = $this->httpRequest;
        $auth = $this->auth;
        $filter = $request->filter ?? [];
        $filter['status'] = [OrientationStatusConstant::APPROVED];
        return $this->mainQuery($request, $auth, $filter);
    }

    protected function getParameters()
    {
        return [
            'order' => [
                [6, 'desc'], [1, 'asc']
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name')->title('Activity'),
            Column::make('orientation_type.title')->name('orientationType.title')->title('Type'),
            // Column::make('is_rsvp')->title('RSVP?'),
            Column::make('orientation_rsvp_count')->title('RSVPed'),
            Column::make('orientation_attendance_count')->title('Attendance'),
            // Column::computed('point')->title('Point')->exportable(false),
            // Column::make('point_min')->title('Point Min')->visible(false),
            // Column::make('point_max')->title('Point Max')->visible(false),
            Column::make('avg_rating')->title('Rating')->searchable(false),
            Column::make('status_name')->title('Status'),
            Column::computed('action')->addClass('text-center')->exportable(false),
        ];
    }
}
