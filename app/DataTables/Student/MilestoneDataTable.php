<?php

namespace App\DataTables\Student;

use App\Models\PreOrientation;
use App\Models\Student\Milestone;
use App\Repositories\Contracts\PreOrientationRepository;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MilestoneDataTable extends DataTable
{
    /**
     * Create pre-orietation repository instance
     *
     * @var PreOrientationRepository
     */
    private PreOrientationRepository $preOrientation;

    /**
     * Create a new datatable instance
     *
     * @param PreOrientationRepository $preOrientation
     */
    public function __construct(PreOrientationRepository $preOrientation)
    {
        parent::__construct();
        $this->preOrientation = $preOrientation;
    }

    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->setRowId('id')
            ->editColumn(PreOrientation::FIELD_ACTIVITY_NAME, function (PreOrientation $entity) {
                return view('student.datatable.preorientation', compact('entity'));
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns([PreOrientation::FIELD_ACTIVITY_NAME]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param   PreOrientation $model
     * @return  \Illuminate\Database\Eloquent\Builder
     */
    public function query(PreOrientation $model): QueryBuilder
    {
        return $this->preOrientation->dataTable($model, request()->all());
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('studentMilestoneTable')
                    ->columns($this->getColumns())
                    ->postAjax([
                        'url' => '',
                        'data' => "function (data) {
                            data.milestone = localStorage.getItem('fMilestone') || '1'
                            localStorage.removeItem('milestone')
                        }" 
                    ])
                    ->parameters([
                        "dom"=>"<tr><'row'<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'li><'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>>",
                    ])
                    ->ordering(false)
                    ->lengthMenu([3, 12, 24, 48, 'All'])
                    ->pageLength(12);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make(PreOrientation::FIELD_ACTIVITY_NAME)
                ->title('')
                ->searchable(true)
                ->orderable(false)
                ->width('100%')
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Pre-Orientation_' . date('YmdHis');
    }
}
