<?php

namespace App\DataTables\Settings;

use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ResourcesManagementDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query)
    {
        $data = collect([
            ['id' => 1, 'file_size' => 2, 'file_types' => ['png', 'jpg'], 'is_active' => true]
        ]);

        return datatables()
            ->of($data)
            ->addIndexColumn();
        /* return (new EloquentDataTable($query))
            ->addColumn('action', 'settings\resourcemanagement.action')
            ->setRowId('id'); */
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Settings\ResourceManagement $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query($model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('resourcesSetting')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->responsive(true)
                    ->searching(true)
                    //->dom('Bfrtip')
                    ->orderBy(2, 'asc')
                    ->parameters([
                        'drawCallback' => "function (settings) { dtx.drawCallback(settings,'resourcesSetting'); }",
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('file_size'),
            Column::make('file_types'),
            Column::make('is_active'),
            Column::computed('action')->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Settings\ResourceManagement_' . date('YmdHis');
    }
}
