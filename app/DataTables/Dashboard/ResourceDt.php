<?php

namespace App\DataTables\Dashboard;

use Illuminate\Http\Request;
use App\Models\Resource;
use App\Models\Role;
use App\Models\UserMeta;
use App\Vendors\Yajra\BaseDataTable;
use Yajra\DataTables\Html\Column;

class ResourceDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'resourceDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('name', function ($instance) {
                return view('datatables.admin.resource.resourcedt.name', compact('instance'))->render();
            })
            ->addColumn('size', function ($instance) {
                return $instance->getFirstMedia() ? $instance->getFirstMedia()->humanReadableSize : 'N/A';
            })
            ->editColumn('updated_at', function ($instance) {
                return !empty($instance->updated_at ?? null) ? $instance->updated_at->format(config('app.date_format.php').' '.config('app.time_format.php')) : null;
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    public function query()
    {
        return Resource::select([
            'resources.*',
        ])->where('resource_type_id', 2);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'dom'=>'t',
                'order' => [
                    2, 'asc'
                ],
                'responsive' => false,
                'searching' => false,
                'paging' => false,
                'bInfo' => false,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            // Column::computed('control', '')->exportable(false)->printable(false)->width(20)->addClass('control'),
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name'),
            Column::computed('size'),
            Column::make('updated_at')->title('Last Modified'),
            Column::computed('actions')->addClass('text-center'),
        ];
    }
}
