<?php

namespace App\DataTables\Dashboard;

use App\Constants\ActivityTypeConstant;
use App\Models\Activity;
use App\Vendors\Yajra\BaseDataTable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Column;

class PreEngagementDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'preEngagementDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }



    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('name', function ($instance) {
                $instance['activity'] = [
                    'swal' => [
                        'id' => 'activity',
                        'settings' => [
                            'title' => 'Activity Details',
                            'showCancelButton' => false,
                            'showConfirmButton' => false,
                            'customClass' => [
                                'popup' => "w-90",
                            ],
                        ],
                    ],
                    'data'=>[
                        'id' => $instance['uuid'],
                        'name' => $instance['name'],
                        'description' => $instance['description'],
                        'programme' => $instance->programme->code_name,
                        'event' => $instance->location . '<br>' . $instance->start_end_at_readable,
                    ]
                ];
                return view('datatables.dashboard.preengagementdt.name',compact('instance'))->render();
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    public function query()
    {
        return Activity::select([
            'activities.*',
        ])
            ->where('activities.activity_type_id', ActivityTypeConstant::PRE_ENGAGEMENT)
            ->whereDate('start_at', '<=', Carbon::now())
            ->whereDate('end_at', '>=', Carbon::now());
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    1, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                'paging' => false,
                'bInfo' => false,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center p-3'),
            Column::make('name'),
            Column::computed('actions')->addClass('text-center'),
        ];
    }
}
