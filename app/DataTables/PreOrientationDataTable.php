<?php

namespace App\DataTables;

use App\Models\Intake;
use Illuminate\Support\Str;
use App\Models\PreOrientation;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\View\Components\Badges\Localities;
use App\View\Components\Datatables\Status;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;

class PreOrientationDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->editColumn('checkbox', function ($item) {
                // return "<input type='checkbox' class='announcement-checkbox' name='bulkSelect' data-id='{$item->id}'>";
                return '
                    <label class="form-check form-check-sm form-check-custom form-check-solid form-checkbox-label">
                        <input class="form-check-input form-checkbox-input checkbox-input-single pre-orientation-checkbox" type="checkbox" name="bulkSelect" data-id="' . $item->id . '">
                    </label>
                ';
            })
            ->addIndexColumn()
            ->setRowId('id')
            ->editColumn(PreOrientation::FIELD_STATUS, function (PreOrientation $item) {
                switch ($item->status) {
                    case PreOrientation::STATUS_PUBLISHED:
                        $badge = 'success';
                        break;

                    case PreOrientation::STATUS_UNPUBLISHED:
                        $badge = 'warning';
                        break;

                    default:
                        $badge = 'secondary';
                        break;
                }

                return sprintf('<span class="badge badge-sm px-4 badge-%s">%s</span>', $badge, Str::title($item->status));
            })
            ->editColumn(PreOrientation::FIELD_CATEGORY, function (PreOrientation $item) {
                return sprintf('<span class="badge badge-sm px-4 badge-danger">%s</span>', $item->checkpoint->label);
            })
            ->editColumn(PreOrientation::FIELD_INTAKES, function (PreOrientation $item) {
                if (is_null($item->intakes['original'])) {
                    return __('All');
                }
                
                $intakesIds = Intake::where('is_active', 1)->get()->pluck('id')->toArray();
                $itemIntakes = collect($item->intakes->get('original', []))->filter(fn ($id) => in_array((int) $id, $intakesIds))->values();

                if ($itemIntakes->isEmpty() || count($intakesIds) === $itemIntakes->count()) {
                    return __('All');
                }

                $intakeNames = Intake::whereIn('id', $item->intakes['original'] ?? [])->get()->pluck('name')->toArray();
                return implode(', ', $intakeNames);
            })
            ->editColumn(PreOrientation::FIELD_LOCALITIES, function (PreOrientation $item) {
                return (new Localities($item->localities))->render();
            })
            ->editColumn('action', function (PreOrientation $item) {
                return view('admin.preorientation.action', [
                    PreOrientation::FIELD_UUID => $item->uuid,
                    'allowEdit' => $item->students()->count() === 0,
                    'isPublished' => $item->status === PreOrientation::STATUS_PUBLISHED,
                    'isUnpublished' => $item->status === PreOrientation::STATUS_UNPUBLISHED,
                ]);
            })
            ->rawColumns([
                'checkbox',
                PreOrientation::FIELD_STATUS,
                PreOrientation::FIELD_INTAKES,
                PreOrientation::FIELD_CATEGORY,
                PreOrientation::FIELD_LOCALITIES
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PreOrientation $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PreOrientation $model): QueryBuilder
    {
        return $model->dataTable()
            ->when(request()->has('status') && request()->get('status') !== 'null', function ($query) {
                $status = explode(',', request()->get('status'));
                $query->whereIn(PreOrientation::FIELD_STATUS, $status);
            })
            ->when(request()->has('checkpoint') && request()->get('checkpoint') !== 'null', function ($query) {
                $checkpoint = explode(',', request()->get('checkpoint'));
                $query->whereIn(PreOrientation::FIELD_CATEGORY, $checkpoint);
            })
            ->when(request()->has('nationality') && (request()->get('nationality') !== 'null' || !empty(request('nationality', ''))), function ($query) {
                $locality = explode(',', request()->get('nationality'));
                $query->where(function ($subquery) use ($locality) {
                    foreach ($locality as $c) {
                        if (!in_array($c, [null, '', 'null'])) {
                            $subquery->orWhereJsonContains(PreOrientation::FIELD_LOCALITIES, $c);
                        }
                    }
                });
            });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('preorientationTable')
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
            ])
            ->orderBy(1)
            ->parameters([
                'searchDelay' => 1000,
            ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        $columns = [
            // Column::computed('checkbox', '')->exportable(false)->printable(false)->width('1%')->addClass('text-center')->title('<input type="checkbox" id="selectAll" name="selectAll">')->titleAttr('Select All'),
            Column::computed('checkbox', '')->exportable(false)->printable(false)->width('1%')->addClass('text-center')->title('
                <label class="form-check form-check-sm form-check-custom form-check-solid form-checkbox-label">
                    <input class="form-check-input form-checkbox-input checkbox-input-all" id="selectAll" name="selectAll" type="checkbox">
                </label>
            ')->titleAttr('Select All'),
            Column::computed('DT_RowIndex', 'No.')->width('5%')->addClass('text-center'),
            Column::make(PreOrientation::FIELD_ACTIVITY_NAME),
            Column::make(PreOrientation::FIELD_CATEGORY)->title('Milestone'),
            Column::make(PreOrientation::FIELD_LOCALITIES)->title('Nationality'),
            Column::make(PreOrientation::FIELD_INTAKES)->title('Intake'),
            Column::make(PreOrientation::FIELD_STATUS),
            Column::computed('action')->exportable(false)->printable(false)
        ];
        if (request()->has('action') && request()->get('action') === 'pdf') {
            $columns = array_slice($columns, 0, 7);
        }
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'PreOrientation_' . date('YmdHis');
    }
}
