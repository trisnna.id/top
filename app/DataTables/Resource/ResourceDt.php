<?php

namespace App\DataTables\Resource;

use App\Constants\ResourceTypeConstant;
use App\Models\Resource;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Column;

class ResourceDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'resourceDt';
    protected $auth;
    protected $tableClass = 'fs-7 thead-none w-100 table tbody-row tr-col-md-3 td-d-block tr-mb-3 td-p-0 td-h-100';

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addIndexColumn()
            ->editColumn('title', function ($instance) {
                $instance['show'] = [
                    'redirect' => route('resources.show', $instance->uuid)
                ];
                return $this->renderColumn('title', compact('instance'));
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['title']);
    }

    public function query()
    {
        return Resource::query()
            ->select([
                'resources.*',
            ])
            ->where('is_active', true)
            ->whereIn('resource_type_id', [ResourceTypeConstant::ALL, ResourceTypeConstant::RESOURCE]);
    }

    protected function getParameters()
    {
        return [
            'ordering' => false,
            'lengthMenu' => [
                [4, 12, 24, 48, -1],
                [4, 12, 24, 48, 'All'],
            ],
            "pageLength" => 12,
            'responsive' => false,
            'searching' => true,
            "dom"=>"<tr><'row'<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'li><'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>>",
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    protected function getColumns()
    {
        return [
            Column::make('title')->title('Title'),
        ];
    }
}
