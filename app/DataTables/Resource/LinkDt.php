<?php

namespace App\DataTables\Resource;

use App\Models\Resource;
use App\Vendors\Yajra\BaseDataTable;
use Illuminate\Http\Request;
use Yajra\DataTables\CollectionDataTable;
use Yajra\DataTables\Html\Column;

class LinkDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'linkDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable()
    {
        $dt = (new CollectionDataTable($this->collection()))
            ->addIndexColumn()
            ->editColumn('value', function ($instance) {
                return "<a href=\"{$instance['url']}}\" target=\"_blank\">View Source</a>";
            });
        
        if (auth()->user()->hasRole('admin')) {
            $dt = $dt->editColumn('action', function ($resource) {
                return view('components.datatables.action', [
                    'instance' => [
                        'upsert' => [
                            'data' => [],
                            'swal' => [
                                'id' => "resourceLink",
                                'settings' => [
                                    'title' => 'Edit Links',
                                ],
                                'axios' => [    
                                    'url' => '',
                                ]
                            ]
                        ],
                        'delete' => [
                            'swal' => [
                                'id' => "swal-blank",
                                'settings' => [
                                    'icon' => 'warning',
                                    'title' => 'Are you sure to DELETE?',
                                    'html' => "You won't be able to revert this!",
                                    'confirmButtonText' => 'Confirm',
                                ],
                                'axios' => [
                                    'url' => '#',
                                    'method' => 'delete'
                                ]
                            ]
                        ],
                        'status' => [
                            'swal' => [
                                'id' => "swal-blank",
                                'settings' => [
                                    'icon' => 'warning',
                                    'title' => "Are you sure to set this as {$resource['is_active']}?",
                                    'html' => "You can change it back later!",
                                    'confirmButtonText' => 'Confirm',
                                ],
                                'axios' => [
                                    'url' => '#',
                                    'method' => 'put'
                                ]
                            ]
                        ],
                    ]
                ]);
            });
        }
        
        return $dt->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['value', 'action']);
    }

    public function collection()
    {
        // fake()->text
        $dummies = [];
        for ($i = 0; $i < 5; $i++) {
            array_push($dummies,[
                'name' => fake()->sentence,
                'description' => fake()->text,
                'url' => fake()->url,
                'is_active' => ['Active', 'Inactive'][rand(0, 1)]
            ]);
        }
        return collect($dummies);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'order' => [
                    2, 'asc'
                ],
                'responsive' => true,
                'searching' => true,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        if (auth()->user()->hasRole('admin')) {
            return [
                Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
                Column::make('name')->title('Title'),
                Column::make('description'),
                Column::make('value')->title('Source'),
                Column::computed('action')->addClass('text-center'),
            ];
        }
        return [
            Column::computed('DT_RowIndex', 'No.')->width(30)->addClass('text-center'),
            Column::make('name')->title('Title'),
            Column::make('description'),
            Column::make('value')->title('Source'),
        ];
    }
}
