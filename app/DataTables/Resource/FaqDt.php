<?php

namespace App\DataTables\Resource;

use Illuminate\Http\Request;
use App\Models\Resource;
use App\Vendors\Yajra\BaseDataTable;
use Yajra\DataTables\Html\Column;

class FaqDt extends BaseDataTable
{
    protected $checkboxId;
    protected $httpRequest;
    protected $tableId = 'faqDt';
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->checkboxId = "{$this->tableId}Cbx";
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('name', function ($instance) {
                return view('datatables.admin.resource.faqdt.name', compact('instance'))->render();
            })
            ->only(array_column($this->getColumns(), 'data'))
            ->rawColumns(['name']);
    }

    public function query()
    {
        return Resource::select([
            'resources.*',
        ])->where('resource_type_id',1);
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters([
                'dom'=>'t',
                'orderable' => false,
                'responsive' => false,
                'searching' => false,
                'paging' => false,
                'bInfo' => false,
                // 'initComplete' => "function (settings, json) { dtx.initComplete(settings, json,'{$this->tableId}'); }",
                // 'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::make('title')->title(''),
        ];
    }
}
