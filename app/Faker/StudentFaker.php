<?php

namespace App\Faker;

use Faker\Provider\Base;

class StudentFaker extends Base
{
    protected static $gender = [
        'Male',
        'Female',
    ];

    protected static $campus = [
        'TU',
        'TC',
    ];

    protected static $programme = array(
        array(
            "code" => "18002",
            "name" => "Bachelor of Arts (Honours) Accounting and Finance",
        ),
        array(
            "code" => "24003",
            "name" => "Bachelor of Science (Honours) Actuarial Studies",
        ),
        array(
            "code" => "24004",
            "name" => "Bachelor of Science (Honours) Actuarial Studies",
        ),
        array(
            "code" => "33001",
            "name" => "Bachelor of Medicine, Bachelor of Surgery - MBBS",
        ),
        array(
            "code" => "34003",
            "name" => "Bachelor of Business (Honours) (International Business and Marketing)",
        ),
        array(
            "code" => "34005",
            "name" => "Bachelor of Business (Honours) Banking and Finance",
        ),
        array(
            "code" => "34006",
            "name" => "Bachelor of Business (Honours) Finance and Economics",
        ),
        array(
            "code" => "91601",
            "name" => "Bachelor of Mass Communication (Honours)",
        ),
        array(
            "code" => "91602",
            "name" => "Bachelor of Mass Communication (Honours) (Advertising)",
        ),
        array(
            "code" => "91603",
            "name" => "Bachelor of Mass Communication (Honours) (Broadcasting)",
        ),
        array(
            "code" => "91604",
            "name" => "Bachelor of Mass Communication (Honours) (Public Relations and Event Management)",
        ),
        array(
            "code" => "91605",
            "name" => "Bachelor of Mass Communication (Honours) (Public Relations and Marketing)",
        ),
        array(
            "code" => "91606",
            "name" => "Bachelor of Education (Honours)",
        ),
        array(
            "code" => "91607",
            "name" => "Bachelor of Psychology (Honours)",
        ),
        array(
            "code" => "91608",
            "name" => "Bachelor of Performing Arts (Honours)",
        ),
        array(
            "code" => "91609",
            "name" => "Bachelor of Mass Communication (Honours) (Advertising and Brand Management)",
        ),
        array(
            "code" => "91610",
            "name" => "Bachelor of Mass Communication (Honours) (Digital Media Production)",
        ),
        array(
            "code" => "91611",
            "name" => "Bachelor of Social Science with Honours (International Relations)",
        ),
        array(
            "code" => "91612",
            "name" => "Bachelor of Social Science with Honours (Social Innovation and Change)",
        ),
        array(
            "code" => "91613",
            "name" => "Bachelor of Social Science (Honours) in Social Innovation and Change",
        ),
        array(
            "code" => "91614",
            "name" => "Bachelor of Social Science (Honours) in International Relations",
        ),
        array(
            "code" => "91615",
            "name" => "Bachelor of Mass Communication (Honours) in Public Relations and Event Management",
        ),
        array(
            "code" => "91616",
            "name" => "Bachelor of Mass Communication (Honours) in Public Relations and Marketing",
        ),
        array(
            "code" => "91617",
            "name" => "Bachelor of Mass Communication (Honours) in Advertising and Brand Management",
        ),
        array(
            "code" => "91618",
            "name" => "Bachelor of Science (Honours) in Culinology",
        ),
        array(
            "code" => "91619",
            "name" => "Bachelor of Mass Communication (Honours)",
        ),
        array(
            "code" => "91620",
            "name" => "Bachelor of Mass Communication (Honours) (Digital Media Production)",
        ),
        array(
            "code" => "92601",
            "name" => "Bachelor of Science (Honours) in Architecture",
        ),
        array(
            "code" => "92602",
            "name" => "Bachelor of Quantity Surveying (Honours)",
        ),
        array(
            "code" => "92603",
            "name" => "Bachelor of Computer Science (Honours)",
        ),
        array(
            "code" => "92604",
            "name" => "Bachelor of Information Technology (Honours)",
        ),
        array(
            "code" => "92605",
            "name" => "Bachelor of Software Engineering (Honours)",
        ),
        array(
            "code" => "92606",
            "name" => "Bachelor of Engineering (Honours) Mechanical Engineering",
        ),
        array(
            "code" => "92607",
            "name" => "Bachelor of Engineering (Honours) Chemical Engineering",
        ),
        array(
            "code" => "92608",
            "name" => "Bachelor of Engineering (Honours) Electrical and Electronic Engineering",
        ),
        array(
            "code" => "92610",
            "name" => "Bachelor of Design (Honours) in Creative Media",
        ),
        array(
            "code" => "92611",
            "name" => "Bachelor of Arts (Honours) in Interior Architecture",
        ),
        array(
            "code" => "92612",
            "name" => "Bachelor in Fashion Design Technology (Honours)",
        ),
        array(
            "code" => "92613",
            "name" => "Bachelor of Science (Honours) in Robotic Design and Development",
        ),
        array(
            "code" => "92614",
            "name" => "Bachelor of Chemical Engineering with Honours",
        ),
        array(
            "code" => "92615",
            "name" => "Bachelor of Mechanical Engineering with Honours",
        ),
        array(
            "code" => "92616",
            "name" => "Bachelor of Electrical and Electronic Engineering with Honours",
        ),
        array(
            "code" => "92617",
            "name" => "Bachelor of Fashion Design Technology (Honours)",
        ),
        array(
            "code" => "92618",
            "name" => "Bachelor of Robotic Design and Development (Honours)",
        ),
        array(
            "code" => "93601",
            "name" => "Bachelor of Business (Honours)",
        ),
        array(
            "code" => "93602",
            "name" => "Bachelor in Entrepreneurship (Team Entrepreneurship) (Honours)",
        ),
        array(
            "code" => "93605",
            "name" => "Bachelor of Laws",
        ),
        array(
            "code" => "93606",
            "name" => "Bachelor in Accounting (Fintech) (Honours)",
        ),
        array(
            "code" => "93607",
            "name" => "Bachelor of Entrepreneurship (Honours) in Team Entrepreneurship",
        ),
        array(
            "code" => "93608",
            "name" => "Bachelor of Laws (Honours)",
        ),
        array(
            "code" => "94601",
            "name" => "Bachelor of Culinary Management (Honours)",
        ),
        array(
            "code" => "94602",
            "name" => "Bachelor of Science (Honours) (Culinology)",
        ),
        array(
            "code" => "94603",
            "name" => "Bachelor of International Hospitality Management (Honours)",
        ),
        array(
            "code" => "94604",
            "name" => "Bachelor of International Tourism Management (Honours)",
        ),
        array(
            "code" => "94605",
            "name" => "Bachelor of International Tourism Management (Honours) (Events Management)",
        ),
        array(
            "code" => "94607",
            "name" => "Bachelor of Patisserie Arts (Honours)",
        ),
        array(
            "code" => "94608",
            "name" => "Bachelor of Food Science (Honours)",
        ),
        array(
            "code" => "95601",
            "name" => "Bachelor of Biotechnology (Honours)",
        ),
        array(
            "code" => "95602",
            "name" => "Bachelor of Biomedical Science (Honours)",
        ),
        array(
            "code" => "95603",
            "name" => "Bachelor of Science (Honours) (Food Science)",
        ),
        array(
            "code" => "95604",
            "name" => "Bachelor of Pharmacy (Honours)",
        ),
    );

    protected static $faculty = [
        [
            'code' => 'FSLM',
            'name' => 'Faculty of Social Sciences & Leisure Management',
        ],
        [
            'code' => 'FBL',
            'name' => 'Faculty of Business & Law',
        ],
        [
            'code' => 'FHMS',
            'name' => 'Faculty of Health & Medical Sciences',
        ],
    ];

    protected static $school = [
        [
            'code' => 'ADP',
            'name' => 'School of Liberal Arts and Sciences',
        ],
        [
            'code' => 'SBS',
            'name' => 'School of Biosciences',
        ],
        [
            'code' => 'TBS',
            'name' => 'Taylor\'s Business School',
        ],
    ];

    protected static $levelOfStudy = [
        'Postgrad',
        'Bachelor',
    ];

    protected static $courseStatus = [
        'Active',
    ];

    protected static $intakeNumber = [
        '202103',
    ];

    protected static $studyMode = [
        'Full Time',
    ];

    protected static $country = [
        [
            'country_from' => 'MALAYSIA',
            'nationality' => 'MALAYSIA',
            'locality' => 'Local',
        ],
        [
            'country_from' => 'INDONESIA',
            'nationality' => 'INDONESIA',
            'locality' => 'International',
        ],
    ];

    protected static $mobility = [
        'Exchange Student',
        'Study Abroad Student',
    ];

    public function studentGender(): string
    {
        return static::randomElement(static::$gender);
    }

    public function studentCampus(): string
    {
        return static::randomElement(static::$campus);
    }

    public function studentProgramme()
    {
        return static::randomElement(static::$programme);
    }

    public function studentFaculty()
    {
        return static::randomElement(static::$faculty);
    }

    public function studentSchool()
    {
        return static::randomElement(static::$school);
    }

    public function studentLevelOfStudy()
    {
        return static::randomElement(static::$levelOfStudy);
    }

    public function studentCourseStatus()
    {
        return static::randomElement(static::$courseStatus);
    }

    public function studentIntakeNumber(): string
    {
        return static::randomElement(static::$intakeNumber);
    }

    public function studentStudyMode(): string
    {
        return static::randomElement(static::$studyMode);
    }

    public function studentCountry(): array
    {
        return static::randomElement(static::$country);
    }

    public function studentMobility()
    {
        return static::randomElement(static::$mobility);
    }
}
