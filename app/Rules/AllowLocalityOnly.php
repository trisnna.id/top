<?php

namespace App\Rules;

use App\Constants\NationalityConstant;
use Illuminate\Contracts\Validation\Rule;

class AllowLocalityOnly implements Rule
{
    private $nationality;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($nationality = null)
    {
        if (!($nationality instanceof NationalityConstant)) {
            $this->nationality = nationality();
        } else {
            $this->nationality = $nationality;
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->nationality->isValid($value ?? []);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.exists', ['attribute', 'nationality']);
    }
}
