<?php

namespace App\Console;

use App\Jobs\Orientation\FixDataTotalStudentJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('update:pre-orientation')
            ->dailyAt('00:00')
            ->withoutOverlapping();
        $schedule->command('top:sync-cms-to-definition-student-table')
            ->dailyAt('00:05')
            ->withoutOverlapping();
        // fix total student for orientation for additional student coming from cms
        $schedule->job(new FixDataTotalStudentJob)
            ->dailyAt('01:05')
            ->withoutOverlapping();
        $schedule->command('top:generate-timetable')
            ->dailyAt('02:00')
            ->withoutOverlapping();
        $schedule->command('top:update-active-student-point')
            ->dailyAt('01:05')
            ->withoutOverlapping();
        $schedule->command('top:update-active-student-point')
            ->dailyAt('14:05')
            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
