<?php

namespace App\Console\Commands;

use App\Services\Admin\StudentService;
use App\Services\Integration\Taylors\CmsService;
use Illuminate\Console\Command;

class SyncCmsToStudentTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'top:sync-cms-to-student-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync cms student view to student view table and sync student view to student table.';

    /**
     * Create a new command instance.
     * 
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (CmsService::syncStudents()) {
            (new StudentService())->syncStudentViews();
            $this->info('Sync successfully');
        } else {
            $this->info('Something went wrong');
        }
    }
}
