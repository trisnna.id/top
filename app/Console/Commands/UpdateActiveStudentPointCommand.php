<?php

namespace App\Console\Commands;

use App\Services\Admin\StudentPointService;
use Illuminate\Console\Command;

class UpdateActiveStudentPointCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'top:update-active-student-point';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update point for active student by intake.';

    /**
     * Create a new command instance.
     * 
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        (new StudentPointService())->updateActiveStudentPoint();
        $this->info('Sync successfully');
    }
}
