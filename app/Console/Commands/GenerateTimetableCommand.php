<?php

namespace App\Console\Commands;

use App\Jobs\Orientation\GenerateTimetableJob;
use App\Models\Intake;
use App\Models\Student;
use App\Services\OrientationService;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class GenerateTimetableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'top:generate-timetable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate student doest not have timetable.';

    /**
     * Create a new command instance.
     * 
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $activeIntakeIds = Intake::where('is_active', true)->pluck('id')->toArray();
        $inActiveIntakeIds = Intake::where('is_active', false)->pluck('id')->toArray();
        $inActiveStudents = Student::whereHas('media', function ($query) {
            $query->where('collection_name', 'orientation-timetable');
        })->whereIn('intake_id', $inActiveIntakeIds)->get();
        foreach ($inActiveStudents as $inActiveStudent) {
            $medias = $inActiveStudent->getMedia('orientation-timetable');
            foreach ($medias as $media) {
                $media->delete();
            }
        }
        $students = Student::whereHas('media', function ($query) {
            $query->where('collection_name', 'orientation-timetable');
        }, 0)->whereIn('intake_id', $activeIntakeIds)->get();
        foreach ($students as $student) {
            $request = new Request();
            $request->request->add(['student_id' => $student->uuid]);
            GenerateTimetableJob::dispatch($request, null, $student);
            // (new OrientationService)->generateTimetable($request, null, $student);
        }
    }
}
