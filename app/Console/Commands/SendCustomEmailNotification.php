<?php

namespace App\Console\Commands;

use App\Models\Student;
use App\Notifications\CustomEmailNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class SendCustomEmailNotification extends Command
{
    /**
     * The name and signature of the console command.
     * view = 'notification.custom.20230720'
     * @var string
     */
    protected $signature = 'top:send-custom-email-notification {--type= : Type} {--email= : Email} {--view= : Template} {--mailer= : Mailer}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send custom email notification';

    /**
     * Create a new command instance.
     * 
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $type = (string) $this->option('type') ?? null;
        $email = (string) $this->option('email') ?? null;
        $view = (string) $this->option('view') ?? null;
        $mailer = (string) $this->option('mailer') ?? 'smtp';
        if (empty($view)) {
            $this->output->error('Please input the template view');
            return;
        }
        if (empty($type)) {
            $this->output->error('Please input the type');
            return;
        } else {
            if ($type == 'student') {
                $ids = $this->getTemplateStudentID($view);
                $taylorsStudents = Student::select(['name', 'taylors_email as email'])
                    ->whereIn('id_number', $ids)
                    ->where('taylors_email', '<>', '')
                    ->get()
                    ->toArray();
                $personalStudents = Student::select(['name', 'personal_email as email'])
                    ->whereIn('id_number', $ids)
                    ->where('personal_email', '<>', '')
                    ->get()
                    ->toArray();
                $students = array_merge($taylorsStudents, $personalStudents);
                $this->emailFilterExplodePush('/', $students);
                $this->emailFilterExplodePush(';', $students);
                $this->emailFilterExplodePush(',', $students);
                $emails = $this->chunkByMailers($students);
                foreach ($emails as $key => $delayEmails) {
                    if ($key === 0) {
                        $delay = null;
                    } else {
                        $delay = Carbon::now()->addDays($key);
                    }
                    foreach ($delayEmails as $delayEmail) {
                        foreach ($delayEmail as $email) {
                            $user = (object)$email;
                            Notification::send($user, new CustomEmailNotification($user, $view, $email['mailer'], $delay));
                            unset($user);
                        }
                    }
                }
            } elseif ($type == 'manual') {
                if (empty($email)) {
                    return $this->output->error('Please input the email');
                } else {
                    foreach (explode(',', $email) as $value) {
                        $user = (object)[
                            'email' => $value,
                            'name' => $value
                        ];
                        Notification::send($user, new CustomEmailNotification($user, $view, $mailer));
                        unset($user);
                    }
                }
            } else {
                $this->output->error('The type not match');
                return;
            }
        }
    }

    protected function emailFilterExplodePush($separator, &$array)
    {
        collect($array)->filter(function ($value, $key) use (&$array, $separator) {
            if (Str::contains($value['email'], $separator)) {
                unset($array[$key]);
                return true;
            } else {
                return false;
            };
        })->each(function ($value) use (&$array, $separator) {
            $emails = explode($separator, $value['email']);
            foreach ($emails as $email) {
                if (!empty(trim($email) ?? null)) {
                    array_push($array, [
                        'name' => trim($value['name']),
                        'email' => trim($email),
                    ]);
                }
            }
        });
        $array = array_filter($array);
        $array = array_values($array);
        $array = array_filter($array, function ($value) {
            return !empty($value['email'] ?? null);
        });
    }

    protected function chunkByMailers(array $emails)
    {
        $mailers = [
            config("mail.mailers.smtp10"),
            config("mail.mailers.smtp9"),
            config("mail.mailers.smtp8"),
            config("mail.mailers.smtp7"),
            config("mail.mailers.smtp6"),
            config("mail.mailers.smtp5"),
            config("mail.mailers.smtp4"),
            config("mail.mailers.smtp3"),
            config("mail.mailers.smtp2"),
            config("mail.mailers.smtp"),
        ];
        $countEmails = collect($emails)->count();
        $sumLimitPersonals = collect($mailers)->sum('limit.personal');
        $limitPersonals = collect($mailers)->pluck('limit.personal');
        $mailers = collect($mailers)->pluck('mailer');
        $mergeLimitPersonals = collect();
        $mergeMailers = collect();
        for ($i = 0; $i < ceil($countEmails / $sumLimitPersonals); $i++) {
            $mergeLimitPersonals = $mergeLimitPersonals->merge($limitPersonals);
            $mergeMailers = $mergeMailers->merge($mailers);
        }
        $valuePattern = 0;
        foreach ($mergeLimitPersonals as $key => $value) {
            $valuePattern += $value;
            $patternLimitPersonals[$key] = $valuePattern;
            unset($key);
        }
        $chunkEmails = collect($emails)->chunk($sumLimitPersonals);
        $chunkEmailsByMailer = [];
        foreach ($chunkEmails as $key => $chunkEmail) {
            $chunkEmailsByMailer[$key] = $chunkEmail->split(count($mailers))->toArray();
            foreach ($chunkEmailsByMailer[$key] as $key2 => $value) {
                $chunkEmailsByMailer[$key][$key2] = collect($value)->map(function ($instance) use ($mailers, $key2) {
                    $instance['mailer'] = $mailers[$key2];
                    return $instance;
                })->toArray();
            }
            unset($key);
        }
        return $chunkEmailsByMailer;
    }

    /**
     * Extract the student id from excel using tools
     * https://tableconvert.com/excel-to-json
     */
    protected function getTemplateStudentID($template)
    {
        $ids = [];
        if ($template == 'notification.custom.20230720') {
            $ids = [];
        } elseif ($template == 'notification.custom.20230725-get') {
            $ids = [];
        } elseif ($template == 'notification.custom.20230725-discover') {
            $ids = [];
        } elseif ($template == 'notification.custom.20230801') {
            $ids = [];
        } elseif ($template == 'notification.custom.20230919') {
            $ids = [];
        } elseif ($template == 'notification.custom.20230818') {
            $ids = [];
        } elseif ($template == 'notification.custom.20230804') {
            $ids = [];
        } elseif ($template == 'notification.custom.20230922') {
            $ids = [];
        } elseif ($template == 'notification.custom.20230905') {
            $ids = [];
        } elseif ($template == 'notification.custom.20230927') {
            $ids = [];
        } elseif ($template == 'notification.custom.20231108') {
            $ids = [];
        } elseif ($template == 'notification.custom.20231109') {
            $ids = [];
        } elseif ($template == 'notification.custom.20231114') {
            $ids = ["0370080", "0368359", "0369423", "0365586", "0369831", "0367022", "0368538", "0370042", "0370112", "0370329", "0370102", "0369569", "0370101", "0367597", "0369527", "0347167", "0370160", "0369981", "0368069", "0368179", "0368680", "0368446", "0369851", "0368592", "0368591", "0370322", "0369871", "0367573", "0368198", "0369306", "0368841", "0369686", "0368811", "0369319", "0365516", "0370178", "0369926", "0367519", "0369848", "0366568", "0367764", "0368187", "0366994", "0369882", "0367640", "0370082", "0368470", "0369968", "0368624", "0368256", "0366995", "0369331", "0369317", "0369316", "0369939", "0368922", "0369541", "0365535", "0369740", "0364217", "0369625", "0369980", "0368404", "0364576", "0369730", "0369076", "0367666", "0368395", "0368674", "0367771", "0367607", "0368898", "0367459", "0364950", "0369178", "0368248", "0370088", "0370074", "0368338", "0369400", "0367253", "0369690", "0370288", "0367936", "0367753", "0370199", "0370094", "0363627", "0369687", "0369700", "0368401", "0370083", "0369746", "0369744", "0369235", "0368042", "0365036", "0369168", "0367553", "0369967", "0369748", "0369119", "0369361", "0369870", "0370095", "0368699", "0367346", "0368368", "0367912", "0370085", "0368370", "0369850", "0367646", "0368833", "0369768", "0369445", "0370060", "0370318", "0367388", "0369426", "0369560", "0368103", "0370084", "0367906", "0367449", "0370048", "0364577", "0368218", "0369544", "0370089", "0366666", "0369954", "0365042", "0370099", "0370077", "0370103", "0368813", "0367637", "0369982", "0370272", "0370144", "0365714", "0368599", "0369742", "0369262", "0367803", "0363539", "0369429", "0369644", "0368866", "0369874", "0370100", "0370273", "0365612", "0370067", "0369586", "0367988", "0369983", "0367951", "0370287", "0370003", "0369382", "0369645", "0368214", "0369562", "0368326", "0370087", "0369734", "0370081", "0369941", "0367910", "0368920", "0366410", "0364611", "0368344", "0367880", "0367419", "0370107", "0368908", "0369509"];
        }
        return $ids;
    }
}
