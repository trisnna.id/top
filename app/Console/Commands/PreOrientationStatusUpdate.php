<?php

namespace App\Console\Commands;

use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Console\Command;
use App\Repositories\Contracts\PreOrientationRepository;

class PreOrientationStatusUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:pre-orientation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update status of pre-orientation if effective date is passed';

    /**
     * Create a new pre-orientation repository instance
     *
     * @var     PreOrientationRepository
     */
    private PreOrientationRepository $preOrientation;

    /**
     * Create a new command instance.
     * 
     * @param   PreOrientationRepository    $preOrientation
     * @return  void
     */
    public function __construct(PreOrientationRepository $preOrientation)
    {
        parent::__construct();
        $this->preOrientation = $preOrientation;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $entity = $this->preOrientation->findWhereDraft();

        $now = now()->format('Y-m-d H:i:s');

        // get pre-orientation record where effective date is passed
        $entity = $entity->filter(function ($item) use ($now) {
            return $item->effective_date <= $now && !is_null($item->effective_date);
        });

        // update status of pre-orientation to publish
        $entity->each(function ($item) {
            $this->preOrientation->updateByUUID($item->uuid, [PreOrientationEntity::FIELD_STATUS => PreOrientationEntity::STATUS_PUBLISHED]);
        });

        // show message
        $this->info('Pre-orientation status updated successfully');
    }
}
