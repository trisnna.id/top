<?php

namespace App\Console\Commands;

use App\Services\Admin\OrientationFilterService;
use App\Services\Admin\StudentService;
use Illuminate\Console\Command;

class FixDummyStudentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'top:fix-dummy-student';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix data for dummy student.';

    /**
     * Create a new command instance.
     * 
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        (new StudentService())->fixDummyStudent();
        $this->info('Fix successfully');
    }
}
