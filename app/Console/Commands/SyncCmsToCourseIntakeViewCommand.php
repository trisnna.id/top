<?php

namespace App\Console\Commands;

use App\Services\Integration\Taylors\CmsService;
use Illuminate\Console\Command;

class SyncCmsToCourseIntakeViewCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'top:sync-cms-to-course-intake-view';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync cms course intake view to course intake view table.';

    /**
     * Create a new command instance.
     * 
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (CmsService::syncCourseIntakes()) {
            $this->info('Sync CMS successfully');
        } else {
            $this->info('Something went wrong');
        }
    }
}
