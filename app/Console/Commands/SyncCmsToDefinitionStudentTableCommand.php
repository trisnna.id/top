<?php

namespace App\Console\Commands;

use App\Services\Admin\CampusService;
use App\Services\Admin\FacultyService;
use App\Services\Admin\IntakeService;
use App\Services\Admin\LevelStudyService;
use App\Services\Admin\MobilityService;
use App\Services\Admin\ProgrammeService;
use App\Services\Admin\SchoolService;
use App\Services\Admin\StudentService;
use App\Services\Integration\Taylors\CmsService;
use Illuminate\Console\Command;

class SyncCmsToDefinitionStudentTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'top:sync-cms-to-definition-student-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync cms view to definition and student table.';

    /**
     * Create a new command instance.
     * 
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (CmsService::syncStudents()) {
            (new StudentService())->syncStudentViews();
            $this->info('Sync successfully');
        } else {
            $this->info('Something went wrong');
        }
        if (CmsService::syncCourseIntakes()) {
            (new CampusService())->syncCourseIntakeView();
            (new FacultyService())->syncCourseIntakeView();
            (new IntakeService())->syncCourseIntakeView();
            (new LevelStudyService())->syncCourseIntakeView();
            (new MobilityService())->syncCourseIntakeView();
            (new ProgrammeService())->syncCourseIntakeView();
            (new SchoolService())->syncCourseIntakeView();
            $this->info('Sync successfully');
        } else {
            $this->info('Something went wrong');
        }
    }
}
