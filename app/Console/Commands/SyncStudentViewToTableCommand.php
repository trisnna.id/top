<?php

namespace App\Console\Commands;

use App\Services\Admin\StudentService;
use Illuminate\Console\Command;

class SyncStudentViewToTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'top:sync-student-view-to-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync student view to student table.';

    /**
     * Create a new command instance.
     * 
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        (new StudentService())->syncStudentViews();
        $this->info('Sync successfully');
    }
}
