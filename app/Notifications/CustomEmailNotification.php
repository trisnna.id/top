<?php

namespace App\Notifications;

use App\Channels\MailChannel;
use App\Channels\MailLogChannel;
use App\Mail\NotificationMail;
use App\Mail\NotificationMailAttachment;
use App\Notifications\BaseNotification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class CustomEmailNotification extends BaseNotification
{

    public $channel = [];
    public $mailer;
    public $template;
    public $view;

    public function __construct($notifiable, $template, $mailer = 'smtp', Carbon $delay = null)
    {
        $this->template = $template;
        parent::__construct($notifiable, $mailer, $delay);
    }

    public function via($notifiable)
    {
        return [MailChannel::class, MailLogChannel::class];
    }

    public function getStub()
    {
        $stub = view($this->template)->renderSections();
        $subject = trim($stub['subject']);
        $content = trim($stub['content']);
        $this->stub = [
            'subject' => $subject,
            'content' => $content,
        ];
    }

    public function toMail($notifiable)
    {
        $subject = $this->parse(trim($this->stub['subject']), $notifiable);
        $content = $this->parse(trim($this->stub['content']), $notifiable);
        $attachments = $this->getTemplateAttachments($this->template);
        return Mail::mailer($this->mailer)
            ->to($this->notifiable->email)
            ->send(new NotificationMailAttachment($subject, $content, $this->mailer, $attachments));
    }

    public function arrayReplacements($notifiable = null): array
    {
        return [];
    }

    protected function getTemplateAttachments($template): array
    {
        $attachments = [];
        if ($template == 'notification.custom.20230720') {
            $attachments = [public_path('media/files/How to add TOP to Home Screen.pdf')];
        }elseif ($template == 'notification.custom.20231108') {
            $attachments = [public_path('media/files/How to add TOP to Home Screen.pdf')];
        }
        return $attachments;
    }
}
