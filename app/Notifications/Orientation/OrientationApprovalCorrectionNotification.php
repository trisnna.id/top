<?php

namespace App\Notifications\Orientation;

use App\Models\Orientation;
use App\Notifications\BaseNotification;

class OrientationApprovalCorrectionNotification extends BaseNotification
{

    public $channel = [];
    public $mailer;
    public $template = 'orientation-approval-correction';
    public $orientation;

    public function __construct($notifiable = null, Orientation $orientation = null)
    {
        parent::__construct($notifiable);
        $this->orientation = $orientation;
    }

    public function arrayReplacements($notifiable = null): array
    {
        return [
            'userName' => [
                'data' => $this->notifiable->name ?? null,
                'description' => 'User Name',
                'example' => 'Muhamad Ali',
            ],
            'model_type' => [
                'data' => !empty($this->orientation) ? $this->orientation::class : null,
                'description' => 'Class Name',
                'example' => 'Class Name',
            ],
            'model_id' => [
                'data' => $this->orientation->id,
                'description' => 'ID',
                'example' => 'ID',
            ],
            'orientationName' => [
                'data' => $this->orientation->name ?? null,
                'description' => 'Orientation Activity',
                'example' => 'Activity 1',
            ],
            'orientationUrl' => [
                'data' => !empty($this->orientation) ? route('admin.orientations.approvals.edit', $this->orientation->uuid) : '#',
                'description' => 'Orientation Activity Url Approval',
                'example' => '#',
            ],
            'orientationCreatedBy' => [
                'data' => $this->orientation->createdByUser->name ?? null,
                'description' => 'Orientation Created By User',
                'example' => 'Ali',
            ],
        ];
    }
}
