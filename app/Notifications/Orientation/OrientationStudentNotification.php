<?php

namespace App\Notifications\Orientation;

use App\Models\Orientation;
use App\Notifications\BaseNotification;

class OrientationStudentNotification extends BaseNotification
{

    public $channel = [];
    public $mailer;
    public $template = 'orientation-student';
    public $orientation;

    public function __construct($notifiable = null, Orientation $orientation = null)
    {
        parent::__construct($notifiable);
        $this->orientation = $orientation;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function arrayReplacements($notifiable = null): array
    {
        return [
            'receiver' => [
                'data' => $this->notifiable->name ?? null,
                'description' => 'Receiver',
                'example' => 'Muhamad Ali',
            ],
            'link' => [
                'data' => !empty($this->orientation) ? route('orientations.index') : '#',
                'description' => 'URL redirect the user',
                'example' => '#',
            ],
            'modelType' => [
                'data' => !empty($this->orientation) ? $this->orientation::class : null,
                'description' => 'Class Name',
                'example' => 'Class Name',
            ],
            'modelId' => [
                'data' => $this->orientation->id,
                'description' => 'ID',
                'example' => 'ID',
            ],
            'orientationName' => [
                'data' => $this->orientation->name ?? null,
                'description' => 'Orientation Activity',
                'example' => 'Activity 1',
            ],
            'orientationCreatedBy' => [
                'data' => $this->orientation->createdByUser->name ?? null,
                'description' => 'Orientation Created By User',
                'example' => 'Ali',
            ],
        ];
    }
}
