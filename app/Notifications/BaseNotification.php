<?php

namespace App\Notifications;

use App\Channels\MailLogChannel;
use App\Channels\MailChannel;
use App\Mail\NotificationMail;
use App\Models\Mail as MailModel;
use App\Models\MailLog;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Mail;

class BaseNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $stub = [];
    public $notifiable;
    public $template = '';
    public $mailer;

    public function __construct($notifiable = null, $mailer = 'smtp', Carbon $delay = null)
    {
        $this->setNotifiable($notifiable);
        $this->getStub();
        $this->mailer = $mailer;
        if (!empty($delay ?? null)) {
            $this->delay($delay);
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if (config('mail.enabled')) {
            return [MailChannel::class, 'database', MailLogChannel::class];
        } else {
            return ['database'];
        }
    }

    public function setNotifiable($notifiable)
    {
        $this->notifiable = $notifiable;
    }

    public function getStub()
    {
        $mail = MailModel::where('name', $this->template)->first();
        $this->stub = [
            'subject' => $mail->subject,
            'content' => json_decode($mail->content),
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return collect($this->arrayReplacements($notifiable))->map(function ($instance) {
            return $instance['data'];
        })->toArray();
    }

    public function toMailLog($notifiable)
    {
        $subject = $this->parse(trim($this->stub['subject']), $notifiable);
        $content = $this->parse(trim($this->stub['content']), $notifiable);
        $fromAddress = config("mail.mailers.{$this->mailer}.from.address") ?? null;
        $fromName = config("mail.mailers.{$this->mailer}.from.name") ?? null;
        return MailLog::create([
            'from_name' => $fromName,
            'from_address' => $fromAddress,
            'to' => $this->notifiable->email,
            'subject' => $subject,
            'body' => $content
        ]);
    }

    public function toMail($notifiable)
    {
        $subject = $this->parse(trim($this->stub['subject']), $notifiable);
        $content = $this->parse(trim($this->stub['content']), $notifiable);
        return Mail::mailer($this->mailer)
            ->to($this->notifiable->email)
            ->send(new NotificationMail($subject, $content, $this->mailer));
    }

    public function parse($stub, $notifiable)
    {
        $replace = $this->replacements($notifiable);
        return str_replace(array_keys($replace), array_values($replace), $stub);
    }

    public function replacements($notifiable)
    {
        $arrayReplacements = $this->arrayReplacements($notifiable);
        $replace = [];
        foreach ($arrayReplacements as $key => $arrayReplacement) {
            $replace['{{ ' . $key . ' }}'] = $arrayReplacement['data'];
            $replace['{{' . $key . '}}'] = $arrayReplacement['data'];
        }
        return $replace;
    }

    public function arrayReplacements($notifiable = null): array
    {
        return [];
    }
}
