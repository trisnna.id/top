<?php

namespace App\Notifications\Admin\User;

use App\Notifications\BaseNotification;

class AssignOrientationLeaderNotification extends BaseNotification
{

    public $channel = [];
    public $mailer;
    public $template = 'student-assign-orientation-leader';

    public function __construct($notifiable = null)
    {
        parent::__construct($notifiable);
    }

    public function arrayReplacements($notifiable = null): array
    {
        return [
            'receiver' => [
                'data' => $this->notifiable->name ?? null,
                'description' => 'Receiver',
                'example' => 'Muhamad Ali',
            ],
            'link' => [
                'data' => route('orientation-leader-activation', $notifiable->remember_token),
                'description' => 'URL redirect the user',
                'example' => '#',
            ],
        ];
    }
}
