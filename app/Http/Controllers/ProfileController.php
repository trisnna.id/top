<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ProfileService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new ProfileService();
    }

    public function indexNotification(Request $request)
    {
        return $this->service->indexNotification($request, auth()->user());
    }

    public function updateNotificationRead(Request $request, $id)
    {
        return $this->service->updateNotificationRead($request->all(), auth()->user(), $id);
    }
}
