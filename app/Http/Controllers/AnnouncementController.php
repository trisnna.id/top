<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Contracts\AnnouncementRepository;

class AnnouncementController extends Controller
{
    /**
     * Create a new announcement repository instance
     *
     * @var     AnnouncementRepository
     */
    private AnnouncementRepository $announcement;

    /**
     * Create a new controller instance.
     * 
     * @param   AnnouncementRepository $announcement
     */
    public function __construct(AnnouncementRepository $announcement)
    {
        $this->announcement = $announcement;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcements = $this->announcement->getPublishedAnnouncements();
        return view('announcement.index', [
            'title' => 'Announcement',
            'announcements' => $announcements,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $entity = $this->announcement->firstByUUID($slug);

        if (is_null($entity)) {
            abort(404, 'Entity not found');
        }

        $entity->publish_date = $entity->created_at->format('d M Y');

        return view('announcement.show', [
            'title' => $entity->name,
            'announcement' => $entity,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
