<?php

namespace App\Http\Controllers;

use App\Services\PreOrientationService;
use App\Traits\Controllers\BaseControllerTrait;

class PreOrientationController extends Controller
{

    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new PreOrientationService();
    }
}
