<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\PageService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class PageController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new PageService();
    }

    public function indexPolicy(Request $request)
    {
        return $this->service->show($request, auth()->user(), 'policy');
    }

    public function indexDisclaimer(Request $request)
    {
        return $this->service->show($request, auth()->user(), 'disclaimer');
    }
}
