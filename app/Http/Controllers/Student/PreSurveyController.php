<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Services\AuthService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Survey\StorePostOrientationRequest;
use App\Http\Requests\Survey\StorePreOrientationRequest;
use App\Models\Faculty;
use App\Models\Programme;

class PreSurveyController extends Controller
{
    public $authService;

    public function __construct()
    {
        $this->authService = new AuthService();
    }

    public function index(Request $request)
    {
        $title = 'Pre Orientation Survey';
        $is_answered = $this->authService->student()->is_pre_orientation_survey_complete;
        $answer = $this->authService->student()->pre_survey_answer ?? null;

        $faculties = Faculty::all();
        $programmes = Programme::all();
        // dd($answer);
        return view('student.survey.pre-orientation', compact('title', 'is_answered', 'answer', 'faculties', 'programmes'));
    }

    public function store(StorePreOrientationRequest $request)
    {
        $data = [];
        $data['q1'] = [
            'question' => 'Nationality',
            'answer' => $request->input('q1'),
        ];
        $data['q2'] = [
            'question' => 'Please indicate your level of concern for each of the following topics related to your transition into Taylor\'s.',
            'answer' => [
                'q2-m-1' => [
                    'question' => 'Academic Support',
                    'answer' => $request->input('q2-m-1'),
                ],
                'q2-m-2' => [
                    'question' => 'Making New Friends',
                    'answer' => $request->input('q2-m-2'),
                ],
                'q2-m-3' => [
                    'question' => 'Personal Well-being',
                    'answer' => $request->input('q2-m-3'),
                ],
                'q2-m-4' => [
                    'question' => 'Support from Staff',
                    'answer' => $request->input('q2-m-4'),
                ],
                'q2-m-5' => [
                    'question' => 'Campus Facilities and Services',
                    'answer' => $request->input('q2-m-5'),
                ],
                'q2-1' => [
                    'question' => 'Academic Support',
                    'answer' => $request->input('q2-1'),
                ],
                'q2-2' => [
                    'question' => 'Making New Friends',
                    'answer' => $request->input('q2-2'),
                ],
                'q2-3' => [
                    'question' => 'Personal Well-being',
                    'answer' => $request->input('q2-3'),
                ],
                'q2-4' => [
                    'question' => 'Support from Staff',
                    'answer' => $request->input('q2-4'),
                ],
                'q2-5' => [
                    'question' => 'Campus Facilities and Services',
                    'answer' => $request->input('q2-5'),
                ],
                'q2-6' => [
                    'question' => 'Language and Communication',
                    'answer' => $request->input('q2-6', ''),
                ],
                'q2-7' => [
                    'question' => 'Cultural Adjusment',
                    'answer' => $request->input('q2-7', ''),
                ],
                'q2-8' => [
                    'question' => 'Visa and Legal Matters',
                    'answer' => $request->input('q2-8', ''),
                ],
                'q2-9' => [
                    'question' => 'Managing Finance',
                    'answer' => $request->input('q2-9', ''),
                ],
                'q2-10' => [
                    'question' => 'Accomodation',
                    'answer' => $request->input('q2-10', ''),
                ],
            ]
        ];
        // for ($i = 1; $i <= 9; $i++) {
        //     switch ($i) {
        //         case 1:
        //             $data['q1'] = [
        //                 'question' => 'Taylor\'s Student ID number (the alternative is your IC/Passport number)',
        //                 'answer' => $request->input('q1'),
        //             ];
        //             break;
        //         case 2:
        //             $data['q2'] = [
        //                 'question' => 'Nationality',
        //                 'answer' => $request->input('q2'),
        //             ];
        //             break;
        //         case 3:
        //             $data['q3'] = [
        //                 'question' => 'Faculty',
        //                 'answer' => $request->input('q3'),
        //             ];
        //             break;
        //         case 4:
        //             $data['q4'] = [
        //                 'question' => 'Programme',
        //                 'answer' => $request->input('q4'),
        //             ];
        //             break;
        //         case 5:
        //             $data['q5'] = [
        //                 'question' => 'Please indicate your level of concern for each of the following topics related to your transition into Taylor\'s.',
        //                 'answer' => [
        //                     'q5-1' => [
        //                         'question' => 'My academic journey',
        //                         'answer' => $request->input('q5-1'),
        //                     ],
        //                     'q5-2' => [
        //                         'question' => 'Making new friends',
        //                         'answer' => $request->input('q5-2'),
        //                     ],
        //                     'q5-3' => [
        //                         'question' => 'My physical health',
        //                         'answer' => $request->input('q5-3'),
        //                     ],
        //                     'q5-4' => [
        //                         'question' => 'Support from staff',
        //                         'answer' => $request->input('q5-4'),
        //                     ],
        //                     'q5-5' => [
        //                         'question' => 'Support from peers',
        //                         'answer' => $request->input('q5-5'),
        //                     ],
        //                     'q5-6' => [
        //                         'question' => 'Campus services such as Campus Central, ICT Helpdesk, e-learning etc',
        //                         'answer' => $request->input('q5-6'),
        //                     ],
        //                     'q5-7' => [
        //                         'question' => 'Support from Orientation Leaders',
        //                         'answer' => $request->input('q5-7'),
        //                     ],
        //                     'q5-8' => [
        //                         'question' => 'My mental health',
        //                         'answer' => $request->input('q5-8'),
        //                     ],
        //                 ],
        //             ];
        //             break;
        //         case 6:
        //             $data['q6'] = [
        //                 'question' => 'Do you have any other concerns related to adapting to Taylor\'s that is not listed above.',
        //                 'answer' => $request->input('q6'),
        //             ];
        //             break;
        //     }
        // }

        $student = $this->authService->student();
        $student->update([
            'is_pre_orientation_survey_complete' => true,
            'pre_survey_answer' => $data,
        ]);
        $student->studentPoints()->updateOrCreate([
            'student_id' => $student->id,
            'name' => 'pre_orientation_survey',
        ], [
            'point' => setting('survey.point_pre_orientation_survey', 0)
        ]);

        return redirect()->route('student.preorientation.index')->with('message', 'Pre-Orientation Survey completed!');
    }
}