<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Services\AuthService;
use App\Http\Controllers\Controller;
use App\DataTables\Student\MilestoneDataTable;
use App\Repositories\Contracts\CheckpointRepository;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Repositories\Contracts\StudentPreOrientationRepository;

class PreorientationController extends Controller
{
    /**
     * Create a new checkpoint repository instance
     *
     * @var CheckpointRepository
     */
    private CheckpointRepository $checkpoint;

    /**
     * Create a new student preorientation repository instance
     * 
     * @var StudentPreOrientationRepository
     */
    private StudentPreOrientationRepository $studentPreOrientation;

    /**
     * Create a new preorientation repository instance
     * 
     * @var PreOrientationRepository
     */
    private PreOrientationRepository $preOrientation;

    /**
     * Create a new controller instance
     *
     * @param CheckpointRepository  $checkpoint
     */
    public function __construct(CheckpointRepository $checkpoint, StudentPreOrientationRepository $studentPreOrientation, PreOrientationRepository $preOrientation)
    {
        $this->checkpoint = $checkpoint;
        $this->studentPreOrientation = $studentPreOrientation;
        $this->preOrientation = $preOrientation;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(MilestoneDataTable $table)
    {
        $milestones = $this->checkpoint->getStudentMilestone(['id', 'label', 'value']);
        $studentActivities = $milestones->map(function ($item) {
            return collect([
                'key' => $item->id,
                'activities' => $item->preOrientations
            ]);
        })->reduce(function ($collection, $milestone) {
            $collection->push(collect([
                'milestone' => $milestone->get('key'),
                'studentActivities' => $milestone->get('activities')->map(function ($item) {
                    return collect($item->students);
                })
            ]));
            return $collection;
        }, collect())->transform(function ($item) {
            return collect([
                'milestone' => $item->get('milestone'),
                'countActivities' => $item->get('studentActivities')->count(),
                'completed' => $item->get('studentActivities')->filter(function ($items) {
                    return $items->isNotEmpty();
                })->count() === $item->get('studentActivities')->count()
            ]);
        });
        $countCompleted = $this->studentPreOrientation->getCompleted()->count();
        $countPreOrientation = $this->preOrientation->countPublished();

        return $table->render('student.preorientation.index', [
            'milestones' => $milestones,
            'studentActivities' => $studentActivities,
            'countCompleted' => $countCompleted,
            'countPreOrientation' => $countPreOrientation
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
