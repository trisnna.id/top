<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Services\AuthService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Survey\StorePostOrientationRequest;

class PostSurveyController extends Controller
{
    public $authService;

    public function __construct()
    {
        $this->authService = new AuthService();
    }

    public function index(Request $request)
    {
        $title = 'Post Orientation Survey';
        $is_answered = $this->authService->student()->is_post_orientation_survey_complete;
        $answer = $this->authService->student()->post_survey_answer ?? null;
        // dd($answer);
        return view('student.survey.post-orientation', compact('title', 'is_answered', 'answer'));
    }

    public function store(StorePostOrientationRequest $request)
    {
        $data = [];
        for ($i = 1; $i <= 7; $i++) {
            switch ($i) {
                case 1:
                    $data['q1'] = [
                        'question' => 'How useful was the information you acquired during the Pre-Orientation on Taylor’s Orientation Portal?',
                        'answer' => $request->input('q1'),
                    ];
                    break;
                case 2:
                    $data['q2'] = [
                        'question' => 'Rate your experience using Taylor\'s Orientation Portal?',
                        'answer' => $request->input('q2'),
                    ];
                    break;
                case 3:
                    $data['q3'] = [
                        'question' => 'How would you rate the level of support you received from the  Orientation Leaders?',
                        'answer' => $request->input('q3'),
                    ];
                    break;
                case 4:
                    $data['q4'] = [
                        'question' => 'Please indicate your level of concern for each of the following topics related to your transition into Taylor\'s after attending Orientation.',
                        'answer' => [
                            'q4-1' => [
                                'question' => 'Academic Support',
                                'answer' => $request->input('q4-1'),
                            ],
                            'q4-2' => [
                                'question' => 'Making New Friends',
                                'answer' => $request->input('q4-2'),
                            ],
                            'q4-3' => [
                                'question' => 'Personal Well-being',
                                'answer' => $request->input('q4-3'),
                            ],
                            'q4-4' => [
                                'question' => 'Campus Facilities and Services',
                                'answer' => $request->input('q4-4'),
                            ],
                            'q4-5' => [
                                'question' => 'Support from Staff',
                                'answer' => $request->input('q4-5'),
                            ],
                        ],
                    ];
                    break;
                case 5:
                    $data['q5'] = [
                        'question' => 'Please share your thoughts and experiences about your overall orientation as a freshman.',
                        'answer' => $request->input('q5'),
                    ];
                    break;
                case 6:
                    $data['q6'] = [
                        'question' => 'How likely are you to recommend Taylor\'s to a friend or colleague?',
                        'answer' => $request->input('q6'),
                    ];
                    break;
            }
        }

        $student = $this->authService->student();
        $student->update([
            'is_post_orientation_survey_complete' => true,
            'post_survey_answer' => $data,
        ]);
        $student->studentPoints()->updateOrCreate([
            'student_id' => $student->id,
            'name' => 'post_orientation_survey',
        ], [
            'point' => setting('survey.point_post_orientation_survey', 0),
        ]);

        return redirect()->route('orientations.index2')->with('message', 'Post Orientation Survey completed!');
    }
}