<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Services\ClubSocietyService;
use App\Traits\Controllers\BaseControllerTrait;

class ClubSocietyController extends Controller
{

    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new ClubSocietyService();
    }
}
