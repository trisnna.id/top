<?php

namespace App\Http\Controllers;

use App\Services\PortalService;
use App\Traits\Controllers\BaseControllerTrait;

class PortalController extends Controller
{

    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new PortalService();
    }
}
