<?php

namespace App\Http\Controllers;

use App\Services\OrientationService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class OrientationController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new OrientationService();
    }

    public function index2(Request $request)
    {
        $this->service->debug($request, __METHOD__);
        return $this->service->index2($request, auth()->user());
    }

    public function exportTimetable(Request $request)
    {
        return $this->service->exportTimetable($request, auth()->user());
    }

    public function upsertAttendance(Request $request)
    {
        return $this->service->upsertAttendance($request->all(), auth()->user());
    }

    public function updateRate(Request $request, $uuid)
    {
        return $this->service->updateRate($request->all(), auth()->user(), $uuid);
    }

    public function upsertRsvp(Request $request, $uuid)
    {
        return $this->service->upsertRsvp($request->all(), auth()->user(), $uuid);
    }

    public function upsertRsvpYes(Request $request, $uuid)
    {
        return $this->service->upsertRsvpYes($request->all(), auth()->user(), $uuid);
    }

    public function upsertRsvpNo(Request $request, $uuid)
    {
        return $this->service->upsertRsvpNo($request->all(), auth()->user(), $uuid);
    }
}
