<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Entities\AnnouncementEntity;
use Illuminate\Http\Request;
use App\Services\AuthService;
use App\Http\Controllers\Controller;
use App\DataTables\AnnouncementDataTable;
use App\Traits\Controllers\BaseControllerTrait;
use App\Repositories\Contracts\CampusRepository;
use App\Repositories\Contracts\IntakeRepository;
use App\Repositories\Contracts\ProgrammeRepository;
use App\Repositories\Contracts\CheckpointRepository;
use App\Repositories\Contracts\AnnouncementRepository;
use App\Http\Requests\Announcement\StoreAnnouncementRequest;
use App\Http\Requests\Announcement\UpdateAnnouncementRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\File;
use Illuminate\Http\UploadedFile;

class AnnouncementController extends Controller
{
    use BaseControllerTrait;

    /**
     * Create a new auth service instance
     *
     * @var     AuthService
     */
    private AuthService $authService;

    /**
     * Create a new intake repository instance
     * 
     * @var     IntakeRepository
     */
    private IntakeRepository $intake;

    /**
     * Create a new checkpoint repository instance
     *
     * @var     CheckpointRepository
     */
    private CheckpointRepository $checkpoint;

    /**
     * Create a new programme repository instance
     *
     * @var     ProgrammeRepository
     */
    private ProgrammeRepository $programme;

    /**
     * Create a new campus repository instance
     *
     * @var     CampusRepository
     */
    private CampusRepository $campus;

    /**
     * Create a new announcement repository instance
     *
     * @var     AnnouncementRepository
     */
    private AnnouncementRepository $announcement;

    /**
     * Create a new controller instance
     *
     * @param   AuthService             $authService
     * @param   IntakeRepository        $intake
     * @param   CheckpointRepository    $checkpoint
     * @param   ProgrammeRepository     $programme
     * @param   CampusRepository        $campus
     * @param   AnnouncementRepository  $announcement
     * @return  void
     */
    public function __construct(
        AuthService $authService,
        IntakeRepository $intake,
        CheckpointRepository $checkpoint,
        ProgrammeRepository $programme,
        CampusRepository $campus,
        AnnouncementRepository $announcement
    )
    {
        $this->authService = $authService;
        $this->intake = $intake;
        $this->checkpoint = $checkpoint;
        $this->programme = $programme;
        $this->campus = $campus;
        $this->announcement = $announcement;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AnnouncementDataTable $dataTable)
    {
        $title = __('Announcements & News');
        $user = $this->authService->user;

        return $dataTable->render('admin.announcement.index', compact('title', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.announcement.upsert', [
            'announcement' => null,
            'intakes' => $this->intake->getActive(),
            'programmes' => $this->programme->getActive(),
            'campuses' => $this->campus->getActive(),
            'title' => __('description.action.create', ['x' => __('description.title.announcement')]),
            'route' => route('admin.announcements.store'),
            'method' => 'POST',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreAnnouncementRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAnnouncementRequest $request)
    {
        $thumbnail_hex = $request->get('thumbnail_hex');
        if ($thumbnail_hex) {
            // decode the base64 file
            $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $thumbnail_hex));

            // save it to temporary dir first.
            $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
            file_put_contents($tmpFilePath, $fileData);

            // this just to help us get file info.
            $tmpFile = new File($tmpFilePath);

            $thumbnail = new UploadedFile(
                $tmpFile->getPathname(),
                $tmpFile->getFilename(),
                $tmpFile->getMimeType(),
                0,
                true // Mark it as test, since the file isn't from real HTTP POST.
            );
        }

        $entity = $this->announcement->create([
            'name' => $request->get('title'),
            'localities' => $request->get('nationality'),
            'intakes' => $request->get('intake'),
            'programmes' => $request->get('programme'),
            'campuses' => $request->get('campus'),
            'content' => $request->get('content'),
            'thumbnail' => isset($thumbnail) ? $thumbnail->store('thumbnail/announcement') : 'https://dummyimage.com/600x400/000/fff',
            AnnouncementEntity::FIELD_SEQUENCE => $request->get(AnnouncementEntity::FIELD_SEQUENCE)
        ]);

        return redirect()->route('admin.announcements.show', ['uuid' => $entity->uuid])
            ->with('message', 'Announcement created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show(string $uuid)
    {
        $entity = $this->announcement->firstByUUID($uuid);

        if (is_null($entity)) {
            abort(404, 'Entity not found');
        }

        return view('admin.announcement.show', [
            'title' => __('Preview News & Announcements'),
            'announcement' => $entity,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit(string $uuid)
    {
        $entity = $this->announcement->firstByUUID($uuid);

        if (is_null($entity)) {
            abort(404, 'Entity not found');
        }

        return view('admin.announcement.upsert', [
            'announcement' => $entity,
            'intakes' => $this->intake->getActive(),
            'programmes' => $this->programme->getActive(),
            'campuses' => $this->campus->getActive(),
            'title' => __('News & Announcements Form'),
            'route' => route('admin.announcements.update', ['uuid' => $entity->uuid]),
            'method' => 'PATCH',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateAnnouncementRequest  $request
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAnnouncementRequest $request, string $uuid)
    {
        // dd($request->all());
        $entity = $this->announcement->firstByUUID($uuid);

        if (is_null($entity)) {
            abort(404, 'Entity not found');
        }

        
        $thumbnail_hex = $request->get('thumbnail_hex');
        if ($thumbnail_hex) {
            // decode the base64 file
            $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $thumbnail_hex));

            // save it to temporary dir first.
            $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
            file_put_contents($tmpFilePath, $fileData);

            // this just to help us get file info.
            $tmpFile = new File($tmpFilePath);

            $thumbnail = new UploadedFile(
                $tmpFile->getPathname(),
                $tmpFile->getFilename(),
                $tmpFile->getMimeType(),
                0,
                true // Mark it as test, since the file isn't from real HTTP POST.
            );
        }

        // $request->merge([
        //     'content' => str_replace('&nbsp;', '', $request->get('content', $entity->content)),
        // ]);

        $this->announcement->update($entity->id, [
            'name' => $request->get('title', $entity->name),
            'localities' => $request->get('nationality', $entity->localities),
            'intakes' => $request->get('intake', $entity->intakes),
            'programmes' => $request->get('programme', $entity->programmes),
            'campuses' => $request->get('campus', $entity->campuses),
            'content' => $request->get('content', $entity->content),
            'thumbnail' => isset($thumbnail) ? $thumbnail->store('thumbnail/announcement') : ($request->get('logo_remove') == 1 ? 'https://dummyimage.com/600x400/000/fff' : $entity->thumbnail),
            AnnouncementEntity::FIELD_SEQUENCE => $request->get(AnnouncementEntity::FIELD_SEQUENCE, $entity->sequence)
        ]);

        return redirect()->route('admin.announcements.show', ['uuid' => $entity->uuid])
            ->with('message', 'Announcement updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $uuid)
    {
        $this->announcement->deleteByUUID($uuid);

        return response()->json([
            'message' => 'Announcement deleted successfully'
        ]);
    }
}
