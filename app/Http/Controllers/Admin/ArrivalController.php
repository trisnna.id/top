<?php

namespace App\Http\Controllers\Admin;

use App\Services\Admin\ArrivalService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class ArrivalController extends ResourceController
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new ArrivalService();
    }

    public function clone(Request $request, $uuid)
    {
        $results = $this->service->clone($request, auth()->user(), $uuid);
        return $this->arrayResponse($results);
    }
}
