<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\QuizService;
use App\Traits\Controllers\BaseControllerTrait;

class QuizController extends Controller
{

    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new QuizService();
    }
}
