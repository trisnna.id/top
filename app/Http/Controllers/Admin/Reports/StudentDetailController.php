<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Http\Controllers\Controller;
use App\Services\Admin\ReportService;
use App\Traits\Controllers\BaseControllerTrait;

class StudentDetailController extends Controller
{
    use BaseControllerTrait;

    public function __construct(ReportService $service)
    {
        $this->service = $service;
    }
}
