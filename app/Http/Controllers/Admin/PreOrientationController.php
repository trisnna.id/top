<?php

namespace App\Http\Controllers\Admin;

use App\Models\Intake;
use App\Models\School;
use App\Models\Faculty;
use App\Models\Mobility;
use App\Models\Programme;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Support\Str;
use App\Services\AuthService;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\DataTables\PreOrientationDataTable;
use Symfony\Component\HttpFoundation\File\File;
use App\Http\Requests\StorePreOrientationRequest;
use App\Http\Requests\UpdatePreOrientationRequest;
use App\Repositories\Contracts\CheckpointRepository;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Repositories\Contracts\PreOrientationQuizRepository;

class PreOrientationController extends Controller
{
    /**
     * Create a new auth service instance
     *
     * @var     AuthService
     */
    private AuthService $authService;

    /**
     * Create a new checkpoint repository instance
     *
     * @var     CheckpointRepository
     */
    private CheckpointRepository $checkpoint;

    /**
     * Create a new pre-orientation repository instance
     *
     * @var     PreOrientationRepository
     */
    private PreOrientationRepository $preOrientation;

    /**
     * Create a new pre-orientation quiz repository instance
     *
     * @var     PreOrientationQuizRepository
     */
    private PreOrientationQuizRepository $preOrientationQuiz;

    /**
     * Create a new controller instance
     *
     * @param   AuthService                     $authService
     * @param   CheckpointRepository            $checkpoint
     * @param   PreOrientationRepository        $preOrientation
     * @param   PreOrientationQuizRepository    $preOrientationQuiz
     * @return  void
     */
    public function __construct(
        AuthService $authService,
        CheckpointRepository $checkpoint,
        PreOrientationRepository $preOrientation,
        PreOrientationQuizRepository $preOrientationQuiz
    ) {
        $this->authService = $authService;
        $this->checkpoint = $checkpoint;
        $this->preOrientation = $preOrientation;
        $this->preOrientationQuiz = $preOrientationQuiz;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PreOrientationDataTable $dataTable)
    {
        $title = __('Pre-Orientation');
        $user = $this->authService->user;

        return $dataTable->render('admin.preorientation.index', compact('title', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.preorientation.upsert', [
            'preOrientation' => null,
            'route' => route('admin.preorientation.store'),
            'method' => 'POST',
            'title' => 'Pre-Orientation Form',
            'checkpoints' => $this->checkpoint->allOnlyLabelValue(),
            'intakes' => Intake::where('is_active', 1)->get(),
            'faculties' => Faculty::where('is_active', 1)->get(),
            'schools' => School::where('is_active', 1)->get(),
            'programmes' => Programme::where('is_active', 1)->get(),
            'mobilities' => Mobility::where('is_active', 1)->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePreOrientationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePreOrientationRequest $request)
    {
        $thumbnail_hex = $request->get('thumbnail_hex');
        if ($thumbnail_hex) {
            // decode the base64 file
            $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $thumbnail_hex));

            // save it to temporary dir first.
            $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
            file_put_contents($tmpFilePath, $fileData);

            // this just to help us get file info.
            $tmpFile = new File($tmpFilePath);

            $thumbnail = new UploadedFile(
                $tmpFile->getPathname(),
                $tmpFile->getFilename(),
                $tmpFile->getMimeType(),
                0,
                true // Mark it as test, since the file isn't from real HTTP POST.
            );
        }

        $entity = $this->preOrientation->create([
            'checkpoint_id' => $request->get('category'),
            'activity_name' => $request->get('activity_name'),
            'effective_date' => $request->get('effective_date'),
            'thumbnail' => isset($thumbnail) ? $thumbnail->store('thumbnail/preorientation') : 'https://dummyimage.com/600x400/000/fff',
            'localities' => $request->get('nationality'),
            'content' => $request->get('content'),

            'intakes' => $request->get('intake'),
            'faculties' => $request->get('faculty'),
            'schools' => $request->get('school'),
            'programmes' => $request->get('programme'),
            'mobilities' => $request->get('mobility'),
        ]);

        if ((bool) $request->get('quiz')) {
            $this->preOrientationQuiz->createMany($entity->id, $request->get('mcq', []));
        }

        return redirect(route('admin.preorientation.index'))
            ->with('message', 'Pre-orientation created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function show(string $uuid)
    {
        $entity = $this->preOrientation->firstByUUID($uuid);

        if (is_null($entity)) {
            abort(404, 'Entity not found');
        }

        return view('admin.preorientation.show', [
            'title' => 'Preview Pre-Orientation',
            'preOrientation' => $entity,
            'preOrientationQuizses' => $this->preOrientationQuiz->findByPreOrientation($entity->id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit(string $uuid)
    {
        $entity = $this->preOrientation->firstByUUID($uuid);

        if (is_null($entity)) {
            abort(404, 'Entity not found');
        }

        return view('admin.preorientation.upsert', [
            'title' => 'Pre-Orientation Form',
            'route' => route('admin.preorientation.update', $entity->id),
            'method' => 'patch', 
            'preOrientation' => $entity,
            'checkpoints' => $this->checkpoint->allOnlyLabelValue(),
            'intakes' => Intake::where('is_active', 1)->get(),
            'faculties' => Faculty::where('is_active', 1)->get(),
            'schools' => School::where('is_active', 1)->get(),
            'programmes' => Programme::where('is_active', 1)->get(),
            'mobilities' => Mobility::where('is_active', 1)->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePreOrientationRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePreOrientationRequest $request, int $id)
    {
        $entity = $this->preOrientation->first((int) $id);

        if (is_null($entity)) {
            abort(404, 'Entity not found');
        }

        $thumbnail_hex = $request->get('thumbnail_hex');
        if ($thumbnail_hex) {
            // decode the base64 file
            $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $thumbnail_hex));

            // save it to temporary dir first.
            $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
            file_put_contents($tmpFilePath, $fileData);

            // this just to help us get file info.
            $tmpFile = new File($tmpFilePath);

            $thumbnail = new UploadedFile(
                $tmpFile->getPathname(),
                $tmpFile->getFilename(),
                $tmpFile->getMimeType(),
                0,
                true // Mark it as test, since the file isn't from real HTTP POST.
            );
        }

        $this->preOrientation->update($id, [
            'checkpoint_id' => $request->get('category', $entity->checkpoint_id),
            'activity_name' => $request->get('activity_name', $entity->activity_name),
            'thumbnail' => isset($thumbnail) ? $thumbnail->store('thumbnail/preorientation') : ($request->get('logo_remove') == 1 ? 'https://dummyimage.com/600x400/000/fff' : $entity->thumbnail),
            'effective_date' => $request->get('effective_date', $entity->effective_date),
            'localities' => $request->get('nationality', $entity->localities),
            'content' => $request->get('content', $entity->content),

            'intakes' => $request->get('intake'),
            'faculties' => $request->get('faculty'),
            'schools' => $request->get('school'),
            'programmes' => $request->get('programme'),
            'mobilities' => $request->get('mobility'),
        ]);

        if ((bool) $request->get('quiz')) {
            $this->preOrientationQuiz->upsertOrDeleteByPreOrientation(
                $entity->id, $request->get('mcq')
            );
        } else {
            $this->preOrientationQuiz->deleteByPreOrientation($entity->id);
        }

        return redirect(route('admin.preorientation.show', ['uuid' => $entity->uuid]))
            ->with('message', 'Pre-orientation updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string  $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $uuid)
    {
        $this->preOrientation->deleteByUUID($uuid);

        return response()->json([
            'message' => 'Pre-orientation deleted successfully'
        ]);
    }
}
