<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\UserService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new UserService();
    }

    public function storeAdmin(Request $request)
    {
        $results = $this->service->storeAdmin($request->all(), auth()->user());
        return $this->arrayResponse($results);
    }

    public function updateBulkRole(Request $request)
    {
        $results = $this->service->updateBulkRole($request->all(), auth()->user());
        return $this->arrayResponse($results);
    }

    public function orientationLeaderActivation(Request $request, $rememberToken)
    {
        $results = $this->service->orientationLeaderActivation($rememberToken, $request->all());
        return $this->arrayResponse($results);
    }
}
