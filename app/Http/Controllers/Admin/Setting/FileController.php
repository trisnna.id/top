<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Services\Admin\Setting\FileService;
use App\Traits\Controllers\BaseControllerTrait;

class FileController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new FileService();
    }
}
