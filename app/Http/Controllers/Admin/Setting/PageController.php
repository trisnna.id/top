<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Services\Admin\Setting\PageService;
use App\Traits\Controllers\BaseControllerTrait;

class PageController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new PageService();
    }
}
