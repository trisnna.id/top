<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Services\Admin\Setting\OrientationService;
use App\Traits\Controllers\BaseControllerTrait;

class OrientationController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new OrientationService();
    }
}
