<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Services\Admin\Setting\PortalService;
use App\Traits\Controllers\BaseControllerTrait;

class PortalController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new PortalService();
    }
}
