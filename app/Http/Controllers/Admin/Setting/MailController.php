<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Services\Admin\Setting\MailService;
use App\Traits\Controllers\BaseControllerTrait;

class MailController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new MailService();
    }
}
