<?php

namespace App\Http\Controllers\Admin\Setting;

use Illuminate\Http\Request;
use App\Constants\SettingConstant;
use App\Http\Controllers\Controller;

class QuicklinkController extends Controller
{
    public function index() {
        $title = __('Quick Link');
        return view('admin.setting.quicklink.index', compact('title'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $inputQuicklink = SettingConstant::QUICKLINK;
        $inputQuicklink = array_merge($inputQuicklink, $input['quicklink']);

        // other $inputQuickLink that not in $input['quicklink'] will be set to empty string
        foreach ($inputQuicklink as $key => $value) {
            if (!isset($input['quicklink'][$key])) {
                $inputQuicklink[$key] = '';
            }
        }

        setting(['quicklink' => $inputQuicklink])->save();
        return response()->json([
            'message' => __('Update Successfully'),
        ]);
    }
}
