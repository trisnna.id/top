<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Services\Admin\Setting\IntakeService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class IntakeController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new IntakeService();
    }

    public function updateBulkActive(Request $request)
    {
        return $this->service->updateBulkActive($request->all(), auth()->user(), true);
    }

    public function updateBulkInactive(Request $request)
    {
        return $this->service->updateBulkActive($request->all(), auth()->user(), false);
    }

    public function updateOrientationActivitiesActive(Request $request, $uuid)
    {
        $results = $this->service->updateOrientationActivitiesActive($request->all(), auth()->user(), $uuid);
        return $this->arrayResponse($results);
    }

    public function updateTimetableActive(Request $request, $uuid)
    {
        $results = $this->service->updateTimetableActive($request->all(), auth()->user(), $uuid);
        return $this->arrayResponse($results);
    }
}
