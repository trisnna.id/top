<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Services\Admin\Setting\SurveyService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new SurveyService();
    }

    public function upload(Request $request)
    {
        $results = $this->service->upload($request->all(), auth()->user());
        return $this->arrayResponse($results);
    }
}
