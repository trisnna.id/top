<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Services\Admin\Setting\LocalityService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class LocalityController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new LocalityService();
    }

    public function updateBulkActive(Request $request)
    {
        return $this->service->updateBulkActive($request->all(), auth()->user(), true);
    }

    public function updateBulkInactive(Request $request)
    {
        return $this->service->updateBulkActive($request->all(), auth()->user(), false);
    }
}
