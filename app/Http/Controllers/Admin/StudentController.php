<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\StudentService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new StudentService();
    }

    public function showOrientation(Request $request, $uuid)
    {
        return $this->service->showOrientation($request, auth()->user(), $uuid);
    }
}
