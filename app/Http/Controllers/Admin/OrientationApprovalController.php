<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\OrientationApprovalService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class OrientationApprovalController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new OrientationApprovalService();
    }

    public function updateApprove(Request $request, $uuid)
    {
        return $this->service->updateApprove($request->all(), auth()->user(), $uuid);
    }

    public function updateCancel(Request $request, $uuid)
    {
        return $this->service->updateCancel($request->all(), auth()->user(), $uuid);
    }

    public function updateCorrection(Request $request, $uuid)
    {
        return $this->service->updateCorrection($request->all(), auth()->user(), $uuid);
    }

    public function updatePublish(Request $request, $uuid)
    {
        return $this->service->updatePublish($request->all(), auth()->user(), $uuid);
    }

    public function notifyStudent(Request $request, $uuid)
    {
        return $this->service->notifyStudent($request->all(), auth()->user(), $uuid);
    }
}
