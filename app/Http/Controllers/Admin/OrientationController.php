<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\OrientationService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class OrientationController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new OrientationService();
    }

    public function index2(Request $request)
    {
        return $this->service->index2($request, auth()->user());
    }

    public function calculateStudent(Request $request)
    {
        $results = $this->service->calculateStudent($request->all(), auth()->user());
        return $this->arrayResponse($results);
    }

    public function storeAttendance(Request $request, $uuid)
    {
        return $this->service->storeAttendance($request->all(), auth()->user(), $uuid);
    }

    public function storeRecording(Request $request, $uuid)
    {
        return $this->service->storeRecording($request->all(), auth()->user(), $uuid);
    }

    public function clone(Request $request, $uuid)
    {
        $results = $this->service->clone($request, auth()->user(), $uuid);
        return $this->arrayResponse($results);
    }

    public function updateLink(Request $request, $uuidOrientation, $uuidLink)
    {
        return $this->service->updateLink($request->all(), auth()->user(), $uuidOrientation, $uuidLink);
    }

    public function destroyAttendance(Request $request, $uuidOrientation, $uuidAttendance)
    {
        return $this->service->deleteAttendance($request->all(), auth()->user(), $uuidOrientation, $uuidAttendance);
    }

    public function destroyLink(Request $request, $uuidOrientation, $uuidLink)
    {
        return $this->service->deleteLink($request->all(), auth()->user(), $uuidOrientation, $uuidLink);
    }

    public function destroyFile(Request $request, $uuidOrientation, $uuidFile)
    {
        return $this->service->deleteFile($request->all(), auth()->user(), $uuidOrientation, $uuidFile);
    }

    public function destroyRecordingFile(Request $request, $uuidOrientation, $uuidFile)
    {
        return $this->service->deleteRecordingFile($request->all(), auth()->user(), $uuidOrientation, $uuidFile);
    }

    public function destroyRecordingLink(Request $request, $uuidOrientation, $uuidLink)
    {
        return $this->service->deleteRecordingLink($request->all(), auth()->user(), $uuidOrientation, $uuidLink);
    }
}
