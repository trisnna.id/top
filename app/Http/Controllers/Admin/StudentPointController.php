<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\StudentPointService;
use App\Traits\Controllers\BaseControllerTrait;

class StudentPointController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new StudentPointService();
    }
}
