<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\ReportService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new ReportService();
    }

    public function indexStudentLogin(Request $request)
    {
        return $this->service->indexStudentLogin($request, auth()->user());
    }

    public function indexStudentAttendance(Request $request)
    {
        return $this->service->indexStudentAttendance($request, auth()->user());
    }

    public function indexStudentRegistration(Request $request)
    {
        return $this->service->indexStudentRegistration($request, auth()->user());
    }

    public function indexStudentActivity(Request $request)
    {
        return $this->service->indexStudentActivity($request, auth()->user());
    }

    public function indexAuditTrail(Request $request)
    {
        return $this->service->indexAuditTrail($request, auth()->user());
    }

    public function indexStudentSurvey(Request $request)
    {
        return $this->service->indexStudentSurvey($request, auth()->user());
    }
}
