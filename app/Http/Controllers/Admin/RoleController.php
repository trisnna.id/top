<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\RoleService;
use App\Traits\Controllers\BaseControllerTrait;

class RoleController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new RoleService();
    }
}
