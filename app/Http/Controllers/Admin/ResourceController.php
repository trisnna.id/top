<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\ResourceService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class ResourceController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new ResourceService();
    }

    public function updateLink(Request $request, $uuidResource, $uuidLink)
    {
        return $this->service->updateLink($request->all(), auth()->user(), $uuidResource, $uuidLink);
    }

    public function updateFile(Request $request, $uuidResource, $uuidFile)
    {
        return $this->service->updateFile($request->all(), auth()->user(), $uuidResource, $uuidFile);
    }

    public function destroyLink(Request $request, $uuidResource, $uuidLink)
    {
        return $this->service->deleteLink($request->all(), auth()->user(), $uuidResource, $uuidLink);
    }

    public function destroyFile(Request $request, $uuidResource, $uuidFile)
    {
        return $this->service->deleteFile($request->all(), auth()->user(), $uuidResource, $uuidFile);
    }
}
