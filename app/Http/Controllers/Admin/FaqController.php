<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\FaqService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new FaqService();
    }

    public function clone(Request $request, $uuid)
    {
        $results = $this->service->clone($request, auth()->user(), $uuid);
        return $this->arrayResponse($results);
    }
}
