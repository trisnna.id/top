<?php

namespace App\Http\Controllers;

use App\Services\ArrivalService;

class ArrivalController extends ResourceController
{

    public function __construct()
    {
        $this->service = new ArrivalService();
    }
}
