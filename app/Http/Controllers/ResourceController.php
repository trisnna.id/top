<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ResourceService;
use App\Traits\Controllers\BaseControllerTrait;

class ResourceController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new ResourceService();
    }
}
