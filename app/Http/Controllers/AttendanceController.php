<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\AttendanceService;
use App\Traits\Controllers\BaseControllerTrait;

class AttendanceController extends Controller
{

    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new AttendanceService();
    }
}
