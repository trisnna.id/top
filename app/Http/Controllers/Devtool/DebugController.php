<?php

namespace App\Http\Controllers\Devtool;

use App\Http\Controllers\Controller;
use App\Models\Intake;
use App\Models\Orientation;
use App\Models\OrientationAttendance;
use App\Models\Student;
use App\Models\StudentPreOrientation;
use App\Models\User;
use App\Notifications\CustomEmailNotification;
use App\Notifications\Orientation\OrientationApproveNotification;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Repositories\Contracts\StudentPreOrientationRepository;
use App\Services\Admin\CampusService;
use App\Services\Admin\FacultyService;
use App\Services\Admin\IntakeService;
use App\Services\Admin\LevelStudyService;
use App\Services\Admin\MobilityService;
use App\Services\Admin\OrientationService;
use App\Services\Admin\SchoolService;
use App\Services\Admin\Setting\OrientationService as SettingOrientationService;
use App\Services\Admin\Setting\ProgrammeService;
use App\Services\Admin\StudentPointService;
use App\Services\Admin\StudentService;
use App\Services\AuthService;
use App\Services\Integration\Taylors\CmsService;
use App\Services\Integration\Taylors\LdapService;
use App\Services\Integration\Taylors\TopasService;
use App\Services\OrientationDateService;
use App\Services\OrientationService as ServicesOrientationService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class DebugController extends Controller
{

    private AuthService $auth;

    private StudentPreOrientationRepository $studentPreOrientation;

    private PreOrientationRepository $preOrientation;

    public function __construct(AuthService $auth, StudentPreOrientationRepository $studentPreOrientation, PreOrientationRepository $preOrientation)
    {
        $this->studentPreOrientation = $studentPreOrientation;
        $this->preOrientation = $preOrientation;
        $this->auth = $auth;
    }

    public function index(Request $request, $name)
    {
        $input = $request->all();
        switch ($name) {
            case 'test-custom-email-notification':
                return $this->testCustomEmailNotification($input);
            case 'test-notification':
                return $this->testNotification($input);
            case 'find-cms-student-like':
                return $this->findCmsStudentLike($input);
            case 'get-cms-views':
                return $this->getViews($input);
            case 'test-ldap-account':
                return $this->testLdapAccount($input);
            case 'test-pdf':
                return $this->testPdf($input);
            case 'fix-data-orientation-point':
                return $this->fixDataOrientationPoint($input);
            case 'fix-data-pre-orientation-point':
                return $this->fixDataPreOrientationPoint($input);
            case 'add-dummy-student':
                return $this->addDummyStudent($input);
            case 'student-preorientation':
                return $this->studentPreorientation($input);
            case 'delete-media-orientation-timetables':
                return $this->deleteMediaOrientationTimetables($input);
            case 'update-student-point':
                return $this->updateStudentPoint($input);
            case 'update-active-student-point':
                return $this->updateActiveStudentPoint($input);
            case 'get-course-intake-views':
                return $this->getCourseIntakeViews($input);
            case 'microsoft':
                return $this->microsoft($input);
            case 'any-test':
                return $this->anyTest($input);
            case 'get-pre-survey-answer':
                return $this->getPreSurveyAnswer($input);
            case 'get-post-survey-answer':
                return $this->getPostSurveyAnswer($input);
        }
    }

    protected function anyTest()
    {
        return (new StudentService())->syncStudentViews();
        dd(1);
    }

    protected function getPostSurveyAnswer()
    {
        return (new StudentService)->getPostSurveyAnswer();
    }
    
    protected function getPreSurveyAnswer()
    {
        return (new StudentService)->getPreSurveyAnswer();
    }

    protected function getCourseIntakeViews()
    {
        (new CmsService())->getCourseIntakes();
        dd('getViews');
    }

    public function testCustomEmailNotification()
    {
        $ids = [];
        $taylorsStudents = Student::select(['name', 'taylors_email as email'])
            ->whereIn('id_number', $ids)
            ->get()
            ->toArray();
        $personalStudents = Student::select(['name', 'personal_email as email'])
            ->whereIn('id_number', $ids)
            ->get()
            ->toArray();
        $students = array_merge($taylorsStudents, $personalStudents);
        $this->emailFilterExplodePush('/', $students);
        $this->emailFilterExplodePush(';', $students);
        $this->emailFilterExplodePush(',', $students);
        $emails = $this->chunkByMailers($students);
        dd($emails, $students);
    }

    protected function customEmailData()
    {
    }

    protected function emailFilterExplodePush($separator, &$array)
    {
        collect($array)->filter(function ($value, $key) use (&$array, $separator) {
            if (Str::contains($value['email'], $separator)) {
                unset($array[$key]);
                return true;
            } else {
                return false;
            };
        })->each(function ($value) use (&$array, $separator) {
            $emails = explode($separator, $value['email']);
            foreach ($emails as $email) {
                if (!empty(trim($email) ?? null)) {
                    array_push($array, [
                        'name' => trim($value['name']),
                        'email' => trim($email),
                    ]);
                }
            }
        });
        $array = array_filter($array);
        $array = array_values($array);
        $array = array_filter($array, function ($value) {
            return !empty($value['email'] ?? null);
        });
    }

    protected function chunkByMailers(array $emails)
    {
        $mailers = [
            config("mail.mailers.smtp10"),
            config("mail.mailers.smtp9"),
            config("mail.mailers.smtp8"),
            config("mail.mailers.smtp7"),
            config("mail.mailers.smtp6"),
            config("mail.mailers.smtp5"),
            config("mail.mailers.smtp4"),
            config("mail.mailers.smtp3"),
            config("mail.mailers.smtp2"),
            config("mail.mailers.smtp"),
        ];
        $countEmails = collect($emails)->count();
        $sumLimitPersonals = collect($mailers)->sum('limit.personal');
        $limitPersonals = collect($mailers)->pluck('limit.personal');
        $mailers = collect($mailers)->pluck('mailer');
        $mergeLimitPersonals = collect();
        $mergeMailers = collect();
        for ($i = 0; $i < ceil($countEmails / $sumLimitPersonals); $i++) {
            $mergeLimitPersonals = $mergeLimitPersonals->merge($limitPersonals);
            $mergeMailers = $mergeMailers->merge($mailers);
        }
        $valuePattern = 0;
        foreach ($mergeLimitPersonals as $key => $value) {
            $valuePattern += $value;
            $patternLimitPersonals[$key] = $valuePattern;
            unset($key);
        }
        $chunkEmails = collect($emails)->chunk($sumLimitPersonals);
        $chunkEmailsByMailer = [];
        foreach ($chunkEmails as $key => $chunkEmail) {
            $chunkEmailsByMailer[$key] = $chunkEmail->split(count($mailers))->toArray();
            foreach ($chunkEmailsByMailer[$key] as $key2 => $value) {
                $chunkEmailsByMailer[$key][$key2] = collect($value)->map(function ($instance) use ($mailers, $key2) {
                    $instance['mailer'] = $mailers[$key2];
                    return $instance;
                })->toArray();
            }
            unset($key);
        }
        return $chunkEmailsByMailer;
    }

    public function updateActiveStudentPoint()
    {
        (new StudentPointService())->updateActiveStudentPoint();
        dd('success');
    }
    public function updateStudentPoint()
    {
        $student = Student::where('taylors_email', 'student1@top')->first();
        (new StudentPointService())->updateStudentPoint($student);
        dd('success');
    }

    public function deleteMediaOrientationTimetables()
    {
        (new OrientationService())->deleteMediaOrientationTimetables();
    }

    public function fixDataPreOrientationPoint($input)
    {
        $studentIds = StudentPreOrientation::select('student_id')
            ->groupBy('student_id')
            ->get()
            ->pluck('student_id')
            ->toArray();
        $students = Student::whereIn('id', $studentIds)->get();
        foreach ($students as $student) {
            (new StudentPointService())->updateStudentPoint($student);
        }
        dd($studentIds);
    }

    public function studentPreorientation()
    {
        $studentPreOrientation = StudentPreOrientation::create([
            'pre_orientation_id' => 5,
            'student_id' => 1,
            'completed' => true,
        ]);
        $student = (new StudentPointService())->updateStudentPoint($this->auth->student());
        dd($studentPreOrientation);
        // dd($this->auth->student());
        $countCompleted = $this->studentPreOrientation->getCompleted()->count();
        $countPreOrientation = $this->preOrientation->countPublished();
        $percentage = $countCompleted > 0 ? number_format(round((100 / $countPreOrientation) * $countCompleted)) : 0;
        $this->auth->student()->update([
            'pre_orientation_percentage' => $percentage
        ]);
        dd($percentage);
    }

    public function get()
    {
        $url = "https://forms.office.com/formapi/api/9188040d-6c67-4c5b-b112-36a304b66dad/users/00000000-0000-0000-0003-bffd63bae9d8/forms('DQSIkWdsW0yxEjajBLZtrQAAAAAAAAAAAAO__WO66dhUNUFRRzkwWlY0RkNJSlM3V0ExMklDT1JCWi4u')/responses";
        $response = Http::withToken('eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyIsImtpZCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyJ9.eyJhdWQiOiJodHRwczovL2Zvcm1zLm9mZmljZS5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9mOGNkZWYzMS1hMzFlLTRiNGEtOTNlNC01ZjU3MWU5MTI1NWEvIiwiaWF0IjoxNjg1MDc2ODQ5LCJuYmYiOjE2ODUwNzY4NDksImV4cCI6MTY4NTE2MzU0OSwiYWlvIjoiRTJaZ1lHaTdHckF0N0tXdjhTZnJ2Z3IxYnFGZkFBPT0iLCJhcHBpZCI6ImE0OTAwNWYzLTIyNTAtNGRlNS05Nzg0LTZhMTYyOGFiZmFiNiIsImFwcGlkYWNyIjoiMSIsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0L2Y4Y2RlZjMxLWEzMWUtNGI0YS05M2U0LTVmNTcxZTkxMjU1YS8iLCJyaCI6IjAuQVZVQU1lX04tQjZqU2t1VDVGOVhIcEVsV3RKWnBjbXJlaE5QcHUzbjZjVXE3SWNCQUFBLiIsInRpZCI6ImY4Y2RlZjMxLWEzMWUtNGI0YS05M2U0LTVmNTcxZTkxMjU1YSIsInV0aSI6ImZSYmlURXc4dVVDeDB2ZWhvQUpnQUEiLCJ2ZXIiOiIxLjAifQ.a0jC4C5XNfqIOO6ebCgF6AuKlUL1V7feXsZa-pcO2gQL3nXLSri1b5_cZLvTsQqLdu7Go8Z5BwNSJm3OSNycGJRSWKZVyS5qDSwCaN__KBl2UTYZdd-xYy-85f_-n1rUlZ7ceR9sLaqJQecIGrX6Ug6__ryaSEXlP_XsvfsysUwyMWMMVM3rsBK7FVpkhe4WhzdQoyQctCkGU_G9XeKWvhF28zquw271qpVtzIwfdUmiur0UHk_EslQGUX4Ytt9eNb6_gJM0ZsVYblnkb_MnsORLncu5VR_PZ-tJ_c3I_QwEWvlnI69BmiRAQYWhlUMJfEX7mmJSEg6kJDRzGOL3Tg');
        // if ($contentType != 'hidden') {
        $response = $response->contentType('application/json');
        // }
        // if ($acceptType != 'hidden') {
        $response = $response->accept('application/json');
        // }
        $response = $response->get($url);
        return $response;
    }

    protected function microsoft($input)
    {
        // $response = $this->get();
        // dd($response);
        $guzzle = new \GuzzleHttp\Client();
        $tenantId = config('services.microsoft.tenant');
        $clientId = config('services.microsoft.client_id');
        $clientSecret = config('services.microsoft.client_secret');
        $url = 'https://login.microsoftonline.com/' . $tenantId . '/oauth2/v2.0/token';
        $token = json_decode($guzzle->post($url, [
            'form_params' => [
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                // 'scope' => 'https://forms.office.com/.default',
                'scope' => 'user.read%20openid%20profile%20offline_access',
                'grant_type' => 'password',
                'username' => 'farihintalib89_hotmail.com',
                'password' => 'INkiller$89',
            ],
        ])
            ->getBody()
            ->getContents());
        $accessToken = $token->access_token;
        dd($accessToken);
        // $accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyIsImtpZCI6Ii1LSTNROW5OUjdiUm9meG1lWm9YcWJIWkdldyJ9.eyJhdWQiOiJodHRwczovL2Zvcm1zLm9mZmljZS5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC8zMTNjMGQ4OS02YjI5LTRlMzUtOWI2ZC1kM2M1MmIxYzZlYzcvIiwiaWF0IjoxNjg1MDg5Njg0LCJuYmYiOjE2ODUwODk2ODQsImV4cCI6MTY4NTA5NDY3NSwiYWNyIjoiMSIsImFpbyI6IkFXUUFtLzhUQUFBQTV5K1pMSXFVTVJ3NGE4UnFDUnZINHpoMEJlUzlXSHFzdkdndUFMUmtVb1YwUzgzc3F1Z3FGRHh0bjBMMUlkdFh0cjk4VE1oTUsrbktuK1h4UkdsK3hSb1U4ZHpIYUl5VnBoQjdDc3p0NzczRFJ2WmwvNHVIWWVUNy95c0ZWelg2IiwiYWx0c2VjaWQiOiIxOmxpdmUuY29tOjAwMDYwMDAwMkIwNDM2N0YiLCJhbXIiOlsicHdkIl0sImFwcGlkIjoiNThkM2Q5ODEtNjFjOC00YmM1LWIzZjYtMTk2YmNlNDQ4MDFlIiwiYXBwaWRhY3IiOiIxIiwiZW1haWwiOiJmYXJpaGludGFsaWI4OUBob3RtYWlsLmNvbSIsImZhbWlseV9uYW1lIjoiVGFsaWIiLCJnaXZlbl9uYW1lIjoiRmFyaWhpbiIsImlkcCI6ImxpdmUuY29tIiwiaXBhZGRyIjoiMTE1LjEzNS4yNy4xNjMiLCJuYW1lIjoiRmFyaWhpbiBUYWxpYiIsIm9pZCI6ImNlYjFiNzI3LTkwN2MtNDA4ZS1hOWMzLTRkMGQzMjQ2M2M4NCIsInB1aWQiOiIxMDAzMjAwMkE2NjdCRDFDIiwicmgiOiIwLkFVb0FpUTA4TVNsck5VNmJiZFBGS3h4dXg5SlpwY21yZWhOUHB1M242Y1VxN0llSkFBUS4iLCJzY3AiOiIuOWViYmY3MWYtZjM5Ny00M2Y0LTg2ZWItZDdjYTRmZmQzYjdlIiwic3ViIjoiakw4cXV2RXBPcDZuZFJqQ2RMSUFZVlJ1RG8tTTRMdXRMV0dmZnZVbTZoNCIsInRpZCI6IjMxM2MwZDg5LTZiMjktNGUzNS05YjZkLWQzYzUyYjFjNmVjNyIsInVuaXF1ZV9uYW1lIjoibGl2ZS5jb20jZmFyaWhpbnRhbGliODlAaG90bWFpbC5jb20iLCJ1dGkiOiIzdkFfNnJES3FrdUQ1dDdIV1JLWkFBIiwidmVyIjoiMS4wIiwid2lkcyI6WyI2MmU5MDM5NC02OWY1LTQyMzctOTE5MC0wMTIxNzcxNDVlMTAiLCJiNzlmYmY0ZC0zZWY5LTQ2ODktODE0My03NmIxOTRlODU1MDkiXSwieG1zX3BsIjoiZW4ifQ.ZWrNAqvQOvnE-VWG1w-QP5y_HES0TAxkal4wSYMayCD93Po-kBctaCUtCV8IixtG4PKqUz1FE9kZmx222yC-wqa1jdJDwK_PcIzRS5EPfCXY3Nrxa6X6NDS33IA1ECMsiJtEVgd6KMN3EzOGC4PvJceh5u3xspu6Qk8HtIVAwi9P-S0Y_pevVFMzeS135nwdtItPUfnQL2khZERKsuPdRyczbDXIZMxx1tc8T0fE9mT5q2UhE7q6SL2dPNGtfJepd8ea1RQA5f1bCWX-lowWI05BRVp54NoOM_dI8jZSMdk0Lj9kw31CQW62AK8mudDtaiIpGNuwKWMiG2iFr7xXQw';
        $userId = '00000000-0000-0000-0006-00002b04367f';
        $formId = 'DQSIkWdsW0yxEjajBLZtrQAAAAAAAAAAAAO__WO66dhUMUNFTk04VVlCUktJREs3MUFFRUI0N1kyMC4u';
        $tenantFormId = '9188040d-6c67-4c5b-b112-36a304b66dad';
        $url = "https://forms.office.com/formapi/api/{$tenantId}/users/{$userId}/forms('{$formId}')/responses";
        $url2 = "https://forms.office.com/formapi/api/{$tenantId}/users/{$userId}/forms";
        // $response = Http::withToken($accessToken);
        $response = (Http::withToken($accessToken))->get($url);
        $response2 = (Http::withToken($accessToken))->get($url2);
        dd($url2, $url, $accessToken, $response, $response2);
    }

    protected function addDummyStudent($input)
    {
        if (!empty($input['email'] ?? null)) {
            $email = $input['email'];
            $intake = Intake::where('is_active', true)
                ->orderBy('name', 'asc')
                ->inRandomOrder()
                ->first();
            $student = Student::where('intake_number', $intake->name)
                ->inRandomOrder()
                ->first()
                ->toArray();
            if (empty(User::where('email', $email)->first() ?? null)) {
                $student['taylors_email'] = $email;
                $student['student_id'] = rand(1111, 9999) . 'xxxx';
                $student['contact_no'] = $student['contact_number'];
                (new StudentService())->upserts([$student]);
                User::where('email', $email)->update([
                    'created_via' => null
                ]);
                dd('success');
            } else {
                dd('Email is been taken');
            }
        } else {
            dd('Email is required');
        }
    }

    protected function fixDataOrientationPoint($input)
    {
        Orientation::where('point_attendance', 0)->update([
            'point_attendance' => setting('orientation.point_attendance'),
            'point_rating' => setting('orientation.point_rating'),
            'point_max' => null,
            'point_min' => null,
            'is_point' => false,
        ]);
        $orientationAttendances = OrientationAttendance::with('orientation', 'student')->get();
        foreach ($orientationAttendances as $orientationAttendance) {
            $pointAttendance = $orientationAttendance->orientation->point_attendance;
            if (!empty($orientationAttendance->rating)) {
                $pointRating = $orientationAttendance->orientation->point_rating;
            } else {
                $pointRating = 0;
            }
            $orientationAttendance->update([
                'point' => $pointAttendance + $pointRating,
                'point_attendance' => $pointAttendance,
                'point_rating' => $pointRating,
            ]);
            (new StudentPointService())->updateStudentPoint($orientationAttendance->student);
        }
        dd('success');
    }

    public function login($email)
    {
        $user = User::where('email', $email)->firstOrFail();
        if ($user) {
            auth()->loginUsingId($user->id);
            return redirect(url("/"));
        }
    }

    protected function testPdf($input)
    {
        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->setOption('enable-javascript', true);
        $pdf->setPaper('a4');
        $pdf->setOption("enable-local-file-access", true);
        $pdf->setOption('javascript-delay', 3000);
        $pdf->setOption('disable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        $pdf->setOption('margin-top', '12.7mm');
        $pdf->setOption('margin-left', '12.7mm');
        $pdf->setOption('margin-right', '12.7mm');
        $pdf->setOption('margin-bottom', '12.7mm');
        $pdf->setOption('encoding', 'UTF-8');
        return $pdf->loadView('orientation.dummy-pdf')->stream();
    }

    protected function getViews($input)
    {
        (new CmsService())->getViews();
        dd('getViews');
    }

    protected function findCmsStudentLike($input)
    {
        (new CmsService())->findStudentLike($input['value'], $input['field'] ?? 'TAYLORS_EMAIL');
        dd('findCmsStudentLikeEmail');
    }

    public function testNotification()
    {
        $user = User::first();
        $orientation = Orientation::first();
        Notification::send($user, new OrientationApproveNotification($user, $orientation));
    }

    public function testLdapAccount()
    {
        $results = [];
        $dataStudent = [
            'type' => 'student',
            'username' => 'oas.stud1',
            'password' => 'oas@123',
        ];
        $results['authStudent'] = $authStudent = LdapService::auth($dataStudent['type'], $dataStudent['username'], $dataStudent['password']);
        $results['infoStudent'] = $infoStudent = LdapService::get('student', 'oas.stud1');
        $dataStaff = [
            'type' => 'staff',
            'username' => 'scip.staff01',
            'password' => 'scip.staff01',
        ];
        $results['authStaff'] = $authStaff = LdapService::auth($dataStaff['type'], $dataStaff['username'], $dataStaff['password']);
        $results['infoStaff'] = $infoStaff = LdapService::get('staff', 'scip.staff01');
        dd($results);
    }

    public function upsertStudent()
    {
        $students = (new StudentService())->upserts(Student::factory()->count(1)->make()->toArray());
        dd($students);
    }
}
