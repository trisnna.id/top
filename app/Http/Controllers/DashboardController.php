<?php

namespace App\Http\Controllers;

use App\Services\DashboardService;
use App\Services\Admin\DashboardService as AdminDashboardService;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index(Request $request)
    {
        if (auth()->user()->can('student')) {
            $service = new DashboardService();
        } elseif(auth()->user()->can('admin')) {
            $service = new AdminDashboardService();
        }else{
            abort(404);
        }
        return $service->index($request, auth()->user());
    }

    public function indexAdmin(Request $request)
    {
        $service = new AdminDashboardService();
        return $service->index($request, auth()->user());
    }
}
