<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);
        // $user = User::where('email', $request->email)->first();
        // if (!empty($user ?? null) && !$user->is_active) {
        //     throw new HttpResponseException(
        //         response()->json([
        //             'status' => '200',
        //             'type' => 'error',
        //             'popup' => 'toastr',
        //             'message' => __('Your account is not active.'),
        //             'title' => ''
        //         ]),
        //     );
        // }
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        return $response == Password::RESET_LINK_SENT ? $this->sendResetLinkResponse(
            $request,
            $response
        ) : $this->sendResetLinkFailedResponse(
            $request,
            $response
        );
    }

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return $request->wantsJson() ? response()->json([
            'message' => 'Successfully Send Password Reset to Email',
            'meta' => [
                'alert' => [
                    'component' => [
                        'name' => 'toastr',
                    ],
                    'type' => 'success',
                    'text' => 'Successfully Send Password Reset to Email',
                ],
            ],
        ]) : back()->with(
            'status',
            trans($response)
        );
    }
}
