<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Login\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Services\Integration\Taylors\LdapService;
use App\Services\Integration\Taylors\TopasService;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    use BaseServiceTrait;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return redirect(route('portal.index'))->with('toastr-warning', 'Unauthorized. Please Login.');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);
        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }
        return $request->wantsJson() ? response()->json([
            'message' => 'User login successfully',
            'meta' => [
                'alert' => [
                    'component' => [
                        'name' => 'toastr',
                    ],
                    'type' => 'success',
                    'title' => 'User login successfully!',
                    'text' => 'You are being redirected',
                ],
            ],
            'links' => [
                'redirect' => redirect()->intended($this->redirectPath())->getTargetUrl()
            ]
        ]) : redirect()->intended($this->redirectPath());
    }

    public function login(Request $request)
    {
        $input = $request->all();
        $this->validateRequest(new LoginRequest(), $input);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (
            method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)
        ) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        if ($this->attemptLogin($request)) {
            if ($request->hasSession()) {
                $request->session()->put('auth.password_confirmed_at', time());
            }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    protected function attemptLogin(Request $request)
    {
        $input = $request->all();
        if ($input['type'] == 1) { //topas account
            $user = User::whereHas('student', function ($query) use ($input) {
                $query->where('taylors_email', $input['username_email'])
                    ->orWhere('personal_email', 'LIKE', "%{$input['username_email']}%");
            })
                ->first();
            if (!empty($user ?? null)) {
                [$topasAuth, $topasAuthMessage] = TopasService::auth($input['username_email'], $input['password']);
                Log::channel('dailyintegration')->info(json_encode([
                    'action' => 'login using topas',
                    'input' => $input['username_email'],
                    'auth' => $topasAuth
                ]));
                if (!$topasAuth) {
                    $this->incrementLoginAttempts($request);
                    return $this->throwResponseException([$topasAuthMessage]);
                }
            }
            if ($topasAuth ?? false && !empty($user)) {
                if ($user->is_active) {
                    auth()->login($user, $request->boolean('remember'));
                    return true;
                } else {
                    $this->incrementLoginAttempts($request);
                    return $this->throwResponseException(['User inactive']);
                }
            } else {
                return false;
            }
        } elseif ($input['type'] == 2 && config('services.taylors.ldap.enabled')) { //taylors account
            $user = User::whereHas('student', function ($query) use ($input) {
                $query->where('id_number', $input['username_email'])->orWhere('taylors_email', $input['username_email']);
            })->first();
            if (!empty($user ?? null)) {
                $ldapAuth = LdapService::auth('student', $user->student->id_number, $input['password']);
                Log::channel('dailyintegration')->info(json_encode([
                    'action' => 'login using ldap',
                    'input' => $user->student->id_number,
                    'auth' => $ldapAuth
                ]));
                if (!$ldapAuth) {
                    $this->incrementLoginAttempts($request);
                    return $this->throwResponseException([__('auth.password_incorrect')]);
                }
            } else {
                $user = User::whereHas('staff', function ($query) use ($input) {
                    $query->where('id_number', $input['username_email'])->orWhere('taylors_email', $input['username_email']);
                })->first();
                if (!empty($user ?? null)) {
                    $ldapAuth = LdapService::auth('staff', $user->staff->id_number, $input['password']);
                    Log::channel('dailyintegration')->info(json_encode([
                        'action' => 'login using ldap',
                        'input' => $user->staff->id_number,
                        'auth' => $ldapAuth
                    ]));
                    if (!$ldapAuth) {
                        $this->incrementLoginAttempts($request);
                        return $this->throwResponseException([__('auth.password_incorrect')]);
                    }
                }
            }
            if ($ldapAuth ?? false && !empty($user)) {
                if ($user->is_active) {
                    auth()->login($user, $request->boolean('remember'));
                    return true;
                } else {
                    $this->incrementLoginAttempts($request);
                    return $this->throwResponseException(['User inactive']);
                }
            } else {
                return false;
            }
        } else {
            $user = User::where([
                'email' => $input['username_email']
            ])
                ->first();
            if (!empty($user) && !in_array($user->created_via, ['cms'])) {
                if ($user->is_active) {
                    $authUser = $this->guard()->attempt(
                        [
                            'email' => $input['username_email'],
                            'password' => $input['password'],
                        ],
                        $request->boolean('remember')
                    );
                    Log::channel('dailyintegration')->info(json_encode([
                        'action' => 'login using guest',
                        'input' => $input['username_email'],
                        'auth' => $authUser
                    ]));
                    if (!$authUser) {
                        $this->incrementLoginAttempts($request);
                        return $this->throwResponseException([__('auth.password_incorrect')]);
                    } else {
                    }
                    return $authUser;
                } else {
                    $this->incrementLoginAttempts($request);
                    return $this->throwResponseException(['User inactive']);
                }
            } else {
                return false;
            }
        }
    }
}
