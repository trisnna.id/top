<?php

namespace App\Http\Controllers\Auth\Microsoft;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Laravel\Socialite\Facades\Socialite;

/*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
 */

class LoginController extends Controller
{

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    public function showLoginForm()
    {
        return view('auth.admin.login');
    }

    public function login(Request $request)
    {
        return Socialite::driver('microsoft')
            ->setScopes(['https://forms.office.com/.default'])
            // ->setScopes(['forms.read','forms.readWrite'])
            // ->setScopes(['user.read'])
            ->redirect();
    }

    public function callback(Request $request)
    {
        // $user = Socialite::driver('microsoft')->user();
        // dd($user);
        // $a = Socialite::driver('microsoft')->getAvatar();
        // dd($a);
        $input = $request->all();
        if(empty($input['code'] ?? null)){
            dd($input);
        }
        // dd($input);
        $guzzle = new \GuzzleHttp\Client();
        $tenantId = config('services.microsoft.tenant');
        $clientId = config('services.microsoft.client_id');
        $clientSecret = config('services.microsoft.client_secret');
        $redirectUri = config('services.microsoft.redirect');

        // $tenantFormId = '9188040d-6c67-4c5b-b112-36a304b66dad';
        // $tenantFormId = '313c0d89-6b29-4e35-9b6d-d3c52b1c6ec7';
        $url = 'https://login.microsoftonline.com/' . $tenantId . '/oauth2/v2.0/token';
        $token = json_decode($guzzle->post($url, [
            'form_params' => [
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                // 'scope' => 'user.read',
                'scope' => 'https://forms.office.com/.default',
                'code' => $input['code'],
                'redirect_uri' => $redirectUri,
                'grant_type' => 'authorization_code',
                'state' => $input['state'],
            ],
        ])
            ->getBody()
            ->getContents());
        $accessToken = $token->access_token;


        // dd($accessToken);
        // $userId = '00000000-0000-0000-0003-bffd63bae9d8';
        $url2 = "https://forms.office.com/formapi/api/forms";
        $url1 = "https://forms.office.com/formapi/api/9188040d-6c67-4c5b-b112-36a304b66dad/users/00000000-0000-0000-0006-00002b04367f/forms('DQSIkWdsW0yxEjajBLZtrQAAAAAAAAAAAAYAACsENn9UNU1EWjBGSjhDS1VXR0haNDZZTk9ROE9PRS4u')/responses";
        $response2 = (Http::withToken($accessToken))->get($url2);
        $response1 = (Http::withToken($accessToken))->get($url1);
        dd($input, $accessToken, $response2, $response2->body(),$response1, $response1->body());

        dd($accessToken, $response->body());

        $state = json_decode(html_entity_decode($request->state));
        if (!empty($request->error) && $request->error == 'access_denied' && !empty($state
            ?? null)) {
            if (!empty($state->cart_amazon ?? null)) {
                return redirect(route('carts.amazon.index'));
            } elseif (!empty($state->cart_merch ?? null)) {
                return redirect(route('carts.merch.index'));
            }
        } else {
            if (!empty($state->lt) && $state->lt == 'sp') {
                return $this->handleSPCallback($state);
            } elseif (!empty($state->action ?? null) && $state->action === 'reauthorize') {
                $user = Socialite::driver('amazon')->stateless()->user();
                return $this->reauthorizeAmazonCallback($state, $user);
            } else {
                $user = Socialite::driver('amazon')->stateless()->user();
                if (!empty($state->amazon_setup ?? null)) {
                    $order = OrderRepository::findOrFailByUuid($state->amazon_setup);
                    OrdermetaRepository::updateOrCreate(
                        [
                            'order_id' => $order->id,
                            'key' => 'amazon_access_token'
                        ],
                        ['value' => $user->token]
                    );
                    OrdermetaRepository::updateOrCreate(
                        [
                            'order_id' => $order->id,
                            'key' => 'amazon_access_token_expire_at'
                        ],
                        ['value' => Carbon::now()->addSeconds(($user->expiresIn - 60))->toDateTimeString()]
                    );
                    OrdermetaRepository::updateOrCreate(
                        [
                            'order_id' => $order->id,
                            'key' => 'amazon_refresh_token'
                        ],
                        ['value' => $user->refreshToken]
                    );
                    OrdermetaRepository::updateOrCreate([
                        'order_id' => $order->id,
                        'key' => 'amazon_name'
                    ], ['value' => $user->name]);
                    OrdermetaRepository::updateOrCreate([
                        'order_id' => $order->id,
                        'key' => 'amazon_email'
                    ], ['value' => $user->email]);
                    return redirect(route(
                        'account.setups.amazon.show',
                        $state->amazon_setup
                    ));
                } elseif (!empty($state->cart_amazon ?? null)) {
                    $amazonId = MarketplaceType::findOrFailByName('amazon')->id;
                    $cart = Cart::findOrFailByUuid($state->cart_amazon);
                    if (AmazonAccount::where(['email' => $user->email, 'marketplace_type_id' => $amazonId])->exists()) {
                        $cart->update(['step' => 2]);
                        Cartmeta::where(['cart_id' => $cart->id])->delete();
                        Cartmeta::updateOrCreate(['cart_id' => $cart->id, 'key' => 'amazon_name'], ['value' => $user->name]);
                        Cartmeta::updateOrCreate(['cart_id' => $cart->id, 'key' => 'is_available_to_add'], ['value' => 'false']);
                    } else {
                        Cartmeta::updateOrCreate(['cart_id' => $cart->id, 'key' => 'is_available_to_add'], ['value' => 'true']);
                    }
                    Cartmeta::updateOrCreate(['cart_id' => $cart->id, 'key' => 'amazon_access_token'], ['value' => $user->token]);
                    Cartmeta::updateOrCreate(['cart_id' => $cart->id, 'key' => 'amazon_access_token_expire_at'], ['value' => Carbon::now()->addSeconds(($user->expiresIn - 60))->toDateTimeString()]);
                    Cartmeta::updateOrCreate(['cart_id' => $cart->id, 'key' => 'amazon_refresh_token'], ['value' => $user->refreshToken]);
                    Cartmeta::updateOrCreate(['cart_id' => $cart->id, 'key' => 'amazon_name'], ['value' => $user->name]);
                    Cartmeta::updateOrCreate(['cart_id' => $cart->id, 'key' => 'amazon_email'], ['value' => $user->email]);
                    return redirect(route('carts.amazon.index'));
                    //                    $request->request->add(['lt' => 'sp']);
                    //                    return Socialite::driver('spAmazon')->stateless()->loginSellerSentral($request, $state->market ?? 'na');
                } elseif (!empty($state->cart_merch ?? null)) {
                    $amazonId = MarketplaceType::findOrFailByName('merch')->id;
                    $cart = Cart::findOrFailByUuid($state->cart_merch);
                    if (AmazonAccount::where(['email' => $user->email, 'marketplace_type_id' => $amazonId])->exists()) {
                        $cart->update(['step' => 2]);
                        Cartmeta::where(['cart_id' => $cart->id])->delete();
                        Cartmeta::updateOrCreate(
                            ['cart_id' => $cart->id, 'key' => 'amazon_name'],
                            ['value' => $user->name]
                        );
                        Cartmeta::updateOrCreate(
                            ['cart_id' => $cart->id, 'key' => 'is_available_to_add'],
                            ['value' => 'false']
                        );
                    } else {
                        Cartmeta::updateOrCreate(
                            ['cart_id' => $cart->id, 'key' => 'is_available_to_add'],
                            ['value' => 'true']
                        );
                        Cartmeta::updateOrCreate(
                            ['cart_id' => $cart->id, 'key' => 'amazon_access_token'],
                            ['value' => $user->token]
                        );
                        Cartmeta::updateOrCreate(
                            ['cart_id' => $cart->id, 'key' => 'amazon_access_token_expire_at'],
                            ['value' => Carbon::now()->addSeconds(($user->expiresIn
                                - 60))->toDateTimeString()]
                        );
                        Cartmeta::updateOrCreate(
                            ['cart_id' => $cart->id, 'key' => 'amazon_refresh_token'],
                            ['value' => $user->refreshToken]
                        );
                        Cartmeta::updateOrCreate(
                            ['cart_id' => $cart->id, 'key' => 'amazon_name'],
                            ['value' => $user->name]
                        );
                        Cartmeta::updateOrCreate(
                            ['cart_id' => $cart->id, 'key' => 'amazon_email'],
                            ['value' => $user->email]
                        );
                    }
                    return redirect(route('carts.merch.index'));
                }
            }
        }
        return redirect()->back();
    }

    /**
     * Check user has any permission related to admin
     * @param type $request
     * @return type
     */
    public function checkAdmin($request)
    {
        $user = $this->userService->getRepository()->where(
            'email',
            $request->email
        )->first();
        if ($user->hasAnyPermission(['uac'])) {
            return true;
        } else {
            if (auth()->check()) {
                auth()->logout();
            }
            return false;
        }
    }

    public function sendFailedCheckAdminResponse($request)
    {
        throw ValidationException::withMessages([
            $this->username() => ['An authorised user'],
        ]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        if ($response = $this->loggedOut($request)) {
            return $response;
        }
        return $request->wantsJson() ? response()->json([
            'status' => '200', 'type' => 'success',
            'title' => 'Success',
            'message' => 'Successfully Logout', 'callback' => 'redirect', 'url' => url(route('admin.login'))
        ])
            : redirect(route('admin.login'));
    }
}
