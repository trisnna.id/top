<?php

namespace App\Http\Controllers\Api\PreOrientation;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\View\Components\PreOrientation\Quiz;
use App\Repositories\Contracts\CheckpointRepository;
use App\Repositories\Contracts\PreOrientationRepository;
use App\View\Components\Completion\PreOrientationProgress;
use App\Http\Requests\PreOrientation\UpdateComponentRequest;
use App\Repositories\Contracts\PreOrientationQuizRepository;
use App\Repositories\Contracts\StudentPreOrientationRepository;

class UpdateComponentController extends Controller
{
    public function __invoke(UpdateComponentRequest $request)
    {
        if ($request->has('state')) {
            return response()->json([
                'progress' => $this->renderProgress($request),
                'checkpoint' => $this->renderCheckpoint($request)
            ]);
        }
        return response()->json([
            'quiz' => $this->renderQuiz($request),
            'progress' => $this->renderProgress($request),
            'checkpoint' => $this->renderCheckpoint($request)
        ]);
    }

    public function renderQuiz($request) {
        $preOrientationQuizRepository = app(PreOrientationQuizRepository::class);
        $studentPreOrientationRepository = app(StudentPreOrientationRepository::class);

        $shuffled = json_decode($request->input('shuffled'), true);
        $shuffled = collect($shuffled);

        $quizComponent = new Quiz(
            $preOrientationQuizRepository,
            $studentPreOrientationRepository,
            $request->input('key'),
            $request->input('title'),
            $request->input('milestone'),
            $request->input('target'),
        );
        return $quizComponent->renderHtml($shuffled);
    }

    public function renderProgress($request) {
        // render component from App\View\Components\Completion\PreOrientationProgress.php
        $studentPreOrientationRepository = app(StudentPreOrientationRepository::class);
        $preOrientationRepository = app(PreOrientationRepository::class);

        $countCompleted = $studentPreOrientationRepository->getCompleted()->count();
        $countPreOrientation = $preOrientationRepository->countPublished();
        $value = $countCompleted > 0 ? round((100 / $countPreOrientation) * $countCompleted) : 0;

        $progressComponent = new PreOrientationProgress(
            $studentPreOrientationRepository,
            $preOrientationRepository,
        );

        return $progressComponent->render()->render();
    }

    public function renderCheckpoint($request) {
        // render view from resources/views/student/includes/milestones.blade.php
        $checkpointRepository = app(CheckpointRepository::class);

        $milestones = $checkpointRepository->getStudentMilestone(['id', 'label', 'value']);
        $studentActivities = $milestones->map(function ($item) {
            return collect([
                'key' => $item->id,
                'activities' => $item->preOrientations
            ]);
        })->reduce(function ($collection, $milestone) {
            $collection->push(collect([
                'milestone' => $milestone->get('key'),
                'studentActivities' => $milestone->get('activities')->map(function ($item) {
                    return collect($item->students);
                })
            ]));
            return $collection;
        }, collect())->transform(function ($item) {
            return collect([
                'milestone' => $item->get('milestone'),
                'countActivities' => $item->get('studentActivities')->count(),
                'completed' => $item->get('studentActivities')->filter(function ($items) {
                    return $items->isNotEmpty();
                })->count() === $item->get('studentActivities')->count()
            ]);
        });

        return view('student.includes.milestones', [
            'milestones' => $milestones,
            'studentActivities' => $studentActivities
        ])->render();
    }
}
