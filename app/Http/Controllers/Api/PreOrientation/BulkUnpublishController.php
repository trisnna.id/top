<?php

namespace App\Http\Controllers\Api\PreOrientation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Http\Requests\PreOrientation\BulkUpdatePreOrientationStatusRequest;

class BulkUnpublishController extends Controller
{
    /**
     * Create pre orientation repository instance
     *
     * @var PreOrientationRepository
     */
    private PreOrientationRepository $preOrientation;

    /**
     * Create a new controller instance
     *
     * @param PreOrientationRepository $preOrientation sepmak
     */
    public function __construct(PreOrientationRepository $preOrientation)
    {
        $this->preOrientation = $preOrientation;
    }

    /**
     * Handle the incoming request.
     * 
     * @param  BulkUpdatePreOrientationStatusRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(BulkUpdatePreOrientationStatusRequest $request)
    {
        $ids = explode(',', $request->get('key'));
        $this->preOrientation->bulkUnpublish($ids);

        return response()->json([
            'data' => ['message' => sprintf('Pre-orientation is unpublished successfully.')]
        ], 200);
    }
}
