<?php

namespace App\Http\Controllers\Api\PreOrientation;

use App\Http\Controllers\Controller;
use App\Http\Requests\PreOrientation\UpdatePreOrientationStatusRequest;
use App\Repositories\Contracts\PreOrientationRepository;

class PublishController extends Controller
{
    /**
     * Create pre orientation repository instance
     *
     * @var PreOrientationRepository
     */
    private PreOrientationRepository $preOrientation;

    /**
     * Create a new controller instance
     *
     * @param PreOrientationRepository $preOrientation sepmak
     */
    public function __construct(PreOrientationRepository $preOrientation)
    {
        $this->preOrientation = $preOrientation;
    }

    /**
     * Handle the incoming request.
     *
     * @param  UpdatePreOrientationStatusRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(UpdatePreOrientationStatusRequest $request)
    {
        $entity = $this->preOrientation->publish($request->get('key'));

        return response()->json([
            'data' => ['message' => sprintf('Pre-orientation named %s is published successfully.', $entity->activity_name)]
        ], 200);
    }
}
