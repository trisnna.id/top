<?php

namespace App\Http\Controllers\Api\PreOrientation;

use App\Contracts\Entities\StudentPreOrientationEntity;
use App\Http\Controllers\Controller;
use App\Http\Requests\PreOrientation\UpdateActivityStudentAsCompleteRequest;
use App\Repositories\Contracts\PreOrientationQuizRepository;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Repositories\Contracts\StudentPreOrientationRepository;

class MarkCompleteController extends Controller
{
    /**
     * Create a new pre-orintation quiz repository instance
     *
     * @var PreOrientationQuizRepository
     */
    private PreOrientationQuizRepository $preorientationQuiz;

    /**
     * Create a new pre-orintation quiz repository instance
     *
     * @var PreOrientationRepository
     */
    private PreOrientationRepository $preorientation;

    /**
     * Create a new student pre-orientation repository instance
     *
     * @var StudentPreOrientationRepository
     */
    private StudentPreOrientationRepository $studentPreOrientation;

    /**
     * Undocumented function
     *
     * @param   PreOrientationQuizRepository    $preorientationQuiz
     * @param   PreOrientationRepository        $preorientation
     * @param   StudentPreOrientationRepository $studentPreOrientation
     */
    public function __construct(
        PreOrientationQuizRepository $preorientationQuiz,
        PreOrientationRepository $preorientation,
        StudentPreOrientationRepository $studentPreOrientation
    ) {
        $this->preorientationQuiz = $preorientationQuiz;
        $this->preorientation = $preorientation;
        $this->studentPreOrientation = $studentPreOrientation;
    }

    /**
     * Handle the incoming request.
     *
     * @param   UpdateActivityStudentAsCompleteRequest $request
     * @return  \Illuminate\Http\Response
     */
    public function __invoke(UpdateActivityStudentAsCompleteRequest $request)
    {
        $preorientation = $this->preorientation->first(
            $request->get(StudentPreOrientationEntity::FIELD_PREORIENTATION)
        );

        $preorientationQuiz = $this->preorientationQuiz->findByPreOrientation($preorientation->id);
        if ($preorientationQuiz->isNotEmpty()) {
            return response()->json([
                'data' => [
                    'title' => 'Oops!, Someting went wrong.',
                    'text' => sprintf(
                        'You need to complete the pre-orientation quiz first to make this %s activity completed.',
                        "<strong>{$preorientation->activity_name}</strong>"
                    )
                ]
            ], 400);
        }

        $this->studentPreOrientation->updateAsComplete($preorientation->id);

        return response()->json([
            'data' => [
                'title' => 'Hooray!',
                'text' => sprintf('You\'ve just completed the %s activity.', "<strong>{$preorientation->activity_name}</strong>")
            ]
        ], 200);
    }
}
