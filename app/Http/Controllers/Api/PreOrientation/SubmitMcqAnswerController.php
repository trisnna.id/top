<?php

namespace App\Http\Controllers\Api\PreOrientation;

use App\Contracts\Entities\StudentPreOrientationEntity;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Repositories\Contracts\StudentPreOrientationRepository;
use Illuminate\Http\Request;

class SubmitMcqAnswerController extends Controller
{
    /**
     * Create a new student pre-orientation repository instance
     *
     * @var StudentPreOrientationRepository
     */
    private StudentPreOrientationRepository $studentPreOrientation;

    /**
     * Create a new pre-orintation repository instance
     *
     * @var PreOrientationRepository
     */
    private PreOrientationRepository $preOrientation;

    /**
     * Create a new controller instance
     *
     * @param PreOrientationRepository          $preOrientation
     * @param StudentPreOrientationRepository   $studentPreOrientation
     */
    public function __construct(PreOrientationRepository $preOrientation, StudentPreOrientationRepository $studentPreOrientation)
    {
        $this->preOrientation = $preOrientation;
        $this->studentPreOrientation = $studentPreOrientation;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $studentPreorientation = $this->studentPreOrientation->createCompletedQuiz(
            $request->get('preorientation'), $request->get('mcq')
        );
        $preOrientation = $this->preOrientation->first($studentPreorientation->pre_orientation_id);

        return response()->json([
            'data' => [
                'score' => $studentPreorientation->score,
                'message' => sprintf('Yeay, you have completed the quiz %s.', $preOrientation->activity_name)
            ]
        ], 201);
    }
}
