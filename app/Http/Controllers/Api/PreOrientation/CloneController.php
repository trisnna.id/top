<?php

namespace App\Http\Controllers\Api\PreOrientation;

use App\Contracts\Entities\PreOrientationEntity;
use App\Http\Controllers\Controller;
use App\Http\Requests\PreOrientation\CloneRequest;
use App\Repositories\Contracts\PreOrientationQuizRepository;
use App\Repositories\Contracts\PreOrientationRepository;

class CloneController extends Controller
{
    /**
     * Pre-Orientation reposiutory instance
     *
     * @var PreOrientationRepository
     */
    private PreOrientationRepository $preOrientation;

    /**
     * Pre-Orientation reposiutory instance
     *
     * @var PreOrientationQuizRepository
     */
    private PreOrientationQuizRepository $preOrientationQuiz;

    /**
     * Create a new controller instance
     *
     * @param PreOrientationRepository $preOrientation
     * @param PreOrientationRepository $preOrientationQuiz
     */
    public function __construct(PreOrientationRepository $preOrientation, PreOrientationQuizRepository $preOrientationQuiz)
    {
        $this->preOrientation = $preOrientation;
        $this->preOrientationQuiz = $preOrientationQuiz;
    }

    /**
     * Handle the incoming request.
     *
     * @param  CLoneRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(CloneRequest $request)
    {
        $entity = $this->preOrientation->firstByUUID($request->uuid);

        if ($entity instanceof PreOrientationEntity) {
            $copied = $this->preOrientation->clone($entity);
            
            if ($copied instanceof PreOrientationEntity && $entity->quizs->count()) {
                $this->preOrientationQuiz->clone($entity->id, $copied->id);
            }

            return response()->json([
                'data' => ['message' => sprintf('Pre-orientation is cloned successfully.')]
            ], 201);
        }

        return response()->json([
            'data' => ['message' => sprintf('Pre-orientation not found')]
        ], 404);
    }
}
