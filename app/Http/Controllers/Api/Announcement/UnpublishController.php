<?php

namespace App\Http\Controllers\Api\Announcement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\AnnouncementRepository;
use App\Http\Requests\Announcement\UpdateAnnouncementStatusRequest;

class UnpublishController extends Controller
{
    /**
     * Create pre orientation repository instance
     *
     * @var AnnouncementRepository
     */
    private AnnouncementRepository $announcement;

    /**
     * Create a new controller instance
     *
     * @param AnnouncementRepository $announcement sepmak
     */
    public function __construct(AnnouncementRepository $announcement)
    {
        $this->announcement = $announcement;
    }

    /**
     * Handle the incoming request.
     *
     * @param  UpdateAnnouncementStatusRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(UpdateAnnouncementStatusRequest $request)
    {
        $entity = $this->announcement->unpublish($request->get('key'));

        return response()->json([
            'data' => ['message' => sprintf('Announcement named %s is un-published successfully.', $entity->name)]
        ], 200);
    }
}
