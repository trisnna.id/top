<?php

namespace App\Http\Controllers\Api\Announcement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\AnnouncementRepository;
use App\Http\Requests\Announcement\UpdateAnnouncementStatusRequest;

class PublishController extends Controller
{
    /**
     * Create pre orientation repository instance
     *
     * @var AnnouncementRepository
     */
    private AnnouncementRepository $announcement;

    /**
     * Create a new controller instance
     *
     * @param AnnouncementRepository $announcement sepmak
     */
    public function __construct(AnnouncementRepository $announcement)
    {
        $this->announcement = $announcement;
    }

    /**
     * Handle the incoming request.
     *
     * @param  UpdateAnnouncementStatusRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(UpdateAnnouncementStatusRequest $request)
    {
        $entity = $this->announcement->publish($request->get('key'));

        return response()->json([
            'data' => ['message' => sprintf('Announcement named %s is published successfully.', $entity->name)]
        ], 200);
    }
}
