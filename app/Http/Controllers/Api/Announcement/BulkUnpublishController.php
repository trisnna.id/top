<?php

namespace App\Http\Controllers\Api\Announcement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\AnnouncementRepository;
use App\Http\Requests\Announcement\BulkUpdateAnnouncementStatusRequest;

class BulkUnpublishController extends Controller
{
    /**
     * Create pre orientation repository instance
     *
     * @var AnnouncementRepository
     */
    private AnnouncementRepository $announcement;

    /**
     * Create a new controller instance
     *
     * @param AnnouncementRepository $announcement sepmak
     */
    public function __construct(AnnouncementRepository $announcement)
    {
        $this->announcement = $announcement;
    }

    /**
     * Handle the incoming request.
     * 
     * @param  BulkUpdateAnnouncementStatusRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(BulkUpdateAnnouncementStatusRequest $request)
    {
        $ids = explode(',', $request->get('key'));
        $this->announcement->bulkUnpublish($ids);

        return response()->json([
            'data' => ['message' => sprintf('Announcement is unpublished successfully.')]
        ], 200);
    }
}
