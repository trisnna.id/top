<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\GoogleAccountService;
use App\Traits\Controllers\BaseControllerTrait;
use Illuminate\Http\Request;

class GoogleAccountController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new GoogleAccountService();
    }

    public function shareEvent(Request $request)
    {
        $results = $this->service->shareEvent($request->all(), auth()->user());
        return $this->arrayResponse($results);
    }

    public function shareEvents(Request $request)
    {
        $results = $this->service->shareEvents($request->all(), auth()->user());
        return $this->arrayResponse($results);
    }
}
