<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\SurveyService;
use App\Traits\Controllers\BaseControllerTrait;

class SurveyController extends Controller
{

    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new SurveyService();
    }
}
