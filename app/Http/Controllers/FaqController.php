<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\FaqService;
use App\Traits\Controllers\BaseControllerTrait;

class FaqController extends Controller
{
    use BaseControllerTrait;

    public function __construct()
    {
        $this->service = new FaqService();
    }
}
