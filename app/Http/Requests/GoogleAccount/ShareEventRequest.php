<?php

namespace App\Http\Requests\GoogleAccount;

use Illuminate\Foundation\Http\FormRequest;

class ShareEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules($input = [])
    {
        return [
            'event' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'event.required' => 'The Event is required.',
        ];
    }
}
