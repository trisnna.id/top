<?php

namespace App\Http\Requests\Admin\Setting\Survey;

use Illuminate\Foundation\Http\FormRequest;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules($input = null)
    {
        $mimes = settingExtensionFormat(setting('survey.upload_extension'), '', ',');
        $max = setting('survey.upload_size') * 1024;
        return [
            'file' => ['required', "mimes:{$mimes}", "max:{$max}"],
        ];
    }

    public function messages()
    {
        $mimes = settingExtensionFormat(setting('survey.upload_extension'), '');
        $max = setting('survey.upload_size');
        return [
            'file.required' => 'The File field is required.',
            'file.mimes' => "The File must be a file of type: {$mimes}.",
            'file.max' => "The File must not be greater than {$max} MB.",
        ];
    }
}
