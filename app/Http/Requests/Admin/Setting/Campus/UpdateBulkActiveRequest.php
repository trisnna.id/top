<?php

namespace App\Http\Requests\Admin\Setting\Campus;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBulkActiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'checkbox' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'checkbox.required' => 'Selected Record(s) is required.',
        ];
    }
}
