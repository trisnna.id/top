<?php

namespace App\Http\Requests\Admin\Setting\Mail;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['required', 'max:100'],
            'subject' => ['required', 'max:100'],
            'content' => ['required', 'max:16000000'],
        ];
    }
}
