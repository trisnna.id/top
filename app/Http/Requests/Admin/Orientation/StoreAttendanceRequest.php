<?php

namespace App\Http\Requests\Admin\Orientation;

use Illuminate\Foundation\Http\FormRequest;

class StoreAttendanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id_number' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'id_number.required' => 'The Student ID field is required.',
        ];
    }
}
