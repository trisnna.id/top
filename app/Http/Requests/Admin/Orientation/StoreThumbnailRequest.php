<?php

namespace App\Http\Requests\Admin\Orientation;

use Illuminate\Foundation\Http\FormRequest;

class StoreThumbnailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules($input = null)
    {
        $mimes = settingExtensionFormat(setting('file.profile_avatar_extension'), '', ',');
        $max = setting('file.profile_avatar_size') * 1024;
        return [
            // 'thumbnail' => ["mimes:{$mimes}", "max:{$max}"],
            'thumbnail_hex' => ['nullable', 'string'],
        ];
    }

    public function messages()
    {
        $mimes = settingExtensionFormat(setting('file.profile_avatar_extension'), '');
        $max = setting('file.profile_avatar_size');
        return [
            'thumbnail.mimes' => "The Thumbnail must be a file of type: {$mimes}.",
            'thumbnail.max' => "The Thumbnail must not be greater than {$max} MB.",
        ];
    }
}
