<?php

namespace App\Http\Requests\Admin\Orientation;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            // 'thumbnail_hex' => ['required', 'string'],
            // // Intake
            // 'intake' => ['required', 'array'],
            // 'intake.*' => ['exists:intakes,id'],

            // // Faculty
            // 'faculty' => ['required', 'array'],
            // 'faculty.*' => ['exists:faculties,id'],

            // // School
            // 'school' => ['required', 'array'],
            // 'school.*' => ['exists:schools,id'],

            // // Programme
            // 'programme' => ['required', 'array'],
            // 'programme.*' => ['exists:programmes,id'],

            // // Mobility
            // 'mobility' => ['required', 'array'],
            // 'mobility.*' => ['exists:mobilities,id'],

            // 'level_study' => ['required', 'array'],
            // 'level_study.*' => ['exists:level_studies,id'],
        ];
    }

    // public function attributes()
    // {
    //     return [
    //         'thumbnail_hex' => 'thumbnail'
    //     ];
    // }

    public function messages()
    {
        return [
            'name.required' => 'The Activity field is required.',
            // 'thumbnail_hex' => 'The thumbnail is required'
        ];
    }
}
