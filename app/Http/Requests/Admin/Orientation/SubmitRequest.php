<?php

namespace App\Http\Requests\Admin\Orientation;

use Illuminate\Foundation\Http\FormRequest;

class SubmitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules($input = [])
    {
        $rules = [
            'name' => ['required'],
            // 'venue' => ['required'],
            'thumbnail_hex' => ['required', 'string'],
            'start_date' => ['required'],
            'start_time' => ['required'],
            'end_time' => ['required'],
            'orientation_type_id' => ['required'],
            'filters.intake' => ['required'],
            'filters.faculty' => ['required'],
            'filters.school' => ['required'],
            'filters.programme' => ['required'],
            'filters.level_study' => ['required'],
        ];
        if (!empty($input['is_rsvp'] ?? null)) {
            $rules = $rules + ['rsvp_capacity' => ['required']];
        }
        if (!empty($input['is_point'] ?? null)) {
            $rules = $rules + [
                'point_min' => ['required'],
                'point_max' => ['required'],
            ];
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'thumbnail_hex' => 'thumbnail'
        ];
    }

    public function messages($input = [])
    {
        $rules = [
            'name.required' => 'Orientation Activity: The Activity field is required.',
            'thumbnail_hex.required' => 'Orientation Activity: The Thumbnail is required',
            // 'venue.required' => 'Orientation Activity: The Venue field is required.',
            'start_date.required' => 'Orientation Activity: The Date of Activity field is required..',
            'start_time.required' => 'Orientation Activity: The Start Time of Activity field is required.',
            'end_time.required' => 'Orientation Activity: The End Time of Activity field is required.',
            'orientation_type_id.required' => 'Activity Filters: The Activity Type field is required.',
            'filters.intake.required' => 'Activity Filters: The Intakes field is required.',
            'filters.faculty.required' => 'Activity Filters: The Faculty field is required.',
            'filters.school.required' => 'Activity Filters: The School field is required.',
            'filters.programme.required' => 'Activity Filters: The Programmes field is required.',
            'filters.level_study.required' => 'Activity Filters: The Student Level field is required.',
        ];
        if (!empty($input['is_rsvp'] ?? null)) {
            $rules = $rules + ['rsvp_capacity.required' => 'RSVP Settings: The Capacity / Vacancies field is required.'];
        }
        if (!empty($input['is_point'] ?? null)) {
            $rules = $rules + [
                'point_min.required' => 'Activity Points: The Minimum No. of Points field is required.',
                'point_max.required' => 'Activity Points: The Maximum No. of Points field is required.',
            ];
        }
        return $rules;
    }
}
