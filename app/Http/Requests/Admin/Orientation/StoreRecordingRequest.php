<?php

namespace App\Http\Requests\Admin\Orientation;

use Illuminate\Foundation\Http\FormRequest;

class StoreRecordingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules($input = null)
    {
        $mimes = settingExtensionFormat(setting('file.orientation_recording_extension'), '', ',');
        $max = setting('file.orientation_recording_size') * 1024;
        return [
            'attachment_recording_files.*' => ["mimes:{$mimes}", "max:{$max}"],
        ];
    }

    public function messages()
    {
        $mimes = settingExtensionFormat(setting('file.orientation_recording_extension'), '');
        $max = setting('file.orientation_recording_size');
        return [
            'attachment_recording_files.*.mimes' => "The File Attachment must be a file of type: {$mimes}.",
            'attachment_recording_files.*.max' => "The File Attachment must not be greater than {$max} MB.",
        ];
    }
}
