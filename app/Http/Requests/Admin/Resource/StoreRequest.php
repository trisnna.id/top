<?php

namespace App\Http\Requests\Admin\Resource;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'resource_type_id' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'The Title field is required.',
            'resource_type_id.required' => 'The Type field is required.',
        ];
    }
}
