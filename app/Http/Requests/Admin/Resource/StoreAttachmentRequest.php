<?php

namespace App\Http\Requests\Admin\Resource;

use Illuminate\Foundation\Http\FormRequest;

class StoreAttachmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules($input = null)
    {
        $mimes = settingExtensionFormat(setting('file.arrival_resource_extension'), '', ',');
        $max = setting('file.arrival_resource_size') * 1024;
        return [
            'attachment_files.*' => ["mimes:{$mimes}", "max:{$max}"],
        ];
    }

    public function messages()
    {
        $mimes = settingExtensionFormat(setting('file.arrival_resource_extension'), '');
        $max = setting('file.arrival_resource_size');
        return [
            'attachment_files.*.mimes' => "The File Attachment must be a file of type: {$mimes}.",
            'attachment_files.*.max' => "The File Attachment must not be greater than {$max} MB.",
        ];
    }
}
