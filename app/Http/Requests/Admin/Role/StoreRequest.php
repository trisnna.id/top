<?php

namespace App\Http\Requests\Admin\Role;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'name' => ['unique:roles,name'],
            'permissions' => ['required', 'array'],
            'is_active' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'The Name of Role field is required.',
            'name.unique' => 'The Name of Role has already been taken.',
            'permissions.required' => 'The Permissions field is required.',
        ];
    }
}
