<?php

namespace App\Http\Requests\Admin\Role;

use App\Http\Requests\Admin\Role\StoreRequest;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['required'],
            'permissions' => ['required', 'array'],
            'is_active' => ['required'],
        ];
    }
}
