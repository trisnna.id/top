<?php

namespace App\Http\Requests\Admin\OrientationRsvp;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules($input = [])
    {
        if ($input['is_go']) {
            return [
                'is_rsvp' => ['required', 'in:true,1'],
                'rsvp_current_capacity' => ['required', 'integer', "lt:{$input['rsvp_capacity']}"],
            ];
        } else {
            return [
                'is_rsvp' => ['required', 'in:true,1'],
            ];
        }
    }

    public function messages()
    {
        return [
            'is_rsvp.required' => 'The Orientation RSVP Setting must set to true.',
            'is_rsvp.in' => 'The Orientation RSVP Setting must set to true.',
            'rsvp_current_capacity.lt' => 'This activity is fully occupied.',
        ];
    }
}
