<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Admin\Role\StoreRequest;

class StoreAdminRequest extends StoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'username_email' => ['required'],
            'role_name' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'username_email.required' => 'The Staff ID / Email field is required.',
            'role_name.required' => 'The Role field is required.',
        ];
    }
}
