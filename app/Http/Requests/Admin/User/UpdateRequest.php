<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Admin\Role\StoreRequest;

class UpdateRequest extends StoreRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'role_name' => ['required'],
            'is_active' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'role_name.required' => 'The Role field is required.',
        ];
    }
}
