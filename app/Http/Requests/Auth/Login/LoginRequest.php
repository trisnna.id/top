<?php

namespace App\Http\Requests\Auth\Login;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    protected $type;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules($input = [])
    {
        $this->type = $input['type'] ?? null;
        if ($input['type'] == 2) {
            $rules = [
                'username_email' => ['required', 'string'],
                'password' => ['required', 'string'],
            ];
        } else {
            $rules = [
                'username_email' => ['required', 'email'],
                'password' => ['required', 'string'],
            ];
        }
        return $rules;
    }

    public function messages()
    {
        if ($this->type == 2) {
            $messages = [
                'username_email.required' => 'The User ID field is required.',
            ];
        } else {
            $messages = [
                'username_email.required' => 'The Email field is required.',
                'username_email.email' => 'The Email must be a valid email address.',
            ];
        }
        return $messages;
    }
}
