<?php

namespace App\Http\Requests\Announcement;

use Illuminate\Support\Facades\DB;
use App\Services\AuthService;
use Illuminate\Foundation\Http\FormRequest;

class BulkUpdateAnnouncementStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        $auth = app()->make(AuthService::class);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'key' => [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    $ids = explode(',', $value);
                    $count = count($ids);
                    $exists = DB::table('announcements')->whereIn('id', $ids)->count();
                    if ($count !== $exists) {
                        $fail('The selected ' . $attribute . ' is invalid.');
                    }
                },
            ],
        ];
    }
}
