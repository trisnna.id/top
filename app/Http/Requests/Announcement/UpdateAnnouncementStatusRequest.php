<?php

namespace App\Http\Requests\Announcement;

use App\Services\AuthService;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\WithCustomValidationResponse;

class UpdateAnnouncementStatusRequest extends FormRequest
{
    use WithCustomValidationResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $auth = app()->make(AuthService::class);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'key' => ['required', 'exists:announcements,uuid']
        ];
    }
}
