<?php

namespace App\Http\Requests\Announcement;

use App\Services\AuthService;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use App\Contracts\Entities\AnnouncementEntity;

class StoreAnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth()->user();
        return $user->can('admin');

        $authService = new AuthService();
        return $authService->staff() ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            // 'thumbnail' => ['nullable', 'file', 'mimes:jpg,jpeg,png,webp', 'max:2048'],
            'thumbnail_hex' => ['nullable', 'string'],

            // Nationality
            'nationality' => ['required', 'array'],
            'nationality.*' => ['exists:localities,id'],

            // Intake
            'intake' => ['required', 'array'],
            'intake.*' => ['exists:intakes,id'],

            // Campus
            'campus' => ['required', 'array'],
            'campus.*' => ['exists:campuses,id'],

            // Programme
            'programme' => ['required', 'array'],
            'programme.*' => ['exists:programmes,id'],

            'content' => ['required', 'string'],
            AnnouncementEntity::FIELD_SEQUENCE => [
                'nullable',
                'numeric',
                'min:1',
                sprintf('unique:%s,%s,NULL,id,deleted_at,NULL', AnnouncementEntity::TABLE, AnnouncementEntity::FIELD_SEQUENCE)
            ]
        ];
    }
}
