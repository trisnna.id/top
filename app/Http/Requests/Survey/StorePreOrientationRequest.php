<?php

namespace App\Http\Requests\Survey;

use Illuminate\Foundation\Http\FormRequest;

class StorePreOrientationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // return [
        //     'q1' => [
        //         'required',
        //         'string'
        //     ],
        //     'q2' => [
        //         'required',
        //         'string',
        //         'in:Malaysian,International'
        //     ],
        //     'q3' => [
        //         'required',
        //         'string',
        //         'exists:faculties,id'
        //     ],
        //     'q4' => [
        //         'required',
        //         'string',
        //         'exists:programmes,id'
        //     ],
        //     'q5-1' => [
        //         'required',
        //         'string',
        //         'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
        //     ],
        //     'q5-2' => [
        //         'required',
        //         'string',
        //         'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
        //     ],
        //     'q5-3' => [
        //         'required',
        //         'string',
        //         'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
        //     ],
        //     'q5-4' => [
        //         'required',
        //         'string',
        //         'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
        //     ],
        //     'q5-5' => [
        //         'required',
        //         'string',
        //         'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
        //     ],
        //     'q5-6' => [
        //         'required',
        //         'string',
        //         'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
        //     ],
        //     'q5-7' => [
        //         'required',
        //         'string',
        //         'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
        //     ],
        //     'q5-8' => [
        //         'required',
        //         'string',
        //         'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
        //     ],
        //     'q6' => [
        //         'required',
        //         'string',
        //         'in:Yes,No'
        //     ],
        // ];
        return [
            'q1' => [
                'required',
                'string',
                'in:Malaysian,International'
            ],
            'q2-m-1' => [
                'required_if:q1,Malaysian',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-m-2' => [
                'required_if:q1,Malaysian',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-m-3' => [
                'required_if:q1,Malaysian',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-m-4' => [
                'required_if:q1,Malaysian',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-m-5' => [
                'required_if:q1,Malaysian',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-1' => [
                'required_if:q1,International',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-2' => [
                'required_if:q1,International',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-3' => [
                'required_if:q1,International',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-4' => [
                'required_if:q1,International',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-5' => [
                'required_if:q1,International',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-6' => [
                'required_if:q1,International',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-7' => [
                'required_if:q1,International',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-8' => [
                'required_if:q1,International',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-9' => [
                'required_if:q1,International',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q2-10' => [
                'required_if:q1,International',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
        ];
    }
}
