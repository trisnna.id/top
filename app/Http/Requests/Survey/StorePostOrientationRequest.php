<?php

namespace App\Http\Requests\Survey;

use Illuminate\Foundation\Http\FormRequest;

class StorePostOrientationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'q1' => [
                'required',
                'string',
                'in:Extremely useful,Somewhat useful,Neutral,Somewhat not useful,Extremely not useful',
            ],
            'q2' => [
                'required',
                'integer',
                'between:1,5',
            ],
            'q3' => [
                'required',
                'integer',
                'between:1,5',
            ],
            'q4-1' => [
                'required',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q4-2' => [
                'required',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q4-3' => [
                'required',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q4-4' => [
                'required',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q4-5' => [
                'required',
                'string',
                'in:Not at all concerned,Slightly concerned,Somewhat concerned,Moderately concerned,Extremely concerned'
            ],
            'q5' => [
                'required',
                'string'
            ],
            'q6' => [
                'required',
                'integer',
                'between:1,10',
            ],
        ];
    }
}
