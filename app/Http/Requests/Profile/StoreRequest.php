<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules($input = null)
    {
        $mimes = settingExtensionFormat(setting('file.profile_avatar_extension'), '', ',');
        $max = setting('file.profile_avatar_size') * 1024;
        return [
            'avatar' => ['required', "mimes:{$mimes}", "max:{$max}"],
        ];
    }

    public function messages()
    {
        $mimes = settingExtensionFormat(setting('file.profile_avatar_extension'), '');
        $max = setting('file.profile_avatar_size');
        return [
            'avatar.required' => 'The Avatar field is required.',
            'avatar.mimes' => "The Avatar must be a file of type: {$mimes}.",
            'avatar.max' => "The Avatar must not be greater than {$max} MB.",
        ];
    }
}
