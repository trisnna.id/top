<?php

namespace App\Http\Requests;

use App\Rules\AllowLocalityOnly;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePreOrientationRequest extends FormRequest
{
    use WithCustomValidationResponse;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth()->user();

        return $user->can('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'activity_name' => ['required', 'max:100'],
            'category' => ['required', 'exists:checkpoints,id'],
            'nationality' => ['required', 'array'],
            'nationality.*' => ['exists:localities,id'],
            'effective_date' => ['nullable', 'date_format:Y-m-d'],
            'content' => ['required', 'string'],
            // 'thumbnail' => ['nullable', 'file', 'mimes:jpg,jpeg,png,webp', 'max:2048'],
            'thumbnail_hex' => ['required', 'string'],
            'quiz' => ['required', Rule::in(['1', 1, '0', '0'])],
            'mcq' => ['required_if:quiz,1', 'array'],
            
            // Intake
            'intake' => ['required', 'array'],
            'intake.*' => ['exists:intakes,id'],

            // Faculty
            'faculty' => ['required', 'array'],
            'faculty.*' => ['exists:faculties,id'],

            // School
            'school' => ['required', 'array'],
            'school.*' => ['exists:schools,id'],

            // Programme
            'programme' => ['required', 'array'],
            'programme.*' => ['exists:programmes,id'],

            // Mobility
            'mobility' => ['required', 'array'],
            'mobility.*' => ['exists:mobilities,id'],
        ];
    }

    public function attributes()
    {
        return [
            'thumbnail_hex' => 'thumbnail'
        ];
    }

    public function messages()
    {
        return [
            'thumbnail_hex' => 'The thumbnail is required'
        ];
    }
}
