<?php

namespace App\Http\Requests\Orientation;

use Illuminate\Foundation\Http\FormRequest;

class UpsertAttendanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules($input = [])
    {
        return [
            'student_id' => ['required'],
            'orientation_id' => ['required'],
            'attendance_qrcode' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'student_id.required' => 'Not a valid format.',
            'orientation_id.required' => 'Not a valid format.',
            'attendance_qrcode.required' => 'Please Scan Attendance QR Code.',
        ];
    }
}
