<?php

namespace App\Http\Requests\PreOrientation;

use App\Contracts\Entities\StudentPreOrientationEntity;
use Illuminate\Foundation\Http\FormRequest;

class UpdateActivityStudentAsCompleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            StudentPreOrientationEntity::FIELD_PREORIENTATION => ['required', 'exists:pre_orientations,id']
        ];
    }
}
