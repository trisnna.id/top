<?php

namespace App\Http\Requests\PreOrientation;

use App\Http\Requests\WithCustomValidationResponse;
use App\Services\AuthService;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePreOrientationStatusRequest extends FormRequest
{
    use WithCustomValidationResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $auth = app()->make(AuthService::class);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'key' => ['required', 'exists:pre_orientations,uuid']
        ];
    }
}
