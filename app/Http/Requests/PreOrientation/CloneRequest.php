<?php

namespace App\Http\Requests\PreOrientation;

use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CloneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            PreOrientationEntity::FIELD_UUID => ['required', 'exists:pre_orientations'],
            'action' => ['required', Rule::in(['clone'])]
        ];
    }
}
