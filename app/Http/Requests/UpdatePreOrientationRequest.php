<?php

namespace App\Http\Requests;

use App\Services\AuthService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePreOrientationRequest extends FormRequest
{
    use WithCustomValidationResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $auth = app()->make(AuthService::class);

        return $auth->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'activity_name' => ['nullable', 'max:100'],
            'category' => ['nullable', 'exists:checkpoints,id'],
            'nationality' => ['nullable', 'array'],
            'nationality.*' => ['exists:localities,id'],
            'effective_date' => ['nullable', 'date_format:Y-m-d'],
            'content' => ['nullable', 'string'],
            // 'thumbnail' => ['nullable', 'file', 'mimes:jpg,jpeg,png,webp'],
            'thumbnail_hex' => ['nullable', 'string'],
            'quiz' => ['nullable', Rule::in(['1', 1, '0', '0'])],
            'mcq' => ['required_if:quiz,1', 'array'],
            
            // Intake
            'intake' => ['required', 'array'],
            'intake.*' => ['exists:intakes,id'],

            // Faculty
            'faculty' => ['required', 'array'],
            'faculty.*' => ['exists:faculties,id'],

            // School
            'school' => ['required', 'array'],
            'school.*' => ['exists:schools,id'],

            // Programme
            'programme' => ['required', 'array'],
            'programme.*' => ['exists:programmes,id'],

            // Mobility
            'mobility' => ['required', 'array'],
            'mobility.*' => ['exists:mobilities,id'],
        ];
    }
}
