<?php declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;


trait WithCustomValidationResponse
{
    /**
    * Get the error messages for the defined validation rules.*
    *
    * {@override}
    */
    protected function failedValidation(Validator $validator)
    {
        if (request()->wantsJson()) {
            throw new HttpResponseException(response()->json([
                'errors' => $validator->errors()->toArray(),
                'status' => true
            ], 422));
        }

        return parent::failedValidation($validator);
    }
}
