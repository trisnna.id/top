<?php

namespace App\Http\Middleware;

use Closure;

class Devtool
{

    public function handle($request, Closure $next)
    {
        if ($this->isAuthCheckAdmin() || config('app.debug')) {
            return $next($request);
        }
        abort(404);
    }

    protected function isAuthCheckAdmin()
    {
        if (auth()->check() && auth()->user()->email == 'admin@top') {
            return true;
        } else {
            return false;
        }
    }
}
