<?php

namespace App\Vendors\Yajra;

use App\Constants\ActiveInactiveConstant;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;
use JShrink\Minifier;
use Yajra\DataTables\Services\DataTable;

class BaseDataTable extends DataTable
{
    protected $template = '';
    protected $tableClass = 'table table-bordered table-striped align-middle table-row-dashed fs-7 gy-5 th-text-uppercase th-fw-bold w-100';
    public $tableName = '';

    public function generateMinifyScripts()
    {
        $parameters = $this->html()->generateJson();
        $configTemplate = $this->html()->config->get('datatables-html.script', 'datatables::script');
        $template = $this->template ?: $configTemplate;
        $template = $this->html()->view->make('datatables::script', ['editors' => $this->editors])->render();
        $scripts = new HtmlString(
            trim(sprintf($template, $this->html()->getTableAttribute('id'), $parameters))
        );
        return Minifier::minify($scripts);
    }

    protected function renderCheckboxAll($checkboxId)
    {
        return $this->renderColumn('checkbox-all', compact('checkboxId'), 'components.datatables');
    }

    protected function renderColumnControl($request, $instance, $checkboxId, $key = 'uuid')
    {
        $checked = null;
        $id = $instance->{$key};
        if (!empty($request->{$checkboxId} ?? null) && (in_array($id, explode(',', $request->{$checkboxId})) || in_array($id, explode(',', $request->{$checkboxId})))) {
            $checked = 'checked';
        }
        return $this->renderColumn('checkbox-single', compact('instance', 'id', 'checked', 'checkboxId'), 'components.datatables');
    }

    protected function renderColumnIsActive($instance, $name, $url = null, $titleConstant = 'title')
    {
        $activeInactiveConstant = ActiveInactiveConstant::getAvailableOptions();
        if ($instance->{$name} == ActiveInactiveConstant::ACTIVE) {
            $title = strtoupper($activeInactiveConstant[ActiveInactiveConstant::INACTIVE][$titleConstant]);
        } elseif ($instance->{$name} == ActiveInactiveConstant::INACTIVE) {
            $title = strtoupper($activeInactiveConstant[ActiveInactiveConstant::ACTIVE][$titleConstant]);
        }
        if (!empty($url)) {
            $instance['upsert'] = [
                'data' => [
                    'status' => $activeInactiveConstant[(int) $instance->{$name}][$titleConstant],
                    'color_class' => $activeInactiveConstant[(int) $instance->{$name}]['color']['class'],
                ],
                'swal' => [
                    'id' => "swal-blank",
                    'settings' => [
                        'icon' => 'warning',
                        'title' => "Are you sure to {$title}?",
                        'html' => "You can change it back later!",
                        'confirmButtonText' => 'Confirm',
                    ],
                    'axios' => [
                        'url' => $url,
                        'method' => 'put'
                    ]
                ]
            ];
            return $this->renderColumn('status', compact('instance'), 'components.datatables');
        } else {
            $instance['upsert'] = [];
            $colorClass = $activeInactiveConstant[(int) $instance->{$name}]['color']['class'];
            $label = $activeInactiveConstant[(int) $instance->{$name}][$titleConstant];
            return $this->renderColumn('status', compact('instance', 'colorClass', 'label'), 'components.datatables');
        }
    }

    protected function renderColumn($name, $data = [], $prefix = null)
    {
        if (!empty($prefix ?? null)) {
            $view = "{$prefix}.{$name}";
        } else {
            $name = Str::slug($name, '-');
            $replace1 = str_replace('App\\', '', get_class($this));
            $replace2 = str_replace('\\', '-', $replace1);
            $view = Str::slug($replace2, '.') . ".{$name}";
        }
        return view($view, $data)->render();
    }

    protected function makeDataScript(array $data)
    {
        $script = '';
        foreach ($data as $key => $value) {
            $script .= PHP_EOL . "data.{$key} = {$value};";
        }
        return $script;
    }

    public function script($datatable)
    {
        return new HtmlString(
            sprintf(
                'window.LaravelDataTables=window.LaravelDataTables||{};window.LaravelDataTables["%1$s"]=$("#%1$s").DataTable(%2$s);',
                $datatable->html()->getTableAttribute('id'),
                $datatable->html()->generateJson()
            )
        );
    }

    protected function getHtmlData()
    {
        return [
            'table' => "'{$this->tableId}'",
            'search.value' => "$('input#{$this->tableId}_search').val()",
        ];
    }

    protected function getColumns()
    {
        return [];
    }

    protected function getParameters()
    {
        return [
            'order' => [
                1, 'asc'
            ],
            'lengthMenu' => [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, 'All'],
            ],
            "pageLength" => 10,
            'responsive' => false,
            'searching' => true,
            'drawCallback' => "function (settings) { dtx.drawCallback(settings,'{$this->tableId}'); }",
        ];
    }

    public function html()
    {
        $data = $this->getHtmlData();
        $appendData = $this->makeDataScript($data);
        return $this->builder()
            ->setTableId($this->tableId)
            ->removeTableClass('table')
            ->addTableClass($this->tableClass)
            ->columns($this->getColumns())
            ->postAjax([
                'url' => '',
                'data' => "function(data) {{$appendData}}",
            ])
            ->parameters($this->getParameters());
    }

    protected function filename(): string
    {
        if (!empty($this->tableName ?? null)) {
            return Str::slug($this->tableName . '_' . date('Ymd'), "_");
        } else {
            return class_basename($this) . '_' . date('YmdHis');
        }
    }
}
