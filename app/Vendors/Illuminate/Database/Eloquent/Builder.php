<?php

namespace App\Vendors\Illuminate\Database\Eloquent;

use App\Traits\Database\Eloquent\CustomBuilderTrait;
use Illuminate\Database\Eloquent\Builder as BaseBuilder;

class Builder extends BaseBuilder
{
    use CustomBuilderTrait;
}
