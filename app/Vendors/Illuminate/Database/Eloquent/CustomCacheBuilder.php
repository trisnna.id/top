<?php

namespace App\Vendors\Illuminate\Database\Eloquent;

use App\Traits\Database\Eloquent\CustomBuilderTrait;
use Illuminate\Database\Eloquent\Builder as BaseBuilder;
use GeneaLabs\LaravelModelCaching\Traits\Buildable;
use GeneaLabs\LaravelModelCaching\Traits\BuilderCaching;
use GeneaLabs\LaravelModelCaching\Traits\Caching;

class CustomCacheBuilder extends BaseBuilder
{
    use CustomBuilderTrait;
    use Buildable;
    use BuilderCaching;
    use Caching;
}
