<?php

namespace App\Vendors\OwenIt\Auditing\Resolvers;

use Illuminate\Support\Facades\Request;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Contracts\Resolver;
use Stevebauman\Location\Facades\Location;

class LocationResolver implements Resolver
{
    public static function resolve(Auditable $auditable): string
    {
        if (Request::ip() == '127.0.0.1') {
            return 'localhost';
        } else {
            return Location::get(Request::ip())->countryName ?? '';
        }
    }
}
