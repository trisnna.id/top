<?php

namespace App\Vendors\OwenIt\Auditing\Resolvers;

use App\Models\OrientationAttendance;
use App\Models\StudentPreOrientation;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Contracts\Resolver;

class StudentIdResolver implements Resolver
{
    public static function resolve(Auditable $auditable)
    {
        if (in_array($auditable::class, [OrientationAttendance::class, StudentPreOrientation::class])) {
            if (!empty($auditable->student_id ?? null)) {
                return $auditable->student_id;
            }
        }
        return null;
    }
}
