<?php

namespace App\Observers;

use App\Models\Announcement;

class AnnouncementObserver
{
    use WithUUID;

    /**
     * Handle the Announcement before "created" event.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return void
     */
    public function creating(Announcement $announcement)
    {
        $this->assignUUID($announcement);
        $announcement->content_links = $announcement->content;
    }

    /**
     * Handle the Announcement "created" event.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return void
     */
    public function created(Announcement $announcement)
    {
        //
    }

    /**
     * Handle the Announcement before "updated" event.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return void
     */
    public function updating(Announcement $announcement)
    {
        $announcement->content_links = $announcement->content;
    }

    /**
     * Handle the Announcement "updated" event.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return void
     */
    public function updated(Announcement $announcement)
    {
        //
    }

    /**
     * Handle the Announcement "deleted" event.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return void
     */
    public function deleted(Announcement $announcement)
    {
        //
    }

    /**
     * Handle the Announcement "restored" event.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return void
     */
    public function restored(Announcement $announcement)
    {
        //
    }

    /**
     * Handle the Announcement "force deleted" event.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return void
     */
    public function forceDeleted(Announcement $announcement)
    {
        //
    }
}
