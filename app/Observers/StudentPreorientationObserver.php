<?php

namespace App\Observers;

use App\Models\Student;
use App\Models\StudentPreOrientation;
use App\Repositories\Contracts\PreOrientationQuizRepository;
use App\Repositories\Contracts\StudentPreOrientationPercentageRepository;
use App\Services\Admin\StudentPointService;
use App\Services\AuthService;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class StudentPreorientationObserver
{
    /**
     * Create a new auth service instance
     *
     * @var AuthService
     */
    private AuthService $auth;

    /**
     * Create a new pre-orientation quiz repository
     *
     * @var PreOrientationQuizRepository
     */
    private PreOrientationQuizRepository $preOrientationQuiz;

    /**
     * Create a new student pre-orientation percentage repository
     * 
     * @var StudentPreOrientationPercentageRepository
     */
    private StudentPreOrientationPercentageRepository $percentage;

    /**
     * Create a new observer instance
     *
     * @param   AuthService $auth
     */
    public function __construct(AuthService $auth, PreOrientationQuizRepository $preOrientationQuiz, StudentPreOrientationPercentageRepository $percentage)
    {
        $this->auth = $auth;
        $this->preOrientationQuiz = $preOrientationQuiz;
        $this->percentage = $percentage;
    }

    /**
     * Handle the PreOrientation before "created" event.
     *
     * @param  StudentPreOrientation  $preOrientation
     * @return void
     */
    public function creating(StudentPreOrientation $entity)
    {
        $countQuestions = $this->preOrientationQuiz->findByPreOrientation($entity->pre_orientation_id)->count();
        $entity->student_id = $this->auth->student(['id'])->id;
        if (($entity->quizzes instanceof Collection)) {
            $entity->score = $this->resolveScore($entity->quizzes, $countQuestions);
        } else {
            $entity->score = 0;
        }
        (new StudentPointService())->updateStudentPoint($this->auth->student());
        $this->percentage->create([
            'student_id' => $entity->student_id,
            'pre_orientation_id' => $entity->pre_orientation_id,
            'score' => $entity->score
        ]);
    }

    /**
     * Check if the student choose the wrong quiz answer
     *
     * @param   Collection  $answerKeys
     * @param   string|null $studentAnswer
     * @return  bool
     */
    protected function isWrongAnswer(Collection $answerKeys, ?string $studentAnswer): bool
    {
        return !in_array($studentAnswer, $answerKeys->toArray());
    }

    /**
     * Get the student quiz score base on the studen's correct
     * answers
     *
     * @param   Collection  $quizzes
     * @param   int         $countQuestions
     * @return  float
     */
    private function resolveScore(Collection $quizzes, int $countQuestions): float
    {
        return $quizzes->reduce(function (float $score, array $quiz) use ($countQuestions) {
            $question = $this->preOrientationQuiz->first($quiz['quiz_id']);
            $studentAnswers = Arr::get($quiz, 'answers', []);

            if ($this->isWrongAnswer($question->answers, Arr::get($studentAnswers, '0', ''))) {
                $score -= round(100 / $countQuestions);
            }

            return $score < 0 ? $score * -1 : $score;
        }, 100);
    }
}
