<?php

namespace App\Observers;

use App\Contracts\Entities\HasUuid;
use Illuminate\Database\Eloquent\Model;

trait WithUUID
{
    /**
     * Generate a `UUID` string for specified model
     *
     * @param  Model  $model
     * @return void
     */
    public function assignUUID(Model $model)
    {
        if ($model instanceOf HasUuid) {
            $model->uuid = uniqueUuidByModel($model);
        }
    }
}
