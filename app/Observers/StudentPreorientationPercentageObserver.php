<?php

namespace App\Observers;

use App\Models\StudentPreOrientationPercentage;

class StudentPreorientationPercentageObserver
{
    use WithUUID;

    /**
     * Handle the StudentPreOrientationPercentage before "created" event.
     *
     * @param  \App\Models\StudentPreOrientationPercentage  $spp
     * @return void
     */
    public function creating(StudentPreOrientationPercentage $spp)
    {
        $this->assignUUID($spp);
    }

    /**
     * Handle the StudentPreOrientationPercentage "created" event.
     *
     * @param  \App\Models\StudentPreOrientationPercentage  $spp
     * @return void
     */
    public function created(StudentPreOrientationPercentage $spp)
    {
        //
    }

    /**
     * Handle the StudentPreOrientationPercentage before "updated" event.
     *
     * @param  \App\Models\StudentPreOrientationPercentage  $spp
     * @return void
     */
    public function updating(StudentPreOrientationPercentage $spp)
    {
        //
    }

    /**
     * Handle the StudentPreOrientationPercentage "updated" event.
     *
     * @param  \App\Models\StudentPreOrientationPercentage  $spp
     * @return void
     */
    public function updated(StudentPreOrientationPercentage $spp)
    {
        //
    }

    /**
     * Handle the StudentPreOrientationPercentage "deleted" event.
     *
     * @param  \App\Models\StudentPreOrientationPercentage  $spp
     * @return void
     */
    public function deleted(StudentPreOrientationPercentage $spp)
    {
        //
    }

    /**
     * Handle the StudentPreOrientationPercentage "restored" event.
     *
     * @param  \App\Models\StudentPreOrientationPercentage  $spp
     * @return void
     */
    public function restored(StudentPreOrientationPercentage $spp)
    {
        //
    }

    /**
     * Handle the StudentPreOrientationPercentage "force deleted" event.
     *
     * @param  \App\Models\StudentPreOrientationPercentage  $spp
     * @return void
     */
    public function forceDeleted(StudentPreOrientationPercentage $spp)
    {
        //
    }
}
