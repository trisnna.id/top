<?php

namespace App\Observers;

use App\Models\PreOrientationQuiz;
use App\ValueObjects\McqOption;
use Illuminate\Support\Collection;

class PreOrientationQuizObserver
{
    /**
     * Handle the PreOrientationQuiz before "created" event.
     *
     * @param  \App\Models\PreOrientationQuiz  $preOrientationQuiz
     * @return void
     */
    public function creating(PreOrientationQuiz $preOrientationQuiz)
    {
        $this->resolveAnswers($preOrientationQuiz);
    }

    /**
     * Handle the PreOrientationQuiz "created" event.
     *
     * @param  \App\Models\PreOrientationQuiz  $preOrientationQuiz
     * @return void
     */
    public function created(PreOrientationQuiz $preOrientationQuiz)
    {
        //
    }

    /**
     * Handle the PreOrientationQuiz before "updated" event.
     *
     * @param  \App\Models\PreOrientationQuiz  $preOrientationQuiz
     * @return void
     */
    public function updating(PreOrientationQuiz $preOrientationQuiz)
    {
        $this->resolveAnswers($preOrientationQuiz);
    }

    /**
     * Handle the PreOrientationQuiz "updated" event.
     *
     * @param  \App\Models\PreOrientationQuiz  $preOrientationQuiz
     * @return void
     */
    public function updated(PreOrientationQuiz $preOrientationQuiz)
    {
        //
    }

    /**
     * Handle the PreOrientationQuiz "deleted" event.
     *
     * @param  \App\Models\PreOrientationQuiz  $preOrientationQuiz
     * @return void
     */
    public function deleted(PreOrientationQuiz $preOrientationQuiz)
    {
        //
    }

    /**
     * Handle the PreOrientationQuiz "restored" event.
     *
     * @param  \App\Models\PreOrientationQuiz  $preOrientationQuiz
     * @return void
     */
    public function restored(PreOrientationQuiz $preOrientationQuiz)
    {
        //
    }

    /**
     * Handle the PreOrientationQuiz "force deleted" event.
     *
     * @param  \App\Models\PreOrientationQuiz  $preOrientationQuiz
     * @return void
     */
    public function forceDeleted(PreOrientationQuiz $preOrientationQuiz)
    {
        //
    }

    /**
     * Resolve the pre-orientation quiz 'answers' values
     *
     * @param   PreOrientationQuiz  $entity
     * @return  void
     */
    private function resolveAnswers(PreOrientationQuiz $entity): void
    {
        $entity->answers = [];
        $options = $entity->options;

        if ($options instanceof Collection) {
            $entity->answers = $options->filter(function (McqOption $option) {
                return $option->isCorrect();
            })->values()->map(function (McqOption $option) {
                return $option->value;
            })->toArray();
        }
    }
}
