<?php

namespace App\Observers;

use App\Models\Intake;
use App\Models\School;
use App\Models\Faculty;
use App\Models\Mobility;
use App\Models\Programme;
use App\Models\PreOrientation;
use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Support\Collection;

class PreOrientationObserver
{
    use WithUUID;

    /**
     * Handle the PreOrientation before "created" event.
     *
     * @param  \App\Models\PreOrientation  $preOrientation
     * @return void
     */
    public function creating(PreOrientation $preOrientation)
    {
        $this->assignUUID($preOrientation);
        $preOrientation->content_links = $preOrientation->content;
    }

    /**
     * Handle the PreOrientation "created" event.
     *
     * @param  \App\Models\PreOrientation  $preOrientation
     * @return void
     */
    public function created(PreOrientation $preOrientation)
    {
        //
    }

    /**
     * Handle the PreOrientation before "updated" event.
     *
     * @param  \App\Models\PreOrientation  $preOrientation
     * @return void
     */
    public function updating(PreOrientation $preOrientation)
    {
        $preOrientation->content_links = $preOrientation->content;

        foreach ([
            PreOrientationEntity::FIELD_INTAKES => Intake::where('is_active', 1)->get('id'),
            PreOrientationEntity::FIELD_FACULTIES => Faculty::where('is_active', 1)->get('id'),
            PreOrientationEntity::FIELD_SCHOOLS => School::where('is_active', 1)->get('id'),
            PreOrientationEntity::FIELD_PROGRAMMES => Programme::where('is_active', 1)->get('id'),
            PreOrientationEntity::FIELD_MOBILITIES => Mobility::where('is_active', 1)->get('id'),
        ] as $key => $value) {
            if (
                is_null($preOrientation[$key]) ||
                ($preOrientation[$key] instanceof Collection && empty($preOrientation[$key]->get('original')))
            ) {
                $preOrientation[$key] = [];
            } else {
                $original = $preOrientation[$key]['original'];
                if (!empty($original) && is_array($original[0]) && array_key_exists('id', $original[0])) {
                    $values = collect($original)
                        ->filter(fn ($item) => in_array($item['id'], $value->pluck('id')->toArray()))
                        ->map(fn ($item) => "{$item['id']}")
                        ->values()
                        ->toArray();
                    
                    $preOrientation[$key] = $values;
                } else if (is_string($original[0])) {
                    $values = collect($original)
                        ->filter(fn ($item) => in_array($item, $value->pluck('id')->toArray()))
                        ->values()
                        ->toArray();

                    $preOrientation[$key] = $values;
                }
            }
        }
    }

    /**
     * Handle the PreOrientation "updated" event.
     *
     * @param  \App\Models\PreOrientation  $preOrientation
     * @return void
     */
    public function updated(PreOrientation $preOrientation)
    {
        //
    }

    /**
     * Handle the PreOrientation "deleted" event.
     *
     * @param  \App\Models\PreOrientation  $preOrientation
     * @return void
     */
    public function deleted(PreOrientation $preOrientation)
    {
        //
    }

    /**
     * Handle the PreOrientation "restored" event.
     *
     * @param  \App\Models\PreOrientation  $preOrientation
     * @return void
     */
    public function restored(PreOrientation $preOrientation)
    {
        //
    }

    /**
     * Handle the PreOrientation "force deleted" event.
     *
     * @param  \App\Models\PreOrientation  $preOrientation
     * @return void
     */
    public function forceDeleted(PreOrientation $preOrientation)
    {
        //
    }
}
