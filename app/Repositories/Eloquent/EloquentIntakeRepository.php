<?php declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Contracts\Entities\IntakeEntity;
use App\Repositories\Contracts\IntakeRepository;
use App\Repositories\Repository;

final class EloquentIntakeRepository extends Repository implements IntakeRepository
{
    public function getActive()
    {
        return $this->entity->where('is_active', 1)->get();
    }
    
    /**
     * {@override}
     *
     * @return string
     */
    protected function entity(): string
    {
        return IntakeEntity::class;
    }
}