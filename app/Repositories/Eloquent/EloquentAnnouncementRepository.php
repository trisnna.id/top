<?php declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use App\Contracts\Entities\AnnouncementEntity;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Repositories\Contracts\AnnouncementRepository;

final class EloquentAnnouncementRepository extends Repository implements AnnouncementRepository
{
    /**
     * Update the status of 'pre-orientation' to publish
     *
     * @param   string $uuid
     * @return  AnnouncementEntity|null
     */
    public function publish(string $uuid): ?AnnouncementEntity
    {
        $entity = $this->firstByUUID($uuid);

        if ($entity instanceof AnnouncementEntity) {
            $this->updateByUUID($entity->uuid, [
                $entity::FIELD_STATUS => $entity::STATUS_PUBLISHED
            ]);
            
            return $this->firstByUUID($uuid);
        }

        return null;
    }

    /**
     * Update the status of 'pre-orientation' to unpublish
     *
     * @param   string $uuid
     * @return  AnnouncementEntity|null
     */
    public function unpublish(string $uuid): ?AnnouncementEntity
    {
        $entity = $this->firstByUUID($uuid);

        if ($entity instanceof AnnouncementEntity) {
            $this->updateByUUID($entity->uuid, [
                $entity::FIELD_STATUS => $entity::STATUS_UNPUBLISHED
            ]);
            
            return $this->firstByUUID($uuid);
        }

        return null;
    }

    /**
     * Get announcements for carousel that match student's state
     *
     * @param  int $group
     * @return  Collection
     */
    public function getAnnouncementsForCarousel(int $group): Collection
    {
        $student = $this->authService->student();

        // get all, and group by 6 (for 6 announcements per slide)
        return $this->entity
            ->where('status', AnnouncementEntity::STATUS_PUBLISHED)
            ->where(function ($query) use ($student) {
                foreach ([
                    AnnouncementEntity::FIELD_LOCALITIES => 'locality_id',
                    AnnouncementEntity::FIELD_INTAKES => 'intake_id',
                    AnnouncementEntity::FIELD_PROGRAMMES => 'programme_id',
                    AnnouncementEntity::FIELD_CAMPUSES => 'campus_id',
                ] as $column => $field) {
                    $query->whereJsonContains($column, [sprintf('%s', $student[$field])]);
                }
            })
            ->orderBy(DB::raw(sprintf('%s is null, %s', AnnouncementEntity::FIELD_SEQUENCE, AnnouncementEntity::FIELD_SEQUENCE)))
            ->latest()
            ->get()
            ->groupBy(function ($item, $key) use ($group) {
                return floor($key / $group);
            });
    }

    /**
     * Find by slug
     * 
     * @param   string $slug
     * @return  AnnouncementEntity|null
     */
    public function findBySlug(string $slug): ?AnnouncementEntity
    {
        // return $this->entity->where('slug', $slug)->first();
        // return $this->entity->whereRaw('LOWER(REPLACE(name, " ", "-")) = ?', [$slug])->first();

        return $this->entity->whereRaw('LOWER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(name, "_", ""), "--", ""), ":", ""), "\'", ""), ".", ""), " ", "-")) = ?', [$slug])->first();
    }

    /**
     * Get all published announcements with pagination (default to 9 per page)
     * 
     * @param   int $perPage
     * @return  LengthAwarePaginator
     */
    public function getPublishedAnnouncements(int $perPage = 9): LengthAwarePaginator
    {
        $student = $this->authService->student();

        return $this->entity
            ->where(AnnouncementEntity::FIELD_STATUS, AnnouncementEntity::STATUS_PUBLISHED)
            ->where(function ($query) use ($student) {
                foreach ([
                    AnnouncementEntity::FIELD_LOCALITIES => 'locality_id',
                    AnnouncementEntity::FIELD_INTAKES => 'intake_id',
                    AnnouncementEntity::FIELD_PROGRAMMES => 'programme_id',
                    AnnouncementEntity::FIELD_CAMPUSES => 'campus_id',
                ] as $column => $field) {
                    $query->whereJsonContains($column, [sprintf('%s', $student[$field])]);
                }
            })
            ->orderBySequence()
            ->paginate($perPage);
    }

    /**
     * Bulk update the status of 'announcement' to publish
     * 
     * @param   array $id
     * @return  int
     */
    public function bulkPublish(array $id): int
    {
        return $this->entity->whereIn('id', $id)->update([
            AnnouncementEntity::FIELD_STATUS => AnnouncementEntity::STATUS_PUBLISHED
        ]);
    }

    /**
     * Bulk update the status of 'announcement' to unpublish
     * 
     * @param   array $id
     * @return  int
     */
    public function bulkUnpublish(array $id): int
    {
        return $this->entity->whereIn('id', $id)->update([
            AnnouncementEntity::FIELD_STATUS => AnnouncementEntity::STATUS_UNPUBLISHED
        ]);
    }

    /**
     * {@override}
     *
     * @return string
     */
    protected function entity(): string
    {
        return AnnouncementEntity::class;
    }
}