<?php declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Contracts\Entities\CheckpointEntity;
use App\Contracts\Entities\PreOrientationEntity;
use App\Repositories\Contracts\CheckpointRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;

final class EloquentCheckpointRepository extends Repository implements CheckpointRepository
{
    /**
     * Get all checkpoints with only select
     * field 'id', 'label', & 'value'
     *
     * @return  Collection<int, CheckpointEntity>
     */
    public function allOnlyLabelValue(): Collection
    {
        return $this->entity->select([
            'id',
            CheckpointEntity::FIELD_LABEL,
            CheckpointEntity::FIELD_VALUE
        ])->get();
    }

    /**
     * Get the milestone base on current authenticated student
     *
     * @return  Collection
     */
    public function getStudentMilestone(): Collection
    {
        $student = $this->authService->student();

        return $this->entity->select([
            'id',
            CheckpointEntity::FIELD_LABEL,
            CheckpointEntity::FIELD_VALUE,
        ])->with([
            'preOrientations' => function ($query) use ($student) {
                $query
                    ->select([
                        'id',
                        PreOrientationEntity::FIELD_CATEGORY,
                        PreOrientationEntity::FIELD_LOCALITIES,
                    ])
                    ->published()
                    ->where(function ($query) use ($student) {
                        foreach ([
                             PreOrientationEntity::FIELD_LOCALITIES => 'locality_id',
                             PreOrientationEntity::FIELD_INTAKES => 'intake_id',
                             PreOrientationEntity::FIELD_PROGRAMMES => 'programme_id',
                             PreOrientationEntity::FIELD_MOBILITIES => 'mobility_id',
                             PreOrientationEntity::FIELD_FACULTIES => 'faculty_id',
                             PreOrientationEntity::FIELD_SCHOOLS => 'school_id',
                         ] as $column => $field) {
                            if ($column !== PreOrientationEntity::FIELD_LOCALITIES) {
                                $query->whereJsonContains($column, [sprintf('%s', $student[$field])]);
                            } else {
                                $query->whereJsonContains($column, [sprintf('%s', $student[$field])]);
                            }
                        }
                    })
                    ->with('students', function ($query) use ($student) {
                        return $query->select([
                            'students_preorientations.id', 'locality_id'
                        ])->where('students_preorientations.student_id', $student->id);
                    });
            }
        ])->get();
    }

    /**
     * {@override}
     *
     * @return string
     */
    protected function entity(): string
    {
        return CheckpointEntity::class;
    }
}
