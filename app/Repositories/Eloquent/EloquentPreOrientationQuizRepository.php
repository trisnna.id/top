<?php declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Contracts\Entities\PreOrientationQuizEntity;
use App\Models\PreOrientationQuiz;
use App\Repositories\Contracts\PreOrientationQuizRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;

final class EloquentPreOrientationQuizRepository extends Repository implements PreOrientationQuizRepository
{
    /**
     * Create a new record from copying existing entity
     *
     * @param int $preOrientationId
     * @param int $preOrientationCloneId
     * @return Collection
     */
    public function clone(int $perOrientationId, int $preOrientationCloneId): Collection
    {
        $entities = Collection::make();

        $this->findByPreOrientation($perOrientationId)
            ->each(function (PreOrientationQuiz $model) use ($entities, $preOrientationCloneId) {
                $cloning = $model->replicate();
                $cloning[PreOrientationQuizEntity::FIELD_PREORIENTATION] = $preOrientationCloneId;
                $cloning->save();
                $entities->push($cloning);
            });

        return $entities;
    }

    /**
     * Delete exsiting pre-orientation quiz record by 'pre_orientation_id'
     *
     * @param   int     $perOrientationId
     * @return  bool
     */
    public function deleteByPreOrientation(int $perOrientationId): bool
    {
        return (bool) $this->entity->where(PreOrientationQuizEntity::FIELD_PREORIENTATION, $perOrientationId)->delete();
    }

    /**
     * Create few pre-orientation quiz and assign them to
     * specified pre-orientation
     *
     * @param   int     $perOrientationId
     * @param   array   $mcq
     * @return  Collection<int, PreOrientationQuiz>
     */
    public function createMany(int $perOrientationId, array $mcq): Collection
    {
        foreach ($mcq as $quiz) {
            $this->create([
                PreOrientationQuizEntity::FIELD_PREORIENTATION => $perOrientationId,
                PreOrientationQuizEntity::FIELD_QUESTION => Arr::get($quiz, 'question', ''),
                PreOrientationQuizEntity::FIELD_OPTIONS => Arr::get($quiz, 'answers', []),
                PreOrientationQuizEntity::FIELD_SETTINGS => Arr::get($quiz, 'settings', []),
            ]);
        }

        return $this->findByPreOrientation($perOrientationId);
    }

    /**
     * Get pre-orientations record by specified pre-orientation id
     *
     * @param   int     $perOrientationId
     * @return  Collection<int, PreOrientationQuiz>
     */
    public function findByPreOrientation(int $perOrientationId): Collection
    {
        return $this->entity
            ->where(PreOrientationQuizEntity::FIELD_PREORIENTATION, $perOrientationId)
            ->get();
    }

    /**
     * Update existing pre-orientation quiz or create a new one if there's not
     * stored in the database. Others existing pre-orientation quiz record that
     * match with the pre-orientation id is not in the mcq list will removed 
     *
     * @param   int     $perOrientationId
     * @param   array   $mcq
     * @return  bool
     */
    public function upsertOrDeleteByPreOrientation(int $perOrientationId, array $mcq): bool
    {
        $countUpdate = 0;

        if (empty($mcq)) {
            $this->deleteByPreOrientation($perOrientationId);
            return false;
        }

        foreach ($mcq as $quiz) {
            // Get the existing pre-orientation quiz
            $entity = $this->first((int) Arr::get($quiz, 'quiz_id', 0));
            $attributes = [
                PreOrientationQuizEntity::FIELD_QUESTION => Arr::get($quiz, 'question', ''),
                PreOrientationQuizEntity::FIELD_OPTIONS => Arr::get($quiz, 'answers', []),
                PreOrientationQuizEntity::FIELD_SETTINGS => Arr::get($quiz, 'settings', []),
            ];
            
            // If the current quiz it's found, then we need to update
            // the existing attributes base on the input values
            if ($entity instanceof PreOrientationQuizEntity) {
                if (Arr::has($quiz, 'question')) {
                    $this->update($entity->id, $attributes);
                } else {
                    // If the input quiz attributes is an empty array, meaning
                    // that quiz has been removed from the frontend side and we need
                    // to remove it from the database
                    $this->delete($entity->id);
                }
            } else {
                // If the present quiz input is not found, we'll
                // create a new record in our `pre_orientation_quizs` table
                $this->create(
                    array_merge(
                        [PreOrientationQuizEntity::FIELD_PREORIENTATION => $perOrientationId],
                        $attributes
                    )
                );
            }

            $countUpdate++;
        }

        return $countUpdate > 0;
    }

    /**
     * {@override}
     *
     * @return string
     */
    protected function entity(): string
    {
        return PreOrientationQuizEntity::class;
    }
}