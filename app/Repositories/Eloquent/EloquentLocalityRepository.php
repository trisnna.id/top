<?php declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Contracts\Entities\LocalityEntity;
use App\Repositories\Contracts\LocalityRepository;
use App\Repositories\Repository;

final class EloquentLocalityRepository extends Repository implements LocalityRepository
{
    public function getActive()
    {
        return $this->entity->where('is_active', 1)->get();
    }

    /**
     * {@override}
     *
     * @return string
     */
    protected function entity(): string
    {
        return LocalityEntity::class;
    }
}