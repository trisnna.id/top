<?php declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Contracts\Entities\CampusEntity;
use App\Repositories\Contracts\CampusRepository;
use App\Repositories\Repository;

final class EloquentCampusRepository extends Repository implements CampusRepository
{
    public function getActive()
    {
        return $this->entity->where('is_active', 1)->get();
    }

    /**
     * {@override}
     *
     * @return string
     */
    protected function entity(): string
    {
        return CampusEntity::class;
    }
}