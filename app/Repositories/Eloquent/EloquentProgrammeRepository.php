<?php declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Contracts\Entities\ProgrammeEntity;
use App\Repositories\Contracts\ProgrammeRepository;
use App\Repositories\Repository;

final class EloquentProgrammeRepository extends Repository implements ProgrammeRepository
{
    public function getActive()
    {
        return $this->entity->where('is_active', 1)->get();
    }
    
    /**
     * {@override}
     *
     * @return string
     */
    protected function entity(): string
    {
        return ProgrammeEntity::class;
    }
}