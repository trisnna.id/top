<?php

declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Contracts\Entities\StudentPreOrientationEntity;
use App\Repositories\Contracts\StudentPreOrientationRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

final class EloquentStudentPreOrientationRepository extends Repository implements StudentPreOrientationRepository
{
    /**
     * Create a new student preorientation record with status completed
     *
     * @param   int     $preOrientationId
     * @param   array   $quizAnswers
     * @return  Model<StudentPreOrientationEntity>
     */
    public function createCompletedQuiz(int $preOrientationId, array $quizAnswers): Model
    {
        return $this->create([
            StudentPreOrientationEntity::FIELD_COMPLETED => StudentPreOrientationEntity::STATUS_COMPLETED,
            StudentPreOrientationEntity::FIELD_PREORIENTATION => $preOrientationId,
            StudentPreOrientationEntity::FIELD_QUIZ => $quizAnswers,
        ]);
    }

    /**
     * Get the student preorientation record by student & pre orientation id
     *
     * @param   int $preOrientationId
     * @return  StudentPreOrientationEntity|null
     */
    public function findByPreOrientation(int $preOrientationId): ?StudentPreOrientationEntity
    {
        $student = $this->authService->student();

        return $this->entity->select([
            'id',
            StudentPreOrientationEntity::FIELD_PREORIENTATION,
            StudentPreOrientationEntity::FIELD_STUDENT,
            StudentPreOrientationEntity::FIELD_COMPLETED,
            StudentPreOrientationEntity::FIELD_QUIZ,
            StudentPreOrientationEntity::FIELD_SCORE,
        ])->where([
            [StudentPreOrientationEntity::FIELD_PREORIENTATION, '=', $preOrientationId],
            [StudentPreOrientationEntity::FIELD_STUDENT, '=', $student->id]
        ])->first();
    }

    /**
     * Update the student pre-orientation status as completed
     *
     * @param   int     $preOrientationId
     * @return  bool
     */
    public function updateAsComplete(int $preOrientationId): bool
    {
        $student = $this->authService->student();

        $entity = $this->entity->where([
            [StudentPreOrientationEntity::FIELD_PREORIENTATION, '=', $preOrientationId],
            [StudentPreOrientationEntity::FIELD_STUDENT, '=', $student->id]
        ])->first();

        if (!($entity instanceof StudentPreOrientationEntity)) {
            $this->create([
                StudentPreOrientationEntity::FIELD_COMPLETED => StudentPreOrientationEntity::STATUS_COMPLETED,
                StudentPreOrientationEntity::FIELD_PREORIENTATION => $preOrientationId,
            ]);

            return true;
        }

        return $this->update($entity->id, [
            StudentPreOrientationEntity::FIELD_COMPLETED => StudentPreOrientationEntity::STATUS_COMPLETED
        ]);
    }

    /**
     * Get completed student pre-orientation records
     * 
     * @return Collection<StudentPreOrientationEntity>
     */
    public function getCompleted(): Collection
    {
        $student = $this->authService->student();
        return $this->entity->completed($student)->get();
    }

    /**
     * Count score of student pre-orientation quiz, return [score, total]
     * 
     * @param   int     $studentId|null
     * @return  array
     */
    public function countScore(?int $studentId = null): array
    {
        $student = $studentId ?? $this->authService->student();

        $entity = $this->entity->select([
            'students_preorientations.id',
            StudentPreOrientationEntity::FIELD_PREORIENTATION,
            StudentPreOrientationEntity::FIELD_STUDENT,
            StudentPreOrientationEntity::FIELD_COMPLETED,
            StudentPreOrientationEntity::FIELD_SCORE,
        ])->where([
            [StudentPreOrientationEntity::FIELD_STUDENT, '=', $student->id],
            [StudentPreOrientationEntity::FIELD_COMPLETED, '=', StudentPreOrientationEntity::STATUS_COMPLETED],
            [StudentPreOrientationEntity::FIELD_QUIZ, '!=', null],
        ]);

        $studentScore = $entity->sum(StudentPreOrientationEntity::FIELD_SCORE);
        $totalScore = $entity->count() * 100;

        return [
            'score' => $studentScore,
            'total' => $totalScore,
        ];
    }

    /**
     * {@override}
     *
     * @return string
     */
    protected function entity(): string
    {
        return StudentPreOrientationEntity::class;
    }
}
