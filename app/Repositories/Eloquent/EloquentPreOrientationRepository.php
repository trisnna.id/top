<?php

declare(strict_types=1);

namespace App\Repositories\Eloquent;

use Illuminate\Support\Arr;
use App\Models\PreOrientation;
use App\Repositories\Repository;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Repositories\Contracts\PreOrientationRepository;

final class EloquentPreOrientationRepository extends Repository implements PreOrientationRepository
{
    /**
     * Create a new record from copying existing entity
     *
     * @param PreOrientationEntity $entity
     * @return PreOrientationEntity|null
     */
    public function clone(PreOrientationEntity $entity): ?PreOrientationEntity
    {
        $cloning = $entity->replicate([
            PreOrientationEntity::FIELD_UUID,
        ]);

        $cloning->status = PreOrientationEntity::STATUS_DRAFT;

        if ($entity->thumbnail && str_contains($entity->thumbnail, 'thumbnail')) {
            $path = explode('/', $entity->thumbnail);
            $destination = "thumbnail/preorientation/clone-{$path[2]}";
            if (Storage::copy($entity->thumbnail, $destination)) {
                $cloning->thumbnail = $destination;
            }
        }

        foreach ([
            PreOrientationEntity::FIELD_INTAKES,
            PreOrientationEntity::FIELD_FACULTIES,
            PreOrientationEntity::FIELD_SCHOOLS,
            PreOrientationEntity::FIELD_PROGRAMMES,
            PreOrientationEntity::FIELD_MOBILITIES,
        ] as $column) {
            if (is_null($entity[$column]->get('original'))) {
                $cloning[$column] = [];
            }
        }

        $cloning->save();

        return $cloning;
    }

    /**
     * Get pre-orientation query base on user role & permissions
     *
     * @param   PreOrientationEntity    $entity
     * @param   array                   $requestData
     * @return  Builder
     */
    public function dataTable(PreOrientationEntity $entity, array $requestData = []): Builder
    {
        $student = $this->authService->student();
        $query = $this->entity->newQuery();

        if (!is_null($student)) {
            return $query->select([
                'id',
                PreOrientationEntity::FIELD_UUID,
                PreOrientationEntity::FIELD_ACTIVITY_NAME,
                PreOrientationEntity::FIELD_CATEGORY,
                PreOrientationEntity::FIELD_CONTENT,
                PreOrientationEntity::FIELD_CONTENT_LINKS,
                PreOrientationEntity::FIELD_THUMBNAIL,
                'created_at',
            ])
                ->published()
                ->when(Arr::has($requestData, 'milestone'), function (Builder $query) use ($student, $requestData) {
                    $milestone = Arr::get($requestData, 'milestone', '*');
                    if (is_numeric($milestone)) {
                        $query->where(PreOrientationEntity::FIELD_CATEGORY, $milestone);
                    } else if ($milestone === '*') {
                        $query->whereHas('students', function ($query) {
                            return $query->where('students_preorientations.completed', 1);
                        });
                    }
                })
                ->where(function ($query) use ($student) {
                    foreach ([
                        PreOrientationEntity::FIELD_LOCALITIES => 'locality_id',
                        PreOrientationEntity::FIELD_INTAKES => 'intake_id',
                        PreOrientationEntity::FIELD_PROGRAMMES => 'programme_id',
                        PreOrientationEntity::FIELD_MOBILITIES => 'mobility_id',
                        PreOrientationEntity::FIELD_FACULTIES => 'faculty_id',
                        PreOrientationEntity::FIELD_SCHOOLS => 'school_id',
                    ] as $column => $field) {
                        if ($column !== PreOrientationEntity::FIELD_LOCALITIES) {
                            $query->whereJsonContains($column, [sprintf('%s', $student[$field])]);
                        } else {
                            $query->whereJsonContains($column, [sprintf('%s', $student[$field])]);
                        }
                    }
                })
                ->with([
                    'students' => function ($query) use ($student) {
                        $query->select('students_preorientations.id')->where('students_preorientations.student_id', $student->id);
                    }
                ]);
        }
        return $query;
    }

    /**
     * Get pre-orientation record by `checkpoint_id`
     *
     * @param   int     $checkpointId
     * @return Collection
     */
    public function findByCheckpoint(int $checkpointId): Collection
    {
        return $this->entity->select([
            'id',
            PreOrientationEntity::FIELD_UUID,
            PreOrientationEntity::FIELD_ACTIVITY_NAME,
            PreOrientationEntity::FIELD_CATEGORY,
            PreOrientationEntity::FIELD_LOCALITIES,
            PreOrientationEntity::FIELD_STATUS,
            PreOrientationEntity::FIELD_CONTENT,
            PreOrientationEntity::FIELD_CONTENT_LINKS,
        ])->with([
            'checkpoint' => function (BelongsTo $query) use ($checkpointId) {
                $query->where('id', $checkpointId);
            }
        ])->get();
    }

    /**
     * Get pre-orientation record where the status is 'draft'
     *
     * @return Collection<int, PreOrientation>
     */
    public function findWhereDraft(): Collection
    {
        return $this->entity->draft()->get();
    }

    /**
     * Get pre-orientation record where the status is 'published'
     *
     * @return Collection<int, PreOrientation>
     */
    public function findWherePublished(): Collection
    {
        return $this->entity->published()->get();
    }

    /**
     * Get pre-orientation record where the status is 'published'
     * and match with specified `checkpoint_id`
     *
     * @param   int     $checkpointId
     * @return  Collection
     */
    public function findWherePublishedByCheckpoint(int $checkpointId): Collection
    {
        return $this->entity->published();
    }

    /**
     * Get pre-orientation record where the status is 'unpublished'
     *
     * @return Collection<int, PreOrientation>
     */
    public function findWhereUnpublished(): Collection
    {
        return $this->entity->unpublished()->get();
    }

    /**
     * Update the status of 'pre-orientation' to publish
     *
     * @param   string $uuid
     * @return  PreOrientationEntity|null
     */
    public function publish(string $uuid): ?PreOrientationEntity
    {
        $entity = $this->firstByUUID($uuid);

        if ($entity instanceof PreOrientationEntity) {
            $this->updateByUUID($entity->uuid, [
                $entity::FIELD_STATUS => $entity::STATUS_PUBLISHED
            ]);

            return $this->firstByUUID($uuid);
        }

        return null;
    }

    /**
     * Update the status of 'pre-orientation' to unpublish
     *
     * @param   string $uuid
     * @return  PreOrientationEntity|null
     */
    public function unpublish(string $uuid): ?PreOrientationEntity
    {
        $entity = $this->firstByUUID($uuid);

        if ($entity instanceof PreOrientationEntity) {
            $this->updateByUUID($entity->uuid, [
                $entity::FIELD_STATUS => $entity::STATUS_UNPUBLISHED
            ]);

            return $this->firstByUUID($uuid);
        }

        return null;
    }

    /**
     * Bulk update the status of 'pre-orientation' to publish
     *
     * @param   array $id
     * @return  int
     */
    public function bulkPublish(array $id): int
    {
        $totalUpdated = 0;

        foreach ($id as $primaryKey) {
            $entity = $this->first((int) $primaryKey);
            $totalUpdated = $this->update($entity->id, [
                PreOrientationEntity::FIELD_STATUS => PreOrientationEntity::STATUS_PUBLISHED
            ]) ? 1 : 0;
        }

        return intval($totalUpdated === count($id));
    }

    /**
     * Bulk update the status of 'pre-orientation' to unpublish
     *
     * @param   array $id
     * @return  int
     */
    public function bulkUnpublish(array $id): int
    {
        $totalUpdated = 0;

        foreach ($id as $primaryKey) {
            $entity = $this->first((int) $primaryKey);
            $totalUpdated = $this->update($entity->id, [
                PreOrientationEntity::FIELD_STATUS => PreOrientationEntity::STATUS_UNPUBLISHED
            ]) ? 1 : 0;
        }

        return intval($totalUpdated === count($id));
    }

    /**
     * Count the number of pre-orientation record where the status is 'published' and localities is match with student locality
     *
     * @return int
     */
    public function countPublished(): int
    {
        $student = $this->authService->student();

        if (!is_null($student)) {
            return $this->entity->published()
                ->whereLikeStudentLocality($student)
                ->count();
        }

        return 0;
    }

    /**
     * Get all pre-orientation record where the status is 'published' and localities is match with student locality, and sort by 'checkpoint_id', then add field 'completed' to each record if the student has completed the pre-orientation
     *
     * @return Collection
     */
    public function allPublished(): Collection
    {
        $student = $this->authService->student();

        if (!is_null($student)) {
            return $this->entity->select([
                'id',
                PreOrientationEntity::FIELD_UUID,
                PreOrientationEntity::FIELD_ACTIVITY_NAME,
                PreOrientationEntity::FIELD_CATEGORY,
                PreOrientationEntity::FIELD_LOCALITIES,
                PreOrientationEntity::FIELD_STATUS,
                PreOrientationEntity::FIELD_CONTENT,
                PreOrientationEntity::FIELD_CONTENT_LINKS,
            ])
                ->published()
                ->where(PreOrientationEntity::FIELD_LOCALITIES, 'like', "%\"{$student->locality_id}\"%")
                ->with([
                    'students' => function ($query) use ($student) {
                        $query->select('students_preorientations.id', 'students_preorientations.completed')->where('students_preorientations.student_id', $student->id);
                    }
                ])
                ->orderBy(PreOrientationEntity::FIELD_CATEGORY, 'asc')
                ->get();
        }

        return collect();
    }

    /**
     * {@override}
     *
     * @return string
     */
    protected function entity(): string
    {
        return PreOrientationEntity::class;
    }
}
