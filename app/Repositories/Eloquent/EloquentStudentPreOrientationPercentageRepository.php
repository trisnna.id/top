<?php declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Repositories\Repository;
use App\Contracts\Entities\StudentPreOrientationPercentageEntity;
use App\Repositories\Contracts\StudentPreOrientationPercentageRepository;

final class EloquentStudentPreOrientationPercentageRepository extends Repository implements StudentPreOrientationPercentageRepository
{
    /**
     * {@override}
     *
     * @return string
     */
    protected function entity(): string
    {
        return StudentPreOrientationPercentageEntity::class;
    }
}