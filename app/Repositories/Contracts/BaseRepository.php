<?php declare(strict_types=1);

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseRepository
{
    /**
     * Get all existing entity record
     *
     * @param   array   $columns
     * @return  Collection
     */
    public function all(array $columns = ['*']): Collection;

    /**
     * Create a new record into a database table
     *
     * @param   array   $attributes
     * @return  Model
     */
    public function create(array $attributes): Model;

    /**
     * Delete a single record by specified ID
     *
     * @param   int     $id
     * @return  boolean
     */
    public function delete(int $id): bool;
    
    /**
     * Delete a single record by specified UUID
     *
     * @param   int     $id
     * @return  boolean
     */
    public function deleteByUUID(string $uuid): bool;

    /**
     * Find a single record by specified ID
     *
     * @param   int     $id
     * @return  Model|null
     */
    public function first(int $id): ?Model;

    /**
     * Find a single record by specified UUID
     *
     * @param   string  $uuid
     * @return  Model|null
     */
    public function firstByUUID(string $uuid): ?Model;

    /**
     * Update a single record by specified ID
     *
     * @param   int     $id
     * @param   array   $attributes
     * @return  bool
     */
    public function update(int $id, array $attributes): bool;

    /**
     * Update a single record by specified UUID
     *
     * @param   string  $uuid
     * @param   array   $attributes
     * @return  bool
     */
    public function updateByUUID(string $uuid, array $attributes): bool;

    /**
     * Update a single record by specified UUID and return the
     * updated entity
     *
     * @param   string  $uuid
     * @param   array   $attributes
     * @return  Model
     */
    public function updateByUUIDWithresult(string $uuid, array $attributes): Model;

    /**
     * Update a single record by specified ID and return the
     * updated entity
     *
     * @param   int     $id
     * @param   array   $attributes
     * @return  Model
     */
    public function updateWithresult(int $id, array $attributes): Model;

    /**
     * Update record if they're matched with the given `matches` attributes
     * or create new if there're no record found 
     *
     * @param   array   $matches
     * @param   array   $attributes
     * @return  Model
     */
    public function upsert(array $matches, array $attributes): Model;
}
