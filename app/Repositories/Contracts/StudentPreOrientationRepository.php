<?php declare(strict_types=1);

namespace App\Repositories\Contracts;

use App\Contracts\Entities\StudentPreOrientationEntity;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface StudentPreOrientationRepository extends BaseRepository
{
    /**
     * Create a new student preorientation record with status completed
     *
     * @param   int     $preOrientationId
     * @param   array   $quizAnswers
     * @return  Model<StudentPreOrientationEntity>
     */
    public function createCompletedQuiz(int $preOrientationId, array $quizAnswers): Model;

    /**
     * Get the student preorientation record by student & pre orientation id
     *
     * @param   int $preOrientationId
     * @return  StudentPreOrientationEntity|null
     */
    public function findByPreOrientation(int $preOrientationId): ?StudentPreOrientationEntity;

    /**
     * Update the student pre-orientation status as completed
     *
     * @param   int     $preOrientationId
     * @return  bool
     */
    public function updateAsComplete(int $preOrientationId): bool;

    /**
     * Count score of student pre-orientation quiz, return [score, total]
     * 
     * @param   int     $studentId
     * @return  array
     */
    public function countScore(?int $studentId): array;

    /**
     * Get completed student pre-orientation records
     * 
     * @return Collection<StudentPreOrientationEntity>
     */
    public function getCompleted(): Collection;
}
