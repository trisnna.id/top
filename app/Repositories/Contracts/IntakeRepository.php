<?php declare(strict_types=1);

namespace App\Repositories\Contracts;

interface IntakeRepository extends BaseRepository
{
    public function getActive();
}