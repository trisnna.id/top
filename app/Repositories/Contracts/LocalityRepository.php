<?php declare(strict_types=1);

namespace App\Repositories\Contracts;

interface LocalityRepository extends BaseRepository
{
    public function getActive();
}