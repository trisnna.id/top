<?php declare(strict_types=1);

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use App\Contracts\Entities\PreOrientationEntity;

interface PreOrientationRepository extends BaseRepository
{   
    /**
     * Create a new record from copying existing entity
     *
     * @param PreOrientationEntity $entity
     * @return PreOrientationEntity|null
     */
    public function clone(PreOrientationEntity $entity): ?PreOrientationEntity;

    /**
     * Get pre-orientation query base on user role & permissions
     *
     * @param   PreOrientationEntity    $entity
     * @param   array                   $requestData
     * @return  Builder
     */
    public function dataTable(PreOrientationEntity $entity, array $requestData = []): Builder;
    
    /**
     * Get pre-orientation record by `checkpoint_id`
     *
     * @param   int     $checkpointId
     * @return Collection
     */
    public function findByCheckpoint(int $checkpointId): Collection;

    /**
     * Get pre-orientation record where the status is 'draft'
     *
     * @return Collection<int, PreOrientation>
     */
    public function findWhereDraft(): Collection;

    /**
     * Get pre-orientation record where the status is 'published'
     *
     * @return Collection<int, PreOrientation>
     */
    public function findWherePublished(): Collection;

    /**
     * Get pre-orientation record where the status is 'published'
     * and match with specified `checkpoint_id`
     *
     * @param   int     $checkpointId
     * @return  Collection
     */
    public function findWherePublishedByCheckpoint(int $checkpointId): Collection;

    /**
     * Get pre-orientation record where the status is 'unpublished'
     *
     * @return Collection<int, PreOrientation>
     */
    public function findWhereUnpublished(): Collection;

    /**
     * Update the status of 'pre-orientation' to publish
     *
     * @param   string $uuid
     * @return  PreOrientationEntity|null
     */
    public function publish(string $uuid): ?PreOrientationEntity;

    /**
     * Update the status of 'pre-orientation' to unpublish
     *
     * @param   string $uuid
     * @return  PreOrientationEntity|null
     */
    public function unpublish(string $uuid): ?PreOrientationEntity;

    /**
     * Bulk update the status of 'pre-orientation' to publish
     * 
     * @param   array $uuids
     * @return  int
     */
    public function bulkPublish(array $uuids): int;

    /**
     * Bulk update the status of 'pre-orientation' to unpublish
     * 
     * @param   array $uuids
     * @return  int
     */
    public function bulkUnpublish(array $uuids): int;

    /**
     * Count the number of pre-orientation record where the status is 'published' and localities is match with student locality
     * 
     * @return int
     */
    public function countPublished(): int;

    /**
     * Get all pre-orientation record where the status is 'published' and localities is match with student locality
     * 
     * @return Collection
     */
    public function allPublished(): Collection;
}
