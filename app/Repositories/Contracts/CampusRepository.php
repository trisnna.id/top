<?php declare(strict_types=1);

namespace App\Repositories\Contracts;

interface CampusRepository extends BaseRepository
{
    public function getActive();
}