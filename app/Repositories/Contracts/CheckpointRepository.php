<?php declare(strict_types=1);

namespace App\Repositories\Contracts;

use App\Contracts\Entities\CheckpointEntity;
use Illuminate\Database\Eloquent\Collection;

interface CheckpointRepository extends BaseRepository
{
    /**
     * Get all checkpoints with only select
     * field 'id', 'label', & 'value'
     *
     * @return  Collection<int, CheckpointEntity>
     */
    public function allOnlyLabelValue(): Collection;

    /**
     * Get the milestone base on current authenticated student
     *
     * @return  Collection
     */
    public function getStudentMilestone(): Collection;
}
