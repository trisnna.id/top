<?php declare(strict_types=1);

namespace App\Repositories\Contracts;

interface ProgrammeRepository extends BaseRepository
{
    public function getActive();
}