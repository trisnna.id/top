<?php declare(strict_types=1);

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface PreOrientationQuizRepository extends BaseRepository
{
    /**
     * Create a new record from copying existing entity
     *
     * @param int $preOrientationId
     * @param int $preOrientationCloneId
     * @return Collection
     */
    public function clone(int $perOrientationId, int $preOrientationCloneId): Collection;

    /**
     * Delete exsiting pre-orientation quiz record by 'pre_orientation_id'
     *
     * @param   int     $perOrientationId
     * @return  bool
     */
    public function deleteByPreOrientation(int $perOrientationId): bool;

    /**
     * Create few pre-orientation quiz and assign them to
     * specified pre-orientation
     *
     * @param   int     $perOrientationId
     * @param   array   $mcq
     * @return  Collection<int, PreOrientationQuiz>
     */
    public function createMany(int $perOrientationId, array $mcq): Collection;

    /**
     * Get pre-orientations record by specified pre-orientation id
     *
     * @param   int     $perOrientationId
     * @return  Collection<int, PreOrientationQuiz>
     */
    public function findByPreOrientation(int $perOrientationId): Collection;

    /**
     * Update existing pre-orientation quiz or create a new one if there's not
     * stored in the database. Others existing pre-orientation quiz record that
     * match with the pre-orientation id is not in the mcq list will removed 
     *
     * @param   int     $perOrientationId
     * @param   array   $mcq
     * @return  bool
     */
    public function upsertOrDeleteByPreOrientation(int $perOrientationId, array $mcq): bool;
}
