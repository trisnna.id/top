<?php declare(strict_types=1);

namespace App\Repositories\Contracts;

use Illuminate\Support\Collection;
use App\Contracts\Entities\AnnouncementEntity;
use Illuminate\Pagination\LengthAwarePaginator;

interface AnnouncementRepository extends BaseRepository
{
    /**
     * Update the status of 'pre-orientation' to publish
     *
     * @param   string $uuid
     * @return  AnnouncementEntity|null
     */
    public function publish(string $uuid): ?AnnouncementEntity;

    /**
     * Update the status of 'pre-orientation' to unpublish
     *
     * @param   string $uuid
     * @return  AnnouncementEntity|null
     */
    public function unpublish(string $uuid): ?AnnouncementEntity;

    /**
     * Get announcements for carousel that match student's state
     *
     * @param  int $group
     * @return  Collection
     */
    public function getAnnouncementsForCarousel(int $group): Collection;

    /**
     * Find by slug
     * 
     * @param   string $slug
     * @return  AnnouncementEntity|null
     */
    public function findBySlug(string $slug): ?AnnouncementEntity;

    /**
     * Get all published announcements with pagination (default to 9 per page)
     * 
     * @param   int $perPage
     * @return  LengthAwarePaginator
     */
    public function getPublishedAnnouncements(int $perPage = 9): LengthAwarePaginator;

    /**
     * Bulk update the status of 'pre-orientation' to publish
     * 
     * @param   array $uuids
     * @return  int
     */
    public function bulkPublish(array $uuids): int;

    /**
     * Bulk update the status of 'pre-orientation' to unpublish
     * 
     * @param   array $uuids
     * @return  int
     */
    public function bulkUnpublish(array $uuids): int;
}