<?php declare(strict_types=1);

namespace App\Repositories;

use App\Repositories\Contracts\BaseRepository;
use App\Services\AuthService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class Repository implements BaseRepository
{
    /**
     * The eloquent model instance
     *
     * @var Model
     */
    protected Model $entity;

    /**
     * Create a new auth service instance
     *
     * @var AuthService
     */
    protected AuthService $authService;

    /**
     * Create a new repository instance
     * 
     * @param   AuthService $authService
     * @return  void
     */
    public function __construct(AuthService $authService)
    {
        $this->entity = app()->make($this->entity());
        $this->authService = $authService;
    }

    /**
     * Get all existing entity record
     *
     * @param   array   $columns
     * @return  Collection
     */
    public function all(array $columns = ['*']): Collection
    {
        return $this->entity->get($columns);
    }

    /**
     * Create a new record into a database table
     *
     * @param   array   $attributes
     * @return  Model
     */
    public function create(array $attributes): Model
    {
        return $this->entity->create($attributes);
    }

    /**
     * Delete a single record by specified ID
     *
     * @param   int     $id
     * @return  boolean
     */
    public function delete(int $id): bool
    {
        return (bool) $this->first($id)->delete($id);
    }
    
    /**
     * Delete a single record by specified UUID
     *
     * @param   int     $id
     * @return  boolean
     */
    public function deleteByUUID(string $uuid): bool
    {
        return (bool) $this->firstByUUID($uuid)->delete();
    }

    /**
     * Find a single record by specified ID
     *
     * @param   int     $id
     * @return  Model|null
     */
    public function first(int $id): ?Model
    {
        return $this->entity->find($id);
    }

    /**
     * Find a single record by specified UUID
     *
     * @param   string  $uuid
     * @return  Model|null
     */
    public function firstByUUID(string $uuid): ?Model
    {
        return $this->entity->where('uuid', $uuid)->first();
    }

    /**
     * Update a single record by specified ID
     *
     * @param   int     $id
     * @param   array   $attributes
     * @return  bool
     */
    public function update(int $id, array $attributes): bool
    {
        return (bool) $this->first($id)->update($attributes);
    }

    /**
     * Update a single record by specified UUID
     *
     * @param   int     $id
     * @param   array   $attributes
     * @return  bool
     */
    public function updateByUUID(string $uuid, array $attributes): bool
    {
        return (bool) $this->firstByUUID($uuid)->update($attributes);
    }

    /**
     * Update a single record by specified UUID and return the
     * updated entity
     *
     * @param   string  $uuid
     * @param   array   $attributes
     * @return  Model
     */
    public function updateByUUIDWithresult(string $uuid, array $attributes): Model
    {
        $this->updateByUUID($uuid, $attributes);

        return $this->firstByUUID($uuid);
    }

    /**
     * Update a single record by specified ID and return the
     * updated entity
     *
     * @param   int     $id
     * @param   array   $attributes
     * @return  Model
     */
    public function updateWithresult(int $id, array $attributes): Model
    {
        $this->update($id, $attributes);

        return $this->first($id);
    }

    /**
     * Update record if they're matched with the given `matches` attributes
     * or create new if there're no record found 
     *
     * @param   array   $matches
     * @param   array   $attributes
     * @return  Model
     */
    public function upsert(array $matches, array $attributes): Model
    {
        $entity = $this->entity->updateOrCreate($matches, $attributes);

        if ($entity instanceof Model) {
            return $entity;
        }

        return $this->first($entity->id);
    }

    /**
     * Get the entity class name
     *
     * @return string
     */
    abstract protected function entity(): string;
}
