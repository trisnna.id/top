<?php declare(strict_types=1);

namespace App\Contracts\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

interface PreOrientationEntity extends HasUuid
{
    /**
     * Desribe column named 'activity_name' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_ACTIVITY_NAME = 'activity_name';

    /**
     * Desribe column named 'thumbnail' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_THUMBNAIL = 'thumbnail';
    
    /**
     * Desribe column named 'checkpoint_id' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_CATEGORY = 'checkpoint_id';

    /**
     * Desribe column named 'localities' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_LOCALITIES = 'localities';

    /**
     * Describe column named 'intakes' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_INTAKES = 'intakes';

    /**
     * Describe column named 'faculties' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_FACULTIES = 'faculties';

    /**
     * Describe column named 'schools' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_SCHOOLS = 'schools';

    /**
     * Describe column named 'programmes' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_PROGRAMMES = 'programmes';

    /**
     * Describe column named 'mobilities' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_MOBILITIES = 'mobilities';

    /**
     * Desribe column named 'effective_date' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_EFFECTIVE_DATE = 'effective_date';

    /**
     * Desribe column named 'status' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_STATUS = 'status';

    /**
     * Desribe column named 'content' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_CONTENT = 'content';

    /**
     * Desribe column named 'content_links' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_CONTENT_LINKS = 'content_links';

    /**
     * The status `draft' means that the `pre-orientation` has been saved and can be accessed
     * anytime for further editing.
     * 
     * @var string
     */
    public CONST STATUS_DRAFT = 'draft';

    /**
     * The status `published' means that the `pre-orientation` is in its published state and
     * can be viewed by relevant students
     * 
     * @var string
     */
    public CONST STATUS_PUBLISHED = 'published';

    /**
     * The status `unpublished' means that the `pre-orientation`
     * This status is only possible to reach after an item is published.
     * In this status, the activity becomes unpublished and editable again
     * 
     * @var string
     */
    public CONST STATUS_UNPUBLISHED = 'unpublished';

    /**
     * Get all the pre-orientation statuses
     * 
     * @var array 
     */
    public const STATUSES = [
        self::STATUS_DRAFT, self::STATUS_PUBLISHED, self::STATUS_UNPUBLISHED,
    ];

    /**
     * Get the query that only scoped for specific `activity_name`
     *
     * @param   Builder  $query
     * @param   string   $value
     * @return  Builder
     */
    public function scopeName(Builder $query, string $value): Builder;

    /**
     * Get the query that only scoped for specific `status`
     *
     * @param   Builder  $query
     * @param   string   $status
     * @return  Builder
     */
    public function scopeStatus(Builder $query, string $status): Builder;

    /**
     * Get a specific `pre-orientation` record where the status is 'draft'
     *
     * @param   Builder  $query
     * @return  Builder
     */
    public function scopeDraft(Builder $query): Builder;

    /**
     * Get a specific `pre-orientation` record where the status is 'published'
     *
     * @param   Builder     $query
     * @return  Builder
     */
    public function scopePublished(Builder $query): Builder;

    /**
     * Get a specific `pre-orientation` record where the status is 'unpublished'
     *
     * @param   Builder     $query
     * @return  Builder
     */
    public function scopeUnpublished(Builder $query): Builder;

    /**
     * Get the current `checkpoint` for the `pre-orientation`
     *
     * @return BelongsTo
     */
    public function checkpoint(): BelongsTo;

    /**
     * Get the `quiz` that are related with this `pre-orientation`
     *
     * @return HasMany
     */
    public function quizs(): HasMany;

    /**
     * The students that belongs to pre-orientations
     *
     * @return BelongsToMany
     */
    public function students(): BelongsToMany;
}
