<?php declare(strict_types=1);

namespace App\Contracts\Entities;

use Illuminate\Database\Eloquent\Builder;

interface ResourceEntity extends HasUuid
{
    /**
     * Desribe column named 'title' for table 'resources'
     * 
     * @var string
     */
    public CONST FIELD_TITLE = 'title';

    /**
     * Desribe column named 'description' for table 'resources'
     * 
     * @var string
     */
    public CONST FIELD_DESCRIPTION = 'description';

    /**
     * Desribe column named 'visible' for table 'resources'.
     * Indicates that the resources should be displayed or not.
     * 
     * @var string
     */
    public CONST FIELD_VISIBLE = 'visible';

    /**
     * Desribe column named 'category' for table 'resources'
     * 
     * @var string
     */
    public CONST FIELD_CATEGORY = 'category';

    /**
     * Desribe column named 'values' for table 'resources'
     * 
     * @var string
     */
    public CONST FIELD_VALUES = 'values';

    /**
     * Indicates that the resources are 'faq'
     * 
     * @var string
     */
    public CONST CATEGORY_FAQ = 'faq';

    /**
     * Indicates that the resources are 'file'
     * 
     * @var string
     */
    public CONST CATEGORY_FILE = 'file';

    /**
     * Indicates that the resources are 'link'
     * 
     * @var string
     */
    public CONST CATEGORY_LINK = 'link';

    /**
     * Indicates that the resources are 'video'
     * 
     * @var string
     */
    public CONST CATEGORY_VIDEO = 'video';

    /**
     * Determine if the current resource is `visible`
     * 
     * @var bool
     */
    public CONST IS_VISIBLE = true;

    /**
     * Define all resource categories
     * 
     * @var array
     */
    public CONST CATEGORIES = [
        self::CATEGORY_FAQ,
        self::CATEGORY_FILE,
        self::CATEGORY_LINK,
        self::CATEGORY_VIDEO,
    ];

    /**
     * Get the resources only by speficied category's 
     *
     * @param   Builder     $query
     * @param   string      $query
     * @return  Builder
     */
    public function scopeCategory(Builder $query, string $category): Builder;
    
    /**
     * Get the resources only if they visible or not
     *
     * @param   Builder     $query
     * @param   boolean     $visible
     * @return  Builder
     */
    public function scopeVisible(Builder $query, bool $visible = true): Builder;

    /**
     * Get the resources where they're not visible
     *
     * @param   Builder     $query
     * @return  Builder
     */
    public function scopeInvisible(Builder $query): Builder;
}
