<?php declare(strict_types=1);

namespace App\Contracts\Entities\Relations;

use Illuminate\Database\Eloquent\Builder;

interface HasPreOrientation
{
    /**
     * Desribe column named 'pre_orientation_id'
     * 
     * @var string
     */
    public CONST FIELD_PREORIENTATION = 'pre_orientation_id';

    /**
     * Get the entity base on specified pre-orientation id
     *
     * @param   Builder     $query
     * @param   int         $preOrientationId
     * @return  Builder
     */
    public function scopePreOrientation(Builder $query, int $preOrientationId): Builder;
}