<?php declare(strict_types=1);

namespace App\Contracts\Entities;

use App\Contracts\Entities\Relations\HasPreOrientation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface StudentPreOrientationEntity extends HasPreOrientation
{
    /**
     * Describe student pre-orientation quiz table name
     * 
     * @var string
     */
    public CONST TABLE = 'students_preorientations';

    /**
     * Desribe column named 'student_id'
     * 
     * @var string
     */
    public CONST FIELD_STUDENT = 'student_id';

    /**
     * Describe column named 'completed' to determine if the quiz was complete or not
     * 
     * @var string
     */
    public CONST FIELD_COMPLETED = 'completed';

    /**
     * Describe column named 'score'
     * 
     * @var string
     */
    public CONST FIELD_SCORE = 'score';

    /**
     * Describe column named 'quizzes'. The value of this column will be an
     * array of `pre_orientation_quiz` id and it answers
     * 
     * @var string
     */
    public CONST FIELD_QUIZ = 'quizzes';

    /**
     * Define the completed status
     * 
     * @var bool
     */
    public CONST STATUS_COMPLETED = true;

    /**
     * Define the incompleted status
     * 
     * @var bool 
     */
    public CONST STATUS_INCOMPLETED = false;

    /**
     * Get the pre-orientation that belongs to student pre-orientation's
     *
     * @param   Builder     $query
     * @param   int         $preOrientationId
     * @return  Builder
     */
    public function scopePreorientation(Builder $query, int $preOrientationId = 0): Builder;

    /**
     * Get the student that belongs to student pre-orientation's
     *
     * @param   Builder     $query
     * @param   int         $preOrientationId
     * @return  Builder
     */
    public function scopeStudent(Builder $query, int $studentId = 0): Builder;

    /**
     * Get pre-orientation quizzes questions that belongs to this entity  
     *
     * @param   Builder     $query
     * @param   int         $quizId 
     * @return  Builder
     */
    public function scopeWithQuestions(Builder $query, int $quizId = 0): Builder;
}
