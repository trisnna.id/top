<?php declare(strict_types=1);

namespace App\Contracts\Entities;

interface StudentPreOrientationPercentageEntity extends HasUuid
{
    public const FIELD_PRE_ORIENTATION_ID = 'pre_orientation_id';
    public const FIELD_STUDENT_ID = 'student_id';
    public const FIELD_SCORE = 'score';
}