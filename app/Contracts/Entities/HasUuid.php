<?php declare(strict_types=1);

namespace App\Contracts\Entities;

use Illuminate\Database\Eloquent\Builder;

interface HasUuid
{
    /**
     * Desribe column named 'uuid'
     * 
     * @var string
     */
    public const FIELD_UUID = 'uuid';

    /**
     * Get the query that only scoped for specific `uuid`
     *
     * @param   Builder $query
     * @param   string|array<string>   $uuids
     * @return  Builder
     */
    public function scopeUuids(Builder $query, $uuids): Builder;
}
