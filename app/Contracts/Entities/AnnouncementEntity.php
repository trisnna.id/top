<?php declare(strict_types=1);

namespace App\Contracts\Entities;

interface AnnouncementEntity extends HasUuid
{
    /**
     * Describe a table name for announcements record
     * 
     * @var string
     */
    public const TABLE = 'announcements';

    /**
     * Desribe column named 'name' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_NAME = 'name';

    /**
     * Desribe column named 'localities' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_LOCALITIES = 'localities';

    /**
     * Desribe column named 'content' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_CONTENT = 'content';

    /**
     * Desribe column named 'content_links' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_CONTENT_LINKS = 'content_links';
    
    /**
     * Desribe column named 'checkpoint_id' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_CATEGORY = 'checkpoint_id';

    /**
     * Describe column named 'intakes' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_INTAKES = 'intakes';

    /**
     * Describe column named 'campuses' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_CAMPUSES = 'campuses';

    /**
     * Describe column named 'programmes' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_PROGRAMMES = 'programmes';

    /**
     * Describe column named 'created_at' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_CREATED_AT = 'created_at';

    /**
     * Describe column named 'updated_at' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_UPDATED_AT = 'updated_at';

    /**
     * Describe column named 'deleted_at' for `announcements` table
     * 
     * @var string
     */
    public CONST FIELD_DELETED_AT = 'deleted_at';

    /**
     * Desribe column named 'status' for `pre_orientations` table
     * 
     * @var string
     */
    public CONST FIELD_STATUS = 'status';

    /**
     * A field used to store the announcement thumbnail path
     * 
     * @var string
     */
    public CONST FIELD_THUMBNAIL = 'thumbnail';

    /**
     * A field used to store the announcement sequence number
     * 
     * @var string
     */
    public CONST FIELD_SEQUENCE = 'sequence';

    /**
     * Get all the pre-orientation statuses
     * 
     * @var array 
     */
    public const STATUSES = [
        self::STATUS_DRAFT, self::STATUS_PUBLISHED, self::STATUS_UNPUBLISHED,
    ];

    /**
     * The status `draft' means that the `pre-orientation` has been saved and can be accessed
     * anytime for further editing.
     * 
     * @var string
     */
    public CONST STATUS_DRAFT = 'draft';

    /**
     * The status `published' means that the `pre-orientation` is in its published state and
     * can be viewed by relevant students
     * 
     * @var string
     */
    public CONST STATUS_PUBLISHED = 'published';

    /**
     * The status `unpublished' means that the `pre-orientation`
     * This status is only possible to reach after an item is published.
     * In this status, the activity becomes unpublished and editable again
     * 
     * @var string
     */
    public CONST STATUS_UNPUBLISHED = 'unpublished';
}
