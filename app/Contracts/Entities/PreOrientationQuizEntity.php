<?php declare(strict_types=1);

namespace App\Contracts\Entities;

use App\Contracts\Entities\Relations\HasPreOrientation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

interface PreOrientationQuizEntity extends HasPreOrientation
{
    /**
     * Desribe column named 'question' for `pre_orientation_quisz` table
     * 
     * @var string
     */
    public CONST FIELD_QUESTION = 'question';

    /**
     * Desribe column named 'options' for `pre_orientation_quisz` table
     * 
     * @var string
     */
    public CONST FIELD_OPTIONS = 'options';

    /**
     * Desribe column named 'answers' for `pre_orientation_quisz` table
     * 
     * @var string
     */
    public CONST FIELD_ANSWERS = 'answers';

    /**
     * Desribe column named 'settings' for `pre_orientation_quisz` table
     * 
     * @var string
     */
    public CONST FIELD_SETTINGS = 'settings';

    /**
     * Desribe the settings property named 'shuffle'
     * 
     * @var string
     */
    public CONST SETTINGS_SHUFFLE = 'shuffle';

    /**
     * Get shuffled mcq `options` if the `shuffle` setting is active
     *
     * @return  Collection
     */
    public function getShuffledOptionsAttribute(): Collection;
}
