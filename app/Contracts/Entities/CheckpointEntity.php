<?php declare(strict_types=1);

namespace App\Contracts\Entities;

use Illuminate\Database\Eloquent\Relations\HasMany;

interface CheckpointEntity extends HasUuid
{
    /**
     * Desribe column named 'label' for `checkpoints` table
     * 
     * @var string
     */
    public CONST FIELD_LABEL = 'label';

    /**
     * Desribe column named 'value' for `checkpoints` table
     * 
     * @var string
     */
    public CONST FIELD_VALUE = 'value';

    /**
     * Get the `pre-orientations` that are related with this `checkpoint`
     *
     * @return HasMany
     */
    public function preOrientations(): HasMany;
}
