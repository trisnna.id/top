<?php

namespace App\Providers;

use App\Constants\NationalityConstant;
use App\View\Components\Completion\Progress;
use App\View\Components\Forms\PreOrientationQuiz;
use App\View\Components\Inputs\Filters\Status;
use App\View\Components\Inputs\Title;
use App\View\Components\Settings\Form\ResourcesForm;
use App\View\Components\Settings\Layout;
use App\View\Components\Swal\Admin\Forms\ResourceFaq;
use App\View\Components\Swal\Admin\Forms\ResourceFile;
use App\View\Components\Swal\Admin\Forms\ResourceLink;
use App\View\Components\Swal\Admin\Forms\ResourceVideo;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('nationality', function () {
            return NationalityConstant::getInstance();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::components([
            'resource.video' => ResourceVideo::class,
            'resource.link' => ResourceLink::class,
            'resource.file' => ResourceFile::class,
            'resource.faq' => ResourceFaq::class,
        ], 'adminform');

        Blade::components([
            'status' => Status::class
        ], 'filter');

        Blade::components([
            'layout' => Layout::class,
            'form.resources' => ResourcesForm::class,
        ], 'setting');

        Blade::components([
            'preorientationquiz' => PreOrientationQuiz::class,
        ], 'form');

        Blade::components([
            'progress' => Progress::class,
        ], 'completion');

        Paginator::useBootstrap();

        if (config('app.env') === 'production') {
            URL::forceScheme('https');
        }
    }
}
