<?php

namespace App\Providers;

use App\Contracts\Entities;
use App\Models;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Finder\SplFileInfo;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * The repository namespace
     *
     * @var string
     */
    protected string $namespace = "App\\Repositories";

    /**
     * The list of mapped models with entities contract
     *
     * @var array
     */
    protected array $mappedModelsEntities = [
        Entities\AnnouncementEntity::class => Models\Announcement::class,
        Entities\CheckpointEntity::class => Models\Checkpoint::class,
        Entities\CampusEntity::class => Models\Campus::class,
        Entities\IntakeEntity::class => Models\Intake::class,
        Entities\PreOrientationEntity::class => Models\PreOrientation::class,
        Entities\PreOrientationQuizEntity::class => Models\PreOrientationQuiz::class,
        Entities\ProgrammeEntity::class => Models\Programme::class,
        Entities\StudentPreOrientationEntity::class => Models\StudentPreOrientation::class,
        Entities\StudentPreOrientationPercentageEntity::class => Models\StudentPreOrientationPercentage::class,
        Entities\LocalityEntity::class => Models\Locality::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap repositories services.
     *
     * @return void
     */
    public function boot()
    {
        $concretes = $this->concretes();

        $this->bootModelsEntities();

        foreach ($this->abstracts() as $abstract) {
            $concrete = $concretes->filter(function (string $className) use ($abstract) {
                return str_contains($className, $abstract);
            })->values()->first();

            $this->app->bind(
                sprintf("{$this->namespace}\\Contracts\\%s", $abstract),
                sprintf("{$this->namespace}\\Eloquent\\%s", $concrete),
            );
        }
    }

    /**
     * Bind the contact entities with the concrete models
     *
     * @return void
     */
    private function bootModelsEntities(): void
    {
        foreach ($this->mappedModelsEntities as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }

    /**
     * Get collection of eloquent contract repositories file name
     *
     * @return Collection
     */
    private function abstracts()
    {
        return Collection::make(
            File::allFiles(app_path("Repositories/Contracts"))
        )->filter(function (SplFileInfo $file) {
            return !str_contains($file->getFilenameWithoutExtension(), 'Base');
        })->values()->map(function (SplFileInfo $file) {
            return $file->getFilenameWithoutExtension();
        });
    }

    /**
     * Get collection of eloquent repositories file name
     *
     * @return Collection
     */
    private function concretes(): Collection
    {
        return Collection::make(File::allFiles(app_path("Repositories/Eloquent")))->map(function (SplFileInfo $file) {
            return $file->getFilenameWithoutExtension();
        });;
    }
}
