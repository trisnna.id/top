<?php

namespace App\Providers;

use App\Listeners\UserEventSubscriber;
use App\Models\Announcement;
use App\Models\PreOrientation;
use App\Models\PreOrientationQuiz;
use App\Models\StudentPreOrientation;
use App\Models\StudentPreOrientationPercentage;
use App\Observers\AnnouncementObserver;
use App\Observers\PreOrientationObserver;
use App\Observers\PreOrientationQuizObserver;
use App\Observers\StudentPreorientationObserver;
use App\Observers\StudentPreorientationPercentageObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use SocialiteProviders\Manager\SocialiteWasCalled;
use SocialiteProviders\Microsoft\MicrosoftExtendSocialite;

class EventServiceProvider extends ServiceProvider
{
    protected $subscribe = [
        UserEventSubscriber::class,
    ];
    
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SocialiteWasCalled::class => [
            MicrosoftExtendSocialite::class.'@handle',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Announcement::observe(AnnouncementObserver::class);
        PreOrientation::observe(PreOrientationObserver::class);
        PreOrientationQuiz::observe(PreOrientationQuizObserver::class);
        StudentPreOrientation::observe(StudentPreorientationObserver::class);
        StudentPreOrientationPercentage::observe(StudentPreorientationPercentageObserver::class);
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
