<?php

namespace App\Imports\Admin\Setting;

use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SurveyImport implements WithHeadingRow {

    /**
     * With Heading Row
     *
     * @return integer
     */
    public function headingRow(): int {
        return 1;
    }

}
