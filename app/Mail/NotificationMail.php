<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class NotificationMail extends Mailable
{

    public $subject;
    public $content;
    public $mailer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $content, $mailer = 'smtp')
    {
        $this->subject = $subject;
        $this->content = $content;
        $this->mailer = $mailer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $fromAddress = config("mail.mailers.{$this->mailer}.from.address");
        $fromName = config("mail.mailers.{$this->mailer}.from.name");
        return $this->from($fromAddress, $fromName)
            ->subject($this->subject)
            ->markdown('vendor.mail.html.message', [
                'slot' => $this->content
            ]);
    }
}
