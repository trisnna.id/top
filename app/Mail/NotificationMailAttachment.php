<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class NotificationMailAttachment extends Mailable
{

    public $subject;
    public $content;
    public $attachmentPaths;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $content, $mailer = 'smtp', $attachmentPaths = [])
    {
        $this->subject = $subject;
        $this->content = $content;
        $this->mailer = $mailer;
        $this->attachmentPaths = $attachmentPaths;
    }

    /**
     * Build the message.
     * Example attachment: public_path('media/files/How to add TOP to Home Screen.pdf')
     * @return $this
     */
    public function build()
    {
        $fromAddress = config("mail.mailers.{$this->mailer}.from.address");
        $fromName = config("mail.mailers.{$this->mailer}.from.name");
        $message = $this->from($fromAddress, $fromName)
            ->subject($this->subject)
            ->markdown('vendor.mail.html.message', [
                'slot' => $this->content
            ]);
        foreach ($this->attachmentPaths as $attachmentPath) {
            $message->attach($attachmentPath);
        }
        return $message;
    }
}
