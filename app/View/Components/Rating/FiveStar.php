<?php

namespace App\View\Components\Rating;

use Illuminate\View\Component;

class FiveStar extends Component
{
    public $rating;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($rating)
    {
        $this->rating = $rating;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $checks = ['', '', '', '', ''];
        for ($x = 0; $x < $this->rating; $x++) {
            $checks[$x] = 'checked';
        }
        return view('components.rating.five-star', compact('checks'));
    }
}
