<?php

namespace App\View\Components\Settings;

use stdClass;
use Illuminate\Support\Collection;
use Illuminate\View\Component;

class Layout extends Component
{
    public string $searchPlaceholder;
    public string $addBtnTitle;
    public bool $withCardHeader;

    /**
     * Create a new component instance.
     *
     * @param   string $searchPlaceholder
     * @param   string $addBtnTitle
     * @param   string $addBtnTitle
     * @return  void
     */
    public function __construct(string $searchPlaceholder = 'Search', string $addBtnTitle = 'Add New', bool $withCardHeader = false)
    {
        $this->searchPlaceholder = $searchPlaceholder;
        $this->addBtnTitle = $addBtnTitle;
        $this->withCardHeader = $withCardHeader;
    }

    /**
     * Get menu title
     *
     * @return  string
     */
    public function getActiveNavTitle(): string
    {
        return collect($this->navs())->filter(function ($instance) {
            return !empty($instance->get('active') ?? null);
        })->first()->get('title') ?? '';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.settings.layout', [
            'navs' => $this->navs()
        ]);
    }

    /**
     * Determine if the current nav link is active
     *
     * @param   Collection $nav
     * @return  bool
     */
    public function isActiveNav(Collection $nav): bool
    {
        return !empty($nav->get('active') ?? null);
    }

    /**
     * Get setting navigation links
     *
     * @return array
     */
    protected function navs(): array
    {
        $icons = getConstant('IconConstant');
        return collect([
            collect([
                'icon-img' => url('media/icons/taylors/orientation.png'),
                'url' => route('admin.settings.orientation.index'),
                'active' => routeSlugActive('admin.settings.orientation.*'),
                'title' => 'Orientation',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/email.png'),
                'url' => route('admin.settings.mails.index'),
                'active' => routeSlugActive('admin.settings.mails.*'),
                'title' => 'Email Management',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/resource-file.png'),
                'url' => route('admin.settings.files.index'),
                'active' => routeSlugActive('admin.settings.files.index'),
                'title' => 'File Sizes and Types',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/campus.png'),
                'url' => route('admin.settings.campuses.index'),
                'active' => routeSlugActive('admin.settings.campuses.index'),
                'title' => 'Institution',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/faculty.png'),
                'url' => route('admin.settings.faculties.index'),
                'active' => routeSlugActive('admin.settings.faculties.index'),
                'title' => 'Faculty',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/intake.png'),
                'url' => route('admin.settings.intakes.index'),
                'active' => routeSlugActive('admin.settings.intakes.index'),
                'title' => 'Intake',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/level.png'),
                'url' => route('admin.settings.level-studies.index'),
                'active' => routeSlugActive('admin.settings.level-studies.index'),
                'title' => 'Level of Study',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/locality.svg'),
                'url' => route('admin.settings.localities.index'),
                'active' => routeSlugActive('admin.settings.localities.index'),
                'title' => 'Locality',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/mobility.png'),
                'url' => route('admin.settings.mobilities.index'),
                'active' => routeSlugActive('admin.settings.mobilities.index'),
                'title' => 'Mobility',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/programme1.png'),
                'url' => route('admin.settings.programmes.index'),
                'active' => routeSlugActive('admin.settings.programmes.index'),
                'title' => 'Programme',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/school.png'),
                'url' => route('admin.settings.schools.index'),
                'active' => routeSlugActive('admin.settings.schools.index'),
                'title' => 'School',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/portal.png'),
                'url' => route('admin.settings.portal.index'),
                'active' => routeSlugActive('admin.settings.portal.index'),
                'title' => 'Portal',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/page.png'),
                'url' => route('admin.settings.pages.index'),
                'active' => routeSlugActive('admin.settings.pages.index'),
                'title' => 'Page',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/page.png'),
                'url' => route('admin.settings.survey.index'),
                'active' => routeSlugActive('admin.settings.survey.index'),
                'title' => 'Survey',
            ]),
            collect([
                'icon-img' => url('media/icons/taylors/page.png'),
                'url' => route('admin.settings.quicklink.index'),
                'active' => routeSlugActive('admin.settings.quicklink.index'),
                'title' => 'Quick Link',
            ]),
        ])
            ->sortBy('title')
            ->all();
    }
}
