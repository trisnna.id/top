<?php

namespace App\View\Components\Badges;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class Localities extends Component
{
    /**
     * List of locality entity
     *
     * @var Collection
     */
    public Collection $localities;

    /**
     * Create a new component instance.
     *
     * @param   array $localities
     * @return  void
     */
    public function __construct(array $localities)
    {
        $this->localities = nationality()->get($localities, 'name');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.badges.localities', [
            'localities' => $this->localities
        ]);
    }
}
