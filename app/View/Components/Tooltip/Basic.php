<?php

namespace App\View\Components\Tooltip;

use Illuminate\View\Component;

class Basic extends Component
{

    /**
     * Reference: https://getbootstrap.com/docs/4.0/components/tooltips/
     */
    public $color;
    public $title;
    public $placement;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($color = 'blue', $title = 'Please provide tooltip here', $placement = 'bottom')
    {
        $this->color = $color;
        $this->placement = $placement;
        $this->title = __($title);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.tooltip.basic');
    }
}
