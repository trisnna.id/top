<?php

namespace App\View\Components\Inputs\Filters;

use Illuminate\View\Component;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class Status extends Component
{
    public Collection $options;

    /**
     * Create a new component instance.
     *
     * @param  array $options
     * @return void
     */
    public function __construct(array $options = [])
    {
        if (!empty($options)) {
            if (!Arr::isAssoc($options)) {
                $this->options = Collection::make($options)->transform(function ($item) {
                    return collect(['label' => Str::title($item), 'value' => $item]);
                });
            } else {
                $this->options = Collection::make($options);
            }
        } else {
            $this->options = Collection::make([
                'approved'  => collect(['label' => 'Approved',      'value' => '1']),
                'pending'   => collect(['label' => 'Pending',       'value' => '2']),
                'process'   => collect(['label' => 'In Process',    'value' => '3']),
                'rejected'  => collect(['label' => 'Rejected',      'value' => '4']),
            ]);
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.inputs.filters.status');
    }
}
