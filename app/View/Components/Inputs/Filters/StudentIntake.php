<?php

namespace App\View\Components\Inputs\Filters;

use Illuminate\View\Component;
use Illuminate\Support\Collection;
use App\Repositories\Contracts\IntakeRepository;

class StudentIntake extends Component
{
    /**
     * Create a new intake repository instance
     * 
     * @var     IntakeRepository
     */
    private IntakeRepository $intake;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(IntakeRepository $intake)
    {
        $this->intake = $intake;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.inputs.filters.student-intake', [
            'options' => $this->options(),
        ]);
    }

    /**
     * Get options of intakes
     * 
     * @return Collection
     */
    public function options(): Collection
    {
        return $this->intake->getActive();
    }
}
