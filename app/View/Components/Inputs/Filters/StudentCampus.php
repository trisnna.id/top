<?php

namespace App\View\Components\Inputs\Filters;

use Illuminate\View\Component;
use Illuminate\Support\Collection;
use App\Repositories\Contracts\CampusRepository;

class StudentCampus extends Component
{
    /**
     * Create a new campus repository instance
     * 
     * @var     CampusRepository
     */
    private CampusRepository $campus;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(CampusRepository $campus)
    {
        $this->campus = $campus;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.inputs.filters.student-campus', [
            'options' => $this->options(),
        ]);
    }

    /**
     * Get options of campuss
     * 
     * @return Collection
     */
    public function options(): Collection
    {
        return $this->campus->getActive();
    }
}
