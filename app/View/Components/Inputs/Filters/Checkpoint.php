<?php

namespace App\View\Components\Inputs\Filters;

use Illuminate\View\Component;
use Illuminate\Support\Collection;
use App\Repositories\Contracts\CheckpointRepository;

class Checkpoint extends Component
{
    /**
     * Create a new intake repository instance
     * 
     * @var     CheckpointRepository
     */
    private CheckpointRepository $checkpoint;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(CheckpointRepository $checkpoint)
    {
        $this->checkpoint = $checkpoint;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.inputs.filters.checkpoint', [
            'options' => $this->options(),
        ]);
    }

    /**
     * Get options of checkpoints
     * 
     * @return Collection
     */
    public function options(): Collection
    {
        return $this->checkpoint->all();
    }
}
