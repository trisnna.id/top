<?php

namespace App\View\Components\Inputs\Filters;

use Illuminate\View\Component;

class PreOrientationStatus extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.inputs.filters.pre-orientation-status');
    }
}
