<?php

namespace App\View\Components\Inputs\Filters;

use Illuminate\View\Component;
use Illuminate\Support\Collection;
use App\Repositories\Contracts\ProgrammeRepository;

class StudentProgramme extends Component
{
    /**
     * Create a new programme repository instance
     * 
     * @var     ProgrammeRepository
     */
    private ProgrammeRepository $programme;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(ProgrammeRepository $programme)
    {
        $this->programme = $programme;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.inputs.filters.student-programme', [
            'options' => $this->options(),
        ]);
    }

    /**
     * Get options of programmes
     * 
     * @return Collection
     */
    public function options(): Collection
    {
        return $this->programme->getActive();
    }
}
