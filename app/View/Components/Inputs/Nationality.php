<?php

namespace App\View\Components\Inputs;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

class Nationality extends Component
{
    use WithOldInput;

    /**
     * The current nationality input value
     *
     * @var array|null
     */
    public ?array $current;

    /**
     * The container class names of this component
     *
     * @var string
     */
    public string $containerClassName;

    /**
     * The module names of this component
     *
     * @var string
     */
    public string $moduleName;

    /**
     * The input name
     * 
     * @var string
     */
    public CONST NAME = 'nationality';

    /**
     * Create a new component instance.
     *
     * @param   array   $current
     * @param   string  $containerClassName
     * @param   string  $moduleName
     * @return  void
     */
    public function __construct(string $containerClassName, string $moduleName, ?array $current = [])
    {
        $this->current = $current;
        $this->containerClassName = $containerClassName;
        $this->moduleName = $moduleName;
    }

    /**
     * Get error class name
     *
     * @param   bool    $isError
     * @return  string
     */
    public function renderError(bool $isError): string
    {
        return $isError ? 'is-invalid' : '';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.inputs.nationality', [
            'name' => self::NAME,
            'value' => $this->toString(),
            'options' => $this->options(),
        ]);
    }

    /**
     * Get locality options
     *
     * @return  Collection
     */
    protected function options(): Collection
    {
        return nationality()->toOptions();
    }

    /**
     * Get input value as string
     *
     * @return  string
     */
    protected function toString(): string
    {
        $value = $this->current;

        if (empty($value)) {
            $value = $this->oldInput(self::NAME, []);
        }

        return implode(',', $value);
    }
}
