<?php

namespace App\View\Components\Inputs;

use Illuminate\View\Component;

class Description extends Component
{
    public bool $grouping;

    /**
     * Create a new component instance.
     *
     * @param   bool $grouping
     * @return  void
     */
    public function __construct(bool $grouping)
    {
        $this->grouping = $grouping;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.inputs.description');
    }

    public function isRequired($required): bool
    {
        return $required === true;
    }
}
