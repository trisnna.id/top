<?php declare(strict_types=1);

namespace App\View\Components\Inputs;

trait WithOldInput
{
    /**
     * Get the old input value from session
     *
     * @param   string $name
     * @param   mixed $default
     * @return  mixed
     */
    public function oldInput(string $name, mixed $default = null): mixed
    {
        return old($name, $default);
    }
}
