<?php

namespace App\View\Components\Inputs;

use Illuminate\View\Component;

class Error extends Component
{
    /**
     * The input name
     *
     * @var string
     */
    public string $name;

    /**
     * Create a new component instance.
     *
     * @param   string $name
     * @return  void
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.inputs.error');
    }
}
