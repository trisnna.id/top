<?php

namespace App\View\Components\Datatables;

use App\Constants\ActiveInactiveConstant;
use Illuminate\View\Component;

class Status extends Component
{
    public bool $status;
    public $message;
    public string $url;

    /**
     * Create a new component instance.
     *
     * @param   bool $status
     * @param   string $message
     * @param   string $url
     * @return  void 
     */
    public function __construct(bool $status, string $message = null, string $url = '#')
    {
        $this->status = $status;
        $this->message = $message;
        $this->url = $url;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.datatables.status', [
            'instance' => ['upsert' => $this->config()],
        ]);
    }

    private function config(): array
    {
        $title = 'Activated';
        $data = ActiveInactiveConstant::getActiveOption();

        if (!$this->status) {
            $title = 'Deactivated';
            $data = ActiveInactiveConstant::getInActiveOption();
        }

        if (is_null($this->message)) {

            $this->message = sprintf('Are you sure to %s?', $title);
        }

        return [
            'data' => ['status' => $data['title'], 'color' => $data['color']],
            'swal' => [
                'id' => "swal-blank",
                'settings' => [
                    'icon' => 'warning',
                    'title' => $this->message,
                    'html' => "You can change it back later!",
                    'confirmButtonText' => 'Confirm',
                ],
                'axios' => [
                    'url' => $this->url,
                    'method' => 'put'
                ]
            ]
        ];
    }
}
