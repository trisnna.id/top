<?php

namespace App\View\Components\Datatables;

use Illuminate\View\Component;

class Export extends Component
{
    public $id;
    public $colorClass;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id = null, string $colorClass = "primary")
    {
        $this->id = $id;
        $this->colorClass = $colorClass;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.datatables.export');
    }
}
