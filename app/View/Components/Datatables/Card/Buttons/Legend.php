<?php

namespace App\View\Components\Datatables\Card\Buttons;

use Illuminate\View\Component;

class Legend extends Component
{
    /**
     * The gap value to determine legend's space
     *
     * @var int
     */
    public int $gap;

    /**
     * The legend that won't being render in the document
     *
     * @var array
     */
    public array $exclude;

    /**
     * The legend that will be render in the document
     *
     * @var array
     */
    public array $include;

    /**
     * Create a new component instance.
     *
     * 
     * @param   int $gap
     * @param   array $exclude
     * @param   array $include
     * @return  void
     */
    public function __construct(int $gap, array $exclude = [], array $include = [])
    {
        $this->gap = $gap;
        $this->exclude = $exclude;
        $this->include = $include;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.datatables.card.buttons.legend', [
            'legends' => $this->legends()
        ]);
    }

    /**
     * Determine if the specified legend name is need to be render in the DOM.
     *
     * @param   string $legendKey
     * @return  bool
     */
    public function isShouldRender(string $legendKey): bool
    {
        if (!empty($this->exclude) && empty($this->include)) {
            return !in_array($legendKey, $this->exclude);
        }

        if (!empty($this->include) && empty($this->exclude)) {
            return in_array($legendKey, $this->include);
        }

        return false;
    }

    /**
     * Get the legends description
     *
     * @return array
     */
    private function legends(): array
    {
        $iconConstants = getConstant('IconConstant');
        return [
            'show' => ['badge' => $iconConstants['btn_show']['color']['class'], 'icon' => $iconConstants['btn_show']['icon_class'], 'label' => 'Detail'],
            'edit' => ['badge' => $iconConstants['btn_edit']['color']['class'], 'icon' => $iconConstants['btn_edit']['icon_class'], 'label' => 'Edit'],
            'delete' => ['badge' => $iconConstants['btn_delete']['color']['class'], 'icon' => $iconConstants['btn_delete']['icon_class'], 'label' => 'Delete'],
            'clone' => ['badge' => $iconConstants['btn_clone']['color']['class'], 'icon' => $iconConstants['btn_clone']['icon_class'], 'label' => 'Clone'],
            'approval' => ['badge' => $iconConstants['btn_approval']['color']['class'], 'icon' => $iconConstants['btn_approval']['icon_class'], 'label' => 'Approval'],
            'submit' => ['badge' => $iconConstants['btn_submit']['color']['class'], 'icon' => $iconConstants['btn_submit']['icon_class'], 'label' => 'Submit'],
            'publish' => ['badge' => $iconConstants['btn_publish']['color']['class'], 'icon' => $iconConstants['btn_publish']['icon_class'], 'label' => 'Publish'],
            'unpublish' => ['badge' => $iconConstants['btn_unpublish']['color']['class'], 'icon' => $iconConstants['btn_unpublish']['icon_class'], 'label' => 'Unpublish'],
        ];
    }
}
