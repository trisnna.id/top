<?php

namespace App\View\Components\Datatables;

use Illuminate\View\Component;

class Search extends Component
{
    public $id;
    public $placeholder;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id = null, string $placeholder = 'Search')
    {
        $this->id = $id;
        $this->placeholder = $placeholder;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.datatables.search');
    }
}
