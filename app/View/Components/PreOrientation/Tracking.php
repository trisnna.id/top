<?php

namespace App\View\Components\PreOrientation;

use App\Models\PreOrientation;
use Illuminate\View\Component;
use App\Repositories\Contracts\CheckpointRepository;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Repositories\Contracts\StudentPreOrientationRepository;

class Tracking extends Component
{
    /**
     * PreOrientation repository
     * 
     * @var PreOrientationRepository
     */
    private PreOrientationRepository $preOrientation;

    /**
     * Checkpoint repository
     * 
     * @var CheckpointRepository
     */
    private CheckpointRepository $checkpoint;

    /**
     * Student pre-orientation repository
     * 
     * @var StudentPreOrientationRepository
     */
    private StudentPreOrientationRepository $studentPreOrientation;

    private PreOrientation $preOrientationModel;

    /**
     * Create a new component instance.
     *
     * @param PreOrientationRepository $preOrientation
     * @param CheckpointRepository $checkpoint
     * @return void
     */
    public function __construct(PreOrientationRepository $preOrientation, CheckpointRepository $checkpoint, StudentPreOrientationRepository $studentPreOrientation, PreOrientation $preOrientationModel)
    {
        $this->preOrientation = $preOrientation;
        $this->checkpoint = $checkpoint;
        $this->studentPreOrientation = $studentPreOrientation;
        $this->preOrientationModel = $preOrientationModel;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.pre-orientation.tracking', [
            'preOrientation' => $this->preOrientation->dataTable($this->preOrientationModel)->get(),
            'checkpoints' => $this->checkpoint->all(),
            'studentPreOrientation' => $this->studentPreOrientation->all()
        ]);
    }

    /**
     * Get the color classname base on the current value
     *
     * @param string $value
     * @return string
     */
    public function color(string $value): string
    {
        $value = (int) $value;
    
        if ($value <= 50) {
            return 'bg-danger';
        }
    
        return 'bg-primary';
    }
    
    public function textColor(string $value): string
    {
        $value = (int) $value;
    
        if ($value <= 50) {
            return '#ff0000';
        }
    
        return '#0070c0';
    }
}
