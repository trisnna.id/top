<?php

namespace App\View\Components\PreOrientation;

use App\Contracts\Entities\StudentPreOrientationEntity;
use App\Repositories\Contracts\PreOrientationQuizRepository;
use App\Repositories\Contracts\StudentPreOrientationRepository;
use App\Services\AuthService;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\View\Component;

class Quiz extends Component
{
    /**
     * The pre-orientation uuid key
     *
     * @var string
     */
    public string $key;

    /**
     * The pre-orientation quiz milestone
     *
     * @var string
     */
    public string $milestone;

    /**
     * The pre-orientation activity name
     *
     * @var string
     */
    public string $title;

    /**
     * The pre-orientation quiz questions
     *
     * @var Collection
     */
    public Collection $questions;

    /**
     * The student quiz
     *
     * @var StudentPreOrientationEntity|null
     */
    public ?StudentPreOrientationEntity $studentQuiz;

    /**
     * Create a new auth service instance
     *
     * @var AuthService
     */
    private AuthService $auth;

    /**
     * Create a new component instance
     *
     * @param PreOrientationQuizRepository $preOrientationQuiz
     * @param StudentPreOrientationRepository $studentPreorientation
     * @param string    $key
     * @param string    $title
     * @param string    $milestone
     * @param int       $target
     */
    public function __construct(
        PreOrientationQuizRepository $preOrientationQuiz,
        StudentPreOrientationRepository $studentPreorientation,
        string $key,
        string $title,
        string $milestone,
        int $target
    ) {
        $this->key = $key;
        $this->title = $title;
        $this->milestone = $milestone;
        $this->auth = new AuthService();
        $this->questions = $preOrientationQuiz->findByPreOrientation($target);
        $this->studentQuiz = $studentPreorientation->findByPreOrientation(
            Arr::get($this->questions->get(0), 'pre_orientation_id', 0)
        );
    }

    /**
     * Get 'is-valid' class name base on condition
     *
     * @param   bool    $markAsAnswer
     * @return  string
     */
    public function renderIsValid(bool $markAsAnswer): string
    {
        $student = $this->auth->student();

        if (!is_null($student)) {
            return '';
        }

        if ($markAsAnswer) {
            return 'is-valid';
        }
    }

    public function renderTitle($quizId, $option)
    {
        if (!is_null($this->studentQuiz) && optional($this->studentQuiz->quizzes)->isNotEmpty()) {
            $studentAnswer = $this->studentQuiz->quizzes->first(function ($item) use ($quizId) {
                return Arr::get($item, 'quiz_id') == $quizId;
            });

            if ($option->isCorrect() && in_array($option->value, Arr::get($studentAnswer, 'answers', []))) {
                return "Your answer is correct";
            } else if (!$option->isCorrect() && in_array($option->value, Arr::get($studentAnswer, 'answers', []))) {
                return "Your answer is wrong";
            } else if ($option->isCorrect()) {
                return "Correct answer";
            }
        }

        return 'Wrong answer';
    }

    public function shouldChecked($quizId, $option)
    {
        if (!is_null($this->studentQuiz) && optional($this->studentQuiz->quizzes)->isNotEmpty()) {
            $studentAnswer = $this->studentQuiz->quizzes->first(function ($item) use ($quizId) {
                return Arr::get($item, 'quiz_id') == $quizId;
            });

            if (
                in_array($option->value, Arr::get($studentAnswer, 'answers', [])) ||
                $option->isCorrect()
            ) {
                return 'checked';
            }
        }

        return '';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.pre-orientation.quiz', [
            'target' => Arr::get($this->questions->get(0), 'pre_orientation_id', '')
        ]);
    }

    public function renderHtml($shuffled)
    {
        return view('components.pre-orientation.quiz', [
            'target' => Arr::get($this->questions->get(0), 'pre_orientation_id', ''),
            'key' => $this->key,
            'questions' => $this->questions,
            'milestone' => $this->milestone,
            'title' => $this->title,
            'studentQuiz' => $this->studentQuiz,
            'renderIsValid' => [$this, 'renderIsValid'],
            'shouldChecked' => [$this, 'shouldChecked'],
            'renderTitle' => [$this, 'renderTitle'],
        ])->render();
    }
}
