<?php

namespace App\View\Components\Completion;

use Illuminate\View\Component;
use Illuminate\Support\Str;

class Progress extends Component
{
    /**
     * The width size of the inner container
     *
     * @var string
     */
    public string $widthClass;

    /**
     * Completion title
     *
     * @var string
     */
    public string $title;

    /**
     * The minimum value of completion
     *
     * @var int
     */
    public string $min;

    /**
     * The maximum value of completion
     *
     * @var int
     */
    public string $max;

    /**
     * The value value of completion
     *
     * @var int
     */
    public string $value;

    /**
     * Create a new component instance.
     *
     * @param   string  $min
     * @param   string  $max
     * @param   string  $value
     * @param   string  $title
     * @param   string  $widthClass
     * @param   string  $alignItem
     * @param   string  $flexDirection
     * @return  void
     */
    public function __construct(string $title, string $value, string $min = '0', string $max = '100', string $widthClass = 'w-200px w-sm-300px',)
    {
        $this->title = Str::title($title);
        $this->min = $min;
        $this->max = $max;
        $this->value = $value;
        $this->widthClass = $widthClass;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.completion.progress');
    }

    /**
     * Get the color classname base on the current value
     *
     * @param string $value
     * @return string
     */
    public function color(string $value): string
    {
        $value = (int) $value;

        if ($value <= 25) {
            return 'bg-danger';
        } else if ($value > 25 && $value <= 50) {
            return 'bg-warning';
        } else if ($value > 50 && $value < $this->max) {
            return 'bg-light-success';
        }

        return 'bg-success';
    }
}
