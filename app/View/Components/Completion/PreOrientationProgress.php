<?php

namespace App\View\Components\Completion;

use Illuminate\View\Component;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Repositories\Contracts\StudentPreOrientationRepository;

class PreOrientationProgress extends Component
{
    /**
     * Create a new student preorientation repository instance
     * 
     * @var StudentPreOrientationRepository
     */
    private StudentPreOrientationRepository $studentPreOrientation;

    /**
     * Create a new preorientation repository instance
     * 
     * @var PreOrientationRepository
     */
    private PreOrientationRepository $preOrientation;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(StudentPreOrientationRepository $studentPreOrientation, PreOrientationRepository $preOrientation)
    {
        $this->studentPreOrientation = $studentPreOrientation;
        $this->preOrientation = $preOrientation;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $countCompleted = $this->studentPreOrientation->getCompleted()->unique('pre_orientation_id')->values()->count();
        $countPreOrientation = $this->preOrientation->countPublished();
        return view('components.completion.progress', [
            'value' => $countCompleted > 0 ? round((100 / $countPreOrientation) * $countCompleted) : 0,
            'title' => 'Pre-Orientation Progress',
            'widthClass' => 'w-200px w-sm-300px',
            'min' => '0',
            'max' => '100',
        ]);
    }

    /**
     * Get the color classname base on the current value
     *
     * @param string $value
     * @return string
     */
    public function color(string $value): string
    {
        $value = (int) $value;

        if ($value <= 25) {
            return 'bg-danger';
        } else if ($value > 25 && $value <= 50) {
            return 'bg-warning';
        } else if ($value > 50 && $value < 100) {
            return 'bg-dark-success';
        }

        return 'bg-success';
    }

    public function textColor(string $value): string
    {
        $value = (int) $value;

        if ($value >= 50) {
            return 'text-progress-dark';
        } else {
            return 'text-red';
        }
    }
}
