<?php

namespace App\View\Components\Share;

use Illuminate\View\Component;
use Illuminate\Support\Collection;

class Breadcrumb extends Component
{
    /**
     * The list of breadcrumb default links
     *
     * @var array
     */
    public Collection $defaultLinks;

    /**
     * Create a new component instance.
     *
     * @param array $links
     * @return void
     */
    public function __construct(array $links = [])
    {
        $this->defaultLinks = Collection::make([
            collect(['url' => url('/'), 'text' => 'Home']),
            collect(['url' => route('dashboard.index'), 'text' => 'Dashboard']),
        ])->merge(collect($links)->transform(fn (array $link) => collect($link)));
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.share.breadcrumb');
    }
}
