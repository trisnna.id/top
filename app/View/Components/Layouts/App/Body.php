<?php

namespace App\View\Components\Layouts\App;

use App\View\Components\Layouts\App as AppComponent;

class Body extends AppComponent
{
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        if (request()->route()->getPrefix() == "/admin") {
            $sidebarLayout = true;
        } else {
            $sidebarLayout = false;
        }
        return view('components.layouts.app.body', compact('sidebarLayout'));
    }
}
