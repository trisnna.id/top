<?php

namespace App\View\Components\Layouts\App\Body\Header;

use App\Models\Mail;
use App\Models\Notification as NotificationModel;
use Illuminate\View\Component;

class Notification extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $user = auth()->user();
        $mails = Mail::all();
        $notificationQuery = NotificationModel::query()
            ->select(['notifications.*'])
            ->where([
                'notifiable_type' => $user::class,
                'notifiable_id' => $user->id,
            ]);
        $countNotificationsByReadAtNull = $notificationQuery->clone()->whereNull('read_at')->count();
        $notifications = $notificationQuery
            ->limit(5)
            ->when($countNotificationsByReadAtNull > 0, function($query){
                $query->orderBy('read_at', 'asc');
            })
            ->orderBy('created_at', 'desc')
            ->get()
            ->map(function ($instance) use ($mails) {
                $instance->html = null;
                $notification = new $instance->type();
                $template = $notification->template ?? null;
                if (!empty($template ?? null)) {
                    $notificationTemplate = $mails->where('name', $template)->first()->notification ?? null;
                    if (!empty($notificationTemplate ?? null)) {
                        $content = $this->parse(trim($notificationTemplate), $instance->data);
                        $instance->html = view('datatables.profile.notification.notificationdt.data', compact('instance', 'content'))->render();
                    }
                }
                $instance->url = ($instance->data['link'] ?? ($instance->data['orientationUrl'] ?? null));
                return $instance;
            });

        return view('components.layouts.app.body.header.notification', compact('notifications', 'countNotificationsByReadAtNull'));
    }

    public function parse($stub, $data)
    {
        $replace = $this->replacements($data);
        return str_replace(array_keys($replace), array_values($replace), $stub);
    }

    public function replacements($data)
    {
        $replace = [];
        foreach ($data as $key => $value) {
            $replace['{{ ' . $key . ' }}'] = $value;
            $replace['{{' . $key . '}}'] = $value;
        }
        return $replace;
    }
}
