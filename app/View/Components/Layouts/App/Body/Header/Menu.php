<?php

namespace App\View\Components\Layouts\App\Body\Header;

use Illuminate\View\Component;

class Menu extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        if(auth()->user()->can('student')){
            return view('components.layouts.app.body.header.menu');
        }
        
    }
}
