<?php

namespace App\View\Components\Layouts;

use Illuminate\View\Component;

class App extends Component
{
    public $title;
    public $toolbar;
    public $toolbarClass;
    public $toolbarStyle;
    public $showFooter;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title = null, $toolbar = null, $toolbarClass = null, $toolbarStyle = null, $showFooter = true)
    {
        $this->title = $title;
        $this->toolbar = $toolbar;
        $this->toolbarClass = $toolbarClass;
        $this->toolbarStyle = $toolbarStyle;
        $this->showFooter = $showFooter;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.layouts.app');
    }
}
