<?php

namespace App\View\Components\Layouts\Auth;

use App\View\Components\Layouts\Auth as AuthComponent;

class Body extends AuthComponent
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.layouts.auth.body');
    }
}
