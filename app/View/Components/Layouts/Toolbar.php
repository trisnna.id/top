<?php

namespace App\View\Components\Layouts;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

class Toolbar extends Component
{
    public string $page;
    public string $title;

    /**
     * Create a new component instance.
     *
     * @param   string $page
     * @param   string $title
     * @return  void
     */
    public function __construct(string $page, string $title)
    {
        $this->page = $page;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.layouts.toolbar', [
            'navs' => $this->options()
        ]);
    }

    protected function options(): array
    {
        switch ($this->page) {
            case 'resources':
                return [
                    Collection::make(['label' => 'Home', 'link' => url('/')]),
                    Collection::make(['label' => 'Dashboard', 'link' => route('dashboard.index')]),
                    Collection::make(['label' => 'Top Management',]),
                ];
            case 'announcements':
                return [
                    Collection::make(['label' => 'Home', 'link' => url('/')]),
                    Collection::make(['label' => 'Dashboard', 'link' => route(auth()->user()->hasRole('admin') ?  'admin.announcements.index' : 'dashboard.index')]),
                    Collection::make(['label' => 'News & Announcements',]),
                ];
            default:
                return [];
        }
    }
}
