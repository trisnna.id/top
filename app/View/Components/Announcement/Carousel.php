<?php

namespace App\View\Components\Announcement;

use App\Repositories\Contracts\AnnouncementRepository;
use Illuminate\View\Component;

class Carousel extends Component
{
    /**
     * Create a new announcement repository instance
     * 
     * @var     AnnouncementRepository
     */
    private AnnouncementRepository $announcement;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(AnnouncementRepository $announcement)
    {
        $this->announcement = $announcement;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.announcement.carousel', [
            'announcementsLg' => $this->getAnnouncements(5),
            'announcementsMd' => $this->getAnnouncements(2),
            'announcementsSm' => $this->getAnnouncements(1)
        ]);
    }

    /**
     * Get announcements for carousel that match student's state
     * 
     * @return  Collection
     */
    public function getAnnouncements($count)
    {
        return $this->announcement->getAnnouncementsForCarousel($count);
    }
}
