<?php

namespace App\View\Components\Dashboard;

use App\Services\AuthService;
use Illuminate\View\Component;

class AchievementStars extends Component
{
    /**
     * Create a new auth service instance
     *
     * @var AuthService
     */
    protected AuthService $authService;

    /**
     * Create a new repository instance
     * 
     * @param   AuthService $authService
     * @return  void
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $student = $this->authService->student();
        $data = $student->achievement_percentage;

        $stars = $data / 20;
        $gauge = $data;

        return view('components.dashboard.achievement-stars', [
            'stars' => $stars,
            'gauge' => $gauge,
        ]);
    }
}
