<?php

namespace App\View\Components\Dashboard;

use App\Models\Orientation;
use App\Services\AuthService;
use App\Models\PreOrientation;
use Illuminate\View\Component;
use App\Models\OrientationRsvp;
use App\Models\OrientationAttendance;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Repositories\Contracts\StudentPreOrientationRepository;

class QuickLinks extends Component
{
    public PreOrientationRepository $preOrientationRepository;
    public StudentPreOrientationRepository $studentPreOrientationRepository;
    public AuthService $authService;
    public PreOrientation $preOrientationModel;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(PreOrientationRepository $preOrientationRepository, AuthService $authService, PreOrientation $preOrientationModel, StudentPreOrientationRepository $studentPreOrientationRepository)
    {
        $this->preOrientationRepository = $preOrientationRepository;
        $this->authService = $authService;
        $this->preOrientationModel = $preOrientationModel;
        $this->studentPreOrientationRepository = $studentPreOrientationRepository;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $isVisible = $this->determineVisibility();

        return view('components.dashboard.quick-links', compact('isVisible'));
    }

    public function determineVisibility()
    {
        $student = $this->authService->student();

        $preOrientation = $this->preOrientationRepository->dataTable($this->preOrientationModel)->get();
        $studentPreOrientation = $this->studentPreOrientationRepository->all()->where('student_id', $student->id);

        $preOrientationIds = $preOrientation->pluck('id');
        $studentPreOrientationIds = $studentPreOrientation->pluck('pre_orientation_id');

        $missingPreOrientationIds = $preOrientationIds->diff($studentPreOrientationIds);

        if ($missingPreOrientationIds->isEmpty()) {
            $preor = true;
        } else {
            $preor = false;
        }

        $orientation = Orientation::where([
            ['is_publish', true],
            ['is_rsvp', false],
        ])->get();
        $orientationRsvp = OrientationRsvp::where([
            ['student_id', $student->id],
        ])->get()->load('orientation')->where('orientation.is_publish', true);
        $orientationAttending = OrientationAttendance::where([
            ['student_id', $student->id],
        ])->get()->load('orientation')->where('orientation.is_publish', true);

        $orientationIds = $orientation->pluck('id');
        $orientationRsvpIds = $orientationRsvp->pluck('orientation.id');
        $allOrientationIds = $orientationIds->concat($orientationRsvpIds);
        $orientationAttendingIds = $orientationAttending->pluck('orientation_id');

        $missingOrientationIds = $allOrientationIds->diff($orientationAttendingIds);

        if ($missingOrientationIds->isEmpty()) {
            $orie = true;
        } else {
            $orie = false;
        }
        $orie = true;

        if
        (
            $preor &&
            $orie
        ) {
            return true;
        }
        else {
            return false;
        }
    }
}
