<?php

namespace App\View\Components\Dashboard;

use App\Models\StudentPoint;
use App\Services\AuthService;
use Illuminate\View\Component;
use App\Http\Middleware\Student;
use App\Repositories\Contracts\StudentPreOrientationRepository;

class SurveyChart extends Component
{
    /**
     * Create a new auth service instance
     *
     * @var AuthService
     */
    protected AuthService $authService;

    /**
     * Create a new repository instance
     * 
     * @param   AuthService $authService
     * @return  void
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $student = $this->authService->student();
        $data = $student->survey_percentage;
        return view('components.dashboard.survey-chart', [
            'completed' => $data,
        ]);
    }
}
