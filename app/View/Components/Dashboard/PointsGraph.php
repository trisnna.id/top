<?php

namespace App\View\Components\Dashboard;

use App\Services\AuthService;
use Illuminate\View\Component;

class PointsGraph extends Component
{
    /**
     * Create a new auth service instance
     *
     * @var AuthService
     */
    protected AuthService $authService;

    /**
     * Create a new repository instance
     * 
     * @param   AuthService $authService
     * @return  void
     */
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $studentPoints = $this->getStudentPoints();
        return view('components.dashboard.points-graph', compact('studentPoints'));
    }

    public function getStudentPoints()
    {
        $student = $this->authService->student();
        $studentPoints = $student->studentPoints()->get();

        return $studentPoints;
    }
}
