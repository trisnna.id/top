<?php

namespace App\View\Components\Forms;

use Illuminate\Support\Collection;
use Illuminate\View\Component;

class PreOrientationQuiz extends Component
{
    public $entity;

    /**
     * Create a new component instance.
     *
     * @param  $entity
     * @return void
     */
    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.pre-orientation-quiz');
    }
}
