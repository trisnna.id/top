<?php

namespace App\View\Components\Swal\Admin\Forms;

use Illuminate\View\Component;

class ResourceVideo extends Component
{
    public string $id = 'resourceVideo';

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.swal.admin.forms.resource-video', [
            'toolbarTitle' => 'Add Video'
        ]);
    }

    public function swalSettings(): array
    {
        return [
            'id' => $this->id,
            'settings' => [
                'title' => 'Videos',
                'showCancelButton' => true,
                'showConfirmButton' => true,
                'customClass' => [
                    'popup' => "w-300px w-md-500px",
                ],
            ],
        ];
    }
}
