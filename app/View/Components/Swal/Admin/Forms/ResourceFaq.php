<?php

namespace App\View\Components\Swal\Admin\Forms;

use Illuminate\View\Component;

class ResourceFaq extends Component
{
    public string $id = 'resourceFaq';

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.swal.admin.forms.resource-faq', [
            'toolbarTitle' => 'Add FAQ'
        ]);
    }

    public function swalSettings(): array
    {
        return [
            'id' => $this->id,
            'settings' => [
                'title' => 'Faqs',
                'showCancelButton' => true,
                'showConfirmButton' => true,
                'customClass' => [
                    'popup' => "w-300px w-md-500px",
                ],
            ],
        ];
    }
}
