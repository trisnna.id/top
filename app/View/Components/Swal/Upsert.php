<?php

namespace App\View\Components\Swal;

use Illuminate\View\Component;

class Upsert extends Component
{
    public $accept;
    public $classActions;
    public $classConfirmButton;
    public $classCancelButton;
    public $classCloseButton;
    public $classPopup;
    public $classValidationMessage;
    public $confirmButtonText;
    public $denyButtonText;
    public $cancelButtonText;
    public $icon;
    public $id;
    public $route;
    public $setCancelBtn;
    public $setCloseBtn;
    public $setConfirmBtn;
    public $setDenyBtn;
    public $title;
    public $swalSettings;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        $id,
        $route = null,
        $swalSettings = null,
    ) {
        $this->id = $id;
        $this->route = $route;
        $defaultSwalSettings = [
            'showCloseButton' => true,
            'showCancelButton' => true,
            'showConfirmButton' => true,
            'showDenyButton' => false,
            'confirmButtonText' => '<i class="fa fa-save"></i> Save',
            'cancelButtonText' => 'Cancel',
            'denyButtonText' => '<i class="fa fa-trash"></i> Delete',
            'showLoaderOnConfirm' => true,
            'backdrop' => true,
            'allowOutsideClick' => false,
            'heightAuto' => false,
            'customClass' => [
                'validationMessage' => 'm-0 p-1',
                'content' => 'text-left px-2 px-sm-4',
                'confirmButton' => 'btn btn-success btn-sm',
                'cancelButton' => 'btn btn-danger btn-sm',
            ]
        ];
        $this->swalSettings = json_encode(array_merge($defaultSwalSettings, $swalSettings));
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.swal.upsert');
    }
}
