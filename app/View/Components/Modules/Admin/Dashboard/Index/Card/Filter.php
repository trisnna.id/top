<?php

namespace App\View\Components\Modules\Admin\Dashboard\Index\Card;

use App\Models\Intake;
use Carbon\Carbon;
use Illuminate\View\Component;

class Filter extends Component
{
    public $id;
    public $applyOnClick;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id, $applyOnClick)
    {
        $this->id = $id;
        $this->applyOnClick = $applyOnClick;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $options = $this->getOptions();
        return view('components.modules.admin.dashboard.index.card.filter', compact('options'));
    }

    protected function getOptions()
    {
        $intakeMonth = Carbon::today()->format('Ym');
        return [
            'intake' => Intake::orderBy('name', 'desc')->get()->pluck('name', 'id'),
            'intake_default' => Intake::where('name', '<=', $intakeMonth)->orderBy('name', 'desc')->limit(3)->get()->pluck('id'),
        ];
    }
}
