<?php

namespace App\View\Components\Modules\Admin\Student\Index;

use App\Models\Campus;
use App\Models\Faculty;
use App\Models\Intake;
use App\Models\LevelStudy;
use App\Models\Locality;
use App\Models\Mobility;
use App\Models\Programme;
use App\Models\School;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class Filter extends Component
{
    public $id;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $options = [
            'faculty' => Faculty::orderBy('name')->get()->pluck('name', 'id'),
            'school' => School::orderBy('name')->get()->pluck('name', 'id'),
            'programme' => Programme::orderBy('name')->get()->pluck('name', 'id'),
            'level_study' => LevelStudy::orderBy('name')->get()->pluck('name', 'id'),
            'intake' => Intake::orderBy('name', 'desc')->get()->pluck('name', 'id'),
            'campus' => Campus::orderBy('name')->get()->pluck('name', 'id'),
            'mobility' => Mobility::orderBy('name')->get()->map(function ($mobility) {
                $mobility->name = Str::title($mobility->name);
                return $mobility;
            })->pluck('name', 'id'),
            'locality' => Locality::orderBy('name')->get()->pluck('name', 'id'),
        ];
        return view('components.modules.admin.student.index.filter', compact('options'));
    }
}
