<?php

namespace App\View\Components\Modules\Orientation\Index\Calendar;

use App\Models\OrientationType;
use Illuminate\View\Component;

class Filter extends Component
{
    public $id;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $options = $this->getOptions();
        return view('components.modules.orientation.index.calendar.filter', compact('options'));
    }

    protected function getOptions()
    {
        return [
            'orientation_type' => OrientationType::all()->pluck('title', 'id'),
        ];
    }
}
