<?php

namespace App\Exports\Admin\Orientation;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use App\Constants\OrientationTypeConstant;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OrientationExport implements FromCollection, WithHeadings, WithStyles
{

    protected $orientations;

    public function __construct($orientations)
    {
        $this->orientations = $orientations;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->orientations
            ->sortBy([['start_date', 'asc'], ['start_time', 'asc']])
            ->values()
            ->map(function ($instance) {
                $startTimes = explode(':', $instance->start_time);
                $endTimes = explode(':', $instance->end_time);
                $instance->start_time = Carbon::createFromTime(intval(Arr::get($startTimes, 0, 0)), intval(Arr::get($startTimes, 1, 0)), intval(Arr::get($startTimes, 2, 0)))->format('h:i A');
                $instance->end_time = Carbon::createFromTime(intval(Arr::get($endTimes, 0, 0)), intval(Arr::get($endTimes, 1, 0)), intval(Arr::get($endTimes, 2, 0)))->format('h:i A');
                $instance = $instance->only([
                    'orientation_type_id',
                    'start_date',
                    'start_time',
                    'end_time',
                    'name',
                    'venue',
                    'presenter',
                    'livestream',
                ]);
                $instance['orientation_type_id'] = OrientationTypeConstant::getAvailableOptions('title')[$instance['orientation_type_id']];

                return $instance;
            });
    }

    public function headings(): array
    {
        return [
            'Type',
            'Date',
            'From',
            'To',
            'Activity',
            'Venue',
            'Presenter',
            'Livestream Link',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];

        for ($i = 0; $i < count($cols); $i++) {
            $sheet->getColumnDimension($cols[$i])->setAutoSize(true);
            $sheet->getCell("{$cols[$i]}1")->getStyle()->getAlignment()->setHorizontal('center');
            $sheet->getCell("{$cols[$i]}1")->getStyle()->getFont()->getColor()->setARGB('FFFFFF');
            $sheet->getCell("{$cols[$i]}1")->getStyle()->getFill()->setFillType(Fill::FILL_SOLID);
            $sheet->getCell("{$cols[$i]}1")->getStyle()->getFill()->getStartColor()->setARGB('1d3354');
        }

        foreach ($this->collection() as $i => $orientation) {
            $row = $i+1+1;
            $sheet->getCell("A{$row}")->getStyle()->getFill()->setFillType(Fill::FILL_SOLID);
            if (Arr::get($orientation, 'orientation_type_id') === 'Complementary') {
                $sheet->getCell("A{$row}")->getStyle()->getFill()->getStartColor()->setARGB('ECF8F4');
            } else {
                $sheet->getCell("A{$row}")->getStyle()->getFill()->getStartColor()->setARGB('FEE8EF');
            }
        }
    }
}
