<?php

namespace App\Exports\Admin\Student;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class PreSurveyAnswerExport implements FromCollection, WithHeadings
{

    protected $students;
    protected $header;

    public function __construct($students, $header)
    {
        $this->students = $students;
        $this->header = $header;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->students;
    }

    public function headings(): array
    {
        return $this->header ?? [];
    }
}
