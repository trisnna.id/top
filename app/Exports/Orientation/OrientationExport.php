<?php

namespace App\Exports\Orientation;

use App\Constants\OrientationTypeConstant;
use App\DataTables\Orientation\TimelineDt;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrientationExport implements FromCollection, WithHeadings
{

    protected $httpRequest;
    protected $auth;

    public function __construct(Request $request, $auth = null)
    {
        $this->httpRequest = $request;
        $this->auth = $auth;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $request = $this->httpRequest;
        $auth = $this->auth;
        return (new TimelineDt($request, $auth))->query()->get()
            ->map(function ($instance) {
                $instance = $instance->only([
                    'name',
                    'venue',
                    'livestream',
                    'start_date',
                    'start_time',
                    'end_time',
                    'presenter',
                    'orientation_type_id',
                    'orientation_rsvp_is_go',
                    'orientation_attendance_updated_at',
                ]);
                if (!empty($instance['orientation_rsvp_is_go'] ?? null)) {
                    $instance['orientation_rsvp_is_go'] = $instance['orientation_rsvp_is_go'] == 1 ? 'Yes' : 'No';
                }
                $instance['orientation_type_id'] = OrientationTypeConstant::getAvailableOptions('title')[$instance['orientation_type_id']];
                return $instance;
            });
    }

    public function headings(): array
    {
        return [
            'Type',
            'Date',
            'From',
            'To',
            'Activity',
            'Venue',
            'Presenter',
            'Livestream Link',
            'RSVP',
            'Attendance',
        ];
    }
}
