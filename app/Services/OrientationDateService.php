<?php

namespace App\Services;

use App\Models\Orientation;
use App\Models\OrientationDate;
use App\Traits\Services\BaseServiceTrait;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

class OrientationDateService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new OrientationDate();
    }

    public function fixData()
    {
        $orientations = Orientation::all();
        foreach ($orientations as $orientation) {
            $this->syncDate($orientation);
        }
    }

    public function syncDate(Orientation $orientation)
    {
        $input['orientation_id'] = $orientation->id;
        if (!empty($orientation->start_date ?? null) && !empty($orientation->end_date ?? null)) {
            $this->model->where('orientation_id', $orientation->id)->delete();
            $period = new CarbonPeriod($orientation->start_date, '1 days', $orientation->end_date);
            foreach ($period as $key => $date) {
                $input['date'] = $date;
                DB::transaction(function () use ($input) {
                    return $this->model->create($input);
                });
            }
        } elseif (!empty($orientation->start_date ?? null) && empty($orientation->end_date ?? null)) {
            $this->model->where('orientation_id', $orientation->id)->delete();
            $input['date'] = $orientation->start_date;
            DB::transaction(function () use ($input) {
                return $this->model->create($input);
            });
        } elseif (empty($orientation->start_date ?? null) && !empty($orientation->end_date ?? null)) {
            $this->model->where('orientation_id', $orientation->id)->delete();
            $input['date'] = $orientation->end_date;
            DB::transaction(function () use ($input) {
                return $this->model->create($input);
            });
        }
        return $orientation->orientationDates;
    }
}
