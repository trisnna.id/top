<?php

namespace App\Services;

use App\DataTables\ClubSociety\ClubSocietyDt;
use App\Models\ClubSociety;
use App\Services\BaseService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Admin\UserApproveNotification;

class ClubSocietyService extends BaseService
{
    protected $model;

    public function __construct()
    {
        $this->model = new ClubSociety();
    }

    public function index(Request $request, $auth)
    {
        $title = __('Club & Society');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('club-society.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $clubSocietyDt = new ClubSocietyDt($request, $auth);
        if ($request->get('table') == 'clubSocietyDt') {
            return $clubSocietyDt->render('');
        }
        return compact(
            'clubSocietyDt'
        );
    }

    // public function updateApproval($input, $auth, $uuid, $approval)
    // {
    //     $user = $this->model->findOrFailByUuid($uuid);
    //     if ($approval == 'approve') {
    //         $isActive = true;
    //         $emailVerifiedAt = Carbon::now();
    //         $message = 'The user have been approved';
    //         $token = app('auth.password.broker')->createToken($user);
    //         Notification::send($user, new UserApproveNotification($token));
    //     } else {
    //         $isActive = false;
    //         $message = 'The user have been rejected';
    //         $emailVerifiedAt = null;
    //     }
    //     $user->update([
    //         'approval' => $approval,
    //         'is_active' => $isActive,
    //         'email_verified_at' => $emailVerifiedAt,
    //     ]);
    //     return $this->responseSwal(
    //         true,
    //         ['type' => 'success', 'message' => $message]
    //     );
    // }

    // public function updateActive($input, $auth, $uuid)
    // {
    //     $user = $this->model->findOrFailByUuid($uuid);
    //     if ($user->is_active) {
    //         $user->is_active = false;
    //     } else {
    //         $user->is_active = true;
    //     }
    //     $user->save();
    //     return $this->responseSwal(
    //         true,
    //         ['type' => 'success', 'message' => 'Successfully Updated']
    //     );
    // }
}
