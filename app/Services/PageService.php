<?php

namespace App\Services;

use App\DataTables\Admin\Setting\Page\PageDt;
use App\Models\Page;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Page();
    }

    public function show(Request $request, $auth, $name)
    {
        $page = $this->model->where('name', $name)->firstOrFail();
        $title = __($page->title);
        return view('page.show', compact('title', 'page'));
    }
}
