<?php

namespace App\Services;

class ArrivalService extends ResourceService
{
    protected $titleIndex = 'Arrival';
    protected $titleShow = 'Arrival Detail';
    protected $viewIndex = 'arrival.index';
}
