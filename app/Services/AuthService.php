<?php declare(strict_types=1);

namespace App\Services;

use App\Models\Staff;
use App\Models\Student;
use App\Models\User;

final class AuthService
{
    /**
     * User model instance
     *
     * @var User|null
     */
    public ?User $user;

    /**
     * Create a new service instance
     */
    public function __construct()
    {
        $this->user = auth()->user();
    }
    
    /**
     * Get the current authenticated user with profile's
     *
     * @return User|null
     */
    public function getFullAttributes(): ?User
    {
        $user = $this->user;

        if ($this->isStudent()) {
            return $user->with('student')->find($this->id());
        }

        return $user->with('staff')->find($this->id());
    }

    public function user(): ?User
    {
        return auth()->user();
    }

    /**
     * Get user ID
     *
     * @return int
     */
    public function id(): int
    {
        return $this->user()->id;
    }

    /**
     * Get the user name or return the default value
     * if there's no user's name
     *
     * @param   string  $default
     * @return  string
     */
    public function name(string $default = ''): ?string
    {
        return $this->user()->name ?? $default;
    }

    /**
     * Get the staff detail of current user
     *
     * @param   array   $columns 
     * @return  Staff|null
     */
    public function staff(array $columns = ['*']): ?Staff
    {
        return Staff::select($columns)->where('user_id', $this->id())->first();
    }

    /**
     * Get the student detail of current user
     *
     * @param   array   $columns
     * @return  Student|null
     */
    public function student(array $columns = ['*']): ?Student
    {
        return Student::select($columns)->where('user_id', $this->id())->first();
    }

    /**
     * Check if the current has `admin` permission
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->user()->can('admin');
    }

    /**
     * Check if the current user is un-authenticated
     *
     * @return bool
     */
    public function isGuest(): bool
    {
        return (bool) $this->id() === false;
    }

    /**
     * Check if the current has `staffs` permission
     *
     * @return bool
     */
    public function isStaff(): bool
    {
        return !$this->isAdmin() && !$this->isStudent();
    }
    
    /**
     * Check if the current has `student` permission
     *
     * @return bool
     */
    public function isStudent(): bool
    {
        return $this->user()->can('student');
    }
}
