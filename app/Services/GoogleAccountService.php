<?php

namespace App\Services;

use App\Http\Requests\GoogleAccount\ShareEventRequest;
use App\Http\Requests\GoogleAccount\ShareEventsRequest;
use App\Models\GoogleAccount;
use App\Services\Integration\Google\CalendarService;
use App\Traits\Services\BaseServiceTrait;
use Carbon\Carbon;
use Google\Service\Calendar\Calendar;
use Illuminate\Support\Facades\DB;

class GoogleAccountService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new GoogleAccount();
    }

    public function shareEvent($input, $auth)
    {
        $this->validateRequest(new ShareEventRequest(), $input);
        if (!empty($input['token'] ?? null)) {
            $this->updateOrCreate($input, $auth);
        }
        $accessToken = $input['token']['access_token'] ?? ($auth->createdByGoogleAccount->access_token ?? null);
        $service = CalendarService::insertEvent($input['event'], $accessToken);
        $message = 'Orientation Activity Successfully Add to Google Calendar';
        return $this->swalResponse($this->swalData(
            message: $message,
        ));
    }

    public function shareEvents($input, $auth)
    {
        $this->validateRequest(new ShareEventsRequest(), $input);
        $events = collect($input['events'] ?? [])->filter(function ($instance) {
            return $instance['check'] == true;
        })->map(function ($instance) {
            unset($instance['check']);
            return $instance;
        });
        if (!empty($input['token'] ?? null)) {
            $this->updateOrCreate($input, $auth);
        }
        $accessToken = $input['token']['access_token'] ?? ($auth->createdByGoogleAccount->access_token ?? null);
        foreach ($events as $event) {
            CalendarService::insertEvent($event, $accessToken);
        }
        $message = 'Orientation Activity Successfully Add to Google Calendar';
        return $this->swalResponse($this->swalData(
            message: $message,
        ));
    }

    public function updateOrCreate($input, $auth)
    {
        $googleAccount = DB::transaction(function () use ($input, $auth) {
            $expiredAt = Carbon::now()->addSeconds($input['token']['expires_in']);
            $googleAccount = GoogleAccount::updateOrCreate(
                ['created_by' => $auth->id],
                ['access_token' => $input['token']['access_token'], 'expired_at' => $expiredAt],
            );
            return $googleAccount;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $googleAccount,
        ];
    }
}
