<?php

namespace App\Services\Admin;

use App\DataTables\Admin\Report\AuditTrailDt;
use App\DataTables\Admin\Report\StudentActivityDt;
use App\DataTables\Admin\Report\StudentAttendanceDt;
use App\DataTables\Admin\Report\StudentLoginDt;
use App\DataTables\Admin\Report\StudentRegistrationDt;
use App\DataTables\Admin\Report\StudentSurveyDt;
use App\Services\BaseService;
use Illuminate\Http\Request;

class ReportService extends BaseService
{

    public function indexStudentLogin(Request $request, $auth)
    {
        $title = __('Student Login Details');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.report.student-login.index', $compact);
    }

    public function indexStudentAttendance(Request $request, $auth)
    {
        $title = __('Student Attendance');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.report.student-attendance.index', $compact);
    }

    public function indexStudentRegistration(Request $request, $auth)
    {
        $title = __('Student Registration');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.report.student-registration.index', $compact);
    }

    public function indexAuditTrail(Request $request, $auth)
    {
        $title = __('Audit Trails');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.report.audit-trail.index', $compact);
    }

    public function indexStudentActivity(Request $request, $auth)
    {
        $title = __('Student Activity Log');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.report.student-activity.index', $compact);
    }

    public function indexStudentSurvey(Request $request, $auth)
    {
        $title = __('Student Survey Results');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.report.student-survey.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $studentLoginDt = new StudentLoginDt($request, $auth);
        $studentAttendanceDt = new StudentAttendanceDt($request, $auth);
        $studentRegistrationDt = new StudentRegistrationDt($request, $auth);
        $auditTrailDt = new AuditTrailDt($request, $auth);
        $studentActivityDt = new StudentActivityDt($request, $auth);
        $studentSurveyDt = new StudentSurveyDt($request, $auth);
        if ($request->get('table') == 'studentLoginDt') {
            return $studentLoginDt->render('');
        }elseif ($request->get('table') == 'studentAttendanceDt') {
            return $studentAttendanceDt->render('');
        }elseif ($request->get('table') == 'studentRegistrationDt') {
            return $studentRegistrationDt->render('');
        }elseif ($request->get('table') == 'auditTrailDt') {
            return $auditTrailDt->render('');
        }elseif ($request->get('table') == 'studentActivityDt') {
            return $studentActivityDt->render('');
        }elseif ($request->get('table') == 'studentSurveyDt') {
            return $studentSurveyDt->render('');
        }
        return compact(
            'studentLoginDt',
            'studentAttendanceDt',
            'auditTrailDt',
            'studentRegistrationDt',
            'studentActivityDt',
            'studentSurveyDt',
        );
    }
}
