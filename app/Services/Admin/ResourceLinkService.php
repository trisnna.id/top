<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\OrientationLink\StoreRequest;
use App\Models\ResourceLink;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class ResourceLinkService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new ResourceLink();
    }

    public function store($input, $auth = null)
    {
        $resourceLink = DB::transaction(function () use ($input) {
            $resourceLink = $this->model->create($input);
            return $resourceLink;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $resourceLink,
        ];
    }

    public function update($input, $auth, $uuid)
    {
        $resourceLink = $this->model->findOrFailByUuid($uuid);
        if (empty($input['name'] ?? null)) {
            $input['name'] = $input['url'];
        }
        $this->validateRequest(new StoreRequest(), $input);
        $resourceLink = DB::transaction(function () use ($input, $resourceLink) {
            $resourceLink->update($input);
            return $resourceLink;
        });
        return $this->responseUpdate();
    }

    public function delete($input, $auth, $uuid)
    {
        $resourceLink = $this->model->findOrFailByUuid($uuid);
        $resourceLink = DB::transaction(function () use ($resourceLink, $input) {
            $resourceLink->delete();
        });
        return $this->responseDestroy();
    }
}
