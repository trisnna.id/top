<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\Setting\LevelStudy\UpdateBulkActiveRequest;
use App\Models\LevelStudy;
use App\Models\StudentView;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class LevelStudyService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new LevelStudy();
    }

    public function syncCourseIntakeView($syncDate = null)
    {
        if (empty($syncDate ?? null)) {
            $syncDate = StudentView::orderBy('sync_date', 'desc')->first()->sync_date;
        }
        $studentViews = array_filter(StudentView::select('level_of_study')
            ->where('sync_date', $syncDate)
            ->groupBy('level_of_study')
            ->get()
            ->map(function ($studentView) {
                $studentView->level_of_study = trim($studentView->level_of_study);
                return $studentView;
            })
            ->pluck('level_of_study')
            ->toArray());
        $studentViews = DB::transaction(function () use ($studentViews) {
            foreach ($studentViews as $key => $value) {
                $this->model->updateOrCreate(
                    ['name' => $value],
                    ['is_cms_active' => true],
                );
            }
            $this->model->whereNotIn('name', $studentViews)
                ->update(['is_active' => false, 'is_cms_active' => false]);
            return $studentViews;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $studentViews,
        ];
    }

    public function store($input, $auth = null)
    {
        $levelStudy = DB::transaction(function () use ($input) {
            $levelStudy = $this->model->create($input);
            return $levelStudy;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $levelStudy,
        ];
    }

    public function updateActive($input, $auth, $uuid)
    {
        $levelStudy = $this->model->findOrFailByUuid($uuid);
        $levelStudy = $this->modelUpdateActive($levelStudy, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $levelStudies = $this->model->whereIn('uuid', $checkboxes)->where('is_cms_active', true);
        DB::transaction(function () use ($levelStudies, $active) {
            return $levelStudies->update([
                'is_active' => $active
            ]);
        });
        $count = $levelStudies->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }
}
