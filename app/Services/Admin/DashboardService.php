<?php

namespace App\Services\Admin;

use App\Constants\OrientationStatusConstant;
use App\Constants\OrientationTypeConstant;
use App\Constants\ResourceTypeConstant;
use App\Contracts\Entities\PreOrientationEntity;
use App\DataTables\Admin\Dashboard\AttendanceOrientationActivityDt;
use App\DataTables\Admin\Dashboard\ComplementaryDt;
use App\DataTables\Admin\Dashboard\CompulsoryCoreDt;
use App\DataTables\Admin\Dashboard\OrientationIntakeDt;
use App\DataTables\Admin\Dashboard\OrientationProgrammeDt;
use App\DataTables\Admin\Dashboard\OrientationTypeDt;
use App\DataTables\Admin\Dashboard\PreOrientationCompletionDt;
use App\DataTables\Admin\Dashboard\RsvpOrientationActivityDt;
use App\DataTables\Dashboard\FaqDt;
use App\DataTables\Dashboard\ResourceDt;
use App\DataTables\Orientation\AttachmentDt;
use App\Models\Faq;
use App\Models\Orientation;
use App\Models\OrientationAttendance;
use App\Models\OrientationFilterValue;
use App\Models\PreOrientation;
use App\Models\Programme;
use App\Models\Resource;
use App\Models\Student;
use App\Models\StudentPreOrientation;
use App\Models\StudentPreOrientationPercentage;
use App\Models\User;
use App\Services\BaseService;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardService extends BaseService
{
    use BaseServiceTrait;

    public function index(Request $request, $auth)
    {
        $user = $auth;
        $title = __('Dashboard');
        $instance['orientation'] = [
            'swal' => [
                'id' => 'orientation',
                'settings' => [
                    'title' => 'Details',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                ],
            ]
        ];
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        } elseif (!empty($request->get('count'))) {
            return $this->countIndex($request, $auth);
        }
        $instance['resource'] = [
            'swal' => [
                'id' => 'resource',
                'settings' => [
                    'title' => 'Resources',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                    'customClass' => [
                        'popup' => "w-90",
                    ],
                ],
            ]
        ];
        $instance['faq'] = [
            'swal' => [
                'id' => 'faq',
                'settings' => [
                    'title' => 'FAQ',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                    'customClass' => [
                        'popup' => "w-90",
                    ],
                ],
            ]
        ];
        $instance['stepbystep'] = [
            'swal' => [
                'id' => 'stepbystep',
                'settings' => [
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                    'customClass' => [
                        'popup' => "w-90",
                    ],
                ],
            ]
        ];
        $count['complementaryActivity'] = (new ComplementaryDt($request, $auth))->query()->count() ?? 0;
        $count['compulsoryCoreActivity'] = (new CompulsoryCoreDt($request, $auth))->query()->count() ?? 0;
        $charts['resources'] = [
            // 'series' => [
            //     Resource::whereIn('resource_type_id', [ResourceTypeConstant::ALL, ResourceTypeConstant::RESOURCE])->count(),
            //     Resource::whereIn('resource_type_id', [ResourceTypeConstant::ALL, ResourceTypeConstant::ARRIVAL])->count(),
            //     Faq::count()
            // ],
            // 'labels' => ['Resource', 'Arrival', 'FAQ'],
            'series' => [
                Resource::whereIn('resource_type_id', [ResourceTypeConstant::ALL, ResourceTypeConstant::ARRIVAL])->count(),
                Faq::count()
            ],
            'labels' => ['Arrival', 'FAQ'],
        ];
        $charts['typeOrientation'] = [
            'series' => [
                Orientation::where('orientation_type_id', OrientationTypeConstant::COMPULSORY_CORE)->count(),
                Orientation::where('orientation_type_id', OrientationTypeConstant::COMPLEMENTARY)->count()
            ],
            'labels' => ['Compulsory Core', 'Complementary'],
        ];
        $charts['intakeOrientation'] = [
            'series' => [
                20,
            ],
            'labels' => ['202103'],
        ];
        $programmeOrientationFilterValues = OrientationFilterValue::query()
            ->select([
                'value',
                DB::raw('COUNT(orientation_filter_values.value) as count')
            ])
            ->whereHas('orientation', function ($query) {
                $query->whereIn('orientations.status_id', [OrientationStatusConstant::APPROVED, OrientationStatusConstant::CANCELLED]);
            })
            ->where('orientation_filter_values.name', 'programme')
            ->groupBy('orientation_filter_values.value')
            ->orderBy('count', 'desc')
            ->limit(5)
            ->get();
        $charts['programmeOrientation'] = [
            'series' => $programmeOrientationFilterValues->pluck('count')->toArray(),
            'labels' => Programme::whereIn('id', $programmeOrientationFilterValues->pluck('value')->toArray())->get('name')->pluck('name')->toArray(),
        ];
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('user', 'title', 'instance', 'count', 'charts')
        );
        return view('admin.dashboard.index', $compact);
    }

    protected function countIndex(Request $request, $auth)
    {
        $filters = $request->filter;
        $data = [];
        if ($request->get('count') == 'activeStudent') {
            $data = $this->countActiveStudent($filters);
        } elseif ($request->get('count') == 'studentLogin') {
            $data = $this->countStudentLogin($filters);
        } elseif ($request->get('count') == 'attendanceOrientation') {
            $data = $this->countAttendanceOrientation($filters);
        } elseif ($request->get('count') == 'attendanceOrientationAcademic') {
            $data = $this->countAttendanceOrientationAcademic($filters);
        } elseif ($request->get('count') == 'preOrientationComplete') {
            $data = $this->countPreOrientationComplete($filters);
        }
        return $this->swalResponse($this->swalData(
            message: "Fetch Successfully",
            data: $data,
        ));
    }

    protected function countPreOrientationComplete($filters)
    {
        $preOInt = PreOrientation::where([
            [PreOrientationEntity::FIELD_STATUS, '=', 'published'],
            [PreOrientationEntity::FIELD_LOCALITIES, 'like', "%\"1\"%"]
        ])->count();
        $preOLoc = PreOrientation::where([
            [PreOrientationEntity::FIELD_STATUS, '=', 'published'],
            [PreOrientationEntity::FIELD_LOCALITIES, 'like', "%\"2\"%"]
        ])->count();
        $studentInt = Student::where([
            ['locality_id', '=', "1"]
        ])->count();
        $studentLoc = Student::where([
            ['locality_id', '=', "2"]
        ])->count();
        $compData = StudentPreOrientationPercentage::all()->count();
        $compInt = $preOInt * $studentInt;
        $compLoc = $preOLoc * $studentLoc;
        $compAll = $compInt + $compLoc;
        $count = $compAll !== 0 ? ($compData / $compAll * 100) : 0;
        return [
            'total' => number_format($count, 2),
        ];
    }

    protected function countAttendanceOrientationAcademic($filters)
    {
        $orientationIds = [];
        if (!empty($filters['intake'] ?? [])) {
            $orientationIds = OrientationFilterValue::where('name', 'intake')->whereIn('value', $filters['intake'])
                ->get()
                ->pluck('orientation_id')
                ->toArray();
        }
        return [
            'total' => $attendanceOrientationTotal = Orientation::where('status_id', OrientationStatusConstant::APPROVED)
                ->where('is_academic', true)
                ->when(!empty($filters['intake'] ?? []), function ($query) use ($orientationIds) {
                    $query->whereIn('id', $orientationIds);
                })
                ->sum('total_student'),
            'attend' => $attendanceOrientationAttend = OrientationAttendance::whereHas('orientation', function ($query) {
                $query->where('status_id', OrientationStatusConstant::APPROVED)->where('is_academic', true);
            })
                ->when(!empty($filters['intake'] ?? []), function ($query) use ($orientationIds) {
                    $query->whereIn('orientation_id', $orientationIds);
                })
                ->count(),
            'percentage' => $attendanceOrientationTotal != 0 ? number_format($attendanceOrientationAttend / $attendanceOrientationTotal * 100) : 0
        ];
    }

    protected function countAttendanceOrientation($filters)
    {
        $orientationIds = [];
        if (!empty($filters['intake'] ?? [])) {
            $orientationIds = OrientationFilterValue::where('name', 'intake')->whereIn('value', $filters['intake'])
                ->get()
                ->pluck('orientation_id')
                ->toArray();
        }
        return [
            'total' => $attendanceOrientationTotal = Orientation::where('status_id', OrientationStatusConstant::APPROVED)
                ->when(!empty($filters['intake'] ?? []), function ($query) use ($orientationIds) {
                    $query->whereIn('id', $orientationIds);
                })
                ->sum('total_student'),
            'attend' => $attendanceOrientationAttend = OrientationAttendance::whereHas('orientation', function ($query) {
                $query->where('status_id', OrientationStatusConstant::APPROVED);
            })
                ->when(!empty($filters['intake'] ?? []), function ($query) use ($orientationIds) {
                    $query->whereIn('orientation_id', $orientationIds);
                })
                ->count(),
            'percentage' => $attendanceOrientationTotal != 0 ? number_format($attendanceOrientationAttend / $attendanceOrientationTotal * 100) : 0
        ];
    }

    protected function countStudentLogin($filters)
    {
        return [
            'total' => User::where('model_type', Student::class)
                ->whereNotNull('last_login')
                ->when(!empty($filters['intake'] ?? []), function ($query) use ($filters) {
                    $query->whereHas('student', function ($query) use ($filters) {
                        $query->whereIn('intake_id', $filters['intake']);
                    });
                })
                ->count(),
        ];
    }

    protected function countActiveStudent($filters)
    {
        return [
            'total' => Student::where('course_status', 'Active')
                ->when(!empty($filters['intake'] ?? []), function ($query) use ($filters) {
                    $query->whereIn('intake_id', $filters['intake']);
                })
                ->count(),
            'local' => Student::where([
                'course_status' => 'Active',
                'locality' => 'Local',
            ])
                ->when(!empty($filters['intake'] ?? []), function ($query) use ($filters) {
                    $query->whereIn('intake_id', $filters['intake']);
                })
                ->count(),
            'international' => Student::where([
                'course_status' => 'Active',
                'locality' => 'International',
            ])
                ->when(!empty($filters['intake'] ?? []), function ($query) use ($filters) {
                    $query->whereIn('intake_id', $filters['intake']);
                })
                ->count(),
        ];
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        if (!empty($request->orientation_id ?? null)) {
            $orientation = Orientation::findOrFailByUuid($request->orientation_id);
        } else {
            $orientation = new Orientation();
        }
        $faqDt = new FaqDt($request, $auth);
        $resourceDt = new ResourceDt($request, $auth);
        $complementaryDt = new ComplementaryDt($request, $auth);
        $compulsoryCoreDt = new CompulsoryCoreDt($request, $auth);
        $attendanceOrientationActivityDt = new AttendanceOrientationActivityDt($request, $auth);
        $rsvpOrientationActivityDt = new RsvpOrientationActivityDt($request, $auth);
        $preOrientationCompletionDt = new PreOrientationCompletionDt($request, $auth);
        $orientationTypeDt = new OrientationTypeDt($request, $auth);
        $orientationIntakeDt = new OrientationIntakeDt($request, $auth);
        $orientationProgrammeDt = new OrientationProgrammeDt($request, $auth);
        $attachmentDt = new AttachmentDt($request, $auth, $orientation);
        if ($request->get('table') == 'faqDt') {
            return $faqDt->render('');
        } elseif ($request->get('table') == 'resourceDt') {
            return $resourceDt->render('');
        } elseif ($request->get('table') == 'complementaryDt') {
            return $complementaryDt->render('');
        } elseif ($request->get('table') == 'compulsoryCoreDt') {
            return $compulsoryCoreDt->render('');
        } elseif ($request->get('table') == 'attendanceOrientationActivityDt') {
            return $attendanceOrientationActivityDt->render('');
        } elseif ($request->get('table') == 'rsvpOrientationActivityDt') {
            return $rsvpOrientationActivityDt->render('');
        } elseif ($request->get('table') == 'preOrientationCompletionDt') {
            return $preOrientationCompletionDt->render('');
        } elseif ($request->get('table') == 'orientationTypeDt') {
            return $orientationTypeDt->render('');
        } elseif ($request->get('table') == 'orientationIntakeDt') {
            return $orientationIntakeDt->render('');
        } elseif ($request->get('table') == 'orientationProgrammeDt') {
            return $orientationProgrammeDt->render('');
        } elseif ($request->get('table') == 'attachmentDt') {
            return $attachmentDt->render('');
        }
        return compact(
            'faqDt',
            'resourceDt',
            'complementaryDt',
            'compulsoryCoreDt',
            'attendanceOrientationActivityDt',
            'rsvpOrientationActivityDt',
            'preOrientationCompletionDt',
            'attachmentDt',
            'orientationTypeDt',
            'orientationIntakeDt',
            'orientationProgrammeDt',
        );
    }
}
