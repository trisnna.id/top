<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\OrientationLink\StoreRequest;
use App\Models\OrientationLink;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class OrientationLinkService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new OrientationLink();
    }

    public function store($input, $auth = null)
    {
        $orientationLink = DB::transaction(function () use ($input) {
            $orientationLink = $this->model->create($input);
            return $orientationLink;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $orientationLink,
        ];
    }

    public function update($input, $auth, $uuid)
    {
        $orientationLink = $this->model->findOrFailByUuid($uuid);
        if (empty($input['name'] ?? null)) {
            $input['name'] = $input['url'];
        }
        $this->validateRequest(new StoreRequest(), $input);
        $orientationLink = DB::transaction(function () use ($input, $orientationLink) {
            $orientationLink->update($input);
            return $orientationLink;
        });
        return $this->responseUpdate();
    }

    public function delete($input, $auth, $uuid)
    {
        $orientationLink = $this->model->findOrFailByUuid($uuid);
        $orientationLink = DB::transaction(function () use ($orientationLink, $input) {
            $orientationLink->delete();
        });
        return $this->responseDestroy();
    }
}
