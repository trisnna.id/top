<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\OrientationRsvp\StoreRequest;
use App\Models\OrientationRsvp;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class OrientationRsvpService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new OrientationRsvp();
    }

    public function updateOrCreate($input, $auth)
    {
        $this->validateRequest(new StoreRequest(), $input);
        if ((bool) $input['is_go']) {
            foreach (json_decode(config('top.orientation.rsvp_groups')) as $orientationIds) {
                if (in_array($input['orientation_id'], $orientationIds)) {
                    foreach ($orientationIds as $orientationId) {
                        $orientationRsvp = DB::transaction(function () use ($input, $orientationId) {
                            $orientationRsvp = OrientationRsvp::updateOrCreate(
                                ['orientation_id' => $orientationId, 'student_id' => $input['student_id']],
                                ['is_go' => false],
                            );
                            return $orientationRsvp;
                        });
                    }
                }
            }
        }
        $orientationRsvp = DB::transaction(function () use ($input) {
            $orientationRsvp = OrientationRsvp::updateOrCreate(
                ['orientation_id' => $input['orientation_id'], 'student_id' => $input['student_id']],
                ['is_go' => $input['is_go']],
            );
            return $orientationRsvp;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $orientationRsvp,
        ];
    }
}
