<?php

namespace App\Services\Admin\Setting;

use anlutro\LaravelSettings\Facades\Setting;
use App\DataTables\Admin\Setting\Orientation\IntakeDt;
use App\Http\Requests\Admin\Setting\Orientation\StoreRequest;
use App\Models\Orientation;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;

class OrientationService
{
    use BaseServiceTrait;

    public function index(Request $request, $auth)
    {
        $title = __('Orientation');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.orientation.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $intakeDt = new IntakeDt($request, $auth);
        if ($request->get('table') == 'intakeDt') {
            return $intakeDt->render('');
        }
        return compact(
            'intakeDt',
        );
    }

    public function store($input, $auth)
    {
        $this->validateRequest(new StoreRequest(), $input);
        setting(['orientation' => $input['orientation']])->save();
        return $this->responseUpdate();
    }
}
