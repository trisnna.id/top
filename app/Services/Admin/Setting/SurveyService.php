<?php

namespace App\Services\Admin\Setting;

use App\Constants\SettingConstant;
use App\Constants\SurveyMigrationTypeConstant;
use App\DataTables\Admin\Setting\Page\PageDt;
use App\DataTables\Admin\Survey\SurveyMigrationDt;
use App\Http\Requests\Admin\Setting\Survey\UploadRequest;
use App\Imports\Admin\Setting\SurveyImport;
use App\Models\Page;
use App\Models\Student;
use App\Models\SurveyMigration;
use App\Services\Admin\StudentPointService;
use App\Traits\Services\BaseServiceTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class SurveyService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        // $this->model = new Page();
    }

    public function index(Request $request, $auth)
    {
        $title = __('Survey');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.survey.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $surveyMigrationDt = new SurveyMigrationDt($request, $auth);
        if ($request->get('table') == 'surveyMigrationDt') {
            return $surveyMigrationDt->render('');
        }
        return compact(
            'surveyMigrationDt',
        );
    }

    public function store($input, $auth)
    {
        $inputSurvey = SettingConstant::SURVEY;
        $inputSurvey = array_merge($inputSurvey, $input['survey']);
        setting(['survey' => $inputSurvey])->save();
        return $this->responseUpdate();
    }

    // TO DO: optimize
    public function upload($input, $auth)
    {
        $this->validateRequest(new UploadRequest(), $input);
        $surveyMigration = DB::transaction(function () use ($input) {
            $surveyMigration = SurveyMigration::create([
                'expired_at' => Carbon::now()->addMinutes(10),
                'type_id' => $input['type_id'],
            ]);
            return $surveyMigration;
        });
        if (!empty($surveyMigration ?? null)) {
            $fileSurveyMigration = $surveyMigration->addMedia($input['file'])->toMediaCollection('migration');
            $excels = Excel::toCollection(new SurveyImport(), $fileSurveyMigration->getPath());
            foreach ($excels as $excel) {
                $input['student_ids'] = $excel->pluck($input['field_map_student_id'])->toArray();
            }
            $surveyMigration->update([
                'file_name' => $fileSurveyMigration->file_name,
                'total_record' => count($input['student_ids'])
            ]);
            $students = Student::whereIn('id_number', $input['student_ids'])
                ->get();
            foreach ($students as $student) {
                if ($input['type_id'] == SurveyMigrationTypeConstant::PRE_ORIENTATION) {
                    $student->update([
                        'is_pre_orientation_survey_complete' => true
                    ]);
                    (new StudentPointService())->updateStudentPoint($student);
                } elseif ($input['type_id'] == SurveyMigrationTypeConstant::POST_ORIENTATION) {
                    $student->update([
                        'is_post_orientation_survey_complete' => true
                    ]);
                    (new StudentPointService())->updateStudentPoint($student);
                }
            }
            $surveyMigration->update([
                'is_complete' => true,
                'expired_at' => null,
            ]);
        }
        return $this->responseUpdate();
    }
}
