<?php

namespace App\Services\Admin\Setting;

use App\DataTables\Admin\Setting\Programme\ProgrammeDt;
use App\Models\Programme;
use Illuminate\Http\Request;
use App\Services\Admin\ProgrammeService as AdminProgrammeService;

class ProgrammeService extends AdminProgrammeService
{

    public function index(Request $request, $auth)
    {
        $title = __('Programme');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.programme.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $programmeDt = new ProgrammeDt($request, $auth);
        if ($request->get('table') == 'programmeDt') {
            return $programmeDt->render('');
        }
        return compact(
            'programmeDt',
        );
    }
}
