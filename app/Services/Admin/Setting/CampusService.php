<?php

namespace App\Services\Admin\Setting;

use App\DataTables\Admin\Setting\Campus\CampusDt;
use App\Models\Campus;
use Illuminate\Http\Request;
use App\Services\Admin\CampusService as AdminCampusService;

class CampusService extends AdminCampusService
{

    public function index(Request $request, $auth)
    {
        $title = __('Campus');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.campus.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $campusDt = new CampusDt($request, $auth);
        if ($request->get('table') == 'campusDt') {
            return $campusDt->render('');
        }
        return compact(
            'campusDt',
        );
    }
}
