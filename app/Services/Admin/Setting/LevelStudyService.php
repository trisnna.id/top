<?php

namespace App\Services\Admin\Setting;

use App\DataTables\Admin\Setting\LevelStudy\LevelStudyDt;
use App\Models\LevelStudy;
use Illuminate\Http\Request;
use App\Services\Admin\LevelStudyService as AdminLevelStudyService;

class LevelStudyService extends AdminLevelStudyService
{

    public function index(Request $request, $auth)
    {
        $title = __('LevelStudy');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.level-study.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $levelStudyDt = new LevelStudyDt($request, $auth);
        if ($request->get('table') == 'levelStudyDt') {
            return $levelStudyDt->render('');
        }
        return compact(
            'levelStudyDt',
        );
    }
}
