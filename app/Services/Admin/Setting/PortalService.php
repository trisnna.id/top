<?php

namespace App\Services\Admin\Setting;

use anlutro\LaravelSettings\Facades\Setting;
use App\Models\Orientation;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;

class PortalService
{
    use BaseServiceTrait;

    public function index(Request $request, $auth)
    {
        $title = __('Orientation');
        return view('admin.setting.portal.index', compact('title'));
    }

    public function store($input, $auth)
    {
        setting(['portal' => $input['portal']])->save();
        return $this->responseUpdate();
    }
}
