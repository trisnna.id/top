<?php

namespace App\Services\Admin\Setting;

use App\DataTables\Admin\Setting\Mobility\MobilityDt;
use App\Models\Mobility;
use Illuminate\Http\Request;
use App\Services\Admin\MobilityService as AdminMobilityService;

class MobilityService extends AdminMobilityService
{

    public function index(Request $request, $auth)
    {
        $title = __('Mobility');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.mobility.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $mobilityDt = new MobilityDt($request, $auth);
        if ($request->get('table') == 'mobilityDt') {
            return $mobilityDt->render('');
        }
        return compact(
            'mobilityDt',
        );
    }
}
