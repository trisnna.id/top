<?php

namespace App\Services\Admin\Setting;

use App\DataTables\Admin\Setting\Page\PageDt;
use App\Models\Page;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Page();
    }

    public function index(Request $request, $auth)
    {
        $title = __('Mail');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.page.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $pageDt = new PageDt($request, $auth);
        if ($request->get('table') == 'pageDt') {
            return $pageDt->render('');
        }
        return compact(
            'pageDt',
        );
    }

    public function updateOrCreate($input, $auth = null)
    {
        $page = DB::transaction(function () use ($input) {
            $page = Page::updateOrCreate(
                ['name' => $input['name']],
                ['title' => $input['title'] ?? null, 'content' => $input['content'] ?? null],
            );
            return $page;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $page,
        ];
    }

    public function update($input, $auth, $uuid)
    {
        $page = $this->model->findOrFailByUuid($uuid);
        $page = DB::transaction(function () use ($input, $page, $auth) {
            $page->update([
                'title' => $input['title'],
                'content' => $input['content'],
            ]);
            return $page;
        });
        return $this->responseUpdate();
    }
}
