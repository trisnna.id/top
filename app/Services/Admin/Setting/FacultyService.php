<?php

namespace App\Services\Admin\Setting;

use App\DataTables\Admin\Setting\Faculty\FacultyDt;
use App\Models\Faculty;
use App\Services\Admin\FacultyService as AdminFacultyService;
use Illuminate\Http\Request;

class FacultyService extends AdminFacultyService
{

    public function index(Request $request, $auth)
    {
        $title = __('Faculty');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.faculty.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $facultyDt = new FacultyDt($request, $auth);
        if ($request->get('table') == 'facultyDt') {
            return $facultyDt->render('');
        }
        return compact(
            'facultyDt',
        );
    }
}
