<?php

namespace App\Services\Admin\Setting;

use App\DataTables\Admin\Setting\School\SchoolDt;
use App\Models\School;
use App\Services\Admin\SchoolService as AdminSchoolService;
use Illuminate\Http\Request;

class SchoolService extends AdminSchoolService
{
    public function index(Request $request, $auth)
    {
        $title = __('School');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.school.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $schoolDt = new SchoolDt($request, $auth);
        if ($request->get('table') == 'schoolDt') {
            return $schoolDt->render('');
        }
        return compact(
            'schoolDt',
        );
    }
}
