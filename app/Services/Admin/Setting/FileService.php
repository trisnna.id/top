<?php

namespace App\Services\Admin\Setting;

use anlutro\LaravelSettings\Facades\Setting;
use App\Constants\SettingConstant;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;

class FileService
{
    use BaseServiceTrait;

    public function index(Request $request, $auth)
    {
        $title = __('File Sizes and Types');
        return view('admin.setting.file.index', compact('title'));
    }

    public function store($input, $auth)
    {
        $inputFile = SettingConstant::FILE;
        $inputFile = array_merge($inputFile, $input['file']);
        setting(['file' => $inputFile])->save();
        return $this->responseUpdate();
    }
}
