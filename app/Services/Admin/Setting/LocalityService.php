<?php

namespace App\Services\Admin\Setting;

use App\DataTables\Admin\Setting\Locality\LocalityDt;
use App\Models\Locality;
use Illuminate\Http\Request;
use App\Services\Admin\LocalityService as AdminLocalityService;

class LocalityService extends AdminLocalityService
{

    public function index(Request $request, $auth)
    {
        $title = __('Locality');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.locality.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $localityDt = new LocalityDt($request, $auth);
        if ($request->get('table') == 'localityDt') {
            return $localityDt->render('');
        }
        return compact(
            'localityDt',
        );
    }
}
