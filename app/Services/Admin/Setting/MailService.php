<?php

namespace App\Services\Admin\Setting;

use App\DataTables\Admin\Setting\Mail\MailDt;
use App\Http\Requests\Admin\Setting\Mail\UpdateRequest;
use App\Mail\NotificationMail;
use App\Models\Mail;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class MailService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Mail();
    }

    public function index(Request $request, $auth)
    {
        $title = __('Mail');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.mail.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $mailDt = new MailDt($request, $auth);
        if ($request->get('table') == 'mailDt') {
            return $mailDt->render('');
        }
        return compact(
            'mailDt',
        );
    }

    public function updateOrCreate($input, $auth = null)
    {
        $mails = DB::transaction(function () use ($input) {
            $mails = Mail::updateOrCreate(
                ['name' => $input['name']],
                ['title' => $input['title'] ?? null, 'subject' => $input['subject'] ?? null, 'content' => $input['content'] ?? null, 'notification' => $input['notification'] ?? null],
            );
            return $mails;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $mails,
        ];
    }

    public function update($input, $auth, $uuid)
    {
        $mail = $this->model->findOrFailByUuid($uuid);
        $input['updated_by'] = $auth->id ?? null;
        $this->validateRequest(new UpdateRequest(), $input);
        $mail = DB::transaction(function () use ($input, $mail, $auth) {
            $mail->update([
                'title' => $input['title'],
                'subject' => $input['subject'],
                'content' => $input['content'],
                'updated_by' => $input['updated_by'],
            ]);
            return $mail;
        });
        $message = 'Update Successfully';
        return $this->swalResponse($this->swalData(
            message: $message,
            reload: true,
        ));
    }

    public function create(Request $request, $auth)
    {
        $title = 'Mails Management';
        return view('admin.setting.mail.create', compact('title'));
    }

    public function store($input, $auth)
    {
        $input['created_by'] = $auth->id;
        $input['name'] = 'custom';
        $this->validateRequest(new UpdateRequest(), $input);
        $mail = DB::transaction(function () use ($input) {
            $mail = $this->model->create($input);
            $mail->update([
                'name' => "{$mail->name}-{$mail->uuid}"
            ]);
            return $mail;
        });
        return $this->swalResponse($this->swalData(
            message: 'Create Successfully',
            redirect: route('admin.settings.mails.edit', [$mail->uuid]),
        ));
    }

    public function edit(Request $request, $auth, $uuid)
    {
        $title = 'Mails Management';
        $mail = $this->model->findOrFailByUuid($uuid);
        // if (!empty($request->get('table'))) {
        //     return $this->dataTableEdit($request, $group, $authUser);
        // }
        // $compact = array_merge(
        //     $this->dataTableEdit($request, $group, $authUser),
        //     compact('title', 'mail')
        // );
        $notificationMail = new NotificationMail($mail->subject, json_decode($mail->content));
        $instance['show'] = [
            'data' => [
                'body' => $notificationMail->render()
            ],
            'swal' => [
                'id' => "show",
                'setting' => [
                    'title' => 'Preview Content'
                ],
                'axios' => [
                    'url' => '#',
                ]
            ]
        ];
        return view('admin.setting.mail.edit', compact('title', 'mail', 'instance'));
    }

    public function fixDataIsDefault(): void
    {
        Mail::whereIn('name', [
            'orientation-approval',
            'orientation-approval-correction',
            'orientation-approve',
            'orientation-cancel',
            'orientation-correction',
            'orientation-student',
            'student-assign-orientation-leader',
        ])->update(['is_default' => true]);
    }
}
