<?php

namespace App\Services\Admin\Setting;

use App\DataTables\Admin\Setting\Intake\IntakeDt;
use App\Models\Intake;
use Illuminate\Http\Request;
use App\Services\Admin\IntakeService as AdminIntakeService;

class IntakeService extends AdminIntakeService
{

    public function index(Request $request, $auth)
    {
        $title = __('Intake');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.setting.intake.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $intakeDt = new IntakeDt($request, $auth);
        if ($request->get('table') == 'intakeDt') {
            return $intakeDt->render('');
        }
        return compact(
            'intakeDt',
        );
    }
}
