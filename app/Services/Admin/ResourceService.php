<?php

namespace App\Services\Admin;

use App\Models\Campus;
use App\Models\Intake;
use Spatie\Image\Image;
use App\Models\Resource;
use App\Models\Programme;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\BaseService;
use Spatie\Image\Manipulations;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use App\Constants\ResourceTypeConstant;
use Illuminate\Support\Facades\Storage;
use App\DataTables\Admin\Resource\FaqDt;
use App\Traits\Services\BaseServiceTrait;
use App\DataTables\Admin\Resource\ArrivalDt;
use App\DataTables\Admin\Resource\ResourceDt;
use Symfony\Component\HttpFoundation\File\File;
use App\Http\Requests\Admin\Resource\StoreRequest;
use App\Http\Requests\Admin\Resource\StoreThumbnailRequest;
use App\Http\Requests\Admin\Resource\StoreAttachmentRequest;
use App\Services\ResourceService as ServicesResourceService;
use App\Http\Requests\Admin\Resource\UpdateBulkActiveRequest;
use App\DataTables\Admin\Resource\Edit\AttachmentDt as EditAttachmentDt;
use Illuminate\Support\Arr;

class ResourceService extends BaseService
{
    use BaseServiceTrait;

    protected $model;
    protected $titleIndex = 'FAQ';
    protected $titleCreate = 'Create Resources';
    protected $titleEdit = 'Resources Detail';
    protected $viewIndex = 'admin.resource.index';
    protected $routeIndex = 'admin.resources.index';
    protected $routeShow = 'admin.resources.show';
    protected $routeEdit = 'admin.resources.edit';
    protected $routeStore = 'admin.resources.store';

    public function __construct()
    {
        $this->model = new Resource();
    }

    public function index(Request $request, $auth)
    {
        $title = __($this->titleIndex);
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $options = $this->getOptions();
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title', 'options')
        );
        return view($this->viewIndex, $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $resourceDt = new ResourceDt($request, $auth);
        $arrivalDt = new ArrivalDt($request, $auth);
        $faqDt = new FaqDt($request, $auth);
        switch ($request->get('table')) {
            case 'resourceDt':
                return $resourceDt->render('');
            case 'arrivalDt':
                return $arrivalDt->render('');
            case 'faqDt':
                return $faqDt->render('');
            default:
                return compact('resourceDt', 'faqDt', 'arrivalDt');
        }
    }

    public function show(Request $request, $auth, $uuid)
    {
        $title = __($this->titleEdit);
        $view = (new ServicesResourceService())->show($request, $auth, $uuid);
        $viewData = $view->getData();
        $resource = $viewData['resource'];
        $routeIndex = route($this->routeIndex);
        $routeEdit = route($this->routeEdit, $resource->uuid);
        $titleIndex = $this->titleIndex;
        $compact = array_merge(
            $viewData,
            compact('title', 'routeIndex', 'titleIndex', 'routeEdit')
        );
        return view('admin.resource.show', $compact);
    }

    public function create(Request $request, $auth)
    {
        $title = __($this->titleCreate);
        $instance['upsert'] = [
            'swal' => [
                'id' => 'upsert',
                'axios' => [
                    'method' => 'post',
                    'url' => route($this->routeStore),
                ],
            ]
        ];
        $routeIndex = route($this->routeIndex);
        $titleIndex = $this->titleIndex;
        $options = $this->getOptions();
        return view('admin.resource.create', compact('title', 'instance', 'options', 'routeIndex', 'titleIndex'));
    }

    public function getOptions($includes = [])
    {
        $intakes = Intake::orderBy('name', 'desc')->get(['id', 'name', 'is_active']);
        $studentIntakeActive = $intakes->filter(function ($intake) use ($includes) {
            if (!empty($includes['intake'] ?? null)) {
                return $intake->is_active == true || in_array($intake->id, $includes['intake']);
            } else {
                return $intake->is_active == true;
            }
        })->pluck('name', 'id');
        $programmes = Programme::orderBy('name')->get(['id', 'name', 'is_active']);
        $studentProgrammeActive = $programmes->filter(function ($programme) use ($includes) {
            if (!empty($includes['programme'] ?? null)) {
                return $programme->is_active == true || in_array($programme->id, $includes['programme']);
            } else {
                return $programme->is_active == true;
            }
        })->pluck('name', 'id');
        $campuses = Campus::orderBy('name')->get(['id', 'name', 'is_active']);
        $studentCampusActive = $campuses->filter(function ($campus) use ($includes) {
            if (!empty($includes['campus'] ?? null)) {
                return $campus->is_active == true || in_array($campus->id, $includes['campus']);
            } else {
                return $campus->is_active == true;
            }
        })->pluck('name', 'id');
        $routeOptionEditable = in_array(request()->route()->getName(), ['admin.resources.index']);
        return [
            'resource_type' => ResourceTypeConstant::getAvailableOptions('title'),
            'student_intake' => $studentIntake = $intakes->pluck('name', 'id'),
            'student_intake_active' => $studentIntakeActive,
            'student_intake_inactive' => $intakes->where('is_active', 0)->pluck('name', 'id'),
            'student_intake_editable' => $routeOptionEditable ? $studentIntakeActive : $studentIntake,
            'student_programme' => $studentProgramme = $programmes->pluck('name', 'id'),
            'student_programme_active' =>  $studentProgrammeActive,
            'student_programme_inactive' => $programmes->where('is_active', 0)->pluck('name', 'id'),
            'student_programme_editable' => $routeOptionEditable ? $studentProgrammeActive : $studentProgramme,
            'student_campus' => $studentCampus = $campuses->pluck('name', 'id'),
            'student_campus_active' =>  $studentCampusActive,
            'student_campus_inactive' => $campuses->where('is_active', 0)->pluck('name', 'id'),
            'student_campus_editable' => $routeOptionEditable ? $studentCampusActive : $studentCampus,
        ];
    }

    public function store($input, $auth)
    {
        $input['is_active'] = !empty($input['is_active'] ?? null) ? true : false;
        $input['created_by'] = $auth->id;
        $input['resource_type_id'] = ResourceTypeConstant::ARRIVAL;
        $thumbnail_hex = $thumbnail_hex = Arr::get($input, 'thumbnail_hex');

        $this->validateRequest(new StoreRequest(), $input);
        if (!empty($input['attachment_files'] ?? null)) {
            $this->validateRequest(new StoreAttachmentRequest(), $input);
        }
        if ((empty($input['logo_remove'] ?? null) && empty($thumbnail_hex ?? null)) || (!empty($thumbnail_hex ?? null))) {
            $this->validateRequest(new StoreThumbnailRequest(), $input);
        }
        $resource = DB::transaction(function () use ($input) {
            $input['description'] = "";
            $resource = $this->model->create($input);
            return $resource;
        });

        if ($thumbnail_hex) {
            // decode the base64 file
            $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $thumbnail_hex));

            // save it to temporary dir first.
            $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
            file_put_contents($tmpFilePath, $fileData);

            // this just to help us get file info.
            $tmpFile = new File($tmpFilePath);

            $thumbnail = new UploadedFile(
                $tmpFile->getPathname(),
                $tmpFile->getFilename(),
                $tmpFile->getMimeType(),
                0,
                true // Mark it as test, since the file isn't from real HTTP POST.
            );
        }

        if (!empty($resource ?? null)) {
            $this->storeTextMedia($resource, $input);
            $this->storeAttachments($resource, $input);

            if (isset($thumbnail)) {
                $this->storeThumbnail($resource, $input, $thumbnail);
            }
        }
        $message = 'Create Successfully';
        return [
            'response' => $this->swalResponse($this->swalData(
                message: $message,
                redirect: route($this->routeEdit, $resource->uuid),
            )),
            'data' => $resource
        ];
    }

    public function edit(Request $request, $auth, $uuid)
    {
        $title = __($this->titleEdit);
        $resource = $this->model->findOrFailByUuid($uuid);
        if (!empty($request->get('table'))) {
            return $this->dataTableEdit($request, $auth, $resource);
        }
        $instance['upsert'] = [
            'swal' => [
                'id' => 'upsert',
                'axios' => [
                    'method' => 'put',
                    'url' => route('admin.resources.update', $resource->uuid),
                ],
            ]
        ];
        $options = $this->getOptions();
        $routeShow = route($this->routeShow, $resource->uuid);
        $routeIndex = route($this->routeIndex);
        $titleIndex = $this->titleIndex;
        $compact = array_merge(
            $this->dataTableEdit($request, $auth, $resource),
            compact('title', 'instance', 'resource', 'options', 'routeIndex', 'routeShow', 'titleIndex')
        );
        return view('admin.resource.create', $compact);
    }

    protected function dataTableEdit(Request $request, $auth, $resource)
    {
        $attachmentDt = new EditAttachmentDt($request, $auth, $resource);
        if ($request->get('table') == 'attachmentDt') {
            return $attachmentDt->render('');
        }
        return compact('attachmentDt');
    }

    public function update($input, $auth, $uuid)
    {
        $resource = $this->model->findOrFailByUuid($uuid);
        $input['is_active'] = !empty($input['is_active'] ?? null) ? true : false;
        $input['updated_by'] = $auth->id;
        $input['resource_type_id'] = ResourceTypeConstant::ARRIVAL;
        $thumbnail_hex = Arr::get($input, 'thumbnail_hex');

        $this->validateRequest(new StoreRequest(), $input);
        if (!empty($input['attachment_files'] ?? null)) {
            $this->validateRequest(new StoreAttachmentRequest(), $input);
        }
        if ((empty($input['logo_remove'] ?? null) && empty($thumbnail_hex ?? null)) || (!empty($thumbnail_hex ?? null))) {
            $this->validateRequest(new StoreThumbnailRequest(), $input);
        }
        $resource = DB::transaction(function () use ($resource, $input) {
            $input['description'] = null;
            $resource->update($input);
            return $resource;
        });

        if ($thumbnail_hex) {
            // decode the base64 file
            $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $thumbnail_hex));

            // save it to temporary dir first.
            $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
            file_put_contents($tmpFilePath, $fileData);

            // this just to help us get file info.
            $tmpFile = new File($tmpFilePath);

            $thumbnail = new UploadedFile(
                $tmpFile->getPathname(),
                $tmpFile->getFilename(),
                $tmpFile->getMimeType(),
                0,
                true // Mark it as test, since the file isn't from real HTTP POST.
            );
        }

        if (!empty($resource ?? null)) {
            $this->storeTextMedia($resource, $input);
            $this->storeAttachments($resource, $input);

            if (isset($thumbnail)) {
                $this->storeThumbnail($resource, $input, $thumbnail);
            }
        }
        $message = 'Update Successfully';
        return $this->swalResponse($this->swalData(
            message: $message,
            function: "window.modules.resource.admin.create.swalUpsertSuccess('form#upsert')",
        ));
    }

    public function storeTextMedia(Resource $resource, $input)
    {
        $description = $this->storeDescription($resource, $input);
        $input['description'] = !empty($description ?? null) ? $description->getFullUrl() : null;
        return DB::transaction(function () use ($input, $resource) {
            $resource->update($input);
            return $resource;
        });
    }
    
    protected function storeThumbnail($orientation, $input, $thumbnail)
    {
        if (!empty($input['logo_remove'] ?? null)) {
            $thumbnail = $orientation->getMedia('thumbnail');
            if (!empty($thumbnail ?? null)) {
                $thumbnail->first()->delete();
            }
        } else {
            $media = $orientation->addMedia($thumbnail)->toMediaCollection('thumbnail');
            $media->file_name = $media->id . '.png';
            $media->save();
        }
    }

    protected function storeDescription($resource, $input)
    {
        $media = null;
        if (!empty($input['description'])) {
            $name = Str::slug("description-{$resource->uuid}") . '.txt';
            $disk = Storage::disk('public');
            if ($disk->put($name, $input['description'])) {
                $filename = Str::slug('description');
                $resource->addMedia($disk->path($name))->usingName($filename)->usingFileName($filename . '.txt')->toMediaCollection('description');
            }
            $media = $resource->getFirstMedia('description');
        }
        return $media;
    }

    protected function storeAttachments($resource, $input)
    {
        if (!empty($input['attachment_files'] ?? null)) {
            foreach ($input['attachment_files'] as $attachmentFile) {
                if (extensionTypeMedia($attachmentFile->clientExtension()) == "video") {
                    $resource->addMedia($attachmentFile)->toMediaCollection('media');
                } else {
                    $resource->addMedia($attachmentFile)->toMediaCollection('documents');
                }
            }
        }
        if (!empty($input['attachment_links'] ?? null)) {
            $attachmentLinks = trimArrayFilterExplode(',', $input['attachment_links']);
            foreach ($attachmentLinks as $attachmentLink) {
                $attachmentLink = toUrlMedia($attachmentLink);
                if (in_array(urlTypeMedia($attachmentLink), ['youtube', 'video'])) {
                    $collectionName = 'media';
                } else {
                    $collectionName = 'links';
                }
                (new ResourceLinkService())->store([
                    'collection_name' => $collectionName,
                    'resource_id' => $resource->id,
                    'name' => trim($attachmentLink),
                    'url' => trim($attachmentLink),
                ]);
            }
        }
    }

    public function updateActive($input, $auth, $uuid)
    {
        $resource = $this->model->findOrFailByUuid($uuid);
        $resource = $this->modelUpdateActive($resource, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function updateLink($input, $auth, $uuidResource, $uuidLink)
    {
        $resource = $this->model->findOrFailByUuid($uuidResource);
        (new ResourceLinkService())->update($input, $auth, $uuidLink);
        return $this->responseUpdate();
    }

    public function deleteLink($input, $auth, $uuidResource, $uuidLink)
    {
        $resource = $this->model->findOrFailByUuid($uuidResource);
        (new ResourceLinkService())->delete($input, $auth, $uuidLink);
        return $this->responseDestroy();
    }

    public function updateFile($input, $auth, $uuidResource, $uuidFile)
    {
        $resource = $this->model->findOrFailByUuid($uuidResource);
        $files = $resource->getMedia('documents', function ($instance) use ($uuidFile) {
            return $instance->uuid == $uuidFile;
        });
        if (empty($files->toArray() ?? null)) {
            $files = $resource->getMedia('media', function ($instance) use ($uuidFile) {
                return $instance->uuid == $uuidFile;
            });
        }
        $file = $files->first();
        $file->setCustomProperty('name', $input['name'] ?? $file->file_name);
        $file->setCustomProperty('description', $input['description'] ?? null);
        $file->save();
        return $this->responseUpdate();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $resources = $this->model->whereIn('uuid', $checkboxes);
        DB::transaction(function () use ($resources, $active) {
            return $resources->update([
                'is_active' => $active
            ]);
        });
        $count = $resources->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }

    public function deleteFile($input, $auth, $uuidResource, $uuidFile)
    {
        $resource = $this->model->findOrFailByUuid($uuidResource);
        $fileDocuments = $resource->getMedia('documents', function ($instance) use ($uuidFile) {
            return $instance->uuid == $uuidFile;
        });
        if (!empty($fileDocuments->toArray() ?? null)) {
            $fileDocuments->first()->delete();
        }
        $fileMedias = $resource->getMedia('media', function ($instance) use ($uuidFile) {
            return $instance->uuid == $uuidFile;
        });
        if (!empty($fileMedias->toArray() ?? null)) {
            $fileMedias->first()->delete() ?? null;
        }
        return $this->responseDestroy();
    }

    public function delete($input, $auth, $uuid)
    {
        $resource = $this->model->findOrFailByUuid($uuid);
        $resource->load(['resourceLinks']);
        if ($resource->rule_can_delete) {
            DB::transaction(function () use ($resource, $input) {
                foreach ($resource->resourceLinks as $resourceLink) {
                    $resourceLink->delete();
                }
                foreach ($resource->getMedia('documents') as $document) {
                    $document->delete();
                }
                foreach ($resource->getMedia('media') as $media) {
                    $media->delete();
                }
                $resource->delete();
            });
        } else {
            return $this->throwResponseException(['Cannot DELETE. This data is being used']);
        }
        return $this->responseDestroy();
    }

    public function clone(Request $request, $auth, $uuid)
    {
        $resource = $this->model->findOrFailByUuid($uuid);
        $input = [
            'title' => $resource->title,
            'description' => $resource->description,
            'resource_type_id' => $resource->resource_type_id,
            'is_active' => false,
        ];
        return DB::transaction(function () use ($input, $auth, $resource) {
            $resourceStore = $this->store($input, $auth);
            $resourceClone = $resourceStore['data'];
            foreach ($resource->getMedia('documents') as $media) {
                $resourceClone->addMediaFromUrl($media->getFullUrl())->toMediaCollection('documents');
            }
            foreach ($resource->getMedia('thumbnail') as $media) {
                $resourceClone->addMediaFromUrl($media->getFullUrl())->toMediaCollection('thumbnail');
            }
            foreach ($resource->resourceLinks->whereIn('collection_name', ['media', 'links']) as $attachmentLink) {
                (new ResourceLinkService())->store([
                    'collection_name' => $attachmentLink->collection_name,
                    'resource_id' => $resourceClone->id,
                    'name' => $attachmentLink->name,
                    'description' => $attachmentLink->description,
                    'url' => $attachmentLink->url,
                ]);
            }
            return $resourceStore;
        });
    }
}
