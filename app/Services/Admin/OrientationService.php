<?php

namespace App\Services\Admin;

use App\Models\User;
use App\Models\Campus;
use App\Models\Intake;
use App\Models\School;
use App\Models\Faculty;
use App\Models\Student;
use Spatie\Image\Image;
use App\Models\Mobility;
use App\Models\Programme;
use App\Models\LevelStudy;
use App\Models\Orientation;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Image\Manipulations;
use App\Models\OrientationFilter;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use App\Constants\ActionLogConstant;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\OrientationAttendance;
use App\Models\OrientationFilterValue;
use Illuminate\Support\Facades\Storage;
use App\Traits\Services\BaseServiceTrait;
use App\Constants\OrientationTypeConstant;
use App\Constants\OrientationStatusConstant;
use App\DataTables\Orientation\AttachmentDt;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\HttpFoundation\File\File;
use App\DataTables\Admin\Orientation\OrientationDt;
use App\DataTables\Admin\Orientation\Show\ActionDt;
use App\Exports\Admin\Orientation\OrientationExport;
use App\Http\Requests\Admin\Orientation\StoreRequest;
use App\Http\Requests\Admin\Orientation\SubmitRequest;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use App\DataTables\Admin\Orientation\ViewOrientationDt;
use App\DataTables\Admin\Orientation\Show\OrientationRsvpDt;
use App\Http\Requests\Admin\Orientation\StoreRecordingRequest;
use App\Http\Requests\Admin\Orientation\StoreThumbnailRequest;
use App\Http\Requests\Admin\Orientation\StoreAttachmentRequest;
use App\Http\Requests\Admin\Orientation\StoreAttendanceRequest;
use App\DataTables\Admin\Orientation\Show\OrientationRecordingDt;
use App\DataTables\Admin\Orientation\Show\OrientationAttendanceDt;
use App\Notifications\Orientation\OrientationApprovalNotification;
use App\DataTables\Admin\Orientation\Edit\AttachmentDt as EditAttachmentDt;
use App\DataTables\Admin\Orientation\Show\AttachmentDt as ShowAttachmentDt;
use App\Notifications\Orientation\OrientationApprovalCorrectionNotification;
use App\Services\OrientationDateService;
use Illuminate\Support\Arr;

class OrientationService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Orientation();
    }

    public function index(Request $request, $auth)
    {
        $title = __('Orientation Timetable');
        $instance['orientation'] = [
            'swal' => [
                'id' => 'orientation',
                'settings' => [
                    'title' => 'Details',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                ],
            ]
        ];
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        } elseif (!empty($request->get('calendar'))) {
            return $this->calendarIndex($request, $auth, $instance);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title', 'instance')
        );
        return view('admin.orientation.index', $compact);
    }

    public function index2(Request $request, $auth)
    {
        $title = __('Orientation Activity');
        $instance['orientation'] = [
            'swal' => [
                'id' => 'orientation',
                'settings' => [
                    'title' => 'Details',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                ],
            ]
        ];
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        } elseif (!empty($request->get('calendar'))) {
            return $this->calendarIndex($request, $auth, $instance);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title', 'instance')
        );
        return view('admin.orientation.index2', $compact);
    }

    protected function calendarIndex(Request $request, $auth, $instance)
    {
        $orientations = (new OrientationDt($request, $auth))->query()
            ->where('is_publish', true)
            ->get();
        if (!empty($request->action ?? null)) {
            $orientationExport = new OrientationExport($orientations);
            if ($request->action == 'excel') {
                return Excel::download($orientationExport, 'orientation.xlsx');
            } elseif ($request->action == 'pdf') {
                return $this->excelToPdf($orientationExport, 'orientation.pdf');
            }
        }
        $calendar = $orientations->map(function ($orientation) use ($instance) {
            $calendar['id'] = $orientation->uuid;
            $calendar['title'] = $orientation->name;
            $calendar['start'] = "{$orientation->start_date} {$orientation->start_time}";
            $calendar['end'] = "{$orientation->start_date} {$orientation->end_time}";
            $calendar['extendedProps'] = [
                'data-swal' => $instance['orientation']['swal'],
                'data-data' => [
                    'id' => $orientation->uuid,
                    'name' => $orientation->name,
                    'synopsis' => $orientation->synopsis_encode,
                    'livestream' => $orientation->livestream,
                    'presenter' => $orientation->presenter,
                    'agenda' => $orientation->agenda_encode,
                    'event' => $orientation->venue . '<br>' . $orientation->start_date . ", {$orientation->start_end_time_readable}",
                    'start_datetime_iso_8601' => $orientation->start_date_iso_8601,
                    'start_end_date_dFY' => $orientation->start_end_date_dFY,
                    'start_date_dFY' => $orientation->start_date_dFY,
                    'start_end_time_readable' => $orientation->start_end_time_readable,
                    'end_datetime_iso_8601' => $orientation->end_date_iso_8601,
                ]
            ];
            $calendar['className'] = OrientationTypeConstant::getAvailableOptions('css_calendar')[$orientation->orientation_type_id];
            return $calendar;
        });
        return $this->swalResponse($this->swalData(
            message: "Fetch Successfully",
            data: $calendar,
        ));
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        if (!empty($request->orientation_id ?? null)) {
            $orientation = $this->model->findOrFailByUuid($request->orientation_id);
        } else {
            $orientation = $this->model;
        }
        $orientationDt = new OrientationDt($request, $auth);
        $viewOrientationDt = new ViewOrientationDt($request, $auth);
        $attachmentDt = new AttachmentDt($request, $auth, $orientation);
        if ($request->get('table') == 'orientationDt') {
            return $orientationDt->render('');
        } elseif ($request->get('table') == 'attachmentDt') {
            return $attachmentDt->render('');
        } elseif ($request->get('table') == 'viewOrientationDt') {
            return $viewOrientationDt->render('');
        }
        return compact(
            'orientationDt',
            'attachmentDt',
            'viewOrientationDt',
        );
    }

    public function show(Request $request, $auth, $uuid)
    {
        $title = __('Orientation Timetable Detail');
        $orientation = $this->model->findOrFailByUuid($uuid);
        if (!empty($request->get('table'))) {
            return $this->dataTableShow($request, $auth, $orientation);
        }
        $options = $this->getOptions();
        $orientationOnly = $this->orientationOnly($orientation);
        $attendanceQrCode = [
            'orientation_id' => $orientation->uuid,
        ];
        $compact = array_merge(
            $this->dataTableShow($request, $auth, $orientation),
            compact('title', 'orientation', 'options', 'orientationOnly', 'attendanceQrCode')
        );
        return view('admin.orientation.show', $compact);
    }

    protected function dataTableShow(Request $request, $auth, Orientation $orientation)
    {
        $attachmentDt = new ShowAttachmentDt($request, $auth, $orientation);
        $actionDt = new ActionDt($request, $auth, $orientation);
        $orientationAttendanceDt = new OrientationAttendanceDt($request, $auth, $orientation);
        $orientationRecordingDt = new OrientationRecordingDt($request, $auth, $orientation);
        if ($request->get('table') == 'attachmentDt') {
            return $attachmentDt->render('');
        } elseif ($request->get('table') == 'actionDt') {
            return $actionDt->render('');
        } elseif ($request->get('table') == 'orientationAttendanceDt') {
            return $orientationAttendanceDt->render('');
        } elseif ($request->get('table') == 'orientationRecordingDt') {
            return $orientationRecordingDt->render('');
        }
        $compact = compact(
            'attachmentDt',
            'actionDt',
            'orientationAttendanceDt',
            'orientationRecordingDt',
        );
        if ($orientation->is_rsvp) {
            $orientationRsvpDt = new OrientationRsvpDt($request, $auth, $orientation);
            if ($request->get('table') == 'orientationRsvpDt') {
                return $orientationRsvpDt->render('');
            }
            $compact = array_merge(
                $compact,
                compact('orientationRsvpDt')
            );
        }
        return $compact;
    }

    public function create(Request $request, $auth)
    {
        $title = __('Create Orientation Timetable');
        $instance['upsert'] = [
            'el' => 'form#stepper-form',
            'axios' => [
                'url' => route('admin.orientations.store'),
                'method' => 'POST'
            ]
        ];
        $instance['upsert-no-next'] = [
            'el' => 'form#stepper-form',
            'axios' => [
                'url' => route('admin.orientations.store', ['no-next' => true]),
                'method' => 'POST'
            ]
        ];
        $options = $this->getOptions();
        return view('admin.orientation.create', compact('title', 'instance', 'options'));
    }

    public function clone(Request $request, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $filterAll = [];
        $filters = $orientation->filters;
        $rawFilters = $orientation->raw_filters;
        foreach ($filters as $key => $values) {
            if (count($values) == 1 && $values[0] == 'all') {
                $filterAll[$key] = 1;
                $filters[$key] = [];
            } else {
                $filterAll[$key] = 0;
            }
        }
        $input = [
            'step' => 1,
            'name' => $orientation->name,
            'venue' => $orientation->venue,
            'livestream' => $orientation->livestream,
            'start_date' => $orientation->start_date,
            'end_date' => $orientation->end_date,
            'start_time' => $orientation->start_time,
            'end_time' => $orientation->end_time,
            'presenter' => $orientation->presenter,
            'agenda' => $orientation->agenda,
            'synopsis' => $orientation->synopsis,
            'orientation_type_id' => $orientation->orientation_type_id,
            'is_academic' => $orientation->is_academic,
            'filter_all' => $filterAll,
            'filters' => $rawFilters->toArray(),
            'is_rsvp' => $orientation->is_rsvp,
            'rsvp_capacity' => $orientation->rsvp_capacity,
            'is_point' => $orientation->is_point,
            'point_max' => $orientation->point_max,
            'point_min' => $orientation->point_min,
            'point_attendance' => $orientation->point_attendance,
            'point_rating' => $orientation->point_rating,
            'total_student' => $orientation->total_student,
            'is_publish' => false,
        ];
        return DB::transaction(function () use ($input, $auth, $orientation) {
            $orientationStore = $this->store($input, $auth);
            $orientationClone = $orientationStore['data'];
            foreach ($orientation->getMedia('files') as $media) {
                $orientationClone->addMediaFromUrl($media->getFullUrl())->toMediaCollection('files');
            }
            foreach ($orientation->orientationLinks->where('collection_name', 'files') as $attachmentLink) {
                (new OrientationLinkService())->store([
                    'collection_name' => 'files',
                    'orientation_id' => $orientationClone->id,
                    'name' => $attachmentLink->name,
                    'url' => $attachmentLink->url,
                ]);
            }
            return $orientationStore;
        });
    }

    protected function storeTextToMedia($model, $input, $attribute)
    {
        $media = null;
        if (!empty($input[$attribute])) {
            $filename = Str::slug("{$attribute}-{$model->uuid}") . '.txt';
            $disk = Storage::disk('public');
            if ($disk->put($filename, $input[$attribute])) {
                $slugAttribute = Str::slug($attribute);
                $model->addMedia($disk->path($filename))->usingName($slugAttribute)->usingFileName($slugAttribute . '.txt')->toMediaCollection($attribute);
            }
            $media = $model->getFirstMedia($attribute);
        }
        return $media;
    }

    public function store($input, $auth)
    {
        $input['created_by'] = $auth->id;
        $input['status_id'] = OrientationStatusConstant::DRAFT;
        $input['status_name'] = getConstant('OrientationStatusConstant', 'title', 'DRAFT');
        $thumbnail_hex = Arr::get($input, 'thumbnail_hex');
        $this->defaultInput($input);
        $this->validateRequest(new StoreRequest(), $input);
        if (!empty($input['attachment_files'] ?? null)) {
            $this->validateRequest(new StoreAttachmentRequest(), $input);
        }
        if ((empty($input['logo_remove'] ?? null) && empty($thumbnail_hex ?? null)) || (!empty($thumbnail_hex ?? null))) {
            $this->validateRequest(new StoreThumbnailRequest(), $input);
        }
        $orientation = DB::transaction(function () use ($input) {
            $input['agenda'] = null;
            $input['synopsis'] = null;
            $orientation = $this->model->create($input);
            return $orientation;
        });

        if ($thumbnail_hex) {
            // decode the base64 file
            $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $thumbnail_hex));

            // save it to temporary dir first.
            $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
            file_put_contents($tmpFilePath, $fileData);

            // this just to help us get file info.
            $tmpFile = new File($tmpFilePath);

            $thumbnail = new UploadedFile(
                $tmpFile->getPathname(),
                $tmpFile->getFilename(),
                $tmpFile->getMimeType(),
                0,
                true // Mark it as test, since the file isn't from real HTTP POST.
            );
        }

        if (!empty($orientation ?? null)) {
            $this->storeTextMedia($orientation, $input);
            $this->storeAttachments($orientation, $input);
            $this->upsertFilter($orientation, $input);
            (new OrientationDateService)->syncDate($orientation);
            if (isset($thumbnail)) {
                $this->storeThumbnail($orientation, $input, $thumbnail);
            }
        }
        if (!empty($input['no-next'] ?? null) && (bool)$input['no-next']) {
            $step = $input['step'] ?? 1;
        } else {
            $step = !empty($input['step'] ?? null) ? $input['step'] + 1 : 2;
        }
        request()->session()->push('orientation.edit.step', $step);
        return [
            'response' => $this->swalResponse($this->swalData(
                message: 'Create Successfully in Draft',
                redirect: route('admin.orientations.edit', [$orientation->uuid]),
            )),
            'data' => $orientation
        ];
    }

    public function storeTextMedia(Orientation $orientation, $input)
    {
        $inputAgenda = $this->storeTextToMedia($orientation, $input, 'agenda');
        $input['agenda'] = !empty($inputAgenda ?? null) ? $inputAgenda->getFullUrl() : null;
        $inputSynopsis = $this->storeTextToMedia($orientation, $input, 'synopsis');
        $input['synopsis'] = !empty($inputSynopsis ?? null) ? $inputSynopsis->getFullUrl() : null;
        return DB::transaction(function () use ($input, $orientation) {
            $orientation->update($input);
            return $orientation;
        });
    }

    public function storeAttendance($input, $auth, $uuid)
    {
        $this->validateRequest(new StoreAttendanceRequest(), $input);
        $orientation = $this->model->findOrFailByUuid($uuid);
        $input['id_number'] = trimArrayFilterExplode(',', $input['id_number']);
        $orientationAttendanceSubQuery = OrientationAttendance::query()
            ->select([
                'orientation_attendances.orientation_id',
                'orientation_attendances.student_id',
                'orientation_attendances.rating',
            ])
            ->where('orientation_attendances.orientation_id', $orientation->id);
        $students = Student::select([
            'students.*',
            'orientation_attendances.rating as orientation_attendance_rating',
        ])
            ->leftJoinSub($orientationAttendanceSubQuery, 'orientation_attendances', function ($query) {
                $query->on('students.id', '=', 'orientation_attendances.student_id');
            })
            ->whereIn('id_number', $input['id_number'])
            ->get();
        $studentIdNumbers = $students->pluck('id_number')->toArray();
        $notValidStudentIdNumbers = array_diff($input['id_number'], $studentIdNumbers);
        if (count($notValidStudentIdNumbers)) {
            $stringNotValidStudentIdNumbers = implode(', ', $notValidStudentIdNumbers);
            return $this->throwResponseException(["Studenf ID {$stringNotValidStudentIdNumbers} not found"]);
        }
        DB::transaction(function () use ($students, $orientation, $auth) {
            foreach ($students as $student) {
                $inputAttendance['orientation_id'] = $orientation->id;
                $inputAttendance['student_id'] = $student->id;
                $inputAttendance['rating'] = $student->orientation_attendance_rating;
                (new OrientationAttendanceService())->updateOrCreate($inputAttendance, $auth);
            }
        });
        return $this->responseUpdate();
    }

    public function storeRecording($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        if (!empty($input['attachment_recording_files'] ?? null)) {
            $this->validateRequest(new StoreRecordingRequest(), $input);
        }
        if (!empty($input['attachment_recording_files'] ?? null)) {
            foreach ($input['attachment_recording_files'] as $attachmentRecordingFile) {
                $orientation->addMedia($attachmentRecordingFile)->toMediaCollection('recordings');
            }
        }
        if (!empty($input['attachment_recording_links'] ?? null)) {
            $attachmentRecordingLinks = trimArrayFilterExplode(',', $input['attachment_recording_links']);
            foreach ($attachmentRecordingLinks as $attachmentLink) {
                $attachmentLink = toUrlMedia($attachmentLink);
                (new OrientationLinkService())->store([
                    'orientation_id' => $orientation->id,
                    'collection_name' => 'recordings',
                    'name' => trim($attachmentLink),
                    'url' => trim($attachmentLink),
                ]);
            }
        }
        return $this->responseStore();
    }

    public function edit(Request $request, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        if (!empty($request->get('table'))) {
            return $this->dataTableEdit($request, $auth, $orientation);
        }
        $title = __('Edit Orientation');
        if ($orientation->rule_can_edit) {
            $orientation->load(['orientationFilters']);
            $instance['upsert'] = [
                'el' => 'form#stepper-form',
                'axios' => [
                    'url' => route('admin.orientations.update', $uuid),
                    'method' => 'PUT'
                ]
            ];
            $instance['upsert-no-next'] = [
                'el' => 'form#stepper-form',
                'axios' => [
                    'url' => route('admin.orientations.update', [$uuid, 'no-next' => true]),
                    'method' => 'PUT'
                ]
            ];
            if (auth()->user()->can(['manage_activity_approval'])) {
                $instance['submit'] = [
                    'el' => 'form#stepper-form',
                    'axios' => [
                        'url' => route('admin.orientations.update-approval', [$uuid, 'redirect' => route('admin.orientations.index'), 'direct_approval' => true]),
                        'method' => 'PUT'
                    ],
                    'button' => [
                        'title' => 'Save & Approved',
                    ],
                ];
            } else {
                $instance['submit'] = [
                    'el' => 'form#stepper-form',
                    'axios' => [
                        'url' => route('admin.orientations.update-approval', [$uuid, 'redirect' => route('admin.orientations.index')]),
                        'method' => 'PUT'
                    ],
                    'button' => [
                        'title' => 'Save & Submit',
                    ],
                ];
            }
            $options = $this->getOptions($this->getIncludes($orientation));
            $step = $request->session()->pull('orientation.edit.step')[0] ?? null;
            $orientationOnly = $this->orientationOnly($orientation);
            $compact = array_merge(
                $this->dataTableEdit($request, $auth, $orientation),
                compact('title', 'orientation', 'instance', 'options', 'step', 'orientationOnly')
            );
            return view('admin.orientation.create', $compact);
        } else {
            return abort(404);
        }
    }

    protected function orientationOnly($orientation)
    {
        return $orientation->only([
            'uuid',
            'name',
            'venue',
            'livestream',
            'start_date',
            'end_date',
            'start_time',
            'end_time',
            'presenter',
            'agenda',
            'synopsis',
            'orientation_type_id',
            'is_rsvp',
            'rsvp_capacity',
            'is_point',
            'point_max',
            'point_min',
            'filters',
            'total_student',
        ]);
    }

    protected function dataTableEdit(Request $request, $auth, Orientation $orientation)
    {
        $compact = [];
        $attachmentDt = new EditAttachmentDt($request, $auth, $orientation);
        if ($request->get('table') == 'attachmentDt') {
            return $attachmentDt->render('');
        }
        $compact = compact('attachmentDt');
        if ($orientation->status_id == OrientationStatusConstant::REQUIRE_CORRECTION) {
            $actionDt = new ActionDt($request, $auth, $orientation);
            if ($request->get('table') == 'actionDt') {
                return $actionDt->render('');
            }
            $compact = array_merge(
                $compact,
                compact('actionDt')
            );
        }
        return $compact;
    }

    protected function getIncludes($orientation)
    {
        return [
            'intake' => $orientation->orientationFilters->where('name', 'intake')->first()->value ?? [],
            'faculty' => $orientation->orientationFilters->where('name', 'faculty')->first()->value ?? [],
            'school' => $orientation->orientationFilters->where('name', 'school')->first()->value ?? [],
            'programme' => $orientation->orientationFilters->where('name', 'programme')->first()->value ?? [],
            'levelStudy' => $orientation->orientationFilters->where('name', 'levelStudy')->first()->value ?? [],
            'mobility' => $orientation->orientationFilters->where('name', 'mobility')->first()->value ?? [],
        ];
    }

    public function getOptions($includes = [])
    {
        $intakes = Intake::orderBy('name', 'desc')->get(['id', 'name', 'is_active']);
        $studentIntakeActive = $intakes->filter(function ($intake) use ($includes) {
            if (!empty($includes['intake'] ?? null)) {
                return $intake->is_active == true || in_array($intake->id, $includes['intake']);
            } else {
                return $intake->is_active == true;
            }
        })->pluck('name', 'id');
        $faculties = Faculty::orderBy('name')->get(['id', 'name', 'is_active']);
        $studentFacultyActive = $faculties->filter(function ($faculty) use ($includes) {
            if (!empty($includes['faculty'] ?? null)) {
                return $faculty->is_active == true || in_array($faculty->id, $includes['faculty']);
            } else {
                return $faculty->is_active == true;
            }
        })->pluck('name', 'id');
        $schools = School::orderBy('name')->get(['id', 'name', 'is_active']);
        $studentSchoolActive = $schools->filter(function ($school) use ($includes) {
            if (!empty($includes['school'] ?? null)) {
                return $school->is_active == true || in_array($school->id, $includes['school']);
            } else {
                return $school->is_active == true;
            }
        })->pluck('name', 'id');
        $programmes = Programme::orderBy('name')->get(['id', 'name', 'is_active']);
        $studentProgrammeActive = $programmes->filter(function ($programme) use ($includes) {
            if (!empty($includes['programme'] ?? null)) {
                return $programme->is_active == true || in_array($programme->id, $includes['programme']);
            } else {
                return $programme->is_active == true;
            }
        })->pluck('name', 'id');
        $levelStudies = LevelStudy::orderBy('name')->get(['id', 'name', 'is_active']);
        $studentLevelStudyActive = $levelStudies->filter(function ($levelStudy) use ($includes) {
            if (!empty($includes['levelStudy'] ?? null)) {
                return $levelStudy->is_active == true || in_array($levelStudy->id, $includes['levelStudy']);
            } else {
                return $levelStudy->is_active == true;
            }
        })->pluck('name', 'id');
        $mobilities = Mobility::orderBy('name')->get(['id', 'name', 'is_active'])->map(function ($mobility) {
            $mobility->name = Str::title($mobility->name);
            return $mobility;
        });
        $studentMobilityActive = $mobilities->filter(function ($mobility) use ($includes) {
            if (!empty($includes['mobility'] ?? null)) {
                return $mobility->is_active == true || in_array($mobility->id, $includes['mobility']);
            } else {
                return $mobility->is_active == true;
            }
        })->pluck('name', 'id');
        $campuses = Campus::orderBy('name')->get(['id', 'name', 'is_active']);
        $studentCampusActive = $campuses->filter(function ($campus) use ($includes) {
            if (!empty($includes['campus'] ?? null)) {
                return $campus->is_active == true || in_array($campus->id, $includes['campus']);
            } else {
                return $campus->is_active == true;
            }
        })->pluck('name', 'id');
        $routeOptionEditable = in_array(request()->route()->getName(), ['admin.orientations.edit', 'admin.orientations.create', 'admin.orientations.approvals.edit',]);
        return [
            'orientation_type' => OrientationTypeConstant::getAvailableOptions('title'),
            'student_intake' => $studentIntake = $intakes->pluck('name', 'id'),
            'student_intake_active' => $studentIntakeActive,
            'student_intake_inactive' => $intakes->where('is_active', 0)->pluck('name', 'id'),
            'student_intake_editable' => $routeOptionEditable ? $studentIntakeActive : $studentIntake,
            'student_faculty' => $studentFaculty = $faculties->pluck('name', 'id'),
            'student_faculty_active' => $studentFacultyActive,
            'student_faculty_inactive' => $faculties->where('is_active', 0)->pluck('name', 'id'),
            'student_faculty_editable' => $routeOptionEditable ? $studentFacultyActive : $studentFaculty,
            'student_school' => $studentSchool = $schools->pluck('name', 'id'),
            'student_school_active' => $studentSchoolActive,
            'student_school_inactive' => $schools->where('is_active', 0)->pluck('name', 'id'),
            'student_school_editable' => $routeOptionEditable ? $studentSchoolActive : $studentSchool,
            'student_programme' => $studentProgramme = $programmes->pluck('name', 'id'),
            'student_programme_active' =>  $studentProgrammeActive,
            'student_programme_inactive' => $programmes->where('is_active', 0)->pluck('name', 'id'),
            'student_programme_editable' => $routeOptionEditable ? $studentProgrammeActive : $studentProgramme,
            'student_level_study' => $studentLevelStudy = $levelStudies->pluck('name', 'id'),
            'student_level_study_active' => $studentLevelStudyActive,
            'student_level_study_inactive' => $levelStudies->where('is_active', 0)->pluck('name', 'id'),
            'student_level_study_editable' => $routeOptionEditable ? $studentLevelStudyActive : $studentLevelStudy,
            'student_mobility' => $studentMobility = $mobilities->pluck('name', 'id'),
            'student_mobility_active' => $studentMobilityActive,
            'student_mobility_inactive' => $mobilities->where('is_active', 0)->pluck('name', 'id'),
            'student_mobility_editable' => $routeOptionEditable ? $studentMobilityActive : $studentMobility,
            'student_campus' => $studentCampus = $campuses->pluck('name', 'id'),
            'student_campus_active' =>  $studentCampusActive,
            'student_campus_inactive' => $campuses->where('is_active', 0)->pluck('name', 'id'),
            'student_campus_editable' => $routeOptionEditable ? $studentCampusActive : $studentCampus,
        ];
    }

    /**
     * Apply scope active filter
     */
    public function queryStudentByFilters($input, $auth = null)
    {
        if (!empty($input['scope'] ?? [])) {
            if (in_array('active_filter', $input['scope'])) {
                $input['active_filter'] = $this->getOptions();
            }
        }
        return Student::query()
            ->when(empty($input['filter_all']['faculty'] ?? null) && !empty($input['filters']['faculty'] ?? null), function ($query) use ($input) {
                $query->whereIn('faculty_id', $input['filters']['faculty']);
            }, function ($query) use ($input) {
                $query->when(!empty($input['scope'] ?? []), function ($query) use ($input) {
                    if (in_array('active_filter', $input['scope'])) {
                        $active = array_keys($input['active_filter']['student_faculty_active']->toArray());
                        $query->whereIn('faculty_id', $active);
                    }
                });
            })
            ->when(empty($input['filter_all']['intake'] ?? null) && !empty($input['filters']['intake'] ?? null), function ($query) use ($input) {
                $query->whereIn('intake_id', $input['filters']['intake']);
            }, function ($query) use ($input) {
                $query->when(!empty($input['scope'] ?? []), function ($query) use ($input) {
                    if (in_array('active_filter', $input['scope'])) {
                        $active = array_keys($input['active_filter']['student_intake_active']->toArray());
                        $query->whereIn('intake_id', $active);
                    }
                });
            })
            ->when(empty($input['filter_all']['level_study'] ?? null) && !empty($input['filters']['level_study'] ?? null), function ($query) use ($input) {
                $query->whereIn('level_study_id', $input['filters']['level_study']);
            }, function ($query) use ($input) {
                $query->when(!empty($input['scope'] ?? []), function ($query) use ($input) {
                    if (in_array('active_filter', $input['scope'])) {
                        $active = array_keys($input['active_filter']['student_level_study_active']->toArray());
                        $query->whereIn('level_study_id', $active);
                    }
                });
            })
            ->when(empty($input['filter_all']['school'] ?? null) && !empty($input['filters']['school'] ?? null), function ($query) use ($input) {
                $query->whereIn('school_id', $input['filters']['school']);
            }, function ($query) use ($input) {
                $query->when(!empty($input['scope'] ?? []), function ($query) use ($input) {
                    if (in_array('active_filter', $input['scope'])) {
                        $active = array_keys($input['active_filter']['student_school_active']->toArray());
                        $query->whereIn('school_id', $active);
                    }
                });
            })
            ->when(empty($input['filter_all']['programme'] ?? null) && !empty($input['filters']['programme'] ?? null), function ($query) use ($input) {
                $query->whereIn('programme_id', $input['filters']['programme']);
            }, function ($query) use ($input) {
                $query->when(!empty($input['scope'] ?? []), function ($query) use ($input) {
                    if (in_array('active_filter', $input['scope'])) {
                        $active = array_keys($input['active_filter']['student_programme_active']->toArray());
                        $query->whereIn('programme_id', $active);
                    }
                });
            })
            ->when(empty($input['filter_all']['campus'] ?? null) && !empty($input['filters']['campus'] ?? null), function ($query) use ($input) {
                $query->whereIn('campus_id', $input['filters']['campus']);
            }, function ($query) use ($input) {
                $query->when(!empty($input['scope'] ?? []), function ($query) use ($input) {
                    if (in_array('active_filter', $input['scope'])) {
                        $active = array_keys($input['active_filter']['student_campus_active']->toArray());
                        $query->whereIn('campus_id', $active);
                    }
                });
            })
            ->when(empty($input['filter_all']['mobility'] ?? null) && !empty($input['filters']['mobility'] ?? null), function ($query) use ($input) {
                $query->whereIn('mobility_id', $input['filters']['mobility']);
            }, function ($query) use ($input) {
                $query->when(!empty($input['scope'] ?? []), function ($query) use ($input) {
                    if (in_array('active_filter', $input['scope'])) {
                        $active = array_keys($input['active_filter']['student_mobility_active']->toArray());
                        $query->whereIn('mobility_id', $active);
                    }
                });
            });
    }

    /**
     * Calculate student by filters
     */
    public function calculateStudent($input, $auth = null)
    {
        $countStudent = $this->queryStudentByFilters($input, $auth)
            ->count();
        $message = "{$countStudent} Number of Student";
        $data = [
            'total' => $countStudent
        ];
        return [
            'response' => $this->swalResponse($this->swalData(
                message: $message,
                data: $data,
            )),
            'data' => $data
        ];
    }

    public function update($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $input['updated_by'] = $auth->id;
        $input['is_academic'] = empty($input['is_academic']) ? false : $input['is_academic'];
        $thumbnail_hex = Arr::get($input, 'thumbnail_hex');
        $this->defaultInput($input);
        if ($orientation->status_id == OrientationStatusConstant::APPROVED) {
            if (empty($input['thumbnail_hex'] ?? null)) {
                $mediaThumbnail = $orientation->getMedia('thumbnail')->first() ?? null;
                if (!empty($mediaThumbnail ?? null)) {
                    $input['thumbnail_hex'] = base64_encode(file_get_contents($mediaThumbnail->getPath()));
                }
            }
            $this->validateRequest(new SubmitRequest(), $input);
        } else {
            $this->validateRequest(new StoreRequest(), $input);
        }
        if (!empty($input['attachment_files'] ?? null)) {
            $this->validateRequest(new StoreAttachmentRequest(), $input);
        }
        if ((empty($input['logo_remove'] ?? null) && empty($thumbnail_hex ?? null)) || (!empty($thumbnail_hex ?? null))) {
            $this->validateRequest(new StoreThumbnailRequest(), $input);
        }
        $orientation = DB::transaction(function () use ($input, $orientation, $auth) {
            $input['agenda'] = null;
            $input['synopsis'] = null;
            $orientation->update($input);
            return $orientation;
        });

        if ($thumbnail_hex) {
            // decode the base64 file
            $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $thumbnail_hex));

            // save it to temporary dir first.
            $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
            file_put_contents($tmpFilePath, $fileData);

            // this just to help us get file info.
            $tmpFile = new File($tmpFilePath);

            $thumbnail = new UploadedFile(
                $tmpFile->getPathname(),
                $tmpFile->getFilename(),
                $tmpFile->getMimeType(),
                0,
                true // Mark it as test, since the file isn't from real HTTP POST.
            );
        }

        if (!empty($orientation ?? null)) {
            $this->storeTextMedia($orientation, $input);
            $this->storeAttachments($orientation, $input);
            $this->upsertFilter($orientation, $input, true);
            $this->deleteMediaOrientationTimetable($input, $auth);
            (new OrientationDateService)->syncDate($orientation);

            if (isset($thumbnail)) {
                $this->storeThumbnail($orientation, $input, $thumbnail);
            }
        }
        if (!empty($input['no-next'] ?? null) && (bool)$input['no-next']) {
            return $this->swalResponse($this->swalData(
                message: "Update Successfully",
                function: "window.modules.orientation.stepperOrientationNoNext('stepper')",
            ));
        } else {
            return $this->swalResponse($this->swalData(
                message: "Update Successfully. Continue Next.",
                function: "window.modules.orientation.stepperOrientationNext('stepper')",
            ));
        }
    }

    protected function storeThumbnail($orientation, $input, $thumbnail)
    {
        if (!empty($input['logo_remove'] ?? null)) {
            $thumbnail = $orientation->getMedia('thumbnail');
            if (!empty($thumbnail ?? null)) {
                $thumbnail->first()->delete();
            }
        } else {
            $media = $orientation->addMedia($thumbnail)->toMediaCollection('thumbnail');
            $media->file_name = $media->id . '.png';
            $media->save();
        }
    }

    protected function deleteMediaOrientationTimetable($input, $auth): void
    {
        $studentIds = $this->queryStudentByFilters($input, $auth)->get('id')->pluck('id')->toArray();
        Media::query()
            ->where([
                'model_type' => Student::class,
                'collection_name' => 'orientation-timetable',
            ])
            ->whereIn('model_id', $studentIds)
            ->delete();
    }

    public function deleteMediaOrientationTimetables(): void
    {
        $students = Student::whereHas('media', function ($query) {
            $query->where('collection_name', 'orientation-timetable');
        })->get();
        foreach ($students as $student) {
            $medias = $student->getMedia('orientation-timetable');
            foreach ($medias as $media) {
                $media->delete();
            }
        }
    }

    protected function defaultInput(array &$input): void
    {
        if (empty($input['is_rsvp'] ?? null)) {
            $input['rsvp_capacity'] = null;
        }
        if (empty($input['is_point'] ?? null)) {
            $input['point_max'] = null;
            $input['point_min'] = null;
        }
        $input['scope'] = ['active_filter'];
        $input['total_student'] = (($this->calculateStudent($input))['data']['total'] ?? 0);
    }

    protected function storeAttachments($orientation, $input)
    {
        if (!empty($input['attachment_files'] ?? null)) {
            foreach ($input['attachment_files'] as $attachmentFile) {
                $orientation->addMedia($attachmentFile)->toMediaCollection('files');
            }
        }
        if (!empty($input['attachment_links'] ?? null)) {
            $attachmentLinks = trimArrayFilterExplode(',', $input['attachment_links']);
            foreach ($attachmentLinks as $attachmentLink) {
                $attachmentLink = toUrlMedia($attachmentLink);
                (new OrientationLinkService())->store([
                    'collection_name' => 'files',
                    'orientation_id' => $orientation->id,
                    'name' => trim($attachmentLink),
                    'url' => trim($attachmentLink),
                ]);
            }
        }
    }

    protected function upsertFilter(Orientation $orientation, $input, $include = false)
    {
        $inputFilter = [];
        $inputFilterValues = [];
        if ($include) {
            $options = $this->getOptions($this->getIncludes($orientation));
        } else {
            $options = $this->getOptions();
        }
        $input['filter_all']['intake'] = "0"; //bypass
        $input['filter_all']['faculty'] = "0"; //bypass
        $input['filter_all']['school'] = "0"; //bypass
        $input['filter_all']['programme'] = "0"; //bypass
        $input['filter_all']['mobility'] = "0"; //bypass
        $input['filter_all']['level_study'] = "0"; //bypass
        foreach ($input['filter_all'] as $key => $value) {
            $inputFilterValue = $input['filters'][$key] ?? ["0"];
            $keyInactive = "student_{$key}_inactive";
            $inputFilterValueInactive = [];
            $optionsKeyInactive = $options[$keyInactive];
            if (!empty($optionsKeyInactive ?? null) && !is_array($optionsKeyInactive)) {
                if (is_array($optionsKeyInactive)) {
                    $inputFilterValueInactive = collect($optionsKeyInactive)->keys()->toArray();
                } else {
                    $inputFilterValueInactive = $optionsKeyInactive->keys()->toArray();
                }
            }
            if (!empty($inputFilterValueInactive ?? null) && !empty($inputFilterValue ?? null) && !$include) {
                $inputFilterValue = array_values(array_filter($inputFilterValue, function ($value) use ($inputFilterValueInactive) {
                    return !in_array($value, $inputFilterValueInactive);
                }));
            }
            if ((bool) $value) {
                $value = ['all'];
            } else {
                $value = $inputFilterValue;
            }
            $rawValue = $inputFilterValue;
            $inputFilter = [...$inputFilter, ...[[
                'orientation_id' => $orientation->id,
                'name' => $key,
                'value' => json_encode($value),
                'raw_value' => json_encode($rawValue),
            ]]];
            foreach ($rawValue as $value) {
                $inputFilterValues = [...$inputFilterValues, ...[[
                    'orientation_id' => $orientation->id,
                    'name' => $key,
                    'value' => $value,
                ]]];
            }
        }
        $orientation = DB::transaction(function () use ($orientation, $inputFilter, $inputFilterValues) {
            OrientationFilter::upsert($inputFilter, ['orientation_id', 'name'], ['value', 'raw_value']);
            OrientationFilterValue::where('orientation_id', $orientation->id)->delete();
            OrientationFilterValue::upsert($inputFilterValues, ['orientation_id', 'name', 'value']);
        });
        return $inputFilter;
    }

    public function updateApproval($input, $auth, $uuid)
    {
        if (!(!empty($input['action'] ?? null) && $input['action'] == 'no-update')) {
            $this->update($input, $auth, $uuid);
        }
        $orientation = $this->model->findOrFailByUuid($uuid);
        $inputOrientation = $orientation->toArray();
        $inputOrientation['thumbnail_hex'] = null;
        $mediaThumbnail = $orientation->getMedia('thumbnail')->first() ?? null;
        if (!empty($mediaThumbnail ?? null)) {
            $inputOrientation['thumbnail_hex'] = base64_encode(file_get_contents($mediaThumbnail->getPath()));
        }
        foreach ($inputOrientation['filters'] as $key => $values) {
            if (count($values ?? []) == 1 && $values[0] == "0") {
                $inputOrientation['filters'][$key] = null;
            }
        }
        $this->validateRequest(new SubmitRequest(), $inputOrientation);
        $orientation = DB::transaction(function () use ($input, $orientation, $auth) {
            if ($orientation->status_id == OrientationStatusConstant::REQUIRE_CORRECTION) {
                (new ActionLogService())->store([
                    'action_id' => ActionLogConstant::USER_SUBMIT_CORRECTION,
                    'model_type' => $orientation::class,
                    'model_id' => $orientation->id,
                    'user_id' => $auth->id,
                ]);
                $userManageActivityApprovals = User::permission('manage_activity_approval')->get();
                if (empty($input['direct_approval'] ?? null)) {
                    foreach ($userManageActivityApprovals as $userManageActivityApproval) {
                        Notification::send($userManageActivityApproval, new OrientationApprovalCorrectionNotification($userManageActivityApproval, $orientation));
                    }
                }
            } else {
                (new ActionLogService())->store([
                    'action_id' => ActionLogConstant::USER_SUBMIT_ORIENTATION,
                    'model_type' => $orientation::class,
                    'model_id' => $orientation->id,
                    'user_id' => $auth->id,
                ]);
                $userManageActivityApprovals = User::permission('manage_activity_approval')->get();
                if (empty($input['direct_approval'] ?? null)) {
                    foreach ($userManageActivityApprovals as $userManageActivityApproval) {
                        Notification::send($userManageActivityApproval, new OrientationApprovalNotification($userManageActivityApproval, $orientation));
                    }
                }
            }
            $orientation->update([
                'status_id' => OrientationStatusConstant::PENDING_APPROVAL,
                'status_name' => getConstant('OrientationStatusConstant', 'title', 'PENDING_APPROVAL'),
            ]);
            return $orientation;
        });
        if (!empty($input['direct_approval'] ?? null) && filter_var($input['direct_approval'], FILTER_VALIDATE_BOOLEAN)) {
            (new OrientationApprovalService())->updateApprove($input, $auth, $uuid);
            $message = 'Orientation Activity Successfully Approved';
            return $this->swalResponse($this->swalData(
                message: $message,
                redirect: route('admin.orientations.approvals.edit', $uuid),
            ));
        } else {
            $message = 'Submit to Approval Successfully';
            if (!empty($input['redirect'] ?? null)) {
                return $this->swalResponse($this->swalData(
                    message: $message,
                    redirect: $input['redirect'],
                ));
            } else {
                return $this->responseUpdate($message);
            }
        }
    }

    public function updateLink($input, $auth, $uuidOrientation, $uuidLink)
    {
        $orientation = $this->model->findOrFailByUuid($uuidOrientation);
        (new OrientationLinkService())->update($input, $auth, $uuidLink);
        return $this->responseUpdate();
    }

    public function delete($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $orientation->load(['orientationFilters']);
        if ($orientation->rule_can_delete) {
            $orientation = DB::transaction(function () use ($orientation, $input) {
                foreach ($orientation->orientationFilters as $orientationFilter) {
                    $orientationFilter->delete();
                }
                $orientation->delete();
            });
        } else {
            return $this->throwResponseException(['Cannot DELETE. This data is being used']);
        }
        return $this->responseDestroy();
    }

    public function deleteLink($input, $auth, $uuidOrientation, $uuidLink)
    {
        $orientation = $this->model->findOrFailByUuid($uuidOrientation);
        // if ($orientation->rule_can_delete_attachment) {
        (new OrientationLinkService())->delete($input, $auth, $uuidLink);
        // } else {
        //     return $this->throwResponseException(['Unable to DELETE']);
        // }
        return $this->responseDestroy();
    }

    public function deleteAttendance($input, $auth, $uuidOrientation, $uuidAttendance)
    {
        $orientation = $this->model->findOrFailByUuid($uuidOrientation);
        (new OrientationAttendanceService())->delete($input, $auth, $uuidAttendance);
        return $this->responseDestroy();
    }

    public function deleteRecordingFile($input, $auth, $uuidOrientation, $uuidFile)
    {
        $orientation = $this->model->findOrFailByUuid($uuidOrientation);
        $files = $orientation->getMedia('recordings');
        $files->where('uuid', $uuidFile)->first()->delete();
        return $this->responseDestroy();
    }

    public function deleteRecordingLink($input, $auth, $uuidOrientation, $uuidLink)
    {
        $orientation = $this->model->findOrFailByUuid($uuidOrientation);
        (new OrientationLinkService())->delete($input, $auth, $uuidLink);
        return $this->responseDestroy();
    }

    public function deleteFile($input, $auth, $uuidOrientation, $uuidFile)
    {
        $orientation = $this->model->findOrFailByUuid($uuidOrientation);
        // if ($orientation->rule_can_delete_attachment) {
        $files = $orientation->getMedia('files');
        $files->where('uuid', $uuidFile)->first()->delete();
        // } else {
        //     return $this->throwResponseException(['Unable to DELETE']);
        // }
        return $this->responseDestroy();
    }

    protected function getOrientationFilters($orientation)
    {
        $filterAll = [];
        $filters = $orientation->filters;
        foreach ($filters as $key => $values) {
            if (count($values) == 1 && $values[0] == 'all') {
                $filterAll[$key] = 1;
                $filters[$key] = [];
            } else {
                $filterAll[$key] = 0;
            }
        }
        return [
            'filter_all' => $filterAll,
            'filters' => $filters->toArray(),
        ];
    }

    public function fixEndDate()
    {
        $orientations = $this->model->whereNull('end_date')->whereNotNull('start_date')->get();
        foreach ($orientations as $orientation) {
            $orientation->update([
                'end_date' => $orientation->start_date
            ]);
        }
    }
}
