<?php

namespace App\Services\Admin;

use App\Constants\RoleConstant;
use App\DataTables\Admin\User\StudentDt;
use App\DataTables\Admin\User\UserDt;
use App\Http\Requests\Admin\User\StoreAdminRequest;
use App\Http\Requests\Admin\User\UpdateBulkActiveRequest;
use App\Http\Requests\Admin\User\UpdateRequest;
use App\Models\Role;
use App\Models\Staff;
use App\Models\User;
use App\Notifications\Admin\User\AssignOrientationLeaderNotification;
use App\Services\BaseService;
use App\Services\Integration\Taylors\LdapService;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;
use Spatie\Permission\PermissionRegistrar;

class UserService extends BaseService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new User();
    }

    public function index(Request $request, $auth)
    {
        $title = __('User Management');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $roleUsers = Role::whereNotIn('name', ['student', 'orientation_leader'])->where('is_active', true)->get();
        $roleStudents = Role::whereIn('name', ['student', 'orientation_leader'])->where('is_active', true)->get();
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title', 'roleUsers', 'roleStudents')
        );
        return view('admin.user.index', $compact);
    }

    public function storeStudent($input, $auth = null)
    {
        $user = DB::transaction(function () use ($input) {
            $user = $this->model->create($input);
            $user->setMeta('color', randomClassColor());
            $user->saveMeta();
            $user->syncRoles($input['role_name'] ?? 'student');
            return $user;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $user,
        ];
    }

    public function updateStudent($input, $auth, $uuid)
    {
        $user = $this->model->findOrFailByUuid($uuid);
        $user = DB::transaction(function () use ($input, $user) {
            $user->update($input);
            return $user;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $user,
        ];
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $userDt = new UserDt($request, $auth);
        $studentDt = new StudentDt($request, $auth);
        if ($request->get('table') == 'userDt') {
            return $userDt->render('');
        } elseif ($request->get('table') == 'studentDt') {
            return $studentDt->render('');
        }
        return compact(
            'userDt',
            'studentDt'
        );
    }

    public function update($input, $auth, $uuid)
    {
        $user = $this->model->findOrFailByUuid($uuid);
        $input['role_id'] = Role::findByName($input['role_name'])->id;
        $input['role_name_old'] = Role::findById($user->role_id)->name;
        if ($input['role_name_old'] == RoleConstant::ROLE_STUDENT && $input['role_name'] == RoleConstant::ROLE_ORIENTATION_LEADER) {
            $input['is_active'] = false;
            $input['remember_token'] = Str::random(60);
        } else {
            $input['is_active'] = !empty($input['is_active'] ?? null) ? true : false;
        }
        $this->validateRequest(new UpdateRequest(), $input);
        $user = DB::transaction(function () use ($user, $input) {
            $user->update(Arr::only($input, ['role_id', 'is_active', 'remember_token']));
            $user->syncRoles($input['role_name']);
            app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
            return $user;
        });
        if (!empty($user ?? null) && $input['role_name_old'] == RoleConstant::ROLE_STUDENT && $input['role_name'] == RoleConstant::ROLE_ORIENTATION_LEADER) {
            Notification::send($user, new AssignOrientationLeaderNotification($user));
        }
        return $this->responseUpdate();
    }

    public function orientationLeaderActivation($rememberToken, $input)
    {
        $user = User::where('remember_token', $rememberToken)->firstOrFail();
        $user = DB::transaction(function () use ($user) {
            $user->update([
                'remember_token' => null,
                'is_active' => true,
            ]);
            auth()->login($user);
        });
        return redirect(route('dashboard.index'))->with('toastr-success', 'Your account as Orientation Leader active');
    }

    public function updateActive($input, $auth, $uuid)
    {
        $user = $this->model->findOrFailByUuid($uuid);
        $user = $this->modelUpdateActive($user, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function storeAdmin($input, $auth)
    {
        $this->validateRequest(new StoreAdminRequest(), $input);
        $input['username_email'] = trimArrayFilterExplode(',', $input['username_email']);
        $input['role_id'] = Role::findByName($input['role_name'])->id;
        foreach ($input['username_email'] as $usernameEmail) {
            $attributes = ['cn', 'samaccountname', 'mail'];
            $ldapStaff = LdapService::get('staff', $usernameEmail, $attributes)[0] ?? [];
            if (!empty($ldapStaff ?? null)) {
                $ldapStaffName =  !empty($ldapStaff['cn'][0]) ? $ldapStaff['cn'][0] : '';
                $ldapStaffId = !empty($ldapStaff['samaccountname'][0]) ? $ldapStaff['samaccountname'][0] : '';
                $ldapStaffEmail = !empty($ldapStaff['mail'][0]) ? $ldapStaff['mail'][0] : "{$ldapStaffId}+taylorstaff@taylors.edu.my";
                $inputStaff['name'] = $ldapStaffName ?? $ldapStaffId;
                $inputStaff['email'] = $ldapStaffEmail;
                $inputStaff['taylors_email'] = $ldapStaffEmail;
                $inputStaff['is_active'] = true;
                $inputStaff['password'] = bcrypt('password');
                $inputStaff['email_verified_at'] = now();
                $inputStaff['role_id'] = $input['role_id'];
                $inputStaff['role_name'] = $input['role_name'];
                $inputStaff['model_type'] = Staff::class;
                $user = DB::transaction(function () use ($inputStaff, $auth, $ldapStaffId) {
                    $user = User::where('email', $inputStaff['email'])->first();
                    if (!empty($user ?? null)) {
                        $user->update($inputStaff);
                        $user->staff->update($inputStaff);
                    } else {
                        $user = $this->storeStudent($inputStaff)['data'];
                        $inputStaff['id_number'] = $ldapStaffId;
                        $inputStaff['user_id'] = $user->id;
                        $staff = (new StaffService())->store($inputStaff, $auth)['data'];
                        $user->update(['model_id' => $staff->id]);
                    }
                    return $user;
                });
            } else {
                return $this->throwResponseException(["Problem retrieving staff data for '{$usernameEmail}'. Please check if the Staff ID / Email entered is valid."]);
            }
        }
        return $this->responseStore();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $users = $this->model->whereIn('uuid', $checkboxes);
        DB::transaction(function () use ($users, $active) {
            return $users->update([
                'is_active' => $active
            ]);
        });
        $count = $users->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }

    public function updateBulkRole($input, $auth)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $input['role_id'] = Role::findByName($input['role_name'])->id;
        $users = $this->model->whereIn('uuid', $checkboxes);
        DB::transaction(function () use ($users, $input) {
            foreach ($users->get() as $user) {
                $user->update(Arr::only($input, ['role_id']));
                $user->syncRoles($input['role_name']);
            }
            app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
        });
        $count = $users->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }
}
