<?php

namespace App\Services\Admin;

use App\Models\OrientationAttendance;
use App\Models\OrientationFilter;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class OrientationFilterService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new OrientationFilter();
    }

    public function fixColumnValue()
    {
        foreach ($this->model->get() as $orientationFilter) {
            $orientationFilter->update([
                'value' => $orientationFilter->raw_value
            ]);
        }
    }
}
