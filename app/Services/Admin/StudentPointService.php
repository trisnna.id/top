<?php

namespace App\Services\Admin;

use App\Constants\OrientationTypeConstant;
use App\DataTables\Admin\StudentPoint\StudentPointDt;
use App\DataTables\Orientation\TimelineDt;
use App\Models\CourseIntakeView;
use App\Models\OrientationAttendance;
use App\Models\PreOrientation;
use App\Models\Student;
use App\Models\StudentPoint;
use App\Models\StudentPreOrientation;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Repositories\Contracts\StudentPreOrientationRepository;
use App\Services\OrientationService;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class StudentPointService
{
    use BaseServiceTrait;

    protected $model;

    private StudentPreOrientationRepository $studentPreOrientation;

    private PreOrientationRepository $preOrientation;

    public function __construct()
    {
        $this->model = new StudentPoint();
        $this->preOrientation = App::make('App\Repositories\Contracts\PreOrientationRepository');
    }

    public function index(Request $request, $auth)
    {
        $title = __('Student Points');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.student-point.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $studentPointDt = new StudentPointDt($request, $auth);
        if ($request->get('table') == 'studentPointDt') {
            return $studentPointDt->render('');
        }
        return compact(
            'studentPointDt'
        );
    }

    public function updateActiveStudentPoint()
    {
        $intakeNumbers = CourseIntakeView::select('intake_number')->groupBy('intake_number')->get()->pluck('intake_number')->toArray();
        $students = Student::whereIn('intake_number', $intakeNumbers)->get();
        foreach ($students as $student) {
            $this->updateStudentPoint($student);
        }
    }

    public function getStudentPoint(Student $student)
    {
        $points['orientation_attendance'] = OrientationAttendance::where('student_id', $student->id)->sum('point_attendance') ?? 0;
        $points['orientation_rating'] = OrientationAttendance::where('student_id', $student->id)->sum('point_rating') ?? 0;
        $points['pre_orientation_quiz'] = StudentPreOrientation::where(['student_id' => $student->id, 'completed' => true])->count() * setting('preorientation.point_quiz');
        $points['pre_orientation_survey'] = $student->is_pre_orientation_survey_complete ? setting('survey.point_pre_orientation_survey') : 0;
        $points['post_orientation_survey'] = $student->is_post_orientation_survey_complete ? setting('survey.point_post_orientation_survey') : 0;
        return $points;
    }

    public function updateStudentPoint(Student $student)
    {
        $points = $this->getStudentPoint($student);
        foreach ($points as $key => $value) {
            $input['student_id'] = $student->id;
            $input['name'] = $key;
            $input['point'] = $value;
            $this->updateOrCreate($input);
        }
        $totalPoints = $this->model->where('student_id', $student->id)->sum('point') ?? 0;
        $countOrientationAttended = OrientationAttendance::where('student_id', $student->id)->count() ?? 0;
        $request = request();
        $request->request->add(['student_id' => $student->id]);
        $countOrientation = (new TimelineDt($request, $student->user))->query()->whereIn('orientation_type_id', [OrientationTypeConstant::COMPLEMENTARY, OrientationTypeConstant::COMPULSORY_CORE])->count();
        $countOrientationRsvped = (new OrientationService())->queryCalendarIndex($request, $student->user)->count() ?? 0;
        $percentageOrientationAttendance = $countOrientationRsvped > 0 ? number_format(round((100 / $countOrientationRsvped) * $countOrientationAttended)) : 0;
        $percentageOrientationAttendance = $percentageOrientationAttendance > 100 ? 100 : $percentageOrientationAttendance;

        $countPreOrientationCompleted = StudentPreOrientation::completed($student)->get()->count() ?? 0;
        $countPreOrientation = PreOrientation::published()->whereLikeStudentLocality($student)->count() ?? 0;
        $percentagePreOrientation = $countPreOrientation > 0 ? number_format(round((100 / $countPreOrientation) * $countPreOrientationCompleted)) : 0;
        $percentagePreOrientation = $percentagePreOrientation > 100 ? 100 : $percentagePreOrientation;

        $pointPreOrientationQuiz = $points['pre_orientation_quiz'];
        $pointOrientationRating = $points['orientation_rating'];
        $pointPreOrientationSurvey = $points['pre_orientation_survey'];
        $pointPostOrientationSurvey = $points['post_orientation_survey'];
        $totalPointSurvey = $countOrientation + setting('survey.point_post_orientation_survey') + setting('survey.point_pre_orientation_survey');
        $percentageSurveyCompleted = $totalPointSurvey > 0 ? number_format(round((100 / $totalPointSurvey) * $pointOrientationRating)) : 0;
        $percentageSurveyCompleted = $percentageSurveyCompleted > 100 ? 100 : $percentageSurveyCompleted;

        $percentageAchievement = ($percentagePreOrientation + $percentageOrientationAttendance + $percentageSurveyCompleted) / 300 * 100;
        $percentageAchievement = $percentageAchievement > 100 ? 100 : $percentageAchievement;

        $student->update([
            'total_points' => $totalPoints,
            'pre_orientation_percentage' => $percentagePreOrientation,
            'orientation_attendance_percentage' => $percentageOrientationAttendance,
            'survey_percentage' => $percentageSurveyCompleted,
            'achievement_percentage' => $percentageAchievement,
        ]);
        return $this->model->where('student_id', $student->id)->get();
    }

    public function updateOrCreate($input, $auth = null)
    {
        $studentPoint = DB::transaction(function () use ($input) {
            $studentPoint = StudentPoint::updateOrCreate(
                ['student_id' => $input['student_id'], 'name' => $input['name']],
                ['point' => $input['point'] ?? 0]
            );
            return $studentPoint;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $studentPoint,
        ];
    }
}
