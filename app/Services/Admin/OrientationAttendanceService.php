<?php

namespace App\Services\Admin;

use App\Models\OrientationAttendance;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class OrientationAttendanceService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new OrientationAttendance();
    }

    public function updateOrCreate($input, $auth = null)
    {
        $orientationAttendance = DB::transaction(function () use ($input) {
            $orientationAttendance = OrientationAttendance::updateOrCreate(
                ['orientation_id' => $input['orientation_id'], 'student_id' => $input['student_id']],
                [
                    'rating' => $input['rating'] ?? null,
                    'point' => $input['point'] ?? 0,
                    'point_attendance' => $input['point_attendance'] ?? 0,
                    'point_rating' => $input['point_rating'] ?? 0
                ]
            );
            return $orientationAttendance;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $orientationAttendance,
        ];
    }

    public function delete($input, $auth, $uuid)
    {
        $orientationAttendance = $this->model->findOrFailByUuid($uuid);
        $orientationAttendance = DB::transaction(function () use ($orientationAttendance, $input) {
            $orientationAttendance->delete();
        });
        return $this->responseDestroy();
    }
}
