<?php

namespace App\Services\Admin;

use App\DataTables\Admin\Student\StudentDt;
use App\Exports\Admin\Student\PreSurveyAnswerExport;
use App\Models\Campus;
use App\Models\CourseIntakeView;
use App\Models\Faculty;
use App\Models\Intake;
use App\Models\LevelStudy;
use App\Models\Locality;
use App\Models\Mobility;
use App\Models\Programme;
use App\Models\Role;
use App\Models\School;
use App\Models\Student;
use App\Models\StudentView;
use App\Models\User;
use App\Services\Admin\CampusService;
use App\Services\Admin\FacultyService;
use App\Services\Admin\IntakeService;
use App\Services\Admin\LevelStudyService;
use App\Services\Admin\LocalityService;
use App\Services\Admin\MobilityService;
use App\Services\BaseService;
use App\Services\OrientationService as StudentOrientationService;
use App\Traits\Services\BaseServiceTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class StudentService extends BaseService
{
    use BaseServiceTrait;

    protected $model;

    public const STUDENT_NAME = 'name'; // "name" => "DEEPSHIKA MANJUSHA MANNICK"
    public const STUDENT_ID_NUMBER = 'student_id'; // "student_id" => "0332734"
    // public const STUDENT_GENDER = 'gender'; 
    public const RECENT_STATUS_CHANGE_DATE = 'recent_status_change_date'; // "recent_status_change_date" => "06-JAN-23"
    public const INTAKE_START_DATE = 'intake_start_date'; // "intake_start_date" => "23-AUG-17"
    public const FLAME_MENTOR_NAME = 'flame_mentor_name'; // "flame_mentor_name" => null
    public const CONTACT_NO = 'contact_no'; // "contact_no" => "23057979097 / 011-62384578"
    // public const STUDENT_COUNTRY_FROM = 'country_from';
    public const STUDENT_NATIONALITY = 'nationality'; // "nationality" => "MAURITIUS"
    // public const STUDENT_PROGRAMME_CODE = 'programme_code';
    public const STUDENT_PROGRAMME_NAME = 'programme_name'; // "programme_name" => "Bachelor of Science (Honours) in Architecture"
    // public const STUDENT_FACULTY_CODE = 'faculty_code';
    public const STUDENT_FACULTY_NAME = 'faculty_name'; // "faculty_name" => "School of Architecture, Building & Design"
    // public const STUDENT_SCHOOL_CODE = 'school_code';
    public const STUDENT_SCHOOL_NAME = 'school_name'; // "school_name" => "School of Architecture, Building and Design"
    public const STUDENT_LEVEL_OF_STUDY = 'level_of_study'; // "level_of_study" => "Bachelor"
    public const STUDENT_COURSE_STATUS = 'course_status'; // "course_status" => "Active"
    public const STUDENT_INTAKE_NUMBER = 'intake_number'; // "intake_number" => "201708"
    // public const STUDENT_CURRENT_SEMESTER = 'current_semester';
    // public const STUDENT_STUDY_MODE = 'study_mode';
    public const STUDENT_TAYLORS_EMAIL = 'taylors_email'; // "taylors_email" => "deepshikamanjushamannick@sd.taylors.edu.my"
    public const STUDENT_PERSONAL_EMAIL = 'personal_email'; // "personal_email" => "rmannick@yahoo.com"
    public const STUDENT_LOCALITY = 'locality';
    public const STUDENT_CAMPUS = 'campus'; // "campus" => "TUCLS"
    public const STUDENT_MOBILITY = 'mobility'; // "mobility" => "NO"

    public function __construct()
    {
        $this->model = new Student();
    }

    public function index(Request $request, $auth)
    {
        $title = __('Student List');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.student.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $studentDt = new StudentDt($request, $auth);
        if ($request->get('table') == 'studentDt') {
            return $studentDt->render('');
        }
        return compact(
            'studentDt'
        );
    }

    /**
     * Sync student view to students table and create user and role
     * $syncDate = Y-m-d
     */
    public function syncStudentViews($syncDate = null)
    {
        if (empty($syncDate ?? null)) {
            $syncDate = StudentView::orderBy('sync_date', 'desc')->first()->sync_date;
        }
        $studentViews = StudentView::where('sync_date', $syncDate)
            ->whereNotNull('student_id')
            ->get()
            ->map(function ($studentView) {
                $studentView->taylors_email = empty($studentView->taylors_email ?? null) ? "{$studentView->student_id}@sd.taylors.edu.my" : $studentView->taylors_email;
                $studentView->locality = Str::lower($studentView->nationality ?? '') == 'malaysia' ? 'Local' : 'International';
                $studentView->intake_start_date = !empty($studentView->intake_start_date ?? null) ? Carbon::parse($studentView->intake_start_date)->format('Y-m-d') : $studentView->intake_start_date;
                $studentView->recent_status_change_date = !empty($studentView->recent_status_change_date ?? null) ? Carbon::parse($studentView->recent_status_change_date)->format('Y-m-d') : $studentView->recent_status_change_date;
                return $studentView;
            })
            // ->whereNotIn('student_id', ['0328580']) //delete data student and do correction
            ->toArray();
        $students = [];
        foreach (array_chunk($studentViews, 500) as $chunkStudentViews) {
            $chunkStudents = $this->upserts($chunkStudentViews)['data'];
            $students = array_merge($students, $chunkStudents->toArray());
        }
        return [
            'response' => $this->responseStore(),
            'data' => $students,
        ];
    }

    public function upserts(array $inputs = [])
    {
        $programmes = Programme::all();
        $faculties = Faculty::all();
        $schools = School::all();
        $campuses = Campus::all();
        $levelStudies = LevelStudy::all();
        $localities = Locality::all();
        $mobilities = Mobility::all();
        $intakes = Intake::all();
        $students = [];
        $users = [];
        $students = DB::transaction(function () use ($inputs, $students, $users, $programmes, $schools, $faculties, $campuses, $levelStudies, $localities, $mobilities, $intakes) {
            collect($inputs)->map(function ($student) use (&$students, &$users, &$programmes, &$schools, &$faculties, &$campuses, &$levelStudies, &$localities, &$mobilities, &$intakes) {
                $user = $this->getUser($student);
                $users = [...$users, $user];
                if (!empty($student[self::STUDENT_ID_NUMBER] ?? null)) {
                    $students[$student[self::STUDENT_ID_NUMBER]] = [
                        'name' => $student[self::STUDENT_NAME],
                        'id_number' => $student[self::STUDENT_ID_NUMBER],
                        'recent_status_change_date' => $student[self::RECENT_STATUS_CHANGE_DATE],
                        'intake_start_date' => $student[self::INTAKE_START_DATE],
                        'flame_mentor_name' => $student[self::FLAME_MENTOR_NAME],
                        'contact_number' => $student[self::CONTACT_NO],
                        'nationality' => $student[self::STUDENT_NATIONALITY] ?? null,
                        'programme_id' => $this->getOptionRelationId($programmes, $student, 'programme'),
                        'programme_name' => $student[self::STUDENT_PROGRAMME_NAME] ?? null,
                        'faculty_id' => $this->getOptionRelationId($faculties, $student, 'faculty'),
                        'faculty_name' => $student[self::STUDENT_FACULTY_NAME] ?? null,
                        'school_id' => $this->getOptionRelationId($schools, $student, 'school'),
                        'school_name' => $student[self::STUDENT_SCHOOL_NAME] ?? null,
                        'level_study_id' => $this->getOptionRelationId($levelStudies, $student, 'levelStudy'),
                        'level_of_study' => $student[self::STUDENT_LEVEL_OF_STUDY] ?? null,
                        'course_status' => $student[self::STUDENT_COURSE_STATUS] ?? null,
                        'intake_id' => $this->getOptionRelationId($intakes, $student, 'intake'),
                        'intake_number' => $student[self::STUDENT_INTAKE_NUMBER] ?? null,
                        'taylors_email' => $student[self::STUDENT_TAYLORS_EMAIL] ?? null,
                        'personal_email' => $student[self::STUDENT_PERSONAL_EMAIL] ?? null,
                        'locality_id' => $this->getOptionRelationId($localities, $student, 'locality'),
                        'locality' => $student[self::STUDENT_LOCALITY] ?? null,
                        'campus_id' => $this->getOptionRelationId($campuses, $student, 'campus'),
                        'campus' => $student[self::STUDENT_CAMPUS] ?? null,
                        'mobility_id' => $this->getOptionRelationId($mobilities, $student, 'mobility'),
                        'mobility' => $student[self::STUDENT_MOBILITY] ?? null,
                        'user_id' => $user->id,
                    ];
                }
            });
            foreach ($students as $student) {
                Student::updateOrCreate(['id_number' => $student['id_number']], $student);
            }
            $students = Student::whereIn('taylors_email', collect($users)->pluck('email')->toArray())->get();
            foreach ($users as $user) {
                if (empty($user->model_id ?? null)) {
                    $user->update([
                        'model_type' => Student::class,
                        'model_id' => $students->where('taylors_email', $user->email)->first()->id,
                    ]);
                }
            }
            return $students;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $students,
        ];
    }

    protected function getOptionRelationId(&$options, $student, $relation)
    {
        $relationId = null;
        if ($relation == 'mobility') {
            $relationId = $options->where('name', $student[self::STUDENT_MOBILITY])->first()->id ?? null;
            if (empty($relationId ?? null) && !empty($student[self::STUDENT_MOBILITY] ?? null)) {
                $relation = (new MobilityService())->store([
                    'name' => $student[self::STUDENT_MOBILITY],
                ]);
                $relationId = $relation['data']->id ?? null;
                $options = Mobility::all();
            }
        } elseif ($relation == 'intake') {
            $relationId = $options->where('name', $student[self::STUDENT_INTAKE_NUMBER])->first()->id ?? null;
            if (empty($relationId ?? null) && !empty($student[self::STUDENT_INTAKE_NUMBER] ?? null)) {
                $relation = (new IntakeService())->store([
                    'name' => $student[self::STUDENT_INTAKE_NUMBER],
                ]);
                $relationId = $relation['data']->id ?? null;
                $options = Intake::all();
            }
        } elseif ($relation == 'locality') {
            $relationId = $options->where('name', $student[self::STUDENT_LOCALITY])->first()->id ?? null;
            if (empty($relationId ?? null) && !empty($student[self::STUDENT_LOCALITY] ?? null)) {
                $relation = (new LocalityService())->store([
                    'name' => $student[self::STUDENT_LOCALITY],
                ]);
                $relationId = $relation['data']->id ?? null;
                $options = Locality::all();
            }
        } elseif ($relation == 'levelStudy') {
            $relationId = $options->where('name', $student[self::STUDENT_LEVEL_OF_STUDY])->first()->id ?? null;
            if (empty($relationId ?? null) && !empty($student[self::STUDENT_LEVEL_OF_STUDY] ?? null)) {
                $relation = (new LevelStudyService())->store([
                    'name' => $student[self::STUDENT_LEVEL_OF_STUDY],
                ]);
                $relationId = $relation['data']->id ?? null;
                $options = LevelStudy::all();
            }
        } elseif ($relation == 'campus') {
            $relationId = $options->where('name', $student[self::STUDENT_CAMPUS])->first()->id ?? null;
            if (empty($relationId ?? null) && !empty($student[self::STUDENT_CAMPUS] ?? null)) {
                $relation = (new CampusService())->store([
                    'name' => $student[self::STUDENT_CAMPUS],
                ]);
                $relationId = $relation['data']->id ?? null;
                $options = Campus::all();
            }
        } elseif ($relation == 'faculty') {
            $relationId = $options->where('name', $student[self::STUDENT_FACULTY_NAME])->first()->id ?? null;
            if (empty($relationId ?? null) && !empty($student[self::STUDENT_FACULTY_NAME] ?? null)) {
                $relation = (new FacultyService())->store([
                    'name' => $student[self::STUDENT_FACULTY_NAME],
                ]);
                $relationId = $relation['data']->id ?? null;
                $options = Faculty::all();
            }
        } elseif ($relation == 'programme') {
            $relationId = $options->where('name', $student[self::STUDENT_PROGRAMME_NAME])->first()->id ?? null;
            if (empty($relationId ?? null) && !empty($student[self::STUDENT_PROGRAMME_NAME] ?? null)) {
                $relation = (new ProgrammeService())->store([
                    'name' => $student[self::STUDENT_PROGRAMME_NAME],
                ]);
                $relationId = $relation['data']->id ?? null;
                $options = Programme::all();
            }
        } elseif ($relation == 'school') {
            $relationId = $options->where('name', $student[self::STUDENT_SCHOOL_NAME])->first()->id ?? null;
            if (empty($relationId ?? null) && !empty($student[self::STUDENT_SCHOOL_NAME] ?? null)) {
                $relation = (new SchoolService())->store([
                    'name' => $student[self::STUDENT_SCHOOL_NAME],
                ]);
                $relationId = $relation['data']->id ?? null;
                $options = School::all();
            }
        }
        return $relationId;
    }

    protected function getUser($studentView)
    {
        $student = Student::where('id_number', $studentView[self::STUDENT_ID_NUMBER])->first() ?? null;
        $user = User::where('email', $studentView[self::STUDENT_TAYLORS_EMAIL])->first() ?? null;
        // echo $studentView[self::STUDENT_TAYLORS_EMAIL].' ';
        if (empty($user ?? null) && empty($student ?? null)) {
            $role = Role::findByName('student');
            $user = (new UserService())->storeStudent([
                'name' => $studentView[self::STUDENT_NAME],
                'email' => $studentView[self::STUDENT_TAYLORS_EMAIL],
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('P@s5w0Rd123'),
                'role_id' => $role->id,
                'role_name' => $role->name,
                'created_via' => 'cms',
            ]);
            $user = $user['data'];
        } else {
            if (empty($user ?? null) && !empty($student ?? null)) {
                $user = User::where([
                    'model_type' => $student::class,
                    'model_id' => $student->id,
                ])->first() ?? null;
                if (empty($user ?? null)) {
                    $role = Role::findByName('student');
                    $user = (new UserService())->storeStudent([
                        'name' => $studentView[self::STUDENT_NAME],
                        'email' => $studentView[self::STUDENT_TAYLORS_EMAIL],
                        'email_verified_at' => Carbon::now(),
                        'password' => Hash::make('P@s5w0Rd123'),
                        'role_id' => $role->id,
                        'role_name' => $role->name,
                        'created_via' => 'cms',
                    ]);
                    $user = $user['data'];
                }
            }
            $user = (new UserService())->updateStudent([
                'name' => $studentView[self::STUDENT_NAME],
                'email' => $studentView[self::STUDENT_TAYLORS_EMAIL],
            ], '', $user->uuid);
            $user = $user['data'];
        }
        return $user;
    }

    public function showOrientation(Request $request, $auth, $uuid)
    {
        $student = $this->model->findOrFailByUuid($uuid);
        $student->load(['user']);
        return (new StudentOrientationService())->index($request, $student->user);
    }

    public function fixDummyStudent()
    {
        $activeIntakes = Intake::where('is_active', true)
            ->orderBy('name', 'asc')
            ->get('name')
            ->pluck('name')
            ->toArray();
        $activeStudents = $this->model->where('intake_number', $activeIntakes[0])
            ->limit(30)
            ->get();
        $dummyStudents = $this->model->where('id_number', 'LIKE', '%xxx')
            ->get();
        foreach ($dummyStudents as $key => $dummyStudent) {
            $dummyStudent->update([
                'nationality' => $activeStudents[$key]->nationality,
                'programme_id' => $activeStudents[$key]->programme_id,
                'programme_name' => $activeStudents[$key]->programme_name,
                'faculty_id' => $activeStudents[$key]->faculty_id,
                'faculty_name' => $activeStudents[$key]->faculty_name,
                'school_id' => $activeStudents[$key]->school_id,
                'school_name' => $activeStudents[$key]->school_name,
                'level_study_id' => $activeStudents[$key]->level_study_id,
                'level_of_study' => $activeStudents[$key]->level_of_study,
                'course_status' => $activeStudents[$key]->course_status,
                'intake_id' => $activeStudents[$key]->intake_id,
                'intake_number' => $activeStudents[$key]->intake_number,
                'locality_id' => $activeStudents[$key]->locality_id,
                'locality' => $activeStudents[$key]->locality,
                'campus_id' => $activeStudents[$key]->campus_id,
                'campus' => $activeStudents[$key]->campus,
                'mobility_id' => $activeStudents[$key]->mobility_id,
                'mobility' => $activeStudents[$key]->mobility,
                'intake_start_date' => $activeStudents[$key]->intake_start_date,
                'recent_status_change_date' => $activeStudents[$key]->recent_status_change_date,
            ]);
        }
    }

    public function getPreSurveyAnswer()
    {
        $headings = [];
        $students = $this->model
            ->select([
                DB::raw('id_number as ID'),
                DB::raw('name as Name'),
                DB::raw('intake_number as Intake'),
                'pre_survey_answer'
            ])
            ->get()
            ->map(function ($instance) use (&$headings) {
                foreach (($instance->pre_survey_answer ?? []) as $preSurveyAnswer) {
                    if (is_array($preSurveyAnswer['answer'])) {
                        foreach (($preSurveyAnswer['answer'] ?? []) as $childPreSurveyAnswer) {
                            $question = "Q: {$childPreSurveyAnswer['question']}";
                            $instance->{$question} = $childPreSurveyAnswer['answer'];
                        }
                    } else {
                        $question = "Q: {$preSurveyAnswer['question']}";
                        $instance->{$question} = $preSurveyAnswer['answer'];
                    }
                }
                unset($instance->pre_survey_answer);
                $data = $instance->toArray();
                if (empty($headings) || (count(array_keys($data)) > count($headings))) {
                    $headings = array_keys($data);
                }
                return $instance;
            });
        return Excel::download(new PreSurveyAnswerExport($students, $headings), 'pre-survey-answer.xlsx');
    }

    public function getPostSurveyAnswer()
    {
        $headings = [];
        $students = $this->model
            ->select([
                DB::raw('id_number as ID'),
                DB::raw('name as Name'),
                DB::raw('intake_number as Intake'),
                'post_survey_answer'
            ])
            ->orderBy('intake_number')
            ->get()
            ->map(function ($instance) use (&$headings) {
                foreach (($instance->post_survey_answer ?? []) as $postSurveyAnswer) {
                    if (is_array($postSurveyAnswer['answer'])) {
                        foreach (($postSurveyAnswer['answer'] ?? []) as $childPostSurveyAnswer) {
                            $question = "Q: {$childPostSurveyAnswer['question']}";
                            $instance->{$question} = $childPostSurveyAnswer['answer'];
                        }
                    } else {
                        $question = "Q: {$postSurveyAnswer['question']}";
                        $instance->{$question} = $postSurveyAnswer['answer'];
                    }
                }
                unset($instance->post_survey_answer);
                $data = $instance->toArray();
                if (empty($headings) || (count(array_keys($data)) > count($headings))) {
                    $headings = array_keys($data);
                }
                return $instance;
            });
        return Excel::download(new PreSurveyAnswerExport($students, $headings), 'post-survey-answer.xlsx');
    }
}
