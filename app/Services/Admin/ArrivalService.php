<?php

namespace App\Services\Admin;

use App\Services\Admin\ResourceService;

class ArrivalService extends ResourceService
{
    protected $titleIndex = 'Pre-Arrival';
    protected $titleCreate = 'Create Pre-Arrival';
    protected $titleEdit = 'Pre-Arrival Detail';
    protected $viewIndex = 'admin.arrival.index';
    protected $routeIndex = 'admin.arrivals.index';
    protected $routeShow = 'admin.arrivals.show';
    protected $routeEdit = 'admin.arrivals.edit';
    protected $routeStore = 'admin.arrivals.store';
}
