<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\Setting\Faculty\UpdateBulkActiveRequest;
use App\Models\CourseIntakeView;
use App\Models\Faculty;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class FacultyService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Faculty();
    }

    public function syncCourseIntakeView($syncDate = null)
    {
        if (empty($syncDate ?? null)) {
            $syncDate = CourseIntakeView::orderBy('sync_date', 'desc')->first()->sync_date;
        }
        $courseIntakeViews = array_filter(CourseIntakeView::select('faculty')
            ->where('sync_date', $syncDate)
            ->groupBy('faculty')
            ->get()
            ->map(function ($courseIntakeView) {
                $courseIntakeView->faculty = trim($courseIntakeView->faculty);
                return $courseIntakeView;
            })
            ->pluck('faculty')
            ->toArray());
        $courseIntakeViews = DB::transaction(function () use ($courseIntakeViews) {
            foreach ($courseIntakeViews as $key => $value) {
                $this->model->updateOrCreate(
                    ['name' => $value],
                    ['is_cms_active' => true],
                );
            }
            $this->model->whereNotIn('name', $courseIntakeViews)
                ->update(['is_active' => false, 'is_cms_active' => false]);
            return $courseIntakeViews;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $courseIntakeViews,
        ];
    }

    public function store($input, $auth = null)
    {
        $faculty = DB::transaction(function () use ($input) {
            $faculty = $this->model->create($input);
            return $faculty;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $faculty,
        ];
    }

    public function updateActive($input, $auth, $uuid)
    {
        $faculty = $this->model->findOrFailByUuid($uuid);
        $faculty = $this->modelUpdateActive($faculty, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $faculties = $this->model->whereIn('uuid', $checkboxes)->where('is_cms_active', true);
        DB::transaction(function () use ($faculties, $active) {
            return $faculties->update([
                'is_active' => $active
            ]);
        });
        $count = $faculties->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }
}
