<?php

namespace App\Services\Admin;

use App\Constants\ActionLogConstant;
use App\Constants\ActiveInactiveConstant;
use App\Constants\OrientationStatusConstant;
use App\DataTables\Admin\Orientation\Edit\AttachmentDt as EditAttachmentDt;
use App\DataTables\Admin\Orientation\Show\ActionDt;
use App\DataTables\Admin\Orientation\Show\OrientationAttendanceDt;
use App\DataTables\Admin\Orientation\Show\OrientationRecordingDt;
use App\DataTables\Admin\Orientation\Show\OrientationRsvpDt;
use App\Models\Orientation;
use App\Models\User;
use App\Notifications\Orientation\OrientationApproveNotification;
use App\Notifications\Orientation\OrientationCancelNotification;
use App\Notifications\Orientation\OrientationCorrectionNotification;
use App\Notifications\Orientation\OrientationStudentNotification;
use App\Services\Admin\ActionLogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class OrientationApprovalService extends OrientationService
{

    public function edit(Request $request, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        if (!empty($request->get('table'))) {
            return $this->dataTableEdit($request, $auth, $orientation);
        }
        $title = __('Orientation Timetable');
        $orientation->load(['orientationFilters']);
        $instance['upsert'] = [
            'el' => 'form#stepper-form',
            'axios' => [
                'url' => route('admin.orientations.update', $uuid),
                'method' => 'PUT'
            ]
        ];
        $instance['upsert-no-next'] = [
            'el' => 'form#stepper-form',
            'axios' => [
                'url' => route('admin.orientations.update', [$uuid, 'no-next' => true]),
                'method' => 'PUT'
            ]
        ];
        if ($orientation->status_id == OrientationStatusConstant::APPROVED) {
            $activeInactiveConstant = ActiveInactiveConstant::getAvailableOptions();
            if ($orientation->is_publish == ActiveInactiveConstant::ACTIVE) {
                $titlePublish = strtoupper($activeInactiveConstant[ActiveInactiveConstant::INACTIVE]['title-publish']);
            } elseif ($orientation->is_publish == ActiveInactiveConstant::INACTIVE) {
                $titlePublish = strtoupper($activeInactiveConstant[ActiveInactiveConstant::ACTIVE]['title-publish']);
            }
            $instance['publish'] = [
                'data' => [
                    'status' => $activeInactiveConstant[!(int) $orientation->is_publish]['title-publish'],
                    'color_class' => $activeInactiveConstant[!(int) $orientation->is_publish]['color']['class'],
                ],
                'swal' => [
                    'id' => "swal-blank",
                    'settings' => [
                        'icon' => 'warning',
                        'title' => "Are you sure to {$titlePublish}?",
                        'html' => "You can change it back later!",
                        'confirmButtonText' => 'Confirm',
                    ],
                    'axios' => [
                        'url' => route('admin.orientations.approvals.update-publish', $orientation->uuid),
                        'method' => 'put'
                    ]
                ]
            ];
        }
        if ($orientation->rule_can_approve) {
            $instance['approve'] = [
                'swal' => [
                    'id' => 'approve',
                    'settings' => [
                        'title' => 'Are you sure?',
                        'icon' => 'warning',
                        'confirmButtonColor' => '#3085d6',
                        'cancelButtonColor' => '#d33',
                        'confirmButtonText' => 'Yes',
                    ],
                    'axios' => [
                        'url' => route('admin.orientations.approvals.update-approve', $uuid),
                        'method' => 'PUT'
                    ]
                ]
            ];
        }
        if ($orientation->rule_can_cancel) {
            $instance['cancel'] = [
                'swal' => [
                    'id' => 'cancel',
                    'settings' => [
                        'title' => 'Are you sure?',
                        'icon' => 'warning',
                        'confirmButtonColor' => '#3085d6',
                        'cancelButtonColor' => '#d33',
                        'confirmButtonText' => 'Yes',
                    ],
                    'axios' => [
                        'url' => route('admin.orientations.approvals.update-cancel', $uuid),
                        'method' => 'PUT'
                    ]
                ]
            ];
        }
        $instance['correction'] = [
            'swal' => [
                'id' => 'correction',
                'settings' => [
                    'title' => 'Are you sure?',
                    'icon' => 'warning',
                    'confirmButtonColor' => '#3085d6',
                    'cancelButtonColor' => '#d33',
                    'confirmButtonText' => 'Yes',
                ],
                'axios' => [
                    'url' => route('admin.orientations.approvals.update-correction', $uuid),
                    'method' => 'PUT'
                ]
            ]
        ];
        if ($orientation->is_publish) {
            $instance['notify'] = [
                'swal' => [
                    'id' => 'notify',
                    'settings' => [
                        'title' => 'Are you sure?',
                        'icon' => 'warning',
                        'confirmButtonColor' => '#3085d6',
                        'cancelButtonColor' => '#d33',
                        'confirmButtonText' => 'Yes',
                    ],
                    'axios' => [
                        'url' => route('admin.orientations.approvals.notify-student', $uuid),
                    ]
                ]
            ];
        }
        $options = $this->getOptions($this->getIncludes($orientation));
        $step = $request->session()->pull('orientation.edit.step')[0] ?? null;
        $orientationOnly = $this->orientationOnly($orientation);
        $attendanceQrCode = [
            'orientation_id' => $orientation->uuid,
        ];
        $compact = array_merge(
            $this->dataTableEdit($request, $auth, $orientation),
            compact('title', 'orientation', 'instance', 'options', 'step', 'orientationOnly', 'attendanceQrCode')
        );
        return view('admin.orientation-approval.edit', $compact);
    }

    protected function dataTableEdit(Request $request, $auth, Orientation $orientation)
    {
        $attachmentDt = new EditAttachmentDt($request, $auth, $orientation);
        $actionDt = new ActionDt($request, $auth, $orientation);
        $orientationAttendanceDt = new OrientationAttendanceDt($request, $auth, $orientation);
        $orientationRecordingDt = new OrientationRecordingDt($request, $auth, $orientation);
        if ($request->get('table') == 'attachmentDt') {
            return $attachmentDt->render('');
        } elseif ($request->get('table') == 'actionDt') {
            return $actionDt->render('');
        } elseif ($request->get('table') == 'orientationAttendanceDt') {
            return $orientationAttendanceDt->render('');
        } elseif ($request->get('table') == 'orientationRecordingDt') {
            return $orientationRecordingDt->render('');
        }
        $compact = compact(
            'attachmentDt',
            'actionDt',
            'orientationAttendanceDt',
            'orientationRecordingDt',
        );
        if ($orientation->is_rsvp) {
            $orientationRsvpDt = new OrientationRsvpDt($request, $auth, $orientation);
            if ($request->get('table') == 'orientationRsvpDt') {
                return $orientationRsvpDt->render('');
            }
            $compact = array_merge(
                $compact,
                compact('orientationRsvpDt')
            );
        }
        return $compact;
    }

    public function updateApprove($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $orientation = DB::transaction(function () use ($input, $orientation, $auth) {
            $orientation->update([
                'is_publish' => true,
                'status_id' => OrientationStatusConstant::APPROVED,
                'status_name' => getConstant('OrientationStatusConstant', 'title', 'APPROVED'),
            ]);
            (new ActionLogService())->store([
                'action_id' => ActionLogConstant::ORIENTATION_APPROVED,
                'model_type' => $orientation::class,
                'model_id' => $orientation->id,
                'user_id' => $auth->id,
                'note' => $input['note'] ?? null,
            ]);
            if(empty($input['direct_approval'] ?? null)){
                Notification::send($orientation->createdByUser, new OrientationApproveNotification($orientation->createdByUser, $orientation));
            }
            return $orientation;
        });
        $message = 'Orientation Activity Successfully Approved';
        return $this->swalResponse($this->swalData(
            message: $message,
            reload: true,
        ));
    }

    public function updateCancel($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $orientation = DB::transaction(function () use ($input, $orientation, $auth) {
            $orientation->update([
                'is_publish' => false,
                'status_id' => OrientationStatusConstant::CANCELLED,
                'status_name' => getConstant('OrientationStatusConstant', 'title', 'CANCELLED'),
            ]);
            (new ActionLogService())->store([
                'action_id' => ActionLogConstant::ORIENTATION_CANCELLED,
                'model_type' => $orientation::class,
                'model_id' => $orientation->id,
                'user_id' => $auth->id,
                'note' => $input['note'],
            ]);
            Notification::send($orientation->createdByUser, new OrientationCancelNotification($orientation->createdByUser, $orientation));
            return $orientation;
        });
        $message = 'Orientation Activity Successfully Cancelled';
        return $this->swalResponse($this->swalData(
            message: $message,
            reload: true,
        ));
    }

    public function updateCorrection($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $orientation = DB::transaction(function () use ($input, $orientation, $auth) {
            $orientation->update([
                'status_id' => OrientationStatusConstant::REQUIRE_CORRECTION,
                'status_name' => getConstant('OrientationStatusConstant', 'title', 'REQUIRE_CORRECTION'),
            ]);
            (new ActionLogService())->store([
                'action_id' => ActionLogConstant::ORIENTATION_REQUIRE_CORRECTION,
                'model_type' => $orientation::class,
                'model_id' => $orientation->id,
                'user_id' => $auth->id,
                'note' => $input['note'],
            ]);
            Notification::send($orientation->createdByUser, new OrientationCorrectionNotification($orientation->createdByUser, $orientation));
            return $orientation;
        });
        $message = 'Orientation Activity Successfully Cancelled';
        return $this->swalResponse($this->swalData(
            message: $message,
            reload: true,
        ));
    }

    public function updatePublish($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $orientation = $this->modelUpdateActive($orientation, $input, $auth, 'is_publish');
        if (!empty($input['reload']) && $input['reload'] == 'no') {
            return $this->responseUpdate();
        } else {
            return $this->swalResponse($this->swalData(
                message: 'Successfully Update',
                reload: true,
            ));
        }
    }

    public function notifyStudent($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        list($input['filter_all'], $input['filters'])  = array_values($this->getOrientationFilters($orientation));
        $input['scope'] = ['active_filter'];
        $userIds = $this->queryStudentByFilters($input)->get('user_id')->pluck('user_id')->toArray();
        $countUserIds = count($userIds);
        $users = User::whereIn('id', $userIds)->get();
        foreach ($users as $user) {
            Notification::send($user, new OrientationStudentNotification($user, $orientation));
        }
        $message = "Successfully notify to {$countUserIds} students";
        return $this->responseUpdate($message);
    }
}
