<?php

namespace App\Services\Admin;

use App\DataTables\Admin\Announcement\AnnouncementDt;
use App\DataTables\Admin\Attendance\ActivityDt;
use App\DataTables\Admin\Attendance\AttendanceDt;
use App\DataTables\Admin\User\UserDt;
use App\Models\Attendance;
use App\Notifications\Admin\UserApproveNotification;
use App\Services\BaseService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class AnnouncementService extends BaseService
{
    protected $model;

    public function __construct()
    {
        $this->model = new Attendance();
    }

    public function index(Request $request, $auth)
    {
        $title = __('News & Announcements');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.announcement.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $announcementDt = new AnnouncementDt($request, $auth);
        if ($request->get('table') == 'announcementDt') {
            return $announcementDt->render('');
        }
        return compact(
            'announcementDt',
        );
    }

}
