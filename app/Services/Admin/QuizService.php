<?php

namespace App\Services\Admin;

use App\DataTables\Admin\Quiz\QuizDt;
use App\Models\Survey;
use App\Notifications\Admin\UserApproveNotification;
use App\Services\BaseService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class QuizService extends BaseService
{
    protected $model;

    public function __construct()
    {
        $this->model = new Survey();
    }

    public function index(Request $request, $auth)
    {
        $title = __('Mini Quiz Maker');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.quiz.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $quizDt = new QuizDt($request, $auth);
        if ($request->get('table') == 'quizDt') {
            return $quizDt->render('');
        }
        return compact(
            'quizDt',
        );
    }

    public function create(Request $request, $auth)
    {
        $title = __('Mini Maker Quiz');
        $instance['upsert'] = [
            'data' => [
                'id' => '',
            ],
            'swal' => [
                'id' => 'survey_form',
                'axios' => [
                    'method' => 'post',
                    'url' => route('admin.quizzes.store'),
                ]
            ],
        ];
        return view('admin.quiz.create', compact('title', 'instance'));
    }
}
