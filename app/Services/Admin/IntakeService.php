<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\Setting\Intake\UpdateBulkActiveRequest;
use App\Models\CourseIntakeView;
use App\Models\Intake;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class IntakeService
{
    use BaseServiceTrait;

    protected $model;
    protected $offsetIntake = '202307';

    public function __construct()
    {
        $this->model = new Intake();
    }

    public function syncCourseIntakeView($syncDate = null)
    {
        if (empty($syncDate ?? null)) {
            $syncDate = CourseIntakeView::orderBy('sync_date', 'desc')->first()->sync_date;
        }
        $courseIntakeViews = array_filter(CourseIntakeView::select('intake_number')
            ->where('sync_date', $syncDate)
            ->groupBy('intake_number')
            ->get()
            ->map(function ($courseIntakeView) {
                $courseIntakeView->intake_number = trim($courseIntakeView->intake_number);
                return $courseIntakeView;
            })
            ->pluck('intake_number')
            ->toArray());
        $courseIntakeViews = DB::transaction(function () use ($courseIntakeViews) {
            foreach ($courseIntakeViews as $key => $value) {
                $this->model->updateOrCreate(
                    ['name' => $value],
                    ['is_cms_active' => true],
                );
            }
            $this->model->whereNotIn('name', $courseIntakeViews)
                ->update(['is_active' => false, 'is_cms_active' => false]);
            return $courseIntakeViews;
        });
        DB::transaction(function () {
            $this->model->where('name', '>=', $this->offsetIntake)->update([
                'is_active' => true
            ]);
            $this->model->where('name', '<', $this->offsetIntake)->update([
                'is_active' => false
            ]);
        });
        return [
            'response' => $this->responseStore(),
            'data' => $courseIntakeViews,
        ];
    }

    public function store($input, $auth = null)
    {
        $intake = DB::transaction(function () use ($input) {
            $intake = $this->model->create($input);
            return $intake;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $intake,
        ];
    }

    public function update($input, $auth, $uuid)
    {
        $intake = $this->model->findOrFailByUuid($uuid);
        $input['is_orientation_activities_active'] = !empty($input['is_orientation_activities_active'] ?? null) ? true : false;
        $input['is_timetable_active'] = !empty($input['is_timetable_active'] ?? null) ? true : false;
        $input['updated_by'] = $auth->id;
        $intake = DB::transaction(function () use ($intake, $input) {
            $input = Arr::only($input, ['is_orientation_activities_active', 'orientation_activities_message', 'is_timetable_active', 'timetable_message', 'updated_by']);
            $intake->update($input);
            return $intake;
        });
        return $this->responseUpdate();
    }

    public function updateActive($input, $auth, $uuid)
    {
        $intake = $this->model->findOrFailByUuid($uuid);
        $intake = $this->modelUpdateActive($intake, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function updateOrientationActivitiesActive($input, $auth, $uuid)
    {
        $intake = $this->model->findOrFailByUuid($uuid);
        $intake = $this->modelUpdateActive($intake, $input, $auth, 'is_orientation_activities_active');
        return $this->responseUpdate();
    }

    public function updateTimetableActive($input, $auth, $uuid)
    {
        $intake = $this->model->findOrFailByUuid($uuid);
        $intake = $this->modelUpdateActive($intake, $input, $auth, 'is_timetable_active');
        return $this->responseUpdate();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $intakes = $this->model->whereIn('uuid', $checkboxes)->where('is_cms_active', true);
        DB::transaction(function () use ($intakes, $active) {
            return $intakes->update([
                'is_active' => $active
            ]);
        });
        $count = $intakes->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }
}
