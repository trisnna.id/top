<?php

namespace App\Services\Admin;

use App\Models\Staff;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class StaffService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Staff();
    }

    public function store($input, $auth)
    {
        $staff = DB::transaction(function () use ($input) {
            $staff = $this->model->create($input);
            return $staff;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $staff
        ];
    }
}
