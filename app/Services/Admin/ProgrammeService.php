<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\Setting\Programme\UpdateBulkActiveRequest;
use App\Models\CourseIntakeView;
use App\Models\Programme;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class ProgrammeService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Programme();
    }

    public function syncCourseIntakeView($syncDate = null)
    {
        if (empty($syncDate ?? null)) {
            $syncDate = CourseIntakeView::orderBy('sync_date', 'desc')->first()->sync_date;
        }
        $courseIntakeViews = array_filter(CourseIntakeView::select('course_description')
            ->where('sync_date', $syncDate)
            ->groupBy('course_description')
            ->get()
            ->map(function ($courseIntakeView) {
                $courseIntakeView->course_description = trim($courseIntakeView->course_description);
                return $courseIntakeView;
            })
            ->pluck('course_description')
            ->toArray());
        $courseIntakeViews = DB::transaction(function () use ($courseIntakeViews) {
            foreach ($courseIntakeViews as $key => $value) {
                $this->model->updateOrCreate(
                    ['name' => $value],
                    ['is_cms_active' => true],
                );
            }
            $this->model->whereNotIn('name', $courseIntakeViews)
                ->update(['is_active' => false, 'is_cms_active' => false]);
            return $courseIntakeViews;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $courseIntakeViews,
        ];
    }

    public function store($input, $auth = null)
    {
        $programme = DB::transaction(function () use ($input) {
            $programme = $this->model->create($input);
            return $programme;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $programme,
        ];
    }

    public function updateActive($input, $auth, $uuid)
    {
        $programme = $this->model->findOrFailByUuid($uuid);
        $programme = $this->modelUpdateActive($programme, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $programmes = $this->model->whereIn('uuid', $checkboxes)->where('is_cms_active', true);
        DB::transaction(function () use ($programmes, $active) {
            return $programmes->update([
                'is_active' => $active
            ]);
        });
        $count = $programmes->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }
}
