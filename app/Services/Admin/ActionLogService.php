<?php

namespace App\Services\Admin;

use App\Models\ActionLog;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class ActionLogService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new ActionLog();
    }

    public function store($input, $auth = null)
    {
        $actionLog = DB::transaction(function () use ($input) {
            $actionLog = $this->model->create($input);
            return $actionLog;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $actionLog,
        ];
    }
}
