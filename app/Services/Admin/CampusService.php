<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\Setting\Campus\UpdateBulkActiveRequest;
use App\Models\Campus;
use App\Models\StudentView;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class CampusService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Campus();
    }

    public function syncCourseIntakeView($syncDate = null)
    {
        if (empty($syncDate ?? null)) {
            $syncDate = StudentView::orderBy('sync_date', 'desc')->first()->sync_date;
        }
        $studentViews = array_filter(StudentView::select('campus')
            ->where('sync_date', $syncDate)
            ->groupBy('campus')
            ->get()
            ->map(function ($studentView) {
                $studentView->campus = trim($studentView->campus);
                return $studentView;
            })
            ->pluck('campus')
            ->toArray());
        $studentViews = DB::transaction(function () use ($studentViews) {
            foreach ($studentViews as $key => $value) {
                $this->model->updateOrCreate(
                    ['name' => $value],
                    ['is_cms_active' => true],
                );
            }
            $this->model->whereNotIn('name', $studentViews)
                ->update(['is_active' => false, 'is_cms_active' => false]);
            return $studentViews;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $studentViews,
        ];
    }

    public function store($input, $auth = null)
    {
        $campus = DB::transaction(function () use ($input) {
            $campus = $this->model->create($input);
            return $campus;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $campus,
        ];
    }

    public function updateActive($input, $auth, $uuid)
    {
        $campus = $this->model->findOrFailByUuid($uuid);
        $campus = $this->modelUpdateActive($campus, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $campuses = $this->model->whereIn('uuid', $checkboxes)->where('is_cms_active', true);
        DB::transaction(function () use ($campuses, $active) {
            return $campuses->update([
                'is_active' => $active
            ]);
        });
        $count = $campuses->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }
}
