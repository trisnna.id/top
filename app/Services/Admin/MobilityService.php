<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\Setting\Mobility\UpdateBulkActiveRequest;
use App\Models\Mobility;
use App\Models\StudentView;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class MobilityService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Mobility();
    }

    public function syncCourseIntakeView($syncDate = null)
    {
        if (empty($syncDate ?? null)) {
            $syncDate = StudentView::orderBy('sync_date', 'desc')->first()->sync_date;
        }
        $studentViews = array_filter(StudentView::select('mobility')
            ->where('sync_date', $syncDate)
            ->groupBy('mobility')
            ->get()
            ->map(function ($studentView) {
                $studentView->mobility = trim($studentView->mobility);
                return $studentView;
            })
            ->pluck('mobility')
            ->toArray());
        $studentViews = DB::transaction(function () use ($studentViews) {
            foreach ($studentViews as $key => $value) {
                $this->model->updateOrCreate(
                    ['name' => $value],
                    ['is_cms_active' => true],
                );
            }
            $this->model->whereNotIn('name', $studentViews)
                ->update(['is_active' => false, 'is_cms_active' => false]);
            return $studentViews;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $studentViews,
        ];
    }

    public function store($input, $auth = null)
    {
        $mobility = DB::transaction(function () use ($input) {
            $mobility = $this->model->create($input);
            return $mobility;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $mobility,
        ];
    }

    public function updateActive($input, $auth, $uuid)
    {
        $mobility = $this->model->findOrFailByUuid($uuid);
        $mobility = $this->modelUpdateActive($mobility, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $mobilities = $this->model->whereIn('uuid', $checkboxes)->where('is_cms_active', true);
        DB::transaction(function () use ($mobilities, $active) {
            return $mobilities->update([
                'is_active' => $active
            ]);
        });
        $count = $mobilities->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }
}
