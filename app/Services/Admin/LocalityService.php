<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\Setting\Locality\UpdateBulkActiveRequest;
use App\Models\Locality;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class LocalityService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Locality();
    }

    public function store($input, $auth = null)
    {
        $locality = DB::transaction(function () use ($input) {
            $locality = $this->model->create($input);
            return $locality;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $locality,
        ];
    }

    public function updateActive($input, $auth, $uuid)
    {
        $locality = $this->model->findOrFailByUuid($uuid);
        $locality = $this->modelUpdateActive($locality, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $localities = $this->model->whereIn('uuid', $checkboxes);
        DB::transaction(function () use ($localities, $active) {
            return $localities->update([
                'is_active' => $active
            ]);
        });
        $count = $localities->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }
}
