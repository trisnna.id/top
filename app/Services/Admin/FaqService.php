<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\Faq\StoreRequest;
use App\Http\Requests\Admin\Faq\UpdateBulkActiveRequest;
use App\Models\Faq;
use App\Models\FaqFilter;
use App\Models\FaqFilterValue;
use App\Services\BaseService;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FaqService extends BaseService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Faq();
    }

    public function store($input, $auth)
    {
        $input['is_active'] = !empty($input['is_active'] ?? null) ? true : false;
        $input['created_by'] = $auth->id;
        $this->validateRequest(new StoreRequest(), $input);
        $faq = DB::transaction(function () use ($input) {
            $inputAnswer = $input['answer'];
            $input['answer'] = "";
            $faq = $this->model->create($input);
            $input['answer'] = $inputAnswer;
            if (!empty($input['answer'] ?? null)) {
                $answer = $this->storeAnswer($faq, $input);
                $input['answer'] = $answer->getFullUrl();
            }
            $faq->update($input);
            $this->upsertFilter($faq, $input);
            return $faq;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $faq
        ];
    }

    public function update($input, $auth, $uuid)
    {
        $faq = $this->model->findOrFailByUuid($uuid);
        $input['is_active'] = !empty($input['is_active'] ?? null) ? true : false;
        $input['updated_by'] = $auth->id;
        $this->validateRequest(new StoreRequest(), $input);
        $faq = DB::transaction(function () use ($faq, $input) {
            if (!empty($input['answer'] ?? null)) {
                $answer = $this->storeAnswer($faq, $input);
                $input['answer'] = $answer->getFullUrl();
            }
            $faq->update($input);
            $this->upsertFilter($faq, $input, true);
            return $faq;
        });
        return $this->responseUpdate();
    }

    protected function storeAnswer($faq, $input)
    {
        $media = null;
        if (!empty($input['answer'])) {
            $name = Str::slug("answer-{$faq->uuid}") . '.txt';
            $disk = Storage::disk('public');
            if ($disk->put($name, $input['answer'])) {
                $filename = Str::slug('answer');
                $faq->addMedia($disk->path($name))->usingName($filename)->usingFileName($filename . '.txt')->toMediaCollection('answer');
            }
            $media = $faq->getFirstMedia('answer');
        }
        return $media;
    }

    protected function getIncludes($faq)
    {
        return [
            'intake' => $faq->faqFilters->where('name', 'intake')->first()->value ?? [],
            'faculty' => $faq->faqFilters->where('name', 'faculty')->first()->value ?? [],
            'school' => $faq->faqFilters->where('name', 'school')->first()->value ?? [],
            'programme' => $faq->faqFilters->where('name', 'programme')->first()->value ?? [],
            'levelStudy' => $faq->faqFilters->where('name', 'levelStudy')->first()->value ?? [],
            'mobility' => $faq->faqFilters->where('name', 'mobility')->first()->value ?? [],
        ];
    }

    protected function upsertFilter(Faq $faq, $input, $include = false)
    {
        $inputFilter = [];
        $inputFilterValues = [];
        if ($include) {
            $options = (new ResourceService())->getOptions($this->getIncludes($faq));
        } else {
            $options = (new ResourceService())->getOptions();
        }
        foreach ($input['filter_all'] as $key => $value) {
            $inputFilterValue = $input['filters'][$key] ?? ["0"];
            $keyInactive = "student_{$key}_inactive";
            $inputFilterValueInactive = [];
            $optionsKeyInactive = $options[$keyInactive];
            if (!empty($optionsKeyInactive ?? null) && !is_array($optionsKeyInactive)) {
                if (is_array($optionsKeyInactive)) {
                    $inputFilterValueInactive = collect($optionsKeyInactive)->keys()->toArray();
                } else {
                    $inputFilterValueInactive = $optionsKeyInactive->keys()->toArray();
                }
            }
            if (!empty($inputFilterValueInactive ?? null) && !empty($inputFilterValue ?? null) && !$include) {
                $inputFilterValue = array_values(array_filter($inputFilterValue, function ($value) use ($inputFilterValueInactive) {
                    return !in_array($value, $inputFilterValueInactive);
                }));
            }
            if ((bool) $value) {
                $value = ['all'];
            } else {
                $value = $inputFilterValue;
            }
            $rawValue = $inputFilterValue;
            $inputFilter = [...$inputFilter, ...[[
                'faq_id' => $faq->id,
                'name' => $key,
                'value' => json_encode($value),
            ]]];
            foreach ($rawValue as $value) {
                $inputFilterValues = [...$inputFilterValues, ...[[
                    'faq_id' => $faq->id,
                    'name' => $key,
                    'value' => $value,
                ]]];
            }
        }
        $faq = DB::transaction(function () use ($faq, $inputFilter, $inputFilterValues) {
            FaqFilter::upsert($inputFilter, ['faq_id', 'name'], ['value']);
            FaqFilterValue::where('faq_id', $faq->id)->delete();
            FaqFilterValue::upsert($inputFilterValues, ['faq_id', 'name', 'value']);
        });
        return $inputFilter;
    }

    public function updateActive($input, $auth, $uuid)
    {
        $faq = $this->model->findOrFailByUuid($uuid);
        $faq = $this->modelUpdateActive($faq, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $faqs = $this->model->whereIn('uuid', $checkboxes);
        DB::transaction(function () use ($faqs, $active) {
            return $faqs->update([
                'is_active' => $active
            ]);
        });
        $count = $faqs->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }

    public function delete($input, $auth, $uuid)
    {
        $faq = $this->model->findOrFailByUuid($uuid);
        if ($faq->rule_can_delete) {
            DB::transaction(function () use ($faq, $input) {
                $faq->delete();
            });
        } else {
            return $this->throwResponseException(['Cannot DELETE. This data is being used']);
        }
        return $this->responseDestroy();
    }

    public function clone(Request $request, $auth, $uuid)
    {
        $faq = $this->model->findOrFailByUuid($uuid);
        $filterAll = [];
        $filters = $faq->filters;
        foreach ($filters as $key => $values) {
            if (count($values) == 1 && $values[0] == 'all') {
                $filterAll[$key] = 1;
                $filters[$key] = [];
            } else {
                $filterAll[$key] = 0;
            }
        }
        $input = [
            'question' => $faq->question,
            'answer' => $faq->answer,
            'is_active' => false,
            'filter_all' => $filterAll,
            'filters' => $filters->toArray(),
        ];
        return DB::transaction(function () use ($input, $auth, $faq) {
            $faqStore = $this->store($input, $auth);
            $faqClone = $faqStore['data'];
            return $faqStore;
        });
    }
}
