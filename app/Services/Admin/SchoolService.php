<?php

namespace App\Services\Admin;

use App\Http\Requests\Admin\Setting\School\UpdateBulkActiveRequest;
use App\Models\CourseIntakeView;
use App\Models\School;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\DB;

class SchoolService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new School();
    }

    public function syncCourseIntakeView($syncDate = null)
    {
        if (empty($syncDate ?? null)) {
            $syncDate = CourseIntakeView::orderBy('sync_date', 'desc')->first()->sync_date;
        }
        $courseIntakeViews = array_filter(CourseIntakeView::select('school')
            ->where('sync_date', $syncDate)
            ->groupBy('school')
            ->get()
            ->map(function ($courseIntakeView) {
                $explodes = trimArrayFilterExplode(' - ', $courseIntakeView->school);
                $courseIntakeView->school = $explodes[1] ?? ($explodes[0] ?? null);
                return $courseIntakeView;
            })
            ->pluck('school')
            ->toArray());
        $courseIntakeViews = DB::transaction(function () use ($courseIntakeViews) {
            foreach ($courseIntakeViews as $key => $value) {
                $this->model->updateOrCreate(
                    ['name' => $value],
                    ['is_cms_active' => true],
                );
            }
            $this->model->whereNotIn('name', $courseIntakeViews)
                ->update(['is_active' => false, 'is_cms_active' => false]);
            return $courseIntakeViews;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $courseIntakeViews,
        ];
    }

    public function store($input, $auth = null)
    {
        $school = DB::transaction(function () use ($input) {
            $school = $this->model->create($input);
            return $school;
        });
        return [
            'response' => $this->responseStore(),
            'data' => $school,
        ];
    }

    public function updateActive($input, $auth, $uuid)
    {
        $school = $this->model->findOrFailByUuid($uuid);
        $school = $this->modelUpdateActive($school, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $schools = $this->model->whereIn('uuid', $checkboxes)->where('is_cms_active', true);
        DB::transaction(function () use ($schools, $active) {
            return $schools->update([
                'is_active' => $active
            ]);
        });
        $count = $schools->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }
}
