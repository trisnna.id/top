<?php

namespace App\Services\Admin;

use App\Constants\RoleConstant;
use App\DataTables\Admin\Role\RoleDt;
use App\Http\Requests\Admin\Role\StoreRequest;
use App\Http\Requests\Admin\Role\UpdateBulkActiveRequest;
use App\Http\Requests\Admin\Role\UpdateRequest;
use App\Models\Role;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Permission\PermissionRegistrar;

class RoleService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Role();
    }

    public function index(Request $request, $auth)
    {
        $title = __('Role Management');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('admin.role.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $roleDt = new RoleDt($request, $auth);
        if ($request->get('table') == 'roleDt') {
            return $roleDt->render('');
        }
        return compact(
            'roleDt'
        );
    }

    public function store($input, $auth)
    {
        $input['name'] = Str::slug($input['title'], '_');
        $input['is_active'] = !empty($input['is_active'] ?? null) ? true : false;
        $input['created_by'] = $auth->id;
        $this->validateRequest(new StoreRequest(), $input);
        $role = DB::transaction(function () use ($input) {
            $role = $this->model->create($input);
            $role->syncPermissions([...$input['permissions'], 'admin']);
            app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
            return $role;
        });
        return $this->responseStore();
    }

    public function update($input, $auth, $uuid)
    {
        $role = $this->model->findOrFailByUuid($uuid);
        $input['name'] = $role->name;
        $input['is_active'] = !empty($input['is_active'] ?? null) ? true : false;
        $input['updated_by'] = $auth->id;
        $this->validateRequest(new UpdateRequest(), $input);
        $role = DB::transaction(function () use ($role, $input) {
            $role->update($input);
            if ($input['name'] == RoleConstant::ROLE_ORIENTATION_LEADER) {
                $role->syncPermissions([...$input['permissions'], 'admin', 'student']);
            } else {
                $role->syncPermissions([...$input['permissions'], 'admin']);
            }
            app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
            return $role;
        });
        return $this->responseUpdate();
    }

    public function updateActive($input, $auth, $uuid)
    {
        $role = $this->model->findOrFailByUuid($uuid);
        $role = $this->modelUpdateActive($role, $input, $auth, 'is_active');
        return $this->responseUpdate();
    }

    public function delete($input, $auth, $uuid)
    {
        $role = $this->model->findOrFailByUuid($uuid);
        if ($role->rule_can_delete) {
            $role = DB::transaction(function () use ($role, $input) {
                $role->syncPermissions([]);
                app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
                $role->delete();
            });
        } else {
            return $this->throwResponseException(['Cannot DELETE. This data is being used']);
        }
        return $this->responseDestroy();
    }

    public function updateBulkActive($input, $auth, $active)
    {
        $this->validateRequest(new UpdateBulkActiveRequest(), $input);
        $checkboxes = trimArrayFilterExplode(',', $input['checkbox']);
        $roles = $this->model->whereIn('uuid', $checkboxes);
        DB::transaction(function () use ($roles, $active) {
            return $roles->update([
                'is_active' => $active
            ]);
        });
        $count = $roles->count();
        $message = "Successfully update {$count} record(s)";
        return $this->responseUpdate($message);
    }
}
