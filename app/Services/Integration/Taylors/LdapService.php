<?php

namespace App\Services\Integration\Taylors;

class LdapService
{

    protected function getConfig($type): array
    {
        $config = [];
        if ($type == 'student') {
            $config['base_dn'] = config('services.taylors.ldap.student.base_dn');
            $config['host'] = ldap_connect(config('services.taylors.ldap.student.host'));
            $config['domain'] = config('services.taylors.ldap.student.domain');
        } elseif ($type == 'staff') {
            $config['base_dn'] = config('services.taylors.ldap.staff.base_dn');
            $config['host'] = ldap_connect(config('services.taylors.ldap.staff.host'));
            $config['domain'] = config('services.taylors.ldap.staff.domain');
        } else {
            $config['base_dn'] = null;
            $config['host'] = null;
            $config['domain'] = null;
        }
        $config['opt_referrals'] = config('services.taylors.ldap.opt_referrals');
        $config['opt_protocol_version'] = config('services.taylors.ldap.opt_protocol_version');
        $config['username'] = config('services.taylors.ldap.username');
        $config['password'] = config('services.taylors.ldap.password');
        return $config;
    }

    /**
     * The method to bind user to ldap with given username/password.
     *
     * @param  string  $type
     * @param  string  $username
     * @param  string  $password
     * @return bool
     */
    public static function auth($type, $username, $password)
    {
        $config = (new self)->getConfig($type);
        ldap_set_option($config['host'], LDAP_OPT_REFERRALS, $config['opt_referrals']);
        ldap_set_option($config['host'], LDAP_OPT_PROTOCOL_VERSION, $config['opt_protocol_version']);
        $ldapBind = @ldap_bind($config['host'], $username . '@' . $config['domain'], $password);
        if ($ldapBind) {
            return true;
        }
        return false;
    }

    /**
     * The method to search user info in active directory.
     *
     * @param  string $type
     * @param  string $ad_id
     * @param  array $attributes
     * @return mixed
     */
    public static function get($type, $adId, $attributes = ['displayName', 'samaccountname'])
    {
        $config = (new self)->getConfig($type);
        ldap_set_option($config['host'], LDAP_OPT_REFERRALS, $config['opt_referrals']);
        ldap_set_option($config['host'], LDAP_OPT_PROTOCOL_VERSION, $config['opt_protocol_version']);
        $username  = $config['username'];
        $password = $config['password'];
        $search_filter = '(samaccountname=' . $adId . ')';
        if ($type == 'staff') {
            $search_filter = "(|(samaccountname={$adId})(userprincipalname={$adId}))";
        }
        if ($ldap_bind = @ldap_bind($config['host'], $username . '@' . $config['domain'], $password)) {
            if ($result = ldap_search($config['host'], $config['base_dn'], $search_filter, $attributes)) {
                if ($count = ldap_count_entries($config['host'], $result)) {
                    if ($info = ldap_get_entries($config['host'], $result)) {
                        return $info;
                    }
                }
            }
        }
        return [];
    }
}
