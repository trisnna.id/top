<?php

namespace App\Services\Integration\Taylors;

use App\Models\CourseIntakeView;
use App\Models\StudentView;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CmsService
{

    protected $conn = null;
    protected $host;
    protected $port;
    protected $user;
    protected $password;
    protected $sid;
    protected $location;

    public function __destruct()
    {
        if ($this->conn) oci_close($this->conn);
    }

    public static function getCourseIntakes()
    {
        if ($query = (new self)->queryCourseIntakes()) {
            $results = [];
            while ($row = oci_fetch_assoc($query)) {
                array_push($results, $row ?? null);
            }
            dd($results, 1);
        } else {
        }
    }

    public static function syncCourseIntakes()
    {
        if (config('services.taylors.cms.enabled') && $query = (new self)->queryCourseIntakes()) {
            while ($row = oci_fetch_assoc($query)) {
                $courseIntake = CourseIntakeView::updateOrCreate([
                    "course_code" => $row['COURSE_CODE'] ?? null,
                    "course_description" => $row['COURSE_DESCRIPTION'] ?? null,
                    "intake_number" => $row['INTAKE_NUMBER'] ?? null,
                    "school" => $row['SCHOOL'] ?? null,
                    "faculty" => $row['FACULTY'] ?? null,
                ], [
                    "sync_date" => Carbon::today(),
                ]);
                Log::channel('dailyintegration')->info(json_encode(array_merge(['event' => 'integration: sync cms course intake'], $courseIntake->toArray())));
            }
            return true;
        } else {
            return false;
        }
    }

    public function queryCourseIntakes()
    {
        $query = "SELECT * FROM V_TOP_COURSE_INTAKE";
        $data = $this->find($query);
        if ($data) {
            return $data;
        }
        return false;
    }

    public function findStudentLike($value, $field = 'TAYLORS_EMAIL')
    {
        $query = "SELECT * FROM V_TOP_STUDENT WHERE {$field} LIKE '%{$value}%'";
        $data = $this->find($query);
        if ($data) {
            $results = [];
            while ($row = oci_fetch_assoc($data)) {
                array_push($results, $row ?? null);
            }
            dd($results);
            return $data;
        }
        return false;
    }

    public static function syncStudents()
    {
        if (config('services.taylors.cms.enabled') && $query = (new self)->queryStudents()) {
            while ($row = oci_fetch_assoc($query)) {
                $student = StudentView::updateOrCreate(['student_id' => $row['STUDENT_ID']], [
                    "name" => trim($row['NAME'] ?? '') ?? null,
                    "student_id" => $row['STUDENT_ID'] ?? null,
                    "faculty_name" => $row['FACULTY_NAME'] ?? null,
                    "school_name" => $row['SCHOOL_NAME'] ?? null,
                    "intake_number" => $row['INTAKE_NUMBER'] ?? null,
                    "contact_no" => $row['CONTACT_NO'] ?? null,
                    "programme_name" => $row['PROGRAMME_NAME'] ?? null,
                    "student_level" => $row['STUDENT_LEVEL'] ?? null,
                    "taylors_email" => $row['TAYLORS_EMAIL'] ?? null,
                    "personal_email" => $row['PERSONAL_EMAIL'] ?? null,
                    "flame_mentor_name" => $row['FLAME_MENTOR_NAME'] ?? null,
                    "campus" => $row['CAMPUS'] ?? null,
                    "nationality" => $row['NATIONALITY'] ?? null,
                    "mobility" => $row['MOBILITY'] ?? null,
                    "level_of_study" => $row['LEVEL_OF_STUDY'] ?? null,
                    "intake_start_date" => $row['INTAKE_START_DATE'] ?? null,
                    "recent_status_change_date" => $row['RECENT_STATUS_CHANGE_DATE'] ?? null,
                    "course_status" => $row['COURSE_STATUS'] ?? null,
                    "profile_status" => $row['PROFILE_STATUS'] ?? null,
                    "sync_date" => Carbon::today(),
                ]);
                Log::channel('dailyintegration')->info(json_encode(array_merge(['event' => 'integration: sync cms student'], $student->toArray())));
            }
            return true;
        } else {
            return false;
        }
    }

    public function queryStudents()
    {
        $query = "SELECT * FROM V_TOP_STUDENT";
        $data = $this->find($query);
        if ($data) {
            return $data;
        }
        return false;
    }

    public static function getViews()
    {
        if ($query = (new self)->queryViews()) {
            $results = [];
            while ($row = oci_fetch_assoc($query)) {
                array_push($results, $row ?? null);
            }
            dd($results, 1);
        } else {
        }
    }

    public function queryViews()
    {
        $query = "SELECT VIEW_NAME FROM all_views";
        $data = $this->find($query);
        if ($data) {
            return $data;
        }
        return false;
    }


    protected function find($sql)
    {
        $this->setConnection();
        $query = oci_parse($this->conn, $sql);
        if (oci_execute($query)) {
            return $query;
        }
        return false;
    }

    protected function setConfig()
    {
        $this->host = config('services.taylors.cms.host');
        $this->port = config('services.taylors.cms.port');
        $this->user = config('services.taylors.cms.user');
        $this->password = config('services.taylors.cms.password');
        $this->sid = config('services.taylors.cms.sid');
        $this->location = "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = {$this->host})(PORT = {$this->port}))(CONNECT_DATA = (SID = {$this->sid})))";
    }

    protected function setConnection()
    {
        $this->setConfig();
        if (config('app.env') == 'staging' || config('app.env') == 'production') {
            $this->conn = oci_connect($this->user, $this->password, $this->location);
        }
    }
}
