<?php

namespace App\Services\Integration\Taylors;

use App\Traits\Services\BaseServiceTrait;
use Illuminate\Support\Facades\Http;

class TopasService
{
    use BaseServiceTrait;

    public static function auth($email, $password)
    {
        $password = base64_encode($password);
        $token = config('services.taylors.topas.token');
        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ])->withToken($token)
            ->post(config('services.taylors.topas.endpoint'), [
                'email' => $email,
                'password' => $password,
            ]);
        if ($response->status() == 200) {
            return [true, null];
        } elseif ($response->status() == 422) {
            $message = __('auth.failed');
            return [false, $message];
        } elseif ($response->status() == 400) {
            $content = json_decode($response->body(), true);
            $errorCode = $content['errors']['code'] ?? null;
            if ($errorCode == 'password_incorrect') {
                $message = __('auth.password_incorrect');
            } else {
                $message = __('auth.failed');
            }
            return [false, $message];
        } else {
            $message = __('auth.failed');
            return [false, $message];
        }
    }
}
