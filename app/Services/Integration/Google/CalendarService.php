<?php

namespace App\Services\Integration\Google;

use App\Services\Integration\Google\GoogleService;

class CalendarService extends GoogleService
{
    public $accessToken;

    public function __construct($accessToken = null)
    {
        $this->accessToken = $accessToken;
    }

    public static function insertEvent($event, $accessToken = null)
    {
        $service = (new self($accessToken))->calendarService();
        $calendarEvent = new \Google_Service_Calendar_Event($event);
        $calendarId = 'primary';
        return $service->events->insert($calendarId, $calendarEvent);
    }
}
