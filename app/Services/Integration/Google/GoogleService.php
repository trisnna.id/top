<?php

namespace App\Services\Integration\Google;

class GoogleService
{

    public $accessToken;

    public function googleClient()
    {
        $client = new \Google_Client();
        $client->setApplicationName(config('app.name'));
        $client->setDeveloperKey(config('services.google.calendar.api_key'));
        $client->setClientId(config('services.google.calendar.client_id'));
        $client->setClientSecret(config('services.google.calendar.client_secret'));
        if (!empty($this->accessToken ?? null)) {
            $client->setAccessToken($this->accessToken);
        }
        return $client;
    }

    public function calendarService()
    {
        $client = $this->googleClient();
        $client->setScopes(array('https://www.googleapis.com/auth/calendar.events'));
        return new \Google_Service_Calendar($client);
    }
}
