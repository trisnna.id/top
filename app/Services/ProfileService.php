<?php

namespace App\Services;

use App\DataTables\Profile\Notification\NotificationDt;
use App\Http\Requests\Profile\StoreRequest;
use App\Models\Notification;
use App\Traits\Services\BaseServiceTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

class ProfileService
{
    use BaseServiceTrait;

    public function index(Request $request, $auth)
    {
        $title = __('Profile');
        $student = $auth->student ?? null;
        $staff = $auth->staff ?? null;
        $user = $auth ?? null;
        return view('profile.index', compact('title', 'student', 'staff', 'user'));
    }

    public function store($input, $auth)
    {
        if ((empty($input['logo_remove'] ?? null) && empty($input['avatar'] ?? null)) || (!empty($input['avatar'] ?? null))) {
            $this->validateRequest(new StoreRequest(), $input);
        }
        if (!empty($input['avatar'] ?? null)) {
            $media = $auth->addMedia($input['avatar'])->toMediaCollection('avatar');
            Image::load($media->getPath())->fit(Manipulations::FIT_CROP, setting('file.profile_avatar_width'), setting('file.profile_avatar_height'))->save();
        } elseif (!empty($input['logo_remove'] ?? null)) {
            $avatars = $auth->getMedia('avatar');
            if (!empty($avatars ?? null)) {
                $avatars->first()->delete();
            }
        }
        $message = 'Update Successfully';
        return $this->swalResponse($this->swalData(
            message: $message,
            reload: true,
        ));
    }

    public function indexNotification(Request $request, $auth)
    {
        $title = __('Notification');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndexNotification($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndexNotification($request, $auth),
            compact('title')
        );
        return view('profile.notification.index', $compact);
    }

    protected function dataTableIndexNotification(Request $request, $auth)
    {
        $notificationDt = new NotificationDt($request, $auth);
        if ($request->get('table') == 'notificationDt') {
            return $notificationDt->render('');
        }
        return compact(
            'notificationDt',
        );
    }

    public function updateNotificationRead($input, $auth, $id)
    {
        $notification = Notification::where([
            'notifiable_type' => $auth::class,
            'notifiable_id' => $auth->id,
        ])->findOrFail($id);
        $notification->update([
            'read_at' => Carbon::now()
        ]);
        return $this->swalResponse($this->swalData(
            message: 'Mark as Read.',
            redirect: $input['url'],
        ));
    }
}
