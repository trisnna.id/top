<?php declare(strict_types=1);

namespace App\Services;

final class SettingsService
{
    /**
     * Get settings page view
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    public static function getView()
    {
        // @todo return view base on user role

        return view('admin.settings');
    }
}
