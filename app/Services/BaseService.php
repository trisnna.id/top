<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class BaseService
{
    // /**
    //  * Default input for DataTables
    //  * @param type $query
    //  * @param type $args
    //  */
    // public function inputDataTables(&$input)
    // {
    //     $input['colvis'] = !empty($input['colvis'] ?? []) ? json_decode(
    //         $input['colvis'],
    //         true
    //     ) : null;
    //     if (!empty($input['daterange'] ?? []) && $input['daterange'] != 'All') {
    //         $daterange = explode(' - ', $input['daterange']);
    //         $input['daterange'] = [
    //             Carbon::createFromFormat(
    //                 config('app.date_format.php') . ' ' . config('app.time_format.php'),
    //                 $daterange[0]
    //             ),
    //             Carbon::createFromFormat(
    //                 config('app.date_format.php') . ' ' . config('app.time_format.php'),
    //                 $daterange[1]
    //             ),
    //         ];
    //     } else {
    //         $input['daterange'] = null;
    //     }
    // }

    public function responseSwal($response, $data)
    {
        if ($response) {
            return response()->json([
                    'type' => $data['type'],
                    'popup' => $data['popup'] ?? 'toastr',
                    'callback' => $data['callback'] ?? null,
                    'url' => $data['url'] ?? null,
                    'title' => $data['title'] ?? '',
                    'message' => $data['message'] ?? '',
                    'timer' => $data['timer'] ?? null,
                    'data' => $data['data'] ?? null,
            ]);
        } else {
            return response()->json(['message' => __('oops_something_wrong')],
                    500);
        };
    }
}
