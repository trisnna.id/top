<?php

namespace App\Services;

use App\Constants\OrientationStatusConstant;
use App\Constants\OrientationTypeConstant;
use App\DataTables\Orientation\AttachmentDt;
use App\DataTables\Orientation\ComplementaryDt;
use App\DataTables\Orientation\CompulsoryCoreDt;
use App\DataTables\Orientation\OrientationDt;
use App\DataTables\Orientation\OrientationRecordingDt;
use App\DataTables\Orientation\TimelineDt;
use App\Exports\Orientation\OrientationExport;
use App\Http\Requests\Orientation\UpsertAttendanceRequest;
use App\Models\Activity;
use App\Models\Orientation;
use App\Models\OrientationDate;
use App\Models\OrientationFilter;
use App\Models\Student;
use App\Services\Admin\OrientationAttendanceService;
use App\Services\Admin\OrientationRsvpService;
use App\Services\Admin\StudentPointService;
use App\Traits\Services\BaseServiceTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class OrientationService
{
    use BaseServiceTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new Orientation();
    }

    public function index(Request $request, $auth)
    {
        $this->debug($request, __METHOD__);
        $title = __('Timetable');
        $instance['orientation'] = [
            'swal' => [
                'id' => 'orientation',
                'settings' => [
                    'title' => 'Details',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                ],
            ]
        ];
        $instance['timelineIndicator'] = [
            'swal' => [
                'id' => 'timelineIndicator',
                'settings' => [
                    'title' => 'Indicator',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                ],
            ]
        ];
        $instance['attendance'] = [
            'swal' => [
                'id' => 'attendance',
                'settings' => [
                    'title' => "Scan Attendance",
                    'showConfirmButton' => false,
                ],
                'axios' => [
                    'url' => route('orientations.upsert-attendance'),
                    'method' => 'put'
                ]
            ],
        ];
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        } elseif (!empty($request->get('calendar'))) {
            return $this->calendarIndex($request, $auth, $instance);
        }
        $count['complementaryActivity'] = (new ComplementaryDt($request, $auth))->query()->count() ?? 0;
        $count['compulsoryCoreActivity'] = (new CompulsoryCoreDt($request, $auth))->query()->count() ?? 0;
        $student = $auth->student;
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title', 'count', 'instance', 'student')
        );
        return view('orientation.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        if (!empty($request->orientation_id ?? null)) {
            $orientation = $this->model->findOrFailByUuid($request->orientation_id);
        } else {
            $orientation = $this->model;
        }
        $complementaryDt = new ComplementaryDt($request, $auth);
        $compulsoryCoreDt = new CompulsoryCoreDt($request, $auth);
        $orientationRecordingDt = new OrientationRecordingDt($request, $auth, $orientation);
        $attachmentDt = new AttachmentDt($request, $auth, $orientation);
        $timelineDt = new TimelineDt($request, $auth);
        $orientationDt = new OrientationDt($request, $auth);
        if ($request->get('table') == 'complementaryDt') {
            return $complementaryDt->render('');
        } elseif ($request->get('table') == 'compulsoryCoreDt') {
            return $compulsoryCoreDt->render('');
        } elseif ($request->get('table') == 'orientationRecordingDt') {
            return $orientationRecordingDt->render('');
        } elseif ($request->get('table') == 'timelineDt') {
            return $timelineDt->render('');
        } elseif ($request->get('table') == 'attachmentDt') {
            return $attachmentDt->render('');
        } elseif ($request->get('table') == 'orientationDt') {
            return $orientationDt->render('');
        }
        return compact(
            'complementaryDt',
            'compulsoryCoreDt',
            'orientationRecordingDt',
            'attachmentDt',
            'timelineDt',
            'orientationDt',
        );
    }

    public function index2(Request $request, $auth)
    {
        $this->debug($request, __METHOD__);
        $title = __('Orientation');
        $instance['orientation'] = [
            'swal' => [
                'id' => 'orientation',
                'settings' => [
                    'title' => 'Details',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                ],
            ]
        ];
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $student = $auth->student;
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title', 'instance', 'student')
        );
        return view('orientation.index2', $compact);
    }

    public function exportTimetable(Request $request, $auth)
    {
        $student = $auth->student;
        if (empty($student ?? null)) {
            $student = Student::findOrFailByUuid($request->student_id);
        }
        if (!empty($student ?? null)) {
            $media = $student->getFirstMedia('orientation-timetable');
            $timestamps = Carbon::now();
            if (!empty($media) && file_exists($media->getPath())) {
                return response()->download($media->getPath(), "Orientation Timetable {$timestamps}.pdf");
            } else {
                $media = $this->generateTimetable($request, $auth, $student);
                if (!empty($media ?? null)) {
                    return response()->download($media->getPath(), "Orientation Timetable {$timestamps}.pdf");
                } else {
                    abort(404);
                }
            }
        } else {
            abort(404);
        }
    }

    public function generateTimetable($request, $auth, $student)
    {
        $title = __('Orientation Timetable');
        $orientationDateSubQuery = OrientationDate::query()
            ->select([
                'orientation_dates.orientation_id',
                'orientation_dates.date',
            ]);
        $orientationQuery = (new TimelineDt($request, $auth))->query();
        $orientationQuery->leftJoinSub($orientationDateSubQuery, 'orientation_dates', function ($query) {
            $query->on('orientations.id', '=', 'orientation_dates.orientation_id');
        })
            ->addSelect('orientation_dates.date as timetable_date');
        $query = $orientationQuery->getQuery();
        $groupOrientations = $orientationQuery->setQuery($query)
            ->reorder('timetable_date', 'asc')
            ->orderBy('start_time', 'asc')
            ->get()
            ->groupBy('timetable_date');
        $data = compact('title', 'groupOrientations');
        $footerHtml = view()->make('orientation.timetable-footer')->render();
        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->setOption('enable-javascript', true);
        $pdf->setPaper('a4');
        $pdf->setOption('javascript-delay', 3000);
        $pdf->setOption('disable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);
        $pdf->setOption('margin-top', '12.7mm');
        $pdf->setOption('margin-left', '12.7mm');
        $pdf->setOption('margin-right', '12.7mm');
        $pdf->setOption('margin-bottom', '12.7mm');
        $pdf->setOption('encoding', 'UTF-8');
        $output = $pdf->loadView('orientation.timetable', $data)->setOption('footer-html', $footerHtml)->output();
        $name = Str::slug($title . "-{$student->id_number}") . '.pdf';
        $disk = Storage::disk('public');
        if ($disk->put($name, $output)) {
            $filename = Str::slug($title);
            $student->addMedia($disk->path($name))->usingName($filename)->usingFileName($filename . '.pdf')->toMediaCollection('orientation-timetable');
            $media = $student->refresh()->getFirstMedia('orientation-timetable');
        }
        return $media;
    }

    public function queryCalendarIndex($request, $auth)
    {
        return (new TimelineDt($request, $auth))->query()
            ->where(function ($query) {
                $query->where('orientation_rsvps.is_go', 1)->orWhere('orientations.orientation_type_id', 1);
            });
    }

    /**
     * Related to app\Services\Admin\OrientationService.php @calendarIndex
     * Related to app\DataTables\Orientation\TimelineDt.php @query
     */
    protected function calendarIndex(Request $request, $auth, $instance)
    {
        $orientations = $this->queryCalendarIndex($request, $auth)
            ->get()
            ->map(function ($orientation) use ($auth, $instance) {
                $calendar['id'] = $orientation->uuid;
                $calendar['title'] = $orientation->name;
                $calendar['start'] = "{$orientation->start_date} {$orientation->start_time}";
                $calendar['end'] = "{$orientation->end_date} {$orientation->end_time}";
                $calendar['extendedProps'] = [
                    'data-swal' => $instance['orientation']['swal'],
                    'data-data' => $this->dataSwalOrientation($orientation, $auth->student),
                ];
                $calendar['className'] = OrientationTypeConstant::getAvailableOptions('css_calendar')[$orientation->orientation_type_id];
                return $calendar;
            });
        return $this->swalResponse($this->swalData(
            message: "Fetch Successfully",
            data: $orientations,
        ));
    }

    // Scan attendance
    public function upsertAttendance($input, $auth)
    {
        $this->validateRequest(new UpsertAttendanceRequest(), $input);
        $input['attendance_qrcode'] = json_decode($input['attendance_qrcode'], true);
        if (empty($input['attendance_qrcode']['orientation_id'] ?? null)) {
            return $this->throwResponseException(['Not a valid format.']);
        }
        if ($input['attendance_qrcode']['orientation_id'] !== $input['orientation_id']) {
            return $this->throwResponseException(['Orientation Activity not match.']);
        }
        $orientation = Orientation::findOrFailByUuid($input['orientation_id']);
        if (!$orientation->rule_can_scan_attendance) {
            return $this->throwResponseException(['Cannot scan attendance at this Moment.']);
        }
        $input['orientation_id'] = $orientation->id;
        $input['student_id'] = Student::findOrFailByUuid($input['student_id'])->id;
        $input['point'] = $orientation->point_attendance;
        $input['point_attendance'] = $orientation->point_attendance;
        $orientationAttendance = DB::transaction(function () use ($input, $auth) {
            $orientationAttendance = (new OrientationAttendanceService())->updateOrCreate($input, $auth);
            (new StudentPointService())->updateStudentPoint($auth->student);
            return $orientationAttendance;
        });
        return $this->swalResponse($this->swalData(
            message: "Update Successfully. Please Rate this Activity.",
            function: "window.modules.orientation.index.clickSwalRate('table#timelineDt','{$orientation->uuid}')",
        ));
    }

    // Rate
    public function updateRate($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $orientation->load(['orientationAttendances' => function ($query) use ($auth) {
            $query->where('student_id', $auth->student->id);
        }]);
        if ($orientation->orientationAttendances->count()) {
            $input['student_id'] = $auth->student->id;
            $input['orientation_id'] = $orientation->id;
            $input['point'] = $orientation->orientationAttendances->first()->point_attendance + $orientation->point_rating;
            $input['point_attendance'] = $orientation->orientationAttendances->first()->point_attendance;
            $input['point_rating'] = $orientation->point_rating;
            $orientationAttendance = DB::transaction(function () use ($input, $auth) {
                $orientationAttendance = (new OrientationAttendanceService())->updateOrCreate($input, $auth);
                (new StudentPointService())->updateStudentPoint($auth->student);
                return $orientationAttendance;
            });
        } else {
            return $this->throwResponseException(['Attendance did not found']);
        }
        return $this->responseUpdate();
    }

    public function upsertRsvpYes($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $input['is_go'] = true;
        $this->defaultRsvpInput($input, $auth, $orientation);
        $orientationRsvp = DB::transaction(function () use ($input, $auth) {
            $orientationRsvp = (new OrientationRsvpService())->updateOrCreate($input, $auth);
            return $orientationRsvp;
        });
        return $this->responseUpdate();
    }

    public function upsertRsvpNo($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $input['is_go'] = false;
        $this->defaultRsvpInput($input, $auth, $orientation);
        $orientationRsvp = DB::transaction(function () use ($input, $auth) {
            $orientationRsvp = (new OrientationRsvpService())->updateOrCreate($input, $auth);
            return $orientationRsvp;
        });
        return $this->responseUpdate();
    }

    public function upsertRsvp($input, $auth, $uuid)
    {
        $orientation = $this->model->findOrFailByUuid($uuid);
        $this->defaultRsvpInput($input, $auth, $orientation);
        $orientationRsvp = DB::transaction(function () use ($input, $auth) {
            $orientationRsvp = (new OrientationRsvpService())->updateOrCreate($input, $auth);
            return $orientationRsvp;
        });
        return $this->responseUpdate();
    }

    protected function defaultRsvpInput(&$input, $auth, $orientation)
    {
        $input['is_rsvp'] = $orientation->is_rsvp;
        $input['rsvp_capacity'] = $orientation->rsvp_capacity;
        $input['rsvp_current_capacity'] = $orientation->count_rsvp_current_capacity;
        $input['orientation_id'] = $orientation->id;
        $input['student_id'] = $auth->student->id ?? null;
    }

    public function fixDataTotalStudent()
    {
        $orientationFilterSubQueryByIntake = OrientationFilter::select(['orientation_id', 'value'])->where('name', 'intake');
        $orientationFilterSubQueryByLevelStudy = OrientationFilter::select(['orientation_id', 'value'])->where('name', 'level_study');
        $orientationFilterSubQueryByMobility = OrientationFilter::select(['orientation_id', 'value'])->where('name', 'mobility');
        $orientationFilterSubQueryByProgramme = OrientationFilter::select(['orientation_id', 'value'])->where('name', 'programme');
        $orientationFilterSubQueryBySchool = OrientationFilter::select(['orientation_id', 'value'])->where('name', 'school');
        $orientationFilterSubQueryByFaculty = OrientationFilter::select(['orientation_id', 'value'])->where('name', 'faculty');
        $studentSubQueryByFilters = Student::query()
            ->select(DB::raw('count(*)'))
            ->whereJsonContains(DB::raw('orientationFilterSubQueryByIntake.value'), DB::raw("CONCAT('[','\"', students.intake_id,'\"',']')"))
            ->whereJsonContains(DB::raw('orientationFilterSubQueryByMobility.value'), DB::raw("CONCAT('[','\"', students.mobility_id,'\"',']')"))
            ->whereJsonContains(DB::raw('orientationFilterSubQueryByProgramme.value'), DB::raw("CONCAT('[','\"', students.programme_id,'\"',']')"))
            ->whereJsonContains(DB::raw('orientationFilterSubQueryBySchool.value'), DB::raw("CONCAT('[','\"', students.school_id,'\"',']')"))
            ->whereJsonContains(DB::raw('orientationFilterSubQueryByFaculty.value'), DB::raw("CONCAT('[','\"', students.faculty_id,'\"',']')"))
            ->whereJsonContains(DB::raw('orientationFilterSubQueryByLevelStudy.value'), DB::raw("CONCAT('[','\"', students.level_study_id,'\"',']')"));
        $orientations = Orientation::query()
            ->select([
                'orientations.*',
            ])
            ->addSelect(DB::raw("({$studentSubQueryByFilters->toSql()}) as new_total_student"))
            ->leftJoinSub($orientationFilterSubQueryByIntake, 'orientationFilterSubQueryByIntake', function ($query) {
                $query->on('orientations.id', '=', 'orientationFilterSubQueryByIntake.orientation_id');
            })
            ->leftJoinSub($orientationFilterSubQueryByLevelStudy, 'orientationFilterSubQueryByLevelStudy', function ($query) {
                $query->on('orientations.id', '=', 'orientationFilterSubQueryByLevelStudy.orientation_id');
            })
            ->leftJoinSub($orientationFilterSubQueryByMobility, 'orientationFilterSubQueryByMobility', function ($query) {
                $query->on('orientations.id', '=', 'orientationFilterSubQueryByMobility.orientation_id');
            })
            ->leftJoinSub($orientationFilterSubQueryByProgramme, 'orientationFilterSubQueryByProgramme', function ($query) {
                $query->on('orientations.id', '=', 'orientationFilterSubQueryByProgramme.orientation_id');
            })
            ->leftJoinSub($orientationFilterSubQueryBySchool, 'orientationFilterSubQueryBySchool', function ($query) {
                $query->on('orientations.id', '=', 'orientationFilterSubQueryBySchool.orientation_id');
            })
            ->leftJoinSub($orientationFilterSubQueryByFaculty, 'orientationFilterSubQueryByFaculty', function ($query) {
                $query->on('orientations.id', '=', 'orientationFilterSubQueryByFaculty.orientation_id');
            })
            ->where('status_id', OrientationStatusConstant::APPROVED)
            ->get();
        foreach ($orientations as $orientation) {
            $orientation->update([
                'total_student' => $orientation->new_total_student
            ]);
        }
    }
}
