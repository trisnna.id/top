<?php

namespace App\Services;

use App\DataTables\Faq\FaqDt;
use App\Models\Faq;
use App\Services\BaseService;
use Illuminate\Http\Request;

class FaqService extends BaseService
{
    protected $model;
    protected $viewIndex = 'resource.index';

    public function __construct()
    {
        $this->model = new Faq();
    }

    public function index(Request $request, $auth)
    {
        $title = __('FAQ');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('faq.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $faqDt = new FaqDt($request, $auth);
        if ($request->get('table') == 'faqDt') {
            return $faqDt->render('');
        }
        return compact(
            'faqDt',
        );
    }
}
