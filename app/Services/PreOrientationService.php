<?php

namespace App\Services;

use App\Constants\ActivityTypeConstant;
use App\DataTables\Dashboard\ComplementaryDt;
use App\DataTables\Dashboard\CompulsoryCoreDt;
use App\DataTables\PreOrientation\PreOrientationDt;
use App\Models\Activity;
use App\Models\PreOrientation;
use App\Services\BaseService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PreOrientationService extends BaseService
{
    protected $model;

    private PreOrientation $entity;

    public function __construct()
    {
        $this->model = new Activity();
        $this->entity = app()->make(PreOrientation::class);
    }

    public function index(Request $request, $auth)
    {
        $title = __('Pre-orientation');
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view('pre-orientation.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $preOrientationDt = new PreOrientationDt($request, $auth);
        if ($request->get('table') == 'preOrientationDt') {
            return $preOrientationDt->render('');
        }
        return compact(
            'preOrientationDt'
        );
    }
}
