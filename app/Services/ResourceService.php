<?php

namespace App\Services;

use App\DataTables\Arrival\ArrivalDt;
use App\DataTables\Resource\ResourceDt;
use App\Models\Resource;
use App\Services\BaseService;
use Illuminate\Http\Request;

class ResourceService extends BaseService
{
    protected $model;
    protected $titleIndex = 'Resources';
    protected $titleShow = 'Resources Detail';
    protected $viewIndex = 'resource.index';

    public function __construct()
    {
        $this->model = new Resource();
    }

    public function index(Request $request, $auth)
    {
        $title = __($this->titleIndex);
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        }
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('title')
        );
        return view($this->viewIndex, $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        $resourceDt = new ResourceDt($request, $auth);
        $arrivalDt = new ArrivalDt($request, $auth);
        if ($request->get('table') == 'resourceDt') {
            return $resourceDt->render('');
        } elseif ($request->get('table') == 'arrivalDt') {
            return $arrivalDt->render('');
        }
        return compact(
            'arrivalDt',
            'resourceDt',
        );
    }

    public function show(Request $request, $auth, $uuid)
    {
        $title = __($this->titleShow);
        $resource = $this->model->findOrFailByUuid($uuid);
        $attachments = collect();
        foreach ($resource->getMedia('media') as $file) {
            $attachment = collect([
                'type' => 'file',
                'name' => $file->getCustomProperty('name') ?? $file->file_name,
                'description' => $file->getCustomProperty('description') ?? '',
                'source' => 'media',
                'updated_at' => $file->updated_at,
                'url' => $file->original_url,
                'instance' => $file
            ]);
            $attachments = $attachments->push($attachment);
        }
        foreach ($resource->getMedia('documents') as $file) {
            $attachment = collect([
                'type' => 'file',
                'name' => $file->file_name,
                'source' => 'documents',
                'updated_at' => $file->updated_at,
                'url' => $file->original_url,
                'instance' => $file
            ]);
            $attachments = $attachments->push($attachment);
        }
        foreach ($resource->resourceLinks->where('collection_name', 'media') as $link) {
            $attachment = collect([
                'type' => 'link',
                'name' => $link->name,
                'description' => $link->description,
                'source' => 'media',
                'updated_at' => $link->updated_at,
                'url' => $link->url,
                'instance' => $link
            ]);
            $attachments = $attachments->push($attachment);
        }
        foreach ($resource->resourceLinks->where('collection_name', 'links') as $link) {
            $attachment = collect([
                'type' => 'link',
                'name' => $link->name,
                'source' => 'links',
                'updated_at' => $link->updated_at,
                'url' => $link->url,
                'instance' => $link
            ]);
            $attachments = $attachments->push($attachment);
        }
        $attachments = $attachments->sortBy('title');
        $attachmentLinks = $attachments->where('source', 'links');
        $attachmentMedias = $attachments->where('source', 'media');
        $attachmentDocuments = $attachments->where('source', 'documents');
        return view('resource.show', compact('title', 'resource', 'attachmentLinks', 'attachmentMedias', 'attachmentDocuments'));
    }

}
