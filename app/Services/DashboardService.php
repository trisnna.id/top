<?php

namespace App\Services;

use App\DataTables\Dashboard\PreEngagementDt;
use App\DataTables\Dashboard\ResourceDt;
use App\DataTables\Faq\FaqDt;
use App\DataTables\Orientation\AttachmentDt;
use App\DataTables\Orientation\ComplementaryDt;
use App\DataTables\Orientation\CompulsoryCoreDt;
use App\DataTables\Orientation\OrientationRecordingDt;
use App\Models\Orientation;
use App\Traits\Services\BaseServiceTrait;
use Illuminate\Http\Request;

class DashboardService
{
    use BaseServiceTrait;

    public function index(Request $request, $auth)
    {
        $this->debug($request, __METHOD__);
        $user = $auth;
        $title = __('Dashboard');
        $instance['orientation'] = [
            'swal' => [
                'id' => 'orientation',
                'settings' => [
                    'title' => 'Details',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                ],
            ]
        ];
        $instance['attendance'] = [
            'swal' => [
                'id' => 'attendance',
                'settings' => [
                    'title' => "Scan Attendance",
                    'showConfirmButton' => false,
                ],
                'axios' => [
                    'url' => route('orientations.upsert-attendance'),
                    'method' => 'put'
                ]
            ],
        ];
        if (!empty($request->get('table'))) {
            return $this->dataTableIndex($request, $auth);
        } elseif (!empty($request->get('calendar'))) {
            return $this->calendarIndex($request, $auth, $instance);
        }
        $instance['resource'] = [
            'swal' => [
                'id' => 'resource',
                'settings' => [
                    'title' => 'Resources',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                    'customClass' => [
                        'popup' => "w-90",
                    ],
                ],
            ]
        ];
        $instance['faq'] = [
            'swal' => [
                'id' => 'faq',
                'settings' => [
                    'title' => 'FAQ',
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                    'customClass' => [
                        'popup' => "w-90",
                    ],
                ],
            ]
        ];
        $instance['stepbystep'] = [
            'swal' => [
                'id' => 'stepbystep',
                'settings' => [
                    'showCancelButton' => false,
                    'showConfirmButton' => false,
                    'customClass' => [
                        'popup' => "w-90",
                    ],
                ],
            ]
        ];
        $count['complementaryActivity'] = (new ComplementaryDt($request, $auth))->query()->count() ?? 0;
        $count['compulsoryCoreActivity'] = (new CompulsoryCoreDt($request, $auth))->query()->count() ?? 0;
        $student = $auth->student;
        $compact = array_merge(
            $this->dataTableIndex($request, $auth),
            compact('user', 'title', 'instance', 'count', 'student')
        );
        return view('dashboard.index', $compact);
    }

    protected function dataTableIndex(Request $request, $auth)
    {
        if (!empty($request->orientation_id ?? null)) {
            $orientation = Orientation::findOrFailByUuid($request->orientation_id);
        } else {
            $orientation = new Orientation();
        }
        if (!empty($request->search['value'] ?? [])) {
            $request->request->add(['defaultFaqDtEmpty' => false]);
        } else {
            $request->request->add(['defaultFaqDtEmpty' => true]);
        }
        $faqDt = new FaqDt($request, $auth);
        $resourceDt = new ResourceDt($request, $auth);
        $complementaryDt = new ComplementaryDt($request, $auth);
        $compulsoryCoreDt = new CompulsoryCoreDt($request, $auth);
        $orientationRecordingDt = new OrientationRecordingDt($request, $auth, $orientation);
        $attachmentDt = new AttachmentDt($request, $auth, $orientation);
        if ($request->get('table') == 'faqDt') {
            return $faqDt->render('');
        } elseif ($request->get('table') == 'resourceDt') {
            return $resourceDt->render('');
        } elseif ($request->get('table') == 'complementaryDt') {
            return $complementaryDt->render('');
        } elseif ($request->get('table') == 'compulsoryCoreDt') {
            return $compulsoryCoreDt->render('');
        } elseif ($request->get('table') == 'orientationRecordingDt') {
            return $orientationRecordingDt->render('');
        } elseif ($request->get('table') == 'attachmentDt') {
            return $attachmentDt->render('');
        }
        return compact(
            'faqDt',
            'resourceDt',
            'complementaryDt',
            'compulsoryCoreDt',
            'orientationRecordingDt',
            'attachmentDt',
        );
    }
}
