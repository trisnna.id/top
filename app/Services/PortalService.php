<?php

namespace App\Services;

use App\Services\BaseService;
use Illuminate\Http\Request;

class PortalService extends BaseService
{

    public function index(Request $request, $auth)
    {
        $title = __('Dashboard');
        $instance['forgotPassword'] = [
            'swal' => [
                'id' => 'forgotPassword',
                'settings' => [
                    'showCancelButton' => false,
                    'confirmButtonText' => 'Submit',
                    'customClass' => [
                        'popup' => "shc-mxh-70vh",
                        'actions' => "mt-0 w-100",
                        'confirmButton' => "btn btn-primary",
                    ],
                ],
            ],
        ];
        $instance['help'] = [
            'swal' => [
                'id' => 'help',
                'settings' => [
                    'showConfirmButton' => false,
                    'showCancelButton' => false,
                    'customClass' => [
                        'popup' => "shc-mxh-70vh",
                        'actions' => "mt-0 w-100",
                        'confirmButton' => "btn btn-primary",
                    ],
                ],
            ],
        ];
        $instance['login'] = [
            'swal' => [
                'id' => 'login',
                'settings' => [
                    'showCancelButton' => false,
                    'confirmButtonText' => 'Continue',
                    'customClass' => [
                        'popup' => "shc-mxh-70vh",
                        'actions' => "mt-0 w-100",
                        'confirmButton' => "btn btn-red",
                    ],
                    // remove sweetalert2 animation
                    'showClass' => [
                        'popup' => '',
                        'backdrop' => 'swal2-noanimation',
                        'icon' => '',
                    ],
                    'hideClass' => [
                        'popup' => '',
                        'backdrop' => 'swal2-noanimation',
                        'icon' => '',
                    ],
                ],
            ],
        ];
        return view('portal.index', compact('title', 'instance'));
    }
}
