<?php declare(strict_types=1);

namespace App\ValueObjects;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

abstract class ValueObject implements Arrayable, Jsonable
{
    /**
     * Create a new value object instance
     *
     * @param   array  $attributes
     * @return  void
     */
    public function __construct(array $attributes = [])
    {
        foreach (array_keys($attributes) as $key) {
            if ($this->hasProperty($key)) {
                $this->{$key} = $attributes[$key];
            }
        }
    }

    /**
     * Get the attribute value
     *
     * @param   string  $name
     * @return  mixed
     */
    public function __get(string $name)
    {
        if ($this->hasProperty($name)) {
            return $this->{$name};
        }

        return null;
    }

    /**
     * Set attribute value
     *
     * @param   string      $name
     * @param   string|null $value
     * @return  void
     */
    public function __set(string $name, ?string $value): void
    {
        if ($this->hasProperty($name)) {
            $this->{$name} = $value;
        }
    }

    /**
     * Get the instance as an array.
     *
     * @return array<TKey, TValue>
     */
    abstract public function toArray(): array;

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0): string
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * Check if the given property name exist in the value object instance
     *
     * @param   string  $propertyName
     * @return  bool
     */
    private function hasProperty(string $propertyName): bool
    {
        return property_exists($this, $propertyName);
    }
}
