<?php declare(strict_types=1);

namespace App\ValueObjects;

class Faq extends ValueObject
{
    /**
     * The faq question
     *
     * @var string
     */
    protected string $question;

    /**
     * The faq answer
     *
     * @var string
     */
    protected string $answer;

    /**
     * Get the instance as an array.
     *
     * @return array<TKey, TValue>
     */
    public function toArray(): array
    {
        return [
            'question' => $this->question,
            'answer' => $this->answer
        ];
    }
}
