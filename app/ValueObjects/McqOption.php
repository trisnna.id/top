<?php declare(strict_types=1);

namespace App\ValueObjects;

use Illuminate\Support\Str;

class McqOption extends ValueObject
{
    /**
     * The option key in alphabetical
     *
     * @var string
     */
    protected string $key = '';

    /**
     * The option value
     *
     * @var string
     */
    protected string $value = '';

    /**
     * Determine if the current option is marked as correct answer
     *
     * @var bool
     */
    protected bool $markAsAnswer = false;

    /**
     * {@override}
     *
     * @param   array  $attributes
     * @return  void
     */
    public function __construct(array $attributes = [])
    {
        if (array_key_exists('key', $attributes)) {
            $attributes['markAsAnswer'] = $attributes['mark_as_answer'];
            unset($attributes['mark_as_answer']);
        } else {
            $attributes = $this->resolveProperties($attributes);
        }

        parent::__construct($attributes);
    }

    /**
     * Get formatted option key 
     *
     * @return string
     */
    public function getTitleKey(): string
    {
        return Str::title($this->key);
    }

    /**
     * Check if the current option instance is
     * marked as correct answer
     *
     * @return boolean
     */
    public function isCorrect(): bool
    {
        return $this->markAsAnswer === true;
    }

    /**
     * Get the instance as an array.
     *
     * @return array<TKey, TValue>
     */
    public function toArray(): array
    {
        return [
            'key' => $this->key,
            'value' => $this->value,
            'mark_as_answer' => $this->markAsAnswer
        ];
    }

    /**
     * Resolve the given attributes to class properties
     *
     * @param   array   $attributes
     * @return  array
     */
    private function resolveProperties(array $attributes): array
    {
        $properties = [];
        $keys = array_keys($attributes);

        $properties['key'] = $keys[0];
        $properties['value'] = $attributes[$keys[0]];
        if (array_key_exists('mark_as_answer', $attributes) && $attributes['mark_as_answer'] === 'on') {
            $properties['markAsAnswer'] = true;
        }

        return $properties;
    }
}
