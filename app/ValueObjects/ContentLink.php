<?php declare(strict_types=1);

namespace App\ValueObjects;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class ContentLink extends ValueObject
{
    /**
     * The text description of anchor tag
     *
     * @var     string
     */
    protected string $text;

    /**
     * The href value of anchor tag
     *
     * @var     string
     */
    protected string $link;

    /**
     * The thumbnail link
     *
     * @var     string|null
     */
    protected ?string $thumbnail;

    /**
     * The anchor tag element
     *
     * @var string
     */
    protected string $raw;

    /**
     * Get the instance as an array.
     *
     * @return array<TKey, TValue>
     */
    public function toArray(): array
    {
        return [
            'text' => $this->text,
            'link' => $this->link,
            'thumbnail' => $this->thumbnail,
            'raw' => $this->raw,
        ];
    }

    /**
     * Parse the HTML anchors tag and convert them to collection 
     *
     * @param   string  $content
     * @return  Collection
     */
    public static function parse(string $content): Collection
    {
        $links = Collection::make();
        $pattern = '/<a\s+[^>]?href="https?:\/\/(?:www\.)?youtube\.com\/watch\?v=[a-zA-Z0-9_-]+".?<\/a>/i';
        preg_match_all('/<a[^>]+href=\"(.*?)\"[^>]*>(.*?)<\/a>/', $content, $anchors);
        
        if (!empty($anchors)) {
            $anchorsTag = $anchors[0];
            $hrefValues = $anchors[1];
            $texts = $anchors[2];
            foreach ($anchorsTag as $i => $anchor) {
                // preg_match('~title[ ]*=[ ]*["\'](.*?)["\']~is', $anchor ,$title);
                $href = $hrefValues[$i];
                $attributes = [
                    'text' => $texts[$i],
                    'link' => $hrefValues[$i],
                    'thumbnail' => null,
                    'raw' => $anchor,
                ];

                if (Str::contains(strtolower($href), 'youtube')) {
                    $attributes['thumbnail'] = sprintf('https://img.youtube.com/vi/%s/0.jpg', explode('&', explode('=', $href)[1])[0]);
                    $links->push(new self($attributes));
                }
            }
        }

        return $links;
    }
}
