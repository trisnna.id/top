<?php declare(strict_types=1);

namespace App\ValueObjects\Resources;

final class Video extends Resource
{
    /**
     * Determine if the video is taken from external link
     *
     * @var bool
     */
    protected bool $isExternal = true;

    /**
     * {@override}
     *
     * @param   array  $attributes
     * @return  void
     */
    public function __construct(array $attributes = [])
    {
        if (array_key_exists('is_external', $attributes)) {
            $attributes['isExternal'] = $attributes['is_external'];
            unset($attributes['is_external']);
        }

        parent::__construct($attributes);

        if ($this->isExternal) {
            $this->source = new Link([
                'source' => $attributes['source']
            ]);
        } else {
            $this->source = new File([
                'source' => $attributes['source']
            ]);
        }
    }

    /**
     * Get the source value to string
     *
     * @return string
     */
    public function getSource(): string
    {
        return $this->source->source;
    }

    /**
     * Get the resource properties as associative array
     *
     * @return  array
     */
    protected function properties(): array
    {
        return [
            'source' => $this->getSource(),
            'is_external' => $this->isExternal,
        ];
    }
}
