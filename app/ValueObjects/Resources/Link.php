<?php declare(strict_types=1);

namespace App\ValueObjects\Resources;

final class Link extends Resource
{
    /**
     * The link text that will displayed on the page
     *
     * @var string
     */
    protected string $text;

    /**
     * Get the resource properties as associative array
     *
     * @return  array
     */
    protected function properties(): array
    {
        return [
            'text' => $this->text
        ];
    }
}
