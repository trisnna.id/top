<?php declare(strict_types=1);

namespace App\ValueObjects\Resources;

final class File extends Resource
{
    /**
     * The file name
     *
     * @var string
     */
    protected string $name;

    /**
     * Get file extension
     *
     * @return string
     */
    public function getExtension(): string
    {
        return pathinfo($this->source, PATHINFO_EXTENSION);
    }

    /**
     * Get the resource properties as associative array
     *
     * @return  array
     */
    protected function properties(): array
    {
        return [
            'name' => $this->name
        ];
    }
}
