<?php declare(strict_types=1);

namespace App\ValueObjects\Resources;

use App\ValueObjects\ValueObject;

abstract class Resource extends ValueObject
{
    /**
     * The source of resources, this can be link, path to file or filename
     *
     * @var mixed
     */
    protected mixed $source;

    /**
     * {@override}
     *
     * @return array
     */
    public function toArray(): array
    {
        return array_merge([
            'source' => $this->source,
        ], $this->properties());
    }

    /**
     * Get the resource properties as associative array
     *
     * @return  array
     */
    abstract protected function properties(): array;
}
