<?php

namespace App\Casts;

use App\Contracts\Entities\AnnouncementEntity;
use App\Contracts\Entities\PreOrientationEntity;
use App\Contracts\Entities\PreOrientationQuizEntity;
use App\Contracts\Entities\ResourceEntity;
use App\ValueObjects\ContentLink;
use App\ValueObjects\Faq;
use App\ValueObjects\McqOption;
use App\ValueObjects\Resources\File;
use App\ValueObjects\Resources\Link;
use App\ValueObjects\Resources\Video;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class Json implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function get($model, string $key, $value, array $attributes)
    {
        $value = json_decode($value, true);;

        if ($model instanceof PreOrientationEntity) {
            if ($key === PreOrientationEntity::FIELD_CONTENT_LINKS) {
                return Collection::make($value)->transform(function (array $attributes) {
                    return new ContentLink($attributes);
                });
            }
        }

        if ($model instanceof AnnouncementEntity) {
            if ($key === AnnouncementEntity::FIELD_CONTENT_LINKS) {
                return Collection::make($value)->transform(function (array $attributes) {
                    return new ContentLink($attributes);
                });
            }
        }

        if ($model instanceof PreOrientationQuizEntity) {
            if ($key === PreOrientationQuizEntity::FIELD_OPTIONS) {
                return Collection::make($value)->transform(function (array $attributes) {
                    return new McqOption($attributes);
                });
            }

            if (in_array($key, [PreOrientationQuizEntity::FIELD_ANSWERS, PreOrientationQuizEntity::FIELD_SETTINGS])) {
                return Collection::make($value);
            }
        }

        if ($model instanceof ResourceEntity) {
            if ($key === ResourceEntity::FIELD_VALUES) {
                switch ($model[ResourceEntity::FIELD_CATEGORY]) {
                    case ResourceEntity::CATEGORY_FAQ:
                        return new Faq($value);
                        
                    case ResourceEntity::CATEGORY_FILE:
                        return new File($value);

                    case ResourceEntity::CATEGORY_LINK:
                        return new Link($value);

                    case ResourceEntity::CATEGORY_VIDEO:
                        return new Video($value);
                }
            }
        }


        return $value;
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, string $key, $value, array $attributes)
    {
        if ($key === PreOrientationEntity::FIELD_CONTENT_LINKS) {
            return ContentLink::parse($value)->toJson();
        }
        
        if ($key === AnnouncementEntity::FIELD_CONTENT_LINKS) {
            return ContentLink::parse($value)->toJson();
        }

        if ($model instanceof PreOrientationQuizEntity) {
            if ($key === PreOrientationQuizEntity::FIELD_OPTIONS) {
                $value = Collection::make($value)->transform(function ($value) {
                    if ($value instanceof McqOption) {
                        return $value->toArray();
                    }

                    $option = new McqOption($value);
                    return $option->toArray();
                })->toArray();
            }

            if ($key === PreOrientationQuizEntity::FIELD_SETTINGS) {
                if (Arr::has($value, PreOrientationQuizEntity::SETTINGS_SHUFFLE)) {
                    $value[PreOrientationQuizEntity::SETTINGS_SHUFFLE] = true;
                } else {
                    $value[PreOrientationQuizEntity::SETTINGS_SHUFFLE] = false;
                }
            }
        }

        return json_encode($value);
    }
}
