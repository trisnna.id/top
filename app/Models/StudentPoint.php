<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;

class StudentPoint extends BaseModel
{
    use BaseModelTrait;

    public $hasUuid = true;
    protected $table = "student_points";
    protected $fillable = [
        'uuid',
        'student_id',
        'name',
        'point',
    ];
    protected $casts = [
        'uuid' => 'string',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
