<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use App\Vendors\Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use App\Vendors\Illuminate\Database\Eloquent\CustomCacheBuilder;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Intake extends BaseModel
{
    use BaseModelTrait;
    use Cachable;

    protected $cachePrefix = "intake";
    public $hasUuid = true;
    protected $fillable = [
        'uuid',
        'name',
        'is_active',
        'is_cms_active',
        'created_by',
        'updated_by',
        'is_orientation_activities_active',
        'orientation_activities_message',
        'is_timetable_active',
        'timetable_message',
    ];
    protected $casts = [
        'uuid' => 'string',
        'is_active' => 'boolean',
        'is_cms_active' => 'boolean',
        'is_orientation_activities_active' => 'boolean',
        'is_timetable_active' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function newEloquentBuilder($query)
    {
        if (!$this->isCachable()) {
            $this->isCachable = false;
            return new EloquentBuilder($query);
        }
        return new CustomCacheBuilder($query);
    }
}
