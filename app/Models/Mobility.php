<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use App\Vendors\Illuminate\Database\Eloquent\CustomCacheBuilder;
use App\Vendors\Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Mobility extends BaseModel
{
    use BaseModelTrait;
    use Cachable;

    protected $cachePrefix = "mobility";
    public $hasUuid = true;
    protected $table = 'mobilities';
    protected $fillable = [
        'uuid',
        'name',
        'is_active',
        'is_cms_active',
        'created_by',
        'updated_by'
    ];
    protected $casts = [
        'uuid' => 'string',
        'is_active' => 'boolean',
        'is_cms_active' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */


    /* =========================================================================
    * Scope
    * =========================================================================
    */


    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    public function getAttribute($key)
    {
        switch ($key) {
            case "code_name":
                $code = $this->getAttributeValue('code');
                $name = $this->getAttributeValue('name');
                return "{$code} - {$name}";
            default:
                return parent::getAttribute($key);
        }
    }
    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function newEloquentBuilder($query)
    {
        if (!$this->isCachable()) {
            $this->isCachable = false;
            return new EloquentBuilder($query);
        }
        return new CustomCacheBuilder($query);
    }
}
