<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class SurveyMigration extends BaseModel implements HasMedia
{

    use BaseModelTrait;
    use InteractsWithMedia;

    public $hasUuid = true;
    protected $fillable = [
        'file_name',
        'type_id',
        'total_record',
        'is_complete',
        'expired_at',
    ];
    protected $casts = [
        'uuid' => 'string',
        'is_complete' => 'boolean',
        'expired_at' => 'datetime',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('migration')
            ->singleFile();
    }
}
