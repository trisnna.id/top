<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use App\Vendors\Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use App\Vendors\Illuminate\Database\Eloquent\CustomCacheBuilder;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Mail extends BaseModel
{
    use BaseModelTrait;
    use Cachable;

    protected $cachePrefix = "mail";
    public $hasUuid = true;
    protected $table = 'mails';
    protected $fillable = [
        'uuid',
        'name',
        'title',
        'subject',
        'content',
        'notification',
        'is_default',
        'created_by',
        'updated_by'
    ];
    protected $casts = [
        'uuid' => 'string',
        'is_default' => 'boolean',
    ];
    protected $auditExclude = [
        'content',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    public function getAttribute($key)
    {
        switch ($key) {
            case "content_encode":
                return toJsonEncode($this->content ?? '');
            default:
                return parent::getAttribute($key);
        }
    }

    protected function content(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => toJsonEncode($value ?? ''),
        );
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function newEloquentBuilder($query)
    {
        if (!$this->isCachable()) {
            $this->isCachable = false;
            return new EloquentBuilder($query);
        }
        return new CustomCacheBuilder($query);
    }
}
