<?php

namespace App\Models;

use App\Contracts\Entities\Relations\HasPreOrientation;
use App\Traits\Models\BaseModelTrait;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Student extends BaseModel implements HasMedia
{

    use BaseModelTrait;
    use InteractsWithMedia;

    public $hasUuid = true;
    protected $fillable = [
        'name',
        'id_number',
        'nationality',
        'programme_id',
        // 'programme_code',
        'programme_name',
        'faculty_id',
        'faculty_name',
        'school_id',
        'school_name',
        'level_study_id',
        'level_of_study',
        'course_status',
        'intake_id',
        'intake_number',
        'contact_number',
        'taylors_email',
        'personal_email',
        'locality_id',
        'locality',
        'campus_id',
        'campus',
        'mobility_id',
        'mobility',
        'user_id',
        'flame_mentor_name',
        'intake_start_date',
        'recent_status_change_date',
        'total_points',
        'pre_orientation_percentage',
        'is_pre_orientation_survey_complete',
        'pre_survey_answer',
        'is_post_orientation_survey_complete',
        'post_survey_answer',
        'orientation_attendance_percentage',
        'survey_percentage',
        'achievement_percentage'
    ];
    protected $casts = [
        'uuid' => 'string',
        'is_pre_orientation_survey_complete' => 'boolean',
        'pre_survey_answer' => 'array',
        'is_post_orientation_survey_complete' => 'boolean',
        'post_survey_answer' => 'array',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function programme()
    {
        return $this->belongsTo(Programme::class);
    }

    public function mentor()
    {
        return $this->belongsTo(Mentor::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function studentPoints()
    {
        return $this->hasMany(StudentPoint::class);
    }

    public function intake()
    {
        return $this->belongsTo(Intake::class);
    }

    /**
     * The preorientations that related to the student
     *
     * @return  BelongsToMany
     */
    public function preOrientations(): BelongsToMany
    {
        return $this->belongsToMany(
            PreOrientation::class,
            StudentPreOrientation::TABLE,
            StudentPreOrientation::FIELD_STUDENT,
            HasPreOrientation::FIELD_PREORIENTATION
        )->using(StudentPreOrientation::class)->withPivot([
            'id',
            StudentPreOrientation::FIELD_COMPLETED,
            StudentPreOrientation::FIELD_QUIZ,
            StudentPreOrientation::FIELD_SCORE,
        ]);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */


    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    public function getAttribute($key)
    {
        switch ($key) {
            case "is_pre_orientation_survey_complete_yes_no":
                return $this->is_pre_orientation_survey_complete ? 'Yes' : 'No';
            case "is_post_orientation_survey_complete_yes_no":
                return $this->is_post_orientation_survey_complete ? 'Yes' : 'No';
            default:
                return parent::getAttribute($key);
        }
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('orientation-timetable')
            ->singleFile();
    }
}
