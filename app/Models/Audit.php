<?php

namespace App\Models;

use OwenIt\Auditing\Models\Audit as OwenItAudit;
use App\Traits\Models\BaseModelTrait;

class Audit extends OwenItAudit
{

    use BaseModelTrait;

    protected $fillable = [
        'user_type',
        'user_id',
        'student_id',
        'event',
        'auditable_type',
        'auditable_id',
        'old_values',
        'new_values',
        'url',
        'ip_address',
        'location',
        'user_agent',
        'tags'
    ];
    protected $casts = [
        'old_values' => 'json',
        'new_values' => 'json',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function user()
    {
        return $this->morphTo();
    }

    public function userByUserId()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function auditable()
    {
        return $this->morphTo();
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    public function getAttribute($key)
    {
        switch ($key) {
            case "created_at_readable":
            case "updated_at_readable":
                $key = explode("_readable", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? $value->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
            default:
                return parent::getAttribute($key);
        }
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
