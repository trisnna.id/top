<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;

class OrientationRsvp extends BaseModel
{
    use BaseModelTrait;

    protected $table = "orientation_rsvps";
    protected $fillable = [
        'orientation_id',
        'student_id',
        'is_go',
    ];
    protected $casts = [
        'is_go' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function orientation()
    {
        return $this->belongsTo(Orientation::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    public function getAttribute($key)
    {
        switch ($key) {
            case "created_at_readable":
            case "updated_at_readable":
                $key = explode("_readable", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? $value->format(config('app.date_format.php').' '.config('app.time_format.php')) : null;
            default:
                return parent::getAttribute($key);
        }
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
