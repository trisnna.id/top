<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseIntakeView extends Model
{
    use BaseModelTrait,
        HasFactory;

    protected $fillable = [
        "course_code",
        "course_description",
        "intake_number",
        "school",
        "faculty",
        "sync_date"
    ];
}
