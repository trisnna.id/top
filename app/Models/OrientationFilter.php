<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;

class OrientationFilter extends BaseModel
{
    use BaseModelTrait;

    protected $fillable = [
        'name',
        'value',
        'raw_value',
    ];
    protected $casts = [
        'value' => 'array',
        'raw_value' => 'array',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function orientation()
    {
        return $this->belongsTo(Orientation::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
