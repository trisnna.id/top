<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;

class GoogleAccount extends BaseModel
{
    use BaseModelTrait;

    protected $table = 'google_accounts';
    protected $fillable = [
        'access_token',
        'expired_at',
        'created_by',
        'updated_by'
    ];
    protected $casts = [
        'expired_at' => 'datetime',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
