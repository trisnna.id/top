<?php

namespace App\Models;

use App\Contracts\Entities\StudentPreOrientationEntity;
use App\Models\Scopes\WithPreOrientation;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Pivot;

class StudentPreOrientation extends Pivot implements StudentPreOrientationEntity
{
    use HasFactory;
    use WithPreOrientation;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        self::FIELD_COMPLETED => self::STATUS_INCOMPLETED,
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_QUIZ => 'collection',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        self::FIELD_PREORIENTATION,
        self::FIELD_QUIZ,
        self::FIELD_COMPLETED
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [
        self::FIELD_PREORIENTATION,
        self::FIELD_STUDENT,
    ];

    /**
     * {@inheritdoc}
     */
    public function generateTags(): array
    {
        return [
            'activities',
            'pre-orientation',
            'students',
        ];
    }

    /**
     * Get the pre-orientation that belongs to student pre-orientation's
     *
     * @param   Builder     $query
     * @param   int         $preOrientationId
     * @return  Builder
     */
    public function scopePreorientation(Builder $query, int $preOrientationId = 0): Builder
    {
        if ($preOrientationId === 0) {
            return $query->whereNot(self::FIELD_PREORIENTATION);
        }

        return $query->where(self::FIELD_PREORIENTATION, $preOrientationId);
    }

    /**
     * Get the student that belongs to student pre-orientation's
     *
     * @param   Builder     $query
     * @param   int         $preOrientationId
     * @return  Builder
     */
    public function scopeStudent(Builder $query, int $studentId = 0): Builder
    {
        if ($studentId === 0) {
            return $query->whereNot(self::FIELD_PREORIENTATION);
        }

        return $query->where(self::FIELD_PREORIENTATION, $studentId);
    }

    /**
     * Get pre-orientation quizzes questions that belongs to this entity  
     *
     * @param   Builder     $query
     * @param   int         $quizId 
     * @return  Builder
     */
    public function scopeWithQuestions(Builder $query, int $quizId = 0): Builder
    {
        return $query;
    }

    public function scopeCompleted($query, Student $student)
    {
        return $query->select([
            'students_preorientations.id',
            StudentPreOrientationEntity::FIELD_PREORIENTATION,
            StudentPreOrientationEntity::FIELD_STUDENT,
            StudentPreOrientationEntity::FIELD_COMPLETED,
            StudentPreOrientationEntity::FIELD_QUIZ,
            StudentPreOrientationEntity::FIELD_SCORE,
        ])
            ->join('pre_orientations', 'pre_orientations.id', '=', 'students_preorientations.pre_orientation_id')
            ->where([
                [StudentPreOrientationEntity::FIELD_STUDENT, '=', $student->id],
                [StudentPreOrientationEntity::FIELD_COMPLETED, '=', StudentPreOrientationEntity::STATUS_COMPLETED],
                ['pre_orientations.status', '=', 'published']
            ]);
    }
}
