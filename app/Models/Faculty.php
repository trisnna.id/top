<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use App\Vendors\Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use App\Vendors\Illuminate\Database\Eloquent\CustomCacheBuilder;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Faculty extends BaseModel
{
    use BaseModelTrait, Cachable;

    protected $cachePrefix = "faculty";
    public $hasUuid = true;
    protected $fillable = [
        'uuid',
        'name',
        'is_active',
        'is_cms_active',
        'created_by',
        'updated_by'
    ];
    protected $casts = [
        'uuid' => 'string',
        'is_active' => 'boolean',
        'is_cms_active' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function newEloquentBuilder($query)
    {
        if (!$this->isCachable()) {
            $this->isCachable = false;
            return new EloquentBuilder($query);
        }
        return new CustomCacheBuilder($query);
    }
}
