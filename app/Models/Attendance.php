<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Carbon\Carbon;

class Attendance extends BaseModel
{

    use BaseModelTrait;

    public $hasUuid = true;
    // protected $fillable = [
    //     'name',
    //     'label',
    //     'description',
    //     'is_active',
    //     'guard_name',
    //     'created_by',
    //     'updated_by'
    // ];
    protected $casts = [
        'uuid' => 'string',
        'attendance_at' => 'datetime',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */


    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    // protected function startCloseAtReadable($start, $close)
    // {
    //     $startAt = $this->getAttributeValue($start);
    //     $startAtFormat = !empty($startAt) ? $startAt->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
    //     $closeAt = $this->getAttributeValue($close);
    //     $closeAtFormat = !empty($closeAt) ? $closeAt->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
    //     if ($startAtFormat == $closeAtFormat) {
    //         return $startAtFormat;
    //     } elseif (!empty($startAtFormat) && empty($closeAtFormat)) {
    //         return $startAtFormat;
    //     } elseif (empty($startAtFormat) && !empty($closeAtFormat)) {
    //         return $closeAtFormat;
    //     } elseif (!empty($startAtFormat) && !empty($closeAtFormat)) {
    //         return $startAtFormat . ' - ' . $closeAtFormat;
    //     } else {
    //         return null;
    //     }
    // }

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
