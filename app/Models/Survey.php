<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Carbon\Carbon;

class Survey extends BaseModel
{

    use BaseModelTrait;

    public $hasUuid = true;
    protected $fillable = [
        'name',
        'label',
        'description',
        'is_active',
        'guard_name',
        'created_by',
        'updated_by'
    ];
    protected $casts = [
        'uuid' => 'string',
        // 'registration_start_at' => 'datetime',
        // 'registration_close_at' => 'datetime',
        'start_at' => 'datetime',
        'end_at' => 'datetime',
        // 'is_active' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    // public function activityType()
    // {
    //     return $this->belongsTo(ActivityType::class);
    // }

    // public function programme()
    // {
    //     return $this->belongsTo(Programme::class);
    // }

    /* =========================================================================
    * Scope
    * =========================================================================
    */


    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    public function getAttribute($key)
    {
        switch ($key) {
            case "start_end_at_readable";
                return $this->startCloseAtReadable('start_at', 'end_at');
            default:
                return parent::getAttribute($key);
        }
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
