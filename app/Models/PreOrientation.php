<?php

namespace App\Models;

use App\Casts\Json;
use App\Models\Scopes\WithUuid;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Builder;
use OwenIt\Auditing\Auditable as CanAudit;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Contracts\Entities\PreOrientationQuizEntity;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Contracts\Entities\Relations\HasPreOrientation;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class PreOrientation extends Model implements Auditable, PreOrientationEntity
{
    use CanAudit;
    use HasFactory;
    use SoftDeletes;
    use WithUuid;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_CONTENT_LINKS => Json::class,
        self::FIELD_LOCALITIES    => 'array',
        self::FIELD_INTAKES       => 'array',
        self::FIELD_FACULTIES     => 'array',
        self::FIELD_SCHOOLS       => 'array',
        self::FIELD_PROGRAMMES    => 'array',
        self::FIELD_MOBILITIES    => 'array',
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        self::FIELD_STATUS => self::STATUS_DRAFT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        self::FIELD_ACTIVITY_NAME,
        self::FIELD_CATEGORY,
        self::FIELD_LOCALITIES,
        self::FIELD_EFFECTIVE_DATE,
        self::FIELD_STATUS,
        self::FIELD_CONTENT,
        self::FIELD_CONTENT_LINKS,
        self::FIELD_THUMBNAIL,
        self::FIELD_INTAKES,
        self::FIELD_FACULTIES,
        self::FIELD_SCHOOLS,
        self::FIELD_PROGRAMMES,
        self::FIELD_MOBILITIES,
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [
        self::FIELD_CONTENT,
        self::FIELD_CONTENT_LINKS,
        self::FIELD_INTAKES,
        self::FIELD_FACULTIES,
        self::FIELD_SCHOOLS,
        self::FIELD_PROGRAMMES,
        self::FIELD_MOBILITIES,
    ];

    /**
     * {@inheritdoc}
     */
    public function generateTags(): array
    {
        return [
            'activities',
            'pre-orientation'
        ];
    }

    /**
     * Get the query scope for datatable
     *
     * @param   Builder $query
     * @return  Builder
     */
    public function scopeDataTable(Builder $query): Builder
    {
        return $query->newQuery()->select([
            $this->primaryKey,
            self::FIELD_UUID,
            self::FIELD_ACTIVITY_NAME,
            self::FIELD_CATEGORY,
            self::FIELD_LOCALITIES,
            self::FIELD_EFFECTIVE_DATE,
            self::FIELD_STATUS,
            self::FIELD_INTAKES,
        ])->with('checkpoint');
    }

    /**
     * Get the query that only scoped for specific `activity_name`
     *
     * @param   Builder  $query
     * @param   string   $value
     * @return  Builder
     */
    public function scopeName(Builder $query, string $value): Builder
    {
        return $query->where(self::FIELD_ACTIVITY_NAME, $value);
    }

    /**
     * Get the query that only scoped for specific `status`
     *
     * @param   Builder  $query
     * @param   string   $status
     * @return  Builder
     */
    public function scopeStatus(Builder $query, string $status): Builder
    {
        return $query->where(self::FIELD_STATUS, $status);
    }

    /**
     * Get a specific `pre-orientation` record where the status is 'draft'
     *
     * @param   Builder  $query
     * @return  Builder
     */
    public function scopeDraft(Builder $query): Builder
    {
        return $this->scopeStatus($query, self::STATUS_DRAFT);
    }

    /**
     * Get a specific `pre-orientation` record where the status is 'published'
     *
     * @param   Builder     $query
     * @return  Builder
     */
    public function scopePublished(Builder $query): Builder
    {
        return $this->scopeStatus($query, self::STATUS_PUBLISHED);
    }

    /**
     * Get a specific `pre-orientation` record where the status is 'unpublished'
     *
     * @param   Builder     $query
     * @return  Builder
     */
    public function scopeUnpublished(Builder $query): Builder
    {
        return $this->scopeStatus($query, self::STATUS_UNPUBLISHED);
    }

    public function scopeWhereLikeStudentLocality(Builder $query, $student): Builder
    {
        return $query->where(PreOrientationEntity::FIELD_LOCALITIES, 'like', "%\"{$student->locality_id}\"%");
    }

    /**
     * Get the current `checkpoint` for the `pre-orientation`
     *
     * @return BelongsTo
     */
    public function checkpoint(): BelongsTo
    {
        return $this->belongsTo(Checkpoint::class, self::FIELD_CATEGORY);
    }

    /**
     * Get the `quiz` that are related with this `pre-orientation`
     *
     * @return HasMany
     */
    public function quizs(): HasMany
    {
        return $this->hasMany(PreOrientationQuiz::class, PreOrientationQuizEntity::FIELD_PREORIENTATION);
    }

    /**
     * The students that belongs to pre-orientations
     *
     * @return BelongsToMany
     */
    public function students(): BelongsToMany
    {
        return $this->belongsToMany(
            Student::class,
            StudentPreOrientation::TABLE,
            HasPreOrientation::FIELD_PREORIENTATION,
            StudentPreOrientation::FIELD_STUDENT
        )->using(StudentPreOrientation::class)->withPivot([
            'id',
            StudentPreOrientation::FIELD_COMPLETED,
            StudentPreOrientation::FIELD_QUIZ,
            StudentPreOrientation::FIELD_SCORE,
        ]);
    }

    /**
     * Get the columns that should receive a unique identifier.
     *
     * @return array
     */
    public function uniqueIds()
    {
        return [$this->primaryKey, self::FIELD_UUID];
    }

    /**
     * Define programmes attribute
     * 
     * @return Collection
     */
    public function getProgrammesAttribute(): Collection
    {
        $original = json_decode($this->attributes['programmes'] ?? '', true);
        $casted = collect($original)->map(function ($programmeId) {
            $programme = Programme::find($programmeId);
            return $programme ? $programme : null;
        })->filter();

        return collect([
            'original' => $original,
            'casted' => $casted,
        ]);
    }

    /**
     * Define intakes attribute
     * 
     * @return Collection
     */
    public function getIntakesAttribute(): Collection
    {
        $original = json_decode($this->attributes['intakes'] ?? '', true);
        $casted = collect($original)->map(function ($intakeId) {
            $intake = Intake::find($intakeId);
            return $intake ? $intake : null;
        })->filter();

        return collect([
            'original' => $original,
            'casted' => $casted,
        ]);
    }

    /**
     * Define faculties attribute
     * 
     * @return Collection
     */
    public function getFacultiesAttribute(): Collection
    {
        $original = json_decode($this->attributes['faculties'] ?? '', true);
        $casted = collect($original)->map(function ($facultyId) {
            $faculty = Faculty::find($facultyId);
            return $faculty ? $faculty : null;
        })->filter();

        return collect([
            'original' => $original,
            'casted' => $casted,
        ]);
    }

    /**
     * Define schools attribute
     * 
     * @return Collection
     */
    public function getSchoolsAttribute(): Collection
    {
        $original = json_decode($this->attributes['schools'] ?? '', true);
        $casted = collect($original)->map(function ($schoolId) {
            $school = School::find($schoolId);
            return $school ? $school : null;
        })->filter();

        return collect([
            'original' => $original,
            'casted' => $casted,
        ]);
    }

    /**
     * Define mobilities attribute
     * 
     * @return Collection
     */
    public function getMobilitiesAttribute(): Collection
    {
        $original = json_decode($this->attributes['mobilities'] ?? '', true);
        $casted = collect($original)->map(function ($mobilityId) {
            $mobility = Mobility::find($mobilityId);
            return $mobility ? $mobility : null;
        })->filter();

        return collect([
            'original' => $original,
            'casted' => $casted,
        ]);
    }
}
