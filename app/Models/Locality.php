<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use App\Vendors\Illuminate\Database\Eloquent\CustomCacheBuilder;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Builder;
use App\Vendors\Illuminate\Database\Eloquent\Builder as EloquentBuilder;

class Locality extends BaseModel
{
    use BaseModelTrait;
    use Cachable;

    protected $cachePrefix = "locality";
    public $hasUuid = true;
    protected $table = 'localities';
    protected $fillable = [
        'uuid',
        'name',
        'is_active',
        'created_by',
        'updated_by'
    ];
    protected $casts = [
        'uuid' => 'string',
        'is_active' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /**
     * Get only the international locality
     *
     * @param   Builder $query
     * @return  Builder
     */
    public function scopeInter(Builder $query): Builder
    {
        return $query->where('name', 'International');
    }

    /**
     * Get only the local locality
     *
     * @param   Builder $query
     * @return  Builder
     */
    public function scopeLocal(Builder $query): Builder
    {
        return $query->where('name', 'Local');
    }


    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function newEloquentBuilder($query)
    {
        if (!$this->isCachable()) {
            $this->isCachable = false;
            return new EloquentBuilder($query);
        }
        return new CustomCacheBuilder($query);
    }
}
