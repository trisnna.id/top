<?php declare(strict_types=1);

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait WithUuid
{
    /**
     * Get the query that only scoped for specific `uuid`
     *
     * @param   Builder $query
     * @param   string|array<string>   $uuids
     * @return  Builder
     */
    public function scopeUuids(Builder $query, $uuids): Builder
    {
        if (is_string($uuids)) {
            return $query->where('uuid', $uuids);
        }

        if (is_array($uuids)) {
            return $query->whereIn('uuid', $uuids);
        }

        return $query;
    }
}
