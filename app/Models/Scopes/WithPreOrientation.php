<?php declare(strict_types=1);

namespace App\Models\Scopes;

use App\Contracts\Entities\Relations\HasPreOrientation;
use Illuminate\Database\Eloquent\Builder;

trait WithPreOrientation
{
    /**
     * Get the specific entity base on `pre_orientation_id`
     *
     * @param   Builder     $query
     * @param   int         $preOrientationId
     * @return  Builder
     */
    public function scopePreOrientation(Builder $query, int $preOrientationId): Builder
    {
        return $query->where(HasPreOrientation::FIELD_PREORIENTATION, $preOrientationId);
    }
}
