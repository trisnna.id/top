<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;

class OrientationAttendance extends BaseModel
{
    use BaseModelTrait;

    public $hasUuid = true;
    protected $table = "orientation_attendances";
    protected $fillable = [
        'uuid',
        'orientation_id',
        'student_id',
        'rating',
        'point',
        'point_rating',
        'point_attendance',
    ];
    protected $casts = [
        'uuid' => 'string',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function orientation()
    {
        return $this->belongsTo(Orientation::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    public function getAttribute($key)
    {
        switch ($key) {
            case "created_at_readable":
            case "updated_at_readable":
                $key = explode("_readable", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? $value->format(config('app.date_format.php').' '.config('app.time_format.php')) : null;
            default:
                return parent::getAttribute($key);
        }
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function generateTags(): array
    {
        if ($this->auditEvent == 'created') {
            return [
                'completed',
            ];
        }
        return [];
    }
}
