<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Carbon\Carbon;

class UserMeta extends BaseModel
{

    use BaseModelTrait;

    protected $table = 'users_meta';

    // public $hasUuid = true;
    // protected $fillable = [
    //     'name',
    //     'label',
    //     'description',
    //     'is_active',
    //     'guard_name',
    //     'created_by',
    //     'updated_by'
    // ];
    // protected $casts = [
    //     'uuid' => 'string',
    //     'is_active' => 'boolean',
    // ];

    /* =========================================================================
		 * Relationship
		 * =========================================================================
		 */

    // public function roleHasPermissions()
    // {
    //     return $this->hasMany(RoleHasPermission::class);
    // }
    /* =========================================================================
		 * Scope
		 * =========================================================================
		 */


    /* =========================================================================
		 * Accessor & Mutator
		 * =========================================================================
		 */

    // public function getAttribute($key)
    // {
    //     switch ($key) {
    //         case "created_at_readable":
    //             $key = explode("_readable", $key)[0];
    //             $value = $this->getAttributeValue($key);
    //             return !empty($value) ? Carbon::createFromFormat(
    //                 'Y-m-d H:i:s',
    //                 $value
    //             )->format(config('app.date_format.php') . ' ' . config('app.time_format.php'))
    //                 : null;
    //         default:
    //             return parent::getAttribute($key);
    //     }
    // }
    /* =========================================================================
		 * Other
		 * =========================================================================
		 */
}
