<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable;

class BaseModel extends Model implements Auditable
{

    use AuditableTrait,
        BaseModelTrait,
        HasFactory;
}
