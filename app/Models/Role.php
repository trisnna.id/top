<?php

namespace App\Models;

use App\Constants\RoleConstant;
use App\Traits\Models\BaseModelTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    use BaseModelTrait;
    use HasFactory;

    public $hasUuid = true;
    protected $fillable = [
        'name',
        'title',
        'description',
        'is_active',
        'guard_name',
        'created_by',
        'updated_by'
    ];
    protected $casts = [
        'uuid' => 'string',
        'is_active' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function roleHasPermissions()
    {
        return $this->hasMany(RoleHasPermission::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */


    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    public function getAttribute($key)
    {
        switch ($key) {
            case "rule_can_delete": //user can delete
                return !$this->users()->exists() && !in_array($this->name, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_ORIENTATION_LEADER, RoleConstant::ROLE_STUDENT]);
            case "created_at_readable":
                $key = explode("_readable", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? Carbon::createFromFormat(
                    'Y-m-d H:i:s',
                    $value
                )->format(config('app.date_format.php') . ' ' . config('app.time_format.php'))
                    : null;
            default:
                return parent::getAttribute($key);
        }
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
