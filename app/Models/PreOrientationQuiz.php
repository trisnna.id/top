<?php

namespace App\Models;

use App\Casts\Json;
use App\Contracts\Entities\PreOrientationQuizEntity;
use App\ValueObjects\McqOption;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use OwenIt\Auditing\Contracts\Auditable;

class PreOrientationQuiz extends Model implements Auditable, PreOrientationQuizEntity
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pre_orientation_quizs';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_OPTIONS => Json::class,
        self::FIELD_ANSWERS => Json::class,
        self::FIELD_SETTINGS => Json::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        self::FIELD_PREORIENTATION,
        self::FIELD_QUESTION,
        self::FIELD_OPTIONS,
        self::FIELD_ANSWERS,
        self::FIELD_SETTINGS,
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [];

    /**
     * {@inheritdoc}
     */
    public function generateTags(): array
    {
        return [
            'activities',
            'pre-orientation'
        ];
    }

    /**
     * Get shuffled mcq `options` if the `shuffle` setting is active
     *
     * @return  Collection
     */
    public function getShuffledOptionsAttribute(): Collection
    {
        if ($this->settings->get('shuffle')) {
            $keys = ['a', 'b', 'c', 'd'];
            $shuffledOptions = Collection::make($this->options->toArray())->shuffle();
            
            while (implode(',', $keys) === $shuffledOptions->map(fn ($item) => $item['key'])->implode(',')) {
                $shuffledOptions = Collection::make($this->options->toArray())->shuffle();
            }

            return Collection::make($keys)->transform(function (string $key, int $i) use ($shuffledOptions) {
                $option = $shuffledOptions->get($i);
                $option['key'] = $key;
                return new McqOption($option);
            });
        }

        return $this->options;
    }

    /**
     * Get the pre-orientation quiz base on specified pre-orientation id
     *
     * @param   Builder     $query
     * @param   int         $preOrientationId
     * @return  Builder
     */
    public function scopePreOrientation(Builder $query, int $preOrientationId): Builder
    {
        return $query->where(self::FIELD_PREORIENTATION, $preOrientationId);
    }

    /**
     * Get the pre orientation that owns this quiz
     *
     * @return BelongsTo
     */
    public function preOrientation(): BelongsTo
    {
        return $this->belongsTo(PreOrientation::class, self::FIELD_PREORIENTATION);
    }
}
