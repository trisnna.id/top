<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Carbon\Carbon;

class SurveyAnswer extends BaseModel
{

    use BaseModelTrait;

    public $hasUuid = true;
    // protected $fillable = [
    //     'name',
    //     'label',
    //     'description',
    //     'is_active',
    //     'guard_name',
    //     'created_by',
    //     'updated_by'
    // ];
    protected $casts = [
        'uuid' => 'string',
        // 'registration_start_at' => 'datetime',
        // 'registration_close_at' => 'datetime',
        // 'start_at' => 'datetime',
        // 'end_at' => 'datetime',
        // 'is_active' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
    
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */


    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    // public function getAttribute($key)
    // {
    //     switch ($key) {
    //         case "registration_start_close_at_readable";
    //             return $this->startCloseAtReadable('registration_start_at', 'registration_close_at');
    //         case "start_end_at_readable";
    //             return $this->startCloseAtReadable('start_at', 'end_at');
    //         case "registration_start_at_readable":
    //         case "registration_close_at_readable":
    //         case "start_at_readable":
    //         case "end_at_readable":
    //         case "created_at_readable":
    //             $key = explode("_readable", $key)[0];
    //             $value = $this->getAttributeValue($key);
    //             return !empty($value) ? $value->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
    //         default:
    //             return parent::getAttribute($key);
    //     }
    // }

    // protected function startCloseAtReadable($start, $close)
    // {
    //     $startAt = $this->getAttributeValue($start);
    //     $startAtFormat = !empty($startAt) ? $startAt->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
    //     $closeAt = $this->getAttributeValue($close);
    //     $closeAtFormat = !empty($closeAt) ? $closeAt->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
    //     if ($startAtFormat == $closeAtFormat) {
    //         return $startAtFormat;
    //     } elseif (!empty($startAtFormat) && empty($closeAtFormat)) {
    //         return $startAtFormat;
    //     } elseif (empty($startAtFormat) && !empty($closeAtFormat)) {
    //         return $closeAtFormat;
    //     } elseif (!empty($startAtFormat) && !empty($closeAtFormat)) {
    //         return $startAtFormat . ' - ' . $closeAtFormat;
    //     } else {
    //         return null;
    //     }
    // }

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
