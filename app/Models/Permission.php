<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{

    use BaseModelTrait;
    protected $fillable = [
        'name',
        'title',
        'description',
        'weight',
        'guard_name',
        'parent_id'
    ];
    protected $casts = [
        'uuid' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /* =========================================================================
		 * Relationship
		 * =========================================================================
		 */

    public function recursivePermissionsByParentId()
    {
        return $this->permissionsByParentId()->with('recursivePermissionsByParentId');
    }

    public function permissionsByParentId()
    {
        return $this->hasMany(Permission::class, 'parent_id', 'id');
    }
    /* =========================================================================
		 * Scope
		 * =========================================================================
		 */

    /* =========================================================================
		 * Accessor & Mutator
		 * =========================================================================
		 */

    /* =========================================================================
		 * Other
		 * =========================================================================
		 */
}
