<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Throwable;

class Resource extends BaseModel implements HasMedia
{

    use BaseModelTrait;
    use InteractsWithMedia;

    public $hasUuid = true;
    protected $fillable = [
        'id',
        'uuid',
        'title',
        'description',
        'resource_type_id',
        'is_active',
        'created_by',
        'updated_by'
    ];
    protected $casts = [
        'uuid' => 'string',
        'is_active' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function resourceLinks()
    {
        return $this->hasMany(ResourceLink::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    public function getAttribute($key)
    {
        switch ($key) {
            case "description_encode":
                return toJsonEncode($this->description ?? '');
            case "rule_can_delete": //user can delete
                return $this->is_active == false;
            case "created_at_readable":
            case "updated_at_readable":
                $key = explode("_readable", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? $value->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
            default:
                return parent::getAttribute($key);
        }
    }

    protected function description(): Attribute
    {
        return Attribute::make(
            get: function ($value) {
                $url = json_decode($value);
                if (filter_var($url, FILTER_VALIDATE_URL)) {
                    $urlPath = parse_url(filter_var($url, FILTER_VALIDATE_URL))['path'];
                    $urlPath = Str::replace("/storage/", '', $urlPath);
                    $path = storage_path('app/public/' . $urlPath);
                    if (File::exists($path)) {
                        return file_get_contents($path);
                    } else {
                        return null;
                    }
                } else {
                    return $value;
                }
            },
            set: fn ($value) => toJsonEncode($value ?? ''),
        );
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('documents');
        $this->addMediaCollection('media');
        $this->addMediaCollection('description')
            ->singleFile();
        $this->addMediaCollection('thumbnail')
            ->useFallbackUrl(url(setting('file.thumbnail_fallback_url')))
            ->singleFile();
    }
}
