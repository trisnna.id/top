<?php

namespace App\Models;

use App\Casts\Json;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\Scopes\WithUuid;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Builder;
use OwenIt\Auditing\Auditable as CanAudit;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contracts\Entities\AnnouncementEntity;
use App\Repositories\Contracts\ProgrammeRepository;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Announcement extends Model implements Auditable, AnnouncementEntity
{
    use CanAudit;
    use HasFactory;
    use SoftDeletes;
    use WithUuid;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_CONTENT_LINKS => Json::class,
        self::FIELD_LOCALITIES    => 'array',
        self::FIELD_INTAKES       => 'array',
        self::FIELD_CAMPUSES      => 'array',
        self::FIELD_PROGRAMMES    => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_LOCALITIES,
        self::FIELD_CONTENT,
        self::FIELD_CONTENT_LINKS,
        self::FIELD_INTAKES,
        self::FIELD_CAMPUSES,
        self::FIELD_PROGRAMMES,
        self::FIELD_STATUS,
        self::FIELD_THUMBNAIL,
        self::FIELD_SEQUENCE,
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array<int, string>
     */
    protected $auditInclude = [
        //
    ];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array<int, string>
     */
    protected $auditExclude = [
        self::FIELD_CONTENT,
        self::FIELD_CONTENT_LINKS,
    ];

    /**
     * Get the query scope for datatable
     *
     * @param   Builder $query
     * @return  Builder
     */
    public function scopeDataTable(Builder $query): Builder
    {
        $query = $query->newQuery()->select([
            $this->primaryKey,
            self::FIELD_UUID,
            self::FIELD_NAME,
            self::FIELD_SEQUENCE,
            self::FIELD_LOCALITIES,
            self::FIELD_CONTENT,
            self::FIELD_CONTENT_LINKS,
            self::FIELD_INTAKES,
            self::FIELD_CAMPUSES,
            self::FIELD_PROGRAMMES,
            self::FIELD_STATUS
        ]);

        return $this->scopeOrderBySequence($query);
    }

    /**
     * Get the query scope ordered by sequence in ascending direction
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderBySequence(Builder $query): Builder
    {
        $orders = request('order', []);
        
        if (!empty($orders)) {
            foreach (request('order', []) as $order) {
                if (Arr::get($order, 'column') === '2' && Arr::get($order, 'dir') === 'desc') {
                    $query = $query->orderBy(DB::raw(sprintf('%s is not null, %s', self::FIELD_SEQUENCE, self::FIELD_SEQUENCE)), 'DESC');
                } else {
                    $query = $query->orderBy(DB::raw(sprintf('%s is null, %s', self::FIELD_SEQUENCE, self::FIELD_SEQUENCE)));
                }
            }
        } else {
            $query = $query->orderBy(DB::raw(sprintf('%s is null, %s', self::FIELD_SEQUENCE, self::FIELD_SEQUENCE)));
        }

        return $query;
    }

    /**
     * Define slug attribute
     * 
     * @return string
     */
    public function getSlugAttribute(): string
    {
        return Str::slug($this->name);
    }

    /**
     * Define programmes attribute
     * 
     * @return Collection
     */
    public function getProgrammesAttribute(): Collection
    {
        $original = json_decode($this->attributes['programmes'], true);
        $casted = collect($original)->map(function ($programmeId) {
            $programme = Programme::find($programmeId);
            return $programme ? $programme : null;
        })->filter();

        return collect([
            'original' => $original,
            'casted' => $casted,
        ]);
    }

    /**
     * Define intakes attribute
     * 
     * @return Collection
     */
    public function getIntakesAttribute(): Collection
    {
        $original = json_decode($this->attributes['intakes'], true);
        $casted = collect($original)->map(function ($intakeId) {
            $intake = Intake::find($intakeId);
            return $intake ? $intake : null;
        })->filter();

        return collect([
            'original' => $original,
            'casted' => $casted,
        ]);
    }

    /**
     * Define localities attribute
     * 
     * @return Collection
     */
    public function getLocalitiesAttribute(): Collection
    {
        $original = json_decode($this->attributes['localities'], true);
        $casted = collect($original)->map(function ($localityId) {
            $locality = Locality::find($localityId);
            return $locality ? $locality : null;
        })->filter();

        return collect([
            'original' => $original,
            'casted' => $casted,
        ]);
    }

    /**
     * Define campuses attribute
     * 
     * @return Collection
     */
    public function getCampusesAttribute(): Collection
    {
        $original = json_decode($this->attributes['campuses'], true);
        $casted = collect($original)->map(function ($campusId) {
            $campus = Campus::find($campusId);
            return $campus ? $campus : null;
        })->filter();

        return collect([
            'original' => $original,
            'casted' => $casted,
        ]);
    }

    /**
     * Check if the current announement status is 'draft'
     *
     * @return boolean
     */
    public function isDraft(): bool
    {
        return $this->status === self::STATUS_DRAFT;
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function generateTags(): array
    {
        if ($this->wasChanged('status')) {
            $status = $this->getChanges()['status'] ?? null;
            if (!empty($status ?? null) && in_array($status, ['published', 'unpublished'])) {
                return [
                    $this->status
                ];
            }
            return [];
        }
        return [];
    }
}
