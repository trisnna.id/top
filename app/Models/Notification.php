<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;

class Notification extends BaseModel
{
    use BaseModelTrait;

    protected $fillable = [
        'type',
        'notifiable_type',
        'notifiable_id',
        'data',
        'read_at',
    ];
    protected $casts = [
        'id' => 'string',
        'data' => 'json',
        'read_at' => 'datetime',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    public function getAttribute($key)
    {
        switch ($key) {
            case "created_at_readable":
            case "updated_at_readable":
                $key = explode("_readable", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? $value->format(config('app.date_format.php').' '.config('app.time_format.php')) : null;
            default:
                return parent::getAttribute($key);
        }
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
