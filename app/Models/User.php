<?php

namespace App\Models;

use App\Constants\RoleConstant;
use App\Traits\Models\BaseModelTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Kodeine\Metable\Metable;
use Laravel\Sanctum\HasApiTokens;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements Auditable, MustVerifyEmail, HasMedia
{
    use Metable, HasRoles, AuditableTrait, BaseModelTrait, HasApiTokens, HasFactory, Notifiable;
    use InteractsWithMedia;

    public $hasUuid = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'uuid',
        'model_type',
        'model_id',
        'name',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'role_id',
        'is_active',
        'last_login',
        'created_via',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        // 'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'uuid' => 'string',
        'last_login' => 'datetime',
        'email_verified_at' => 'datetime',
        'is_active' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function student()
    {
        return $this->hasOne(Student::class);
    }
    public function staff()
    {
        return $this->hasOne(Staff::class);
    }

    public function model()
    {
        return $this->morphTo();
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function createdByGoogleAccount()
    {
        return $this->hasOne(GoogleAccount::class, 'created_by');
    }

    /* =========================================================================
     * Scope
     * =========================================================================
     */

    /* =========================================================================
     * Accessor & Mutator
     * =========================================================================
     */
    public function getProfileAttribute()
    {
        if ($this->hasRole(RoleConstant::ROLE_STUDENT)) {
            return $this->student;
        }

        return $this->staff;
    }

    /* =========================================================================
     * Other
     * =========================================================================
     */

    public function generateTags(): array
    {
        if ($this->wasChanged('last_login')) {
            return [
                'login',
            ];
        }
        return [];
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')
            ->useFallbackUrl(url('media/avatars/blank.png'))
            ->singleFile();
    }
}
