<?php

namespace App\Models;

use App\Constants\OrientationStatusConstant;
use App\Constants\OrientationTypeConstant;
use App\Traits\Models\BaseModelTrait;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Throwable;

class Orientation extends BaseModel implements HasMedia
{
    use BaseModelTrait;
    use InteractsWithMedia;

    public $hasUuid = true;
    protected $fillable = [
        'uuid',
        'name',
        'venue',
        'livestream',
        'start_date',
        'start_time',
        'end_date',
        'end_time',
        'presenter',
        'agenda',
        'synopsis',
        'notes',
        'orientation_type_id',
        'is_academic',
        'is_rsvp',
        'rsvp_capacity',
        'is_point',
        'point_max',
        'point_min',
        'point_rating',
        'point_attendance',
        'total_student',
        'is_publish',
        'status_id',
        'status_name',
        'created_by',
        'updated_by'
    ];
    protected $casts = [
        'uuid' => 'string',
        'is_rsvp' => 'boolean',
        'is_point' => 'boolean',
        'is_publish' => 'boolean',
        'is_academic' => 'boolean',
    ];
    protected $appends = [
        'filters'
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function orientationFilters()
    {
        return $this->hasMany(OrientationFilter::class);
    }

    public function orientationDates()
    {
        return $this->hasMany(OrientationDate::class);
    }

    public function orientationType()
    {
        return $this->belongsTo(OrientationType::class);
    }

    public function orientationLinks()
    {
        return $this->hasMany(OrientationLink::class);
    }

    public function orientationRsvps()
    {
        return $this->hasMany(OrientationRsvp::class);
    }

    public function orientationAttendances()
    {
        return $this->hasMany(OrientationAttendance::class);
    }

    public function createdByUser()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */

    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    protected function agenda(): Attribute
    {
        return Attribute::make(
            get: function ($value) {
                $url = json_decode($value);
                if (filter_var($url, FILTER_VALIDATE_URL)) {
                    $urlPath = parse_url(filter_var($url, FILTER_VALIDATE_URL))['path'];
                    $urlPath = Str::replace("/storage/", '', $urlPath);
                    $path = storage_path('app/public/' . $urlPath);
                    if (File::exists($path)) {
                        return file_get_contents($path);
                    } else {
                        return null;
                    }
                } else {
                    return $value;
                }
            },
            set: fn ($value) => toJsonEncode($value ?? ''),
        );
    }

    protected function synopsis(): Attribute
    {
        return Attribute::make(
            get: function ($value) {
                $url = json_decode($value);
                if (filter_var($url, FILTER_VALIDATE_URL)) {
                    $urlPath = parse_url(filter_var($url, FILTER_VALIDATE_URL))['path'];
                    $urlPath = Str::replace("/storage/", '', $urlPath);
                    $path = storage_path('app/public/' . $urlPath);
                    if (File::exists($path)) {
                        return file_get_contents($path);
                    } else {
                        return null;
                    }
                } else {
                    return $value;
                }
            },
            set: fn ($value) => toJsonEncode($value ?? ''),
        );
    }

    public function getFiltersAttribute()
    {
        return $this->orientationFilters->pluck('value', 'name');
    }

    public function getRawFiltersAttribute()
    {
        return $this->orientationFilters->pluck('raw_value', 'name');
    }

    public function getAttribute($key)
    {
        $orientationTypeConstants = getConstant('OrientationTypeConstant');
        switch ($key) {
            case "icon":
                return $orientationTypeConstants[$this->orientation_type_id] ?? [];
            case "orientation_type_constant":
                return $orientationTypeConstants[$this->orientation_type_id] ?? [];
            case "agenda_encode":
                return toJsonEncode($this->agenda ?? '');
            case "name_uppercase":
                return Str::upper($this->name);
            case "synopsis_encode":
                return toJsonEncode($this->synopsis ?? '');
            case "count_rsvp_current_capacity":
                return $this->orientationRsvps->where('is_go', true)->count();
            case "is_rsvp_full":
                if ($this->count_rsvp_current_capacity >= $this->rsvp_capacity) {
                    return true;
                } else {
                    return false;
                }
            case "is_type_activity":
                return in_array($this->orientation_type_id, [OrientationTypeConstant::COMPLEMENTARY, OrientationTypeConstant::COMPULSORY_CORE]);
            case "is_status_approved":
                return $this->status_id == OrientationStatusConstant::APPROVED;
            case "is_rsvp_yes_no":
                return $this->is_rsvp ? 'Yes' : 'No';
            case "is_publish_yes_no":
                return $this->is_publish ? 'Yes' : 'No';
            case "rule_can_rsvp":
                return $this->is_rsvp && ($this->start_date_carbon->isFuture() || $this->start_date_carbon->isToday());
            case "rule_can_approve": //user can approve
                return in_array($this->status_id, [OrientationStatusConstant::PENDING_APPROVAL, OrientationStatusConstant::REQUIRE_CORRECTION, OrientationStatusConstant::CANCELLED]);
            case "rule_can_cancel": //user can cancel
                return in_array($this->status_id, [OrientationStatusConstant::PENDING_APPROVAL, OrientationStatusConstant::REQUIRE_CORRECTION, OrientationStatusConstant::APPROVED]);
            case "rule_can_view": //user can view
                return in_array($this->status_id, [OrientationStatusConstant::PENDING_APPROVAL, OrientationStatusConstant::APPROVED, OrientationStatusConstant::CANCELLED]) && $this->created_by == (auth()->user()->id ?? false);
            case "rule_can_approval": //user can approval
                return $this->status_id !== OrientationStatusConstant::DRAFT && auth()->user()->can('manage_activity_approval');
            case "rule_can_publish": //user can publish
                return $this->status_id == OrientationStatusConstant::APPROVED && auth()->user()->can('manage_activity_approval');
            case "rule_can_edit": //user can edit
                return in_array($this->status_id, [OrientationStatusConstant::DRAFT, OrientationStatusConstant::REQUIRE_CORRECTION]) && $this->created_by == (auth()->user()->id ?? false);
            case "rule_can_submit": //user can submit
                return $this->status_id == OrientationStatusConstant::DRAFT;
            case "rule_can_delete": //user can delete
                return $this->status_id == OrientationStatusConstant::DRAFT;
            case "rule_can_delete_attachment": //user can delete
                return in_array($this->status_id, [OrientationStatusConstant::DRAFT, OrientationStatusConstant::REQUIRE_CORRECTION]);
            case "start_end_date_dFY":
                if ($this->start_date == $this->end_date) {
                    return "{$this->start_date_dFY}";
                } else {
                    return "{$this->start_date_dFY} to {$this->end_date_dFY}";
                }
            case "start_end_datetime_dFY":
                if ($this->start_date == $this->end_date) {
                    return "{$this->start_date_dFY}, {$this->start_time_readable} to {$this->end_time_readable}";
                } else {
                    return "{$this->start_date_dFY} to {$this->end_date_dFY}, {$this->start_time_readable} to {$this->end_time_readable}";
                }
            case "start_end_datetime_dFY_scan":
                $endTime = Carbon::createFromFormat('H:i:s', $this->end_time)->addMinutes(setting('orientation.extension_scan_attendance'))->format(config('app.time_format.php'));
                if ($this->start_date == $this->end_date) {
                    return "{$this->start_date_dFY}, {$this->start_time_readable} to {$endTime}";
                } else {
                    return "{$this->start_date_dFY} {$this->start_time_readable} to {$this->end_date_dFY} {$endTime}";
                }
            case "start_end_datetime_scan":
                $endTime = Carbon::createFromFormat('H:i:s', $this->end_time)->addMinutes(setting('orientation.extension_scan_attendance'))->format(config('app.time_format.php'));
                return "{$this->start_date}, {$this->start_time_readable} to {$endTime}";
            case "rule_can_scan_attendance":
                $now = Carbon::now();
                $startDatetime = $this->start_datetime_carbon;
                $endDatetime = $this->end_datetime_carbon->addMinutes(setting('orientation.extension_scan_attendance'));
                return $now->between($startDatetime, $endDatetime) && $this->is_publish && $this->is_type_activity;
            case "end_date_carbon":
            case "start_date_carbon":
                $key = explode("_carbon", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? Carbon::createFromFormat('Y-m-d', $value) : null;
            case "start_datetime_carbon":
                return Carbon::createFromFormat('Y-m-d H:i:s', "{$this->start_date} {$this->start_time}");
            case "end_datetime_carbon":
                return Carbon::createFromFormat('Y-m-d H:i:s', "{$this->end_date} {$this->end_time}");
            case "start_date_iso_8601":
                return $this->start_datetime_carbon->toIso8601String();
            case "end_date_iso_8601":
                return $this->end_datetime_carbon->toIso8601String();
            case "end_date_j":
            case "start_date_j":
                $key = explode("_j", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? Carbon::createFromFormat('Y-m-d', $value)->format('j') : null;
            case "end_date_M":
            case "start_date_M":
                $key = explode("_M", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? Carbon::createFromFormat('Y-m-d', $value)->format('M') : null;
            case "end_date_dFY":
            case "start_date_dFY":
                $key = explode("_dFY", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? Carbon::createFromFormat('Y-m-d', $value)->format('d F Y') : null;
            case "start_date_readable":
                $key = explode("_readable", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? Carbon::createFromFormat('Y-m-d', $value)->format(config('app.date_format.php')) : null;
            case "start_end_time_readable":
                return "{$this->start_time_readable} to {$this->end_time_readable}";
            case "start_time_readable":
            case "end_time_readable":
                $key = explode("_readable", $key)[0];
                $value = $this->getAttributeValue($key);
                return !empty($value) ? Carbon::createFromFormat('H:i:s', $value)->format(config('app.time_format.php')) : null;
            default:
                return parent::getAttribute($key);
        }
    }

    /* =========================================================================
    * Other
    * =========================================================================
    */

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('files');
        $this->addMediaCollection('recordings');
        $this->addMediaCollection('synopsis')
            ->singleFile();
        $this->addMediaCollection('agenda')
            ->singleFile();
        $this->addMediaCollection('thumbnail')
            ->useFallbackUrl(url(setting('file.thumbnail_fallback_url')))
            ->singleFile();
    }

    public function generateTags(): array
    {
        if ($this->wasChanged('is_publish')) {
            $publish = $this->getChanges()['is_publish'] ?? null;
            if ($publish) {
                return ['published'];
            } else {
                return ['unpublished'];
            }
        }
        return [];
    }
}
