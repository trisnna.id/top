<?php

namespace App\Models;

use App\Contracts\Entities\CheckpointEntity;
use App\Models\Scopes\WithUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable as CanAudit;
use OwenIt\Auditing\Contracts\Auditable;

class Checkpoint extends BaseModel implements Auditable, CheckpointEntity
{
    use CanAudit;
    use HasFactory;
    use SoftDeletes;
    use WithUuid;

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        self::FIELD_LABEL, self::FIELD_VALUE
    ];

    /**
     * {@inheritdoc}
     */
    public function generateTags(): array
    {
        return [
            'constant',
            'master'
        ];
    }

    public function getLabelAttribute($value): string
    {
        return str_replace('Milestone', config('view.description.checkpoint'), $value);
    }

    /**
     * Get the `pre-orientations` that are related with this `checkpoint`
     *
     * @return  HasMany
     */
    public function preOrientations(): HasMany
    {
        return $this->hasMany(PreOrientation::class, PreOrientation::FIELD_CATEGORY);
    }
}
