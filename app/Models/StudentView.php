<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentView extends Model
{
    use BaseModelTrait,
        HasFactory;

    protected $fillable = [
        "name",
        "student_id",
        "faculty_name",
        "school_name",
        "intake_number",
        "contact_no",
        "programme_name",
        "student_level",
        "taylors_email",
        "personal_email",
        "flame_mentor_name",
        "campus",
        "nationality",
        "mobility",
        "level_of_study",
        "intake_start_date",
        "recent_status_change_date",
        "course_status",
        "profile_status",
        "sync_date",
    ];
}
