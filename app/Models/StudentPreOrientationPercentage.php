<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Auditable as CanAudit;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Contracts\Entities\StudentPreOrientationPercentageEntity;
use App\Models\Scopes\WithUuid;

class StudentPreOrientationPercentage extends Model implements Auditable, StudentPreOrientationPercentageEntity
{
    use HasFactory;
    use CanAudit;
    use WithUuid;

    protected $table = 'students_preorientations_percentages';

    protected $fillable = [
        self::FIELD_UUID,
        self::FIELD_PRE_ORIENTATION_ID,
        self::FIELD_STUDENT_ID,
        self::FIELD_SCORE,
    ];

    protected $auditInclude = [];

    protected $auditExclude = [];
}
