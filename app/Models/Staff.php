<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Carbon\Carbon;

class Staff extends BaseModel
{

    use BaseModelTrait;

    public $hasUuid = true;
    protected $table = 'staffs';
    protected $fillable = [
        'uuid',
        'name',
        'id_number',
        'taylors_email',
        'user_id',
    ];
    protected $casts = [
        'uuid' => 'string',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */


    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    /* =========================================================================
    * Other
    * =========================================================================
    */
}
