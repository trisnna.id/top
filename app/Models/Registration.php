<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Carbon\Carbon;

class Registration extends BaseModel
{

    use BaseModelTrait;

    public $hasUuid = true;
    // protected $fillable = [
    //     'name',
    //     'label',
    //     'description',
    //     'is_active',
    //     'guard_name',
    //     'created_by',
    //     'updated_by'
    // ];
    protected $casts = [
        'uuid' => 'string',
        // 'attendance_at' => 'datetime',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */


    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */


    /* =========================================================================
    * Other
    * =========================================================================
    */
}
