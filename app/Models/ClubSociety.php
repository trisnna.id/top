<?php

namespace App\Models;

use App\Traits\Models\BaseModelTrait;
use Carbon\Carbon;

class ClubSociety extends BaseModel
{

    use BaseModelTrait;

    public $hasUuid = true;
    // protected $fillable = [
    //     'name',
    //     'label',
    //     'description',
    //     'is_active',
    //     'guard_name',
    //     'created_by',
    //     'updated_by'
    // ];
    protected $casts = [
        'uuid' => 'string',
        'is_active' => 'boolean',
    ];

    /* =========================================================================
    * Relationship
    * =========================================================================
    */

    public function clubSocietyCategory()
    {
        return $this->belongsTo(ClubSocietyCategory::class);
    }

    /* =========================================================================
    * Scope
    * =========================================================================
    */


    /* =========================================================================
    * Accessor & Mutator
    * =========================================================================
    */

    // public function getAttribute($key)
    // {
    //     switch ($key) {
    //         case "created_at_readable":
    //             $key = explode("_readable", $key)[0];
    //             $value = $this->getAttributeValue($key);
    //             return !empty($value) ? Carbon::createFromFormat(
    //                 'Y-m-d H:i:s',
    //                 $value
    //             )->format(config('app.date_format.php') . ' ' . config('app.time_format.php'))
    //                 : null;
    //         default:
    //             return parent::getAttribute($key);
    //     }
    // }

    /* =========================================================================
    * Other
    * =========================================================================
    */
    
    /**
     * Generate dummy data for prototyping
     *
     * @param   int $total total of each pre-orientation checkpoint that will be generated
     * @return  mixed
     */
    public static function proto(int $total = 5)
    {
        $id = 1;
        $faker = fake();
        $result = collect([]);
        $checkpoints = ['done', 'done', 'ongoing', 'todo', 'locked', 'locked'];

        for ($i = 0; $i < 6; $i++) {
            for ($t = 0; $t < $total; $t++) {
                $checkpoint = $checkpoints[$i];
                $preOrientation = self::select('id', 'name as title', 'description')->find($id);
                $preOrientation->checkpoint = $i+1;
                $preOrientation->checkpoint_status = $checkpoint;
                $preOrientation->image_link = asset('media/patterns/pattern-1.jpg');

                $mcq = collect();
                for ($j = 0; $j < 3; $j++) { 
                    $mcq->push([
                        'question' => sprintf('%s%s', $faker->sentences(5, true), $faker->randomElement(['?', ':'])),
                        'choices' => $faker->words(4),
                        'key' => $j,
                    ]);
                }
                $preOrientation->mcq = $mcq;

                if ($checkpoint === 'done') {
                    $preOrientation->status = 'completed';
                } else if ($checkpoint === 'ongoing') {
                    if ($t === 0) {
                        $preOrientation->status = 'completed';
                    } else {
                        $preOrientation->status = 'progress';
                    }
                } else {
                    $preOrientation->status = 'todo';
                }
                
                $result->push($preOrientation);
                $id++;
            }
        }

        return $result;
    }
}
