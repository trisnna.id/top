<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;

if (!function_exists('nationality')) {
    function nationality(bool $asArray = false)
    {
        $nationality = app('nationality');

        if ($asArray) {
            return $nationality->toArray();
        }

        return $nationality;
    }
}

if (!function_exists('stripStylePdf')) {
    function stripStylePdf($string)
    {
        $string = Str::replace('font-family', 'fontfamily', $string);
        $string = Str::replace('font-size', 'fontsize', $string);
        $string = Str::replace('background-color', 'backgroundcolor', $string);
        $string = Str::replace('width=', 'widthh=', $string);
        $string = Str::replace('height=', 'heightt=', $string);
        $string = Str::replace('heightt=', 'width="100%" height="auto" heightt=', $string);
        return $string;
    }
}

if (!function_exists('uniqueUuidByModel')) {

    function uniqueUuidByModel($model, string $key = 'uuid', bool $string = true)
    {
        $exist = true;
        while ($exist) {
            $uuid = Str::orderedUuid();
            $exist = $model->where($key, $uuid)->exists();
        }
        return $string ? $uuid->toString() : $uuid;
    }
}

if (!function_exists('routeSlugActive')) {

    function routeSlugActive($slug, $string = 'active')
    {
        if (is_array($slug) && in_array(Route::currentRouteName(), $slug)) {
            return $string;
        } elseif ($slug == Route::currentRouteName()) {
            return $string;
        } else {
            return request()->routeIs($slug) ? $string : '';
        }
    }
}

if (!function_exists('randomClassColor')) {

    function randomClassColor()
    {
        $colors = [
            'danger',
            'warning',
            'primary',
            'info',
            'success'
        ];
        return $colors[rand(0, 4)];
    }
}
if (!function_exists('randomLoremIpsum20Word')) {

    function randomLoremIpsum20Word()
    {
        $texts = [
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla dictum enim eu tempus scelerisque. Quisque rutrum eget tellus eget tristique. Donec elit nisl, convallis in ligula sed, eleifend elementum libero. Nunc viverra purus nec tortor ultricies porttitor. Nunc id semper.',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eget porta nunc. Ut viverra tristique ex, eget convallis tellus. Nam dapibus ante leo, eu dictum ligula pellentesque vel. Nulla non leo a tellus viverra facilisis. Quisque eu venenatis lacus, a.',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend libero felis, nec semper nisi hendrerit at. Fusce in sodales risus, at placerat dolor. Donec commodo et urna ut accumsan. Duis sed urna nec est facilisis auctor at eu odio.',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ullamcorper tellus mattis neque aliquet, commodo porttitor nulla egestas. Integer eu elementum ante. Phasellus rutrum leo eu felis volutpat mattis. Nam et euismod purus. Proin volutpat sit amet nisi quis volutpat.',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras lacinia nulla viverra, lobortis nisi eu, pellentesque ante. Pellentesque quis lacus eros. Nam eu dignissim diam. Vivamus leo urna, ultricies id ligula ac, tincidunt faucibus sem. Proin libero libero, tempus vel.',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eget sodales dui, vel blandit quam. Ut rutrum justo tellus, ac elementum diam ornare et. Nulla nibh leo, vestibulum in metus eget, dictum aliquet ipsum. Nunc faucibus, tortor ut molestie cursus.',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vulputate tellus lorem, eu feugiat nisl fermentum ac. Aenean porttitor volutpat elit sed varius. Curabitur sit amet sollicitudin ante. Morbi eleifend enim vitae lacus gravida placerat. Lorem ipsum dolor sit amet.',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc aliquam dapibus tortor, ullamcorper sagittis tellus dictum in. Sed vestibulum hendrerit lorem. Praesent orci elit, finibus sed luctus quis, vehicula at tortor. Curabitur commodo elit sit amet diam efficitur semper. Curabitur.',
        ];
        return $texts[rand(0, 7)];
    }
}


if (!function_exists('randomUrlFile')) {

    function randomUrlFile(int $index = null)
    {
        $urls = [
            asset('media/others/sample.mp4'),
            asset('media/others/sample.txt'),
            asset('media/others/sample.docx'),
            asset('media/others/sample.xlsx'),
            asset('media/others/sample.pptx'),
            asset('media/others/sample.pdf'),
            asset('media/books/1.png'),
            asset('media/books/img-72.jpg'),
            asset('media/books/5.png'),
            asset('media/books/10.png'),
            asset('media/misc/bg-2.jpg'),
            asset('media/misc/preview-1.jpg'),
        ];
        return $index !== null ? $urls[$index] : $urls[rand(0, count($urls) - 1)];
    }
}

if (!function_exists('randomBackgroundImage')) {

    function randomBackgroundImage()
    {
        $imgs = [
            '/media/auth/bg3.jpg',
            '/media/auth/bg4.jpg',
            '/media/auth/bg7-dark.jpg',
            '/media/auth/bg5.jpg',
            '/media/auth/bg1.jpg'
        ];
        return url($imgs[rand(0, 4)]);
    }
}

/**
 * Reference: https://www.w3schools.com/html/html_media.asp#:~:text=Note%3A%20Only%20MP4%2C%20WebM%2C,supported%20by%20the%20HTML%20standard.
 */
function extensionTypeMedia($media)
{
    $type = null;
    switch ($media) {
        case 'png':
        case 'jpg':
        case 'jpeg':
            $type = 'picture';
            break;
        case 'mp4':
        case 'webm':
        case 'ogg':
        case 'mp3':
            $type = 'video';
            break;
    }
    return $type;
}

if (!function_exists('urlTypeMedia')) {
    function urlTypeMedia($url)
    {
        $type = null;
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $parseUrl = parse_url($url);
            if (in_array($parseUrl['host'], ['www.youtube.com', 'youtube.com']) && preg_match("/embed/i", $parseUrl['path'])) {
                $type = 'youtube';
            } else {
                $extension = pathinfo(parse_url($url, PHP_URL_PATH), PATHINFO_EXTENSION);
                if (!empty($extension ?? null)) {
                    $type = extensionTypeMedia($extension);
                }
            }
        }
        return $type;
    }
}

if (!function_exists('toUrlMedia')) {
    function toUrlMedia($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            preg_match('/src="([^"]+)"/', $url, $urls);
            if (!empty($urls ?? [])) {
                $url = $urls[1] ?? '';
            }
        }
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $parseUrl = parse_url($url);
            if (in_array($parseUrl['host'], ['www.youtube.com', 'youtube.com'])) {
                if (!empty($parseUrl['query'] ?? null)) {
                    parse_str($parseUrl['query'], $params);
                    $v = $params['v'];
                    $url = "https://www.youtube.com/embed/{$v}";
                }
            } elseif (in_array($parseUrl['host'], ['www.youtu.be', 'youtu.be'])) {
                $v = array_values(trimArrayFilterExplode('/', $parseUrl['path']))[0] ?? '';
                if (!empty($v)) {
                    $url = "https://www.youtube.com/embed/{$v}";
                }
            }
        }
        return $url;
    }
}

if (!function_exists('fileIconExtension')) {
    function fileIconExtension($extension)
    {
        $icons = getConstant('IconConstant', 'all');
        switch ($extension) {
            case 'txt':
                $icon = $icons['ext_txt'];
                break;
            case 'mp4':
                $icon = $icons['ext_mp4'];
                break;
            case 'pptx':
                $icon = $icons['ext_pptx'];
                break;
            case 'pdf':
                $icon = $icons['ext_pdf'];
                break;
            case 'xlsx':
            case 'xls':
                $icon = $icons['ext_xlsx'];
                break;
            case 'docx':
            case 'doc':
                $icon = $icons['ext_docx'];
                break;
            default:
                $icon = $icons['ext_file'];
                break;
        }
        return $icon;
    }
}

if (!function_exists('upperSubstr')) {

    function upperSubstr($string)
    {
        return Str::upper(substr($string, 0, 1));
    }
}

if (!function_exists('urlIconMediaSocial')) {
    function urlIconMediaSocial($string)
    {
        $mediaSocial = explode('=', trim($string));
        if (!empty($mediaSocial)) {
            $type = Str::lower($mediaSocial[0]);
            $url = $mediaSocial[1];
            switch ($type) {
                case 'instagram':
                    $icon = asset('media/svg/brand-logos/instagram-2-1.svg');
                    break;
                case 'facebook':
                    $icon = asset('media/svg/brand-logos/facebook-4.svg');
                    break;
                case 'github':
                    $icon = asset('media/svg/brand-logos/github.svg');
                    break;
                case 'twitter':
                    $icon = asset('media/svg/brand-logos/twitter.svg');
                    break;
                case 'pinterest':
                    $icon = asset('media/svg/brand-logos/pinterest-p.svg');
                    break;
            }
            return [$type, $icon, $url];
        } else {
            return [];
        }
    }
}

if (!function_exists('columnDataTablesView')) {
    function columnDataTablesView(string $columnName, string $className)
    {
        $replace1 = str_replace('App\\', '', $className);
        $replace2 = str_replace('\\', '-', $replace1);
        return Str::slug($replace2, '.') . ".{$columnName}";
    }
}

if (!function_exists('getConstant')) {
    function getConstant($className, $option = null, $constant = null)
    {
        $class = "\\App\\Constants\\{$className}";
        if (!empty($constant ?? null)) {
            if (!empty($class::getAvailableOptions($option)[$constant] ?? [])) {
                return $class::getAvailableOptions($option)[$constant];
            } else {
                $constantValue = constant("{$class}::{$constant}");
                return $class::getAvailableOptions($option)[$constantValue];
            }
        } else {
            return $class::getAvailableOptions($option);
        }
    }
}

if (!function_exists('trimArrayFilterExplode')) {
    function trimArrayFilterExplode(string $seperator, string $string, string $prefix = null): array
    {
        $results = [];
        foreach (array_filter(explode($seperator, $string)) as $key => $value) {
            $results[$key] = $prefix . trim($value);
        };
        return $results;
    }
}

if (!function_exists('toJsonEncode')) {
    function toJsonEncode(string $string)
    {
        if (!empty($string) && empty(json_decode($string))) {
            $string = json_encode($string);
        }
        return $string;
    }
}

if (!function_exists('settingExtensionFormat')) {
    function settingExtensionFormat(string $string, string $prefix = '.', string $seperator = ', ')
    {
        return implode($seperator, trimArrayFilterExplode(' ', $string, $prefix));
    }
}
