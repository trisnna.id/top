<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;

class UserEventSubscriber
{

    /**
     * Handle user login events.
     */
    public function handleUserLogin($event)
    {
        $user = $event->user;
        $user->last_login = Carbon::now();
        $user->save();
    }

    /**
     * Handle user logout events.
     */
    public function handleUserLogout($event)
    {
        //
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events)
    {
        return [
            Login::class => 'handleUserLogin',
            Logout::class => 'handleUserLogout',
        ];
    }
}
