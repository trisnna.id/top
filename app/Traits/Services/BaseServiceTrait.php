<?php

namespace App\Traits\Services;

use App\Constants\OrientationTypeConstant;
use App\View\Components\Rating\FiveStar;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

trait BaseServiceTrait
{
    /* =========================================================================
    * General
    * =========================================================================
    */

    public function modelUpdateActive($model, $input, $auth, $name)
    {
        $input['updated_by'] = $auth->id;
        $input[$name] = !$model->{$name};
        $model = DB::transaction(function () use ($model, $input) {
            $model->update($input);
            return $model;
        });
        return $model;
    }

    public function validateRequest($request, array $input = [])
    {
        $input = empty($input) ? $request->all() : $input;
        $validator = Validator::make($input, $request->rules($input), $request->messages($input));
        $this->failedValidation($request, $validator);
    }

    public function failedValidation($request, $validator)
    {
        if ($validator->fails()) {
            if ($request->wantsJson()) {
                throw new HttpResponseException(response()->json(['errors' => $validator->messages()], 422));
            } else {
                throw (new ValidationException($validator))
                    ->errorBag($request->errorBag)
                    ->redirectTo(url()->previous());
            }
        }
    }

    public function throwResponseException(array $message)
    {
        throw new HttpResponseException(response()->json(['errors' => [
            "message" => $message,
        ]], 422));
    }

    public function responseStore($message = 'Create Successfully')
    {
        return request()->wantsJson() ? response()->json($this->swalData($message)) : response()->noContent(201);
    }

    public function responseUpdate($message = 'Update Successfully')
    {
        return request()->wantsJson() ? response()->json($this->swalData($message)) : response()->noContent(204);
    }

    public function responseDestroy($message = 'Delete Successfully')
    {
        return request()->wantsJson() ? response()->json($this->swalData($message)) : response()->noContent(204);
    }

    public function swalResponse(array $swalData)
    {
        return request()->wantsJson() ? response()->json($swalData) : response()->noContent(204);
    }

    public function swalData($message,  $type = "success", $alert = "toastr", $title = null, $redirect = null, $reload = false, $function = null, $data = null)
    {
        $results = [
            'message' => !empty($title ?? null) ? "{$title}. {$message}." : $message,
            'meta' => [
                'alert' => [
                    'component' => [
                        'name' => $alert,
                    ],
                    'type' => $type,
                    'title' => $title,
                    'text' => $message,
                ],
            ],
        ];
        if (!empty($data ?? null)) {
            $results['data'] = $data;
        }
        if (!empty($function ?? null)) {
            $results['meta']['function']['name'] = $function;
        }
        if (!empty($redirect ?? null)) {
            $results['links']['redirect'] = $redirect;
        } elseif ($reload) {
            $results['links']['reload'] = $reload;
        }
        return $results;
    }

    public function excelToPdf($exportCollection, $filename)
    {
        $collection = $exportCollection->collection();
        $headings = $exportCollection->headings();
        $snappy = app('snappy.pdf.wrapper');
        $options = (array) config('datatables-buttons.snappy.options');
        $orientation = config('datatables-buttons.snappy.orientation');
        $snappy->setOptions($options)->setOrientation($orientation);
        $data = $collection->map(function ($instance) use ($headings) {
            return array_combine($headings, $instance);
        });
        return $snappy->loadHTML(view($filename === 'orientation.pdf' ? "prints.orientation-pdf": 'datatables::print', compact('data')))->download($filename);
    }

    public function debug($input, $method)
    {
        if (!empty($input['debug'] ?? null) && filter_var($input['debug'], FILTER_VALIDATE_BOOLEAN)) {
            if (!empty($input['method'] ?? null)) {
                $methodName = Str::lower($input['method']);
            } else {
                $methodName = 'services';
            }
            $methodString = Str::lower(str_replace('\\', ' ', $method));
            if (Str::contains($methodString, $methodName)) {
                dd($method);
            }
        }
    }

    /* =========================================================================
    * Modules
    * =========================================================================
    */

    public function dataSwalOrientation($instance, $student): array
    {
        $accessTokenRequired = false;
        if (empty($student->user->createdByGoogleAccount->access_token)) {
            $accessTokenRequired = true;
        } elseif ($student->user->createdByGoogleAccount->expired_at->isPast()) {
            $accessTokenRequired = true;
        }
        return [
            'id' => $instance['uuid'],
            'student_id' => $student->uuid ?? null,
            'name' => $instance['name'],
            'orientation_type_id' => $instance['orientation_type_id'],
            'synopsis' => $instance['synopsis'],
            'livestream' => $instance['livestream'],
            'presenter' => $instance['presenter'],
            'agenda' => $instance['agenda'],
            'is_rsvp' => $instance['is_rsvp'],
            'is_go' => $instance->orientation_rsvp_is_go,
            'is_point' => $instance['is_point'],
            'is_type_activity' => $instance->is_type_activity,
            'point' => $instance->orientation_attendance_point,
            'point_min' => $instance->point_min,
            'point_max' => $instance->point_max,
            'attendance' => $instance->orientation_attendance_updated_at,
            'rating' => $instance->orientation_attendance_rating,
            'rating_html' => !empty($instance->orientation_attendance_rating ?? null) ? Blade::renderComponent(new FiveStar($instance->orientation_attendance_rating)) : null,
            'rule_can_scan_attendance' => $instance->rule_can_scan_attendance && (($instance->orientation_rsvp_is_go && $instance->orientation_type_id == OrientationTypeConstant::COMPLEMENTARY) || $instance->orientation_type_id == OrientationTypeConstant::COMPULSORY_CORE),
            'venue' => $instance->venue,
            'event' => $instance->venue . '<br>' . $instance->start_date_readable . ", {$instance->start_end_time_readable}",
            'start_datetime_iso_8601' => $instance->start_date_iso_8601,
            'start_end_date_dFY' => $instance->start_end_date_dFY,
            'start_date_dFY' => $instance->start_date_dFY,
            'start_end_time_readable' => $instance->start_end_time_readable,
            'end_datetime_iso_8601' => $instance->end_date_iso_8601,
            'access_token_required' => $accessTokenRequired,
        ];
    }
}
