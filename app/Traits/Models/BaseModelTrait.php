<?php

namespace App\Traits\Models;

use App\Vendors\Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

trait BaseModelTrait
{
    /* =========================================================================
    * Scope
    * =========================================================================
    */

    public function scopeWhenByArgs($query, $args, $table = null)
    {
        $table = !empty($table) ? "{$table}." : null;
        return $query->when(($args['created_at'] ?? null),
            function ($query) use ($args, $table) {
                if (is_array($args['created_at'])) {
                    $query->whereBetween(
                        "{$table}created_at",
                        $args['created_at']
                    );
                } else {
                    $query->where("{$table}created_at", $args['created_at']);
                }
            }
        )->when(($args['is_active'] ?? null),
            function ($query) use ($args, $table) {
                if (is_array($args['is_active'])) {
                    $query->whereIn("{$table}is_active", $args['is_active']);
                } else {
                    $query->where("{$table}is_active", $args['is_active']);
                }
            }
        )->when(($args['group_id'] ?? null),
            function ($query) use ($args, $table) {
                if (is_array($args['group_id'])) {
                    $query->whereIn("{$table}group_id", $args['group_id']);
                } else {
                    $query->where("{$table}group_id", $args['group_id']);
                }
            }
        )->when(($args['approval'] ?? null),
            function ($query) use ($args, $table) {
                if (is_array($args['approval'])) {
                    $query->whereIn("{$table}approval", $args['approval']);
                } else {
                    $query->where("{$table}approval", $args['approval']);
                }
            }
        )->when(($args['subgroup_id'] ?? null),
            function ($query) use ($args, $table) {
                if (is_array($args['subgroup_id'])) {
                    $query->whereIn(
                        "{$table}subgroup_id",
                        $args['subgroup_id']
                    );
                } else {
                    $query->where(
                        "{$table}subgroup_id",
                        $args['subgroup_id']
                    );
                }
            }
        )->when(($args['uuid'] ?? null),
            function ($query) use ($args, $table) {
                if (is_array($args['uuid'])) {
                    $query->whereIn("{$table}uuid", $args['uuid']);
                } else {
                    $query->where("{$table}uuid", $args['uuid']);
                }
            }
        );
    }
    /* =========================================================================
		 * Accessor & Mutator
		 * =========================================================================
		 */

    // public function getAttribute($key)
    // {
    //     switch ($key) {
    //         case "updated_at_readable":
    //         case "created_at_readable":
    //             $key = explode("_readable", $key)[0];
    //             $value = $this->getAttributeValue($key);
    //             return !empty($value) ? $value->format(config('app.date_format.php') . ' ' . config('app.time_format.php'))
    //                 : null;
    //         default:
    //             return parent::getAttribute($key);
    //     }
    // }
    /* =========================================================================
    * Other
    * =========================================================================
    */

    protected function startCloseAtReadable($start, $close)
    {
        $startAt = $this->getAttributeValue($start);
        $startAtFormat = !empty($startAt) ? $startAt->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
        $closeAt = $this->getAttributeValue($close);
        $closeAtFormat = !empty($closeAt) ? $closeAt->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null;
        if ($startAtFormat == $closeAtFormat) {
            return $startAtFormat;
        } elseif (!empty($startAtFormat) && empty($closeAtFormat)) {
            return $startAtFormat;
        } elseif (empty($startAtFormat) && !empty($closeAtFormat)) {
            return $closeAtFormat;
        } elseif (!empty($startAtFormat) && !empty($closeAtFormat)) {
            return $startAtFormat . ' - ' . $closeAtFormat;
        } else {
            return null;
        }
    }

    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }

    public function getUuidKey()
    {
        return $this->getAttribute('uuid');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (($model->hasUuid ?? false) && empty($model->uuid)) {
                $model->uuid = uniqueUuidByModel($model);
            }
        });
    }
}
