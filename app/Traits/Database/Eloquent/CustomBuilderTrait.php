<?php

namespace App\Traits\Database\Eloquent;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;


trait CustomBuilderTrait
{
    public function findOrFailByUuid($uuid, $columns = ['*'])
    {
        $result = $this->findByUuid($uuid, $columns);
        $uuid = $uuid instanceof Arrayable ? $uuid->toArray() : $uuid;
        if (is_array($uuid)) {
            if (count($result) === count(array_unique($uuid))) {
                return $result;
            }
        } elseif (!is_null($result)) {
            return $result;
        }
        throw (new ModelNotFoundException)->setModel(
            get_class($this->model),
            $uuid
        );
    }

    public function findByUuid($uuid, $columns = ['*'])
    {
        if (is_array($uuid) || $uuid instanceof Arrayable) {
            return $this->findMany($uuid, $columns);
        }
        return $this->whereUuidKey($uuid)->first($columns);
    }

    public function whereUuidKey($uuid)
    {
        if ($uuid instanceof Model) {
            $uuid = $uuid->getUuidKey();
        }
        if (is_array($uuid) || $uuid instanceof Arrayable) {
            $this->query->whereIn('uuid', $uuid);
            return $this;
        }
        if ($uuid !== null && $this->model->getKeyType() === 'string') {
            $uuid = (string) $uuid;
        }
        return $this->where('uuid', '=', $uuid);
    }

    /**
     * Find or fail by name
     * @param type $name
     * @param type $columns
     * @return type
     * @throws type
     */
    public function findOrFailByName($name, $columns = ['*'])
    {
        $result = $this->findByName($name, $columns);
        $name = $name instanceof Arrayable ? $name->toArray() : $name;
        if (is_array($name)) {
            if (count($result) === count(array_unique($name))) {
                return $result;
            }
        } elseif (!is_null($result)) {
            return $result;
        }
        throw (new ModelNotFoundException)->setModel(
            get_class($this->model),
            $name
        );
    }

    /**
     * Find by name
     * @param type $name
     * @param type $columns
     * @return type
     */
    public function findByName($name, $columns = ['*'])
    {
        return $this->where('name', $name)->first($columns);
    }

    /**
     * Find or fail by type and name
     * @param type $type
     * @param type $name
     * @param type $columns
     * @return type
     * @throws type
     */
    public function findOrFailByTypeName($type, $name, $columns = ['*'])
    {
        $result = $this->findByTypeName($type, $name, $columns);
        $type = $type instanceof Arrayable ? $type->toArray() : $type;
        $name = $name instanceof Arrayable ? $name->toArray() : $name;
        if (is_array($type) && is_array($name)) {
            if (count($result) === (count(array_unique($type)) && count(array_unique($name)))) {
                return $result;
            }
        } elseif (!is_null($result)) {
            return $result;
        }
        throw (new ModelNotFoundException)->setModel(
            get_class($this->model),
            $type
        );
    }

    /**
     * Find by type and name
     * @param type $type
     * @param type $name
     * @param type $columns
     * @return type
     */
    public function findByTypeName($type, $name, $columns = ['*'])
    {
        return $this->where(['type' => $type, 'name' => $name])->first($columns);
    }

    /**
     * Upsert or ignore the existing data. Avoid the gap of auto increment keep increasing.
     * @param array $inputs
     * @param array $uniqueBy
     * @return array
     */
    public function upsertUniqueBy(
        array $inputs,
        array $uniqueBy,
        $update = null
    ) {
        $chunkInputs = array_chunk($inputs, 2);
        foreach ($chunkInputs as $chunkInput) {
            $uniqueByInputs = [];
            $upsertInputs = [];
            $insertInputs = [];
            $existingInputs = $this->where(function ($query) use (
                $chunkInput,
                $uniqueBy,
                &$uniqueByInputs
            ) {
                foreach ($chunkInput as $index => $input) {
                    $uniqueByInputs[$index] = array_filter(
                        $input,
                        function ($uniqueByInput) use ($uniqueBy) {
                            return in_array($uniqueByInput, $uniqueBy);
                        },
                        ARRAY_FILTER_USE_KEY
                    );
                    $query->orWhere(function ($query2) use (
                        $index,
                        $uniqueByInputs
                    ) {
                        $query2->where($uniqueByInputs[$index]);
                    });
                }
            })->get()->makeHidden(['created_at', 'updated_at']);
            foreach ($chunkInput as $chunkIndex => $chunkValue) {
                $countExistingInputs = null;
                foreach ($uniqueByInputs[$chunkIndex] as $keyInput => $valueInput) {
                    if (empty($countExistingInputs)) {
                        $countExistingInputs = $existingInputs->where(
                            $keyInput,
                            $valueInput
                        );
                    } else {
                        $countExistingInputs = $countExistingInputs->where(
                            $keyInput,
                            $valueInput
                        );
                    }
                }
                if (
                    !empty($countExistingInputs) && $countExistingInputs->count()
                    > 0
                ) {
                    if (array_key_exists('uuid', $chunkValue)) {
                        unset($chunkValue['uuid']);
                    }
                    $upsertInputs[$chunkIndex] = array_merge(
                        $countExistingInputs->first()->toArray(),
                        $chunkValue
                    );
                } else {
                    if (array_key_exists('id', $chunkValue)) {
                        unset($chunkValue['id']);
                    }
                    $insertInputs[$chunkIndex] = $chunkValue;
                }
            }
            count($insertInputs) > 0 ? $this->upsert(
                $insertInputs,
                $uniqueBy,
                $update
            ) : null;
            count($upsertInputs) > 0 ? $this->upsert(
                $upsertInputs,
                $uniqueBy,
                $update
            ) : null;
        }
        return true;
    }
}
