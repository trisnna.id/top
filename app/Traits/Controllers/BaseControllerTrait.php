<?php

namespace App\Traits\Controllers;

use Illuminate\Http\Request;

trait BaseControllerTrait
{
    protected $service;

    /**
     * Pattern by iNKiLLeRz
     * Use variadic parameter in case to add more parameter
     * Check compatibility
     * @param Request $request
     */
    public function index(Request $request)
    {
        if (!empty($this->service ?? null)) {
            return $this->service->index($request, auth()->user());
        } else {
            abort(404);
        }
    }

    /**
     * Pattern by iNKiLLeRz
     * Use variadic parameter in case to add more parameter
     * Check compatibility
     * @param Request $request
     */
    public function create(Request $request)
    {
        if (!empty($this->service ?? null)) {
            return $this->service->create($request, auth()->user());
        } else {
            abort(404);
        }
    }

    /**
     * Pattern by iNKiLLeRz
     * Use variadic parameter in case to add more parameter
     * Check compatibility
     * @param Request $request
     */
    public function store(Request $request)
    {
        if (!empty($this->service ?? null)) {
            $results = $this->service->store($request->all(), auth()->user());
            return $this->arrayResponse($results);
        } else {
            abort(404);
        }
    }

    /**
     * Pattern by iNKiLLeRz
     * Use variadic parameter in case to add more parameter
     * Check compatibility
     * @param Request $request
     */
    public function show(Request $request, $uuid)
    {
        if (!empty($this->service ?? null)) {
            return $this->service->show($request, auth()->user(), $uuid);
        } else {
            abort(404);
        }
    }

    /**
     * Pattern by iNKiLLeRz
     * Use variadic parameter in case to add more parameter
     * Check compatibility
     * @param Request $request
     */
    public function edit(Request $request, $uuid)
    {
        if (!empty($this->service ?? null)) {
            return $this->service->edit($request, auth()->user(), $uuid);
        } else {
            abort(404);
        }
    }

    /**
     * Pattern by iNKiLLeRz
     * Use variadic parameter in case to add more parameter
     * Check compatibility
     * @param Request $request
     */
    public function update(Request $request, $uuid)
    {
        if (!empty($this->service ?? null)) {
            $results = $this->service->update(
                $request->all(),
                auth()->user(),
                $uuid
            );
            return $this->arrayResponse($results);
        } else {
            abort(404);
        }
    }

    /**
     * Pattern by iNKiLLeRz
     * Use variadic parameter in case to add more parameter
     * Check compatibility
     * @param Request $request
     */
    public function updateActive(Request $request, $uuid)
    {
        if (!empty($this->service ?? null)) {
            $results = $this->service->updateActive(
                $request->all(),
                auth()->user(),
                $uuid
            );
            return $this->arrayResponse($results);
        } else {
            abort(404);
        }
    }

    /**
     * Pattern by iNKiLLeRz
     * Use variadic parameter in case to add more parameter
     * Check compatibility
     * @param Request $request
     */
    public function updateApproval(Request $request, $uuid)
    {
        if (!empty($this->service ?? null)) {
            $results = $this->service->updateApproval(
                $request->all(),
                auth()->user(),
                $uuid
            );
            return $this->arrayResponse($results);
        } else {
            abort(404);
        }
    }

    public function updateBulkActive(Request $request)
    {
        if (!empty($this->service ?? null)) {
            $results = $this->service->updateBulkActive($request->all(), auth()->user(), true);
            return $this->arrayResponse($results);
        } else {
            abort(404);
        }
    }

    public function updateBulkInactive(Request $request)
    {
        if (!empty($this->service ?? null)) {
            $results = $this->service->updateBulkActive($request->all(), auth()->user(), false);
            return $this->arrayResponse($results);
        } else {
            abort(404);
        }
    }

    /**
     * Pattern by iNKiLLeRz
     * Use variadic parameter in case to add more parameter
     * Check compatibility
     * @param Request $request
     */
    public function destroy(Request $request, $uuid)
    {
        if (!empty($this->service ?? null)) {
            return $this->service->delete($request->all(), auth()->user(), $uuid);
        } else {
            abort(404);
        }
    }

    protected function arrayResponse($results)
    {
        if (is_array($results)) {
            return $results['response'];
        } else {
            return $results;
        }
    }
}
