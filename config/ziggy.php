<?php

return [
    'only' => [
        'portal.index',
        'orientations.index',
        'admin.orientations.index',
        'admin.orientations.calculate-student',
        'google-accounts.share-event',
        'google-accounts.share-events',
        'profile.notification-read.update',
        'dashboard.index',
    ],
];
