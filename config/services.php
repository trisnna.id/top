<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'google' => [
        'calendar' => [
            'client_id' => env('GOOGLE_CALENDAR_CLIENT_ID'),
            'client_secret' => env('GOOGLE_CALENDAR_CLIENT_SECRET'),
            'api_key' => env('GOOGLE_CALENDAR_API_KEY'),
            'discovery_doc' => env('GOOGLE_CALENDAR_DISCOVERY_DOC'),
            'scopes' => env('GOOGLE_CALENDAR_SCOPES'),
            'enabled' => env('GOOGLE_CALENDAR_ENABLED', false),
        ],
    ],
    'microsoft' => [ //farihintalib89@hotmail.com
        // 'client_id' => env('MICROSOFT_CLIENT_ID', '58d3d981-61c8-4bc5-b3f6-196bce44801e'),
        // 'client_secret' => env('MICROSOFT_CLIENT_SECRET', 'UVE8Q~97hGn6ntjoyq.wPc0HcnmxAqz3ZDfx1bFh'),
        // 'client_id' => env('MICROSOFT_CLIENT_ID', 'a435d17c-d005-43fe-9bfd-9c6dca844f74'),
        // 'client_secret' => env('MICROSOFT_CLIENT_SECRET', 'x388Q~W4VfXv2D_D04atZ7__yt948oZsfKyk3awN'),
        'client_id' => env('MICROSOFT_CLIENT_ID', '1266293b-ec4a-4259-969a-062459a9e86e'),
        'client_secret' => env('MICROSOFT_CLIENT_SECRET', 'ayT8Q~yHJmApMfziBhmF6ec1xGmx.gN34OKYoaFI'),
        'redirect' => env('MICROSOFT_REDIRECT_URI', 'https://top-dev.com/microsoft/login/callback'),
        'tenant' => env('MICROSOFT_TENANT_ID', '313c0d89-6b29-4e35-9b6d-d3c52b1c6ec7'),
        // 'tenant' => env('MICROSOFT_TENANT_ID', '9188040d-6c67-4c5b-b112-36a304b66dad'),
        // 'tenant' => env('MICROSOFT_TENANT_ID', 'common'),
        'include_tenant_info' => true,
    ],
    'taylors' => [
        'ldap' => [
            'student' => [
                'host' => env('TAYLORS_LDAP_STUDENT_HOST'),
                'domain' => env('TAYLORS_LDAP_STUDENT_DOMAIN'),
                'base_dn' => env('TAYLORS_LDAP_STUDENT_BASE_DN'),
            ],
            'staff' => [
                'host' => env('TAYLORS_LDAP_STAFF_HOST'),
                'domain' => env('TAYLORS_LDAP_STAFF_DOMAIN'),
                'base_dn' => env('TAYLORS_LDAP_STAFF_BASE_DN'),
            ],
            'port' => env('TAYLORS_LDAP_PORT'),
            'username' => env('TAYLORS_LDAP_USERNAME'),
            'password' => env('TAYLORS_LDAP_PASSWORD'),
            'opt_referrals' => env('TAYLORS_LDAP_OPT_REFERRALS'),
            'opt_protocol_version' => env('TAYLORS_LDAP_OPT_PROTOCOL_VERSION'),
            'enabled' => env('TAYLORS_LDAP_ENABLED', false),
        ],
        'cms' => [
            'enabled' => env('TAYLORS_CMS_ENABLED', false),
            'host' => env('TAYLORS_CMS_HOST'),
            'port' => env('TAYLORS_CMS_PORT'),
            'user' => env('TAYLORS_CMS_USER'),
            'password' => env('TAYLORS_CMS_PASSWORD'),
            'sid' => env('TAYLORS_CMS_SID'),
            'view' => [
                'student' => env('TAYLORS_CMS_VIEW_STUDENT')
            ],
        ],
        'topas' => [
            'endpoint' => env('TAYLORS_TOPAS_ENDPOINT', 'https://topas-stg.taylors.edu.my/api/oauth2/check-topas-user'),
            'token' => env('TAYLORS_TOPAS_TOKEN', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImNkNjRmYTVkZjhkYWY1ZDExOTgzMzQwYjc3MmUwZDA5OGEwNDM1ZGVlNmUwOGM5NGE0OWI1OGFhMjY2NmE0ZWE4MDIzN2Y5MjJhOGVhMmU5In0.eyJhdWQiOiIxIiwianRpIjoiY2Q2NGZhNWRmOGRhZjVkMTE5ODMzNDBiNzcyZTBkMDk4YTA0MzVkZWU2ZTA4Yzk0YTQ5YjU4YWEyNjY2YTRlYTgwMjM3ZjkyMmE4ZWEyZTkiLCJpYXQiOjE2Nzk2MzE4MDQsIm5iZiI6MTY3OTYzMTgwNCwiZXhwIjozMjU3NTU0OTkzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.XWHTzW9zzziz2tHbbJmCP53Ui9oWxgb_5T3HlZRwwfnsQukT6Vfa9A0Qd3Lmgv4xKZTCJjB6oifQTHrY339fkf_GW0RGbzft7fN9-QHlv-I-dA4oqDxqXWC_gIPCw3N1D8XIw56G3ydiSB5H5ACzaKFKcWUgVJyncsziemXVxxheqPDfQpmFzMQADY26fktOI-h9uCj2XeP1zZoygpDwO4UMKC4ipsWn1UPT09XtQHZebyvgOoy1U34G9WOrlkTzMZCLDo_sRzZwWO-PKw_DB_J1DbG4Epr7owsXs0GNggOgZBRcFVZc783rBbGnnLDGEyoGQWoqzU7a1wa7Rg7WxSPIwDkF_wUWhHRPnfvwIjfIdzUWOvHh-ChyR4G-hGp4cTKlxekjBZTGoeOCt3PyKWbIBtrJZ8jhj1TMvSGu_n5cgPc7sXOALjWcTBq0KspiVmB7d8K9ZjrEXgCcJdPaCEGT9SmRqGIV-JuvrJSW0bsNCTisSsACBB5xNUFoJAxrherhoCMACjSXgrk5z7muClbLtl4gvL-aTK6SgunjlgIUQKC6hZqXsx-IDlCOZG98I2lKqroX3bifNr3uljpi-IUrzUkdeen3OVVlKEf7w4EglKCioWcnOEhVCtoeGzm6lZzKBvQ-Nn03HeUZgCfTzOYfrZdq3B9mzhHZQbzRkc8'),
        ]
    ]
];
