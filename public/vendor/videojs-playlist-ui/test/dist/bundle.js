/*! @name videojs-playlist-ui @version 4.1.0 @license Apache-2.0 */
(function (QUnit, videojs) {
	'use strict';

	function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

	var QUnit__default = /*#__PURE__*/_interopDefaultLegacy(QUnit);
	var videojs__default = /*#__PURE__*/_interopDefaultLegacy(videojs);

	var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function getAugmentedNamespace(n) {
		if (n.__esModule) return n;
		var a = Object.defineProperty({}, '__esModule', {value: true});
		Object.keys(n).forEach(function (k) {
			var d = Object.getOwnPropertyDescriptor(n, k);
			Object.defineProperty(a, k, d.get ? d : {
				enumerable: true,
				get: function () {
					return n[k];
				}
			});
		});
		return a;
	}

	var _nodeResolve_empty = {};

	var _nodeResolve_empty$1 = /*#__PURE__*/Object.freeze({
		__proto__: null,
		'default': _nodeResolve_empty
	});

	var require$$0 = /*@__PURE__*/getAugmentedNamespace(_nodeResolve_empty$1);

	var topLevel = typeof commonjsGlobal !== 'undefined' ? commonjsGlobal : typeof window !== 'undefined' ? window : {};
	var minDoc = require$$0;
	var doccy;

	if (typeof document !== 'undefined') {
	  doccy = document;
	} else {
	  doccy = topLevel['__GLOBAL_DOCUMENT_CACHE@4'];

	  if (!doccy) {
	    doccy = topLevel['__GLOBAL_DOCUMENT_CACHE@4'] = minDoc;
	  }
	}

	var document_1 = doccy;

	var win;

	if (typeof window !== "undefined") {
	  win = window;
	} else if (typeof commonjsGlobal !== "undefined") {
	  win = commonjsGlobal;
	} else if (typeof self !== "undefined") {
	  win = self;
	} else {
	  win = {};
	}

	var window_1 = win;

	/*! @name videojs-playlist @version 4.3.1 @license Apache-2.0 */
	/**
	 * Validates a number of seconds to use as the auto-advance delay.
	 *
	 * @private
	 * @param   {number} s
	 *          The number to check
	 *
	 * @return  {boolean}
	 *          Whether this is a valid second or not
	 */

	var validSeconds = function validSeconds(s) {
	  return typeof s === 'number' && !isNaN(s) && s >= 0 && s < Infinity;
	};
	/**
	 * Resets the auto-advance behavior of a player.
	 *
	 * @param {Player} player
	 *        The player to reset the behavior on
	 */


	var reset = function reset(player) {
	  var aa = player.playlist.autoadvance_;

	  if (aa.timeout) {
	    player.clearTimeout(aa.timeout);
	  }

	  if (aa.trigger) {
	    player.off('ended', aa.trigger);
	  }

	  aa.timeout = null;
	  aa.trigger = null;
	};
	/**
	 * Sets up auto-advance behavior on a player.
	 *
	 * @param  {Player} player
	 *         the current player
	 *
	 * @param  {number} delay
	 *         The number of seconds to wait before each auto-advance.
	 *
	 * @return {undefined}
	 *         Used to short circuit function logic
	 */


	var setup$1 = function setup(player, delay) {
	  reset(player); // Before queuing up new auto-advance behavior, check if `seconds` was
	  // called with a valid value.

	  if (!validSeconds(delay)) {
	    player.playlist.autoadvance_.delay = null;
	    return;
	  }

	  player.playlist.autoadvance_.delay = delay;

	  player.playlist.autoadvance_.trigger = function () {
	    // This calls setup again, which will reset the existing auto-advance and
	    // set up another auto-advance for the next "ended" event.
	    var cancelOnPlay = function cancelOnPlay() {
	      return setup(player, delay);
	    }; // If there is a "play" event while we're waiting for an auto-advance,
	    // we need to cancel the auto-advance. This could mean the user seeked
	    // back into the content or restarted the content. This is reproducible
	    // with an auto-advance > 0.


	    player.one('play', cancelOnPlay);
	    player.playlist.autoadvance_.timeout = player.setTimeout(function () {
	      reset(player);
	      player.off('play', cancelOnPlay);
	      player.playlist.next();
	    }, delay * 1000);
	  };

	  player.one('ended', player.playlist.autoadvance_.trigger);
	};
	/**
	 * Removes all remote text tracks from a player.
	 *
	 * @param  {Player} player
	 *         The player to clear tracks on
	 */


	var clearTracks = function clearTracks(player) {
	  var tracks = player.remoteTextTracks();
	  var i = tracks && tracks.length || 0; // This uses a `while` loop rather than `forEach` because the
	  // `TextTrackList` object is a live DOM list (not an array).

	  while (i--) {
	    player.removeRemoteTextTrack(tracks[i]);
	  }
	};
	/**
	 * Plays an item on a player's playlist.
	 *
	 * @param  {Player} player
	 *         The player to play the item on
	 *
	 * @param  {Object} item
	 *         A source from the playlist.
	 *
	 * @return {Player}
	 *         The player that is now playing the item
	 */


	var playItem = function playItem(player, item) {
	  var replay = !player.paused() || player.ended();
	  player.trigger('beforeplaylistitem', item.originalValue || item);

	  if (item.playlistItemId_) {
	    player.playlist.currentPlaylistItemId_ = item.playlistItemId_;
	  }

	  player.poster(item.poster || '');
	  player.src(item.sources);
	  clearTracks(player);
	  player.ready(function () {
	    (item.textTracks || []).forEach(player.addRemoteTextTrack.bind(player));
	    player.trigger('playlistitem', item.originalValue || item);

	    if (replay) {
	      var playPromise = player.play(); // silence error when a pause interrupts a play request
	      // on browsers which return a promise

	      if (typeof playPromise !== 'undefined' && typeof playPromise.then === 'function') {
	        playPromise.then(null, function (e) {});
	      }
	    }

	    setup$1(player, player.playlist.autoadvance_.delay);
	  });
	  return player;
	};
	/**
	 * Returns whether a playlist item is an object of any kind, excluding null.
	 *
	 * @private
	 *
	 * @param {Object}
	 *         value to be checked
	 *
	 * @return {boolean}
	 *          The result
	 */


	var isItemObject = function isItemObject(value) {
	  return !!value && typeof value === 'object';
	};
	/**
	 * Look through an array of playlist items and transform any primitive
	 * as well as null values to objects. This method also adds a property
	 * to the transformed item containing original value passed in an input list.
	 *
	 * @private
	 *
	 * @param  {Array} arr
	 *         An array of playlist items
	 *
	 * @return {Array}
	 *         A new array with transformed items
	 */


	var transformPrimitiveItems = function transformPrimitiveItems(arr) {
	  var list = [];
	  var tempItem;
	  arr.forEach(function (item) {
	    if (!isItemObject(item)) {
	      tempItem = Object(item);
	      tempItem.originalValue = item;
	    } else {
	      tempItem = item;
	    }

	    list.push(tempItem);
	  });
	  return list;
	};
	/**
	 * Generate a unique id for each playlist item object. This id will be used to determine
	 * index of an item in the playlist array for cases where there are multiple items with
	 * the same source set.
	 *
	 * @private
	 *
	 * @param  {Array} arr
	 *         An array of playlist items
	 */


	var generatePlaylistItemId = function generatePlaylistItemId(arr) {
	  var guid = 1;
	  arr.forEach(function (item) {
	    item.playlistItemId_ = guid++;
	  });
	};
	/**
	 * Look through an array of playlist items for a specific playlist item id.
	 *
	 * @private
	 * @param   {Array} list
	 *          An array of playlist items to look through
	 *
	 * @param   {number} currentItemId
	 *          The current item ID.
	 *
	 * @return  {number}
	 *          The index of the playlist item or -1 if not found
	 */


	var indexInPlaylistItemIds = function indexInPlaylistItemIds(list, currentItemId) {
	  for (var i = 0; i < list.length; i++) {
	    if (list[i].playlistItemId_ === currentItemId) {
	      return i;
	    }
	  }

	  return -1;
	};
	/**
	 * Given two sources, check to see whether the two sources are equal.
	 * If both source urls have a protocol, the protocols must match, otherwise, protocols
	 * are ignored.
	 *
	 * @private
	 * @param {string|Object} source1
	 *        The first source
	 *
	 * @param {string|Object} source2
	 *        The second source
	 *
	 * @return {boolean}
	 *         The result
	 */


	var sourceEquals = function sourceEquals(source1, source2) {
	  var src1 = source1;
	  var src2 = source2;

	  if (typeof source1 === 'object') {
	    src1 = source1.src;
	  }

	  if (typeof source2 === 'object') {
	    src2 = source2.src;
	  }

	  if (/^\/\//.test(src1)) {
	    src2 = src2.slice(src2.indexOf('//'));
	  }

	  if (/^\/\//.test(src2)) {
	    src1 = src1.slice(src1.indexOf('//'));
	  }

	  return src1 === src2;
	};
	/**
	 * Look through an array of playlist items for a specific `source`;
	 * checking both the value of elements and the value of their `src`
	 * property.
	 *
	 * @private
	 * @param   {Array} arr
	 *          An array of playlist items to look through
	 *
	 * @param   {string} src
	 *          The source to look for
	 *
	 * @return  {number}
	 *          The index of that source or -1
	 */


	var indexInSources = function indexInSources(arr, src) {
	  for (var i = 0; i < arr.length; i++) {
	    var sources = arr[i].sources;

	    if (Array.isArray(sources)) {
	      for (var j = 0; j < sources.length; j++) {
	        var source = sources[j];

	        if (source && sourceEquals(source, src)) {
	          return i;
	        }
	      }
	    }
	  }

	  return -1;
	};
	/**
	 * Randomize the contents of an array.
	 *
	 * @private
	 * @param  {Array} arr
	 *         An array.
	 *
	 * @return {Array}
	 *         The same array that was passed in.
	 */


	var randomize = function randomize(arr) {
	  var index = -1;
	  var lastIndex = arr.length - 1;

	  while (++index < arr.length) {
	    var rand = index + Math.floor(Math.random() * (lastIndex - index + 1));
	    var value = arr[rand];
	    arr[rand] = arr[index];
	    arr[index] = value;
	  }

	  return arr;
	};
	/**
	 * Factory function for creating new playlist implementation on the given player.
	 *
	 * API summary:
	 *
	 * playlist(['a', 'b', 'c']) // setter
	 * playlist() // getter
	 * playlist.currentItem() // getter, 0
	 * playlist.currentItem(1) // setter, 1
	 * playlist.next() // 'c'
	 * playlist.previous() // 'b'
	 * playlist.first() // 'a'
	 * playlist.last() // 'c'
	 * playlist.autoadvance(5) // 5 second delay
	 * playlist.autoadvance() // cancel autoadvance
	 *
	 * @param  {Player} player
	 *         The current player
	 *
	 * @param  {Array=} initialList
	 *         If given, an initial list of sources with which to populate
	 *         the playlist.
	 *
	 * @param  {number=}  initialIndex
	 *         If given, the index of the item in the list that should
	 *         be loaded first. If -1, no video is loaded. If omitted, The
	 *         the first video is loaded.
	 *
	 * @return {Function}
	 *         Returns the playlist function specific to the given player.
	 */


	function factory(player, initialList, initialIndex) {
	  if (initialIndex === void 0) {
	    initialIndex = 0;
	  }

	  var list = null;
	  var changing = false;
	  /**
	   * Get/set the playlist for a player.
	   *
	   * This function is added as an own property of the player and has its
	   * own methods which can be called to manipulate the internal state.
	   *
	   * @param  {Array} [newList]
	   *         If given, a new list of sources with which to populate the
	   *         playlist. Without this, the function acts as a getter.
	   *
	   * @param  {number}  [newIndex]
	   *         If given, the index of the item in the list that should
	   *         be loaded first. If -1, no video is loaded. If omitted, The
	   *         the first video is loaded.
	   *
	   * @return {Array}
	   *         The playlist
	   */

	  var playlist = player.playlist = function (newList, newIndex) {
	    if (newIndex === void 0) {
	      newIndex = 0;
	    }

	    if (changing) {
	      throw new Error('do not call playlist() during a playlist change');
	    }

	    if (Array.isArray(newList)) {
	      // @todo - Simplify this to `list.slice()` for v5.
	      var previousPlaylist = Array.isArray(list) ? list.slice() : null;
	      var nextPlaylist = newList.slice();
	      list = nextPlaylist.slice(); // Transform any primitive and null values in an input list to objects

	      if (list.filter(function (item) {
	        return isItemObject(item);
	      }).length !== list.length) {
	        list = transformPrimitiveItems(list);
	      } // Add unique id to each playlist item. This id will be used
	      // to determine index in cases where there are more than one
	      // identical sources in the playlist.


	      generatePlaylistItemId(list); // Mark the playlist as changing during the duringplaylistchange lifecycle.

	      changing = true;
	      player.trigger({
	        type: 'duringplaylistchange',
	        nextIndex: newIndex,
	        nextPlaylist: nextPlaylist,
	        previousIndex: playlist.currentIndex_,
	        // @todo - Simplify this to simply pass along `previousPlaylist` for v5.
	        previousPlaylist: previousPlaylist || []
	      });
	      changing = false;

	      if (newIndex !== -1) {
	        playlist.currentItem(newIndex);
	      } // The only time the previous playlist is null is the first call to this
	      // function. This allows us to fire the `duringplaylistchange` event
	      // every time the playlist is populated and to maintain backward
	      // compatibility by not firing the `playlistchange` event on the initial
	      // population of the list.
	      //
	      // @todo - Remove this condition in preparation for v5.


	      if (previousPlaylist) {
	        player.setTimeout(function () {
	          player.trigger('playlistchange');
	        }, 0);
	      }
	    } // Always return a shallow clone of the playlist list.
	    //  We also want to return originalValue if any item in the list has it.


	    return list.map(function (item) {
	      return item.originalValue || item;
	    }).slice();
	  }; // On a new source, if there is no current item, disable auto-advance.


	  player.on('loadstart', function () {
	    if (playlist.currentItem() === -1) {
	      reset(player);
	    }
	  });
	  playlist.currentIndex_ = -1;
	  playlist.player_ = player;
	  playlist.autoadvance_ = {};
	  playlist.repeat_ = false;
	  playlist.currentPlaylistItemId_ = null;
	  /**
	   * Get or set the current item in the playlist.
	   *
	   * During the duringplaylistchange event, acts only as a getter.
	   *
	   * @param  {number} [index]
	   *         If given as a valid value, plays the playlist item at that index.
	   *
	   * @return {number}
	   *         The current item index.
	   */

	  playlist.currentItem = function (index) {
	    // If the playlist is changing, only act as a getter.
	    if (changing) {
	      return playlist.currentIndex_;
	    } // Act as a setter when the index is given and is a valid number.


	    if (typeof index === 'number' && playlist.currentIndex_ !== index && index >= 0 && index < list.length) {
	      playlist.currentIndex_ = index;
	      playItem(playlist.player_, list[playlist.currentIndex_]);
	      return playlist.currentIndex_;
	    }

	    var src = playlist.player_.currentSrc() || ''; // If there is a currentPlaylistItemId_, validate that it matches the
	    // current source URL returned by the player. This is sufficient evidence
	    // to suggest that the source was set by the playlist plugin. This code
	    // exists primarily to deal with playlists where multiple items have the
	    // same source.

	    if (playlist.currentPlaylistItemId_) {
	      var indexInItemIds = indexInPlaylistItemIds(list, playlist.currentPlaylistItemId_);
	      var item = list[indexInItemIds]; // Found a match, this is our current index!

	      if (item && Array.isArray(item.sources) && indexInSources([item], src) > -1) {
	        playlist.currentIndex_ = indexInItemIds;
	        return playlist.currentIndex_;
	      } // If this does not match the current source, null it out so subsequent
	      // calls can skip this step.


	      playlist.currentPlaylistItemId_ = null;
	    } // Finally, if we don't have a valid, current playlist item ID, we can
	    // auto-detect it based on the player's current source URL.


	    playlist.currentIndex_ = playlist.indexOf(src);
	    return playlist.currentIndex_;
	  };
	  /**
	   * Checks if the playlist contains a value.
	   *
	   * @param  {string|Object|Array} value
	   *         The value to check
	   *
	   * @return {boolean}
	   *         The result
	   */


	  playlist.contains = function (value) {
	    return playlist.indexOf(value) !== -1;
	  };
	  /**
	   * Gets the index of a value in the playlist or -1 if not found.
	   *
	   * @param  {string|Object|Array} value
	   *         The value to find the index of
	   *
	   * @return {number}
	   *         The index or -1
	   */


	  playlist.indexOf = function (value) {
	    if (typeof value === 'string') {
	      return indexInSources(list, value);
	    }

	    var sources = Array.isArray(value) ? value : value.sources;

	    for (var i = 0; i < sources.length; i++) {
	      var source = sources[i];

	      if (typeof source === 'string') {
	        return indexInSources(list, source);
	      } else if (source.src) {
	        return indexInSources(list, source.src);
	      }
	    }

	    return -1;
	  };
	  /**
	   * Get the index of the current item in the playlist. This is identical to
	   * calling `currentItem()` with no arguments.
	   *
	   * @return {number}
	   *         The current item index.
	   */


	  playlist.currentIndex = function () {
	    return playlist.currentItem();
	  };
	  /**
	   * Get the index of the last item in the playlist.
	   *
	   * @return {number}
	   *         The index of the last item in the playlist or -1 if there are no
	   *         items.
	   */


	  playlist.lastIndex = function () {
	    return list.length - 1;
	  };
	  /**
	   * Get the index of the next item in the playlist.
	   *
	   * @return {number}
	   *         The index of the next item in the playlist or -1 if there is no
	   *         current item.
	   */


	  playlist.nextIndex = function () {
	    var current = playlist.currentItem();

	    if (current === -1) {
	      return -1;
	    }

	    var lastIndex = playlist.lastIndex(); // When repeating, loop back to the beginning on the last item.

	    if (playlist.repeat_ && current === lastIndex) {
	      return 0;
	    } // Don't go past the end of the playlist.


	    return Math.min(current + 1, lastIndex);
	  };
	  /**
	   * Get the index of the previous item in the playlist.
	   *
	   * @return {number}
	   *         The index of the previous item in the playlist or -1 if there is
	   *         no current item.
	   */


	  playlist.previousIndex = function () {
	    var current = playlist.currentItem();

	    if (current === -1) {
	      return -1;
	    } // When repeating, loop back to the end of the playlist.


	    if (playlist.repeat_ && current === 0) {
	      return playlist.lastIndex();
	    } // Don't go past the beginning of the playlist.


	    return Math.max(current - 1, 0);
	  };
	  /**
	   * Plays the first item in the playlist.
	   *
	   * @return {Object|undefined}
	   *         Returns undefined and has no side effects if the list is empty.
	   */


	  playlist.first = function () {
	    if (changing) {
	      return;
	    }

	    var newItem = playlist.currentItem(0);

	    if (list.length) {
	      return list[newItem].originalValue || list[newItem];
	    }

	    playlist.currentIndex_ = -1;
	  };
	  /**
	   * Plays the last item in the playlist.
	   *
	   * @return {Object|undefined}
	   *         Returns undefined and has no side effects if the list is empty.
	   */


	  playlist.last = function () {
	    if (changing) {
	      return;
	    }

	    var newItem = playlist.currentItem(playlist.lastIndex());

	    if (list.length) {
	      return list[newItem].originalValue || list[newItem];
	    }

	    playlist.currentIndex_ = -1;
	  };
	  /**
	   * Plays the next item in the playlist.
	   *
	   * @return {Object|undefined}
	   *         Returns undefined and has no side effects if on last item.
	   */


	  playlist.next = function () {
	    if (changing) {
	      return;
	    }

	    var index = playlist.nextIndex();

	    if (index !== playlist.currentIndex_) {
	      var newItem = playlist.currentItem(index);
	      return list[newItem].originalValue || list[newItem];
	    }
	  };
	  /**
	   * Plays the previous item in the playlist.
	   *
	   * @return {Object|undefined}
	   *         Returns undefined and has no side effects if on first item.
	   */


	  playlist.previous = function () {
	    if (changing) {
	      return;
	    }

	    var index = playlist.previousIndex();

	    if (index !== playlist.currentIndex_) {
	      var newItem = playlist.currentItem(index);
	      return list[newItem].originalValue || list[newItem];
	    }
	  };
	  /**
	   * Set up auto-advance on the playlist.
	   *
	   * @param  {number} [delay]
	   *         The number of seconds to wait before each auto-advance.
	   */


	  playlist.autoadvance = function (delay) {
	    setup$1(playlist.player_, delay);
	  };
	  /**
	   * Sets `repeat` option, which makes the "next" video of the last video in
	   * the playlist be the first video in the playlist.
	   *
	   * @param  {boolean} [val]
	   *         The value to set repeat to
	   *
	   * @return {boolean}
	   *         The current value of repeat
	   */


	  playlist.repeat = function (val) {
	    if (val === undefined) {
	      return playlist.repeat_;
	    }

	    if (typeof val !== 'boolean') {
	      videojs__default["default"].log.error('videojs-playlist: Invalid value for repeat', val);
	      return;
	    }

	    playlist.repeat_ = !!val;
	    return playlist.repeat_;
	  };
	  /**
	   * Sorts the playlist array.
	   *
	   * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort}
	   * @fires playlistsorted
	   *
	   * @param {Function} compare
	   *        A comparator function as per the native Array method.
	   */


	  playlist.sort = function (compare) {
	    // Bail if the array is empty.
	    if (!list.length) {
	      return;
	    }

	    list.sort(compare); // If the playlist is changing, don't trigger events.

	    if (changing) {
	      return;
	    }
	    /**
	     * Triggered after the playlist is sorted internally.
	     *
	     * @event playlistsorted
	     * @type {Object}
	     */


	    player.trigger('playlistsorted');
	  };
	  /**
	   * Reverses the playlist array.
	   *
	   * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse}
	   * @fires playlistsorted
	   */


	  playlist.reverse = function () {
	    // Bail if the array is empty.
	    if (!list.length) {
	      return;
	    }

	    list.reverse(); // If the playlist is changing, don't trigger events.

	    if (changing) {
	      return;
	    }
	    /**
	     * Triggered after the playlist is sorted internally.
	     *
	     * @event playlistsorted
	     * @type {Object}
	     */


	    player.trigger('playlistsorted');
	  };
	  /**
	   * Shuffle the contents of the list randomly.
	   *
	   * @see   {@link https://github.com/lodash/lodash/blob/40e096b6d5291a025e365a0f4c010d9a0efb9a69/shuffle.js}
	   * @fires playlistsorted
	   * @todo  Make the `rest` option default to `true` in v5.0.0.
	   * @param {Object} [options]
	   *        An object containing shuffle options.
	   *
	   * @param {boolean} [options.rest = false]
	   *        By default, the entire playlist is randomized. However, this may
	   *        not be desirable in all cases, such as when a user is already
	   *        watching a video.
	   *
	   *        When `true` is passed for this option, it will only shuffle
	   *        playlist items after the current item. For example, when on the
	   *        first item, will shuffle the second item and beyond.
	   */


	  playlist.shuffle = function (_temp) {
	    var _ref = _temp === void 0 ? {} : _temp,
	        rest = _ref.rest;

	    var index = 0;
	    var arr = list; // When options.rest is true, start randomization at the item after the
	    // current item.

	    if (rest) {
	      index = playlist.currentIndex_ + 1;
	      arr = list.slice(index);
	    } // Bail if the array is empty or too short to shuffle.


	    if (arr.length <= 1) {
	      return;
	    }

	    randomize(arr); // When options.rest is true, splice the randomized sub-array back into
	    // the original array.

	    if (rest) {
	      var _list;

	      (_list = list).splice.apply(_list, [index, arr.length].concat(arr));
	    } // If the playlist is changing, don't trigger events.


	    if (changing) {
	      return;
	    }
	    /**
	     * Triggered after the playlist is sorted internally.
	     *
	     * @event playlistsorted
	     * @type {Object}
	     */


	    player.trigger('playlistsorted');
	  }; // If an initial list was given, populate the playlist with it.


	  if (Array.isArray(initialList)) {
	    playlist(initialList.slice(), initialIndex); // If there is no initial list given, silently set an empty array.
	  } else {
	    list = [];
	  }

	  return playlist;
	}

	var version$1 = "4.3.1";
	var registerPlugin$1 = videojs__default["default"].registerPlugin || videojs__default["default"].plugin;
	/**
	 * The video.js playlist plugin. Invokes the playlist-maker to create a
	 * playlist function on the specific player.
	 *
	 * @param {Array} list
	 *        a list of sources
	 *
	 * @param {number} item
	 *        The index to start at
	 */

	var plugin = function plugin(list, item) {
	  factory(this, list, item);
	};

	registerPlugin$1('playlist', plugin);
	plugin.VERSION = version$1;

	var version = "4.1.0";

	function cov_2c38f07m8x() {
	  var path = "/Users/bclifford/Code/videojs-playlist-ui/src/plugin.js";
	  var hash = "16d69ac6050c7f318cf74aa8c24646dce2711f25";
	  var global = new Function("return this")();
	  var gcv = "__coverage__";
	  var coverageData = {
	    path: "/Users/bclifford/Code/videojs-playlist-ui/src/plugin.js",
	    statementMap: {
	      "0": {
	        start: {
	          line: 5,
	          column: 12
	        },
	        end: {
	          line: 5,
	          column: 34
	        }
	      },
	      "1": {
	        start: {
	          line: 6,
	          column: 23
	        },
	        end: {
	          line: 6,
	          column: 63
	        }
	      },
	      "2": {
	        start: {
	          line: 9,
	          column: 33
	        },
	        end: {
	          line: 14,
	          column: 4
	        }
	      },
	      "3": {
	        start: {
	          line: 10,
	          column: 18
	        },
	        end: {
	          line: 10,
	          column: 45
	        }
	      },
	      "4": {
	        start: {
	          line: 12,
	          column: 2
	        },
	        end: {
	          line: 12,
	          column: 48
	        }
	      },
	      "5": {
	        start: {
	          line: 13,
	          column: 2
	        },
	        end: {
	          line: 13,
	          column: 48
	        }
	      },
	      "6": {
	        start: {
	          line: 16,
	          column: 17
	        },
	        end: {
	          line: 20,
	          column: 1
	        }
	      },
	      "7": {
	        start: {
	          line: 25,
	          column: 25
	        },
	        end: {
	          line: 27,
	          column: 1
	        }
	      },
	      "8": {
	        start: {
	          line: 26,
	          column: 2
	        },
	        end: {
	          line: 26,
	          column: 30
	        }
	      },
	      "9": {
	        start: {
	          line: 28,
	          column: 28
	        },
	        end: {
	          line: 34,
	          column: 1
	        }
	      },
	      "10": {
	        start: {
	          line: 29,
	          column: 2
	        },
	        end: {
	          line: 29,
	          column: 33
	        }
	      },
	      "11": {
	        start: {
	          line: 31,
	          column: 2
	        },
	        end: {
	          line: 33,
	          column: 3
	        }
	      },
	      "12": {
	        start: {
	          line: 32,
	          column: 4
	        },
	        end: {
	          line: 32,
	          column: 62
	        }
	      },
	      "13": {
	        start: {
	          line: 36,
	          column: 15
	        },
	        end: {
	          line: 38,
	          column: 1
	        }
	      },
	      "14": {
	        start: {
	          line: 37,
	          column: 2
	        },
	        end: {
	          line: 37,
	          column: 29
	        }
	      },
	      "15": {
	        start: {
	          line: 39,
	          column: 18
	        },
	        end: {
	          line: 41,
	          column: 1
	        }
	      },
	      "16": {
	        start: {
	          line: 40,
	          column: 2
	        },
	        end: {
	          line: 40,
	          column: 32
	        }
	      },
	      "17": {
	        start: {
	          line: 43,
	          column: 24
	        },
	        end: {
	          line: 91,
	          column: 1
	        }
	      },
	      "18": {
	        start: {
	          line: 44,
	          column: 2
	        },
	        end: {
	          line: 49,
	          column: 3
	        }
	      },
	      "19": {
	        start: {
	          line: 45,
	          column: 24
	        },
	        end: {
	          line: 45,
	          column: 53
	        }
	      },
	      "20": {
	        start: {
	          line: 47,
	          column: 4
	        },
	        end: {
	          line: 47,
	          column: 88
	        }
	      },
	      "21": {
	        start: {
	          line: 48,
	          column: 4
	        },
	        end: {
	          line: 48,
	          column: 23
	        }
	      },
	      "22": {
	        start: {
	          line: 51,
	          column: 18
	        },
	        end: {
	          line: 51,
	          column: 51
	        }
	      },
	      "23": {
	        start: {
	          line: 53,
	          column: 2
	        },
	        end: {
	          line: 53,
	          column: 47
	        }
	      },
	      "24": {
	        start: {
	          line: 55,
	          column: 2
	        },
	        end: {
	          line: 89,
	          column: 3
	        }
	      },
	      "25": {
	        start: {
	          line: 57,
	          column: 16
	        },
	        end: {
	          line: 57,
	          column: 45
	        }
	      },
	      "26": {
	        start: {
	          line: 59,
	          column: 4
	        },
	        end: {
	          line: 59,
	          column: 25
	        }
	      },
	      "27": {
	        start: {
	          line: 60,
	          column: 4
	        },
	        end: {
	          line: 60,
	          column: 24
	        }
	      },
	      "28": {
	        start: {
	          line: 61,
	          column: 4
	        },
	        end: {
	          line: 61,
	          column: 17
	        }
	      },
	      "29": {
	        start: {
	          line: 62,
	          column: 4
	        },
	        end: {
	          line: 62,
	          column: 29
	        }
	      },
	      "30": {
	        start: {
	          line: 68,
	          column: 4
	        },
	        end: {
	          line: 77,
	          column: 5
	        }
	      },
	      "31": {
	        start: {
	          line: 68,
	          column: 17
	        },
	        end: {
	          line: 68,
	          column: 18
	        }
	      },
	      "32": {
	        start: {
	          line: 69,
	          column: 22
	        },
	        end: {
	          line: 69,
	          column: 34
	        }
	      },
	      "33": {
	        start: {
	          line: 70,
	          column: 21
	        },
	        end: {
	          line: 70,
	          column: 53
	        }
	      },
	      "34": {
	        start: {
	          line: 73,
	          column: 6
	        },
	        end: {
	          line: 75,
	          column: 7
	        }
	      },
	      "35": {
	        start: {
	          line: 74,
	          column: 8
	        },
	        end: {
	          line: 74,
	          column: 37
	        }
	      },
	      "36": {
	        start: {
	          line: 76,
	          column: 6
	        },
	        end: {
	          line: 76,
	          column: 34
	        }
	      },
	      "37": {
	        start: {
	          line: 80,
	          column: 20
	        },
	        end: {
	          line: 80,
	          column: 51
	        }
	      },
	      "38": {
	        start: {
	          line: 81,
	          column: 16
	        },
	        end: {
	          line: 81,
	          column: 45
	        }
	      },
	      "39": {
	        start: {
	          line: 83,
	          column: 4
	        },
	        end: {
	          line: 83,
	          column: 25
	        }
	      },
	      "40": {
	        start: {
	          line: 84,
	          column: 4
	        },
	        end: {
	          line: 84,
	          column: 17
	        }
	      },
	      "41": {
	        start: {
	          line: 85,
	          column: 4
	        },
	        end: {
	          line: 87,
	          column: 5
	        }
	      },
	      "42": {
	        start: {
	          line: 86,
	          column: 6
	        },
	        end: {
	          line: 86,
	          column: 32
	        }
	      },
	      "43": {
	        start: {
	          line: 88,
	          column: 4
	        },
	        end: {
	          line: 88,
	          column: 29
	        }
	      },
	      "44": {
	        start: {
	          line: 90,
	          column: 2
	        },
	        end: {
	          line: 90,
	          column: 17
	        }
	      },
	      "45": {
	        start: {
	          line: 93,
	          column: 18
	        },
	        end: {
	          line: 93,
	          column: 51
	        }
	      },
	      "46": {
	        start: {
	          line: 98,
	          column: 4
	        },
	        end: {
	          line: 100,
	          column: 5
	        }
	      },
	      "47": {
	        start: {
	          line: 99,
	          column: 6
	        },
	        end: {
	          line: 99,
	          column: 84
	        }
	      },
	      "48": {
	        start: {
	          line: 102,
	          column: 4
	        },
	        end: {
	          line: 102,
	          column: 60
	        }
	      },
	      "49": {
	        start: {
	          line: 103,
	          column: 4
	        },
	        end: {
	          line: 103,
	          column: 32
	        }
	      },
	      "50": {
	        start: {
	          line: 104,
	          column: 4
	        },
	        end: {
	          line: 104,
	          column: 34
	        }
	      },
	      "51": {
	        start: {
	          line: 106,
	          column: 4
	        },
	        end: {
	          line: 106,
	          column: 46
	        }
	      },
	      "52": {
	        start: {
	          line: 108,
	          column: 4
	        },
	        end: {
	          line: 108,
	          column: 25
	        }
	      },
	      "53": {
	        start: {
	          line: 110,
	          column: 4
	        },
	        end: {
	          line: 110,
	          column: 56
	        }
	      },
	      "54": {
	        start: {
	          line: 111,
	          column: 4
	        },
	        end: {
	          line: 111,
	          column: 44
	        }
	      },
	      "55": {
	        start: {
	          line: 118,
	          column: 4
	        },
	        end: {
	          line: 120,
	          column: 5
	        }
	      },
	      "56": {
	        start: {
	          line: 119,
	          column: 6
	        },
	        end: {
	          line: 119,
	          column: 33
	        }
	      },
	      "57": {
	        start: {
	          line: 124,
	          column: 4
	        },
	        end: {
	          line: 124,
	          column: 82
	        }
	      },
	      "58": {
	        start: {
	          line: 125,
	          column: 4
	        },
	        end: {
	          line: 127,
	          column: 5
	        }
	      },
	      "59": {
	        start: {
	          line: 126,
	          column: 6
	        },
	        end: {
	          line: 126,
	          column: 26
	        }
	      },
	      "60": {
	        start: {
	          line: 131,
	          column: 15
	        },
	        end: {
	          line: 131,
	          column: 43
	        }
	      },
	      "61": {
	        start: {
	          line: 132,
	          column: 17
	        },
	        end: {
	          line: 132,
	          column: 35
	        }
	      },
	      "62": {
	        start: {
	          line: 133,
	          column: 28
	        },
	        end: {
	          line: 133,
	          column: 57
	        }
	      },
	      "63": {
	        start: {
	          line: 135,
	          column: 4
	        },
	        end: {
	          line: 143,
	          column: 5
	        }
	      },
	      "64": {
	        start: {
	          line: 136,
	          column: 23
	        },
	        end: {
	          line: 136,
	          column: 45
	        }
	      },
	      "65": {
	        start: {
	          line: 138,
	          column: 6
	        },
	        end: {
	          line: 142,
	          column: 9
	        }
	      },
	      "66": {
	        start: {
	          line: 139,
	          column: 22
	        },
	        end: {
	          line: 139,
	          column: 36
	        }
	      },
	      "67": {
	        start: {
	          line: 141,
	          column: 8
	        },
	        end: {
	          line: 141,
	          column: 32
	        }
	      },
	      "68": {
	        start: {
	          line: 145,
	          column: 4
	        },
	        end: {
	          line: 145,
	          column: 39
	        }
	      },
	      "69": {
	        start: {
	          line: 146,
	          column: 4
	        },
	        end: {
	          line: 146,
	          column: 35
	        }
	      },
	      "70": {
	        start: {
	          line: 149,
	          column: 4
	        },
	        end: {
	          line: 149,
	          column: 53
	        }
	      },
	      "71": {
	        start: {
	          line: 150,
	          column: 4
	        },
	        end: {
	          line: 150,
	          column: 35
	        }
	      },
	      "72": {
	        start: {
	          line: 153,
	          column: 4
	        },
	        end: {
	          line: 161,
	          column: 5
	        }
	      },
	      "73": {
	        start: {
	          line: 154,
	          column: 23
	        },
	        end: {
	          line: 154,
	          column: 53
	        }
	      },
	      "74": {
	        start: {
	          line: 155,
	          column: 19
	        },
	        end: {
	          line: 155,
	          column: 52
	        }
	      },
	      "75": {
	        start: {
	          line: 157,
	          column: 6
	        },
	        end: {
	          line: 157,
	          column: 51
	        }
	      },
	      "76": {
	        start: {
	          line: 158,
	          column: 6
	        },
	        end: {
	          line: 158,
	          column: 72
	        }
	      },
	      "77": {
	        start: {
	          line: 159,
	          column: 6
	        },
	        end: {
	          line: 159,
	          column: 58
	        }
	      },
	      "78": {
	        start: {
	          line: 160,
	          column: 6
	        },
	        end: {
	          line: 160,
	          column: 31
	        }
	      },
	      "79": {
	        start: {
	          line: 164,
	          column: 25
	        },
	        end: {
	          line: 164,
	          column: 55
	        }
	      },
	      "80": {
	        start: {
	          line: 165,
	          column: 27
	        },
	        end: {
	          line: 165,
	          column: 55
	        }
	      },
	      "81": {
	        start: {
	          line: 167,
	          column: 4
	        },
	        end: {
	          line: 167,
	          column: 61
	        }
	      },
	      "82": {
	        start: {
	          line: 168,
	          column: 4
	        },
	        end: {
	          line: 168,
	          column: 70
	        }
	      },
	      "83": {
	        start: {
	          line: 169,
	          column: 4
	        },
	        end: {
	          line: 169,
	          column: 55
	        }
	      },
	      "84": {
	        start: {
	          line: 170,
	          column: 4
	        },
	        end: {
	          line: 170,
	          column: 45
	        }
	      },
	      "85": {
	        start: {
	          line: 173,
	          column: 29
	        },
	        end: {
	          line: 173,
	          column: 58
	        }
	      },
	      "86": {
	        start: {
	          line: 175,
	          column: 4
	        },
	        end: {
	          line: 175,
	          column: 64
	        }
	      },
	      "87": {
	        start: {
	          line: 176,
	          column: 4
	        },
	        end: {
	          line: 176,
	          column: 49
	        }
	      },
	      "88": {
	        start: {
	          line: 179,
	          column: 21
	        },
	        end: {
	          line: 179,
	          column: 51
	        }
	      },
	      "89": {
	        start: {
	          line: 180,
	          column: 23
	        },
	        end: {
	          line: 180,
	          column: 47
	        }
	      },
	      "90": {
	        start: {
	          line: 182,
	          column: 4
	        },
	        end: {
	          line: 182,
	          column: 44
	        }
	      },
	      "91": {
	        start: {
	          line: 183,
	          column: 4
	        },
	        end: {
	          line: 183,
	          column: 62
	        }
	      },
	      "92": {
	        start: {
	          line: 184,
	          column: 4
	        },
	        end: {
	          line: 184,
	          column: 47
	        }
	      },
	      "93": {
	        start: {
	          line: 185,
	          column: 4
	        },
	        end: {
	          line: 185,
	          column: 43
	        }
	      },
	      "94": {
	        start: {
	          line: 188,
	          column: 20
	        },
	        end: {
	          line: 188,
	          column: 50
	        }
	      },
	      "95": {
	        start: {
	          line: 189,
	          column: 22
	        },
	        end: {
	          line: 189,
	          column: 66
	        }
	      },
	      "96": {
	        start: {
	          line: 191,
	          column: 4
	        },
	        end: {
	          line: 191,
	          column: 44
	        }
	      },
	      "97": {
	        start: {
	          line: 192,
	          column: 4
	        },
	        end: {
	          line: 192,
	          column: 60
	        }
	      },
	      "98": {
	        start: {
	          line: 193,
	          column: 4
	        },
	        end: {
	          line: 193,
	          column: 45
	        }
	      },
	      "99": {
	        start: {
	          line: 194,
	          column: 4
	        },
	        end: {
	          line: 194,
	          column: 42
	        }
	      },
	      "100": {
	        start: {
	          line: 197,
	          column: 4
	        },
	        end: {
	          line: 205,
	          column: 5
	        }
	      },
	      "101": {
	        start: {
	          line: 198,
	          column: 28
	        },
	        end: {
	          line: 198,
	          column: 57
	        }
	      },
	      "102": {
	        start: {
	          line: 199,
	          column: 30
	        },
	        end: {
	          line: 199,
	          column: 52
	        }
	      },
	      "103": {
	        start: {
	          line: 201,
	          column: 6
	        },
	        end: {
	          line: 201,
	          column: 59
	        }
	      },
	      "104": {
	        start: {
	          line: 202,
	          column: 6
	        },
	        end: {
	          line: 202,
	          column: 74
	        }
	      },
	      "105": {
	        start: {
	          line: 203,
	          column: 6
	        },
	        end: {
	          line: 203,
	          column: 59
	        }
	      },
	      "106": {
	        start: {
	          line: 204,
	          column: 6
	        },
	        end: {
	          line: 204,
	          column: 50
	        }
	      },
	      "107": {
	        start: {
	          line: 207,
	          column: 4
	        },
	        end: {
	          line: 207,
	          column: 14
	        }
	      },
	      "108": {
	        start: {
	          line: 214,
	          column: 4
	        },
	        end: {
	          line: 216,
	          column: 5
	        }
	      },
	      "109": {
	        start: {
	          line: 215,
	          column: 6
	        },
	        end: {
	          line: 215,
	          column: 81
	        }
	      },
	      "110": {
	        start: {
	          line: 218,
	          column: 4
	        },
	        end: {
	          line: 218,
	          column: 27
	        }
	      },
	      "111": {
	        start: {
	          line: 219,
	          column: 4
	        },
	        end: {
	          line: 219,
	          column: 20
	        }
	      },
	      "112": {
	        start: {
	          line: 221,
	          column: 4
	        },
	        end: {
	          line: 225,
	          column: 5
	        }
	      },
	      "113": {
	        start: {
	          line: 222,
	          column: 6
	        },
	        end: {
	          line: 222,
	          column: 47
	        }
	      },
	      "114": {
	        start: {
	          line: 224,
	          column: 6
	        },
	        end: {
	          line: 224,
	          column: 45
	        }
	      },
	      "115": {
	        start: {
	          line: 230,
	          column: 4
	        },
	        end: {
	          line: 232,
	          column: 5
	        }
	      },
	      "116": {
	        start: {
	          line: 231,
	          column: 6
	        },
	        end: {
	          line: 231,
	          column: 44
	        }
	      },
	      "117": {
	        start: {
	          line: 234,
	          column: 4
	        },
	        end: {
	          line: 234,
	          column: 27
	        }
	      },
	      "118": {
	        start: {
	          line: 236,
	          column: 4
	        },
	        end: {
	          line: 238,
	          column: 5
	        }
	      },
	      "119": {
	        start: {
	          line: 237,
	          column: 6
	        },
	        end: {
	          line: 237,
	          column: 33
	        }
	      },
	      "120": {
	        start: {
	          line: 240,
	          column: 4
	        },
	        end: {
	          line: 242,
	          column: 7
	        }
	      },
	      "121": {
	        start: {
	          line: 241,
	          column: 6
	        },
	        end: {
	          line: 241,
	          column: 20
	        }
	      },
	      "122": {
	        start: {
	          line: 246,
	          column: 4
	        },
	        end: {
	          line: 248,
	          column: 7
	        }
	      },
	      "123": {
	        start: {
	          line: 247,
	          column: 6
	        },
	        end: {
	          line: 247,
	          column: 38
	        }
	      },
	      "124": {
	        start: {
	          line: 250,
	          column: 4
	        },
	        end: {
	          line: 252,
	          column: 7
	        }
	      },
	      "125": {
	        start: {
	          line: 251,
	          column: 6
	        },
	        end: {
	          line: 251,
	          column: 41
	        }
	      },
	      "126": {
	        start: {
	          line: 254,
	          column: 4
	        },
	        end: {
	          line: 257,
	          column: 7
	        }
	      },
	      "127": {
	        start: {
	          line: 255,
	          column: 6
	        },
	        end: {
	          line: 255,
	          column: 20
	        }
	      },
	      "128": {
	        start: {
	          line: 256,
	          column: 6
	        },
	        end: {
	          line: 256,
	          column: 33
	        }
	      },
	      "129": {
	        start: {
	          line: 259,
	          column: 4
	        },
	        end: {
	          line: 261,
	          column: 7
	        }
	      },
	      "130": {
	        start: {
	          line: 260,
	          column: 6
	        },
	        end: {
	          line: 260,
	          column: 21
	        }
	      },
	      "131": {
	        start: {
	          line: 265,
	          column: 4
	        },
	        end: {
	          line: 265,
	          column: 69
	        }
	      },
	      "132": {
	        start: {
	          line: 269,
	          column: 4
	        },
	        end: {
	          line: 272,
	          column: 5
	        }
	      },
	      "133": {
	        start: {
	          line: 270,
	          column: 6
	        },
	        end: {
	          line: 270,
	          column: 43
	        }
	      },
	      "134": {
	        start: {
	          line: 270,
	          column: 30
	        },
	        end: {
	          line: 270,
	          column: 41
	        }
	      },
	      "135": {
	        start: {
	          line: 271,
	          column: 6
	        },
	        end: {
	          line: 271,
	          column: 28
	        }
	      },
	      "136": {
	        start: {
	          line: 276,
	          column: 21
	        },
	        end: {
	          line: 276,
	          column: 50
	        }
	      },
	      "137": {
	        start: {
	          line: 277,
	          column: 15
	        },
	        end: {
	          line: 277,
	          column: 64
	        }
	      },
	      "138": {
	        start: {
	          line: 278,
	          column: 18
	        },
	        end: {
	          line: 278,
	          column: 68
	        }
	      },
	      "139": {
	        start: {
	          line: 280,
	          column: 4
	        },
	        end: {
	          line: 284,
	          column: 5
	        }
	      },
	      "140": {
	        start: {
	          line: 281,
	          column: 6
	        },
	        end: {
	          line: 281,
	          column: 42
	        }
	      },
	      "141": {
	        start: {
	          line: 282,
	          column: 6
	        },
	        end: {
	          line: 282,
	          column: 48
	        }
	      },
	      "142": {
	        start: {
	          line: 283,
	          column: 6
	        },
	        end: {
	          line: 283,
	          column: 33
	        }
	      },
	      "143": {
	        start: {
	          line: 286,
	          column: 4
	        },
	        end: {
	          line: 286,
	          column: 18
	        }
	      },
	      "144": {
	        start: {
	          line: 289,
	          column: 4
	        },
	        end: {
	          line: 296,
	          column: 5
	        }
	      },
	      "145": {
	        start: {
	          line: 289,
	          column: 17
	        },
	        end: {
	          line: 289,
	          column: 18
	        }
	      },
	      "146": {
	        start: {
	          line: 290,
	          column: 19
	        },
	        end: {
	          line: 292,
	          column: 23
	        }
	      },
	      "147": {
	        start: {
	          line: 294,
	          column: 6
	        },
	        end: {
	          line: 294,
	          column: 28
	        }
	      },
	      "148": {
	        start: {
	          line: 295,
	          column: 6
	        },
	        end: {
	          line: 295,
	          column: 33
	        }
	      },
	      "149": {
	        start: {
	          line: 300,
	          column: 4
	        },
	        end: {
	          line: 307,
	          column: 5
	        }
	      },
	      "150": {
	        start: {
	          line: 301,
	          column: 6
	        },
	        end: {
	          line: 301,
	          column: 45
	        }
	      },
	      "151": {
	        start: {
	          line: 302,
	          column: 6
	        },
	        end: {
	          line: 302,
	          column: 52
	        }
	      },
	      "152": {
	        start: {
	          line: 303,
	          column: 6
	        },
	        end: {
	          line: 303,
	          column: 32
	        }
	      },
	      "153": {
	        start: {
	          line: 306,
	          column: 6
	        },
	        end: {
	          line: 306,
	          column: 32
	        }
	      },
	      "154": {
	        start: {
	          line: 310,
	          column: 26
	        },
	        end: {
	          line: 310,
	          column: 61
	        }
	      },
	      "155": {
	        start: {
	          line: 312,
	          column: 4
	        },
	        end: {
	          line: 320,
	          column: 5
	        }
	      },
	      "156": {
	        start: {
	          line: 313,
	          column: 6
	        },
	        end: {
	          line: 313,
	          column: 50
	        }
	      },
	      "157": {
	        start: {
	          line: 315,
	          column: 24
	        },
	        end: {
	          line: 315,
	          column: 78
	        }
	      },
	      "158": {
	        start: {
	          line: 317,
	          column: 6
	        },
	        end: {
	          line: 319,
	          column: 7
	        }
	      },
	      "159": {
	        start: {
	          line: 318,
	          column: 8
	        },
	        end: {
	          line: 318,
	          column: 60
	        }
	      },
	      "160": {
	        start: {
	          line: 325,
	          column: 21
	        },
	        end: {
	          line: 325,
	          column: 44
	        }
	      },
	      "161": {
	        start: {
	          line: 327,
	          column: 4
	        },
	        end: {
	          line: 332,
	          column: 5
	        }
	      },
	      "162": {
	        start: {
	          line: 330,
	          column: 6
	        },
	        end: {
	          line: 330,
	          column: 29
	        }
	      },
	      "163": {
	        start: {
	          line: 331,
	          column: 6
	        },
	        end: {
	          line: 331,
	          column: 13
	        }
	      },
	      "164": {
	        start: {
	          line: 334,
	          column: 4
	        },
	        end: {
	          line: 341,
	          column: 5
	        }
	      },
	      "165": {
	        start: {
	          line: 334,
	          column: 17
	        },
	        end: {
	          line: 334,
	          column: 18
	        }
	      },
	      "166": {
	        start: {
	          line: 335,
	          column: 6
	        },
	        end: {
	          line: 340,
	          column: 7
	        }
	      },
	      "167": {
	        start: {
	          line: 338,
	          column: 8
	        },
	        end: {
	          line: 338,
	          column: 31
	        }
	      },
	      "168": {
	        start: {
	          line: 339,
	          column: 8
	        },
	        end: {
	          line: 339,
	          column: 15
	        }
	      },
	      "169": {
	        start: {
	          line: 344,
	          column: 24
	        },
	        end: {
	          line: 344,
	          column: 59
	        }
	      },
	      "170": {
	        start: {
	          line: 346,
	          column: 4
	        },
	        end: {
	          line: 362,
	          column: 5
	        }
	      },
	      "171": {
	        start: {
	          line: 346,
	          column: 17
	        },
	        end: {
	          line: 346,
	          column: 18
	        }
	      },
	      "172": {
	        start: {
	          line: 347,
	          column: 19
	        },
	        end: {
	          line: 347,
	          column: 32
	        }
	      },
	      "173": {
	        start: {
	          line: 349,
	          column: 6
	        },
	        end: {
	          line: 361,
	          column: 7
	        }
	      },
	      "174": {
	        start: {
	          line: 350,
	          column: 8
	        },
	        end: {
	          line: 350,
	          column: 31
	        }
	      },
	      "175": {
	        start: {
	          line: 351,
	          column: 8
	        },
	        end: {
	          line: 353,
	          column: 9
	        }
	      },
	      "176": {
	        start: {
	          line: 352,
	          column: 10
	        },
	        end: {
	          line: 352,
	          column: 67
	        }
	      },
	      "177": {
	        start: {
	          line: 354,
	          column: 8
	        },
	        end: {
	          line: 354,
	          column: 24
	        }
	      },
	      "178": {
	        start: {
	          line: 355,
	          column: 13
	        },
	        end: {
	          line: 361,
	          column: 7
	        }
	      },
	      "179": {
	        start: {
	          line: 356,
	          column: 8
	        },
	        end: {
	          line: 356,
	          column: 34
	        }
	      },
	      "180": {
	        start: {
	          line: 357,
	          column: 8
	        },
	        end: {
	          line: 357,
	          column: 21
	        }
	      },
	      "181": {
	        start: {
	          line: 359,
	          column: 8
	        },
	        end: {
	          line: 359,
	          column: 34
	        }
	      },
	      "182": {
	        start: {
	          line: 360,
	          column: 8
	        },
	        end: {
	          line: 360,
	          column: 24
	        }
	      },
	      "183": {
	        start: {
	          line: 377,
	          column: 20
	        },
	        end: {
	          line: 384,
	          column: 1
	        }
	      },
	      "184": {
	        start: {
	          line: 378,
	          column: 2
	        },
	        end: {
	          line: 382,
	          column: 3
	        }
	      },
	      "185": {
	        start: {
	          line: 378,
	          column: 15
	        },
	        end: {
	          line: 378,
	          column: 16
	        }
	      },
	      "186": {
	        start: {
	          line: 379,
	          column: 4
	        },
	        end: {
	          line: 381,
	          column: 5
	        }
	      },
	      "187": {
	        start: {
	          line: 380,
	          column: 6
	        },
	        end: {
	          line: 380,
	          column: 18
	        }
	      },
	      "188": {
	        start: {
	          line: 383,
	          column: 2
	        },
	        end: {
	          line: 383,
	          column: 15
	        }
	      },
	      "189": {
	        start: {
	          line: 395,
	          column: 17
	        },
	        end: {
	          line: 407,
	          column: 1
	        }
	      },
	      "190": {
	        start: {
	          line: 396,
	          column: 14
	        },
	        end: {
	          line: 396,
	          column: 56
	        }
	      },
	      "191": {
	        start: {
	          line: 399,
	          column: 2
	        },
	        end: {
	          line: 404,
	          column: 3
	        }
	      },
	      "192": {
	        start: {
	          line: 399,
	          column: 15
	        },
	        end: {
	          line: 399,
	          column: 16
	        }
	      },
	      "193": {
	        start: {
	          line: 400,
	          column: 4
	        },
	        end: {
	          line: 403,
	          column: 5
	        }
	      },
	      "194": {
	        start: {
	          line: 401,
	          column: 6
	        },
	        end: {
	          line: 401,
	          column: 18
	        }
	      },
	      "195": {
	        start: {
	          line: 402,
	          column: 6
	        },
	        end: {
	          line: 402,
	          column: 12
	        }
	      },
	      "196": {
	        start: {
	          line: 406,
	          column: 2
	        },
	        end: {
	          line: 406,
	          column: 12
	        }
	      },
	      "197": {
	        start: {
	          line: 425,
	          column: 19
	        },
	        end: {
	          line: 472,
	          column: 1
	        }
	      },
	      "198": {
	        start: {
	          line: 426,
	          column: 17
	        },
	        end: {
	          line: 426,
	          column: 21
	        }
	      },
	      "199": {
	        start: {
	          line: 428,
	          column: 2
	        },
	        end: {
	          line: 430,
	          column: 3
	        }
	      },
	      "200": {
	        start: {
	          line: 429,
	          column: 4
	        },
	        end: {
	          line: 429,
	          column: 93
	        }
	      },
	      "201": {
	        start: {
	          line: 432,
	          column: 2
	        },
	        end: {
	          line: 435,
	          column: 3
	        }
	      },
	      "202": {
	        start: {
	          line: 433,
	          column: 4
	        },
	        end: {
	          line: 433,
	          column: 133
	        }
	      },
	      "203": {
	        start: {
	          line: 434,
	          column: 4
	        },
	        end: {
	          line: 434,
	          column: 28
	        }
	      },
	      "204": {
	        start: {
	          line: 437,
	          column: 2
	        },
	        end: {
	          line: 437,
	          column: 52
	        }
	      },
	      "205": {
	        start: {
	          line: 442,
	          column: 2
	        },
	        end: {
	          line: 465,
	          column: 3
	        }
	      },
	      "206": {
	        start: {
	          line: 443,
	          column: 15
	        },
	        end: {
	          line: 443,
	          column: 39
	        }
	      },
	      "207": {
	        start: {
	          line: 447,
	          column: 4
	        },
	        end: {
	          line: 464,
	          column: 5
	        }
	      },
	      "208": {
	        start: {
	          line: 448,
	          column: 25
	        },
	        end: {
	          line: 448,
	          column: 38
	        }
	      },
	      "209": {
	        start: {
	          line: 449,
	          column: 26
	        },
	        end: {
	          line: 449,
	          column: 40
	        }
	      },
	      "210": {
	        start: {
	          line: 453,
	          column: 6
	        },
	        end: {
	          line: 453,
	          column: 36
	        }
	      },
	      "211": {
	        start: {
	          line: 454,
	          column: 6
	        },
	        end: {
	          line: 454,
	          column: 22
	        }
	      },
	      "212": {
	        start: {
	          line: 457,
	          column: 6
	        },
	        end: {
	          line: 461,
	          column: 7
	        }
	      },
	      "213": {
	        start: {
	          line: 458,
	          column: 8
	        },
	        end: {
	          line: 458,
	          column: 49
	        }
	      },
	      "214": {
	        start: {
	          line: 460,
	          column: 8
	        },
	        end: {
	          line: 460,
	          column: 35
	        }
	      },
	      "215": {
	        start: {
	          line: 463,
	          column: 6
	        },
	        end: {
	          line: 463,
	          column: 22
	        }
	      },
	      "216": {
	        start: {
	          line: 467,
	          column: 2
	        },
	        end: {
	          line: 469,
	          column: 3
	        }
	      },
	      "217": {
	        start: {
	          line: 468,
	          column: 4
	        },
	        end: {
	          line: 468,
	          column: 45
	        }
	      },
	      "218": {
	        start: {
	          line: 471,
	          column: 2
	        },
	        end: {
	          line: 471,
	          column: 58
	        }
	      },
	      "219": {
	        start: {
	          line: 475,
	          column: 0
	        },
	        end: {
	          line: 475,
	          column: 56
	        }
	      },
	      "220": {
	        start: {
	          line: 476,
	          column: 0
	        },
	        end: {
	          line: 476,
	          column: 64
	        }
	      },
	      "221": {
	        start: {
	          line: 479,
	          column: 0
	        },
	        end: {
	          line: 479,
	          column: 41
	        }
	      },
	      "222": {
	        start: {
	          line: 481,
	          column: 0
	        },
	        end: {
	          line: 481,
	          column: 29
	        }
	      }
	    },
	    fnMap: {
	      "0": {
	        name: "(anonymous_0)",
	        decl: {
	          start: {
	            line: 9,
	            column: 34
	          },
	          end: {
	            line: 9,
	            column: 35
	          }
	        },
	        loc: {
	          start: {
	            line: 9,
	            column: 40
	          },
	          end: {
	            line: 14,
	            column: 1
	          }
	        },
	        line: 9
	      },
	      "1": {
	        name: "(anonymous_1)",
	        decl: {
	          start: {
	            line: 25,
	            column: 25
	          },
	          end: {
	            line: 25,
	            column: 26
	          }
	        },
	        loc: {
	          start: {
	            line: 25,
	            column: 38
	          },
	          end: {
	            line: 27,
	            column: 1
	          }
	        },
	        line: 25
	      },
	      "2": {
	        name: "(anonymous_2)",
	        decl: {
	          start: {
	            line: 28,
	            column: 28
	          },
	          end: {
	            line: 28,
	            column: 29
	          }
	        },
	        loc: {
	          start: {
	            line: 28,
	            column: 41
	          },
	          end: {
	            line: 34,
	            column: 1
	          }
	        },
	        line: 28
	      },
	      "3": {
	        name: "(anonymous_3)",
	        decl: {
	          start: {
	            line: 36,
	            column: 15
	          },
	          end: {
	            line: 36,
	            column: 16
	          }
	        },
	        loc: {
	          start: {
	            line: 36,
	            column: 28
	          },
	          end: {
	            line: 38,
	            column: 1
	          }
	        },
	        line: 36
	      },
	      "4": {
	        name: "(anonymous_4)",
	        decl: {
	          start: {
	            line: 39,
	            column: 18
	          },
	          end: {
	            line: 39,
	            column: 19
	          }
	        },
	        loc: {
	          start: {
	            line: 39,
	            column: 31
	          },
	          end: {
	            line: 41,
	            column: 1
	          }
	        },
	        line: 39
	      },
	      "5": {
	        name: "(anonymous_5)",
	        decl: {
	          start: {
	            line: 43,
	            column: 24
	          },
	          end: {
	            line: 43,
	            column: 25
	          }
	        },
	        loc: {
	          start: {
	            line: 43,
	            column: 44
	          },
	          end: {
	            line: 91,
	            column: 1
	          }
	        },
	        line: 43
	      },
	      "6": {
	        name: "(anonymous_6)",
	        decl: {
	          start: {
	            line: 97,
	            column: 2
	          },
	          end: {
	            line: 97,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 97,
	            column: 46
	          },
	          end: {
	            line: 113,
	            column: 3
	          }
	        },
	        line: 97
	      },
	      "7": {
	        name: "(anonymous_7)",
	        decl: {
	          start: {
	            line: 115,
	            column: 2
	          },
	          end: {
	            line: 115,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 115,
	            column: 24
	          },
	          end: {
	            line: 121,
	            column: 3
	          }
	        },
	        line: 115
	      },
	      "8": {
	        name: "(anonymous_8)",
	        decl: {
	          start: {
	            line: 123,
	            column: 2
	          },
	          end: {
	            line: 123,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 123,
	            column: 29
	          },
	          end: {
	            line: 128,
	            column: 3
	          }
	        },
	        line: 123
	      },
	      "9": {
	        name: "(anonymous_9)",
	        decl: {
	          start: {
	            line: 130,
	            column: 2
	          },
	          end: {
	            line: 130,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 130,
	            column: 13
	          },
	          end: {
	            line: 208,
	            column: 3
	          }
	        },
	        line: 130
	      },
	      "10": {
	        name: "(anonymous_10)",
	        decl: {
	          start: {
	            line: 138,
	            column: 23
	          },
	          end: {
	            line: 138,
	            column: 24
	          }
	        },
	        loc: {
	          start: {
	            line: 138,
	            column: 30
	          },
	          end: {
	            line: 142,
	            column: 7
	          }
	        },
	        line: 138
	      },
	      "11": {
	        name: "(anonymous_11)",
	        decl: {
	          start: {
	            line: 213,
	            column: 2
	          },
	          end: {
	            line: 213,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 213,
	            column: 31
	          },
	          end: {
	            line: 262,
	            column: 3
	          }
	        },
	        line: 213
	      },
	      "12": {
	        name: "(anonymous_12)",
	        decl: {
	          start: {
	            line: 240,
	            column: 71
	          },
	          end: {
	            line: 240,
	            column: 72
	          }
	        },
	        loc: {
	          start: {
	            line: 240,
	            column: 82
	          },
	          end: {
	            line: 242,
	            column: 5
	          }
	        },
	        line: 240
	      },
	      "13": {
	        name: "(anonymous_13)",
	        decl: {
	          start: {
	            line: 246,
	            column: 31
	          },
	          end: {
	            line: 246,
	            column: 32
	          }
	        },
	        loc: {
	          start: {
	            line: 246,
	            column: 37
	          },
	          end: {
	            line: 248,
	            column: 5
	          }
	        },
	        line: 246
	      },
	      "14": {
	        name: "(anonymous_14)",
	        decl: {
	          start: {
	            line: 250,
	            column: 29
	          },
	          end: {
	            line: 250,
	            column: 30
	          }
	        },
	        loc: {
	          start: {
	            line: 250,
	            column: 35
	          },
	          end: {
	            line: 252,
	            column: 5
	          }
	        },
	        line: 250
	      },
	      "15": {
	        name: "(anonymous_15)",
	        decl: {
	          start: {
	            line: 254,
	            column: 23
	          },
	          end: {
	            line: 254,
	            column: 24
	          }
	        },
	        loc: {
	          start: {
	            line: 254,
	            column: 29
	          },
	          end: {
	            line: 257,
	            column: 5
	          }
	        },
	        line: 254
	      },
	      "16": {
	        name: "(anonymous_16)",
	        decl: {
	          start: {
	            line: 259,
	            column: 31
	          },
	          end: {
	            line: 259,
	            column: 32
	          }
	        },
	        loc: {
	          start: {
	            line: 259,
	            column: 37
	          },
	          end: {
	            line: 261,
	            column: 5
	          }
	        },
	        line: 259
	      },
	      "17": {
	        name: "(anonymous_17)",
	        decl: {
	          start: {
	            line: 264,
	            column: 2
	          },
	          end: {
	            line: 264,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 264,
	            column: 13
	          },
	          end: {
	            line: 266,
	            column: 3
	          }
	        },
	        line: 264
	      },
	      "18": {
	        name: "(anonymous_18)",
	        decl: {
	          start: {
	            line: 268,
	            column: 2
	          },
	          end: {
	            line: 268,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 268,
	            column: 11
	          },
	          end: {
	            line: 273,
	            column: 3
	          }
	        },
	        line: 268
	      },
	      "19": {
	        name: "(anonymous_19)",
	        decl: {
	          start: {
	            line: 270,
	            column: 25
	          },
	          end: {
	            line: 270,
	            column: 26
	          }
	        },
	        loc: {
	          start: {
	            line: 270,
	            column: 30
	          },
	          end: {
	            line: 270,
	            column: 41
	          }
	        },
	        line: 270
	      },
	      "20": {
	        name: "(anonymous_20)",
	        decl: {
	          start: {
	            line: 275,
	            column: 2
	          },
	          end: {
	            line: 275,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 275,
	            column: 20
	          },
	          end: {
	            line: 321,
	            column: 3
	          }
	        },
	        line: 275
	      },
	      "21": {
	        name: "(anonymous_21)",
	        decl: {
	          start: {
	            line: 323,
	            column: 2
	          },
	          end: {
	            line: 323,
	            column: 3
	          }
	        },
	        loc: {
	          start: {
	            line: 323,
	            column: 11
	          },
	          end: {
	            line: 363,
	            column: 3
	          }
	        },
	        line: 323
	      },
	      "22": {
	        name: "(anonymous_22)",
	        decl: {
	          start: {
	            line: 377,
	            column: 20
	          },
	          end: {
	            line: 377,
	            column: 21
	          }
	        },
	        loc: {
	          start: {
	            line: 377,
	            column: 28
	          },
	          end: {
	            line: 384,
	            column: 1
	          }
	        },
	        line: 377
	      },
	      "23": {
	        name: "(anonymous_23)",
	        decl: {
	          start: {
	            line: 395,
	            column: 17
	          },
	          end: {
	            line: 395,
	            column: 18
	          }
	        },
	        loc: {
	          start: {
	            line: 395,
	            column: 32
	          },
	          end: {
	            line: 407,
	            column: 1
	          }
	        },
	        line: 395
	      },
	      "24": {
	        name: "(anonymous_24)",
	        decl: {
	          start: {
	            line: 425,
	            column: 19
	          },
	          end: {
	            line: 425,
	            column: 20
	          }
	        },
	        loc: {
	          start: {
	            line: 425,
	            column: 37
	          },
	          end: {
	            line: 472,
	            column: 1
	          }
	        },
	        line: 425
	      }
	    },
	    branchMap: {
	      "0": {
	        loc: {
	          start: {
	            line: 5,
	            column: 12
	          },
	          end: {
	            line: 5,
	            column: 34
	          }
	        },
	        type: "binary-expr",
	        locations: [{
	          start: {
	            line: 5,
	            column: 12
	          },
	          end: {
	            line: 5,
	            column: 23
	          }
	        }, {
	          start: {
	            line: 5,
	            column: 27
	          },
	          end: {
	            line: 5,
	            column: 34
	          }
	        }],
	        line: 5
	      },
	      "1": {
	        loc: {
	          start: {
	            line: 6,
	            column: 23
	          },
	          end: {
	            line: 6,
	            column: 63
	          }
	        },
	        type: "binary-expr",
	        locations: [{
	          start: {
	            line: 6,
	            column: 23
	          },
	          end: {
	            line: 6,
	            column: 45
	          }
	        }, {
	          start: {
	            line: 6,
	            column: 49
	          },
	          end: {
	            line: 6,
	            column: 63
	          }
	        }],
	        line: 6
	      },
	      "2": {
	        loc: {
	          start: {
	            line: 31,
	            column: 2
	          },
	          end: {
	            line: 33,
	            column: 3
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 31,
	            column: 2
	          },
	          end: {
	            line: 33,
	            column: 3
	          }
	        }, {
	          start: {
	            line: 31,
	            column: 2
	          },
	          end: {
	            line: 33,
	            column: 3
	          }
	        }],
	        line: 31
	      },
	      "3": {
	        loc: {
	          start: {
	            line: 44,
	            column: 2
	          },
	          end: {
	            line: 49,
	            column: 3
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 44,
	            column: 2
	          },
	          end: {
	            line: 49,
	            column: 3
	          }
	        }, {
	          start: {
	            line: 44,
	            column: 2
	          },
	          end: {
	            line: 49,
	            column: 3
	          }
	        }],
	        line: 44
	      },
	      "4": {
	        loc: {
	          start: {
	            line: 55,
	            column: 2
	          },
	          end: {
	            line: 89,
	            column: 3
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 55,
	            column: 2
	          },
	          end: {
	            line: 89,
	            column: 3
	          }
	        }, {
	          start: {
	            line: 55,
	            column: 2
	          },
	          end: {
	            line: 89,
	            column: 3
	          }
	        }],
	        line: 55
	      },
	      "5": {
	        loc: {
	          start: {
	            line: 98,
	            column: 4
	          },
	          end: {
	            line: 100,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 98,
	            column: 4
	          },
	          end: {
	            line: 100,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 98,
	            column: 4
	          },
	          end: {
	            line: 100,
	            column: 5
	          }
	        }],
	        line: 98
	      },
	      "6": {
	        loc: {
	          start: {
	            line: 118,
	            column: 4
	          },
	          end: {
	            line: 120,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 118,
	            column: 4
	          },
	          end: {
	            line: 120,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 118,
	            column: 4
	          },
	          end: {
	            line: 120,
	            column: 5
	          }
	        }],
	        line: 118
	      },
	      "7": {
	        loc: {
	          start: {
	            line: 118,
	            column: 8
	          },
	          end: {
	            line: 118,
	            column: 48
	          }
	        },
	        type: "binary-expr",
	        locations: [{
	          start: {
	            line: 118,
	            column: 8
	          },
	          end: {
	            line: 118,
	            column: 26
	          }
	        }, {
	          start: {
	            line: 118,
	            column: 30
	          },
	          end: {
	            line: 118,
	            column: 48
	          }
	        }],
	        line: 118
	      },
	      "8": {
	        loc: {
	          start: {
	            line: 125,
	            column: 4
	          },
	          end: {
	            line: 127,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 125,
	            column: 4
	          },
	          end: {
	            line: 127,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 125,
	            column: 4
	          },
	          end: {
	            line: 127,
	            column: 5
	          }
	        }],
	        line: 125
	      },
	      "9": {
	        loc: {
	          start: {
	            line: 135,
	            column: 4
	          },
	          end: {
	            line: 143,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 135,
	            column: 4
	          },
	          end: {
	            line: 143,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 135,
	            column: 4
	          },
	          end: {
	            line: 143,
	            column: 5
	          }
	        }],
	        line: 135
	      },
	      "10": {
	        loc: {
	          start: {
	            line: 153,
	            column: 4
	          },
	          end: {
	            line: 161,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 153,
	            column: 4
	          },
	          end: {
	            line: 161,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 153,
	            column: 4
	          },
	          end: {
	            line: 161,
	            column: 5
	          }
	        }],
	        line: 153
	      },
	      "11": {
	        loc: {
	          start: {
	            line: 189,
	            column: 22
	          },
	          end: {
	            line: 189,
	            column: 66
	          }
	        },
	        type: "binary-expr",
	        locations: [{
	          start: {
	            line: 189,
	            column: 22
	          },
	          end: {
	            line: 189,
	            column: 31
	          }
	        }, {
	          start: {
	            line: 189,
	            column: 35
	          },
	          end: {
	            line: 189,
	            column: 66
	          }
	        }],
	        line: 189
	      },
	      "12": {
	        loc: {
	          start: {
	            line: 197,
	            column: 4
	          },
	          end: {
	            line: 205,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 197,
	            column: 4
	          },
	          end: {
	            line: 205,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 197,
	            column: 4
	          },
	          end: {
	            line: 205,
	            column: 5
	          }
	        }],
	        line: 197
	      },
	      "13": {
	        loc: {
	          start: {
	            line: 199,
	            column: 30
	          },
	          end: {
	            line: 199,
	            column: 52
	          }
	        },
	        type: "binary-expr",
	        locations: [{
	          start: {
	            line: 199,
	            column: 30
	          },
	          end: {
	            line: 199,
	            column: 46
	          }
	        }, {
	          start: {
	            line: 199,
	            column: 50
	          },
	          end: {
	            line: 199,
	            column: 52
	          }
	        }],
	        line: 199
	      },
	      "14": {
	        loc: {
	          start: {
	            line: 214,
	            column: 4
	          },
	          end: {
	            line: 216,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 214,
	            column: 4
	          },
	          end: {
	            line: 216,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 214,
	            column: 4
	          },
	          end: {
	            line: 216,
	            column: 5
	          }
	        }],
	        line: 214
	      },
	      "15": {
	        loc: {
	          start: {
	            line: 221,
	            column: 4
	          },
	          end: {
	            line: 225,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 221,
	            column: 4
	          },
	          end: {
	            line: 225,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 221,
	            column: 4
	          },
	          end: {
	            line: 225,
	            column: 5
	          }
	        }],
	        line: 221
	      },
	      "16": {
	        loc: {
	          start: {
	            line: 230,
	            column: 4
	          },
	          end: {
	            line: 232,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 230,
	            column: 4
	          },
	          end: {
	            line: 232,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 230,
	            column: 4
	          },
	          end: {
	            line: 232,
	            column: 5
	          }
	        }],
	        line: 230
	      },
	      "17": {
	        loc: {
	          start: {
	            line: 236,
	            column: 4
	          },
	          end: {
	            line: 238,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 236,
	            column: 4
	          },
	          end: {
	            line: 238,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 236,
	            column: 4
	          },
	          end: {
	            line: 238,
	            column: 5
	          }
	        }],
	        line: 236
	      },
	      "18": {
	        loc: {
	          start: {
	            line: 269,
	            column: 4
	          },
	          end: {
	            line: 272,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 269,
	            column: 4
	          },
	          end: {
	            line: 272,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 269,
	            column: 4
	          },
	          end: {
	            line: 272,
	            column: 5
	          }
	        }],
	        line: 269
	      },
	      "19": {
	        loc: {
	          start: {
	            line: 269,
	            column: 8
	          },
	          end: {
	            line: 269,
	            column: 39
	          }
	        },
	        type: "binary-expr",
	        locations: [{
	          start: {
	            line: 269,
	            column: 8
	          },
	          end: {
	            line: 269,
	            column: 18
	          }
	        }, {
	          start: {
	            line: 269,
	            column: 22
	          },
	          end: {
	            line: 269,
	            column: 39
	          }
	        }],
	        line: 269
	      },
	      "20": {
	        loc: {
	          start: {
	            line: 276,
	            column: 21
	          },
	          end: {
	            line: 276,
	            column: 50
	          }
	        },
	        type: "binary-expr",
	        locations: [{
	          start: {
	            line: 276,
	            column: 21
	          },
	          end: {
	            line: 276,
	            column: 44
	          }
	        }, {
	          start: {
	            line: 276,
	            column: 48
	          },
	          end: {
	            line: 276,
	            column: 50
	          }
	        }],
	        line: 276
	      },
	      "21": {
	        loc: {
	          start: {
	            line: 280,
	            column: 4
	          },
	          end: {
	            line: 284,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 280,
	            column: 4
	          },
	          end: {
	            line: 284,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 280,
	            column: 4
	          },
	          end: {
	            line: 284,
	            column: 5
	          }
	        }],
	        line: 280
	      },
	      "22": {
	        loc: {
	          start: {
	            line: 300,
	            column: 4
	          },
	          end: {
	            line: 307,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 300,
	            column: 4
	          },
	          end: {
	            line: 307,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 300,
	            column: 4
	          },
	          end: {
	            line: 307,
	            column: 5
	          }
	        }],
	        line: 300
	      },
	      "23": {
	        loc: {
	          start: {
	            line: 312,
	            column: 4
	          },
	          end: {
	            line: 320,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 312,
	            column: 4
	          },
	          end: {
	            line: 320,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 312,
	            column: 4
	          },
	          end: {
	            line: 320,
	            column: 5
	          }
	        }],
	        line: 312
	      },
	      "24": {
	        loc: {
	          start: {
	            line: 312,
	            column: 8
	          },
	          end: {
	            line: 312,
	            column: 47
	          }
	        },
	        type: "binary-expr",
	        locations: [{
	          start: {
	            line: 312,
	            column: 8
	          },
	          end: {
	            line: 312,
	            column: 25
	          }
	        }, {
	          start: {
	            line: 312,
	            column: 29
	          },
	          end: {
	            line: 312,
	            column: 47
	          }
	        }],
	        line: 312
	      },
	      "25": {
	        loc: {
	          start: {
	            line: 317,
	            column: 6
	          },
	          end: {
	            line: 319,
	            column: 7
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 317,
	            column: 6
	          },
	          end: {
	            line: 319,
	            column: 7
	          }
	        }, {
	          start: {
	            line: 317,
	            column: 6
	          },
	          end: {
	            line: 319,
	            column: 7
	          }
	        }],
	        line: 317
	      },
	      "26": {
	        loc: {
	          start: {
	            line: 327,
	            column: 4
	          },
	          end: {
	            line: 332,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 327,
	            column: 4
	          },
	          end: {
	            line: 332,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 327,
	            column: 4
	          },
	          end: {
	            line: 332,
	            column: 5
	          }
	        }],
	        line: 327
	      },
	      "27": {
	        loc: {
	          start: {
	            line: 335,
	            column: 6
	          },
	          end: {
	            line: 340,
	            column: 7
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 335,
	            column: 6
	          },
	          end: {
	            line: 340,
	            column: 7
	          }
	        }, {
	          start: {
	            line: 335,
	            column: 6
	          },
	          end: {
	            line: 340,
	            column: 7
	          }
	        }],
	        line: 335
	      },
	      "28": {
	        loc: {
	          start: {
	            line: 349,
	            column: 6
	          },
	          end: {
	            line: 361,
	            column: 7
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 349,
	            column: 6
	          },
	          end: {
	            line: 361,
	            column: 7
	          }
	        }, {
	          start: {
	            line: 349,
	            column: 6
	          },
	          end: {
	            line: 361,
	            column: 7
	          }
	        }],
	        line: 349
	      },
	      "29": {
	        loc: {
	          start: {
	            line: 351,
	            column: 8
	          },
	          end: {
	            line: 353,
	            column: 9
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 351,
	            column: 8
	          },
	          end: {
	            line: 353,
	            column: 9
	          }
	        }, {
	          start: {
	            line: 351,
	            column: 8
	          },
	          end: {
	            line: 353,
	            column: 9
	          }
	        }],
	        line: 351
	      },
	      "30": {
	        loc: {
	          start: {
	            line: 355,
	            column: 13
	          },
	          end: {
	            line: 361,
	            column: 7
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 355,
	            column: 13
	          },
	          end: {
	            line: 361,
	            column: 7
	          }
	        }, {
	          start: {
	            line: 355,
	            column: 13
	          },
	          end: {
	            line: 361,
	            column: 7
	          }
	        }],
	        line: 355
	      },
	      "31": {
	        loc: {
	          start: {
	            line: 379,
	            column: 4
	          },
	          end: {
	            line: 381,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 379,
	            column: 4
	          },
	          end: {
	            line: 381,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 379,
	            column: 4
	          },
	          end: {
	            line: 381,
	            column: 5
	          }
	        }],
	        line: 379
	      },
	      "32": {
	        loc: {
	          start: {
	            line: 400,
	            column: 4
	          },
	          end: {
	            line: 403,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 400,
	            column: 4
	          },
	          end: {
	            line: 403,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 400,
	            column: 4
	          },
	          end: {
	            line: 403,
	            column: 5
	          }
	        }],
	        line: 400
	      },
	      "33": {
	        loc: {
	          start: {
	            line: 428,
	            column: 2
	          },
	          end: {
	            line: 430,
	            column: 3
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 428,
	            column: 2
	          },
	          end: {
	            line: 430,
	            column: 3
	          }
	        }, {
	          start: {
	            line: 428,
	            column: 2
	          },
	          end: {
	            line: 430,
	            column: 3
	          }
	        }],
	        line: 428
	      },
	      "34": {
	        loc: {
	          start: {
	            line: 432,
	            column: 2
	          },
	          end: {
	            line: 435,
	            column: 3
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 432,
	            column: 2
	          },
	          end: {
	            line: 435,
	            column: 3
	          }
	        }, {
	          start: {
	            line: 432,
	            column: 2
	          },
	          end: {
	            line: 435,
	            column: 3
	          }
	        }],
	        line: 432
	      },
	      "35": {
	        loc: {
	          start: {
	            line: 442,
	            column: 2
	          },
	          end: {
	            line: 465,
	            column: 3
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 442,
	            column: 2
	          },
	          end: {
	            line: 465,
	            column: 3
	          }
	        }, {
	          start: {
	            line: 442,
	            column: 2
	          },
	          end: {
	            line: 465,
	            column: 3
	          }
	        }],
	        line: 442
	      },
	      "36": {
	        loc: {
	          start: {
	            line: 447,
	            column: 4
	          },
	          end: {
	            line: 464,
	            column: 5
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 447,
	            column: 4
	          },
	          end: {
	            line: 464,
	            column: 5
	          }
	        }, {
	          start: {
	            line: 447,
	            column: 4
	          },
	          end: {
	            line: 464,
	            column: 5
	          }
	        }],
	        line: 447
	      },
	      "37": {
	        loc: {
	          start: {
	            line: 457,
	            column: 6
	          },
	          end: {
	            line: 461,
	            column: 7
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 457,
	            column: 6
	          },
	          end: {
	            line: 461,
	            column: 7
	          }
	        }, {
	          start: {
	            line: 457,
	            column: 6
	          },
	          end: {
	            line: 461,
	            column: 7
	          }
	        }],
	        line: 457
	      },
	      "38": {
	        loc: {
	          start: {
	            line: 467,
	            column: 2
	          },
	          end: {
	            line: 469,
	            column: 3
	          }
	        },
	        type: "if",
	        locations: [{
	          start: {
	            line: 467,
	            column: 2
	          },
	          end: {
	            line: 469,
	            column: 3
	          }
	        }, {
	          start: {
	            line: 467,
	            column: 2
	          },
	          end: {
	            line: 469,
	            column: 3
	          }
	        }],
	        line: 467
	      }
	    },
	    s: {
	      "0": 0,
	      "1": 0,
	      "2": 0,
	      "3": 0,
	      "4": 0,
	      "5": 0,
	      "6": 0,
	      "7": 0,
	      "8": 0,
	      "9": 0,
	      "10": 0,
	      "11": 0,
	      "12": 0,
	      "13": 0,
	      "14": 0,
	      "15": 0,
	      "16": 0,
	      "17": 0,
	      "18": 0,
	      "19": 0,
	      "20": 0,
	      "21": 0,
	      "22": 0,
	      "23": 0,
	      "24": 0,
	      "25": 0,
	      "26": 0,
	      "27": 0,
	      "28": 0,
	      "29": 0,
	      "30": 0,
	      "31": 0,
	      "32": 0,
	      "33": 0,
	      "34": 0,
	      "35": 0,
	      "36": 0,
	      "37": 0,
	      "38": 0,
	      "39": 0,
	      "40": 0,
	      "41": 0,
	      "42": 0,
	      "43": 0,
	      "44": 0,
	      "45": 0,
	      "46": 0,
	      "47": 0,
	      "48": 0,
	      "49": 0,
	      "50": 0,
	      "51": 0,
	      "52": 0,
	      "53": 0,
	      "54": 0,
	      "55": 0,
	      "56": 0,
	      "57": 0,
	      "58": 0,
	      "59": 0,
	      "60": 0,
	      "61": 0,
	      "62": 0,
	      "63": 0,
	      "64": 0,
	      "65": 0,
	      "66": 0,
	      "67": 0,
	      "68": 0,
	      "69": 0,
	      "70": 0,
	      "71": 0,
	      "72": 0,
	      "73": 0,
	      "74": 0,
	      "75": 0,
	      "76": 0,
	      "77": 0,
	      "78": 0,
	      "79": 0,
	      "80": 0,
	      "81": 0,
	      "82": 0,
	      "83": 0,
	      "84": 0,
	      "85": 0,
	      "86": 0,
	      "87": 0,
	      "88": 0,
	      "89": 0,
	      "90": 0,
	      "91": 0,
	      "92": 0,
	      "93": 0,
	      "94": 0,
	      "95": 0,
	      "96": 0,
	      "97": 0,
	      "98": 0,
	      "99": 0,
	      "100": 0,
	      "101": 0,
	      "102": 0,
	      "103": 0,
	      "104": 0,
	      "105": 0,
	      "106": 0,
	      "107": 0,
	      "108": 0,
	      "109": 0,
	      "110": 0,
	      "111": 0,
	      "112": 0,
	      "113": 0,
	      "114": 0,
	      "115": 0,
	      "116": 0,
	      "117": 0,
	      "118": 0,
	      "119": 0,
	      "120": 0,
	      "121": 0,
	      "122": 0,
	      "123": 0,
	      "124": 0,
	      "125": 0,
	      "126": 0,
	      "127": 0,
	      "128": 0,
	      "129": 0,
	      "130": 0,
	      "131": 0,
	      "132": 0,
	      "133": 0,
	      "134": 0,
	      "135": 0,
	      "136": 0,
	      "137": 0,
	      "138": 0,
	      "139": 0,
	      "140": 0,
	      "141": 0,
	      "142": 0,
	      "143": 0,
	      "144": 0,
	      "145": 0,
	      "146": 0,
	      "147": 0,
	      "148": 0,
	      "149": 0,
	      "150": 0,
	      "151": 0,
	      "152": 0,
	      "153": 0,
	      "154": 0,
	      "155": 0,
	      "156": 0,
	      "157": 0,
	      "158": 0,
	      "159": 0,
	      "160": 0,
	      "161": 0,
	      "162": 0,
	      "163": 0,
	      "164": 0,
	      "165": 0,
	      "166": 0,
	      "167": 0,
	      "168": 0,
	      "169": 0,
	      "170": 0,
	      "171": 0,
	      "172": 0,
	      "173": 0,
	      "174": 0,
	      "175": 0,
	      "176": 0,
	      "177": 0,
	      "178": 0,
	      "179": 0,
	      "180": 0,
	      "181": 0,
	      "182": 0,
	      "183": 0,
	      "184": 0,
	      "185": 0,
	      "186": 0,
	      "187": 0,
	      "188": 0,
	      "189": 0,
	      "190": 0,
	      "191": 0,
	      "192": 0,
	      "193": 0,
	      "194": 0,
	      "195": 0,
	      "196": 0,
	      "197": 0,
	      "198": 0,
	      "199": 0,
	      "200": 0,
	      "201": 0,
	      "202": 0,
	      "203": 0,
	      "204": 0,
	      "205": 0,
	      "206": 0,
	      "207": 0,
	      "208": 0,
	      "209": 0,
	      "210": 0,
	      "211": 0,
	      "212": 0,
	      "213": 0,
	      "214": 0,
	      "215": 0,
	      "216": 0,
	      "217": 0,
	      "218": 0,
	      "219": 0,
	      "220": 0,
	      "221": 0,
	      "222": 0
	    },
	    f: {
	      "0": 0,
	      "1": 0,
	      "2": 0,
	      "3": 0,
	      "4": 0,
	      "5": 0,
	      "6": 0,
	      "7": 0,
	      "8": 0,
	      "9": 0,
	      "10": 0,
	      "11": 0,
	      "12": 0,
	      "13": 0,
	      "14": 0,
	      "15": 0,
	      "16": 0,
	      "17": 0,
	      "18": 0,
	      "19": 0,
	      "20": 0,
	      "21": 0,
	      "22": 0,
	      "23": 0,
	      "24": 0
	    },
	    b: {
	      "0": [0, 0],
	      "1": [0, 0],
	      "2": [0, 0],
	      "3": [0, 0],
	      "4": [0, 0],
	      "5": [0, 0],
	      "6": [0, 0],
	      "7": [0, 0],
	      "8": [0, 0],
	      "9": [0, 0],
	      "10": [0, 0],
	      "11": [0, 0],
	      "12": [0, 0],
	      "13": [0, 0],
	      "14": [0, 0],
	      "15": [0, 0],
	      "16": [0, 0],
	      "17": [0, 0],
	      "18": [0, 0],
	      "19": [0, 0],
	      "20": [0, 0],
	      "21": [0, 0],
	      "22": [0, 0],
	      "23": [0, 0],
	      "24": [0, 0],
	      "25": [0, 0],
	      "26": [0, 0],
	      "27": [0, 0],
	      "28": [0, 0],
	      "29": [0, 0],
	      "30": [0, 0],
	      "31": [0, 0],
	      "32": [0, 0],
	      "33": [0, 0],
	      "34": [0, 0],
	      "35": [0, 0],
	      "36": [0, 0],
	      "37": [0, 0],
	      "38": [0, 0]
	    },
	    _coverageSchema: "1a1c01bbd47fc00a2c39e90264f33305004495a9",
	    hash: "16d69ac6050c7f318cf74aa8c24646dce2711f25"
	  };
	  var coverage = global[gcv] || (global[gcv] = {});

	  if (!coverage[path] || coverage[path].hash !== hash) {
	    coverage[path] = coverageData;
	  }

	  var actualCoverage = coverage[path];
	  {
	    // @ts-ignore
	    cov_2c38f07m8x = function () {
	      return actualCoverage;
	    };
	  }
	  return actualCoverage;
	}

	cov_2c38f07m8x();
	const dom$1 = (cov_2c38f07m8x().s[0]++, (cov_2c38f07m8x().b[0][0]++, videojs__default["default"].dom) || (cov_2c38f07m8x().b[0][1]++, videojs__default["default"]));
	const registerPlugin = (cov_2c38f07m8x().s[1]++, (cov_2c38f07m8x().b[1][0]++, videojs__default["default"].registerPlugin) || (cov_2c38f07m8x().b[1][1]++, videojs__default["default"].plugin)); // see https://github.com/Modernizr/Modernizr/blob/master/feature-detects/css/pointerevents.js

	const supportsCssPointerEvents = (cov_2c38f07m8x().s[2]++, (() => {
	  cov_2c38f07m8x().f[0]++;
	  const element = (cov_2c38f07m8x().s[3]++, document_1.createElement('x'));
	  cov_2c38f07m8x().s[4]++;
	  element.style.cssText = 'pointer-events:auto';
	  cov_2c38f07m8x().s[5]++;
	  return element.style.pointerEvents === 'auto';
	})());
	const defaults = (cov_2c38f07m8x().s[6]++, {
	  className: 'vjs-playlist',
	  playOnSelect: false,
	  supportsCssPointerEvents
	}); // we don't add `vjs-playlist-now-playing` in addSelectedClass
	// so it won't conflict with `vjs-icon-play
	// since it'll get added when we mouse out

	cov_2c38f07m8x().s[7]++;

	const addSelectedClass = function (el) {
	  cov_2c38f07m8x().f[1]++;
	  cov_2c38f07m8x().s[8]++;
	  el.addClass('vjs-selected');
	};

	cov_2c38f07m8x().s[9]++;

	const removeSelectedClass = function (el) {
	  cov_2c38f07m8x().f[2]++;
	  cov_2c38f07m8x().s[10]++;
	  el.removeClass('vjs-selected');
	  cov_2c38f07m8x().s[11]++;

	  if (el.thumbnail) {
	    cov_2c38f07m8x().b[2][0]++;
	    cov_2c38f07m8x().s[12]++;
	    dom$1.removeClass(el.thumbnail, 'vjs-playlist-now-playing');
	  } else {
	    cov_2c38f07m8x().b[2][1]++;
	  }
	};

	cov_2c38f07m8x().s[13]++;

	const upNext = function (el) {
	  cov_2c38f07m8x().f[3]++;
	  cov_2c38f07m8x().s[14]++;
	  el.addClass('vjs-up-next');
	};

	cov_2c38f07m8x().s[15]++;

	const notUpNext = function (el) {
	  cov_2c38f07m8x().f[4]++;
	  cov_2c38f07m8x().s[16]++;
	  el.removeClass('vjs-up-next');
	};

	cov_2c38f07m8x().s[17]++;

	const createThumbnail = function (thumbnail) {
	  cov_2c38f07m8x().f[5]++;
	  cov_2c38f07m8x().s[18]++;

	  if (!thumbnail) {
	    cov_2c38f07m8x().b[3][0]++;
	    const placeholder = (cov_2c38f07m8x().s[19]++, document_1.createElement('div'));
	    cov_2c38f07m8x().s[20]++;
	    placeholder.className = 'vjs-playlist-thumbnail vjs-playlist-thumbnail-placeholder';
	    cov_2c38f07m8x().s[21]++;
	    return placeholder;
	  } else {
	    cov_2c38f07m8x().b[3][1]++;
	  }

	  const picture = (cov_2c38f07m8x().s[22]++, document_1.createElement('picture'));
	  cov_2c38f07m8x().s[23]++;
	  picture.className = 'vjs-playlist-thumbnail';
	  cov_2c38f07m8x().s[24]++;

	  if (typeof thumbnail === 'string') {
	    cov_2c38f07m8x().b[4][0]++; // simple thumbnails

	    const img = (cov_2c38f07m8x().s[25]++, document_1.createElement('img'));
	    cov_2c38f07m8x().s[26]++;
	    img.loading = 'lazy';
	    cov_2c38f07m8x().s[27]++;
	    img.src = thumbnail;
	    cov_2c38f07m8x().s[28]++;
	    img.alt = '';
	    cov_2c38f07m8x().s[29]++;
	    picture.appendChild(img);
	  } else {
	    cov_2c38f07m8x().b[4][1]++;
	    cov_2c38f07m8x().s[30]++; // responsive thumbnails
	    // additional variations of a <picture> are specified as
	    // <source> elements

	    for (let i = (cov_2c38f07m8x().s[31]++, 0); i < thumbnail.length - 1; i++) {
	      const variant = (cov_2c38f07m8x().s[32]++, thumbnail[i]);
	      const source = (cov_2c38f07m8x().s[33]++, document_1.createElement('source')); // transfer the properties of each variant onto a <source>

	      cov_2c38f07m8x().s[34]++;

	      for (const prop in variant) {
	        cov_2c38f07m8x().s[35]++;
	        source[prop] = variant[prop];
	      }

	      cov_2c38f07m8x().s[36]++;
	      picture.appendChild(source);
	    } // the default version of a <picture> is specified by an <img>


	    const variant = (cov_2c38f07m8x().s[37]++, thumbnail[thumbnail.length - 1]);
	    const img = (cov_2c38f07m8x().s[38]++, document_1.createElement('img'));
	    cov_2c38f07m8x().s[39]++;
	    img.loading = 'lazy';
	    cov_2c38f07m8x().s[40]++;
	    img.alt = '';
	    cov_2c38f07m8x().s[41]++;

	    for (const prop in variant) {
	      cov_2c38f07m8x().s[42]++;
	      img[prop] = variant[prop];
	    }

	    cov_2c38f07m8x().s[43]++;
	    picture.appendChild(img);
	  }

	  cov_2c38f07m8x().s[44]++;
	  return picture;
	};

	const Component = (cov_2c38f07m8x().s[45]++, videojs__default["default"].getComponent('Component'));

	class PlaylistMenuItem extends Component {
	  constructor(player, playlistItem, settings) {
	    cov_2c38f07m8x().f[6]++;
	    cov_2c38f07m8x().s[46]++;

	    if (!playlistItem.item) {
	      cov_2c38f07m8x().b[5][0]++;
	      cov_2c38f07m8x().s[47]++;
	      throw new Error('Cannot construct a PlaylistMenuItem without an item option');
	    } else {
	      cov_2c38f07m8x().b[5][1]++;
	    }

	    cov_2c38f07m8x().s[48]++;
	    playlistItem.showDescription = settings.showDescription;
	    cov_2c38f07m8x().s[49]++;
	    super(player, playlistItem);
	    cov_2c38f07m8x().s[50]++;
	    this.item = playlistItem.item;
	    cov_2c38f07m8x().s[51]++;
	    this.playOnSelect = settings.playOnSelect;
	    cov_2c38f07m8x().s[52]++;
	    this.emitTapEvents();
	    cov_2c38f07m8x().s[53]++;
	    this.on(['click', 'tap'], this.switchPlaylistItem_);
	    cov_2c38f07m8x().s[54]++;
	    this.on('keydown', this.handleKeyDown_);
	  }

	  handleKeyDown_(event) {
	    cov_2c38f07m8x().f[7]++;
	    cov_2c38f07m8x().s[55]++; // keycode 13 is <Enter>
	    // keycode 32 is <Space>

	    if ((cov_2c38f07m8x().b[7][0]++, event.which === 13) || (cov_2c38f07m8x().b[7][1]++, event.which === 32)) {
	      cov_2c38f07m8x().b[6][0]++;
	      cov_2c38f07m8x().s[56]++;
	      this.switchPlaylistItem_();
	    } else {
	      cov_2c38f07m8x().b[6][1]++;
	    }
	  }

	  switchPlaylistItem_(event) {
	    cov_2c38f07m8x().f[8]++;
	    cov_2c38f07m8x().s[57]++;
	    this.player_.playlist.currentItem(this.player_.playlist().indexOf(this.item));
	    cov_2c38f07m8x().s[58]++;

	    if (this.playOnSelect) {
	      cov_2c38f07m8x().b[8][0]++;
	      cov_2c38f07m8x().s[59]++;
	      this.player_.play();
	    } else {
	      cov_2c38f07m8x().b[8][1]++;
	    }
	  }

	  createEl() {
	    cov_2c38f07m8x().f[9]++;
	    const li = (cov_2c38f07m8x().s[60]++, document_1.createElement('li'));
	    const item = (cov_2c38f07m8x().s[61]++, this.options_.item);
	    const showDescription = (cov_2c38f07m8x().s[62]++, this.options_.showDescription);
	    cov_2c38f07m8x().s[63]++;

	    if (typeof item.data === 'object') {
	      cov_2c38f07m8x().b[9][0]++;
	      const dataKeys = (cov_2c38f07m8x().s[64]++, Object.keys(item.data));
	      cov_2c38f07m8x().s[65]++;
	      dataKeys.forEach(key => {
	        cov_2c38f07m8x().f[10]++;
	        const value = (cov_2c38f07m8x().s[66]++, item.data[key]);
	        cov_2c38f07m8x().s[67]++;
	        li.dataset[key] = value;
	      });
	    } else {
	      cov_2c38f07m8x().b[9][1]++;
	    }

	    cov_2c38f07m8x().s[68]++;
	    li.className = 'vjs-playlist-item';
	    cov_2c38f07m8x().s[69]++;
	    li.setAttribute('tabIndex', 0); // Thumbnail image

	    cov_2c38f07m8x().s[70]++;
	    this.thumbnail = createThumbnail(item.thumbnail);
	    cov_2c38f07m8x().s[71]++;
	    li.appendChild(this.thumbnail); // Duration

	    cov_2c38f07m8x().s[72]++;

	    if (item.duration) {
	      cov_2c38f07m8x().b[10][0]++;
	      const duration = (cov_2c38f07m8x().s[73]++, document_1.createElement('time'));
	      const time = (cov_2c38f07m8x().s[74]++, videojs__default["default"].formatTime(item.duration));
	      cov_2c38f07m8x().s[75]++;
	      duration.className = 'vjs-playlist-duration';
	      cov_2c38f07m8x().s[76]++;
	      duration.setAttribute('datetime', 'PT0H0M' + item.duration + 'S');
	      cov_2c38f07m8x().s[77]++;
	      duration.appendChild(document_1.createTextNode(time));
	      cov_2c38f07m8x().s[78]++;
	      li.appendChild(duration);
	    } else {
	      cov_2c38f07m8x().b[10][1]++;
	    } // Now playing


	    const nowPlayingEl = (cov_2c38f07m8x().s[79]++, document_1.createElement('span'));
	    const nowPlayingText = (cov_2c38f07m8x().s[80]++, this.localize('Now Playing'));
	    cov_2c38f07m8x().s[81]++;
	    nowPlayingEl.className = 'vjs-playlist-now-playing-text';
	    cov_2c38f07m8x().s[82]++;
	    nowPlayingEl.appendChild(document_1.createTextNode(nowPlayingText));
	    cov_2c38f07m8x().s[83]++;
	    nowPlayingEl.setAttribute('title', nowPlayingText);
	    cov_2c38f07m8x().s[84]++;
	    this.thumbnail.appendChild(nowPlayingEl); // Title container contains title and "up next"

	    const titleContainerEl = (cov_2c38f07m8x().s[85]++, document_1.createElement('div'));
	    cov_2c38f07m8x().s[86]++;
	    titleContainerEl.className = 'vjs-playlist-title-container';
	    cov_2c38f07m8x().s[87]++;
	    this.thumbnail.appendChild(titleContainerEl); // Up next

	    const upNextEl = (cov_2c38f07m8x().s[88]++, document_1.createElement('span'));
	    const upNextText = (cov_2c38f07m8x().s[89]++, this.localize('Up Next'));
	    cov_2c38f07m8x().s[90]++;
	    upNextEl.className = 'vjs-up-next-text';
	    cov_2c38f07m8x().s[91]++;
	    upNextEl.appendChild(document_1.createTextNode(upNextText));
	    cov_2c38f07m8x().s[92]++;
	    upNextEl.setAttribute('title', upNextText);
	    cov_2c38f07m8x().s[93]++;
	    titleContainerEl.appendChild(upNextEl); // Video title

	    const titleEl = (cov_2c38f07m8x().s[94]++, document_1.createElement('cite'));
	    const titleText = (cov_2c38f07m8x().s[95]++, (cov_2c38f07m8x().b[11][0]++, item.name) || (cov_2c38f07m8x().b[11][1]++, this.localize('Untitled Video')));
	    cov_2c38f07m8x().s[96]++;
	    titleEl.className = 'vjs-playlist-name';
	    cov_2c38f07m8x().s[97]++;
	    titleEl.appendChild(document_1.createTextNode(titleText));
	    cov_2c38f07m8x().s[98]++;
	    titleEl.setAttribute('title', titleText);
	    cov_2c38f07m8x().s[99]++;
	    titleContainerEl.appendChild(titleEl); // We add thumbnail video description only if specified in playlist options

	    cov_2c38f07m8x().s[100]++;

	    if (showDescription) {
	      cov_2c38f07m8x().b[12][0]++;
	      const descriptionEl = (cov_2c38f07m8x().s[101]++, document_1.createElement('div'));
	      const descriptionText = (cov_2c38f07m8x().s[102]++, (cov_2c38f07m8x().b[13][0]++, item.description) || (cov_2c38f07m8x().b[13][1]++, ''));
	      cov_2c38f07m8x().s[103]++;
	      descriptionEl.className = 'vjs-playlist-description';
	      cov_2c38f07m8x().s[104]++;
	      descriptionEl.appendChild(document_1.createTextNode(descriptionText));
	      cov_2c38f07m8x().s[105]++;
	      descriptionEl.setAttribute('title', descriptionText);
	      cov_2c38f07m8x().s[106]++;
	      titleContainerEl.appendChild(descriptionEl);
	    } else {
	      cov_2c38f07m8x().b[12][1]++;
	    }

	    cov_2c38f07m8x().s[107]++;
	    return li;
	  }

	}

	class PlaylistMenu extends Component {
	  constructor(player, options) {
	    cov_2c38f07m8x().f[11]++;
	    cov_2c38f07m8x().s[108]++;

	    if (!player.playlist) {
	      cov_2c38f07m8x().b[14][0]++;
	      cov_2c38f07m8x().s[109]++;
	      throw new Error('videojs-playlist is required for the playlist component');
	    } else {
	      cov_2c38f07m8x().b[14][1]++;
	    }

	    cov_2c38f07m8x().s[110]++;
	    super(player, options);
	    cov_2c38f07m8x().s[111]++;
	    this.items = [];
	    cov_2c38f07m8x().s[112]++;

	    if (options.horizontal) {
	      cov_2c38f07m8x().b[15][0]++;
	      cov_2c38f07m8x().s[113]++;
	      this.addClass('vjs-playlist-horizontal');
	    } else {
	      cov_2c38f07m8x().b[15][1]++;
	      cov_2c38f07m8x().s[114]++;
	      this.addClass('vjs-playlist-vertical');
	    } // If CSS pointer events aren't supported, we have to prevent
	    // clicking on playlist items during ads with slightly more
	    // invasive techniques. Details in the stylesheet.


	    cov_2c38f07m8x().s[115]++;

	    if (options.supportsCssPointerEvents) {
	      cov_2c38f07m8x().b[16][0]++;
	      cov_2c38f07m8x().s[116]++;
	      this.addClass('vjs-csspointerevents');
	    } else {
	      cov_2c38f07m8x().b[16][1]++;
	    }

	    cov_2c38f07m8x().s[117]++;
	    this.createPlaylist_();
	    cov_2c38f07m8x().s[118]++;

	    if (!videojs__default["default"].browser.TOUCH_ENABLED) {
	      cov_2c38f07m8x().b[17][0]++;
	      cov_2c38f07m8x().s[119]++;
	      this.addClass('vjs-mouse');
	    } else {
	      cov_2c38f07m8x().b[17][1]++;
	    }

	    cov_2c38f07m8x().s[120]++;
	    this.on(player, ['loadstart', 'playlistchange', 'playlistsorted'], event => {
	      cov_2c38f07m8x().f[12]++;
	      cov_2c38f07m8x().s[121]++;
	      this.update();
	    }); // Keep track of whether an ad is playing so that the menu
	    // appearance can be adapted appropriately

	    cov_2c38f07m8x().s[122]++;
	    this.on(player, 'adstart', () => {
	      cov_2c38f07m8x().f[13]++;
	      cov_2c38f07m8x().s[123]++;
	      this.addClass('vjs-ad-playing');
	    });
	    cov_2c38f07m8x().s[124]++;
	    this.on(player, 'adend', () => {
	      cov_2c38f07m8x().f[14]++;
	      cov_2c38f07m8x().s[125]++;
	      this.removeClass('vjs-ad-playing');
	    });
	    cov_2c38f07m8x().s[126]++;
	    this.on('dispose', () => {
	      cov_2c38f07m8x().f[15]++;
	      cov_2c38f07m8x().s[127]++;
	      this.empty_();
	      cov_2c38f07m8x().s[128]++;
	      player.playlistMenu = null;
	    });
	    cov_2c38f07m8x().s[129]++;
	    this.on(player, 'dispose', () => {
	      cov_2c38f07m8x().f[16]++;
	      cov_2c38f07m8x().s[130]++;
	      this.dispose();
	    });
	  }

	  createEl() {
	    cov_2c38f07m8x().f[17]++;
	    cov_2c38f07m8x().s[131]++;
	    return dom$1.createEl('div', {
	      className: this.options_.className
	    });
	  }

	  empty_() {
	    cov_2c38f07m8x().f[18]++;
	    cov_2c38f07m8x().s[132]++;

	    if ((cov_2c38f07m8x().b[19][0]++, this.items) && (cov_2c38f07m8x().b[19][1]++, this.items.length)) {
	      cov_2c38f07m8x().b[18][0]++;
	      cov_2c38f07m8x().s[133]++;
	      this.items.forEach(i => {
	        cov_2c38f07m8x().f[19]++;
	        cov_2c38f07m8x().s[134]++;
	        return i.dispose();
	      });
	      cov_2c38f07m8x().s[135]++;
	      this.items.length = 0;
	    } else {
	      cov_2c38f07m8x().b[18][1]++;
	    }
	  }

	  createPlaylist_() {
	    cov_2c38f07m8x().f[20]++;
	    const playlist = (cov_2c38f07m8x().s[136]++, (cov_2c38f07m8x().b[20][0]++, this.player_.playlist()) || (cov_2c38f07m8x().b[20][1]++, []));
	    let list = (cov_2c38f07m8x().s[137]++, this.el_.querySelector('.vjs-playlist-item-list'));
	    let overlay = (cov_2c38f07m8x().s[138]++, this.el_.querySelector('.vjs-playlist-ad-overlay'));
	    cov_2c38f07m8x().s[139]++;

	    if (!list) {
	      cov_2c38f07m8x().b[21][0]++;
	      cov_2c38f07m8x().s[140]++;
	      list = document_1.createElement('ol');
	      cov_2c38f07m8x().s[141]++;
	      list.className = 'vjs-playlist-item-list';
	      cov_2c38f07m8x().s[142]++;
	      this.el_.appendChild(list);
	    } else {
	      cov_2c38f07m8x().b[21][1]++;
	    }

	    cov_2c38f07m8x().s[143]++;
	    this.empty_(); // create new items

	    cov_2c38f07m8x().s[144]++;

	    for (let i = (cov_2c38f07m8x().s[145]++, 0); i < playlist.length; i++) {
	      const item = (cov_2c38f07m8x().s[146]++, new PlaylistMenuItem(this.player_, {
	        item: playlist[i]
	      }, this.options_));
	      cov_2c38f07m8x().s[147]++;
	      this.items.push(item);
	      cov_2c38f07m8x().s[148]++;
	      list.appendChild(item.el_);
	    } // Inject the ad overlay. We use this element to block clicks during ad
	    // playback and darken the menu to indicate inactivity


	    cov_2c38f07m8x().s[149]++;

	    if (!overlay) {
	      cov_2c38f07m8x().b[22][0]++;
	      cov_2c38f07m8x().s[150]++;
	      overlay = document_1.createElement('li');
	      cov_2c38f07m8x().s[151]++;
	      overlay.className = 'vjs-playlist-ad-overlay';
	      cov_2c38f07m8x().s[152]++;
	      list.appendChild(overlay);
	    } else {
	      cov_2c38f07m8x().b[22][1]++;
	      cov_2c38f07m8x().s[153]++; // Move overlay to end of list

	      list.appendChild(overlay);
	    } // select the current playlist item


	    const selectedIndex = (cov_2c38f07m8x().s[154]++, this.player_.playlist.currentItem());
	    cov_2c38f07m8x().s[155]++;

	    if ((cov_2c38f07m8x().b[24][0]++, this.items.length) && (cov_2c38f07m8x().b[24][1]++, selectedIndex >= 0)) {
	      cov_2c38f07m8x().b[23][0]++;
	      cov_2c38f07m8x().s[156]++;
	      addSelectedClass(this.items[selectedIndex]);
	      const thumbnail = (cov_2c38f07m8x().s[157]++, this.items[selectedIndex].$('.vjs-playlist-thumbnail'));
	      cov_2c38f07m8x().s[158]++;

	      if (thumbnail) {
	        cov_2c38f07m8x().b[25][0]++;
	        cov_2c38f07m8x().s[159]++;
	        dom$1.addClass(thumbnail, 'vjs-playlist-now-playing');
	      } else {
	        cov_2c38f07m8x().b[25][1]++;
	      }
	    } else {
	      cov_2c38f07m8x().b[23][1]++;
	    }
	  }

	  update() {
	    cov_2c38f07m8x().f[21]++; // replace the playlist items being displayed, if necessary

	    const playlist = (cov_2c38f07m8x().s[160]++, this.player_.playlist());
	    cov_2c38f07m8x().s[161]++;

	    if (this.items.length !== playlist.length) {
	      cov_2c38f07m8x().b[26][0]++;
	      cov_2c38f07m8x().s[162]++; // if the menu is currently empty or the state is obviously out
	      // of date, rebuild everything.

	      this.createPlaylist_();
	      cov_2c38f07m8x().s[163]++;
	      return;
	    } else {
	      cov_2c38f07m8x().b[26][1]++;
	    }

	    cov_2c38f07m8x().s[164]++;

	    for (let i = (cov_2c38f07m8x().s[165]++, 0); i < this.items.length; i++) {
	      cov_2c38f07m8x().s[166]++;

	      if (this.items[i].item !== playlist[i]) {
	        cov_2c38f07m8x().b[27][0]++;
	        cov_2c38f07m8x().s[167]++; // if any of the playlist items have changed, rebuild the
	        // entire playlist

	        this.createPlaylist_();
	        cov_2c38f07m8x().s[168]++;
	        return;
	      } else {
	        cov_2c38f07m8x().b[27][1]++;
	      }
	    } // the playlist itself is unchanged so just update the selection


	    const currentItem = (cov_2c38f07m8x().s[169]++, this.player_.playlist.currentItem());
	    cov_2c38f07m8x().s[170]++;

	    for (let i = (cov_2c38f07m8x().s[171]++, 0); i < this.items.length; i++) {
	      const item = (cov_2c38f07m8x().s[172]++, this.items[i]);
	      cov_2c38f07m8x().s[173]++;

	      if (i === currentItem) {
	        cov_2c38f07m8x().b[28][0]++;
	        cov_2c38f07m8x().s[174]++;
	        addSelectedClass(item);
	        cov_2c38f07m8x().s[175]++;

	        if (document_1.activeElement !== item.el()) {
	          cov_2c38f07m8x().b[29][0]++;
	          cov_2c38f07m8x().s[176]++;
	          dom$1.addClass(item.thumbnail, 'vjs-playlist-now-playing');
	        } else {
	          cov_2c38f07m8x().b[29][1]++;
	        }

	        cov_2c38f07m8x().s[177]++;
	        notUpNext(item);
	      } else {
	        cov_2c38f07m8x().b[28][1]++;
	        cov_2c38f07m8x().s[178]++;

	        if (i === currentItem + 1) {
	          cov_2c38f07m8x().b[30][0]++;
	          cov_2c38f07m8x().s[179]++;
	          removeSelectedClass(item);
	          cov_2c38f07m8x().s[180]++;
	          upNext(item);
	        } else {
	          cov_2c38f07m8x().b[30][1]++;
	          cov_2c38f07m8x().s[181]++;
	          removeSelectedClass(item);
	          cov_2c38f07m8x().s[182]++;
	          notUpNext(item);
	        }
	      }
	    }
	  }

	}
	/**
	* Returns a boolean indicating whether an element has child elements.
	*
	* Note that this is distinct from whether it has child _nodes_.
	*
	* @param  {HTMLElement} el
	*         A DOM element.
	*
	* @return {boolean}
	*         Whether the element has child elements.
	*/


	cov_2c38f07m8x().s[183]++;

	const hasChildEls = el => {
	  cov_2c38f07m8x().f[22]++;
	  cov_2c38f07m8x().s[184]++;

	  for (let i = (cov_2c38f07m8x().s[185]++, 0); i < el.childNodes.length; i++) {
	    cov_2c38f07m8x().s[186]++;

	    if (dom$1.isEl(el.childNodes[i])) {
	      cov_2c38f07m8x().b[31][0]++;
	      cov_2c38f07m8x().s[187]++;
	      return true;
	    } else {
	      cov_2c38f07m8x().b[31][1]++;
	    }
	  }

	  cov_2c38f07m8x().s[188]++;
	  return false;
	};
	/**
	* Finds the first empty root element.
	*
	* @param  {string} className
	*         An HTML class name to search for.
	*
	* @return {HTMLElement}
	*         A DOM element to use as the root for a playlist.
	*/


	cov_2c38f07m8x().s[189]++;

	const findRoot = className => {
	  cov_2c38f07m8x().f[23]++;
	  const all = (cov_2c38f07m8x().s[190]++, document_1.querySelectorAll('.' + className));
	  let el;
	  cov_2c38f07m8x().s[191]++;

	  for (let i = (cov_2c38f07m8x().s[192]++, 0); i < all.length; i++) {
	    cov_2c38f07m8x().s[193]++;

	    if (!hasChildEls(all[i])) {
	      cov_2c38f07m8x().b[32][0]++;
	      cov_2c38f07m8x().s[194]++;
	      el = all[i];
	      cov_2c38f07m8x().s[195]++;
	      break;
	    } else {
	      cov_2c38f07m8x().b[32][1]++;
	    }
	  }

	  cov_2c38f07m8x().s[196]++;
	  return el;
	};
	/**
	* Initialize the plugin on a player.
	*
	* @param  {Object} [options]
	*         An options object.
	*
	* @param  {HTMLElement} [options.el]
	*         A DOM element to use as a root node for the playlist.
	*
	* @param  {string} [options.className]
	*         An HTML class name to use to find a root node for the playlist.
	*
	* @param  {boolean} [options.playOnSelect = false]
	*         If true, will attempt to begin playback upon selecting a new
	*         playlist item in the UI.
	*/


	cov_2c38f07m8x().s[197]++;

	const playlistUi = function (options) {
	  cov_2c38f07m8x().f[24]++;
	  const player = (cov_2c38f07m8x().s[198]++, this);
	  cov_2c38f07m8x().s[199]++;

	  if (!player.playlist) {
	    cov_2c38f07m8x().b[33][0]++;
	    cov_2c38f07m8x().s[200]++;
	    throw new Error('videojs-playlist plugin is required by the videojs-playlist-ui plugin');
	  } else {
	    cov_2c38f07m8x().b[33][1]++;
	  }

	  cov_2c38f07m8x().s[201]++;

	  if (dom$1.isEl(options)) {
	    cov_2c38f07m8x().b[34][0]++;
	    cov_2c38f07m8x().s[202]++;
	    videojs__default["default"].log.warn('videojs-playlist-ui: Passing an element directly to playlistUi() is deprecated, use the "el" option instead!');
	    cov_2c38f07m8x().s[203]++;
	    options = {
	      el: options
	    };
	  } else {
	    cov_2c38f07m8x().b[34][1]++;
	  }

	  cov_2c38f07m8x().s[204]++;
	  options = videojs__default["default"].mergeOptions(defaults, options); // If the player is already using this plugin, remove the pre-existing
	  // PlaylistMenu, but retain the element and its location in the DOM because
	  // it will be re-used.

	  cov_2c38f07m8x().s[205]++;

	  if (player.playlistMenu) {
	    cov_2c38f07m8x().b[35][0]++;
	    const el = (cov_2c38f07m8x().s[206]++, player.playlistMenu.el()); // Catch cases where the menu may have been disposed elsewhere or the
	    // element removed from the DOM.

	    cov_2c38f07m8x().s[207]++;

	    if (el) {
	      cov_2c38f07m8x().b[36][0]++;
	      const parentNode = (cov_2c38f07m8x().s[208]++, el.parentNode);
	      const nextSibling = (cov_2c38f07m8x().s[209]++, el.nextSibling); // Disposing the menu will remove `el` from the DOM, but we need to
	      // empty it ourselves to be sure.

	      cov_2c38f07m8x().s[210]++;
	      player.playlistMenu.dispose();
	      cov_2c38f07m8x().s[211]++;
	      dom$1.emptyEl(el); // Put the element back in its place.

	      cov_2c38f07m8x().s[212]++;

	      if (nextSibling) {
	        cov_2c38f07m8x().b[37][0]++;
	        cov_2c38f07m8x().s[213]++;
	        parentNode.insertBefore(el, nextSibling);
	      } else {
	        cov_2c38f07m8x().b[37][1]++;
	        cov_2c38f07m8x().s[214]++;
	        parentNode.appendChild(el);
	      }

	      cov_2c38f07m8x().s[215]++;
	      options.el = el;
	    } else {
	      cov_2c38f07m8x().b[36][1]++;
	    }
	  } else {
	    cov_2c38f07m8x().b[35][1]++;
	  }

	  cov_2c38f07m8x().s[216]++;

	  if (!dom$1.isEl(options.el)) {
	    cov_2c38f07m8x().b[38][0]++;
	    cov_2c38f07m8x().s[217]++;
	    options.el = findRoot(options.className);
	  } else {
	    cov_2c38f07m8x().b[38][1]++;
	  }

	  cov_2c38f07m8x().s[218]++;
	  player.playlistMenu = new PlaylistMenu(player, options);
	}; // register components


	cov_2c38f07m8x().s[219]++;
	videojs__default["default"].registerComponent('PlaylistMenu', PlaylistMenu);
	cov_2c38f07m8x().s[220]++;
	videojs__default["default"].registerComponent('PlaylistMenuItem', PlaylistMenuItem); // register the plugin

	cov_2c38f07m8x().s[221]++;
	registerPlugin('playlistUi', playlistUi);
	cov_2c38f07m8x().s[222]++;
	playlistUi.VERSION = version;

	/* eslint-disable no-console */
	const playlist = [{
	  name: 'Movie 1',
	  description: 'Movie 1 description',
	  duration: 100,
	  data: {
	    id: '1',
	    foo: 'bar'
	  },
	  sources: [{
	    src: '//example.com/movie1.mp4',
	    type: 'video/mp4'
	  }]
	}, {
	  sources: [{
	    src: '//example.com/movie2.mp4',
	    type: 'video/mp4'
	  }],
	  thumbnail: '//example.com/movie2.jpg'
	}];

	const resolveUrl = url => {
	  const a = document_1.createElement('a');
	  a.href = url;
	  return a.href;
	};

	const dom = videojs__default["default"].dom || videojs__default["default"];
	const Html5 = videojs__default["default"].getTech('Html5');
	QUnit__default["default"].test('the environment is sane', function (assert) {
	  assert.ok(true, 'everything is swell');
	});

	function setup() {
	  this.oldVideojsBrowser = videojs__default["default"].browser;
	  videojs__default["default"].browser = videojs__default["default"].mergeOptions({}, videojs__default["default"].browser);
	  this.fixture = document_1.querySelector('#qunit-fixture'); // force HTML support so the tests run in a reasonable
	  // environment under phantomjs

	  this.realIsHtmlSupported = Html5.isSupported;

	  Html5.isSupported = function () {
	    return true;
	  }; // create a video element


	  const video = document_1.createElement('video');
	  this.fixture.appendChild(video); // create a video.js player

	  this.player = videojs__default["default"](video); // Create two playlist container elements.

	  this.fixture.appendChild(dom.createEl('div', {
	    className: 'vjs-playlist'
	  }));
	  this.fixture.appendChild(dom.createEl('div', {
	    className: 'vjs-playlist'
	  }));
	}

	function teardown() {
	  videojs__default["default"].browser = this.oldVideojsBrowser;
	  Html5.isSupported = this.realIsHtmlSupported;
	  this.player.dispose();
	  this.player = null;
	  dom.emptyEl(this.fixture);
	}

	QUnit__default["default"].module('videojs-playlist-ui', {
	  beforeEach: setup,
	  afterEach: teardown
	});
	QUnit__default["default"].test('registers itself', function (assert) {
	  assert.ok(this.player.playlistUi, 'registered the plugin');
	});
	QUnit__default["default"].test('errors if used without the playlist plugin', function (assert) {
	  assert.throws(function () {
	    this.player.playlist = null;
	    this.player.playlistUi();
	  }, 'threw on init');
	});
	QUnit__default["default"].test('is empty if the playlist plugin isn\'t initialized', function (assert) {
	  this.player.playlistUi();
	  const items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.ok(this.fixture.querySelector('.vjs-playlist'), 'created the menu');
	  assert.strictEqual(items.length, 0, 'displayed no items');
	});
	QUnit__default["default"].test('can be initialized with an element (deprecated form)', function (assert) {
	  const elem = dom.createEl('div');
	  this.player.playlist(playlist);
	  this.player.playlistUi(elem);
	  assert.strictEqual(elem.querySelectorAll('li.vjs-playlist-item').length, playlist.length, 'created an element for each playlist item');
	});
	QUnit__default["default"].test('can be initialized with an element', function (assert) {
	  const elem = dom.createEl('div');
	  this.player.playlist(playlist);
	  this.player.playlistUi({
	    el: elem
	  });
	  assert.strictEqual(elem.querySelectorAll('li.vjs-playlist-item').length, playlist.length, 'created an element for each playlist item');
	});
	QUnit__default["default"].test('can look for an element with the class "vjs-playlist" that is not already in use', function (assert) {
	  const firstEl = this.fixture.querySelectorAll('.vjs-playlist')[0];
	  const secondEl = this.fixture.querySelectorAll('.vjs-playlist')[1]; // Give the firstEl a child, so the plugin thinks it is in use and moves on
	  // to the next one.

	  firstEl.appendChild(dom.createEl('div'));
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  assert.strictEqual(this.player.playlistMenu.el(), secondEl, 'used the first matching/empty element');
	  assert.strictEqual(secondEl.querySelectorAll('li.vjs-playlist-item').length, playlist.length, 'found an element for each playlist item');
	});
	QUnit__default["default"].test('can look for an element with a custom class that is not already in use', function (assert) {
	  const firstEl = dom.createEl('div', {
	    className: 'super-playlist'
	  });
	  const secondEl = dom.createEl('div', {
	    className: 'super-playlist'
	  }); // Give the firstEl a child, so the plugin thinks it is in use and moves on
	  // to the next one.

	  firstEl.appendChild(dom.createEl('div'));
	  this.fixture.appendChild(firstEl);
	  this.fixture.appendChild(secondEl);
	  this.player.playlist(playlist);
	  this.player.playlistUi({
	    className: 'super-playlist'
	  });
	  assert.strictEqual(this.player.playlistMenu.el(), secondEl, 'used the first matching/empty element');
	  assert.strictEqual(this.fixture.querySelectorAll('li.vjs-playlist-item').length, playlist.length, 'created an element for each playlist item');
	});
	QUnit__default["default"].test('specializes the class name if touch input is absent', function (assert) {
	  videojs__default["default"].browser.TOUCH_ENABLED = false;
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  assert.ok(this.player.playlistMenu.hasClass('vjs-mouse'), 'marked the playlist menu');
	});
	QUnit__default["default"].test('can be re-initialized without doubling the contents of the list', function (assert) {
	  const el = this.fixture.querySelectorAll('.vjs-playlist')[0];
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  this.player.playlistUi();
	  this.player.playlistUi();
	  assert.strictEqual(this.player.playlistMenu.el(), el, 'used the first matching/empty element');
	  assert.strictEqual(el.querySelectorAll('li.vjs-playlist-item').length, playlist.length, 'found an element for each playlist item');
	});
	QUnit__default["default"].module('videojs-playlist-ui: Components', {
	  beforeEach: setup,
	  afterEach: teardown
	}); // --------------------
	// Creation and Updates
	// --------------------

	QUnit__default["default"].test('includes the video name if provided', function (assert) {
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  const items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items[0].querySelector('.vjs-playlist-name').textContent, playlist[0].name, 'wrote the name');
	  assert.strictEqual(items[1].querySelector('.vjs-playlist-name').textContent, 'Untitled Video', 'wrote a placeholder for the name');
	});
	QUnit__default["default"].test('includes the video description if user specifies it', function (assert) {
	  this.player.playlist(playlist);
	  this.player.playlistUi({
	    showDescription: true
	  });
	  const items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items[0].querySelector('.vjs-playlist-description').textContent, playlist[0].description, 'description is displayed');
	});
	QUnit__default["default"].test('hides video description by default', function (assert) {
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  const items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items[0].querySelector('.vjs-playlist-description'), null, 'description is not displayed');
	});
	QUnit__default["default"].test('includes custom data attribute if provided', function (assert) {
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  const items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items[0].dataset.id, playlist[0].data.id, 'set a single data attribute');
	  assert.strictEqual(items[0].dataset.id, '1', 'set a single data attribute (actual value)');
	  assert.strictEqual(items[0].dataset.foo, playlist[0].data.foo, 'set an addtional data attribute');
	  assert.strictEqual(items[0].dataset.foo, 'bar', 'set an addtional data attribute');
	});
	QUnit__default["default"].test('outputs a <picture> for simple thumbnails', function (assert) {
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  const pictures = this.fixture.querySelectorAll('.vjs-playlist-item picture');
	  assert.strictEqual(pictures.length, 1, 'output one picture');
	  const imgs = pictures[0].querySelectorAll('img');
	  assert.strictEqual(imgs.length, 1, 'output one img');
	  assert.strictEqual(imgs[0].src, window_1.location.protocol + playlist[1].thumbnail, 'set the src attribute');
	});
	QUnit__default["default"].test('outputs a <picture> for responsive thumbnails', function (assert) {
	  const playlistOverride = [{
	    sources: [{
	      src: '//example.com/movie.mp4',
	      type: 'video/mp4'
	    }],
	    thumbnail: [{
	      srcset: '/test/example/oceans.jpg',
	      type: 'image/jpeg',
	      media: '(min-width: 400px;)'
	    }, {
	      src: '/test/example/oceans-low.jpg'
	    }]
	  }];
	  this.player.playlist(playlistOverride);
	  this.player.playlistUi();
	  const sources = this.fixture.querySelectorAll('.vjs-playlist-item picture source');
	  const imgs = this.fixture.querySelectorAll('.vjs-playlist-item picture img');
	  assert.strictEqual(sources.length, 1, 'output one source');
	  assert.strictEqual(sources[0].srcset, playlistOverride[0].thumbnail[0].srcset, 'wrote the srcset attribute');
	  assert.strictEqual(sources[0].type, playlistOverride[0].thumbnail[0].type, 'wrote the type attribute');
	  assert.strictEqual(sources[0].media, playlistOverride[0].thumbnail[0].media, 'wrote the type attribute');
	  assert.strictEqual(imgs.length, 1, 'output one img');
	  assert.strictEqual(imgs[0].src, resolveUrl(playlistOverride[0].thumbnail[1].src), 'output the img src attribute');
	});
	QUnit__default["default"].test('outputs a placeholder for items without thumbnails', function (assert) {
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  const thumbnails = this.fixture.querySelectorAll('.vjs-playlist-item .vjs-playlist-thumbnail');
	  assert.strictEqual(thumbnails.length, playlist.length, 'output two thumbnails');
	  assert.strictEqual(thumbnails[0].nodeName.toLowerCase(), 'div', 'the second is a placeholder');
	});
	QUnit__default["default"].test('includes the duration if one is provided', function (assert) {
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  const durations = this.fixture.querySelectorAll('.vjs-playlist-item .vjs-playlist-duration');
	  assert.strictEqual(durations.length, 1, 'skipped the item without a duration');
	  assert.strictEqual(durations[0].textContent, '1:40', 'wrote the duration');
	  assert.strictEqual(durations[0].getAttribute('datetime'), 'PT0H0M' + playlist[0].duration + 'S', 'wrote a machine-readable datetime');
	});
	QUnit__default["default"].test('marks the selected playlist item on startup', function (assert) {
	  this.player.playlist(playlist);

	  this.player.currentSrc = () => playlist[0].sources[0].src;

	  this.player.playlistUi();
	  const selectedItems = this.fixture.querySelectorAll('.vjs-playlist-item.vjs-selected');
	  assert.strictEqual(selectedItems.length, 1, 'marked one playlist item');
	  assert.strictEqual(selectedItems[0].querySelector('.vjs-playlist-name').textContent, playlist[0].name, 'marked the first playlist item');
	});
	QUnit__default["default"].test('updates the selected playlist item on loadstart', function (assert) {
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  this.player.playlist.currentItem(1);

	  this.player.currentSrc = () => playlist[1].sources[0].src;

	  this.player.trigger('loadstart');
	  const selectedItems = this.fixture.querySelectorAll('.vjs-playlist-item.vjs-selected');
	  assert.strictEqual(this.fixture.querySelectorAll('.vjs-playlist-item').length, playlist.length, 'displayed the correct number of items');
	  assert.strictEqual(selectedItems.length, 1, 'marked one playlist item');
	  assert.strictEqual(selectedItems[0].querySelector('img').src, resolveUrl(playlist[1].thumbnail), 'marked the second playlist item');
	});
	QUnit__default["default"].test('selects no item if the playlist is not in use', function (assert) {
	  this.player.playlist(playlist);

	  this.player.playlist.currentItem = () => -1;

	  this.player.playlistUi();
	  this.player.trigger('loadstart');
	  assert.strictEqual(this.fixture.querySelectorAll('.vjs-playlist-item.vjs-selected').length, 0, 'no items selected');
	});
	QUnit__default["default"].test('updates on "playlistchange", different lengths', function (assert) {
	  this.player.playlist([]);
	  this.player.playlistUi();
	  let items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, 0, 'no items initially');
	  this.player.playlist(playlist);
	  this.player.trigger('playlistchange');
	  items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, playlist.length, 'updated with the new items');
	});
	QUnit__default["default"].test('updates on "playlistchange", equal lengths', function (assert) {
	  this.player.playlist([{
	    sources: []
	  }, {
	    sources: []
	  }]);
	  this.player.playlistUi();
	  let items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, 2, 'two items initially');
	  this.player.playlist(playlist);
	  this.player.trigger('playlistchange');
	  items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, playlist.length, 'updated with the new items');
	  assert.strictEqual(this.player.playlistMenu.items[0].item, playlist[0], 'we have updated items');
	  assert.strictEqual(this.player.playlistMenu.items[1].item, playlist[1], 'we have updated items');
	});
	QUnit__default["default"].test('updates on "playlistchange", update selection', function (assert) {
	  this.player.playlist(playlist);

	  this.player.currentSrc = function () {
	    return playlist[0].sources[0].src;
	  };

	  this.player.playlistUi();
	  let items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, 2, 'two items initially');
	  assert.ok(/vjs-selected/.test(items[0].getAttribute('class')), 'first item is selected by default');
	  this.player.playlist.currentItem(1);

	  this.player.currentSrc = function () {
	    return playlist[1].sources[0].src;
	  };

	  this.player.trigger('playlistchange');
	  items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, playlist.length, 'updated with the new items');
	  assert.ok(/vjs-selected/.test(items[1].getAttribute('class')), 'second item is selected after update');
	  assert.ok(!/vjs-selected/.test(items[0].getAttribute('class')), 'first item is not selected after update');
	});
	QUnit__default["default"].test('updates on "playlistsorted", different lengths', function (assert) {
	  this.player.playlist([]);
	  this.player.playlistUi();
	  let items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, 0, 'no items initially');
	  this.player.playlist(playlist);
	  this.player.trigger('playlistsorted');
	  items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, playlist.length, 'updated with the new items');
	});
	QUnit__default["default"].test('updates on "playlistsorted", equal lengths', function (assert) {
	  this.player.playlist([{
	    sources: []
	  }, {
	    sources: []
	  }]);
	  this.player.playlistUi();
	  let items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, 2, 'two items initially');
	  this.player.playlist(playlist);
	  this.player.trigger('playlistsorted');
	  items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, playlist.length, 'updated with the new items');
	  assert.strictEqual(this.player.playlistMenu.items[0].item, playlist[0], 'we have updated items');
	  assert.strictEqual(this.player.playlistMenu.items[1].item, playlist[1], 'we have updated items');
	});
	QUnit__default["default"].test('updates on "playlistsorted", update selection', function (assert) {
	  this.player.playlist(playlist);

	  this.player.currentSrc = function () {
	    return playlist[0].sources[0].src;
	  };

	  this.player.playlistUi();
	  let items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, 2, 'two items initially');
	  assert.ok(/vjs-selected/.test(items[0].getAttribute('class')), 'first item is selected by default');
	  this.player.playlist.currentItem(1);

	  this.player.currentSrc = function () {
	    return playlist[1].sources[0].src;
	  };

	  this.player.trigger('playlistsorted');
	  items = this.fixture.querySelectorAll('.vjs-playlist-item');
	  assert.strictEqual(items.length, playlist.length, 'updated with the new items');
	  assert.ok(/vjs-selected/.test(items[1].getAttribute('class')), 'second item is selected after update');
	  assert.ok(!/vjs-selected/.test(items[0].getAttribute('class')), 'first item is not selected after update');
	});
	QUnit__default["default"].test('tracks when an ad is playing', function (assert) {
	  this.player.playlist([]);
	  this.player.playlistUi();

	  this.player.duration = () => 5;

	  const playlistMenu = this.player.playlistMenu;
	  assert.ok(!playlistMenu.hasClass('vjs-ad-playing'), 'does not have class vjs-ad-playing');
	  this.player.trigger('adstart');
	  assert.ok(playlistMenu.hasClass('vjs-ad-playing'), 'has class vjs-ad-playing');
	  this.player.trigger('adend');
	  assert.ok(!playlistMenu.hasClass('vjs-ad-playing'), 'does not have class vjs-ad-playing');
	}); // -----------
	// Interaction
	// -----------

	QUnit__default["default"].test('changes the selection when tapped', function (assert) {
	  let playCalled = false;
	  this.player.playlist(playlist);
	  this.player.playlistUi({
	    playOnSelect: true
	  });

	  this.player.play = function () {
	    playCalled = true;
	  };

	  let sources;

	  this.player.src = src => {
	    if (src) {
	      sources = src;
	    }

	    return sources[0];
	  };

	  this.player.currentSrc = () => sources[0].src;

	  this.player.playlistMenu.items[1].trigger('tap'); // trigger a loadstart synchronously to simplify the test

	  this.player.trigger('loadstart');
	  assert.ok(this.player.playlistMenu.items[1].hasClass('vjs-selected'), 'selected the new item');
	  assert.ok(!this.player.playlistMenu.items[0].hasClass('vjs-selected'), 'deselected the old item');
	  assert.strictEqual(playCalled, true, 'play gets called if option is set');
	});
	QUnit__default["default"].test('play should not get called by default upon selection of menu items ', function (assert) {
	  let playCalled = false;
	  this.player.playlist(playlist);
	  this.player.playlistUi();

	  this.player.play = function () {
	    playCalled = true;
	  };

	  let sources;

	  this.player.src = src => {
	    if (src) {
	      sources = src;
	    }

	    return sources[0];
	  };

	  this.player.currentSrc = () => sources[0].src;

	  this.player.playlistMenu.items[1].trigger('tap'); // trigger a loadstart synchronously to simplify the test

	  this.player.trigger('loadstart');
	  assert.strictEqual(playCalled, false, 'play should not get called by default');
	});
	QUnit__default["default"].test('disposing the playlist menu nulls out the player\'s reference to it', function (assert) {
	  assert.strictEqual(this.fixture.querySelectorAll('.vjs-playlist').length, 2, 'there are two playlist containers at the start');
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  this.player.playlistMenu.dispose();
	  assert.strictEqual(this.fixture.querySelectorAll('.vjs-playlist').length, 1, 'only the unused playlist container is left');
	  assert.strictEqual(this.player.playlistMenu, null, 'the playlistMenu property is null');
	});
	QUnit__default["default"].test('disposing the playlist menu removes playlist menu items', function (assert) {
	  assert.strictEqual(this.fixture.querySelectorAll('.vjs-playlist').length, 2, 'there are two playlist containers at the start');
	  this.player.playlist(playlist);
	  this.player.playlistUi(); // Cache some references so we can refer to them after disposal.

	  const items = [].concat(this.player.playlistMenu.items);
	  this.player.playlistMenu.dispose();
	  assert.strictEqual(this.fixture.querySelectorAll('.vjs-playlist').length, 1, 'only the unused playlist container is left');
	  assert.strictEqual(this.player.playlistMenu, null, 'the playlistMenu property is null');
	  items.forEach(i => {
	    assert.strictEqual(i.el_, null, `the item "${i.id_}" has been disposed`);
	  });
	});
	QUnit__default["default"].test('disposing the player also disposes the playlist menu', function (assert) {
	  assert.strictEqual(this.fixture.querySelectorAll('.vjs-playlist').length, 2, 'there are two playlist containers at the start');
	  this.player.playlist(playlist);
	  this.player.playlistUi();
	  this.player.dispose();
	  assert.strictEqual(this.fixture.querySelectorAll('.vjs-playlist').length, 1, 'only the unused playlist container is left');
	  assert.strictEqual(this.player.playlistMenu, null, 'the playlistMenu property is null');
	});

})(QUnit, videojs);
