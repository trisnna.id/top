/**
 * This scripts will handle client/server side process that
 * related with resources module
 *
 * @author Tris <tris@grtech.com.my>
 */
$(function (jq) {
    jq("#linkPreviewContainer").data({ links: [] });
});

// Handle thumbnail selector
$(function (jq) {
    let input = jq("input[name='thumbnail']");
    let hex_input = jq("input[name='thumbnail_hex']");
    let placeholder = jq(".image-placeholder");
    let file = null;
    let reader = null;
    let cropper = null;

    input.on("change", function (e) {
        const $this = jq(this);
        file = $this[0].files[0];
        reader = new FileReader();

        // open modal with id 'cropper-modal', then initialize cropper.js
        jq("#cropper-modal").modal("show");
    });
    jq("#cropper-modal").on("show.bs.modal", function () {
        console.log(1);
        let image = document.getElementById("cropper-image");
        image.src = URL.createObjectURL(file);
        cropper = new Cropper(image, {
            aspectRatio: 6 / 4,
            viewMode: 0,
            strict: true,

            // minCanvasWidth is 100% of it's parent element
            minCanvasWidth: 100,
        });

        jq("#cropper-save").on("click", function () {
            // let canvas = cropper.getCroppedCanvas({
            //     width: 400,
            //     height: 225,
            // });
            // do same, but with aspect ratio 6/4
            let canvas = cropper.getCroppedCanvas({
                width: 600,
                height: 400,
            });

            canvas.toBlob(function (blob) {
                let url = URL.createObjectURL(blob);
                let reader = new FileReader();

                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    let base64data = reader.result;
                    let hex = base64data.split(",")[1];

                    // input.val(url);
                    hex_input.val(hex);
                    placeholder.attr("src", url);

                    // set placeholder background image
                    placeholder.css("background-image", `url(${url})`);

                    jq("#cropper-modal").modal("hide");
                };
            });
        });
    });

    jq("#cropper-modal").on("hide.bs.modal", function () {
        jq("#cropper-submit").off("click");

        cropper.destroy();

        let image = document.getElementById("cropper-image");
        image.src = "";
    });
});

$(function (jq) {
    "use strict";
    console.log();

    const Component = {
        error: function (message) {
            if (Boolean(message)) {
                return jq(document.createElement("div"), {
                    class: "fv-plugins-message-container invalid-feedback",
                    text: message,
                });
            }
        },
        editor: function (selector) {
            const options = {
                selector: `#${selector}`,
                height: "350",
                plugins: "image",
                menubar: "file edit view format table tools help ",
                toolbar: "image | outdent indent | undo redo",
                file_picker_callback: function (callback, value, meta) {
                    var input = document.createElement("input");
                    input.setAttribute("type", "file");
                    input.setAttribute("accept", "image/*");

                    /*
            Note: In modern browsers input[type="file"] is functional without
            even adding it to the DOM, but that might not be the case in some older
            or quirky browsers like IE, so you might want to add it to the DOM
            just in case, and visually hide it. And do not forget do remove it
            once you do not need it anymore.
          */

                    input.onchange = function () {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.onload = function () {
                            /*
                Note: Now we need to register the blob in TinyMCEs image blob
                registry. In the next release this part hopefully won't be
                necessary, as we are looking to handle it internally.
              */
                            var id = "blobid" + new Date().getTime();
                            var blobCache =
                                tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(",")[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);

                            /* call the callback and populate the Title field with the file name */
                            callback(blobInfo.blobUri(), { title: file.name });
                        };
                        reader.readAsDataURL(file);
                    };

                    input.click();
                },
            };

            tinymce.init(options);
        },
    };

    Component.editor("contentEditor");

    var myDropzone = new Dropzone("#resourceMedia", {
        url: "https://keenthemes.com/scripts/void.php", // Set the url for your upload script location
        paramName: "file", // The name that will be used to transfer the file
        maxFiles: 10,
        maxFilesize: 10, // MB
        addRemoveLinks: true,
        accept: function (file, done) {
            if (file.name == "wow.jpg") {
                done("Naha, you don't.");
            } else {
                done();
            }
        },
    });

    class ResourceLink {
        constructor(selector) {
            const container = jq(selector);

            this.errors = {};
            this.linkCount = 0;
            this._links = [];
            this.element = {
                container: {
                    root: container,
                    preview: container.find("#linkPreviewContainer"),
                },
                inputs: container.find("input").filter(".resource-link"),
                addBtn: container.find("a#link"),
            };
        }

        /**
         * @param {any} link
         */
        set links(link) {
            const key = this.linkCount;
            this.links.push({ key: key, link: link });
        }

        /**
         * @return {Array} array of link
         */
        get links() {
            return this._links;
        }

        validate(value) {
            const errors = {};

            if (typeof value !== "object") {
                throw new Error("The resource link is invalid");
            }

            if (!value.description.length) {
                errors.description = "The description is required";
            }

            if (!value.text.length) {
                errors.text = "The text to display is required";
            }

            if (!value.url.length) {
                errors.url = "The url is required";
            }

            this.errors = errors;
        }

        handleEditLink(key, e) {
            alert("Comming soon.");

            e.preventDefault();

            // const value = this.links.at(key - 1);
            // const link = value.link;

            // this.element.inputs.each(function (_i, element) {
            //   const $elm = jq(element)
            //   const name = $elm.attr('name');

            //   if (name.includes('description')) {
            //     $elm.val(link.description)
            //   } else if (name.includes('text')) {
            //     $elm.val(link.text)
            //   } else if (name.includes('url')) {
            //     $elm.val(link.url)
            //   }
            // })
        }

        handleDeleteLink(key, e) {
            if (e) {
                e.preventDefault();
            }

            return jq(`div[data-link=${key}]`).remove();
        }

        handleUpdateLink(nextLink) {
            const prevLink = this.linkToUpdate;
            console.log(this);
            console.log(prevLink);
            console.log(nextLink);
        }

        renderCollapsible(description, text, url, key) {
            const container = jq(document.createElement("div"))
                .addClass(
                    "d-flex align-items-center collapsible rotate collapsed"
                )
                .attr({
                    "aria-controls": `link_${key}`,
                    "aria-expanded": "false",
                    "data-bs-toggle": "collapse",
                    href: `#link_${key}`,
                    role: "button",
                });

            const caretIcon = jq(document.createElement("div")).addClass(
                "me-3 rotate-90"
            ).append(`
          <span
            class="svg-icon svg-icon-3"
          >
            <svg
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                  d="M12.6343 12.5657L8.45001 16.75C8.0358 17.1642 8.0358 17.8358 8.45001 18.25C8.86423 18.6642 9.5358 18.6642 9.95001 18.25L15.4929 12.7071C15.8834 12.3166 15.8834 11.6834 15.4929 11.2929L9.95001 5.75C9.5358 5.33579 8.86423 5.33579 8.45001 5.75C8.0358 6.16421 8.0358 6.83579 8.45001 7.25L12.6343 11.4343C12.9467 11.7467 12.9467 12.2533 12.6343 12.5657Z"
                  fill="currentColor"></path>
            </svg>
          </span>
        `);

            const link = jq(document.createElement("div"))
                .addClass("me-3")
                .append(
                    jq(document.createElement("div"))
                        .addClass("d-flex align-items-center")
                        .append([
                            jq(document.createElement("div"))
                                .addClass("text-gray-800 fw-bold")
                                .text(description),
                            jq(document.createElement("div"))
                                .addClass("badge badge-light-primary ms-5")
                                .append(
                                    jq(document.createElement("a"))
                                        .attr({
                                            target: "_blank",
                                            href: url,
                                        })
                                        .append(`<u>${text}</u>`)
                                ),
                        ])
                );

            return container.append([caretIcon, link]);
        }

        renderActionButtons(key) {
            const self = this;
            const container = jq(document.createElement("div")).addClass(
                "d-flex my-3 ms-9"
            );

            for (let i = 0; i < 2; i++) {
                const text = i % 2 === 0 ? "Edit" : "Delete";

                const btn = jq(document.createElement("a"))
                    .addClass(
                        "btn btn-icon btn-active-light-danger w-30px h-30px me-3"
                    )
                    .attr({ href: "#" });

                const span = jq(document.createElement("span")).attr({
                    "data-bs-toggle": "toggle",
                    "data-bs-trigger": "hover",
                    "aria-label": text,
                    "data-bs-original-title": text,
                    "data-kt-initialized": i + 1,
                });
                const spanIcon = jq(document.createElement("span")).addClass(
                    "svg-icon svg-icon-3"
                );

                if (i % 2 === 0) {
                    spanIcon
                        .append(
                            `
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path opacity="0.3"
                    d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z"
                    fill="currentColor"></path>
                <path
                    d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z"
                    fill="currentColor"></path>
              </svg>
            `
                        )
                        .on("click", function (e) {
                            self.handleEditLink(key, e);
                        });
                } else {
                    spanIcon
                        .append(
                            `
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                      d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z"
                      fill="currentColor"></path>
                  <path opacity="0.5"
                      d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z"
                      fill="currentColor"></path>
                  <path opacity="0.5"
                      d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z"
                      fill="currentColor"></path>
              </svg>
            `
                        )
                        .on("click", function (e) {
                            self.handleDeleteLink(key, e);
                        });
                }

                btn.append(span.append(spanIcon)).appendTo(container);
            }

            return container;
        }

        renderLinkPreview(value) {
            const key = this.linkCount;
            const description = value.description;
            const text = value.text;
            const url = value.url;

            // First child of root container
            const container = jq(document.createElement("div"))
                .addClass("py-0")
                .attr({
                    "data-link": key,
                    "data-kt-customer-payment-method": "row",
                });

            // First child of the container elemenent
            const innerContainer = jq(document.createElement("div"))
                .addClass("py-3 d-flex flex-stack flex-wrap")
                .append([
                    this.renderCollapsible(description, text, url, key),
                    this.renderActionButtons(key),
                ]);

            // Second child of the container element
            // This element will contains the detail inputs of resource link
            const tableContainer = jq(document.createElement("div"))
                .addClass("fs-6 ps-10 collapse")
                .attr({ id: `link_${key}` })
                .append([
                    jq(document.createElement("div"))
                        .addClass("d-flex flex-wrap py-5")
                        .append(
                            jq(document.createElement("div"))
                                .addClass("flex-equal me-5")
                                .append(
                                    this.renderTable(description, text, url)
                                )
                        ),
                ]);

            const preview = this.element.container.preview;
            if (preview.children().length < 1) {
                preview.append([
                    container.append([innerContainer, tableContainer]),
                ]);
            } else {
                preview.append([
                    '<div class="separator separator-dashed"></div>',
                    container.append([innerContainer, tableContainer]),
                ]);
            }

            this.links = value;
        }

        renderTable(description, text, url) {
            const table = jq(document.createElement("table"))
                .addClass("table table-flush fw-semibold gy-1")
                .append(document.createElement("tbody"));

            for (const value of [
                ["Description", description],
                ["Text to Display", text],
                ["Link", url],
            ]) {
                table.find("tbody").append(
                    jq(document.createElement("tr")).append(`
              <td class="min-w-125px w-125px">${value.at(0)}</td>
              <td class="text-gray-800">${value.at(1)}</td>
            `)
                );
            }

            return table;
        }

        static render() {
            const component = new ResourceLink("#resourceLinkContainer");
            const addBtn = component.element.addBtn;
            const inputs = component.element.inputs;

            addBtn.on("click", function (e) {
                e.preventDefault();

                // This var will contains the reduce values of resource link input
                // as an object => { description: string, text: string, url: string }
                const value = Object.assign(
                    {},
                    inputs.get().reduce(function (prevObj, currObj) {
                        const input = jq(currObj);
                        const name = input.attr("name");
                        const value = input.val();

                        if (name.includes("description")) {
                            prevObj["description"] = value;
                        } else if (name.includes("text")) {
                            prevObj["text"] = value;
                        } else if (name.includes("url")) {
                            prevObj["url"] = value;
                        }

                        input.val(null);
                        return prevObj;
                    }, {})
                );

                try {
                    component.validate(value);

                    if (Object.keys(component.errors).length === 0) {
                        component.linkCount += 1;
                        component.renderLinkPreview(value);
                    }

                    if (component.linkCount >= 1) {
                        jq("#previewContainer > div").fadeIn("slow");
                    }
                } catch (error) {
                    console.log(error);
                    // handle error message
                }
            });
        }
    }

    ResourceLink.render();
});
