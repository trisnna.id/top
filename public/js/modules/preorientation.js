$(function (jq) {
    const currentMCQ = JSON.parse($("input#MCQ").val() || "[]");
    const mcqContainer = $("#mcqContainer");
    const addQuestionBtn = $("#addQuestion");
    const inputQuiz = $("input[name=quiz]");
    const ACTION = {
        add_question: "add.questions",
        update_question: "update.questions",
        remove_question: "remove.questions",
        update_shuffles: "update.shuffles",
        update_answers: "update.answers",
        open_quiz: "quiz.open",
        close_quiz: "quiz.close",
    };

    const mcq = $.extend(
        $({}),
        (function (jObject) {
            "use strict";

            const state = {
                answers: [],
                shuffles: [],
                questions: [],
                open: false,
            };
            jObject.state = {
                update: function (action) {
                    const payload = arguments[1];
                    const prevState = Object.keys(state).reduce(function (
                        obj,
                        key
                    ) {
                        obj[key] = state[key];
                        return obj;
                    },
                    {});

                    function setState(values) {
                        for (const key of Object.keys(values)) {
                            state[key] = values[key];
                        }
                    }

                    if (action === ACTION.add_question) {
                        setState(payload.value);
                        return jObject.trigger("did.update", [
                            {
                                current: state,
                                prev: prevState,
                                action: action,
                                payload: payload,
                            },
                        ]);
                    }

                    if (action === ACTION.remove_question) {
                        state.questions = excludeByIndex(
                            state.questions,
                            payload.value
                        );
                        state.answers = excludeByIndex(
                            state.answers,
                            payload.value
                        );
                        state.shuffles = excludeByIndex(
                            state.shuffles,
                            payload.value
                        );

                        return jObject.trigger("did.update", [
                            {
                                current: state,
                                prev: prevState,
                                action: action,
                                payload: payload,
                            },
                        ]);
                    }

                    if (action === ACTION.update_answers) {
                        state.answers[payload.answerIndex][
                            payload.optionIndex
                        ] = payload.value;

                        return jObject.trigger("did.update", [
                            {
                                current: state,
                                prev: prevState,
                                action: action,
                                payload: payload,
                            },
                        ]);
                    }

                    if (
                        [ACTION.open_quiz, ACTION.close_quiz].includes(action)
                    ) {
                        state.open = payload.value;
                        return jObject.trigger("did.update", [
                            {
                                current: state,
                                prev: prevState,
                                action: action,
                                payload: payload,
                            },
                        ]);
                    }
                },
                get: function (key) {
                    const keys = Object.keys(state);

                    if (keys.includes(key)) {
                        return state[key];
                    }

                    return state;
                },
            };

            const component = {
                QuestionTabButton: function (props) {
                    const component = createElement("li")
                        .addClass(
                            `nav-item question-sequence ${props.className}`
                        )
                        .attr({ id: props.id })
                        .append([
                            createElement("a")
                                .addClass(
                                    "nav-link btn btn-active-light btn-color-gray-800 btn-active-color-success rounded-bottom-0"
                                )
                                .attr({
                                    href: props.target,
                                    "data-bs-toggle": "tab",
                                })
                                .append([
                                    createElement("i").addClass(
                                        "bi bi-trash fs-4 me-2 text-danger question-remove"
                                    ),
                                    props.title,
                                ]),
                        ]);

                    component.insertBefore("#addQuestionContainer");

                    return component;
                },
                inputQuestion: function (props) {
                    return createElement("textarea")
                        .addClass(
                            "form-control form-control-solid mcq-question"
                        )
                        .attr({
                            name: `mcq[${props.index}][question]`,
                            value: props.value,
                            cols: "15",
                            rows: "10",
                            placeholder: "Put the question here...",
                        })
                        .on("blur", function () {
                            state.questions[props.index] = $(this).val();
                        })
                        .val(state.questions[props.index]);
                },
                inputAnswers: function (props) {
                    const answers = state.answers[props.index];
                    const flexContainer = createElement("div").addClass(
                        "d-flex flex-column gap-5"
                    );
                    const listContainer = createElement("li").addClass(
                        "d-flex flex-column align-items-start py-5"
                    );

                    for (let i = 0; i <= answers.length / 2; i += 2) {
                        let start = i,
                            end = null;
                        for (let j = 0; j <= i + 2; j += 2) {
                            end = j;
                        }

                        const row = createElement("div").addClass(
                            "d-flex flex-stack gap-7"
                        );
                        state.answers[props.index]
                            .slice(start, end)
                            .reduce(function (list, item, i) {
                                const answerIndex =
                                    state.answers[props.index].indexOf(item);
                                const flexContainer =
                                    createElement("div").addClass(
                                        "d-flex gap-2"
                                    );
                                const inputContainer =
                                    createElement("div").addClass(
                                        "form-floating"
                                    );

                                const input = createElement("input")
                                    .addClass(
                                        `form-control ${
                                            item.mark_as_answer
                                                ? "is-valid"
                                                : "form-control-solid"
                                        } input-answer`
                                    )
                                    .attr({
                                        id: `answer${props.index}${item.key}`,
                                        name: `mcq[${props.index}][answers][${answerIndex}][${item.key}]`,
                                        placeholder: `Option ${item.key.toUpperCase()}`,
                                    })
                                    .on("change", function () {
                                        const input = $(this);
                                        state.answers[props.index].forEach(
                                            function (answer) {
                                                if (answer.key === item.key) {
                                                    answer["value"] =
                                                        input.val();
                                                }
                                            }
                                        );
                                    })
                                    .val(
                                        (function (answers) {
                                            const option = answers.find(
                                                function (answer) {
                                                    return (
                                                        answer.key === item.key
                                                    );
                                                }
                                            );

                                            return option ? option.value : null;
                                        })(state.answers[props.index])
                                    );

                                const label = createElement("label")
                                    .attr({ for: `option_${item.key}` })
                                    .append(
                                        createElement("strong").text(
                                            `Option [${item.key.toUpperCase()}]`
                                        )
                                    );

                                list.push(
                                    createElement("div")
                                        .addClass("col-md-6")
                                        .append([
                                            flexContainer.append([
                                                inputContainer.append([
                                                    input,
                                                    label,
                                                ]),
                                            ]),
                                            createElement("div")
                                                .addClass(
                                                    "form-check form-check-inline py-2"
                                                )
                                                .append([
                                                    createElement("input")
                                                        .addClass(
                                                            "form-check-input markable-answer"
                                                        )
                                                        .attr({
                                                            type: "checkbox",
                                                            name: `mcq[${props.index}][answers][${answerIndex}][mark_as_answer]`,
                                                            "data-answer-index":
                                                                props.index,
                                                            "data-answer-key":
                                                                item.key,
                                                            checked: (function (
                                                                answers
                                                            ) {
                                                                const option =
                                                                    answers.find(
                                                                        function (
                                                                            answer
                                                                        ) {
                                                                            return (
                                                                                answer.key ===
                                                                                item.key
                                                                            );
                                                                        }
                                                                    );

                                                                return (
                                                                    option &&
                                                                    option.mark_as_answer
                                                                );
                                                            })(
                                                                state.answers[
                                                                    props.index
                                                                ]
                                                            ),
                                                        })
                                                        .css(
                                                            "cursor",
                                                            "pointer"
                                                        ),
                                                    "Mark as correct answer",
                                                ]),
                                        ])
                                );

                                return list;
                            }, [])
                            .forEach(function (elements) {
                                listContainer.append(row.append(elements));
                            });
                    }

                    return flexContainer.append(listContainer);
                },
                inputSettings: function (props) {
                    const settings = [
                        /* {
          title: 'Mark all as true',
          description: 'Set all options as true answers',
          className: 'mark-all-answers',
          checked: state.answers[props.index].some(function (option) {
            return option.mark_as_answer === true;
          })
        },  */ {
                            title: "Shuffle the options",
                            description:
                                "When you activated this setting, Students will see these options in random order",
                            className: "mark-as-shuffle",
                            checked: state.shuffles[props.index] === true,
                            name: `mcq[${props.index}][settings][shuffle]`,
                        },
                    ].reduce(function (settings, item, i) {
                        settings.push(
                            createElement("div")
                                .addClass("d-flex flex-stack")
                                .append([
                                    createElement("div")
                                        .addClass("d-flex")
                                        .append(
                                            createElement("div")
                                                .addClass("d-flex flex-column")
                                                .append([
                                                    createElement("span")
                                                        .addClass(
                                                            "fs-5 text-dark text-hover-primary fw-bold"
                                                        )
                                                        .text(item.title),
                                                    createElement("div")
                                                        .addClass(
                                                            "fs-6 fw-semibold text-muted text-hover-danger"
                                                        )
                                                        .text(item.description),
                                                ])
                                        ),
                                    createElement("div")
                                        .addClass("d-flex justify-content-end")
                                        .append([
                                            createElement("label")
                                                .addClass(
                                                    "form-check form-switch form-switch-sm form-check-custom form-check-solid"
                                                )
                                                .append([
                                                    createElement("input")
                                                        .addClass(
                                                            `form-check-input ${item.className}`
                                                        )
                                                        .attr({
                                                            id:
                                                                "setting" +
                                                                props.index +
                                                                i,
                                                            type: "checkbox",
                                                            "data-question-index":
                                                                props.index,
                                                            checked:
                                                                item.checked,
                                                            name: item.name,
                                                        })
                                                        .on(
                                                            "change",
                                                            function () {
                                                                state.shuffles[
                                                                    props.index
                                                                ] =
                                                                    $(this).is(
                                                                        ":checked"
                                                                    );
                                                            }
                                                        ),
                                                    createElement("span")
                                                        .addClass(
                                                            "form-check-label fw-semibold text-muted"
                                                        )
                                                        .attr({
                                                            for:
                                                                "setting" +
                                                                props.index +
                                                                i,
                                                        }),
                                                ]),
                                        ]),
                                ]),
                            createElement("div").addClass(
                                "separator separator-dashed my-5"
                            )
                        );
                        return settings;
                    }, []);

                    return createElement("div")
                        .addClass("card mb-5 mb-xl-8")
                        .append([
                            createElement("div")
                                .addClass("card-header border-0")
                                .append(
                                    createElement("div")
                                        .addClass("card-title")
                                        .append(
                                            createElement("div")
                                                .addClass("fw-bold m-0")
                                                .text("Question Settings")
                                        )
                                ),
                            createElement("div")
                                .addClass("card-body pt-2")
                                .append(
                                    createElement("div")
                                        .addClass("py-2")
                                        .append(settings)
                                ),
                        ]);
                },
                tabContainer: function (props, components) {
                    const component = createElement("div")
                        .addClass(`tab-pane fade ${props.className}`)
                        .attr({ id: props.id, role: "tabpanel" })
                        .append(
                            createElement("div")
                                .addClass(
                                    "d-flex flex-column mb-5 fv-row fv-plugins-icon-container"
                                )
                                .append(
                                    createElement("div")
                                        .addClass("row")
                                        .append([
                                            createElement("div")
                                                .addClass("col-md-8")
                                                .append([
                                                    components.question,
                                                    components.answers,
                                                ]),
                                            createElement("div")
                                                .addClass("col-md-4")
                                                .append([
                                                    this.inputSettings(props),
                                                ]),
                                        ])
                                )
                        );

                    component.appendTo("#mcqTabContent");

                    return component;
                },
            };

            function excludeByIndex(arr, index) {
                return arr.filter(function (_values, i) {
                    return i !== index;
                });
            }

            function createElement(name) {
                return $(document.createElement(name));
            }

            function handleStateAnswers(answers, questionIndex) {
                /* const markAllbtn = $('.mark-all-answers');

markAllbtn
.eq(questionIndex)
.on('change', function (e) {
	const $this = $(this);
	const checked = Boolean($this.is(':checked'));

	answers
		.filter(function (option) {
			return option.mark_as_answer !== checked;
		}).forEach(function (option, index) {
			const indexOfOption = answers.indexOf(option)
			$(`input[name="mcq[${questionIndex}][answers][${indexOfOption}][mark_as_answer]"]`)
				.attr('checked', checked)
				.triggerHandler('change')
		});
}) */

                answers.forEach(function (option, index) {
                    $(
                        `input[name="mcq[${questionIndex}][answers][${index}][mark_as_answer]"]`
                    ).on("change", function (e) {
                        const $this = $(this);
                        const checked = $this.is(":checked");
                        const target = $(
                            `#answer${$this.attr(
                                "data-answer-index"
                            )}${$this.attr("data-answer-key")}`
                        );

                        if (checked) {
                            target
                                .removeClass("form-control-solid")
                                .addClass("is-valid");
                        } else {
                            target
                                .removeClass("is-valid")
                                .addClass("form-control-solid");
                        }

                        jObject.state.update(ACTION.update_answers, {
                            answerIndex: questionIndex,
                            optionIndex: answers.indexOf(option),
                            value: Object.assign(option, {
                                mark_as_answer: checked,
                            }),
                        });
                    });
                });
            }

            jObject.unmount = function () {
                jObject.trigger("did.update", [
                    {
                        current: Object.assign(state, {
                            answers: [],
                            questions: [],
                            shuffles: [],
                        }),
                        prev: state,
                        action: null,
                        payload: {},
                    },
                ]);
            };

            jObject.render = function (data, payload) {
                if (!data) {
                    return jObject.trigger("did.mount", [
                        { current: state, prev: state, action: null },
                    ]);
                }

                if (state.open) {
                    if (state.questions.length !== data.questions.length) {
                        $(".question-sequence").remove();
                        $(".tab-pane").remove();

                        $.each(state.questions, function (i) {
                            component.QuestionTabButton({
                                index: i,
                                id: `Q${i}`,
                                target: `#Tab${i}`,
                                title: `Question ${i + 1}`,
                                className: "",
                            });

                            component.tabContainer(
                                { index: i, id: `Tab${i}`, className: "" },
                                {
                                    question: component.inputQuestion({
                                        index: i,
                                    }),
                                    answers: component.inputAnswers({
                                        index: i,
                                    }),
                                }
                            );

                            $(".question-remove")
                                .eq(i)
                                .on("click", function (e) {
                                    e.preventDefault();
                                    jObject.state.update(
                                        ACTION.remove_question,
                                        { value: i }
                                    );
                                });

                            handleStateAnswers(state.answers[i], i);

                            $(".mcq-question")
                                .eq(i)
                                .on("blur change", function () {
                                    const $this = $(this);

                                    if ($this.val().length >= 1) {
                                        $this.removeClass("form-control-solid");
                                    } else {
                                        $this.addClass("form-control-solid");
                                    }
                                });
                        });

                        $(`a[href="#Tab${state.questions.length - 1}"]`).tab(
                            "show"
                        );
                    }
                }
            };

            return jObject;
        })($({}))
    );

    inputQuiz.on("change", function (event) {
        event.preventDefault();

        const $this = $(this);
        const value = parseInt($this.val());
        const checked = $this.is(":checked");

        if (checked) {
            // Manually update the quiz option to `yes` or `no`
            $this.attr("checked", checked);

            // Set the `open` state base on what's the quiz checkbox value
            // if user choose 'yes' then the question form will open or close otherwise
            mcq.state.update(checked ? ACTION.open_quiz : ACTION.close_quiz, {
                value: Boolean(value),
            });
        }
    });

    addQuestionBtn.on("click", function (e) {
        if (mcq.state.get("open")) {
            const questions = mcq.state.get("questions");
            const shuffles = mcq.state.get("shuffles");
            const answers = mcq.state.get("answers");

            answers.push(
                ["a", "b", "c", "d"].reduce(function (arr, option) {
                    arr.push({ key: option, value: "", mark_as_answer: false });
                    return arr;
                }, [])
            );

            mcq.state.update(ACTION.add_question, {
                value: {
                    answers: answers,
                    questions: questions.concat([""]),
                    shuffles: shuffles.concat([""]),
                },
            });

            mcq.state.update(ACTION.open_quiz, { value: true });
        }
    });

    mcq.on("did.mount", function (event, data) {
        const current = data.current;
        const prev = data.prev;
        const action = data.action;

        if (!current.open) {
            mcqContainer.hide("fast");
        }

        mcq.render(prev, action);
        inputQuiz.each(function (_i, elm) {
            const $this = $(elm);

            if (Array.isArray(currentMCQ)) {
                const value = currentMCQ.length >= 1 ? 1 : 0;
                if (parseInt($this.val()) === value) {
                    $this.attr("checked", value).trigger("change");
                }
            }
        });

        if (Array.isArray(currentMCQ) && currentMCQ.length) {
            const questions = [];
            const answers = [];
            const shuffles = [];

            currentMCQ.forEach(function (mcq, index) {
                questions.push(mcq.question);
                answers.push(mcq.options);
                shuffles.push(mcq.settings.shuffle);

                $("form#preOrientationForm").append(
                    `<input name="mcq[${index}][quiz_id]" value="${mcq.id}" hidden >`
                );
            });
            mcq.state.update(ACTION.add_question, {
                value: {
                    answers: answers,
                    questions: questions,
                    shuffles: shuffles,
                },
            });
        }
    }).on("did.update", function (e, data) {
        const current = data.current;
        const prev = data.prev;
        const action = data.action;

        // Determine if the `open` value has change
        if (current.open !== prev.open) {
            // If it does and the value it's `true`, meaning
            // we need to display the MCQ inputs otherwise, close
            // the MCQ container element.
            if (current.open) {
                mcqContainer.show("slow", function () {
                    if (Array.isArray(currentMCQ) && currentMCQ.length === 0) {
                        addQuestionBtn.trigger("click");
                    }

                    mcq.render(prev, action);
                });
            } else {
                mcqContainer.hide("slow", function () {
                    mcq.unmount();
                });
            }
        } else {
            mcq.render(prev, action);
        }
    });
    mcq.render();
});

// $(function (jq) {
//     setTimeout(function () {
//         jq("[data-fslightbox]").each(function () {
//             console.log(this);
//             jq(this).on("click", function (e) {
//                 e.preventDefault();
//                 var $this = jq(this);
//                 var type = $this.data("fslightbox-type");
//                 var source = $this.data("fslightbox");
//                 var caption = $this.data("fslightbox-caption");
//                 var thumbs = $this.data("fslightbox-thumbs") || "";

//                 $.fancybox.open({
//                     src: source,
//                     type: type,
//                     caption: caption,
//                     thumbs: {
//                         autoStart: thumbs,
//                     },
//                 });
//             });
//         });
//     }, 500);
// });

// Handle input effective date using datetimepicker
$(function (jq) {
    const input = jq("#effectiveDate");
    const options = {
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 2022,
        maxYear: parseInt(moment().format("YYYY"), 12),
        minDate: new Date(),
        setDate: input.attr("data-old"),
        locale: {
            format: "yyyy-MM-DD",
        },
        cancelButtonText: "Clear",
    };

    input.on("cancel.daterangepicker", function (ev, picker) {
        $(this).val("");
    });

    function callback(start, end, label) {
        // console.log(start, end, label)
        // var years = moment().diff(start, "years");
        // alert("You are " + years + " years old!");
    }

    input.daterangepicker(options, callback).val(input.attr("data-old"));
    jq(".cancelBtn").text("Clear");
});

// handle input of select2 element
$(function (jq) {
    const config = {
        theme: "bootstrap5",
        allowClear: true,
        allowSearch: true,
        placeholder: "-- No option selected --",
        tags: false,
    };
    const select2 = jq(".pre-orientation-select2");

    function selectAll(select2) {
        const value = select2.val();

        if (Array.isArray(value) && value.includes("all")) {
            const options = $(".all-support")
                .find("option")
                .filter(function (_i, $element) {
                    return jq($element).attr("value") !== "all";
                })
                .map(function (_i, $element) {
                    return $($element).val();
                })
                .get();

            // select2.val(options).trigger("change.select2");

            // set value to options without triggering change event
            select2.val(options);
        }
    }

    // Set default value of select2
    var defaultToAll = [
        "nationality",
        "intake",
        "programme",
        "school",
        "faculty",
        "mobility",
    ];

    // Using 'select' library to handle input of select2 element
    select2.each(function (_, elm) {
        const $elm = jq(elm);

        $elm.on("change", function () {
            const $this = $(this);

            // if $this.val() contains other than 'all', remove 'all' from $this.val()
            if (
                Array.isArray($this.val()) &&
                $this.val().length > 1 &&
                $this.val().includes("all")
            ) {
                let newVal = $this.val().filter(function (val) {
                    return val !== "all";
                });
                $this.val(newVal).trigger("change.select2");
            }

            // and if all selected again, select all and forget about the rest
            if (
                $this.val().length === 0 ||
                $this.val().length === $this.find("option").length - 1
            ) {
                $this.val("all").trigger("change.select2");
            }

            // selectAll($this);
        });

        // Handle initial value of select2 element
        if (defaultToAll.includes($elm.attr("id"))) {
            $elm.val("all").select2(config);
        } else {
            $elm.select2(config);
        }

        // Handle previous value of select2 element if previous submission failed
        let value = $elm.attr("data-old");

        if ($elm.attr("multiple") && value) {
            value = value.split(",");
            if (
                value.length === 0 ||
                value.length === $elm.find("option").length - 1
            ) {
                value = ["all"];
            }

            $elm.val(value).trigger("change");
        } else if (value) {
            $elm.val(value).trigger("change");
        }
    });
});

// Pre sent form data to server
$(function (jq) {
    const form = jq("#preOrientationForm");

    form.on("submit", function (e) {
        // get the select2 element
        const select2 = jq(".pre-orientation-select2");

        // foreach select2 element, get the value, if value is 'all', set it to all value of the option except 'all'
        select2.each(function (_, elm) {
            const $elm = jq(elm);
            const value = $elm.val();

            if (Array.isArray(value) && value.includes("all")) {
                const options = $(".all-support")
                    .find("option")
                    .filter(function (_i, $element) {
                        return jq($element).attr("value") !== "all";
                    })
                    .map(function (_i, $element) {
                        return $($element).val();
                    })
                    .get();

                $elm.val(options);
            }
        });
    });
});

// Handle thumbnail selector
$(function (jq) {
    let input = jq("input[name='thumbnail']");
    let hex_input = jq("input[name='thumbnail_hex']");
    let placeholder = jq(".image-placeholder");
    let file = null;
    let reader = null;
    let cropper = null;

    input.on("change", function (e) {
        const $this = jq(this);
        file = $this[0].files[0];
        reader = new FileReader();

        // open modal with id 'cropper-modal', then initialize cropper.js
        jq("#cropper-modal").modal("show");
    });
    jq("#cropper-modal").on("show.bs.modal", function () {
        let image = document.getElementById("cropper-image");
        image.src = URL.createObjectURL(file);
        cropper = new Cropper(image, {
            aspectRatio: 6 / 4,
            viewMode: 0,
            strict: true,

            // minCanvasWidth is 100% of it's parent element
            minCanvasWidth: 100,
        });

        jq("#cropper-save").on("click", function () {
            // let canvas = cropper.getCroppedCanvas({
            //     width: 400,
            //     height: 225,
            // });
            // do same, but with aspect ratio 6/4
            let canvas = cropper.getCroppedCanvas({
                width: 600,
                height: 400,
            });

            canvas.toBlob(function (blob) {
                let url = URL.createObjectURL(blob);
                let reader = new FileReader();

                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    let base64data = reader.result;
                    let hex = base64data.split(",")[1];

                    // input.val(url);
                    hex_input.val(hex);
                    placeholder.attr("src", url);

                    // set placeholder background image
                    placeholder.css("background-image", `url(${url})`);

                    jq("#cropper-modal").modal("hide");
                };
            });
        });
    });

    jq("#cropper-modal").on("hide.bs.modal", function () {
        jq("#cropper-submit").off("click");

        cropper.destroy();

        let image = document.getElementById("cropper-image");
        image.src = "";
    });
});

// Handle input `content`. Pre-orientation using `tinymce` editor library
$(function (jq) {
    return tinymce.init({
        selector: "#contentEditor",
        height: "350",
        plugins: "image lists link",
        // menubar: false,
        toolbar:
            "fontselect | bold italic strikethrough underline | bullist numlist | alignleft aligncenter alignright | image link ",
        menubar: "file edit view insert format", // hanya menampilkan menu yang diinginkan
        menu: {
            format: {
                title: "Format",
                items: "bold italic underline strikethrough superscript subscript code | formats blockformats fontsizes align lineheight forecolor | bullist numlist | removeformat",
            },
        },
        link_title: false,
        link_quicklink: false,
        file_picker_types: "image",
        file_picker_callback: function (callback, value, meta) {
            var input = document.createElement("input");
            input.setAttribute("type", "file");
            input.setAttribute("accept", "image/*");

            /*
	Note: In modern browsers input[type="file"] is functional without
	even adding it to the DOM, but that might not be the case in some older
	or quirky browsers like IE, so you might want to add it to the DOM
	just in case, and visually hide it. And do not forget do remove it
	once you do not need it anymore.
*/

            input.onchange = function () {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {
                    /*
	Note: Now we need to register the blob in TinyMCEs image blob
	registry. In the next release this part hopefully won't be
	necessary, as we are looking to handle it internally.
*/
                    var id = "blobid" + new Date().getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(",")[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    /* call the callback and populate the Title field with the file name */
                    callback(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
            };

            input.click();
        },
    });
});

function handleClickViewDetailPreOrientation(className) {
    const milestoneTable = $("#studentMilestoneTable");
    const renderViewDetailPreOrientation = function (props) {
        const entity = props.entity;
        const viewDetailComponent = $(`#${entity.uuid}`);
        const hasQuiz = viewDetailComponent.attr("data-has-quiz");
        var quizComponent;
        var questions = [];
        try {
            quizComponent = $(`#quiz_${entity.uuid}`);
            // const studentQuiz = JSON.parse(quizComponent.attr('data-quiz-student'));
            questions = JSON.parse(quizComponent.attr("data-questions"));
        } catch (error) {
            quizComponent = $(document.createElement("div")).attr(
                "data-questions-count",
                "0"
            );
        }

        return Swal.fire({
            title: $(document.createElement("h3"))
                .addClass("text-light bg-dark fs-2 fw-bold py-2")
                .css(
                    "font-family",
                    "Century Gothic,CenturyGothic,AppleGothic,sans-serif"
                )
                .text(entity.activity_name),
            html: viewDetailComponent.html(),
            width: window.innerWidth < 768 ? null : "80%",
            // showConfirmButton: Boolean(
            //     parseInt(quizComponent.attr("data-questions-count"))
            // ),
            // showConfirmButton: hasQuiz === "true" ? true : false,
            showConfirmButton: false,
            confirmButtonText: "Next",
            confirmButtonColor: "#ff0000",
            customClass: {
                htmlContainer: "card card-shadow-md",
                confirmButton: `btn btn-sm btn-danger hover-scale`,
            },
            didOpen: function () {
                let milestone = JSON.parse(
                    localStorage.getItem("currentMilestone")
                ).milestone;

                $("button#btn_next_" + entity.uuid).on("click", function () {
                    $("div#content_container_" + entity.uuid).addClass(
                        "col-md-6"
                    );
                    $("div#quiz_container_" + entity.uuid).removeClass(
                        "d-none"
                    );

                    $("button#btn_next_" + entity.uuid).addClass("d-none");

                    const getDetailCard = () => {
                        const el = $("div#content_container_" + entity.uuid);
                        let height = 0;
                        let newEl = $();
                        for (let i = 0; i < el.length; i++) {
                            const element = el[i];
                            if ($(element).outerHeight() !== 0) {
                                height = $(element).outerHeight();
                                newEl = $(element);
                                break;
                            }
                        }
                        return {
                            height: height,
                            element: newEl,
                        };
                    };
                    const detailCard = getDetailCard();
                    const getQuizCard = () => {
                        const el = $("div#quiz_container_" + entity.uuid);
                        let height = 0;
                        let newEl = $();
                        for (let i = 0; i < el.length; i++) {
                            const element = el[i];
                            if ($(element).outerHeight() !== 0) {
                                height = $(element).outerHeight();
                                newEl = $(element);
                                break;
                            }
                        }
                        return {
                            height: height,
                            element: newEl,
                        };
                    };
                    const quizCard = getQuizCard();
                    if (quizCard.height > detailCard.height) {
                        quizCard.element.css({
                            "overflow-y": "scroll",
                            "max-height": `${detailCard.height}px`,
                        });
                    }
                });

                refreshFsLightbox();
                YouTube();

                localStorage.setItem("milestone", milestone);

                viewDetailComponent.css("display", "auto");
                handleClickMarkAsComplete(
                    "button#markComplete",
                    milestoneTable
                );
            },
            didDestroy: function () {
                viewDetailComponent.css("display", "none");
            },
            didRender: function () {
                const mcq = questions.map(function (item) {
                    return { quiz_id: item.id, answers: [""] };
                });

                $("input.quiz-answer").each(function (i, el) {
                    YouTube();
                    const $this = $(el);
                    const keys = $this.attr("name").split(".");
                    const key = keys.at(0);

                    $this.on("change", function (e) {
                        const value = $this.val();
                        const checked = $this.is(":checked");
                        const quizValue = mcq.find(function (item) {
                            return item.quiz_id === parseInt(key);
                        });

                        if (checked) {
                            quizValue.answers[0] = value;
                            mcq[mcq.indexOf(quizValue)] = quizValue;
                        }
                    });
                });

                // Change color of checkbox on student page dynamically
                $(function (jq) {
                    // correct answer handler
                    const correctAnswer = jq(".correct:checked[type=checkbox]");
                    correctAnswer.each(function (i, elm) {
                        const $this = jq(elm);

                        // change background color of the checkbox
                        $this.css("background-color", "#15b960");
                    });

                    // my answer handler
                    const myAnswer = jq(".myAnswer:checked[type=checkbox]");
                    myAnswer.each(function (i, elm) {
                        // check if this element has class correct
                        const $this = jq(elm);
                        const isCorrect = $this.hasClass("correct");

                        // change background color of the checkbox
                        $this.css(
                            "background-color",
                            isCorrect ? "#15b960" : "#FF0000"
                        );
                        $this.css(
                            "background-image",
                            isCorrect
                                ? `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-check' viewBox='0 0 16 16'%3E%3Cpath fill='%23FFFFFF' d='M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z'/%3E%3C/svg%3E")`
                                : `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-x' viewBox='0 0 16 16'%3E%3Cpath fill='%23FFFFFF' d='M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z'/%3E%3C/svg%3E")`
                        );
                        $this.css("background-repeat", "no-repeat");
                        $this.css("background-position", "center");
                        $this.css("background-size", "contain");
                        $this.css(
                            "border-color",
                            isCorrect ? "#5F7070" : "#8F6E91"
                        );
                    });
                });

                $("button#cancelQuiz").on("click", function (e) {
                    e.preventDefault();
                    Swal.close();

                    $("div#content_container_" + entity.uuid)
                        .addClass("col-12")
                        .removeClass("col-6");
                    $("div#quiz_container_" + entity.uuid).addClass("d-none");
                    $("div#content_container_" + entity.uuid)
                        .removeClass("col-md-6")
                        .addClass("col-12");

                    $("button#btn_next_" + entity.uuid).removeClass("d-none");

                    renderViewDetailPreOrientation(props);
                });

                $("button#submitQuiz").on("click", function (e) {
                    e.preventDefault();
                    const $this = $(this);
                    const quizElem = $($("div#quiz_" + entity.uuid + "")[0]);
                    const quizAnswersedCount = Array.from(mcq).filter(function (
                        quiz
                    ) {
                        return quiz.answers[0].length >= 1;
                    }).length;

                    if (quizAnswersedCount !== mcq.length) {
                        const toBe =
                            mcq.length - quizAnswersedCount === 1
                                ? "is still <b>1</b>"
                                : `are still <b>${
                                      mcq.length - quizAnswersedCount
                                  }</b>`;
                        toastr.warning(
                            `there ${toBe} quiz questions that you haven\'t answered. Please answer the quiz before proceeding.`,
                            "Quiz Alert",
                            { toastClass: "toast-milestone" }
                        );
                        return;
                    }

                    $this.attr("disabled", "disabled");

                    $.ajax({
                        method: "POST",
                        url: props.urlSubmitQuiz,
                        data: {
                            _token: props.token,
                            preorientation: entity.id,
                            mcq: mcq,
                        },
                        success: function (res) {
                            const quizComponent = $(
                                $(
                                    "div#quiz_container_" +
                                        entity.uuid +
                                        " quiz-component"
                                )[0]
                            );

                            $.ajax({
                                method: "POST",
                                url: quizComponent.attr("data-render-url"),
                                data: {
                                    _token: props.token,
                                    key: quizComponent.attr("key"),
                                    title: quizComponent.attr("title"),
                                    milestone: quizComponent.attr("milestone"),
                                    target: quizComponent.attr("target"),
                                    shuffled: quizElem.attr("data-questions"),
                                },
                                success: function (res) {
                                    let quizHtml = res.quiz;
                                    let milestoneHtml = res.checkpoint;
                                    let progressHtml = res.progress;
                                    $(
                                        $(
                                            "div#quiz_container_" +
                                                entity.uuid +
                                                " .quiz-content"
                                        )[1]
                                    ).html(quizHtml);
                                    $("#milestone-selector").html(
                                        milestoneHtml
                                    );
                                    $("#progress-view").html(progressHtml);
                                    window.reloadDataTable();

                                    $(function (jq) {
                                        // correct answer handler
                                        const correctAnswer = jq(
                                            ".correct:checked[type=checkbox]"
                                        );
                                        correctAnswer.each(function (i, elm) {
                                            const $this = jq(elm);

                                            // change background color of the checkbox
                                            $this.css(
                                                "background-color",
                                                "#15b960"
                                            );
                                        });

                                        // my answer handler
                                        const myAnswer = jq(
                                            ".myAnswer:checked[type=checkbox]"
                                        );
                                        myAnswer.each(function (i, elm) {
                                            // check if this element has class correct
                                            const $this = jq(elm);
                                            const isCorrect =
                                                $this.hasClass("correct");

                                            // change background color of the checkbox
                                            $this.css(
                                                "background-color",
                                                isCorrect
                                                    ? "#15b960"
                                                    : "#FF0000"
                                            );
                                            $this.css(
                                                "background-image",
                                                isCorrect
                                                    ? `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-check' viewBox='0 0 16 16'%3E%3Cpath fill='%23FFFFFF' d='M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z'/%3E%3C/svg%3E")`
                                                    : `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-x' viewBox='0 0 16 16'%3E%3Cpath fill='%23FFFFFF' d='M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z'/%3E%3C/svg%3E")`
                                            );
                                            $this.css(
                                                "background-repeat",
                                                "no-repeat"
                                            );
                                            $this.css(
                                                "background-position",
                                                "center"
                                            );
                                            $this.css(
                                                "background-size",
                                                "contain"
                                            );
                                            $this.css(
                                                "border-color",
                                                isCorrect
                                                    ? "#5F7070"
                                                    : "#8F6E91"
                                            );
                                        });
                                    });
                                },
                            });
                            // const data = res.data;
                            // if (data) {
                            //     Swal.fire({
                            //         title: "Congratulation!",
                            //         html: `<div class="mb-4 text-gray-600 fw-semibold fs-6 ps-10"><strong>${data.message} You'r score for this quiz is <bold>${data.score}</bold></strong></div>`,
                            //         icon: "success",
                            //     }).then(function (result) {
                            //         if (result.isConfirmed) {
                            //             // window.reloadDataTable();

                            //         }
                            //     });
                            // }
                        },
                    });

                    // Swal.fire({
                    // 	title: "Are you sure wants to submit your answers?",
                    // 	html: `<div class="mb-4 text-gray-600 fw-semibold fs-6 ps-10">This action cannot be undone.</div>`,
                    // 	icon: "warning",
                    // 	showConfirmButton: true,
                    // 	showCancelButton: true,
                    // }).then(function (result) {
                    // 	if (result.isConfirmed) {

                    // 	}
                    // })
                });
            },
        }).then(function (result) {
            if (result.isConfirmed) {
                Swal.fire({
                    html: quizComponent.html(),
                    showConfirmButton: false,
                    width: window.innerWidth < 768 ? null : "80%",
                    customClass: {
                        htmlContainer: "card card-shadow-md",
                    },
                    didOpen: function () {
                        // stepperx.init(
                        //     "#swal2-html-container #quiz_stepper",
                        //     "#swal2-html-container #quiz_form"
                        // );
                    },
                    didRender: function () {
                        const mcq = questions.map(function (item) {
                            return { quiz_id: item.id, answers: [""] };
                        });

                        $("input.quiz-answer").each(function (i, el) {
                            const $this = $(el);
                            const keys = $this.attr("name").split(".");
                            const key = keys.at(0);

                            $this.on("change", function (e) {
                                const value = $this.val();
                                const checked = $this.is(":checked");
                                const quizValue = mcq.find(function (item) {
                                    return item.quiz_id === parseInt(key);
                                });

                                if (checked) {
                                    quizValue.answers[0] = value;
                                    mcq[mcq.indexOf(quizValue)] = quizValue;
                                }
                            });
                        });

                        // Change color of checkbox on student page dynamically
                        $(function (jq) {
                            // correct answer handler
                            const correctAnswer = jq(
                                ".correct:checked[type=checkbox]"
                            );
                            correctAnswer.each(function (i, elm) {
                                const $this = jq(elm);

                                // change background color of the checkbox
                                $this.css("background-color", "#9ED8DB");
                            });

                            // my answer handler
                            const myAnswer = jq(
                                ".myAnswer:checked[type=checkbox]"
                            );
                            myAnswer.each(function (i, elm) {
                                // check if this element has class correct
                                const $this = jq(elm);
                                const isCorrect = $this.hasClass("correct");

                                // change background color of the checkbox
                                $this.css(
                                    "background-color",
                                    isCorrect ? "#9ED8DB" : "#FF0000"
                                );
                                $this.css(
                                    "background-image",
                                    isCorrect
                                        ? `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-check' viewBox='0 0 16 16'%3E%3Cpath fill='%23FFFFFF' d='M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z'/%3E%3C/svg%3E")`
                                        : `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-x' viewBox='0 0 16 16'%3E%3Cpath fill='%23FFFFFF' d='M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z'/%3E%3C/svg%3E")`
                                );
                                $this.css("background-repeat", "no-repeat");
                                $this.css("background-position", "center");
                                $this.css("background-size", "contain");
                                $this.css(
                                    "border-color",
                                    isCorrect ? "#5F7070" : "#8F6E91"
                                );
                            });
                        });

                        $("button#cancelQuiz").on("click", function (e) {
                            e.preventDefault();
                            Swal.close();

                            $("div#content_container_" + entity.uuid)
                                .addClass("col-12")
                                .removeClass("col-6");
                            $("div#quiz_container_" + entity.uuid).addClass(
                                "d-none"
                            );

                            $("button#btn_next_" + entity.uuid).removeClass(
                                "d-none"
                            );

                            renderViewDetailPreOrientation(props);
                        });

                        $("button#submitQuiz").on("click", function (e) {
                            e.preventDefault();
                            const $this = $(this);
                            $this.attr("disabled", "disabled");

                            $.ajax({
                                method: "POST",
                                url: props.urlSubmitQuiz,
                                data: {
                                    _token: props.token,
                                    preorientation: entity.id,
                                    mcq: mcq,
                                },
                                success: function (res) {
                                    const data = res.data;
                                    if (data) {
                                        Swal.fire({
                                            title: "Congratulation!",
                                            html: `<div class="mb-4 text-gray-600 fw-semibold fs-6 ps-10"><strong>${data.message} You'r score for this quiz is <bold>${data.score}</bold></strong></div>`,
                                            icon: "success",
                                        }).then(function (result) {
                                            if (result.isConfirmed) {
                                                window.reloadDataTable();
                                            }
                                        });
                                    }
                                },
                            });

                            // Swal.fire({
                            // 	title: "Are you sure wants to submit your answers?",
                            // 	html: `<div class="mb-4 text-gray-600 fw-semibold fs-6 ps-10">This action cannot be undone.</div>`,
                            // 	icon: "warning",
                            // 	showConfirmButton: true,
                            // 	showCancelButton: true,
                            // }).then(function (result) {
                            // 	if (result.isConfirmed) {

                            // 	}
                            // })
                        });
                    },
                });
            }
        });
    };

    $(`.${className}`).each(function (_i, el) {
        const btn = $(el);

        btn.on("click", function (e) {
            e.preventDefault();
            const $this = $(this);
            const input = $this.find("input#preOrientation");

            if (false
                // $this.data("milestone") >
                //     JSON.parse(localStorage.getItem("currentMilestone"))
                //         .milestone &&
                // JSON.parse(localStorage.getItem("currentMilestone"))
                //     .completed == false
            ) {
                toastr.info(
                    "Please ensure that you complete all activities in the previous checkpoint before proceeding",
                    undefined,
                    {
                        toastClass: "toast-milestone",
                    }
                );
            } else {
                renderViewDetailPreOrientation({
                    entity: JSON.parse(input.val()),
                    token: input.attr("data-csrf"),
                    urlSubmitQuiz: input.attr("data-url-submit-quiz"),
                });
            }
        });
    });
}

function handleClickMarkAsComplete(selector, table) {
    return $(selector).each(function (_i, element) {
        $(element).on("click", function () {
            const $this = $(this);

            // Close swal
            Swal.close();

            $.ajax({
                method: "POST",
                url: $this.attr("data-route"),
                data: {
                    _token: $this.attr("data-csrf"),
                    pre_orientation_id: $this.val(),
                },
                success: function (res) {
                    const data = res.data;

                    $.ajax({
                        method: "POST",
                        url: $this.attr("data-render-url"),
                        data: {
                            _token: $this.attr("data-csrf"),
                            state: "Mark as complete",
                            key: "{}",
                            title: "{}",
                            milestone: "{}",
                            target: "{}",
                            shuffled: "{}",
                        },
                        success: function (res) {
                            let milestoneHtml = res.checkpoint;
                            let progressHtml = res.progress;
                            $("#milestone-selector").html(milestoneHtml);
                            $("#progress-view").html(progressHtml);
                        },
                    });

                    Swal.fire({
                        title: data.title,
                        html: `<div class="mb-4 text-gray-600 fw-semibold fs-6 ps-10">${data.text}</div>`,
                        showConfirmButton: true,
                        icon: "success",
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            reloadDataTable();
                        }
                    });
                },
                error: function (res) {
                    if (res.status === 400) {
                        const data = res.responseJSON.data;

                        Swal.fire({
                            title: data.title,
                            html: `<div class="mb-4 text-gray-600 fw-semibold fs-6 ps-10">${data.text}</div>`,
                            showConfirmButton: true,
                            icon: "warning",
                        });
                    }
                },
            });
        });
    });
}

/**
 * This scripts will handle client/server side process that
 * related with resources module
 *
 * @author Tris <tris@grtech.com.my>
 */
$(function (jq) {
    "use strict";
    // Datatable instance
    // const preOrientationDt = new $.fn.dataTable.Api('#preOrientationDt');

    const action = {
        submit: function (selector = "preOrientationForm") {
            // Handle submit pre-orientation form
            jq("button[type=submit]").on("click", function (e) {
                jq(`form#${selector}`).trigger("submit");
            });
        },
        // handle notification message after CRUD event happend
        notify: function (message) {
            if (message) {
                Swal.fire({
                    icon: "success",
                    title: message,
                    showConfirmButton: false,
                    timer: 3000,
                });
                return;
            }

            const $element = jq(".has-notif");
            if ($element.length) {
                const message = $element.attr("data-message");

                if (typeof message === "string" && message.length) {
                    Swal.fire({
                        icon: "success",
                        title: message,
                        showConfirmButton: false,
                        timer: 3000,
                    });
                }
            }
        },
        watchDelete: function (callback) {
            const self = this;
            $("a[data-method=delete]").each(function (_i, el) {
                const $this = $(el);

                $this.on("click", function (e) {
                    e.preventDefault();
                    const url = $this.attr("href");
                    const token = $this.attr("data-token");

                    Swal.fire({
                        title: "Are you sure want to delete this item? ",
                        text: "This process cannot be undone!",
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Yes",
                        showCancelButton: true,
                        customClass: {
                            confirmButton: "btn btn-danger",
                            cancelButton: "btn btn-secondary",
                        },
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            jq.ajax({
                                method: "POST",
                                url: url,
                                data: {
                                    _token: token,
                                    _method: "DELETE",
                                },
                                success: function (res) {
                                    self.notify(res.message);
                                    table.DataTable().draw();
                                },
                                error: function (req, res) {
                                    // console.log(req, res);
                                },
                            });
                        }
                    });
                });
            });
        },
        whenStatusClick: function (status) {
            $(`a[href="#${status}"]`).each(function (_e, el) {
                const $this = $(el);
                const route = $this.attr("data-route");
                const value = $this.attr("data-target");
                const token = $this.attr("data-token");

                $this.on("click", function (e) {
                    e.preventDefault();
                    Swal.fire({
                        title: `Are you sure want to ${status} this item?`,
                        // text: 'You still able to make un',
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Yes",
                        showCancelButton: true,
                        customClass: {
                            confirmButton: "btn btn-danger",
                            cancelButton: "btn btn-secondary",
                        },
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            jq.ajax({
                                method: "POST",
                                url: route,
                                data: { _token: token, key: value },
                                success: function (res) {
                                    Swal.fire({
                                        icon: "success",
                                        title: res.data.message,
                                        showConfirmButton: false,
                                        timer: 3000,
                                    });
                                    table.DataTable().draw();
                                },
                                error: function (res) {
                                    if (res.status === 422) {
                                        Swal.fire({
                                            title: "Oops!, something went wrong.",
                                            text: `Cannot ${status} the pre-orientation activity`,
                                            icon: "danger",
                                            buttonsStyling: false,
                                            confirmButtonText: "Close",
                                            showCancelButton: false,
                                            customClass: {
                                                confirmButton: "btn btn-danger",
                                            },
                                        });
                                    }
                                },
                            });
                        }
                    });
                });
            });
        },

        whenBulkActionClick: function (status, humanReadableStatus) {
            $(`a[href="#${status}"]`).each(function (_e, el) {
                const $this = $(el);
                const route = $this.attr("data-route");
                const value = localStorage.getItem("preOrientationBulkID");
                const token = $this.attr("data-token");

                $this.on("click", function (e) {
                    e.preventDefault();
                    Swal.fire({
                        title: `Are you sure want to ${humanReadableStatus} all items you checked?`,
                        // text: 'You still able to make un',
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Yes",
                        showCancelButton: true,
                        customClass: {
                            confirmButton: "btn btn-danger",
                            cancelButton: "btn btn-secondary",
                        },
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            jq.ajax({
                                method: "POST",
                                url: route,
                                data: { _token: token, key: value },
                                success: function (res) {
                                    Swal.fire({
                                        icon: "success",
                                        title: res.data.message,
                                        showConfirmButton: false,
                                        timer: 3000,
                                    });
                                    localStorage.removeItem(
                                        "preOrientationBulkID"
                                    );
                                    table.DataTable().draw();
                                },
                                error: function (res) {
                                    if (res.status === 422) {
                                        Swal.fire({
                                            title: "Oops!, something went wrong.",
                                            text: `Cannot ${humanReadableStatus} the pre-orientation activity`,
                                            icon: "danger",
                                            buttonsStyling: false,
                                            confirmButtonText: "Close",
                                            showCancelButton: false,
                                            customClass: {
                                                confirmButton: "btn btn-danger",
                                            },
                                        });
                                    }
                                },
                            });
                        }
                    });
                });
            });
        },
    };

    action.submit();
    action.notify();

    $('a[href="#viewQuiz"]').on("click", function (e) {
        e.preventDefault();
        let hasQuiz = $(`#quiz_stepper_container`).attr("data-has-quiz");
        const html =
            hasQuiz == "true"
                ? $(`#quiz_stepper_container`).html()
                : `<div class="text-center">No quiz available</div>`;
        Swal.fire({
            title: $(`#quiz_stepper_container`).attr("data-title"),
            html: html,
            showConfirmButton: false,
            showCancelButton: hasQuiz == "true" ? false : true,
            cancelButtonText: "Close",
            customClass: {
                popup: "w-300px w-md-500px",
            },
            didOpen: function () {
                stepperx.init(
                    "#swal2-html-container #quiz_stepper",
                    "#swal2-html-container #quiz_form"
                );
            },
        });
    });

    /**
     * Get the pre-orientation datatable element
     *
     * @var {JQuery<HTMLElement>}
     */
    const table = jq("#preorientationTable");
    const milestoneTable = $("#studentMilestoneTable");

    function swalFire(icon, title, message) {
        return Swal.fire({
            title: title,
            text: message,
            icon: icon,
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
                confirmButton: "btn btn-danger",
            },
        });
    }

    function onDataTableReload(_json) {
        const btnDetails = jq(".btn-detail");
        const filterCheckpoints = jq("select[name=filterCheckpoint]").val();

        localStorage.removeItem("milestone");

        if (Array.isArray(filterCheckpoints) && filterCheckpoints.length) {
            localStorage.setItem("milestone", filterCheckpoints);
        }

        btnDetails.each(function (i, elm) {
            const btn = $(elm);
            const status = btn.attr("data-completed");
            const prevBtn = function (currentIndex) {
                if (currentIndex === 0) {
                    return btn;
                }

                return btnDetails.eq(currentIndex - 1);
            };

            btn.on("click", function (e) {
                // e.preventDefault();
                const $this = jq(this);

                if (status === "progress") {
                    const prevStatus = prevBtn(i).attr("data-completed");
                    if (prevStatus === status) {
                        swalFire(
                            "warning",
                            "Wait",
                            "Please complete the previous activities."
                        );
                    } else {
                        renderQuiz($this);
                    }
                } else {
                    renderQuiz($this);
                }
            });
        });
    }

    /* handle button checkpoints behaviour */
    function handleCheckpoints() {
        const CHECKPOINT_BTN_COLOR = {
            completed: "#9ED8DB",
            incomplete: "#D9D9D9",
        };

        jq(".btn-checkpoint").each(function (i, elm) {
            const $this = $(elm);
            const anchor = $this.find("a");
            const milestone = anchor.attr("data-checkpoint");
            const studentMilestone = JSON.parse(
                anchor.attr("data-student-milestone")
            );
            const studentMilestoneCurrent = JSON.parse(
                anchor.attr("data-student-milestone-current")
            );
            let btnColor = CHECKPOINT_BTN_COLOR.incomplete;

            if (
                Object.keys(studentMilestone).length &&
                studentMilestone.completed === true
            ) {
                btnColor = CHECKPOINT_BTN_COLOR.completed;
            }
            anchor.css({ "background-color": btnColor });

            localStorage.setItem(
                "currentMilestone",
                JSON.stringify(studentMilestoneCurrent)
            );

            anchor.on("click", function (e) {
                e.preventDefault();
                jq("select[name=filterCheckpoint]").val(null).trigger("change");
                // Store checkpoint value into local storage so,
                // we can use it as request data datatable
                localStorage.setItem("milestone", milestone);
                localStorage.setItem("fMilestone", milestone);

                // Send request to pre-orientation datatable endpoint
                // To update the table list base on the pre-orientation checkpoint
                milestoneTable.DataTable().draw(onDataTableReload);
            });
        });
    }

    // Handle select all checkbox
    function handleSelectAllCheckbox() {
        const selectAllCheckboxSelector = 'input[name="selectAll"]';

        jq(document).on("change", selectAllCheckboxSelector, function () {
            // detect this checkbox is checked or not
            const checkboxes = jq('input[name="bulkSelect"]');
            const isChecked = jq(this).is(":checked");

            // check or uncheck all checkboxes
            checkboxes.prop("checked", isChecked);

            // get all checked checkboxes and set to localStorage with key 'preOrientationBulkID'
            const checked_ids = checkboxes.filter(":checked").map(function () {
                return jq(this).attr("data-id");
            });

            const unchecked_ids = checkboxes
                .filter(":not(:checked)")
                .map(function () {
                    return jq(this).attr("data-id");
                });

            const bulkID = localStorage.getItem("preOrientationBulkID") || "";
            const bulkIDArray = bulkID.split(",");

            const newbulkID = bulkIDArray
                .filter(function (id) {
                    return !unchecked_ids.toArray().includes(id);
                })
                .concat(checked_ids.toArray())
                .filter(function (id, index, self) {
                    return id !== "" && self.indexOf(id) === index;
                });

            localStorage.setItem("preOrientationBulkID", newbulkID.join(","));

            action.whenBulkActionClick("bulkPublish", "publish");
            action.whenBulkActionClick("bulkUnpublish", "unpublish");
        });
    }

    // Handle checkbox checked for bulk action
    function handleBulkCheckbox() {
        const bulkCheckboxSelector = 'input[name="bulkSelect"]';
        const elem = jq(document);

        // for every checkbox checked, get the data-id from that element and set to localStorage with key 'bulkID'. the value is id separated by comma
        elem.on("change", bulkCheckboxSelector, function () {
            const checkboxes = jq(bulkCheckboxSelector);
            const checked = checkboxes.filter(":checked");
            const unchecked = checkboxes.filter(":not(:checked)");

            const checked_ids = checked
                .map(function () {
                    return jq(this).attr("data-id");
                })
                .get();
            const unchecked_ids = unchecked
                .map(function () {
                    return jq(this).attr("data-id");
                })
                .get();

            const bulkID = localStorage.getItem("preOrientationBulkID");
            const bulkIDArray = bulkID ? bulkID.split(",") : [];

            const checked_id = checked_ids.filter(function (id) {
                return !bulkIDArray.includes(id);
            });

            const unchecked_id = unchecked_ids.filter(function (id) {
                return bulkIDArray.includes(id);
            });

            const newbulkID = bulkIDArray
                .filter(function (id) {
                    return !unchecked_id.includes(id);
                })
                .concat(checked_id);

            localStorage.setItem("preOrientationBulkID", newbulkID.join(","));

            action.whenBulkActionClick("bulkPublish", "publish");
            action.whenBulkActionClick("bulkUnpublish", "unpublish");
        });
    }

    function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this,
                args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    function handleQuickSearch() {
        const searchInput = jq('input[name="search"]');
        const elem = jq(document);

        elem.on(
            "keyup",
            searchInput,
            debounce(function (e) {
                // if user press enter key
                // if (e.keyCode === 13) {
                // 	let tbl;

                // 	if (table.length > 0) {
                // 			tbl = table;
                // 	} else if (milestoneTable.length > 0) {
                // 			tbl = milestoneTable;
                // 	} else {
                // 			console.log('Tidak ada elemen yang ditemukan');
                // 	}
                // 	tbl.DataTable().search(searchInput.val()).draw(onDataTableReload);
                // }
                let tbl;

                if (table.length > 0) {
                    tbl = table;
                } else if (milestoneTable.length > 0) {
                    tbl = milestoneTable;
                } else {
                    // console.log("Tidak ada elemen yang ditemukan");
                }

                tbl.DataTable()
                    .search(searchInput.val())
                    .draw(onDataTableReload);
                // jq.debounce(2000, tbl.DataTable().search(searchInput.val()).draw(onDataTableReload));
            }, 500)
        );
    }

    function handleFilters() {
        const filters = {
            checkpoint: null,
            nationality: null,
            status: null,
        };

        jq("select[name=filterWeeks]").on("change", function (e) {
            e.preventDefault();
            filters.checkpoint = $(this).val();
        });
        jq("select[name=filterNationality]").on("change", function (e) {
            e.preventDefault();
            filters.nationality = $(this).val();
        });
        jq("select[name=filterStatuses]").on("change", function (e) {
            e.preventDefault();
            filters.status = $(this).val();
        });

        jq("#applyFilter").on("click", function (e) {
            e.preventDefault();

            // announcementDt.DataTable().ajax.reload(onDataTableReload);
            window.LaravelDataTables["preorientationTable"].ajax
                .url(
                    `?checkpoint=${filters.checkpoint}&nationality=${filters.nationality}&status=${filters.status}`
                )
                .load(onDataTableReload);
        });

        jq("#resetFilter").on("click", function (e) {
            e.preventDefault();

            window.location.reload();
        });
    }

    function handleResetBulkCheckbox() {
        const resetBulkCheckbox = jq("a[href='#resetCheckbox']");

        resetBulkCheckbox.on("click", function (e) {
            e.preventDefault();

            localStorage.removeItem("announcementBulkID");
            jq("input[name=bulkSelect]").prop("checked", false);
            jq("input[name=selectAll]").prop("checked", false);
        });
    }

    // make global function reloadDataTable
    window.reloadDataTable = function () {
        let tbl;

        if (table.length > 0) {
            tbl = table;
        } else if (milestoneTable.length > 0) {
            tbl = milestoneTable;
        } else {
            // console.log("Tidak ada elemen yang ditemukan");
        }

        tbl.DataTable().draw(onDataTableReload);
    };

    // handleCheckpoints();
    handleFilters();
    handleQuickSearch();

    // on page loaded
    jq(document).ready(function () {
        localStorage.removeItem("fMilestone");
        // refresh table
        reloadDataTable();
    });

    jq(document).on("draw.dt", function (e, settings) {
        var api = new $.fn.dataTable.Api(settings);
        // localStorage.removeItem("milestone");
        action.watchDelete();
        action.whenStatusClick("publish");
        action.whenStatusClick("unpublish");

        handleCheckpoints();

        handleBulkCheckbox();
        handleSelectAllCheckbox();
        handleResetBulkCheckbox();

        handleClickViewDetailPreOrientation("btn-view-detail");

        // ... use `state` to restore information
        // handle edit pre-orientation
        // jq('a[data-key="edit-preorientation"]').each(function (_i, elm) {
        //   const $elm = jq(elm)
        //   $elm.on('click', function (e) {
        //     e.preventDefault();
        //     return handleEditPreOrientation(e, $elm.attr('href'));
        //   })
        // })

        const ids = localStorage.getItem("preOrientationBulkID")
            ? localStorage.getItem("preOrientationBulkID").split(",")
            : [];
        if (ids && ids.length > 0) {
            const checkboxes = jq('input[name="bulkSelect"]');

            checkboxes.each(function () {
                const checkbox = jq(this);
                const id = checkbox.attr("data-id");
                if (ids.includes(id)) {
                    checkbox.prop("checked", true);
                }
            });

            // if all checkboxes are checked, then check the select all checkbox
            const allChecked =
                checkboxes.filter(":checked").length === checkboxes.length;
            jq('input[name="selectAll"]').prop("checked", allChecked);
        }

        $(".clone-preor").each(function (_i, el) {
            $(el).on("click", function () {
                const uuid = $(this).attr("data-cloned");
                const route = $(this).attr("data-route");
                const token = $(this).attr("data-token");

                Swal.fire({
                    title: "Are you sure to CLONE?",
                    text: "You won't be able to revert this!",
                    icon: "warning",
                    buttonsStyling: false,
                    showCancelButton: true,
                    confirmButtonText: "confirm",
                    customClass: {
                        confirmButton: "btn btn-success mx-3",
                        cancelButton: "btn btn-danger",
                    },
                }).then(function (result) {
                    if (result.isConfirmed) {
                        jq.ajax({
                            method: "POST",
                            url: route,
                            data: {
                                uuid: uuid,
                                action: "clone",
                                _token: token,
                            },
                            success: function (res) {
                                Swal.fire({
                                    icon: "success",
                                    title: res.data.message,
                                    showConfirmButton: false,
                                    timer: 3000,
                                });
                                table.DataTable().draw();
                            },
                            error: function (res) {
                                swalFire(
                                    "danger",
                                    "Oops!, clone failed.",
                                    res.data.message
                                );
                            },
                        });
                    }
                });
            });
        });
    });
});
