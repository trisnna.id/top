$(function (jq) {
  const table = jq("#studentPoints");

  function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this,
        args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

  function onDataTableReload(_json) {
    const btnDetails = jq(".btn-detail");
    const filterCheckpoints = jq("select[name=filterCheckpoint]").val();

    localStorage.removeItem("milestone");

    if (Array.isArray(filterCheckpoints) && filterCheckpoints.length) {
      localStorage.setItem("milestone", filterCheckpoints);
    }

    btnDetails.each(function (i, elm) {
      const btn = $(elm);
      const status = btn.attr("data-completed");
      const prevBtn = function (currentIndex) {
        if (currentIndex === 0) {
          return btn;
        }

        return btnDetails.eq(currentIndex - 1);
      };

      btn.on("click", function (e) {
        // e.preventDefault();
        const $this = jq(this);

        if (status === "progress") {
          const prevStatus = prevBtn(i).attr("data-completed");
          if (prevStatus === status) {
            swalFire(
              "warning",
              "Wait",
              "Please complete the previous activities."
            );
          } else {
            renderQuiz($this);
          }
        } else {
          renderQuiz($this);
        }
      });
    });
  }

  function handleFilters() {
    const filters = {
      faculty: null,
      school: null,
      programme: null,
      intake: null,
      level_study: null,
      campus: null,
      mobility: null,
      locality: null,
    };

    Object.keys(filters).forEach(function (key) {
      const filterName = `[name=filter_${key}]`;
      jq('select' + filterName)
        .on('change', function (e) {
          e.preventDefault();
          filters[key] = $(this).val();
        })
    })

    jq("#applyFilter").on("click", function (e) {
      e.preventDefault();

      const queries = []
      for (const key of Object.keys(filters)) {
        const value = filters[key]
        if (value !== null) {
          queries.push(`${key}=${filters[key]}`)
        }
      }

      // announcementDt.DataTable().ajax.reload(onDataTableReload);
      window.LaravelDataTables["studentPoints"].ajax.url(`?${queries.join('&')}`).load();
    });

    jq("#resetFilter").on("click", function (e) {
      e.preventDefault();

      window.location.reload();
    });
  }

  function handleQuickSearch() {
    const searchInput = jq('input[name="search"]');
    const elem = jq(document);

    elem.on(
      "keyup",
      searchInput,
      debounce(function (e) {
        // if user press enter key
        // if (e.keyCode === 13) {
        // 	let tbl;

        // 	if (table.length > 0) {
        // 			tbl = table;
        // 	} else if (milestoneTable.length > 0) {
        // 			tbl = milestoneTable;
        // 	} else {
        // 			console.log('Tidak ada elemen yang ditemukan');
        // 	}
        // 	tbl.DataTable().search(searchInput.val()).draw(onDataTableReload);
        // }
        let tbl;

        if (table.length > 0) {
          tbl = table;
        }

        tbl.DataTable()
          .search(searchInput.val())
          .draw();
        // jq.debounce(2000, tbl.DataTable().search(searchInput.val()).draw(onDataTableReload));
      }, 500)
    );
  }

  handleQuickSearch();
  handleFilters();
})
