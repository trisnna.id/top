// Handle thumbnail selector
$(function (jq) {
    let input = jq("input[name='thumbnail']");
    let hex_input = jq("input[name='thumbnail_hex']");
    let placeholder = jq(".image-placeholder");
    let file = null;
    let reader = null;
    let cropper = null;

    input.on("change", function (e) {
        const $this = jq(this);
        file = $this[0].files[0];
        reader = new FileReader();

        // open modal with id 'cropper-modal', then initialize cropper.js
        jq("#cropper-modal").modal("show");
    });
    jq("#cropper-modal").on("show.bs.modal", function () {
        console.log(1);
        let image = document.getElementById("cropper-image");
        image.src = URL.createObjectURL(file);
        cropper = new Cropper(image, {
            aspectRatio: 6 / 4,
            viewMode: 0,
            strict: true,

            // minCanvasWidth is 100% of it's parent element
            minCanvasWidth: 100,
        });

        jq("#cropper-save").on("click", function () {
            // let canvas = cropper.getCroppedCanvas({
            //     width: 400,
            //     height: 225,
            // });
            // do same, but with aspect ratio 6/4
            let canvas = cropper.getCroppedCanvas({
                width: 600,
                height: 400,
            });

            canvas.toBlob(function (blob) {
                let url = URL.createObjectURL(blob);
                let reader = new FileReader();

                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    let base64data = reader.result;
                    let hex = base64data.split(",")[1];

                    // input.val(url);
                    hex_input.val(hex);
                    placeholder.attr("src", url);

                    // set placeholder background image
                    placeholder.css("background-image", `url(${url})`);

                    jq("#cropper-modal").modal("hide");
                };
            });
        });
    });

    jq("#cropper-modal").on("hide.bs.modal", function () {
        jq("#cropper-submit").off("click");

        cropper.destroy();

        let image = document.getElementById("cropper-image");
        image.src = "";
    });
});
