// handle input of select2 element
$(function (jq) {
    const config = {
        theme: "bootstrap5",
        allowClear: true,
        allowSearch: true,
        placeholder: "-- No option selected --",
        tags: false,
    };
    const select2 = jq(".announcement-select2");

    function selectAll(select2) {
        const value = select2.val();

        if (Array.isArray(value) && value.includes("all")) {
            const options = $(".all-support")
                .find("option")
                .filter(function (_i, $element) {
                    return jq($element).attr("value") !== "all";
                })
                .map(function (_i, $element) {
                    return $($element).val();
                })
                .get();

            // select2.val(options).trigger("change.select2");

            // set value to options without triggering change event
            select2.val(options);
        }
    }

    // Set default value of select2
    var defaultToAll = ["nationality", "intake", "programme", "campus"];

    // Using 'select' library to handle input of select2 element
    select2.each(function (_, elm) {
        const $elm = jq(elm);

        $elm.on("change", function () {
            const $this = $(this);

            console.log($this.val());

            // if $this.val() contains other than 'all', remove 'all' from $this.val()
            if (
                Array.isArray($this.val()) &&
                $this.val().length > 1 &&
                $this.val().includes("all")
            ) {
                let newVal = $this.val().filter(function (val) {
                    return val !== "all";
                });
                $this.val(newVal).trigger("change.select2");
            }

            // and if all selected again, select all and forget about the rest
            if ($this.val().length === $this.find("option").length - 1) {
                $this.val("all").trigger("change.select2");
            }

            // selectAll($this);
        });

        // Handle initial value of select2 element
        if (defaultToAll.includes($elm.attr("id"))) {
            $elm.val("all").select2(config);
        } else {
            $elm.select2(config);
        }

        // Handle previous value of select2 element if previous submission failed
        let value = $elm.attr("data-old");

        if ($elm.attr("multiple") && value) {
            value = value.split(",");
            if (value.length === $elm.find("option").length - 1) {
                value = ["all"];
            }

            $elm.val(value).trigger("change");
        } else if (value) {
            $elm.val(value).trigger("change");
        }
    });
});

// Pre sent form data to server
$(function (jq) {
    const form = jq("#announcementForm");

    form.on("submit", function (e) {
        // get the select2 element
        const select2 = jq(".announcement-select2");

        // foreach select2 element, get the value, if value is 'all', set it to all value of the option except 'all'
        select2.each(function (_, elm) {
            const $elm = jq(elm);
            const value = $elm.val();

            if (Array.isArray(value) && value.includes("all")) {
                const options = $(".all-support")
                    .find("option")
                    .filter(function (_i, $element) {
                        return jq($element).attr("value") !== "all";
                    })
                    .map(function (_i, $element) {
                        return $($element).val();
                    })
                    .get();

                $elm.val(options);
            }
        });
    });
});

// Handle thumbnail selector
$(function (jq) {
    let input = jq("input[name='thumbnail']");
    let hex_input = jq("input[name='thumbnail_hex']");
    let placeholder = jq(".image-placeholder");
    let file = null;
    let reader = null;
    let cropper = null;

    input.on("change", function (e) {
        const $this = jq(this);
        file = $this[0].files[0];
        reader = new FileReader();

        // open modal with id 'cropper-modal', then initialize cropper.js
        jq("#cropper-modal").modal("show");
    });
    jq("#cropper-modal").on("show.bs.modal", function () {
        console.log(1);
        let image = document.getElementById("cropper-image");
        image.src = URL.createObjectURL(file);
        cropper = new Cropper(image, {
            aspectRatio: 6 / 4,
            viewMode: 0,
            strict: true,

            // minCanvasWidth is 100% of it's parent element
            minCanvasWidth: 100,
        });

        jq("#cropper-save").on("click", function () {
            // let canvas = cropper.getCroppedCanvas({
            //     width: 400,
            //     height: 225,
            // });
            // do same, but with aspect ratio 6/4
            let canvas = cropper.getCroppedCanvas({
                width: 600,
                height: 400,
            });

            canvas.toBlob(function (blob) {
                let url = URL.createObjectURL(blob);
                let reader = new FileReader();

                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    let base64data = reader.result;
                    let hex = base64data.split(",")[1];

                    // input.val(url);
                    hex_input.val(hex);
                    placeholder.attr("src", url);

                    // set placeholder background image
                    placeholder.css("background-image", `url(${url})`);

                    jq("#cropper-modal").modal("hide");
                };
            });
        });
    });

    jq("#cropper-modal").on("hide.bs.modal", function () {
        jq("#cropper-submit").off("click");

        cropper.destroy();

        let image = document.getElementById("cropper-image");
        image.src = "";
    });
});

// Handle input `content`. Announcement using `tinymce` editor library
$(function (jq) {
    return tinymce.init({
        selector: "#contentEditor",
        height: "350",
        plugins: "image lists link",
        // menubar: false,
        // toolbar:
        //     "bold italic strikethrough underline | bullist numlist | alignleft aligncenter alignright | image link ",
        link_title: false,
        link_quicklink: false,
        file_picker_types: "image",
        setup: function (editor) {
            var max = 350; // maximum number of characters allowed
            editor.on("submit", function (event) {
                var numChars =
                    tinymce.activeEditor.plugins.wordcount.body.getCharacterCount();
                if (numChars > max) {
                    alert("Maximum " + max + " characters allowed.");
                    event.preventDefault();
                    return false;
                }
            });
        },
        file_picker_callback: function (callback, value, meta) {
            console.log(value, meta);
            var input = document.createElement("input");
            input.setAttribute("type", "file");
            input.setAttribute("accept", "image/*");

            /*
        Note: In modern browsers input[type="file"] is functional without
        even adding it to the DOM, but that might not be the case in some older
        or quirky browsers like IE, so you might want to add it to the DOM
        just in case, and visually hide it. And do not forget do remove it
        once you do not need it anymore.
      */

            input.onchange = function () {
                var file = this.files[0];

                var reader = new FileReader();
                reader.onload = function () {
                    /*
            Note: Now we need to register the blob in TinyMCEs image blob
            registry. In the next release this part hopefully won't be
            necessary, as we are looking to handle it internally.
          */
                    var id = "blobid" + new Date().getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(",")[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    /* call the callback and populate the Title field with the file name */
                    callback(blobInfo.blobUri(), { title: file.name });
                };
                reader.readAsDataURL(file);
            };

            input.click();
        },
    });
});

/**
 * This scripts will handle client/server side process that
 * related with resources module
 *
 * @author Tris <tris@grtech.com.my>
 */
$(function (jq) {
    ("use strict");
    // Datatable instance
    // const announcementDt = $.fn.dataTable.tables(true);

    const action = {
        submit: function () {
            const form = jq("#announcementForm");
            const btn = jq("button[type=submit]");

            btn.on("click", function (e) {
                e.preventDefault();
                form.trigger("submit");
            });
        },
        whenBulkActionClick: function (status, humanReadableStatus) {
            $(`a[href="#${status}"]`).each(function (_e, el) {
                const $this = $(el);
                const route = $this.attr("data-route");
                const value = localStorage.getItem("announcementBulkID");
                console.log(value);
                const token = $this.attr("data-token");

                $this.on("click", function (e) {
                    e.preventDefault();
                    Swal.fire({
                        title: `Are you sure want to ${humanReadableStatus} all items you checked?`,
                        // text: 'You still able to make un',
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Yes",
                        showCancelButton: true,
                        customClass: {
                            confirmButton: "btn btn-danger",
                            cancelButton: "btn btn-secondary",
                        },
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            jq.ajax({
                                method: "POST",
                                url: route,
                                data: { _token: token, key: value },
                                success: function (res) {
                                    Swal.fire({
                                        icon: "success",
                                        title: res.data.message,
                                        showConfirmButton: false,
                                        timer: 3000,
                                    });
                                    localStorage.removeItem(
                                        "announcementBulkID"
                                    );
                                    table.DataTable().draw();
                                },
                                error: function (res) {
                                    if (res.status === 422) {
                                        Swal.fire({
                                            title: "Oops!, something went wrong.",
                                            text: `Cannot ${humanReadableStatus} the pre-orientation activity`,
                                            icon: "danger",
                                            buttonsStyling: false,
                                            confirmButtonText: "Close",
                                            showCancelButton: false,
                                            customClass: {
                                                confirmButton: "btn btn-danger",
                                            },
                                        });
                                    }
                                },
                            });
                        }
                    });
                });
            });
        },
        // handle notification message after CRUD event happend
        notify: function (message) {
            if (message) {
                Swal.fire({
                    icon: "success",
                    title: message,
                    showConfirmButton: false,
                    timer: 3000,
                });
                return;
            }

            const $element = jq(".has-notif");
            if ($element.length) {
                const message = $element.attr("data-message");
                console.log(message);
                if (typeof message === "string" && message.length) {
                    Swal.fire({
                        icon: "success",
                        title: message,
                        showConfirmButton: false,
                        timer: 3000,
                    });
                }
            }
        },

        watchDelete: function (callback) {
            const self = this;
            $("a[data-method=delete]").each(function (_i, el) {
                const $this = $(el);

                $this.on("click", function (e) {
                    e.preventDefault();
                    const url = $this.attr("href");
                    const token = $this.attr("data-token");

                    Swal.fire({
                        title: "Are you sure want to delete this item? ",
                        text: "This process cannot be undone!",
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Yes",
                        showCancelButton: true,
                        customClass: {
                            confirmButton: "btn btn-danger",
                            cancelButton: "btn btn-secondary",
                        },
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            jq.ajax({
                                method: "POST",
                                url: url,
                                data: {
                                    _token: token,
                                    _method: "DELETE",
                                },
                                success: function (res) {
                                    self.notify(res.message);
                                    table.DataTable().draw();
                                },
                                error: function (req, res) {
                                    console.log(req, res);
                                },
                            });
                        }
                    });
                });
            });
        },

        whenStatusClick: function (status) {
            $(`a[href="#${status}"]`).each(function (_e, el) {
                const $this = $(el);
                const route = $this.attr("data-route");
                const value = $this.parent().find('input[name="key"]').val();
                const token = $this.attr("data-token");

                $this.on("click", function (e) {
                    e.preventDefault();
                    Swal.fire({
                        title: `Are you sure want to ${status} this item?`,
                        // text: 'You still able to make un',
                        icon: "warning",
                        buttonsStyling: false,
                        confirmButtonText: "Yes",
                        showCancelButton: true,
                        customClass: {
                            confirmButton: "btn btn-danger",
                            cancelButton: "btn btn-secondary",
                        },
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            jq.ajax({
                                method: "POST",
                                url: route,
                                data: { _token: token, key: value },
                                success: function (res) {
                                    Swal.fire({
                                        icon: "success",
                                        title: res.data.message,
                                        showConfirmButton: false,
                                        timer: 3000,
                                    });
                                    table.DataTable().draw();
                                },
                                error: function (res) {
                                    if (res.status === 422) {
                                        Swal.fire({
                                            title: "Oops!, something went wrong.",
                                            text: `Cannot ${status} the pre-orientation activity`,
                                            icon: "danger",
                                            buttonsStyling: false,
                                            confirmButtonText: "Close",
                                            showCancelButton: false,
                                            customClass: {
                                                confirmButton: "btn btn-danger",
                                            },
                                        });
                                    }
                                },
                            });
                        }
                    });
                });
            });
        },
    };

    action.submit();
    action.notify();

    /**
     * Get the pre-orientation datatable element
     *
     * @var {JQuery<HTMLElement>}
     */
    const table = jq("#announcementTable");

    function swalFire(icon, title, message) {
        return Swal.fire({
            title: title,
            text: message,
            icon: icon,
            buttonsStyling: false,
            confirmButtonText: "Ok, got it!",
            customClass: {
                confirmButton: "btn btn-danger",
            },
        });
    }

    function onDataTableReload(_json) {
        const btnDetails = jq(".btn-detail");
        const filterCheckpoints = jq("select[name=filterCheckpoint]").val();

        localStorage.removeItem("checkpoint");

        if (Array.isArray(filterCheckpoints) && filterCheckpoints.length) {
            localStorage.setItem("checkpoint", filterCheckpoints);
        }

        btnDetails.each(function (i, elm) {
            const btn = $(elm);
            const status = btn.attr("data-completed");
            const prevBtn = function (currentIndex) {
                if (currentIndex === 0) {
                    return btn;
                }

                return btnDetails.eq(currentIndex - 1);
            };

            btn.on("click", function (e) {
                // e.preventDefault();
                const $this = jq(this);

                if (status === "progress") {
                    const prevStatus = prevBtn(i).attr("data-completed");
                    if (prevStatus === status) {
                        swalFire(
                            "warning",
                            "Wait",
                            "Please complete the previous activities."
                        );
                    } else {
                        renderQuiz($this);
                    }
                } else {
                    renderQuiz($this);
                }
            });
        });
    }

    /* handle button checkpoints behaviour */
    function handleCheckpoints() {
        let checkpoints = $("#checkpointContainer").attr("data-checkpoints");
        const CHECKPOINT_BTN_COLOR = {
            done: "success",
            ongoing: "#FF6600",
            todo: "info",
            locked: "#5A5A5A",
        };

        if (typeof checkpoints === "string" && checkpoints.length) {
            checkpoints = checkpoints.split(",");
        }

        jq(".btn-checkpoint").each(function (i, elm) {
            const $this = $(elm);
            const anchor = $this.find("a");
            const checkpoint = anchor.attr("data-checkpoint");
            const btnColor = CHECKPOINT_BTN_COLOR[checkpoint];

            if (btnColor.includes("#")) {
                anchor.css({ "background-color": btnColor });
            } else {
                anchor.addClass(`btn-${CHECKPOINT_BTN_COLOR[checkpoint]}`);
            }

            anchor.on("click", function (e) {
                e.preventDefault();

                jq("select[name=filterCheckpoint]").val(null).trigger("change");

                if (["locked", "todo"].includes(checkpoint)) {
                    const title = `Checkpoint ${i + 1}`;
                    let message = "";
                    let icon = "";

                    if (checkpoint === "locked") {
                        (message = `Oops!, This checkpoint is locked`),
                            (icon = "error");
                    } else if (checkpoint === "todo") {
                        message = `Oops!, You need to complete all the previous checkpoints.`;
                        icon = "warning";
                    }

                    swalFire(icon, title, message).then(function (result) {
                        if (result.isConfirmed) {
                            // Remove the checkpoint list when user click the confirmation button
                            localStorage.setItem("checkpoint", null);
                            table.DataTable().draw();
                        }
                    });
                } else {
                    // Store checkpoint value into local storage so,
                    // we can use it as request data datatable
                    localStorage.setItem("checkpoint", i + 1);

                    // Send request to pre-orientation datatable endpoint
                    // To update the table list base on the pre-orientation checkpoint
                    table.DataTable().draw(onDataTableReload);
                }
            });
        });
    }

    // Handle selectAll checkbox
    function handleSelectAllCheckbox() {
        const selectAllCheckboxSelector = 'input[name="selectAll"]';

        jq(document).on("change", selectAllCheckboxSelector, function () {
            // detect this checkbox is checked or not
            const checkboxes = jq('input[name="bulkSelect"]');
            const isChecked = jq(this).is(":checked");

            // check or uncheck all checkboxes
            checkboxes.prop("checked", isChecked);

            // get all checked checkboxes and set to localStorage with key 'preOrientationBulkID'
            const checked_ids = checkboxes.filter(":checked").map(function () {
                return jq(this).attr("data-id");
            });

            const unchecked_ids = checkboxes
                .filter(":not(:checked)")
                .map(function () {
                    return jq(this).attr("data-id");
                });

            const bulkID = localStorage.getItem("announcementBulkID") || "";
            const bulkIDArray = bulkID.split(",");

            const newbulkID = bulkIDArray
                .filter(function (id) {
                    return !unchecked_ids.toArray().includes(id);
                })
                .concat(checked_ids.toArray())
                .filter(function (id, index, self) {
                    return id !== "" && self.indexOf(id) === index;
                });

            localStorage.setItem("announcementBulkID", newbulkID.join(","));

            action.whenBulkActionClick("bulkPublish", "publish");
            action.whenBulkActionClick("bulkUnpublish", "unpublish");
        });
    }

    // Handle checkbox checked for bulk action
    function handleBulkCheckbox() {
        const bulkCheckboxSelector = 'input[name="bulkSelect"]';
        const elem = jq(document);

        // for every checkbox checked, get the data-id from that element and set to localStorage with key 'bulkID'. the value is id separated by comma
        elem.on("change", bulkCheckboxSelector, function () {
            const checkboxes = jq(bulkCheckboxSelector);
            const checked = checkboxes.filter(":checked");
            const unchecked = checkboxes.filter(":not(:checked)");

            const checked_ids = checked
                .map(function () {
                    return jq(this).attr("data-id");
                })
                .get();
            const unchecked_ids = unchecked
                .map(function () {
                    return jq(this).attr("data-id");
                })
                .get();

            const bulkID = localStorage.getItem("announcementBulkID");
            const bulkIDArray = bulkID ? bulkID.split(",") : [];

            const checked_id = checked_ids.filter(function (id) {
                return !bulkIDArray.includes(id);
            });

            const unchecked_id = unchecked_ids.filter(function (id) {
                return bulkIDArray.includes(id);
            });

            const newbulkID = bulkIDArray
                .filter(function (id) {
                    return !unchecked_id.includes(id);
                })
                .concat(checked_id);

            localStorage.setItem("announcementBulkID", newbulkID.join(","));

            action.whenBulkActionClick("bulkPublish", "publish");
            action.whenBulkActionClick("bulkUnpublish", "unpublish");
        });
    }

    function handleFilters() {
        const filters = {
            intake: null,
            programme: null,
            nationality: null,
            campus: null,
            status: null,
        };

        jq("select[name=filterIntakes]").on("change", function (e) {
            e.preventDefault();
            filters.intake = $(this).val();
        });
        jq("select[name=filterProgrammes]").on("change", function (e) {
            e.preventDefault();
            filters.programme = $(this).val();
        });
        jq("select[name=filterNationality]").on("change", function (e) {
            e.preventDefault();
            filters.nationality = $(this).val();
        });
        jq("select[name=filterCampuses]").on("change", function (e) {
            e.preventDefault();
            filters.campus = $(this).val();
        });
        jq("select[name=filterStatuses]").on("change", function (e) {
            e.preventDefault();
            filters.status = $(this).val();
        });

        jq("#applyFilter").on("click", function (e) {
            e.preventDefault();

            localStorage.setItem("checkpoint", filters.checkpoint);
            // announcementDt.DataTable().ajax.reload(onDataTableReload);
            window.LaravelDataTables["announcementTable"].ajax
                .url(
                    `?checkpoint=${filters.checkpoint}&intake=${filters.intake}&programme=${filters.programme}&nationality=${filters.nationality}&campus=${filters.campus}&status=${filters.status}`
                )
                .load(onDataTableReload);
        });

        jq("#resetFilter").on("click", function (e) {
            e.preventDefault();

            window.location.reload();
        });
    }

    function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this,
                args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    function handleQuickSearch() {
        const searchInput = jq('input[name="search"]');
        const elem = jq(document);

        elem.on(
            "keyup",
            searchInput,
            debounce(function (e) {
                // if user press enter key
                // if (e.keyCode === 13) {
                // 	let tbl;

                // 	if (table.length > 0) {
                // 			tbl = table;
                // 	} else if (milestoneTable.length > 0) {
                // 			tbl = milestoneTable;
                // 	} else {
                // 			console.log('Tidak ada elemen yang ditemukan');
                // 	}
                // 	tbl.DataTable().search(searchInput.val()).draw(onDataTableReload);
                // }
                let tbl;

                if (table.length > 0) {
                    tbl = table;
                } else {
                    console.log("Tidak ada elemen yang ditemukan");
                }

                tbl.DataTable()
                    .search(searchInput.val())
                    .draw(onDataTableReload);
                // jq.debounce(2000, tbl.DataTable().search(searchInput.val()).draw(onDataTableReload));
            }, 500)
        );
    }

    function handleResetBulkCheckbox() {
        const resetBulkCheckbox = jq("a[href='#resetCheckbox']");

        resetBulkCheckbox.on("click", function (e) {
            e.preventDefault();

            localStorage.removeItem("announcementBulkID");
            jq("input[name=bulkSelect]").prop("checked", false);
            jq("input[name=selectAll]").prop("checked", false);
        });
    }

    handleCheckpoints();
    handleFilters();
    handleQuickSearch();

    jq(document).on("draw.dt", function (e, settings) {
        var api = new $.fn.dataTable.Api(settings);

        handleBulkCheckbox();
        handleSelectAllCheckbox();
        handleResetBulkCheckbox();

        action.watchDelete();
        action.whenStatusClick("publish");
        action.whenStatusClick("unpublish");

        // ... use `state` to restore information
        // handle edit pre-orientation
        // jq('a[data-key="edit-preorientation"]').each(function (_i, elm) {
        //   const $elm = jq(elm)
        //   $elm.on('click', function (e) {
        //     e.preventDefault();
        //     return handleEditPreOrientation(e, $elm.attr('href'));
        //   })
        // })

        const ids = localStorage.getItem("announcementBulkID")
            ? localStorage.getItem("announcementBulkID").split(",")
            : [];
        if (ids && ids.length > 0) {
            const checkboxes = jq('input[name="bulkSelect"]');

            checkboxes.each(function () {
                const checkbox = jq(this);
                const id = checkbox.attr("data-id");
                if (ids.includes(id)) {
                    checkbox.prop("checked", true);
                }
            });

            // if all checkboxes are checked, then check the select all checkbox
            const allChecked =
                checkboxes.filter(":checked").length === checkboxes.length;
            jq('input[name="selectAll"]').prop("checked", allChecked);
        }
    });
});
