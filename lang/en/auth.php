<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // 'no_account' => "Uh-oh! We couldn't locate your account.",
    'password_incorrect' => 'Uh-Oh! You have entered the wrong password',
    'failed' => "Uh-oh! We couldn't locate your account.",
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
