<?php

return [
    'action' => [
        'create'                => 'Create :x',
    ],

    'labels' => [
        'back_to_list'          => 'Back to :x List',
        'sequence'              => 'Sequence',
        'title'                 => 'Title',
    ],

    'placeholder' => [
        'sequence'              => 'Use number value only, eg: 1',
    ],

    'title' => [
        'announcement'          => 'News & Announcements',
        'arrival'               => 'Pre-Arrival',
        'management'            =>  ':x Management',
        'preor'                 => 'Pre-Orientation',
        'orientation'           => 'Orientation :x',
    ]
];

