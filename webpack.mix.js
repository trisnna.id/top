const mix = require('laravel-mix');
const glob = require('glob');
const path = require('path');
const ReplaceInFileWebpackPlugin = require('replace-in-file-webpack-plugin');
const rimraf = require('rimraf');
// const del = require('del');
const fs = require('fs');
require('dotenv').config();
// const MergeIntoSingle = require('webpack-merge-and-include-globally/index');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/auth.js', 'public/js')
    .js('public/js/modules/announcement.js', 'public/js/modules/announcement.min.js')
    .js('public/js/modules/resource.js', 'public/js/modules/resource.min.js')
    .js('public/js/modules/studentpoint.js', 'public/js/modules/studentpoint.min.js')
    .js('public/js/modules/orientation.js', 'public/js/modules/orientation.min.js')
    .js('public/js/modules/preorientation.js', 'public/js/modules/preorientation.min.js')
    .sass(`resources/sass/app.scss`, `public/css`)
    .sass(`resources/sass/auth.scss`, `public/css`)
    .alias({
        '~ziggy': path.join(__dirname, 'vendor/tightenco/ziggy/src/js'),
        '~resources': path.join(__dirname, 'resources'),
    }).options({
        cssNano: {
            discardComments: false,
        }
    })
    .version();

// Build 3rd party plugins css/js
mix.sass(`resources/plugins/plugins.scss`, `public/plugins/global/plugins.bundle.css`)
    .then(() => {
        // remove unused preprocessed fonts folder
        rimraf(path.resolve('public/fonts'), () => {
        });
        rimraf(path.resolve('public/images'), () => {
        });
    })
    .sourceMaps(!mix.inProduction())
    .options({ processCssUrls: false })
    .js('resources/plugins/plugins.js', 'public/plugins/global/plugins.bundle.js');

// Build custom 3rd party plugins
(glob.sync(`resources/plugins/custom/**/*.js`) || []).forEach(file => {
    mix.js(file, `public/${file.replace(`resources/`, '').replace('.js', '.bundle.js')}`);
});
(glob.sync(`resources/plugins/custom/**/*.scss`) || []).forEach(file => {
    mix.sass(file, `public/${file.replace(`resources/`, '').replace('.scss', '.bundle.css')}`);
});

let plugins = [
    new ReplaceInFileWebpackPlugin([
        {
            // rewrite font paths
            dir: path.resolve(`public/plugins/global`),
            test: /\.css$/,
            rules: [
                {
                    // fontawesome
                    search: /url\((\.\.\/)?webfonts\/(fa-.*?)"?\)/g,
                    replace: 'url(./fonts/@fortawesome/$2)',
                },
                {
                    // lineawesome fonts
                    search: /url\(("?\.\.\/)?fonts\/(la-.*?)"?\)/g,
                    replace: 'url(./fonts/line-awesome/$2)',
                },
                {
                    // bootstrap-icons
                    search: /url\(.*?(bootstrap-icons\..*?)"?\)/g,
                    replace: 'url(./fonts/bootstrap-icons/$1)',
                },
                {
                    // fonticon
                    search: /url\(.*?(fonticon\..*?)"?\)/g,
                    replace: 'url(./fonts/fonticon/$1)',
                },
            ],
        },
    ]),
];

mix.webpackConfig({
    plugins: plugins,
    ignoreWarnings: [{
        module: /esri-leaflet/,
        message: /version/,
    }],
});

mix.disableNotifications();

// Webpack.mix does not copy fonts, manually copy
(glob.sync('node_modules/+(@fortawesome|socicon|line-awesome|bootstrap-icons)/**/*.+(woff|woff2|eot|ttf)') || []).forEach(file => {
    var folder = file.match(/node_modules\/(.*?)\//)[1];
    mix.copy(file, `public/plugins/global/fonts/${folder}/${path.basename(file)}`);
});
(glob.sync('node_modules/jstree/dist/themes/default/*.+(png|gif)') || []).forEach(file => {
    mix.copy(file, `public/plugins/custom/jstree/${path.basename(file)}`);
});

// Raw plugins
(glob.sync(`resources/plugins/custom/**/*.js.json`) || []).forEach(file => {
    let filePaths = JSON.parse(fs.readFileSync(file, 'utf-8'));
    const fileName = path.basename(file).replace('.js.json', '');
    mix.scripts(filePaths, `public/plugins/custom/${fileName}/${fileName}.bundle.js`);
});
