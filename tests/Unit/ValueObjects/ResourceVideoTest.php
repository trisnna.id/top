<?php

namespace Tests\Unit\ValueObjects;

use App\ValueObjects\Resources\Video;
use PHPUnit\Framework\TestCase;

class ResourceVideoTest extends TestCase
{
    /**
     * Test 'getSource' method
     *
     * @group   resources
     * @group   value_objects
     * 
     * @return  void
     */
    public function test_get_source_as_string()
    {
        $instance = new Video([
            'source' => 'just string'
        ]);

        $this->assertEquals('just string', $instance->getSource());
    }
}
