<?php

namespace Tests\Unit\ValueObjects;

use App\ValueObjects\ContentLink;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Tests\DataProviders\ContentDataProvider;

class ContentLinkTest extends TestCase
{
    use ContentDataProvider;

    /**
     * Get input content paragraph
     *
     * @return  array
     */
    public function contentOnlyParagraph(): array
    {
        return [
            'content only contain paragraph' => [$this->paragraphElement()]
        ];
    }

    /**
     * Get input content paragraph & image
     *
     * @return  array
     */
    public function contentWithImage(): array
    {
        return [
            'content contains paragraph and image' => [$this->contentWithoutLinks()]
        ];
    }

    /**
     * Get input content paragraph & image
     *
     * @return  array
     */
    public function contentWithLinks(): array
    {
        return [
            'content contains paragraph and links' => [$this->contentWithoutImage()]
        ];
    }

    /**
     * Get input content paragraph, image, & links
     *
     * @return  array
     */
    public function contentWithImageAndLinks(): array
    {
        return [
            'content contains paragraph, image, and links' => [$this->contentFull()]
        ];
    }

    /**
     * Test create a new content link from array attributes
     * 
     * @group           pre_orientation
     * @group           value_objects
     *
     * @return ContentLink
     */
    public function test_can_create_new_content_link_instance_from_array(): ContentLink
    {
        $attributes = $this->linksExpectation()[0];

        $instance = new ContentLink($attributes);

        $this->assertSame($attributes, $instance->toArray());
        $this->assertSame(json_encode($attributes), $instance->toJson());
        $this->assertEquals($attributes['text'], $instance->text);
        $this->assertEquals($attributes['link'], $instance->link);
        $this->assertEquals($attributes['thumbnail'], $instance->thumbnail);
        $this->assertEquals($attributes['raw'], $instance->raw);

        return $instance;
    }

    /**
     * Test getter/setter methods of content link instance
     * 
     * @depends     test_can_create_new_content_link_instance_from_array
     * @group       pre_orientation
     * @group       value_objects
     *
     * @param   ContentLink     $instance
     * @return  ContentLink
     */
    public function test_can_update_the_current_content_link_attributes(ContentLink $instance): void
    {
        $oldAttributes = $this->linksExpectation()[0]; 
        $attributes = $this->linksExpectation()[1];

        $instance->text = $attributes['text'];
        $instance->link = $attributes['link'];
        $instance->thumbnail = $attributes['thumbnail'];
        $instance->raw = $attributes['raw'];

        $this->assertSame($attributes, $instance->toArray());
        $this->assertSame(json_encode($attributes), $instance->toJson());
        $this->assertNotEquals($oldAttributes['raw'], $instance->raw);
        $this->assertNotEquals($oldAttributes['text'], $instance->text);
        $this->assertNotEquals($oldAttributes['link'], $instance->link);
        $this->assertNotEquals($oldAttributes['thumbnail'], $instance->thumbnail);
        $this->assertNotEquals($oldAttributes['raw'], $instance->raw);
        $this->assertEquals($attributes['text'], $instance->text);
        $this->assertEquals($attributes['link'], $instance->link);
        $this->assertEquals($attributes['thumbnail'], $instance->thumbnail);
    }

    /**
     * The content link should extract some `anchor` tags and
     * convert the the tags attributes as an collection 
     *
     * @dataProvider    contentWithLinks
     * @dataProvider    contentWithImageAndLinks
     * @group           pre_orientation
     * @group           value_objects
     * 
     * @param   string  $content
     * @return  void
     */
    public function test_can_parse_link_from_youtube(string $content): void
    {
        $links = ContentLink::parse($content);

        $this->assertInstanceOf(Collection::class, $links);
        $this->assertCount(2, $links);
        $links->each(function ($link, $index) {
            $this->assertInstanceOf(ContentLink::class, $link);
            $this->assertSame($this->linksExpectation()[$index], $link->toArray());
            $this->assertSame(json_encode($this->linksExpectation()[$index]), $link->toJson());
        });
    }

    /**
     * Test handle content if there are `anchor` tags that not pointed
     * to youtube link
     *
     * @group   pre_orientation
     * @group   value_objects
     * 
     * @param   string  $content
     * @return  void
     */
    public function test_can_parse_link_from_others_link_then_youtube(): void
    {
        $content = '<p><a href="https://netflix.com/abcd12345">Not Youtube Link</a></p>';
        $result = [
            'text'  => 'Not Youtube Link',
            'link'  => 'https://netflix.com/abcd12345',
            'thumbnail' => null,
            'raw'   => '<a href="https://netflix.com/abcd12345">Not Youtube Link</a>',
        ];
        $links = ContentLink::parse($content);

        $this->assertInstanceOf(Collection::class, $links);
        $this->assertCount(1, $links);
        $links->each(function ($link) use ($result) {
            $this->assertInstanceOf(ContentLink::class, $link);
            $this->assertSame($result, $link->toArray());
            $this->assertSame(json_encode($result), $link->toJson());
        });
    }

    /**
     * Test the 'links' property should contains empty data
     * because there are no anchor tags found within th HTML content
     * 
     * @dataProvider    contentOnlyParagraph
     * @dataProvider    contentWithImage
     * @group           pre_orientation
     * @group           value_objects
     * 
     * @param   string  $content
     * @return  void
     */
    public function test_content_links_should_empty_because_there_are_no_links_found_in_html(string $content): void
    {
        $links = ContentLink::parse($content);

        $this->assertInstanceOf(Collection::class, $links);
        $this->assertCount(0, $links);
    }
}
