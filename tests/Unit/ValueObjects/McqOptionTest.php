<?php

namespace Tests\Unit\ValueObjects;

use App\ValueObjects\McqOption;
use PHPUnit\Framework\TestCase;
use Tests\DataProviders\QuizDataProvider;

class McqOptionTest extends TestCase
{
    use QuizDataProvider;

    /**
     * Get question options that all options are marked
     * as correct answer
     *
     * @return array
     */
    public function correctAnswersDataProvider(): array
    {
        $options = $this->questionNumber9()['answers'];

        return [
            'option a' => [
                $options[0],
                ['key' => 'a', 'value' => $options[0]['a'], 'mark_as_answer' => true]
            ],
            'option b' => [
                $options[1],
                ['key' => 'b', 'value' => $options[1]['b'], 'mark_as_answer' => true]
            ],
            'option c' => [
                $options[2],
                ['key' => 'c', 'value' => $options[2]['c'], 'mark_as_answer' => true]
            ],
            'option d' => [
                $options[3],
                ['key' => 'd', 'value' => $options[3]['d'], 'mark_as_answer' => true]
            ],
        ];
    }

    /**
     * Test create MCQ option and marked as correct answer
     *
     * @dataProvider correctAnswersDataProvider
     * @group   pre_orientations
     * @group   value_objects
     *
     * @param   array   $input
     * @param   array   $expectation
     * @return  void
     */
    public function test_can_create_mcq_option_marked_as_correct_answer(array $input, array $expectation): void
    {
        $instance = new McqOption($input);

        $this->assertEquals($expectation['key'], $instance->key);
        $this->assertEquals($expectation['value'], $instance->value);
        $this->assertTrue($instance->markAsAnswer);
        $this->assertSame($expectation, $instance->toArray());
        $this->assertSame(json_encode($expectation), $instance->toJson());
        $this->assertTrue($instance->isCorrect());
    }

    /**
     * Get incorrect options from question attributes
     *
     * @return array
     */
    public function incorrectAnswersDataProvider(): array
    {
        $options = $this->questionNumber8()['answers'];

        return [
            'option a' => [
                $options[0],
                ['key' => 'a', 'value' => $options[0]['a'], 'mark_as_answer' => false]
            ],
            'option b' => [
                $options[1],
                ['key' => 'b', 'value' => $options[1]['b'], 'mark_as_answer' => false]
            ],
        ];
    }

    /**
     * Test create MCQ option and marked as in correct answer
     *
     * @dataProvider incorrectAnswersDataProvider
     * @group   pre_orientations
     * @group   value_objects
     *
     * @param   array   $input
     * @param   array   $expectation
     * @return  void
     */
    public function test_can_create_mcq_option_marked_as_incorrect_answer(array $input, array $expectation): void
    {
        $instance = new McqOption($input);

        $this->assertEquals($expectation['key'], $instance->key);
        $this->assertEquals($expectation['value'], $instance->value);
        $this->assertFalse($instance->markAsAnswer);
        $this->assertSame($expectation, $instance->toArray());
        $this->assertSame(json_encode($expectation), $instance->toJson());
        $this->assertFalse($instance->isCorrect());
    }

    /**
     * Test option key to upper case format
     *
     * @group   pre_orientations
     * @group   value_objects
     *
     * @return  void
     */
    public function test_get_formatted_option_key(): void
    {
        $input = $this->questionNumber1()['answers'];

        $instance = new McqOption($input[0]);

        $this->assertEquals('A', $instance->getTitleKey());
    }

    public function attributesDataProperties(): array
    {
        return [
            'correct answer' => [
                ['a' => 'Value A', 'mark_as_answer' => 'on'],
                ['key' => 'a', 'value' => 'Value A', 'mark_as_answer' => true]
            ],
            'wrong answer' => [
                ['a' => 'Value A'],
                ['key' => 'a', 'value' => 'Value A', 'mark_as_answer' => false]
            ],
            'mcq attribute correct' => [
                ['key' => 'a', 'value' => 'Value A', 'mark_as_answer' => true],
                ['key' => 'a', 'value' => 'Value A', 'mark_as_answer' => true]
            ],
            'mcq attribute incorrect' => [
                ['key' => 'a', 'value' => 'Value A', 'mark_as_answer' => false],
                ['key' => 'a', 'value' => 'Value A', 'mark_as_answer' => false]
            ]
        ];
    }
    
    /**
     * Test '__construct' method
     * 
     * @dataProvider incorrectAnswersDataProvider
     * @group   pre_orientations
     * @group   value_objects
     *
     * @param   array   $attributes
     * @param   array   $expectation
     * @return  void
     */
    public function test_can_map_the_given_attributes_to_class_properties(array $attributes, array $expectation): void
    {
        $instance = new McqOption($attributes);

        $this->assertEquals($expectation['key'], $instance->key);
        $this->assertEquals($expectation['value'], $instance->value);
        $this->assertEquals($expectation['mark_as_answer'], $instance->markAsAnswer);
        $this->assertEquals($expectation['mark_as_answer'], $instance->isCorrect());
        $this->assertSame($expectation, $instance->toArray());
        $this->assertSame(json_encode($expectation), $instance->toJson());
    }
}
