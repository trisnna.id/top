<?php

namespace Tests\Unit\ValueObjects;

use App\ValueObjects\Faq;
use App\ValueObjects\Resources\File;
use App\ValueObjects\Resources\Link;
use App\ValueObjects\Resources\Video;
use PHPUnit\Framework\TestCase;
use Tests\DataProviders\ContentDataProvider;

class ResourcesTest extends TestCase
{
    use ContentDataProvider;

    /**
     * Get resource input by category
     *
     * @return array
     */
    public function resourcesDataProvider(): array
    {
        return [
            'resource faq' => [
                Faq::class,
                [
                    'question' => 'Just a simple faq question?',
                    'answer' => $this->paragraphElement(),
                ]
            ],
            'resource video from external link' => [
                Video::class,
                [
                    'source' => 'https://youtube.com/v=120398',
                    'is_external' => true,
                ]
            ],
            'resource video from file' => [
                Video::class,
                [
                    'source' => 'path/to/file.mp4',
                    'is_external' => false,
                ]
            ],
            'resource file' => [
                File::class,
                [
                    'source' => 'path/to/file.pdf',
                    'name' => 'document.pdf',
                ]
            ],
            'resource link' => [
                Link::class,
                [
                    'source' => 'Journal Repository',
                    'text' => 'https://document.com/journal-repository',
                ]
            ]
        ];
    }

    /**
     * Test create resource instance of faq, file, link, or video
     *
     * @dataProvider resourcesDataProvider
     * @group   resources
     * @group   value_objects
     * 
     * @param   string  $className
     * @param   array   $attributes
     * @return  void
     */
    public function test_can_create_resource_instance_base_on_category(string $className, array $attributes): void
    {
        $instance = new $className($attributes);

        $this->assertSame($attributes, $instance->toArray());
        $this->assertJson($instance->toJson());
    }
}
