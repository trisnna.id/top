<?php

namespace Tests\Unit\ValueObjects;

use App\ValueObjects\Resources\File;
use PHPUnit\Framework\TestCase;

class ResourceFileTest extends TestCase
{
    /**
     * Test 'getExtension' method
     *
     * @group   resources
     * @group   value_objects
     * 
     * @return  void
     */
    public function test_get_file_extension()
    {
        $instance = new File([
            'source' => '/path/to/file.pdf'
        ]);

        $this->assertEquals('pdf', $instance->getExtension());
    }
}
