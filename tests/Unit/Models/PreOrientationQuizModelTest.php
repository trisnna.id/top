<?php

namespace Tests\Unit\Models;

use App\Contracts\Entities\PreOrientationQuizEntity;
use App\Models\PreOrientation;
use App\Models\PreOrientationQuiz;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DataProviders\QuizDataProvider;
use Tests\TestCase;

class PreOrientationQuizModelTest extends TestCase
{
    use QuizDataProvider;
    use RefreshDatabase;

    /**
     * Get mcq questions
     *
     * @return  array
     */
    public function fourQuestionsDataProvider(): array
    {
        return [
            'question number 1 with shuffle' => [
                $question1 = $this->questionNumber1(true),
                [
                    'question' => $question1['question'],
                    'options'  => json_encode($this->formatOptions($question1['answers'])),
                    'answers'  => json_encode(['Value A1']),
                    'settings' => json_encode($question1['settings'])
                ]
            ],
            'question number 2 without shuffle' => [
                $question2 = $this->questionNumber2(),
                [
                    'question' => $question2['question'],
                    'options'  => json_encode($this->formatOptions($question2['answers'])),
                    'answers'  => json_encode(['Value B2']),
                    'settings' => json_encode(['shuffle' => false])
                ]
            ],
            'question number 3 with shuffle' => [
                $question3 = $this->questionNumber3(true),
                [
                    'question' => $question3['question'],
                    'options'  => json_encode($this->formatOptions($question3['answers'])),
                    'answers'  => json_encode(['Value C3']),
                    'settings' => json_encode($question3['settings'])
                ]
            ],
            'question number 4 without shuffle' => [
                $question4 = $this->questionNumber4(),
                [
                    'question' => $question4['question'],
                    'options'  => json_encode($this->formatOptions($question4['answers'])),
                    'answers'  => json_encode(['Value D4']),
                    'settings' => json_encode(['shuffle' => false])
                ]
            ]
        ];
    }

    /**
     * Test create new pre-orientation quiz record
     *
     * @dataProvider fourQuestionsDataProvider
     * @group   pre_orientations
     * @group   eloquents
     * 
     * @param   array   $input
     * @param   array   $expectation
     * @return  void
     */
    public function test_create_a_new_pre_orientation_quiz_with_shuffle_options(array $input, array $expectation): void
    {
        $model = new PreOrientationQuiz();
        $preOrientation = PreOrientation::factory()->create();

        $model = $model->create([
            PreOrientationQuizEntity::FIELD_PREORIENTATION => $preOrientation->id,
            PreOrientationQuizEntity::FIELD_QUESTION => $input['question'],
            PreOrientationQuizEntity::FIELD_OPTIONS => $input['answers'],
            PreOrientationQuizEntity::FIELD_SETTINGS => array_key_exists('settings', $input) ? $input['settings'] : []
        ]);

        $this->assertDatabaseHas($model->getTable(), array_merge([
            PreOrientationQuizEntity::FIELD_PREORIENTATION => $preOrientation->id,
        ], $expectation));
    }

    /**
     * Test 'getShuffledOptionsAttribute' method
     * 
     * @group   pre_orientations
     * @group   eloquents
     * 
     * @return void
     */
    public function test_get_pre_orientation_quiz_shuffled_options(): void
    {
        $input = $this->questionNumber1(true);
        $model = new PreOrientationQuiz();
        $preOrientation = PreOrientation::factory()->create();

        $model = $model->create([
            PreOrientationQuizEntity::FIELD_PREORIENTATION => $preOrientation->id,
            PreOrientationQuizEntity::FIELD_QUESTION => $input['question'],
            PreOrientationQuizEntity::FIELD_OPTIONS => $input['answers'],
            PreOrientationQuizEntity::FIELD_SETTINGS => $input['settings']
        ]);

        $this->assertSame(json_encode($this->formatOptions($input['answers'])), $model->options->toJson());
        $this->assertNotSame(json_encode($this->formatOptions($input['answers'])), $model->shuffledOptions->toJson());
        $this->assertNotSame($model->options->toArray(), $model->shuffledOptions->toArray());
    }

    /**
     * Test 'scopePreOrientation' & 'preOrientation' methods
     *
     * @group   pre_orientations
     * @group   eloquents
     * 
     * @return void
     */
    public function test_get_pre_orientation_quiz_that_belongs_to_pre_orientation(): void
    {
        $preOrientation = PreOrientation::factory()->create();
        PreOrientationQuiz::factory(2)->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation->id
        ]);

        $entities = PreOrientationQuiz::select(
            'id', PreOrientationQuiz::FIELD_PREORIENTATION
        )->preOrientation($preOrientation->id)->get();

        $this->assertCount(2, $entities);
        $entities->each(function ($entity) {
            $this->assertInstanceOf(PreOrientation::class, $entity->preOrientation);
        });
    }
}
