<?php

namespace Tests\Unit\Models;

use App\Models\Resource;
use App\ValueObjects\Faq;
use App\ValueObjects\Resources\File;
use App\ValueObjects\Resources\Link;
use App\ValueObjects\Resources\Video;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DataProviders\ContentDataProvider;
use Tests\TestCase;

class ResourceModelTest extends TestCase
{
    use ContentDataProvider;
    use RefreshDatabase;
    use WithFaker;

    /**
     * Get resource input by category
     *
     * @return array
     */
    public function resourceCategoryDataProvider(): array
    {
        return [
            'create visible resource faq' => [
                Faq::class,
                [
                    Resource::FIELD_TITLE => null,
                    Resource::FIELD_DESCRIPTION => null,
                    Resource::FIELD_CATEGORY => Resource::CATEGORY_FAQ,
                    Resource::FIELD_VALUES => [
                        'question' => 'Just a simple question?',
                        'answer' => trim($this->paragraphElement()),
                    ]
                ]
            ],
            'create visible resource video from external link' => [
                Video::class,
                [
                    Resource::FIELD_TITLE => 'Youtube Video',
                    Resource::FIELD_DESCRIPTION => 'Video from external link',
                    Resource::FIELD_CATEGORY => Resource::CATEGORY_VIDEO,
                    Resource::FIELD_VALUES => [
                        'source' => 'https://youtube.com/v=1234abcd',
                    ]
                ]
            ],
            'create visible resource video file' => [
                Video::class,
                [
                    Resource::FIELD_TITLE => 'Local Video',
                    Resource::FIELD_DESCRIPTION => 'Video from local computer',
                    Resource::FIELD_CATEGORY => Resource::CATEGORY_VIDEO,
                    Resource::FIELD_VALUES => [
                        'source' => 'path/to/file.mp4',
                    ]
                ]
            ],
            'create visible resource file' => [
                File::class,
                [
                    Resource::FIELD_TITLE => 'File',
                    Resource::FIELD_DESCRIPTION => 'Video file',
                    Resource::FIELD_CATEGORY => Resource::CATEGORY_FILE,
                    Resource::FIELD_VALUES => [
                        'name' => 'File',
                        'source' => 'path/to/file.mp4',
                    ]
                ]
            ],
            'create visible resource link' => [
                Link::class,
                [
                    Resource::FIELD_TITLE => 'External Link',
                    Resource::FIELD_DESCRIPTION => 'From external',
                    Resource::FIELD_CATEGORY => Resource::CATEGORY_LINK,
                    Resource::FIELD_VALUES => [
                        'text' => 'link',
                        'source' => 'https://external.com',
                    ]
                ]
            ],
        ];
    }

    /**
     * Test create resource model instance for specific category
     *
     * @dataProvider resourceCategoryDataProvider
     * @group   resources
     * @group   eloquents
     * 
     * @param   string  $valueObject
     * @param   array   $attributes
     * @return  void
     */
    public function test_can_create_new_model_base_on_category(string $valueObject, array $attributes): void
    {
        $entity = new Resource();

        $entity = $entity->create($attributes);

        $this->assertDatabaseHas($entity->getTable(), [
            Resource::FIELD_TITLE => $attributes[Resource::FIELD_TITLE],
            Resource::FIELD_DESCRIPTION => $attributes[Resource::FIELD_DESCRIPTION],
            Resource::FIELD_CATEGORY => $attributes[Resource::FIELD_CATEGORY],
            Resource::FIELD_VALUES => json_encode($attributes[Resource::FIELD_VALUES])
        ]);
        $this->assertTrue($entity->visible);
        $this->assertInstanceOf($valueObject, $entity->values);
        $this->assertEquals($attributes[Resource::FIELD_TITLE], $entity->title);
        $this->assertEquals($attributes[Resource::FIELD_DESCRIPTION], $entity->description);
        $this->assertEquals($attributes[Resource::FIELD_CATEGORY], $entity->category);
    }

    /**
     * Get resource categories
     *
     * @return array
     */
    public function categoryDataProvider(): array
    {
        return [
            'category faq' => [Resource::CATEGORY_FAQ],
            'category file' => [Resource::CATEGORY_FILE],
            'category link' => [Resource::CATEGORY_LINK],
            'category video' => [Resource::CATEGORY_VIDEO],
        ];
    }

    /**
     * Test 'scopeCategory' method
     *
     * @dataProvider categoryDataProvider
     * @group   resources
     * @group   eloquents
     * 
     * @param   string  $category
     * @return  void
     */
    public function test_can_get_the_resources_base_on_category(string $category): void
    {
        $entity = Resource::factory()->create([
            Resource::FIELD_CATEGORY => $category
        ]);

        $entity = Resource::select(Resource::FIELD_CATEGORY)->category($category)->first();

        $this->assertEquals($category, $entity->category);
    }

    /**
     * Test 'scopeVisible' method
     *
     * @return void
     */
    public function test_can_get_resource_that_visible(): void
    {
        // By default it will create 'visible' resource
        $entity = Resource::factory()->create();

        $entity = Resource::select(Resource::FIELD_VISIBLE)->visible()->first();

        $this->assertTrue($entity->visible);
    }

    /**
     * Test 'scopeInvisible' method
     *
     * @return void
     */
    public function test_can_get_resource_that_invisible(): void
    {
        $entity = Resource::factory()->create([
            Resource::FIELD_VISIBLE => false
        ]);

        $entity = Resource::select(Resource::FIELD_VISIBLE)->invisible()->first();

        $this->assertFalse($entity->visible);
    }
}
