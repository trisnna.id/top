<?php

namespace Tests\Unit\Models;

use App\Contracts\Entities\HasUuid;
use App\Contracts\Entities\PreOrientationEntity;
use App\Models\Checkpoint;
use App\Models\PreOrientation;
use App\Models\PreOrientationQuiz;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DataProviders\ContentDataProvider;
use Tests\TestCase;

class PreOrientationModelTest extends TestCase
{
    use ContentDataProvider;
    use RefreshDatabase;
    use WithFaker;

    /**
     * Test the pre-orientation model should implements 'UUID' field
     *
     * @group   pre_orientations
     * 
     * @return  void
     */
    public function test_pre_orientation_should_have_uuid_field(): void
    {
        $checkpoint = Checkpoint::factory()->create();
        $content = $this->paragraphElement();
        $localities = ['1', '2'];
        $entity = [
            PreOrientation::FIELD_ACTIVITY_NAME => 'Binggo Game',
            PreOrientation::FIELD_CATEGORY => $checkpoint->id,
            PreOrientation::FIELD_EFFECTIVE_DATE => $this->faker->date('Y-m-d'),
            PreOrientation::FIELD_LOCALITIES => $localities,
            PreOrientation::FIELD_CONTENT => $content,
        ];
        $model = new PreOrientation();

        $model = $model->create($entity);

        $this->assertInstanceOf(HasUuid::class, $model);
        $this->assertDatabaseHas($model->getTable(), array_merge($entity, [
            PreOrientation::FIELD_CONTENT_LINKS => "[]",
            PreOrientation::FIELD_LOCALITIES => json_encode($localities),
        ]));
        $this->assertIsString($model->uuid);
    }

    /**
     * Test the pre-orientation status should be 'draft' if there's
     * no status value specified
     *
     * @depends test_pre_orientation_should_have_uuid_field
     * @group   pre_orientations
     * 
     * @return  void
     */
    public function test_pre_orientation_should_be_stored_as_draft(): void
    {
        $checkpoint = Checkpoint::factory()->create();
        $localities = ['1', '2'];
        $content = $this->contentWithoutLinks();
        $entity = [
            PreOrientation::FIELD_ACTIVITY_NAME => 'Binggo Game',
            PreOrientation::FIELD_CATEGORY => $checkpoint->id,
            PreOrientation::FIELD_EFFECTIVE_DATE => $this->faker->date('Y-m-d'),
            PreOrientation::FIELD_LOCALITIES => $localities,
            PreOrientation::FIELD_CONTENT => $content,
        ];
        $model = new PreOrientation();

        $model = $model->create($entity);

        $this->assertDatabaseHas($model->getTable(), array_merge($entity, [
            PreOrientation::FIELD_CONTENT_LINKS => "[]",
            PreOrientation::FIELD_LOCALITIES => json_encode($localities),
        ]));
        $this->assertSame($model->status, PreOrientation::STATUS_DRAFT);
    }

    /**
     * Test the pre-orientation `content_links` attribute will contains
     * a list of content links array
     * 
     * @group   pre_orientations
     *
     * @return  void
     */
    public function test_pre_orienatation_record_should_have_the_content_links(): void
    {
        $checkpoint = Checkpoint::factory()->create();
        $localities = ['1', '2'];
        $content = $this->contentFull();
        $entity = [
            PreOrientation::FIELD_ACTIVITY_NAME => 'Binggo Game',
            PreOrientation::FIELD_CATEGORY => $checkpoint->id,
            PreOrientation::FIELD_EFFECTIVE_DATE => $this->faker->date('Y-m-d'),
            PreOrientation::FIELD_LOCALITIES => $localities,
            PreOrientation::FIELD_CONTENT => $content,
        ];
        $model = new PreOrientation();

        $model = $model->create($entity);

        $this->assertDatabaseHas($model->getTable(), array_merge($entity, [
            PreOrientation::FIELD_CONTENT_LINKS => json_encode($this->linksExpectation()),
            PreOrientation::FIELD_LOCALITIES => json_encode($localities),
        ]));
        $this->assertSame($model->status, PreOrientation::STATUS_DRAFT);
    }

    /**
     * Test `scopeName` method
     *
     * @group   pre_orientations
     * 
     * @return  void
     */
    public function test_can_query_pre_orientation_table_scoped_by_name(): void
    {
        PreOrientation::factory()->create([
            PreOrientation::FIELD_ACTIVITY_NAME => 'Binggo Game',
        ]);

        $model = PreOrientation::name('Binggo Game')->first();

        $this->assertSame($model->activity_name, 'Binggo Game');
    }

    /**
     * Get status 'inputs'
     *
     * @return array
     */
    public function statusDataProvider(): array
    {
        return [
            'status draft' => [PreOrientationEntity::STATUS_DRAFT], 
            'status published' => [PreOrientationEntity::STATUS_PUBLISHED], 
            'status unpublished' => [PreOrientationEntity::STATUS_UNPUBLISHED], 
        ];
    }

    /**
     * Test `scopeStatus` method
     *
     * @dataProvider    statusDataProvider
     * @group           pre_orientation
     * 
     * @param   string  $status
     * @return  void
     */
    public function test_can_query_pre_orientation_table_scoped_by_status(string $status): void
    {
        PreOrientation::factory()->create([
            PreOrientation::FIELD_STATUS => $status
        ]);

        $model = PreOrientation::status($status)->first();

        $this->assertSame($model->status, $status);
    }

    /**
     * Test `scopeDraft` method
     *
     * @group   pre_orientations
     * 
     * @return  void
     */
    public function test_can_query_pre_orientation_table_scoped_by_status_draft(): void
    {
        PreOrientation::factory()->create([
            PreOrientation::FIELD_STATUS => PreOrientation::STATUS_DRAFT
        ]);

        $model = PreOrientation::draft()->first();

        $this->assertSame($model->status, PreOrientation::STATUS_DRAFT);
    }

    /**
     * Test `scopePublished` method
     *
     * @group   pre_orientations
     * @return  void
     */
    public function test_can_query_pre_orientation_table_scoped_by_status_published(): void
    {
        PreOrientation::factory()->create([
            PreOrientation::FIELD_STATUS => PreOrientation::STATUS_PUBLISHED
        ]);

        $model = PreOrientation::published()->first();

        $this->assertSame($model->status, PreOrientation::STATUS_PUBLISHED);
    }

    /**
     * Test `scopeUnpublished` method
     *
     * @group   pre_orientations
     * @return  void
     */
    public function test_can_query_pre_orientation_table_scoped_by_status_unpublished(): void
    {
        PreOrientation::factory()->create([
            PreOrientation::FIELD_STATUS => PreOrientation::STATUS_UNPUBLISHED
        ]);

        $model = PreOrientation::unpublished()->first();

        $this->assertSame($model->status, PreOrientation::STATUS_UNPUBLISHED);
    }

    /**
     * Test the pre-orientation should belongs to checkpoint
     *
     * @group   pre_orientations
     * @return  void
     */
    public function test_can_get_pre_orientation_checkpoint():void
    {
        $model = PreOrientation::factory()->create();

        $this->assertInstanceOf(Checkpoint::class, $model->checkpoint);
    }

    /**
     * Test the pre-orientation should has many to quizses
     *
     * @group   pre_orientations
     * @return  void
     */
    public function test_can_get_pre_orientation_quisz():void
    {
        $model = PreOrientation::factory()->create();
        $quiz = PreOrientationQuiz::factory()->make([
            PreOrientationQuiz::FIELD_PREORIENTATION => $model->id
        ]);
        $model->quizs()->create($quiz->toArray());

        $entity = PreOrientation::find($model->id);
        $entityQuiz = $entity->quizs->first();

        $this->assertInstanceOf(Collection::class, $entity->quizs);
        $this->assertInstanceOf(PreOrientationQuiz::class, $entityQuiz);
        $this->assertSame($quiz->question, $entityQuiz->question);
    }
}
