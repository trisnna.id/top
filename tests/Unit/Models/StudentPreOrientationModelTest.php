<?php

namespace Tests\Unit\Models;

use App\Models\PreOrientation;
use App\Models\PreOrientationQuiz;
use App\Models\Student;
use App\Models\StudentPreOrientation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DataProviders\QuizDataProvider;
use Tests\TestCase;

class StudentPreOrientationModelTest extends TestCase
{
    use RefreshDatabase;
    use QuizDataProvider;

    /**
     * A new record will be stored when we call 'create' method.
     * The newly created record will be marked as 'incomplete'
     * by default
     *
     * @return void
     */
    public function test_it_should_store_student_preorientation_quiz_record(): void
    {
        $student = Student::factory()->create();
        $quiz = PreOrientationQuiz::factory()->create();
        $entity = StudentPreOrientation::factory()->make();
        $answers = $quiz->options->filter(function ($item) {
            return $item->markAsAnswer;
        })->map(function ($item) {
            return $item->value;
        })->values()->toArray();

        $this->actingAs($student->user);
        $entity = $entity->create([
            StudentPreOrientation::FIELD_PREORIENTATION => $quiz->preOrientation->id,
            StudentPreOrientation::FIELD_QUIZ => [
                ['quiz_id' => $quiz->id, 'answers' => $answers]
            ],
        ]);

        $this->assertDatabaseHas(StudentPreOrientation::TABLE, [
            StudentPreOrientation::FIELD_PREORIENTATION => $quiz->preOrientation->id,
            StudentPreOrientation::FIELD_STUDENT => $entity->student_id,
            StudentPreOrientation::FIELD_COMPLETED => 0,
            StudentPreOrientation::FIELD_SCORE => 100,
            StudentPreOrientation::FIELD_QUIZ => json_encode([
                ['quiz_id' => $quiz->id, 'answers' => $answers]
            ]),
        ]);
        $this->assertSame($answers, $entity->quizzes->get(0)['answers']);
    }


    public function test_it_should_create_record_with_score_value_is_60(): void
    {
        $student = Student::factory()->create();
        $preOrientation = PreOrientation::factory()->create();
        $quizzes = PreOrientationQuiz::factory(5)->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation->id
        ]);
        $entity = StudentPreOrientation::factory()->make();
        $answers = $quizzes->transform(function ($quiz, $i) {
            return [
                'quiz_id' => $quiz->id,
                'answers' => $i < 2 ? [] : $quiz->answers->toArray()
            ];
        });

        $this->actingAs($student->user);
        $entity = $entity->create([
            StudentPreOrientation::FIELD_PREORIENTATION => $preOrientation->id,
            StudentPreOrientation::FIELD_QUIZ => $answers,
        ]);

        $this->assertDatabaseHas(StudentPreOrientation::TABLE, [
            StudentPreOrientation::FIELD_PREORIENTATION => $preOrientation->id,
            StudentPreOrientation::FIELD_STUDENT => $entity->student_id,
            StudentPreOrientation::FIELD_COMPLETED => 0,
            StudentPreOrientation::FIELD_SCORE => 60,
            StudentPreOrientation::FIELD_QUIZ => json_encode($answers),
        ]);
    }
}
