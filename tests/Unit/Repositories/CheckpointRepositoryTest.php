<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Entities\CheckpointEntity;
use App\Models\Checkpoint;
use App\Repositories\Contracts\CheckpointRepository;
use App\Repositories\Eloquent\EloquentCheckpointRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CheckpointRepositoryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * The pre-orientation quiz repository instance
     *
     * @var EloquentCheckpointRepository
     */
    private EloquentCheckpointRepository $repository;

    /**
     * {@override}
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = app()->make(CheckpointRepository::class);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_get_all_checkpoints_and_select_only_id_label_value(): void
    {
        Checkpoint::factory(2)->create();

        $results = $this->repository->allOnlyLabelValue();

        $this->assertInstanceOf(Collection::class, $results);
        $this->assertCount(2, $results);
        $results->each(function (CheckpointEntity $entity) {
            $entities = $entity->toArray();
            $this->assertArrayNotHasKey(CheckpointEntity::FIELD_UUID, $entities);
            $this->assertArrayNotHasKey('created_at', $entities);
            $this->assertArrayNotHasKey('updated_at', $entities);
            $this->assertArrayNotHasKey('updated_at', $entities);
        });
    }
}
