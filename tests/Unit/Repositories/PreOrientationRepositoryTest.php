<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Entities\PreOrientationEntity;
use App\Models\Checkpoint;
use App\Models\PreOrientation;
use App\Repositories\Contracts\PreOrientationRepository;
use App\Repositories\Eloquent\EloquentPreOrientationRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DataProviders\ContentDataProvider;
use Tests\TestCase;

class PreOrientationRepositoryTest extends TestCase
{
    use ContentDataProvider;
    use RefreshDatabase;
    use WithFaker;

    private EloquentPreOrientationRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = app()->make(PreOrientationRepository::class);
    }

    /**
     * Test 'create' method
     *
     * @group   repositories
     * @group   pre_orientation
     * 
     * @return void
     */
    public function test_create_a_new_pre_orientation_record(): void
    {
        $checkpoint = Checkpoint::factory()->create();
        PreOrientation::factory()->create([PreOrientation::FIELD_CATEGORY => $checkpoint->id]);

        $entity = $this->repository->create([
            PreOrientationEntity::FIELD_ACTIVITY_NAME => $activityName = $this->faker->colorName,
            PreOrientationEntity::FIELD_CATEGORY => $checkpoint->id,
            PreOrientationEntity::FIELD_LOCALITIES => $localities = ['1'],
            PreOrientationEntity::FIELD_EFFECTIVE_DATE => $effectiveDate = $this->faker->date('Y-m-d'),
            PreOrientationEntity::FIELD_CONTENT => $content = $this->contentFull(),
            PreOrientationEntity::FIELD_CONTENT_LINKS => $content,
        ]);

        $this->assertInstanceOf(PreOrientationEntity::class, $entity);
        $this->assertIsInt($entity->id);
        $this->assertIsString($entity->uuid);
        $this->assertSame($activityName, $entity->activity_name);
        $this->assertSame(PreOrientationEntity::STATUS_DRAFT, $entity->status);
        $this->assertSame($checkpoint->id, $entity->checkpoint_id);
        $this->assertSame($localities, $entity->localities);
        $this->assertSame($effectiveDate, $entity->effective_date);
        $this->assertSame($content, $entity->content);
        $this->assertSame($this->linksExpectation(), $entity->content_links->toArray());
    }

    /**
     * Test 'findByCheckpoint' method
     *
     * @group   repositories
     * @group   pre_orientation
     * 
     * @return void
     */
    public function test_find_pre_orientation_by_checkpoint_id(): void
    {
        $checkpoint = Checkpoint::factory()->create();
        PreOrientation::factory()->create([PreOrientation::FIELD_CATEGORY => $checkpoint->id]);

        $entities = $this->repository->findByCheckpoint($checkpoint->id);

        $this->assertInstanceOf(Collection::class, $entities);
        $this->assertCount(1, $entities);
        $this->assertSame($checkpoint->id, $entities->get(0)->checkpoint->id);
    }

    /**
     * Test 'publish' method
     *
     * @group repositories
     * @group pre_orientations
     * 
     * @return  void
     */
    public function test_can_publish_pre_orientation(): void
    {
        $entity = PreOrientation::factory()->create();

        $entity = $this->repository->publish($entity->uuid);

        $this->assertInstanceOf(PreOrientation::class, $entity);
        $this->assertEquals(PreOrientation::STATUS_PUBLISHED, $entity->status);
        $this->assertDatabaseHas($entity->getTable(), [
            'uuid' => $entity->uuid,
            'status' => PreOrientation::STATUS_PUBLISHED
        ]);
        $this->assertDatabaseMissing($entity->getTable(), [
            'uuid' => $entity->uuid,
            'status' => PreOrientation::STATUS_DRAFT
        ]);
    }

    /**
     * Test 'publish' method
     *
     * @group repositories
     * @group pre_orientations
     * 
     * @return  void
     */
    public function test_cannot_publish_pre_orientation_because_it_cannot_be_found(): void
    {
        $entity = $this->repository->publish('');

        $this->assertNull($entity);
    }
}
