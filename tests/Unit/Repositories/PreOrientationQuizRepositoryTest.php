<?php

namespace Tests\Unit\Repositories;

use App\Contracts\Entities\PreOrientationQuizEntity;
use App\Models\PreOrientation;
use App\Models\PreOrientationQuiz;
use App\Repositories\Contracts\PreOrientationQuizRepository;
use App\Repositories\Eloquent\EloquentPreOrientationQuizRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DataProviders\QuizDataProvider;
use Tests\TestCase;

class PreOrientationQuizRepositoryTest extends TestCase
{
    use QuizDataProvider;
    use RefreshDatabase;

    /**
     * The pre-orientation quiz repository instance
     *
     * @var EloquentPreOrientationQuizRepository
     */
    private EloquentPreOrientationQuizRepository $repository;

    /**
     * {@override}
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = app()->make(PreOrientationQuizRepository::class);
    }

    /**
     * Test 'deleteByPreOrientation' method
     * 
     * @group   eloquents
     * @group   pre_orientations
     * @group   repositories
     *
     * @return  void
     */
    public function test_can_delete_quiz_by_pre_orientation_id(): void
    {
        $preOrientation = PreOrientation::factory()->create();
        PreOrientationQuiz::factory(5)->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation
        ]);

        $deleted = $this->repository->deleteByPreOrientation($preOrientation->id);

        $this->assertTrue($deleted);
        $this->assertDatabaseEmpty('pre_orientation_quizs');
    }

    /**
     * Test 'createMany' method
     * 
     * @depends test_can_find_records_by_pre_orientation_id
     * @group   eloquents
     * @group   pre_orientations
     * @group   repositories
     *
     * @return  void
     */
    public function test_can_create_many_quiz(): void
    {
        $preOrientation = PreOrientation::factory()->create();
        $mcq = $this->allQuestions();

        $results = $this->repository->createMany($preOrientation->id, $mcq);

        $this->assertDatabaseCount('pre_orientation_quizs', count($mcq));
        $results->each(function (PreOrientationQuizEntity $entity, int $index) use ($preOrientation, $mcq) {
            $this->assertDatabaseHas($entity->getTable(), [
                $entity::FIELD_PREORIENTATION => $preOrientation->id,
                $entity::FIELD_QUESTION => $mcq[$index]['question'],
                $entity::FIELD_OPTIONS => json_encode($this->formatOptions($mcq[$index]['answers'])),
                $entity::FIELD_ANSWERS => json_encode($this->formatCorrectAnswers($mcq[$index]['answers'])),
                $entity::FIELD_SETTINGS => json_encode($mcq[$index]['settings']),
            ]);
        });
    }

    /**
     * Test 'findByPreorinetation' method
     * 
     * @group   pre_orientation
     * @group   repositories
     *
     * @return  void
     */
    public function test_can_find_records_by_pre_orientation_id(): void
    {
        $preOrientation = PreOrientation::factory()->create();
        PreOrientationQuiz::factory()->create([
            PreOrientationQuizEntity::FIELD_PREORIENTATION => $preOrientation
        ]);

        $results = $this->repository->findByPreOrientation($preOrientation->id);

        $this->assertInstanceOf(Collection::class, $results);
        $this->assertCount(1, $results);
        $this->assertEquals($preOrientation->id, $results->get(0)->pre_orientation_id);
    }

    /**
     * Test 'upsertOrDeleteByPreOrientation' method
     * 
     * @group   pre_orientation
     * @group   repositories
     *
     * @return void
     */
    public function test_it_should_update_existing_quiz_attributes(): void
    {
        $inputQuiz = $this->questionNumber2(true);
        $preOrientation = PreOrientation::factory()->create();
        $preOrientationQuiz = PreOrientationQuiz::factory()->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation,
            PreOrientationQuiz::FIELD_SETTINGS => [
                PreOrientationQuizEntity::SETTINGS_SHUFFLE => false
            ]
        ]);

        $updated = $this->repository->upsertOrDeleteByPreOrientation(
            $preOrientation->id,
            [array_merge(['quiz_id' => $preOrientationQuiz->id], $inputQuiz)]
        );

        $this->assertTrue($updated);
        $this->assertDatabaseHas($preOrientationQuiz->getTable(), [
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation->id, 
            PreOrientationQuiz::FIELD_QUESTION => $inputQuiz['question'], 
            PreOrientationQuiz::FIELD_OPTIONS => json_encode($this->formatOptions($inputQuiz['answers'])), 
            PreOrientationQuiz::FIELD_ANSWERS => json_encode($this->formatCorrectAnswers($inputQuiz['answers'])),
            PreOrientationQuiz::FIELD_SETTINGS => json_encode($inputQuiz['settings']),
        ]);
        $this->assertDatabaseMissing($preOrientationQuiz->getTable(), [
            PreOrientationQuiz::FIELD_QUESTION => $preOrientationQuiz->question, 
            PreOrientationQuiz::FIELD_OPTIONS => json_encode($preOrientationQuiz->options), 
            PreOrientationQuiz::FIELD_ANSWERS => json_encode($preOrientationQuiz->answers),
            PreOrientationQuiz::FIELD_SETTINGS => json_encode($preOrientationQuiz->settings),
        ]);
    }

    /**
     * Test 'upsertOrDeleteByPreOrientation' method
     * 
     * @group   pre_orientation
     * @group   repositories
     *
     * @return void
     */
    public function test_it_should_delete_existing_quiz_attributes_if_the_input_attributes_empty(): void
    {
        $preOrientation = PreOrientation::factory()->create();
        $preOrientationQuiz = PreOrientationQuiz::factory()->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation,
            PreOrientationQuiz::FIELD_SETTINGS => [
                PreOrientationQuizEntity::SETTINGS_SHUFFLE => false
            ]
        ]);

        $updated = $this->repository->upsertOrDeleteByPreOrientation(
            $preOrientation->id,
            [array_merge(['quiz_id' => $preOrientationQuiz->id], [])]
        );

        $this->assertTrue($updated);
        $this->assertDatabaseMissing($preOrientationQuiz->getTable(), [
            PreOrientationQuiz::FIELD_QUESTION => $preOrientationQuiz->question, 
            PreOrientationQuiz::FIELD_OPTIONS => json_encode($preOrientationQuiz->options), 
            PreOrientationQuiz::FIELD_ANSWERS => json_encode($preOrientationQuiz->answers),
            PreOrientationQuiz::FIELD_SETTINGS => json_encode($preOrientationQuiz->settings),
        ]);
        $this->assertDatabaseEmpty($preOrientationQuiz->getTable());
    }

    /**
     * Test 'upsertOrDeleteByPreOrientation' method
     * 
     * @group   pre_orientation
     * @group   repositories
     *
     * @return void
     */
    public function test_it_should_delete_all_existing_quiz_that_belong_to_pre_orientation_if_the_mcq_list_is_empty(): void
    {
        $preOrientation = PreOrientation::factory()->create();
        $preOrientationQuiz = PreOrientationQuiz::factory()->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation,
            PreOrientationQuiz::FIELD_SETTINGS => [
                PreOrientationQuizEntity::SETTINGS_SHUFFLE => false
            ]
        ]);

        $updated = $this->repository->upsertOrDeleteByPreOrientation(
            $preOrientation->id,
            []
        );

        $this->assertFalse($updated);
        $this->assertDatabaseMissing($preOrientationQuiz->getTable(), [
            PreOrientationQuiz::FIELD_QUESTION => $preOrientationQuiz->question, 
            PreOrientationQuiz::FIELD_OPTIONS => json_encode($preOrientationQuiz->options), 
            PreOrientationQuiz::FIELD_ANSWERS => json_encode($preOrientationQuiz->answers),
            PreOrientationQuiz::FIELD_SETTINGS => json_encode($preOrientationQuiz->settings),
        ]);
        $this->assertDatabaseEmpty($preOrientationQuiz->getTable());
    }

    /**
     * Test 'upsertOrDeleteByPreOrientation' method
     * 
     * @group   pre_orientation
     * @group   repositories
     *
     * @return void
     */
    public function test_it_should_create_new_record_if_the_attributes_are_not_exist_in_the_table(): void
    {
        $inputQuiz = $this->questionNumber2(true);
        $preOrientation = PreOrientation::factory()->create();
        $preOrientationQuiz = PreOrientationQuiz::factory()->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation,
            PreOrientationQuiz::FIELD_SETTINGS => [
                PreOrientationQuizEntity::SETTINGS_SHUFFLE => false
            ]
        ]);

        $updated = $this->repository->upsertOrDeleteByPreOrientation(
            $preOrientation->id,
            [$inputQuiz]
        );

        $this->assertTrue($updated);
        $this->assertDatabaseCount($preOrientationQuiz->getTable(), 2);
        $this->assertDatabaseHas($preOrientationQuiz->getTable(), [
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation->id,
            PreOrientationQuiz::FIELD_QUESTION => $inputQuiz['question'], 
            PreOrientationQuiz::FIELD_OPTIONS => json_encode($this->formatOptions($inputQuiz['answers'])), 
            PreOrientationQuiz::FIELD_ANSWERS => json_encode($this->formatCorrectAnswers($inputQuiz['answers'])),
            PreOrientationQuiz::FIELD_SETTINGS => json_encode($inputQuiz['settings']),
        ]);
        $this->assertDatabaseHas($preOrientationQuiz->getTable(), [
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation->id,
            PreOrientationQuiz::FIELD_QUESTION => $preOrientationQuiz->question, 
            PreOrientationQuiz::FIELD_OPTIONS => json_encode($preOrientationQuiz->options), 
            PreOrientationQuiz::FIELD_ANSWERS => json_encode($preOrientationQuiz->answers),
            PreOrientationQuiz::FIELD_SETTINGS => json_encode($preOrientationQuiz->settings),
        ]);
    }
}
