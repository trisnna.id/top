<?php

namespace Tests\Unit\Casts;

use App\Casts\Json;
use App\Contracts\Entities\PreOrientationEntity;
use App\Contracts\Entities\PreOrientationQuizEntity;
use App\Models\PreOrientation;
use App\Models\PreOrientationQuiz;
use App\Models\Resource;
use App\ValueObjects\ContentLink;
use App\ValueObjects\Faq;
use App\ValueObjects\McqOption;
use App\ValueObjects\Resources\File;
use App\ValueObjects\Resources\Video;
use Illuminate\Support\Collection;
use Tests\DataProviders\ContentDataProvider;
use Tests\DataProviders\QuizDataProvider;
use Tests\TestCase;

class JsonTest extends TestCase
{
    use ContentDataProvider;
    use QuizDataProvider;

    /**
     * Get pre-orientation 'content_links' inputs
     *
     * @return array
     */
    public function preOrientationContentLinksDataProvider(): array
    {
        return [
            'pre-orientation content links is not empty' => [
                $this->contentFull(),
                json_encode($this->linksExpectation()),
                2
            ],
            'pre-orientation content links is empty' => [
                $this->contentWithoutLinks(),
                json_encode([]),
                0,
            ]
        ];
    }

    /**
     * Test casting the pre-orientation `content_links` value
     *
     * @dataProvider    preOrientationContentLinksDataProvider
     * @group           eloquent
     * @group           pre_orientation
     * 
     * @param   string  $content
     * @param   string  $setterExpectation
     * @param   int     $countGetterExpectation
     * @return  void
     */
    public function test_can_casting_pre_orientation_content_links(string $content, string $setterExpectation, int $countGetterExpectation): void
    {
        $jsonCast = new Json();
        $model = new PreOrientation();

        $setter = $jsonCast->set($model, PreOrientationEntity::FIELD_CONTENT_LINKS, $content, []);
        $getter = $jsonCast->get($model, PreOrientationEntity::FIELD_CONTENT_LINKS, $setter, []);

        $this->assertEquals($setterExpectation, $setter);
        $this->assertInstanceOf(Collection::class, $getter);
        $this->assertCount($countGetterExpectation, $getter);
        $getter->each(function ($item) {
            $this->assertInstanceOf(ContentLink::class, $item);
        });
    }

    /**
     * Get pre-orientation input attributes
     *
     * @return array
     */
    public function preOrientationQuizAttributesDataProvider(): array
    {
        $quiz = $this->questionNumber1(true);

        return [
            'pre-orientation quiz options' => [
                PreOrientationQuizEntity::FIELD_OPTIONS,
                $quiz['answers'],
                'assertNotEquals',
                4
            ],
            'pre-orientation quiz settings' => [
                PreOrientationQuizEntity::FIELD_SETTINGS,
                $quiz['settings'],
                'assertEquals',
                1
            ]
        ];
    }

    /**
     * Test casting the pre-orientation quiz attributes
     *
     * @dataProvider    preOrientationQuizAttributesDataProvider
     * @group           eloquent
     * @group           pre_orientation
     * 
     * @param   string  $field
     * @param   array   $values
     * @param   string  $assertion
     * @param   int     $countGetterExpectation
     * @return  void
     */
    public function test_can_casting_pre_orientation_quiz_attributes(string $field, array $values, string $assertion, int $countGetterExpectation): void
    {
        $jsonCast = new Json();
        $model = new PreOrientationQuiz();

        $setter = $jsonCast->set($model, $field, $values, []);
        $getter = $jsonCast->get($model, $field, $setter, []);

        $this->assertJson($setter);
        $this->{$assertion}(json_encode($values), $setter);
        $this->assertInstanceOf(Collection::class, $getter);
        $this->assertCount($countGetterExpectation, $getter);
        $getter->each(function ($item) {
            $this->assertTrue(is_string($item) ||  is_bool($item) || ($item instanceof McqOption));
        });
    }

    /**
     * Get input data resources
     *
     * @return array
     */
    public function resourceDataProvider(): array
    {
        return [
            'resource faq' => [
                Faq::class,
                Resource::CATEGORY_FAQ,
                [
                    'question' => 'Just a simple faq question?',
                    'answer' => $this->paragraphElement()
                ]
            ],
            'resource video external' => [
                Video::class,
                Resource::CATEGORY_VIDEO,
                [
                    'source' => 'https://youtube.com/v=12345'
                ]
            ],
            'resource video file' => [
                Video::class,
                Resource::CATEGORY_VIDEO,
                [
                    'source' => 'path/to/file.mp4'
                ]
            ],
            'resource file' => [
                File::class,
                Resource::CATEGORY_FILE,
                [
                    'source' => 'path/to/file.mp4'
                ]
            ]
        ];
    }

    /**
     * Test casting resource values
     *
     * @dataProvider resourceDataProvider
     * @group   eloquent
     * @group   resources
     * 
     * @param   string  $instanceClassName
     * @param   string  $category
     * @param   array   $values
     * @return  void
     */
    public function test_can_casting_resource_values(string  $instanceClassName, string $category, array $values): void
    {
        $jsonCast = new Json();
        $model = new Resource();

        $model->category = $category;
        $setter = $jsonCast->set($model, Resource::FIELD_VALUES, $values, []);
        $getter = $jsonCast->get($model, Resource::FIELD_VALUES, $setter, []);

        $this->assertEquals(json_encode($values), $setter);
        $this->assertInstanceOf($instanceClassName, $getter);
    }

    /**
     * Test can casting an array to default json encode/decoded
     *
     * @group   eloquent
     * 
     * @return  void
     */
    public function test_can_casting_to_json(): void
    {
        $model = new \stdClass;
        $jsonCast = new Json();

        $setter = $jsonCast->set($model, 'any_key', [1, 2], []);
        $getter = $jsonCast->get($model, 'any_key', $setter, []);

        $this->assertJson($setter);
        $this->assertIsArray($getter);
    }
}
