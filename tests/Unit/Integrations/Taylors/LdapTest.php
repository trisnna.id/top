<?php

namespace Tests\Unit\Integrations\Taylors;

use App\Services\Integration\Taylors\LdapService;
use PHPUnit\Framework\TestCase;

class LdapTest extends TestCase
{
    public function dataStaff(): array
    {
        return [
            'type' => 'staff',
            'username' => 'oas.staff1',
            'password' => 'oas@123',
        ];
    }

    public function dataStudent(): array
    {
        return [
            'type' => 'student',
            'username' => 'oas.stud1',
            'password' => 'oas@123',
        ];
    }

    /**
     * Test LDAP authentication for student
     */
    public function test_ldap_auth_student()
    {
        $data = $this->dataStudent();
        $this->assertTrue(LdapService::auth($data['type'], $data['username'], $data['password']));
    }

    /**
     * Test LDAP authentication for staff
     */
    public function test_ldap_auth_staff()
    {
        $data = $this->dataStaff();
        $this->assertTrue(LdapService::auth($data['type'], $data['username'], $data['password']));
    }
}
