<?php declare(strict_types=1);

namespace Tests\DataProviders;

trait ContentDataProvider
{
    /**
     * Get input content that contains paragraphs, images, & links
     *
     * @return string
     */
    public function contentFull(): string
    {
        return sprintf(
            "%s\n\n%s\n%s",
            $this->imageElement(),
            $this->paragraphElement(),
            $this->linkElements()
        );
    }

    /**
     * Get input content that contains paragraphs & images
     *
     * @return string
     */
    public function contentWithoutImage(): string
    {
        return sprintf("%s\n\n%s", $this->paragraphElement(), $this->linkElements());
    }

    /**
     * Get input content that contains paragraphs & links
     *
     * @return string
     */
    public function contentWithoutLinks(): string
    {
        return sprintf("%s\n\n%s", $this->imageElement(), $this->paragraphElement());
    }

    /**
     * Get image HTML string
     *
     * @return  string
     */
    public function imageElement(): string
    {
        return '<p><image src="link" /></p>';
    }

    /**
     * Get the assertion expectation for content link
     *
     * @return  array
     */
    public function linksExpectation(): array
    {
        return [
            [
                'text'  => 'Video 1',
                'link'  => 'https://www.youtube.com/watch?v=Yh17zw00sJE',
                'thumbnail' => 'https://img.youtube.com/vi/Yh17zw00sJE/0.jpg',
                'raw'   => '<a href="https://www.youtube.com/watch?v=Yh17zw00sJE">Video 1</a>',
            ],
            [
                'text'  => 'Video 2',
                'link'  => 'https://www.youtube.com/watch?v=A4uWNMme-qM',
                'thumbnail' => 'https://img.youtube.com/vi/A4uWNMme-qM/0.jpg',
                'raw'   => '<a href="https://www.youtube.com/watch?v=A4uWNMme-qM">Video 2</a>',
            ],
        ];
    }

    /**
     * Get anchor HTML string
     *
     * @return  string
     */
    public function linkElements(): string
    {
        return '
            <p><a href="https://www.youtube.com/watch?v=Yh17zw00sJE">Video 1</a></p>
            <p><a href="https://www.youtube.com/watch?v=A4uWNMme-qM">Video 2</a></p>
        ';
    }

    /**
     * Get paragraph HTML string
     *
     * @return  string
     */
    public function paragraphElement(): string
    {
        return '
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Integer venenatis tellus nec ex volutpat luctus.
            Donec ullamcorper pharetra enim, at sollicitudin turpis congue nec.
            Etiam commodo neque odio, ac cursus dolor iaculis at.
            Suspendisse vitae gravida turpis, eget gravida massa.
            Aenean facilisis leo vitae ligula gravida, ut porta nisl scelerisque.
            Vestibulum ultricies orci vel dui ornare consequat. Curabitur eu lacus felis.
            Nunc lectus mi, ultrices vitae orci a, fermentum bibendum turpis.
            Sed porttitor tincidunt dui, eu vestibulum odio luctus ut.</p>
        ';
    }
}
