<?php declare(strict_types=1);

namespace Tests\DataProviders;

trait QuizDataProvider
{
    /**
     * Get all mcq quiz
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function allQuestions(bool $shuffle = true): array
    {
        return [
            $this->questionNumber1($shuffle),
            $this->questionNumber2($shuffle),
            $this->questionNumber3($shuffle),
            $this->questionNumber4($shuffle),
            $this->questionNumber5($shuffle),
            $this->questionNumber6($shuffle),
            $this->questionNumber7($shuffle),
            $this->questionNumber8($shuffle),
            $this->questionNumber9($shuffle),
            $this->questionNumber10($shuffle),
        ];
    }

    /**
     * Get mcq question number 1
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function questionNumber1(bool $shuffle = false): array
    {
        $mcq = [
            'question' => 'Simple question number 1?',
            'answers' => [
                ['a' => 'Value A1', 'mark_as_answer' => 'on'],
                ['b' => 'Value B1',],
                ['c' => 'Value C1',],
                ['d' => 'Value D1',],
            ]
        ];

        if ($shuffle) {
            return array_merge([
                'settings' => [
                    'shuffle' => $shuffle
                ]
            ], $mcq);
        }

        return $mcq;
    }

    /**
     * Get mcq question number 2
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function questionNumber2(bool $shuffle = false): array
    {
        $mcq = [
            'question' => 'Simple question number 2?',
            'answers' => [
                ['a' => 'Value A2',],
                ['b' => 'Value B2', 'mark_as_answer' => 'on'],
                ['c' => 'Value C2',],
                ['d' => 'Value D2',],
            ]
        ];

        if ($shuffle) {
            return array_merge([
                'settings' => [
                    'shuffle' => $shuffle
                ]
            ], $mcq);
        }

        return $mcq;
    }

    /**
     * Get mcq question number 3
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function questionNumber3(bool $shuffle = false): array
    {
        $mcq = [
            'question' => 'Simple question number 3?',
            'answers' => [
                ['a' => 'Value A3',],
                ['b' => 'Value B3',],
                ['c' => 'Value C3', 'mark_as_answer' => 'on'],
                ['d' => 'Value D3',],
            ]
        ];

        if ($shuffle) {
            return array_merge([
                'settings' => [
                    'shuffle' => $shuffle
                ]
            ], $mcq);
        }

        return $mcq;
    }

    /**
     * Get mcq question number 4
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function questionNumber4(bool $shuffle = false): array
    {
        $mcq = [
            'question' => 'Simple question number 4?',
            'answers' => [
                ['a' => 'Value A4',],
                ['b' => 'Value B4',],
                ['c' => 'Value C4',],
                ['d' => 'Value D4', 'mark_as_answer' => 'on'],
            ]
        ];

        if ($shuffle) {
            return array_merge([
                'settings' => [
                    'shuffle' => $shuffle
                ]
            ], $mcq);
        }

        return $mcq;
    }

    /**
     * Get mcq question number 5
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function questionNumber5(bool $shuffle = false): array
    {
        $mcq = [
            'question' => 'Simple question number 5?',
            'answers' => [
                ['a' => 'Value A5', 'mark_as_answer' => 'on'],
                ['b' => 'Value B5',],
                ['c' => 'Value C5', 'mark_as_answer' => 'on'],
                ['d' => 'Value D5',],
            ]
        ];

        if ($shuffle) {
            return array_merge([
                'settings' => [
                    'shuffle' => $shuffle
                ]
            ], $mcq);
        }

        return $mcq;
    }

    /**
     * Get mcq question number 6
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function questionNumber6(bool $shuffle = false): array
    {
        $mcq = [
            'question' => 'Simple question number 6?',
            'answers' => [
                ['a' => 'Value A6',],
                ['b' => 'Value B6', 'mark_as_answer' => 'on'],
                ['c' => 'Value C6',],
                ['d' => 'Value D6', 'mark_as_answer' => 'on'],
            ]
        ];

        if ($shuffle) {
            return array_merge([
                'settings' => [
                    'shuffle' => $shuffle
                ]
            ], $mcq);
        }

        return $mcq;
    }

    /**
     * Get mcq question number 7
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function questionNumber7(bool $shuffle = false): array
    {
        $mcq = [
            'question' => 'Simple question number 7?',
            'answers' => [
                ['a' => 'Value A7', 'mark_as_answer' => 'on'],
                ['b' => 'Value B7', 'mark_as_answer' => 'on'],
                ['c' => 'Value C7',],
                ['d' => 'Value D7',],
            ]
        ];

        if ($shuffle) {
            return array_merge([
                'settings' => [
                    'shuffle' => $shuffle
                ]
            ], $mcq);
        }

        return $mcq;
    }

    /**
     * Get mcq question number 8
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function questionNumber8(bool $shuffle = false): array
    {
        $mcq = [
            'question' => 'Simple question number 8?',
            'answers' => [
                ['a' => 'Value A8',],
                ['b' => 'Value B8',],
                ['c' => 'Value C8', 'mark_as_answer' => 'on'],
                ['d' => 'Value D8', 'mark_as_answer' => 'on'],
            ]
        ];

        if ($shuffle) {
            return array_merge([
                'settings' => [
                    'shuffle' => $shuffle
                ]
            ], $mcq);
        }

        return $mcq;
    }

    /**
     * Get mcq question number 9
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function questionNumber9(bool $shuffle = false): array
    {
        $mcq = [
            'question' => 'Simple question number 9?',
            'answers' => [
                ['a' => 'Value A9', 'mark_as_answer' => 'on'],
                ['b' => 'Value B9', 'mark_as_answer' => 'on'],
                ['c' => 'Value C9', 'mark_as_answer' => 'on'],
                ['d' => 'Value D9', 'mark_as_answer' => 'on'],
            ]
        ];

        if ($shuffle) {
            return array_merge([
                'settings' => [
                    'shuffle' => $shuffle
                ]
            ], $mcq);
        }

        return $mcq;    }

    /**
     * Get mcq question number 10
     *
     * @param   bool    $shuffle
     * @return  array
     */
    public function questionNumber10(bool $shuffle = false): array
    {
        $mcq = [
            'question' => 'Simple question number 10?',
            'answers' => [
                ['a' => 'Value A10',],
                ['b' => 'Value B10',],
                ['c' => 'Value C10',],
                ['d' => 'Value D10',],
            ]
        ];

        if ($shuffle) {
            return array_merge([
                'settings' => [
                    'shuffle' => $shuffle
                ]
            ], $mcq);
        }

        return $mcq;
    }

    /**
     * Get formatted correct answers
     *
     * @param   array   $options
     * @return  array
     */
    private function formatCorrectAnswers(array $options): array
    {
        return array_values(array_map(function (array $option) {
            return $option['value'];
        }, array_filter($this->formatOptions($options), function (array $option) {
            return $option['mark_as_answer'];
        })));
    }

    /**
     * Get formatted mcq options
     *
     * @param   array $options
     * @return  array
     */
    private function formatOptions(array $options): array
    {
        return array_reduce($options, function (array $options, array $option) {
            $keys = array_keys($option);
            array_push($options, [
                'key' => $keys[0],
                'value' => $option[$keys[0]],
                'mark_as_answer' => array_key_exists('mark_as_answer', $option) && $option['mark_as_answer'] === 'on'
            ]);
            return $options;
        }, []);
    }
}
