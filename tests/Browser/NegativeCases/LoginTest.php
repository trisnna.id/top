<?php

namespace Tests\Browser\NegativeCases;

use App\Models\Role;
use App\Models\User;
use App\Constants\RoleConstant;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * Test user will redirect back into index page when
     * they're put wrong credential
     * 
     * @return void
     */
    public function testFailedToLogin(): void
    {
        $role = Role::factory()->create(['name' => RoleConstant::ROLE_ADMIN]);
        $user = User::factory()->create([
            'email' => $this->faker->safeEmail,
            'password' => bcrypt('password')
        ]);

        $user->assignRole($role);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/')
                    ->click('@loginButton')
                    ->type('email', $user->email)
                    ->type('password', 'wrong')
                    ->click('@confirmButton');
            
            $browser->pause(3600);

            $browser->assertPathIs('/');
            $browser->assertDontSee('My Info');
        });
    }
}
