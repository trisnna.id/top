<?php

namespace Tests\Browser\PositiveCases;

use App\Models\Role;
use App\Models\User;
use App\Constants\RoleConstant;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginAndLogoutTest extends DuskTestCase
{
    use DatabaseMigrations;
    use WithFaker;

    /**
     * Test user should redirect to home page when they're
     * succesfully login
     * 
     * @group authentication
     * @return  void
     */
    public function testLoginAsAdmin(): void
    {
        $role = Role::factory()->create(['name' => RoleConstant::ROLE_ADMIN]);
        $user = User::factory()->create([
            'email' => $this->faker->safeEmail,
            'password' => bcrypt('password')
        ]);

        $user->assignRole($role);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/')
                    ->click('@loginButton')
                    ->type('email', $user->email)
                    ->type('password', 'password')
                    ->click('@confirmButton');

            $browser->waitForLocation('/dashboard', 30);
                    
            $browser->assertAuthenticatedAs($user, 'web');
            $browser->assertSee('My Info');
            $this->assertTrue($user->hasRole(RoleConstant::ROLE_ADMIN));
        });
    }

    /**
     * Test to logout from the system
     * 
     * @depends testLoginAsAdmin
     * @group authentication
     * @return  void
     */
    public function testLogout(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->click('@userMenu');

            $browser->pause(150)
                    ->waitFor('#logoutButton', 5)
                    ->click('@logoutButton')
                    ->waitForLocation('/');

            $browser->assertSee('Welcome message');
        });
    }
}
