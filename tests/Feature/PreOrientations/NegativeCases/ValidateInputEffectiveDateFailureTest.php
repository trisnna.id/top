<?php

namespace Tests\Feature\PreOrientations\NegativeCases;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class ValidateInputEffectiveDateFailureTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use WithSetup;

    /**
     * Test the input effective date should required and it's a valid date with
     * format `Y-m-d`
     *
     * @dataProvider rulesDataProvider
     * @group pre_orientation
     * 
     * @param   string $effectiveDate
     * @return  void
     */
    public function test_effective_date_validation_rules(string $effectiveDate)
    {
        $user = $this->actingAs($this->user);

        $user->get(route('admin.preorientation.create'));
        $response = $user->post(route('admin.preorientation.store'), [
            'activity_name' => 'new connection bingo game',
            'category' => $this->checkpoint->id,
            'nationality' => ["{$this->localityInter->id}"],
            'effective_date' => $effectiveDate,
            'content' => 'test',
            'quiz' => '0',
            'mcq' => []
        ]);

        $response
            ->assertRedirect(route('admin.preorientation.create'))
            ->assertSessionHasErrors('effective_date');
    }

    /**
     * Get data rules testing
     *
     * @return array
     */
    public function rulesDataProvider(): array
    {
        return [
            'rule required' => [''],
            'rule date:Y-m-d' => ['25-02-2023 21:53:00']
        ];
    }
}
