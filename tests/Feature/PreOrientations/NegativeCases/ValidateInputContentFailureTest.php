<?php

namespace Tests\Feature\PreOrientations\NegativeCases;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class ValidateInputContentFailureTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use WithSetup;

    /**
     * Test the input content should have not empty description value and asset
     * as json encoded
     *
     * @dataProvider rulesDataProvider
     * @group pre_orientation
     * 
     * @param   string  $content
     * @return  void
     */
    public function test_nationality_validation_rules(string $content)
    {
        $user = $this->actingAs($this->user);

        $user->get(route('admin.preorientation.create'));
        $response = $user->post(route('admin.preorientation.store'), [
            'activity_name' => 'new connection bingo game',
            'category' => $this->checkpoint->id,
            'nationality' => ["{$this->localityInter->id}"],
            'effective_date' => '2023-02-25 21:53:00',
            'content' => $content,
            'quiz' => '0',
            'mcq' => [],
        ]);

        $response
            ->assertRedirect(route('admin.preorientation.create'))
            ->assertSessionHasErrors('content');
    }

    /**
     * Get data rules testing
     *
     * @return array
     */
    public function rulesDataProvider(): array
    {
        return [
            'rule required' => [''],
        ];
    }
}
