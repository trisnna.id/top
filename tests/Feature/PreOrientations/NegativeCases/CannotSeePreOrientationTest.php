<?php

namespace Tests\Feature\PreOrientations\NegativeCases;

use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class CannotSeePreOrientationTest extends TestCase
{
    use RefreshDatabase;
    use WithSetup;

    /**
     * Test the system should return response with status 404
     * becuase the existing pre-orientation doesn't exist in the
     * datatabse record
     * 
     * @group   pre_orientation
     *
     * @return  void
     */
    public function test_should_received_response_404()
    {
        $user = $this->actingAs($this->user);

        $response = $user->get(route('admin.preorientation.show', [PreOrientationEntity::FIELD_UUID => '1234']));

        $response
            ->assertStatus(404)
        ;
    }
}
