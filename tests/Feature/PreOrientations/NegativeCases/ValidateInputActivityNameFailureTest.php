<?php

namespace Tests\Feature\PreOrientations\NegativeCases;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class ValidateInputActivityNameFailureTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use WithSetup;

    /**
     * Test the input activity_name value is required and the length is not greater
     * than 100 chars
     *
     * @dataProvider rulesDataProvider
     * @group pre_orientation
     * 
     * @param   string $activityName
     * @return  void
     */
    public function test_activity_name_validation_rules(string $activityName)
    {
        $user = $this->actingAs($this->user);

        $user->get(route('admin.preorientation.create'));
        $response = $user->post(route('admin.preorientation.store'), [
                'activity_name' => $activityName,
                'category' => $this->checkpoint->id,
                'nationality' => ["{$this->localityInter->id}"],
                'effective_date' => '2023-02-25',
                'content' => 'test',
                'quiz' => '0',
                'mcq' => []
            ]);

        $response
            ->assertRedirect(route('admin.preorientation.create'))
            ->assertSessionHasErrors('activity_name');
    }

    /**
     * Get data rules testing
     *
     * @return array
     */
    public function rulesDataProvider(): array
    {
        return [
            'rule required' => [''],
            'rule max:100' => [str_repeat('activity name', 12)]
        ];
    }
}
