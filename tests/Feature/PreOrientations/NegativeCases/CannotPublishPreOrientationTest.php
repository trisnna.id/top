<?php

namespace Tests\Feature\PreOrientations\NegativeCases;

use App\Models\PreOrientation;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class CannotPublishPreOrientationTest extends TestCase
{
    use RefreshDatabase;
    use WithSetup;

    /**
     * Test it will return response code 403 because the user
     * doesn't have authorities to publish the pre-orientation
     * 
     * @group   pre_orientations
     * @group   crud
     *
     * @return  void
     */
    public function test_it_should_return_unauthorized_response(): void
    {
        $entity = PreOrientation::factory()->create();
        $user = User::factory()->create();

        $response = $this->actingAs($user)->postJson(route('api.preorientation.publish'), [
            'key' => $entity->uuid,
        ]);

        $response
            ->assertStatus(403);
    }

    /**
     * Get data to test the validation input
     *
     * @return  array
     */
    public function validationDataProvider(): array
    {
        return [
            'rule required' => [''],
            'rule exists' => ['12345'],
        ];
    }

    /**
     * Test it will return response code 422 because the input
     * `key` is invalid
     * 
     * @dataProvider validationDataProvider
     * @group   pre_orientations
     * @group   crud
     *
     * @param   string  $input
     * @return  void
     */
    public function test_it_should_return_error_with_key_required(string $input): void
    {
        $response = $this->actingAs($this->user)->postJson(route('api.preorientation.publish'), [
            'key' => $input,
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonStructure([
                'errors' => [
                    'key'
                ]
            ]);
    }
}
