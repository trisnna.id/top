<?php

namespace Tests\Feature\PreOrientations\NegativeCases;

use App\Models\PreOrientation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class CannotUpdateQuizTest extends TestCase
{
    use RefreshDatabase;
    use WithSetup;

    /**
     * Test the system should return response with status 404
     * becuase the existing pre-orientation doesn't exist in the
     * datatabse record
     * 
     * @group   pre_orientation
     *
     * @return  void
     */
    public function test_should_received_response_404()
    {
        $user = $this->actingAs($this->user);

        $response = $user->patch(route('admin.preorientation.update', ['id' => 100]), [
            'quiz' => 0
        ]);

        $response
            ->assertStatus(404)
        ;
    }
}
