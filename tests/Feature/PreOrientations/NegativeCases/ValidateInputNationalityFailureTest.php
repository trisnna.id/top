<?php

namespace Tests\Feature\PreOrientations\NegativeCases;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class ValidateInputNationalityFailureTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use WithSetup;

    /**
     * Test the input nationality value is required and the value should be exists
     * in the validation rule
     *
     * @dataProvider rulesDataProvider
     * @group pre_orientation
     * 
     * @param   array   $localities
     * @return  void
     */
    public function test_nationality_validation_rules(array $localities)
    {
        $user = $this->actingAs($this->user);

        $user->get(route('admin.preorientation.create'));
        $response = $user->post(route('admin.preorientation.store'), [
            'activity_name' => 'new connection bingo game',
            'category' => $this->checkpoint->id,
            'nationality' => [''],
            'effective_date' => '2023-02-25',
            'content' => 'test',
            'quiz' => '0',
            'mcq' => [],
        ]);

        $response
            ->assertRedirectToRoute('admin.preorientation.create')
            ->assertSessionHasErrors('nationality.0');
    }

    /**
     * Get data rules testing
     *
     * @return array
     */
    public function rulesDataProvider(): array
    {
        return [
            'rule required' => [['']],
            'rule checkpoint id should exists' => [['3']]
        ];
    }
}
