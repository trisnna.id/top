<?php

namespace Tests\Feature\PreOrientations\NegativeCases;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class ValidateInputQuizMCQFailureTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use WithSetup;

    /**
     * Test the input `quiz` value is required and should be in boolean
     * numeric value [1||0]
     *
     * @dataProvider inputQuizRulesDataProvider
     * @group pre_orientation
     * 
     * @return void
     */
    public function test_quiz_validation_rules($quiz)
    {
        $user = $this->actingAs($this->user);

        $user->get(route('admin.preorientation.create'));
        $response = $user->post(route('admin.preorientation.store'), [
            'activity_name' => 'new connection bingo game',
            'category' => $this->checkpoint->id,
            'nationality' => [''],
            'effective_date' => '2023-02-25',
            'content' => 'test',
            'quiz' => $quiz,
            'mcq' => [],
        ]);

        $response
            ->assertRedirectToRoute('admin.preorientation.create')
            ->assertSessionHasErrors('quiz');
    }

    /**
     * Test the input `mcq` value is required and should be present & cannot
     * be empty if `quiz` value is `TRUE`
     *
     * @return void
     */
    public function test_mcq_validation_rules()
    {
        $user = $this->actingAs($this->user);

        $user->get(route('admin.preorientation.create'));
        $response = $user->post(route('admin.preorientation.store'), [
            'activity_name' => 'new connection bingo game',
            'category' => $this->checkpoint->id,
            'nationality' => [''],
            'effective_date' => '2023-02-25',
            'content' => 'test',
            'quiz' => 1,
            'mcq' => '[]',
        ]);

        $response
            ->assertRedirectToRoute('admin.preorientation.create')
            ->assertSessionHasErrors('mcq');
    }

    /**
     * Get data rules testing
     *
     * @return array
     */
    public function inputQuizRulesDataProvider(): array
    {
        return [
            'rule required' => [''],
            'rule invalid value' => ['true']
        ];
    }
}
