<?php

namespace Tests\Feature\PreOrientations\NegativeCases;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class ValidateInputCategoryFailureTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    use WithSetup;

    /**
     * Test the input category value is required and the value should be exists
     * in the validation rule
     *
     * @dataProvider rulesDataProvider
     * @group pre_orientation
     * 
     * @param   string $category
     * @return  void
     */
    public function test_category_validation_rules(string $category)
    {
        $user = $this->actingAs($this->user);

        $user->get(route('admin.preorientation.create'));
        $response = $user->post(route('admin.preorientation.store'), [
            'activity_name' => 'new connection bingo game',
            'category' => $category,
            'nationality' => ["{$this->localityLocal->id}"],
            'effective_date' => '2023-02-25',
            'content' => 'test',
            'quiz' => '0',
            'mcq' => []
        ]);

        $response
            ->assertRedirect(route('admin.preorientation.create'))
            ->assertSessionHasErrors('category');
    }

    /**
     * Get data rules testing
     *
     * @return array
     */
    public function rulesDataProvider(): array
    {
        return [
            'rule required' => [''],
            'rule checkpoint exists' => ['3']
        ];
    }
}
