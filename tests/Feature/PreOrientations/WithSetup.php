<?php declare(strict_types=1);

namespace Tests\Feature\PreOrientations;

use App\Models\Checkpoint;
use App\Models\Locality;
use App\Models\User;

trait WithSetup
{
    /**
     * Locality model instance for type 'Local'
     *
     * @var Locality
     */
    protected Locality $localityLocal;

    /**
     * Locality model instance for type 'International'
     *
     * @var Locality
     */
    protected Locality $localityInter;

    /**
     * Checkpoint model instance
     *
     * @var Checkpoint
     */
    protected Checkpoint $checkpoint;

    /**
     * User model instance with role 'admin'
     *
     * @var User
     */
    protected User $user;

    /**
     * Set up the testing needs
     *
     * {@override}
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create(['name' => 'admin']);
        $this->checkpoint = Checkpoint::factory()->create();
        $this->localityLocal = Locality::factory()->create(['name' => 'Local']);
        $this->localityInter = Locality::factory()->create(['name' => 'International']);
    }
}
