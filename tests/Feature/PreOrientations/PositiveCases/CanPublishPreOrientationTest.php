<?php

namespace Tests\Feature\PreOrientations\PositiveCases;

use App\Models\PreOrientation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class CanPublishPreOrientationTest extends TestCase
{
    use RefreshDatabase;
    use WithSetup;

    /**
     * Test publish pre-orientation
     * 
     * @group   pre_orientations
     * @group   crud
     *
     * @return  void
     */
    public function test_a_staff_can_publish_pre_orientation(): void
    {
        // create 'draft' pre-orientation
        $entity = PreOrientation::factory()->create();
        $user = $this->actingAs($this->user);

        $response = $user->post(route('api.preorientation.publish'), [
            'key' => $entity->uuid,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'data' => [
                    'message' => "Pre-orientation named {$entity->activity_name} is published successfully."
                ]
            ]);
    }
}
