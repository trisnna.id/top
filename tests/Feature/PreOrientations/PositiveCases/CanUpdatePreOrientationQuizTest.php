<?php

namespace Tests\Feature\PreOrientations\PositiveCases;

use App\Models\PreOrientation;
use App\Models\PreOrientationQuiz;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DataProviders\QuizDataProvider;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class CanUpdatePreOrientationQuizTest extends TestCase
{
    use QuizDataProvider;
    use RefreshDatabase;
    use WithFaker;
    use WithSetup;

    /**
     * Test update existing quiz attributes by quiz id
     *
     * @group   pre_orientation
     * @group   crud
     * 
     * @return void
     */
    public function test_can_replace_existing_quiz_with_the_new_one()
    {
        $inputQuiz = $this->questionNumber1(true);
        $user = $this->actingAs($this->user);
        $preOrientation = PreOrientation::factory()->create();
        $preOrientationQuiz = PreOrientationQuiz::factory()->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation
        ]);

        $user->get(route('admin.preorientation.edit', [PreOrientation::FIELD_UUID => $preOrientation->uuid]));
        $response = $user->patch(route('admin.preorientation.update', ['id' => $preOrientation->id]), [
            'quiz' => 1,
            'mcq' => [array_merge(['quiz_id' => $preOrientationQuiz->id], $inputQuiz)]
        ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertSessionHas('message')
            ->assertRedirectToRoute('admin.preorientation.show', [PreOrientation::FIELD_UUID => $preOrientation->uuid])
        ;
        $this->assertDatabaseHas($preOrientationQuiz->getTable(), [
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation->id, 
            PreOrientationQuiz::FIELD_QUESTION => $inputQuiz['question'], 
            PreOrientationQuiz::FIELD_OPTIONS => json_encode($this->formatOptions($inputQuiz['answers'])), 
            PreOrientationQuiz::FIELD_ANSWERS => json_encode($this->formatCorrectAnswers($inputQuiz['answers'])),
            PreOrientationQuiz::FIELD_SETTINGS => json_encode($inputQuiz['settings']),
        ]);
        $this->assertDatabaseMissing($preOrientationQuiz->getTable(), [
            PreOrientationQuiz::FIELD_QUESTION => $preOrientationQuiz->question, 
            PreOrientationQuiz::FIELD_OPTIONS => json_encode($preOrientationQuiz->options), 
            PreOrientationQuiz::FIELD_ANSWERS => json_encode($preOrientationQuiz->answers),
            PreOrientationQuiz::FIELD_SETTINGS => json_encode($preOrientationQuiz->settings),
        ]);
    }

    /**
     * Test the existing quizs should be removed if the input mcq that
     * contains the specific quiz id is empty (The quiz was removed from view)
     * 
     * @group   pre_orientation
     * @group   crud
     *
     * @return void
     */
    public function test_it_should_remove_the_existing_quiz(): void
    {
        $user = $this->actingAs($this->user);
        $preOrientation = PreOrientation::factory()->create();
        $preOrientationQuiz = PreOrientationQuiz::factory(3)->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation
        ]);

        $user->get(route('admin.preorientation.edit', [PreOrientation::FIELD_UUID => $preOrientation->uuid]));
        $response = $user->patch(route('admin.preorientation.update', ['id' => $preOrientation->id]), [
            'quiz' => 1,
            'mcq' => $preOrientationQuiz->transform(function (PreOrientationQuiz $quiz) {
                return ['quiz_id' => $quiz->id];
            })->toArray()
        ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertSessionHas('message')
            ->assertRedirectToRoute('admin.preorientation.show', [PreOrientation::FIELD_UUID => $preOrientation->uuid])
        ;
        $this->assertDatabaseEmpty('pre_orientation_quizs');
    }

    /**
     * Test should create a new quiz record if the mcq input contains
     * quiz that not stored in the datatabase table yet
     *
     * @group   pre_orientation
     * @group   crud
     * 
     * @return void
     */
    public function test_it_should_create_a_new_quiz_record_if_its_not_stored_in_the_table(): void
    {
        $newQuiz1 = $this->questionNumber1(true);
        $newQuiz2 = $this->questionNumber2();
        $user = $this->actingAs($this->user);
        $preOrientation = PreOrientation::factory()->create();
        $preOrientationQuiz = PreOrientationQuiz::factory()->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation
        ]);

        $user->get(route('admin.preorientation.edit', [PreOrientation::FIELD_UUID => $preOrientation->uuid]));
        $response = $user->patch(route('admin.preorientation.update', ['id' => $preOrientation->id]), [
            'quiz' => 1,
            'mcq' => [$newQuiz1, $newQuiz2]
        ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertSessionHas('message')
            ->assertRedirectToRoute('admin.preorientation.show', [PreOrientation::FIELD_UUID => $preOrientation->uuid])
        ;
        $this->assertDatabaseCount($preOrientationQuiz->getTable(), 3);
        $this->assertDatabaseHas($preOrientationQuiz->getTable(), [
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation->id, 
            PreOrientationQuiz::FIELD_QUESTION => $newQuiz1['question'], 
            PreOrientationQuiz::FIELD_OPTIONS => json_encode($this->formatOptions($newQuiz1['answers'])), 
            PreOrientationQuiz::FIELD_ANSWERS => json_encode($this->formatCorrectAnswers($newQuiz1['answers'])),
            PreOrientationQuiz::FIELD_SETTINGS => json_encode($newQuiz1['settings']),
        ]);
        $this->assertDatabaseHas($preOrientationQuiz->getTable(), [
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation->id, 
            PreOrientationQuiz::FIELD_QUESTION => $newQuiz2['question'],
            PreOrientationQuiz::FIELD_OPTIONS => json_encode($this->formatOptions($newQuiz2['answers'])), 
            PreOrientationQuiz::FIELD_ANSWERS => json_encode($this->formatCorrectAnswers($newQuiz2['answers'])),
            PreOrientationQuiz::FIELD_SETTINGS => json_encode([
                PreOrientationQuiz::SETTINGS_SHUFFLE => false
            ]),
        ]);
    }
}
