<?php

namespace Tests\Feature\PreOrientations\PositiveCases;

use App\Models\PreOrientation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DataProviders\ContentDataProvider;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class CanSeeExistingPreOrientationTest extends TestCase
{
    use ContentDataProvider;
    use RefreshDatabase;
    use WithSetup;

    /**
     * Test can see the existing quiz in preview pre-orientation page 
     *
     * @group   pre_orientation
     * @group   crud
     * 
     * @return  void
     */
    public function test_can_see_pre_orientation_record()
    {
        $user = $this->actingAs($this->user);
        $entity = PreOrientation::factory()->create([
            PreOrientation::FIELD_CONTENT => $this->contentFull()
        ]);

        $response = $user->get(route('admin.preorientation.show', [PreOrientation::FIELD_UUID => $entity->uuid]));

        $response
            ->assertStatus(200)
            ->assertViewIs('admin.preorientation.show')
            ->assertSeeInOrder([
                $entity->activity_name,
                'Lorem ipsum',
                'Video 1',
                'Video 2',
            ]);
    }
}
