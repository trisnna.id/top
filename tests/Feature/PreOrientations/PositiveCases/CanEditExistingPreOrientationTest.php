<?php

namespace Tests\Feature\PreOrientations\PositiveCases;

use App\Models\PreOrientation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class CanEditExistingPreOrientationTest extends TestCase
{
    use RefreshDatabase;
    use WithSetup;

    /**
     * Test can see the existing quiz in edit pre-orientation page 
     *
     * @group   pre_orientation
     * @group   crud
     * 
     * @return  void
     */
    public function test_can_edit_pre_orientation_record()
    {
        $entity = PreOrientation::factory()->create();
        $user = $this->actingAs($this->user);

        $response = $user->get(route('admin.preorientation.edit', [PreOrientation::FIELD_UUID => $entity->uuid]));

        $response
            ->assertStatus(200)
            ->assertViewIs('admin.preorientation.upsert')
            ->assertSeeInOrder([
                $entity->activity_name,
                $entity->checkpoint->label
            ]);
    }
}
