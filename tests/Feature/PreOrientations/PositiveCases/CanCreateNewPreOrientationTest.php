<?php

namespace Tests\Feature\PreOrientations\PositiveCases;

use App\Contracts\Entities\PreOrientationEntity;
use App\Contracts\Entities\PreOrientationQuizEntity;
use App\Models\PreOrientation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DataProviders\ContentDataProvider;
use Tests\DataProviders\QuizDataProvider;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class CanCreateNewPreOrientationTest extends TestCase
{
    use ContentDataProvider;
    use QuizDataProvider;
    use RefreshDatabase;
    use WithFaker;
    use WithSetup;

    /**
     * Get input data for pre-orientation
     *
     * @return array
     */
    public function preOrientationInputDataProvider(): array
    {
        return [
            'pre-orientation with quiz' => [
                'Binggo Game 1',
                '2023-03-11',
                $this->contentFull(),
                json_encode($this->linksExpectation()),
                1,
                [
                    $this->questionNumber1(),
                    $this->questionNumber2(),
                    $this->questionNumber3(),
                    $this->questionNumber4(),
                    $this->questionNumber5(),
                ],
            ],
            'pre-orientation without quiz' => [
                'Binggo Game 2',
                '2023-03-22',
                $this->contentWithoutLinks(),
                json_encode([]),
                0,
            ]
        ];
    }

    /**
     * Test create a new pre-orientation record that contains content
     * containing images, paragraphs, & links and has multiple choice questions 
     * 
     * @dataProvider    preOrientationInputDataProvider
     * @group           pre_orientation
     * @group           crud
     *
     * @param   string  $activityName
     * @param   string  $effectiveDate
     * @param   string  $content
     * @param   string  $contentLinksExpectation
     * @param   int     $quiz
     * @param   array   $mcq
     * @return  void
     */
    public function test_successfully_store_pre_orientation(
        string $activityName,
        string $effectiveDate,
        string $content,
        string $contentLinksExpectation,
        int $quiz,
        array $mcq = []
    ): void {
        $checkpointId = $this->checkpoint->id;
        $localities = ["{$this->localityLocal->id}"];

        $response = $this->actingAs($this->user)->post(route('admin.preorientation.store'), [
            PreOrientationEntity::FIELD_ACTIVITY_NAME => $activityName,
            PreOrientationEntity::FIELD_EFFECTIVE_DATE => $effectiveDate,
            PreOrientationEntity::FIELD_CONTENT => $content,
            'category' => $checkpointId,
            'nationality' => $localities,
            'quiz' => $quiz,
            'mcq' => $mcq
        ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertSessionHas('message');
        $this->assertDatabaseHas('pre_orientations', [
            PreOrientationEntity::FIELD_ACTIVITY_NAME => $activityName,
            PreOrientationEntity::FIELD_EFFECTIVE_DATE => $effectiveDate,
            PreOrientationEntity::FIELD_CONTENT => trim($content),
            PreOrientationEntity::FIELD_STATUS => PreOrientationEntity::STATUS_DRAFT,
            PreOrientationEntity::FIELD_CATEGORY => $checkpointId,
            PreOrientationEntity::FIELD_LOCALITIES => json_encode($localities),
            PreOrientationEntity::FIELD_CONTENT_LINKS => $contentLinksExpectation
        ]);

        foreach ($mcq as $quiz) {
            $this->assertDatabaseHas('pre_orientation_quizs', [
                PreOrientationQuizEntity::FIELD_PREORIENTATION => PreOrientation::latest()->first()->id,
                PreOrientationQuizEntity::FIELD_QUESTION => $quiz['question'],
                PreOrientationQuizEntity::FIELD_OPTIONS => json_encode($this->formatOptions($quiz['answers'])),
                PreOrientationQuizEntity::FIELD_ANSWERS => json_encode($this->formatCorrectAnswers($quiz['answers'])),
                PreOrientationQuizEntity::FIELD_SETTINGS => json_encode(['shuffle' => false])
            ]);
        }
    }
}
