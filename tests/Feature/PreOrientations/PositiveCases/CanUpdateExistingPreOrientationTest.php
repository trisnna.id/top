<?php

namespace Tests\Feature\PreOrientations\PositiveCases;

use App\Contracts\Entities\PreOrientationEntity;
use App\Models\PreOrientation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DataProviders\ContentDataProvider;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class CanUpdateExistingPreOrientationTest extends TestCase
{
    use ContentDataProvider;
    use RefreshDatabase;
    use WithFaker;
    use WithSetup;

    /**
     * Get input data for pre-orientation
     *
     * @return array
     */
    public function preOrientationInputPrimitiveDataProvider(): array
    {
        return [
            'update pre-orientation activity name' => [
                PreOrientationEntity::FIELD_ACTIVITY_NAME,
                'Binggo Game'
            ],
            'update pre-orientation effective date' => [
                PreOrientationEntity::FIELD_EFFECTIVE_DATE,
                '2023-03-19'
            ],
        ];
    }

    /**
     * Test update pre-orientation `activity name` value
     *
     * @dataProvider    preOrientationInputPrimitiveDataProvider
     * @group           pre_orientation
     * @group           crud
     * 
     * @param   string  $field
     * @param   mixed   $value
     * @return  void
     */
    public function test_successfully_updating_pre_orientation_primitive_data(string $field, $value): void
    {
        $user = $this->actingAs($this->user);
        $entity = PreOrientation::factory()->create();

        $user->get(route('admin.preorientation.edit', [PreOrientationEntity::FIELD_UUID => $entity->uuid]));
        $response = $user->patch(route('admin.preorientation.update', ['id' => $entity->id]), [
            $field => $value
        ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertSessionHas('message')
            ->assertRedirectToRoute('admin.preorientation.show', [
                PreOrientationEntity::FIELD_UUID => $entity->uuid
            ])
        ;
        $this->assertDatabaseHas($entity->getTable(), [$field => $value]);
        $this->assertDatabaseMissing($entity->getTable(), [$field => $entity[$field]]);
    }

    /**
     * Test update pre-orientation `checkpoint id` value
     *
     * @group   pre_orientation
     * @group   crud
     * 
     * @return  void
     */
    public function test_successfully_updating_pre_orientation_checkpoint(): void
    {
        $user = $this->actingAs($this->user);
        $entity = PreOrientation::factory()->create();

        $user->get(route('admin.preorientation.edit', [PreOrientationEntity::FIELD_UUID => $entity->uuid]));
        $response = $user->patch(route('admin.preorientation.update', ['id' => $entity->id]), [
            'category' => $this->checkpoint->id
        ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertSessionHas('message')
            ->assertRedirectToRoute('admin.preorientation.show', [PreOrientationEntity::FIELD_UUID => $entity->uuid])
        ;
        $this->assertDatabaseHas($entity->getTable(), [
            PreOrientationEntity::FIELD_CATEGORY => $this->checkpoint->id
        ]);
        $this->assertDatabaseMissing($entity->getTable(), [
            PreOrientationEntity::FIELD_CATEGORY => $entity->checkpoint_id
        ]);
    }

    /**
     * Test update pre-orientation `localities` value
     *
     * @group   pre_orientation
     * @group   crud
     * 
     * @return  void
     */
    public function test_successfully_updating_pre_orientation_localities(): void
    {
        $newValue = ["{$this->localityInter->id}", "{$this->localityLocal->id}"];
        $user = $this->actingAs($this->user);
        $entity = PreOrientation::factory()->create();

        $user->get(route('admin.preorientation.edit', [PreOrientationEntity::FIELD_UUID => $entity->uuid]));
        $response = $user->patch(route('admin.preorientation.update', ['id' => $entity->id]), [
            'nationality' => $newValue
        ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertSessionHas('message')
            ->assertRedirectToRoute('admin.preorientation.show', [PreOrientationEntity::FIELD_UUID => $entity->uuid])
        ;
        $this->assertDatabaseHas($entity->getTable(), [
            PreOrientationEntity::FIELD_LOCALITIES => json_encode($newValue)
        ]);
        $this->assertDatabaseMissing($entity->getTable(), [
            PreOrientationEntity::FIELD_LOCALITIES => json_encode($entity->localities)
        ]);
    }

    /**
     * Get input content
     *
     * @return  array
     */
    public function contentDataProvider(): array
    {
        return [
            'content only paragraph' => [
                $this->paragraphElement(),
                json_encode([])
            ],
            'content without links' => [
                $this->contentWithoutLinks(),
                json_encode([]),
            ],
            'content without images' => [
                $this->contentWithoutImage(),
                json_encode($this->linksExpectation())
            ],
            'content with images & links' => [
                $this->contentFull(),
                json_encode($this->linksExpectation())
            ]
        ];
    }

    /**
     * Test update pre-orientation `content` value
     *
     * @dataProvider    contentDataProvider
     * @group           pre_orientation
     * @group           crud
     * 
     * @param   string $content
     * @param   string $contentLinkExpectation
     * @return  void
     */
    public function test_successfully_updating_pre_orientation_content(string $content, string $contentLinkExpectation): void
    {
        $user = $this->actingAs($this->user);
        $entity = PreOrientation::factory()->create();

        $user->get(route('admin.preorientation.edit', [PreOrientationEntity::FIELD_UUID => $entity->uuid]));
        $response = $user->patch(route('admin.preorientation.update', ['id' => $entity->id]), [
            PreOrientationEntity::FIELD_CONTENT => $content
        ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertSessionHas('message')
            ->assertRedirectToRoute('admin.preorientation.show', [PreOrientationEntity::FIELD_UUID => $entity->uuid])
        ;
        $this->assertDatabaseHas($entity->getTable(), [
            PreOrientationEntity::FIELD_CONTENT => trim($content), 
            PreOrientationEntity::FIELD_CONTENT_LINKS => $contentLinkExpectation, 
        ]);
        $this->assertDatabaseMissing($entity->getTable(), [
            PreOrientationEntity::FIELD_CONTENT => $entity->content, 
            PreOrientationEntity::FIELD_CONTENT_LINKS => json_encode($entity->content_links->toArray()), 
        ]);
    }
}
