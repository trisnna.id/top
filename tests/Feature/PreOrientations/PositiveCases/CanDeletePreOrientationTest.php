<?php

namespace Tests\Feature\PreOrientations\PositiveCases;

use App\Models\PreOrientation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\PreOrientations\WithSetup;
use Tests\TestCase;

class CanDeletePreOrientationTest extends TestCase
{
    use RefreshDatabase;
    use WithSetup;
    
    /**
     * Test can delete existing pre-orientation quiz
     *
     * @group   pre_orientation
     * @group   crud
     * 
     * @return  void
     */
    public function test_can_delete_pre_orientation_record()
    {
        $entity = PreOrientation::factory()->create();
        $user = $this->actingAs($this->user);

        $response = $user->delete(route('admin.preorientation.delete', [PreOrientation::FIELD_UUID => $entity->uuid]));

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }
}
