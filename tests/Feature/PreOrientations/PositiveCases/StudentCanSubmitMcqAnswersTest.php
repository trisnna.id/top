<?php

namespace Tests\Feature\PreOrientations\PositiveCases;

use App\Contracts\Entities\StudentPreOrientationEntity;
use App\Models\PreOrientation;
use App\Models\PreOrientationQuiz;
use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DataProviders\QuizDataProvider;
use Tests\TestCase;

class StudentCanSubmitMcqAnswersTest extends TestCase
{
    use RefreshDatabase;
    use QuizDataProvider;
    use WithFaker;

    /**
     * Get questions data
     *
     * @return array
     */
    public function quiestionsDataProvider(): array
    {
        return [
            'all answers are correct' => [
                [
                    $this->questionNumber1(true),
                    $this->questionNumber2(),
                    $this->questionNumber3(true),
                    $this->questionNumber4()
                ],
                100
            ]
        ];
    }

    /**
     * Test student's submit the mcq answers and expect they
     * get the maximum score because all answers are correct
     *
     * @dataProvider    quiestionsDataProvider
     * @group           pre_orientations
     * @group           crud
     * 
     * @param   array   $questions
     * @param   float   $score
     * @return  void
     */
    public function test_it_should_receive_maximum_score(array $questions, float $score)
    {
        $preOrientation = PreOrientation::factory()->create();
        $preOrientationQuizs = PreOrientationQuiz::factory(count($questions))->sequence(function ($sequence) use ($questions) {
            $quiz = $questions[$sequence->index];
            return [
                PreOrientationQuiz::FIELD_QUESTION => $quiz['question'],
                PreOrientationQuiz::FIELD_OPTIONS => $quiz['answers'],
                PreOrientationQuiz::FIELD_SETTINGS => array_key_exists('settings', $quiz) ? $quiz['settings'] : []
            ];
        })->create([
            PreOrientationQuiz::FIELD_PREORIENTATION => $preOrientation
        ]);
        $student = Student::factory()->create([
            'taylors_email' => sprintf('%s@top.com', $this->faker->name)
        ]);
        $answers = $preOrientationQuizs->transform(function (PreOrientationQuiz $entity) {
            return [
                'quiz_id' => $entity->id,
                'answers' => $entity->answers->toArray()
            ];
        })->toArray();


        $response = $this->actingAs($student->user)->postJson(route('api.preorientation.submitmcq'), [
            'preorientation' => $preOrientation->id,
            'mcq' => $answers
        ]);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'message'
                ]
            ]);
        $this->assertDatabaseHas(StudentPreOrientationEntity::TABLE, [
            StudentPreOrientationEntity::FIELD_PREORIENTATION => $preOrientation->id,
            StudentPreOrientationEntity::FIELD_STUDENT => $student->id,
            StudentPreOrientationEntity::FIELD_COMPLETED => false,
            StudentPreOrientationEntity::FIELD_SCORE => $score,
            StudentPreOrientationEntity::FIELD_QUIZ => json_encode($answers),
        ]);
    }
}
