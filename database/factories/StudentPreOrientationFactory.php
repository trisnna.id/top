<?php

namespace Database\Factories;

use App\Contracts\Entities\StudentPreOrientationEntity;
use App\Models\PreOrientation;
use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\StudentPreOrientationQuiz>
 */
class StudentPreOrientationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            StudentPreOrientationEntity::FIELD_PREORIENTATION => PreOrientation::factory()->create(),
            StudentPreOrientationEntity::FIELD_STUDENT => Student::factory()->create(),
            StudentPreOrientationEntity::FIELD_COMPLETED => false,
            StudentPreOrientationEntity::FIELD_SCORE => 0,
            StudentPreOrientationEntity::FIELD_QUIZ => [] 
        ];
    }

    /**
     * Indicates that the student has completed the pre-orientation quiz
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function completed()
    {
        return $this->state(function (array $attributes) {
            return [
                StudentPreOrientationEntity::FIELD_COMPLETED => true
            ];
        });
    }
}
