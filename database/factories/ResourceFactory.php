<?php

namespace Database\Factories;

use App\Contracts\Entities\ResourceEntity;
use App\Models\Resource;
use App\Models\ResourceType;
use App\Models\Staff;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Resource>
 */
class ResourceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<\Illuminate\Database\Eloquent\Model|TModel>
     */
    protected $model = Resource::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            ResourceEntity::FIELD_UUID => $this->faker->uuid,
            ResourceEntity::FIELD_TITLE => $this->faker->words(3, true),
            ResourceEntity::FIELD_DESCRIPTION => $this->faker->sentence,
            ResourceEntity::FIELD_CATEGORY => $this->faker->randomElement(ResourceEntity::CATEGORIES),
            ResourceEntity::FIELD_VALUES => json_encode([]),
        ];
    }

    /**
     * Set the resource values
     *
     * @param   array   $values
     * @return  static
     */
    public function values(array $values)
    {
        return $this->state(function () use ($values) {
            return [
                ResourceEntity::FIELD_VALUES => $values
            ];
        });
    }
}
