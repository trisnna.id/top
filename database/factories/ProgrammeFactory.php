<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Programme>
 */
class ProgrammeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'uuid' => $this->faker->uuid,
            'code' => $this->faker->countryCode,
            'name' => $this->faker->colorName,
            'code_name' => $this->faker->languageCode,
            'is_active' => true,
            'created_by' => $user = User::factory()->create(),
            'updated_by' => $user
        ];
    }
}
