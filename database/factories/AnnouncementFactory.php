<?php

namespace Database\Factories;

use App\Contracts\Entities\AnnouncementEntity;
use App\Models\Checkpoint;
use App\Models\Locality;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Announcement>
 */
class AnnouncementFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        // $locality = Locality::factory()->create();

        return [
            AnnouncementEntity::FIELD_UUID => $this->faker->uuid(),
            AnnouncementEntity::FIELD_NAME => $this->faker->sentence,
            AnnouncementEntity::FIELD_CATEGORY => $this->faker->randomElement(['1', '2', '3', '4', '5', '6']),
            AnnouncementEntity::FIELD_LOCALITIES => [$this->faker->randomElement(['1', '2'])],
            AnnouncementEntity::FIELD_STATUS => $this->faker->randomElement(AnnouncementEntity::STATUSES),
            AnnouncementEntity::FIELD_CONTENT => $content = $this->faker->randomHtml,
            AnnouncementEntity::FIELD_CONTENT_LINKS => $content,
            AnnouncementEntity::FIELD_INTAKES => ["1"],
            AnnouncementEntity::FIELD_CAMPUSES => [$this->faker->randomElement(['1', '2'])],
            AnnouncementEntity::FIELD_PROGRAMMES => [$this->faker->randomElement(['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26'])],
        ];
    }
}
