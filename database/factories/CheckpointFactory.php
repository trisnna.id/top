<?php

namespace Database\Factories;

use App\Contracts\Entities\CheckpointEntity;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Checkpoint>
 */
class CheckpointFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            CheckpointEntity::FIELD_UUID => $this->faker->uuid,
            CheckpointEntity::FIELD_LABEL => $checkpoint = sprintf('%s %s', config('view.description.checkpoint'), rand(1, 6)),
            CheckpointEntity::FIELD_VALUE => strtolower($checkpoint),
        ];
    }
}
