<?php

namespace Database\Factories;

use App\Models\Programme;
use App\Models\Student;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Faq>
 */
class StudentFactory extends Factory
{
    protected $model = Student::class;

    public function definition()
    {
        $studentCountry = $this->faker->studentCountry;
        $studentProgramme = $this->faker->studentProgramme;
        $studentFaculty = $this->faker->studentFaculty;
        $studentSchool = $this->faker->studentSchool;
        $idNumber = rand(100000, 999999) . 'xxx';
        return [
            'name' => $this->faker->name,
            'user_id' => User::factory()->create(),
            'id_number' => $idNumber,
            'gender' => $this->faker->studentGender,
            'country_from' => $studentCountry['country_from'],
            'nationality' => $studentCountry['nationality'],
            'programme_id' => Programme::factory()->create(),
            'programme_code' => $studentProgramme['code'],
            'programme_name' => $studentProgramme['name'],
            'faculty_name' => $studentFaculty['name'],
            'faculty_code' => $studentFaculty['code'],
            'school_code' => $studentSchool['code'],
            'school_name' => $studentSchool['name'],
            'level_of_study' => $this->faker->studentLevelOfStudy,
            'course_status' => $this->faker->studentCourseStatus,
            'intake_number' => $this->faker->studentIntakeNumber,
            'current_semester' => rand(1, 3),
            'study_mode' => $this->faker->studentStudyMode,
            'contact_number' => $this->faker->phoneNumber,
            'taylors_email' => "{$idNumber}@top",
            'personal_email' => $this->faker->email,
            'locality' => $studentCountry['locality'],
            'campus' => $this->faker->studentCampus,
            'mobility' => $this->faker->studentMobility,
        ];
    }
}
