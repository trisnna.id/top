<?php

namespace Database\Factories;

use App\Models\Locality;
use App\Models\Checkpoint;
use App\Models\PreOrientation;
use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PreOrientation>
 */
class PreOrientationFactory extends Factory
{
    // protected $model = PreOrientation::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        // $locality = Locality::factory()->create();

        return [
            PreOrientationEntity::FIELD_UUID => $this->faker->uuid(),
            PreOrientationEntity::FIELD_ACTIVITY_NAME => $this->faker->sentence,
            PreOrientationEntity::FIELD_CATEGORY => Checkpoint::factory()->create(),
            PreOrientationEntity::FIELD_LOCALITIES => [],
            PreOrientationEntity::FIELD_EFFECTIVE_DATE => $this->faker->date('Y-m-d'),
            PreOrientationEntity::FIELD_STATUS => $this->faker->randomElement(PreOrientationEntity::STATUSES),
            PreOrientationEntity::FIELD_CONTENT => $content = $this->faker->randomHtml,
            PreOrientationEntity::FIELD_CONTENT_LINKS => $content,
        ];
    }
}
