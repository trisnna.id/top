<?php

namespace Database\Factories;

use App\Contracts\Entities\PreOrientationQuizEntity;
use App\Models\PreOrientation;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PreOrientationQuiz>
 */
class PreOrientationQuizFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            PreOrientationQuizEntity::FIELD_PREORIENTATION => PreOrientation::factory()->create(),
            PreOrientationQuizEntity::FIELD_QUESTION => $this->faker->sentence,
            PreOrientationQuizEntity::FIELD_OPTIONS => [
                ['key' => 'a', 'value' => $this->faker->word, 'mark_as_answer' => $this->faker->randomElement([true, false])],
                ['key' => 'b', 'value' => $this->faker->word, 'mark_as_answer' => $this->faker->randomElement([true, false])],
                ['key' => 'c', 'value' => $this->faker->word, 'mark_as_answer' => $this->faker->randomElement([true, false])],
                ['key' => 'd', 'value' => $this->faker->word, 'mark_as_answer' => $this->faker->randomElement([true, false])],
            ],
            PreOrientationQuizEntity::FIELD_SETTINGS => [
                PreOrientationQuizEntity::SETTINGS_SHUFFLE => $this->faker->randomElement([true, false])
            ],
        ];
    }
}
