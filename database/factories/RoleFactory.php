<?php

namespace Database\Factories;

use App\Constants\RoleConstant;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Role>
 */
class RoleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'uuid' => $this->faker->uuid,
            'name' => $name = $this->faker->name,
            'title' => Str::title($name),
            'description' => $this->faker->sentence,
            'is_active' => RoleConstant::STATUS_ACTIVE,
            'guard_name' => RoleConstant::GUARD_WEB,
        ];
    }
}
