<?php

use App\Contracts\Entities\PreOrientationQuizEntity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_orientation_quizs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(PreOrientationQuizEntity::FIELD_PREORIENTATION);
            $table->text(PreOrientationQuizEntity::FIELD_QUESTION);
            $table->json(PreOrientationQuizEntity::FIELD_OPTIONS);
            $table->json(PreOrientationQuizEntity::FIELD_ANSWERS);
            $table->json(PreOrientationQuizEntity::FIELD_SETTINGS);

            // Relationships
            $table->foreign(PreOrientationQuizEntity::FIELD_PREORIENTATION)->references('id')->on('pre_orientations')->onDelete('cascade');

            // Indexing
            $table->index(PreOrientationQuizEntity::FIELD_PREORIENTATION);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_orientation_quiz');
    }
};
