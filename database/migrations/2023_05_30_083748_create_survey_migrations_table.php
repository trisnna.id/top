<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_migrations', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique();
            $table->string('file_name')->nullable();
            $table->unsignedBigInteger('type_id')->nullable()->index();
            $table->integer('total_record')->default(0);
            $table->boolean('is_complete')->default(false);
            $table->datetime('expired_at')->nullable();
            $table->unsignedBigInteger('created_by')->nullable()->index();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->unsignedBigInteger('updated_by')->nullable()->index();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->timestamps();
        });
        Schema::table('students', function (Blueprint $table) {
            $table->boolean('is_pre_orientation_survey_complete')->default(false);
            $table->boolean('is_post_orientation_survey_complete')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_migrations');
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn(['is_pre_orientation_survey_complete', 'is_post_orientation_survey_complete']);
        });
    }
};
