<?php

use App\Services\Admin\OrientationService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orientations', function (Blueprint $table) {
            $table->date('end_date')->nullable()->after('start_time');
        });
        (new OrientationService())->fixEndDate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orientations', function (Blueprint $table) {
            $table->dropColumn(['end_date']);
        });
    }
};
