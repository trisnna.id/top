<?php

use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_orientations', function (Blueprint $table) {
            $table->json(PreOrientationEntity::FIELD_INTAKES);
            $table->json(PreOrientationEntity::FIELD_FACULTIES);
            $table->json(PreOrientationEntity::FIELD_SCHOOLS);
            $table->json(PreOrientationEntity::FIELD_PROGRAMMES);
            $table->json(PreOrientationEntity::FIELD_MOBILITIES);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_orientations', function (Blueprint $table) {
            $table->dropColumn(PreOrientationEntity::FIELD_INTAKES);
            $table->dropColumn(PreOrientationEntity::FIELD_FACULTIES);
            $table->dropColumn(PreOrientationEntity::FIELD_SCHOOLS);
            $table->dropColumn(PreOrientationEntity::FIELD_PROGRAMMES);
            $table->dropColumn(PreOrientationEntity::FIELD_MOBILITIES);
        });
    }
};
