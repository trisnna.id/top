<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique();

            $table->string('name');
            $table->string('id_number')->unique();

            $table->string('gender');
            $table->string('country_from')->nullable();
            $table->string('nationality')->nullable();
            $table->unsignedBigInteger('programme_id')->index();
            $table->foreign('programme_id')->references('id')->on('programmes')->onDelete('cascade');
            $table->string('programme_code')->nullable();
            $table->string('programme_name')->nullable();
            $table->unsignedBigInteger('faculty_id')->nullable()->index();
            $table->foreign('faculty_id')->references('id')->on('faculties')->onDelete('set null')->onUpdate('cascade');
            $table->string('faculty_code')->nullable();
            $table->string('faculty_name')->nullable();
            $table->unsignedBigInteger('school_id')->nullable()->index();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('set null')->onUpdate('cascade');
            $table->string('school_code')->nullable();
            $table->string('school_name')->nullable();
            $table->unsignedBigInteger('level_study_id')->nullable()->index();
            $table->foreign('level_study_id')->references('id')->on('level_studies')->onDelete('set null')->onUpdate('cascade');
            $table->string('level_of_study')->nullable();
            $table->string('course_status')->nullable();

            $table->unsignedBigInteger('intake_id')->nullable()->index();
            $table->foreign('intake_id')->references('id')->on('intakes')->onDelete('set null')->onUpdate('cascade');
            $table->string('intake_number')->nullable();
            $table->string('current_semester')->nullable();
            $table->string('study_mode')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('taylors_email');
            $table->string('personal_email')->nullable();

            $table->unsignedBigInteger('locality_id')->nullable()->index();
            $table->foreign('locality_id')->references('id')->on('localities')->onDelete('set null')->onUpdate('cascade');
            $table->string('locality');
            $table->unsignedBigInteger('campus_id')->nullable()->index();
            $table->foreign('campus_id')->references('id')->on('campuses')->onDelete('set null')->onUpdate('cascade');
            $table->string('campus')->nullable();
            $table->unsignedBigInteger('mobility_id')->nullable()->index();
            $table->foreign('mobility_id')->references('id')->on('mobilities')->onDelete('set null')->onUpdate('cascade');
            $table->string('mobility')->nullable();

            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
};
