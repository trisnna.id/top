<?php

use App\Contracts\Entities\AnnouncementEntity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcements', function (Blueprint $table) {
            $table->string(AnnouncementEntity::FIELD_THUMBNAIL)->nullable()->after(AnnouncementEntity::FIELD_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcements', function (Blueprint $table) {
                $table->dropColumn(AnnouncementEntity::FIELD_THUMBNAIL);
        });
    }
};
