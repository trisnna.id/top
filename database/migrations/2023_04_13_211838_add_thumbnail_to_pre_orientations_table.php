<?php

use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_orientations', function (Blueprint $table) {
            $table->string(PreOrientationEntity::FIELD_THUMBNAIL)->nullable()->after(PreOrientationEntity::FIELD_ACTIVITY_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_orientations', function (Blueprint $table) {
            $table->dropColumn(PreOrientationEntity::FIELD_THUMBNAIL);
        });
    }
};
