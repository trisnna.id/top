<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_intake_views', function (Blueprint $table) {
            $table->id();
            $table->string('course_code')->nullable();
            $table->string('course_description')->nullable();
            $table->string('intake_number')->nullable();
            $table->string('school')->nullable();
            $table->string('faculty')->nullable();
            $table->date('sync_date')->nullable();
            $table->timestamps();
            $table->unique(['course_code', 'course_description', 'intake_number', 'school', 'faculty'], 'course_intake');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_intake_views');
    }
};
