<?php

use App\Contracts\Entities\AnnouncementEntity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcements', function (Blueprint $table) {
            if (Schema::hasColumn('announcements', AnnouncementEntity::FIELD_CATEGORY)) {
                $table->dropForeign([AnnouncementEntity::FIELD_CATEGORY]);
                $table->dropIndex([AnnouncementEntity::FIELD_CATEGORY]);
                $table->dropColumn(AnnouncementEntity::FIELD_CATEGORY);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcements', function (Blueprint $table) {
            $table->unsignedBigInteger(AnnouncementEntity::FIELD_CATEGORY);
            $table->foreign(AnnouncementEntity::FIELD_CATEGORY)->references('id')->on('checkpoints');
            $table->index(AnnouncementEntity::FIELD_CATEGORY);
        });
    }
};
