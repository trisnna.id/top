<?php

use App\Services\Admin\Setting\MailService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mails', function (Blueprint $table) {
            $table->boolean('is_default')->default(false);
            $table->unsignedBigInteger('created_by')->nullable()->index();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->unsignedBigInteger('updated_by')->nullable()->index();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
        });
        (new MailService)->fixDataIsDefault();
        Schema::table('mail_logs', function (Blueprint $table) {
            $table->unsignedBigInteger('mail_id')->nullable()->index();
            $table->foreign('mail_id')->references('id')->on('mails')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mails', function (Blueprint $table) {
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
            $table->dropColumn([
                'is_default',
                'created_by',
                'updated_by',
            ]);
        });
        Schema::table('mail_logs', function (Blueprint $table) {
            $table->dropForeign(['mail_id']);
            $table->dropColumn([
                'mail_id',
            ]);
        });
    }
};
