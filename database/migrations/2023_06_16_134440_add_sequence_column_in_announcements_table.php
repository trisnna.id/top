<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Contracts\Entities\AnnouncementEntity;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(AnnouncementEntity::TABLE, function (Blueprint $table) {
            $table->char(AnnouncementEntity::FIELD_SEQUENCE)->after('id')->nullable()->index();

            $table->index(['id', AnnouncementEntity::FIELD_SEQUENCE]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(AnnouncementEntity::TABLE, function (Blueprint $table) {
            $table->dropColumn([AnnouncementEntity::FIELD_SEQUENCE]);
        });
    }
};
