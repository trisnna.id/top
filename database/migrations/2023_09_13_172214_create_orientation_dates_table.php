<?php

use App\Services\OrientationDateService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orientation_dates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('orientation_id')->index();
            $table->foreign('orientation_id')->references('id')->on('orientations')->onDelete('cascade');
            $table->date('date')->nullable();
            $table->timestamps();
        });
        (new OrientationDateService)->fixData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orientation_dates');
    }
};
