<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('programmes', function (Blueprint $table) {
            $table->unique(['name']);
            $table->dropColumn(['code', 'code_name']);
            $table->boolean('is_cms_active')->default(false);
        });
        Schema::table('schools', function (Blueprint $table) {
            $table->unique(['name']);
            $table->dropColumn(['code', 'code_name']);
            $table->boolean('is_cms_active')->default(false);
        });
        Schema::table('faculties', function (Blueprint $table) {
            $table->unique(['name']);
            $table->dropColumn(['code', 'code_name']);
            $table->boolean('is_cms_active')->default(false);
        });
        Schema::table('intakes', function (Blueprint $table) {
            $table->boolean('is_cms_active')->default(false);
        });
        Schema::table('campuses', function (Blueprint $table) {
            $table->boolean('is_cms_active')->default(false);
        });
        Schema::table('level_studies', function (Blueprint $table) {
            $table->boolean('is_cms_active')->default(false);
        });
        Schema::table('mobilities', function (Blueprint $table) {
            $table->boolean('is_cms_active')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programmes', function (Blueprint $table) {
            $table->dropUnique(['name']);
            $table->string('code');
            $table->string('code_name')->nullable();
            $table->dropColumn(['is_cms_active']);
        });
        Schema::table('schools', function (Blueprint $table) {
            $table->dropUnique(['name']);
            $table->string('code');
            $table->string('code_name')->nullable();
            $table->dropColumn(['is_cms_active']);
        });
        Schema::table('faculties', function (Blueprint $table) {
            $table->dropUnique(['name']);
            $table->string('code');
            $table->string('code_name')->nullable();
            $table->dropColumn(['is_cms_active']);
        });
        Schema::table('intakes', function (Blueprint $table) {
            $table->dropColumn(['is_cms_active']);
        });
        Schema::table('campuses', function (Blueprint $table) {
            $table->dropColumn(['is_cms_active']);
        });
        Schema::table('level_studies', function (Blueprint $table) {
            $table->dropColumn(['is_cms_active']);
        });
        Schema::table('mobilities', function (Blueprint $table) {
            $table->dropColumn(['is_cms_active']);
        });
    }
};
