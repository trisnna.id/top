<?php

use App\Contracts\Entities\StudentPreOrientationEntity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(StudentPreOrientationEntity::TABLE, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(StudentPreOrientationEntity::FIELD_PREORIENTATION);
            $table->unsignedBigInteger(StudentPreOrientationEntity::FIELD_STUDENT);
            $table->boolean(StudentPreOrientationEntity::FIELD_COMPLETED);
            $table->json(StudentPreOrientationEntity::FIELD_QUIZ)->nullable();
            $table->integer(StudentPreOrientationEntity::FIELD_SCORE);
            $table->timestamps();

            $table->foreign(StudentPreOrientationEntity::FIELD_PREORIENTATION)->references('id')->on('pre_orientations')->onDelete('cascade');
            $table->foreign(StudentPreOrientationEntity::FIELD_STUDENT)->references('id')->on('students')->onDelete('cascade');
            
            $table->index(StudentPreOrientationEntity::FIELD_PREORIENTATION, 'student_quiz_preorientation');
            $table->index(StudentPreOrientationEntity::FIELD_STUDENT, 'student_quiz_student_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(StudentPreOrientationEntity::TABLE);
    }
};
