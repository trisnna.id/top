<?php

use App\Contracts\Entities\StudentPreOrientationPercentageEntity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_preorientations_percentages', function (Blueprint $table) {
            $table->id();
            $table->uuid();

            $table->foreignId(StudentPreOrientationPercentageEntity::FIELD_PRE_ORIENTATION_ID)
                ->constrained('pre_orientations')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreignId(StudentPreOrientationPercentageEntity::FIELD_STUDENT_ID)
                ->constrained('students')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer(StudentPreOrientationPercentageEntity::FIELD_SCORE);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_preorientations_percentages');
    }
};
