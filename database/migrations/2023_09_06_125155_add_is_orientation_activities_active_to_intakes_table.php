<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('intakes', function (Blueprint $table) {
            $table->boolean('is_orientation_activities_active')->default(false);
            $table->string('orientation_activities_message')->nullable()->default('Dear Students, access to this section will be available soon.');
            $table->boolean('is_timetable_active')->default(false);
            $table->string('timetable_message')->nullable()->default('Dear Students, access to this section will be available soon.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('intakes', function (Blueprint $table) {
            $table->dropColumn([
                'is_orientation_activities_active',
                'orientation_activities_message',
                'is_timetable_active',
                'timetable_message',
            ]);
        });
    }
};
