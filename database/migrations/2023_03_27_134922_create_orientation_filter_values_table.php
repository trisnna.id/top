<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orientation_filter_values', function (Blueprint $table) {
            $table->unsignedBigInteger('orientation_id')->index();
            $table->foreign('orientation_id')->references('id')->on('orientations')->onDelete('cascade');
            $table->string('name');
            $table->unsignedBigInteger('value');
            $table->unique(['orientation_id', 'name', 'value']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orientation_filter_values');
    }
};
