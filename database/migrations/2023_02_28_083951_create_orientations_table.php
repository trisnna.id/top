<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orientations', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique();
            $table->string('name');
            $table->string('venue')->nullable();
            $table->string('livestream')->nullable();
            $table->date('start_date')->nullable();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->string('presenter')->nullable();
            $table->text('agenda')->nullable();
            $table->text('synopsis')->nullable();
            $table->unsignedBigInteger('orientation_type_id')->nullable()->index();
            $table->foreign('orientation_type_id')->references('id')->on('orientation_types')->onDelete('set null')->onUpdate('cascade');
            $table->boolean('is_rsvp')->default(false);
            $table->integer('rsvp_capacity')->nullable();
            $table->boolean('is_point')->default(false);
            $table->integer('point_max')->nullable();
            $table->integer('point_min')->nullable();
            $table->boolean('is_publish')->default(false);
            $table->integer('total_student')->default(0);
            $table->unsignedBigInteger('status_id')->nullable()->index();
            $table->string('status_name')->nullable();
            $table->unsignedBigInteger('created_by')->nullable()->index();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->unsignedBigInteger('updated_by')->nullable()->index();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orientations');
    }
};
