<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orientation_attendances', function (Blueprint $table) {
            $table->integer('point_rating')->default(0)->after('point');
            $table->integer('point_attendance')->default(0)->after('point');
        });
        Schema::table('orientations', function (Blueprint $table) {
            $table->integer('point_rating')->default(0)->after('point_min');
            $table->integer('point_attendance')->default(0)->after('point_min');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orientation_attendances', function (Blueprint $table) {
            $table->dropColumn(['point_rating', 'point_attendance']);
        });
        Schema::table('orientations', function (Blueprint $table) {
            $table->dropColumn(['point_rating', 'point_attendance']);
        });
    }
};
