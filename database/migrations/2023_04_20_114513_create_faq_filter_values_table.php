<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_filter_values', function (Blueprint $table) {
            $table->unsignedBigInteger('faq_id')->index();
            $table->foreign('faq_id')->references('id')->on('faqs')->onDelete('cascade');
            $table->string('name');
            $table->unsignedBigInteger('value');
            $table->unique(['faq_id', 'name', 'value']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_filter_values');
    }
};
