<?php

use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_orientations', function (Blueprint $table) {
            $table->date(PreOrientationEntity::FIELD_EFFECTIVE_DATE)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pre_orientations', function (Blueprint $table) {
            $table->date(PreOrientationEntity::FIELD_EFFECTIVE_DATE)->nullable(false)->change();
        });
    }
};
