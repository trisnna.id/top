<?php

use App\Contracts\Entities\PreOrientationEntity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_orientations', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->unsignedBigInteger(PreOrientationEntity::FIELD_CATEGORY);
            $table->string(PreOrientationEntity::FIELD_ACTIVITY_NAME, 100);
            $table->date(PreOrientationEntity::FIELD_EFFECTIVE_DATE);
            $table->json(PreOrientationEntity::FIELD_LOCALITIES);
            $table->enum(PreOrientationEntity::FIELD_STATUS, PreOrientationEntity::STATUSES)->default(PreOrientationEntity::STATUS_DRAFT);
            $table->longText(PreOrientationEntity::FIELD_CONTENT);
            $table->json(PreOrientationEntity::FIELD_CONTENT_LINKS);
            $table->timestamps();
            $table->softDeletes();

            // Relationships
            $table->foreign(PreOrientationEntity::FIELD_CATEGORY)->references('id')->on('checkpoints');

            // Indexing
            $table->index(PreOrientationEntity::FIELD_ACTIVITY_NAME);
            $table->index(PreOrientationEntity::FIELD_LOCALITIES);
            $table->index(PreOrientationEntity::FIELD_CATEGORY);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_orientations');
    }
};
