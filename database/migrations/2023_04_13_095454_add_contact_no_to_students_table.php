<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('flame_mentor_name')->nullable();
            $table->date('intake_start_date')->nullable();
            $table->date('recent_status_change_date')->nullable();
            $table->string('taylors_email')->nullable()->change();
            $table->unsignedBigInteger('programme_id')->nullable()->change();
            $table->dropForeign(['programme_id']);
            $table->foreign('programme_id')->references('id')->on('programmes')->onDelete('set null')->onUpdate('cascade');
            $table->dropColumn(['gender', 'country_from', 'programme_code', 'faculty_code', 'school_code', 'current_semester', 'study_mode']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('gender');
            $table->string('country_from')->nullable();
            $table->string('programme_code')->nullable();
            $table->string('faculty_code')->nullable();
            $table->string('school_code')->nullable();
            $table->string('current_semester')->nullable();
            $table->string('study_mode')->nullable();
            $table->dropColumn(['flame_mentor_name', 'intake_start_date', 'recent_status_change_date']);
        });
    }
};
