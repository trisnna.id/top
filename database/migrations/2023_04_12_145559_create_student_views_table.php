<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_views', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('student_id')->unique()->nullable();
            $table->string('faculty_name')->nullable();
            $table->string('school_name')->nullable();
            $table->string('intake_number')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('programme_name')->nullable();
            $table->string('student_level')->nullable();
            $table->string('taylors_email')->nullable();
            $table->string('personal_email')->nullable();
            $table->string('flame_mentor_name')->nullable();
            $table->string('campus')->nullable();
            $table->string('nationality')->nullable();
            $table->string('mobility')->nullable();
            $table->string('level_of_study')->nullable();
            $table->string('intake_start_date')->nullable();
            $table->string('recent_status_change_date')->nullable();
            $table->string('course_status')->nullable();
            $table->string('profile_status')->nullable();
            $table->date('sync_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_views');
    }
};
