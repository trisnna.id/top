<?php

use App\Contracts\Entities\AnnouncementEntity;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->string(AnnouncementEntity::FIELD_NAME);
            $table->json(AnnouncementEntity::FIELD_LOCALITIES);
            $table->longText(AnnouncementEntity::FIELD_CONTENT);
            $table->json(AnnouncementEntity::FIELD_CONTENT_LINKS)->nullable();
            $table->json(AnnouncementEntity::FIELD_INTAKES);
            $table->json(AnnouncementEntity::FIELD_CAMPUSES);
            $table->unsignedBigInteger(AnnouncementEntity::FIELD_CATEGORY);
            $table->json(AnnouncementEntity::FIELD_PROGRAMMES);
            $table->enum(AnnouncementEntity::FIELD_STATUS, AnnouncementEntity::STATUSES)->default(AnnouncementEntity::STATUS_DRAFT);
            $table->timestamps();
            $table->softDeletes();
            
            // Relationships
            $table->foreign(AnnouncementEntity::FIELD_CATEGORY)->references('id')->on('checkpoints');

            // Indexing
            $table->index(AnnouncementEntity::FIELD_NAME);
            $table->index(AnnouncementEntity::FIELD_LOCALITIES);
            $table->index(AnnouncementEntity::FIELD_CATEGORY);
            $table->index(AnnouncementEntity::FIELD_INTAKES);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
};
