<?php

namespace Database\Seeders;

use App\Services\Admin\Setting\MailService;
use Illuminate\Database\Seeder;

class MailSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mails = [
            [
                'name' => 'orientation-approval',
                'title' => 'Send email to Activity Manager for orientation approval',
                'subject' => 'Your receive approval',
                'content' => '"<p>Dear <strong>{{userName}},</strong></p>\n<p>You receive approval, \"{{orientationName}}\".</p>\n<p>Click <a href=\"{{orientationUrl}}\">here </a>to take an action.</p>"',
                'notification' => 'A new Orientation Activity titled: {{orientationName}}, has been submitted by {{orientationCreatedBy}} and is pending your approval.',
            ],
            [
                'name' => 'orientation-approval-correction',
                'title' => 'Send email to Activity Manager for orientation approval correction',
                'subject' => 'Your receive approval (Correction)',
                'content' => '"<p>Dear <strong>{{userName}},</strong></p>\n<p>You receive approval correction, \"{{orientationName}}\".</p>\n<p>Click <a href=\"{{orientationUrl}}\">here </a>to take an action.</p>"',
                'notification' => 'A correction Orientation Activity titled: {{orientationName}}, has been submitted by {{orientationCreatedBy}} and is pending your approval.',
            ],
            [
                'name' => 'orientation-approve',
                'title' => 'Send email to Activity Editor for orientation approve',
                'subject' => 'Your application was approved',
                'content' => '"<p>Dear <strong>{{userName}},</strong></p>\n<p>Your orientation activity \"{{orientationName}}\" is approve.</p>\n<p>Click <a href=\"{{orientationUrl}}\">here </a>to see the details.</p>"',
                'notification' => 'Your Orientation Activity titled: {{orientationName}}, has been approved.',
            ],
            [
                'name' => 'orientation-cancel',
                'title' => 'Send email to Activity Editor for orientation cancel',
                'subject' => 'Your application was cancelled',
                'content' => '"<p>Dear <strong>{{userName}},</strong></p>\n<p>Your orientation activity \"{{orientationName}}\" is cancel.</p>\n<p>Click <a href=\"{{orientationUrl}}\">here </a>to see the details.</p>"',
                'notification' => 'Your Orientation Activity titled: {{orientationName}}, has been cancelled.',
            ],
            [
                'name' => 'orientation-correction',
                'title' => 'Send email to Activity Editor for orientation correction',
                'subject' => 'Your application have correction',
                'content' => '"<p>Dear <strong>{{userName}},</strong></p>\n<p>Your orientation activity \"{{orientationName}}\" have correction.</p>\n<p>Click <a href=\"{{orientationUrl}}\">here </a>to see the details.</p>"',
                'notification' => 'Your Orientation Activity titled: {{orientationName}}, has a correction.',
            ],
            [
                'name' => 'orientation-student',
                'title' => 'Send email to Student for orientation updated',
                'subject' => 'The orientation activity has been updated',
                'content' => '""',
                'notification' => 'The Orientation Activity titled: {{orientationName}}, has has been updated.',
            ],
            [
                'name' => 'student-assign-orientation-leader',
                'title' => 'Send email to Student after been assigned as Orientation Leader',
                'subject' => 'You been assign as Orientation Leader',
                'content' => '"<p>Dear <strong>{{receiver}},</strong></p>\n<p>You been assign as Orientation Leader.</p>\n<p>Click <a href=\"{{link}}\">here </a>to go Dashboard.</p>"',
                'notification' => 'You been assign as Orientation Leader',
            ],
        ];
        foreach ($mails as $mail) {
            (new MailService())->updateOrCreate($mail);
        }
    }
}
