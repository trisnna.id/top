<?php

namespace Database\Seeders;

use App\Models\Mentor;
use App\Models\Programme;
use App\Models\Role;
use App\Models\Staff;
use App\Models\Student;
use App\Models\User;
use App\Services\Admin\StudentService;
use Carbon\Carbon;
use Database\Seeders\RolePermissionSeeder;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\PermissionRegistrar;

class DefaultCredentialSeeder extends Seeder
{
    protected $domainEmail = 'top';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new RolePermissionSeeder())->roleSeeder();
        $this->userSeeder();
    }

    public function userSeeder()
    {
        $faker = Container::getInstance()->make(Generator::class);
        $totalProgramme = Programme::count();
        $roles['admin'] = Role::findByName('admin');
        $roles['activity_manager'] = Role::findByName('activity_manager');
        $roles['staff'] = Role::findByName('staff');
        $users = [
            [
                'uuid' => uniqueUuidByModel(new User()), 'name' => $faker->name, 'email' => "admin@{$this->domainEmail}",
                'email_verified_at' => Carbon::now(), 'password' => Hash::make('password'),
                'role_id' => $roles['admin']->id
            ],
            [
                'uuid' => uniqueUuidByModel(new User()), 'name' => $faker->name, 'email' => "activity_manager1@{$this->domainEmail}",
                'email_verified_at' => Carbon::now(), 'password' => Hash::make('password'),
                'role_id' => $roles['activity_manager']->id
            ],
            [
                'uuid' => uniqueUuidByModel(new User()), 'name' => $faker->name, 'email' => "staff1@{$this->domainEmail}",
                'email_verified_at' => Carbon::now(), 'password' => Hash::make('password'),
                'role_id' => $roles['staff']->id
            ],
            [
                'uuid' => uniqueUuidByModel(new User()), 'name' => $faker->name, 'email' => "staff2@{$this->domainEmail}",
                'email_verified_at' => Carbon::now(), 'password' => Hash::make('password'),
                'role_id' => $roles['staff']->id
            ],
            [
                'uuid' => uniqueUuidByModel(new User()), 'name' => $faker->name, 'email' => "staff3@{$this->domainEmail}",
                'email_verified_at' => Carbon::now(), 'password' => Hash::make('password'),
                'role_id' => $roles['staff']->id
            ],
            [
                'uuid' => uniqueUuidByModel(new User()), 'name' => $faker->name, 'email' => "staff4@{$this->domainEmail}",
                'email_verified_at' => Carbon::now(), 'password' => Hash::make('password'),
                'role_id' => $roles['staff']->id
            ],
        ];
        User::upsertUniqueBy($users, ['email']);
        foreach ($users as $user) {
            $user = User::with(['role'])->where('email', $user['email'])->first();
            $user->setMeta('color', randomClassColor());
            $user->saveMeta();
            $user->assignRole($user->role->name);
        }
        $userRoleLecturers = User::where('role_id', $roles['staff']->id)->get();
        foreach ($userRoleLecturers as $userRoleLecturer) {
            $staffs = [
                [
                    'uuid' => uniqueUuidByModel(new Staff()),
                    'name' => $userRoleLecturer->name,
                    'id_number' => rand(100000, 999999),
                    'taylors_email' => $userRoleLecturer->email,
                    'user_id' => $userRoleLecturer->id,
                ]
            ];
            Staff::upsertUniqueBy($staffs, ['user_id']);
            $staff = Staff::where('taylors_email', $userRoleLecturer->email)->first();
            $userRoleLecturer->update([
                'model_type' => Staff::class,
                'model_id' => $staff->id,
            ]);

            // $mentors = [
            //     [
            //         'uuid' => uniqueUuidByModel(new Mentor()),
            //         'model_type' => Staff::class,
            //         'model_id' => $staff->id,
            //         'content' => '<div class="card"> <div class="card-body p-lg-20"> <div class="mb-17"> <h3 class="text-dark mb-7">Latest Tutorial Blog</h3> <div class="separator separator-dashed mb-9"></div> <div class="row"> <div class="col-md-6"> <div class="h-100 d-flex flex-column justify-content-between pe-lg-6 mb-lg-0 mb-10"> <div class="mb-3"> <iframe class="embed-responsive-item card-rounded h-275px w-100" src="https://www.youtube.com/embed/TWdDZYNqlg4" allowfullscreen="allowfullscreen"></iframe> </div> <div class="mb-5"> <a href="#" class="fs-2 text-dark fw-bold text-hover-primary text-dark lh-base">Admin Panel - How To Get Started Tutorial. <br>Create easy customizable applications</a> <div class="fw-semibold fs-5 text-gray-600 text-dark mt-4">We’ve been focused on making the from v4 to v5 but we have also not been afraid to step away been focused on from v4 to v5 speaker approachable making focused a but from a step away afraid to step away been focused Writing a blog post is a little like driving; you can study the highway code (or read articles telling you how to write a blog post) for months, but nothing can prepare you for the real thing like getting behind the wheel</div> </div> <div class="d-flex flex-stack flex-wrap"> <div class="d-flex align-items-center pe-2"> </div> <span class="badge badge-light-primary fw-bold my-2">TUTORIALS</span> </div> </div> </div> <div class="col-md-6 justify-content-between d-flex flex-column"> <div class="ps-lg-6 mb-16 mt-md-0 mt-17"> <div class="mb-6"> <a href="#" class="fw-bold text-dark mb-4 fs-2 lh-base text-hover-primary">Bootstrap Admin Theme - How To Get Started Tutorial. Create customizable applications</a> <div class="fw-semibold fs-5 mt-4 text-gray-600 text-dark">We’ve been focused on making the from v4 to v5 a but we’ve also not been afraid to step away been focused on from v4 to v5 speaker approachable making focused</div> </div> <div class="d-flex flex-stack flex-wrap"> <div class="d-flex align-items-center pe-2"> </div> <span class="badge badge-light-info fw-bold my-2">BLOG</span> </div> </div> <div class="ps-lg-6 mb-16"> <div class="mb-6"> <a href="#" class="fw-bold text-dark mb-4 fs-2 lh-base text-hover-primary">Angular Admin Theme - How To Get Started Tutorial.</a> <div class="fw-semibold fs-5 mt-4 text-gray-600 text-dark">We’ve been focused on making the from v4 to v5 a but we’ve also not been afraid to step away</div> </div> <div class="d-flex flex-stack flex-wrap"> <div class="d-flex align-items-center pe-2"> </div> <span class="badge badge-light-primary fw-bold my-2">TUTORIALS</span> </div> </div> <div class="ps-lg-6"> <div class="mb-6"> <a href="#" class="fw-bold text-dark mb-4 fs-2 lh-base text-hover-primary">React Admin Theme - How To Get Started Tutorial. Create best applications</a> <div class="fw-semibold fs-5 mt-4 text-gray-600 text-dark">We’ve been focused on making the from v4 to v5 but we’ve also not been afraid to step away been focused</div> </div> <div class="d-flex flex-stack flex-wrap"> <div class="d-flex align-items-center pe-2"> </div> <span class="badge badge-light-warning fw-bold my-2">NEWS</span> </div> </div> </div> </div> </div> </div> </div>',
            //         'is_publish' => rand(0, 1),
            //     ]
            // ];
            // Mentor::upsertUniqueBy($mentors, ['model_type', 'model_id']);
        }
        // $students = (new StudentService())->upserts(Student::factory()->count(30)->make()->toArray());
        // foreach ($students['data'] as $key => $student) {
        //     $email = "student" . ($key + 1) . "@top";
        //     $student->update([
        //         'taylors_email' => $email
        //     ]);
        //     $student->load('user');
        //     $student->user->update([
        //         'email' => $email
        //     ]);
        // }
        app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
        return User::all();
    }
}
