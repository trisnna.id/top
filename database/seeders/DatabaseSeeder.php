<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolePermissionSeeder::class,
            DefaultCredentialSeeder::class,
            OrientationSeeder::class,
            MailSeeder::class,
            PageSeeder::class,
            // TODO: Remove
            // ClubSocietySeeder::class,
            ResourceSeeder::class,
            // ActivitySeeder::class,
            // AttendanceSeeder::class,
            // SurveySeeder::class,
            // FaqsSeeder::class,
            CheckpointsSeeder::class,
            SettingSeeder::class,
            // ResourceVideosSeeder::class,
            // AnnouncementSeeder::class,
        ]);
    }
}
