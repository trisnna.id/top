<?php

namespace Database\Seeders;

use App\Constants\ActivityTypeConstant;
use App\Models\Attendance;
use App\Models\Activity;
use App\Models\Student;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AttendanceSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->attendanceSeeder();
    }

    public function attendanceSeeder()
    {
        $faker = Factory::create();
        $attendances = [];
        $totalStudent = Student::count();
        $totalActivity = Activity::count();
        for ($i = 0; $i < 100; $i++) {
            array_push($attendances, [
                'uuid' => uniqueUuidByModel(new Attendance()),
                'student_id' => rand(1, $totalStudent),
                'activity_id' => rand(1, $totalActivity),
                'attendance_at' => Carbon::now()->subDays(rand(1, 10))->subMinutes(rand(1, 60)),
                'note' => $faker->text
            ]);
        }
        Attendance::upsertUniqueBy($attendances, ['student_id', 'activity_id']);
    }
}
