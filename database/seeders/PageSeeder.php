<?php

namespace Database\Seeders;

use App\Services\Admin\Setting\PageService;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = [
            [
                'name' => 'policy',
                'title' => 'Privacy Policy',
                'content' => '"<p style=\"text-align: center;\"><strong>COMING SOON</strong></p>"',
            ],
            [
                'name' => 'disclaimer',
                'title' => 'Disclaimers',
                'content' => '"<p style=\"text-align: center;\"><strong>COMING SOON</strong></p>"',
            ],
        ];
        foreach ($pages as $page) {
            (new PageService())->updateOrCreate($page);
        }
    }
}
