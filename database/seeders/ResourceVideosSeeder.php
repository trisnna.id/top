<?php

namespace Database\Seeders;

use App\Models\Resource;
use App\Models\ResourceType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ResourceVideosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['uuid' => uniqueUuidByModel(new ResourceType()), 'name' => ResourceType::RESOURCE_VIDEO_FILE],
            ['uuid' => uniqueUuidByModel(new ResourceType()), 'name' => ResourceType::RESOURCE_VIDEO_LINK],
        ];
        ResourceType::upsertUniqueBy($categories, ['name']);

        for ($i = 0; $i < 10; $i++) {
            if ($i % 2 === 0) {
                $type = ResourceType::where('name', ResourceType::RESOURCE_VIDEO_FILE)->first();
                Resource::factory()->create(['resource_type_id' => $type, 'value' => randomUrlFile(0)]);
            } else {
                $type = ResourceType::where('name', ResourceType::RESOURCE_VIDEO_LINK)->first();
                Resource::factory()->create(['resource_type_id' => $type, 'value' => 'https://www.youtube.com/embed/PAPPoANdeS0']);
            }
        }
    }
}
