<?php

namespace Database\Seeders;

use App\Constants\ActivityTypeConstant;
use App\Models\Activity;
use App\Models\Survey;
use App\Models\SurveyAnswer;
use App\Models\ActivityType;
use App\Models\SurveyAnswerStatus;
use App\Models\Programme;
use App\Constants\SurveyAnswerStatusConstant;
use App\Models\Student;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

class SurveySeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->surveySeeder();
        $this->surveyAnswerStatusSeeder();
        $this->surveyAnswerSeeder();
    }

    public function surveyAnswerStatusSeeder()
    {
        $types = [];
        foreach (SurveyAnswerStatusConstant::getAvailableOptions() as $key => $value) {
            array_push($types, [
                'id' => $key, 'uuid' => uniqueUuidByModel(new SurveyAnswerStatus()), 'name' => Str::slug($value), 'title' => $value
            ]);
        }
        SurveyAnswerStatus::upsertUniqueBy($types, ['id']);
    }

    public function surveySeeder()
    {
        $faker = Factory::create();
        $surveys = [];
        for ($i = 0; $i < 20; $i++) {
            $startAt = Carbon::now()->subDays(rand(8, 12))->subMinutes(rand(1, 60));
            $endAt = $startAt->copy()->addDays(rand(3, 5));
            array_push($surveys, [
                'uuid' => uniqueUuidByModel(new Survey()),
                "name" => $faker->sentence,
                "description" => $faker->text,
                "start_at" => $startAt,
                "end_at" => $endAt,
            ]);
        }
        Survey::upsertUniqueBy($surveys, ['uuid']);
    }

    public function surveyAnswerSeeder()
    {
        $faker = Factory::create();

        $totalStudent = Student::count();
        $totalSurvey = Survey::count();
        $totalSurveyAnswerStatus = SurveyAnswerStatus::count();
        $surveyAnswers = [];
        for ($i = 0; $i < 20; $i++) {
            array_push($surveyAnswers, [
                'uuid' => uniqueUuidByModel(new SurveyAnswer()),
                "student_id" => rand(1, $totalStudent),
                "survey_id" => rand(1, $totalSurvey),
                "survey_answer_status_id" => rand(2, $totalSurveyAnswerStatus),
            ]);
        }
        SurveyAnswer::upsertUniqueBy($surveyAnswers, ['student_id', 'survey_id']);
    }
}
