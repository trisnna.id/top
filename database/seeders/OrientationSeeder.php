<?php

namespace Database\Seeders;

use App\Constants\OrientationTypeConstant;
use App\Models\OrientationType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class OrientationSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->orientationTypeSeeder();
    }

    public function orientationTypeSeeder()
    {
        $types = [];
        foreach (getConstant('OrientationTypeConstant', 'title') as $key => $value) {
            array_push($types, [
                'id' => $key, 'uuid' => uniqueUuidByModel(new OrientationType()), 'name' => Str::slug($value), 'title' => $value
            ]);
        }
        OrientationType::upsert($types, ['name']);
    }
}
