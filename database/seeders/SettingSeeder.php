<?php

namespace Database\Seeders;

use App\Constants\SettingConstant;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        setting([
            'orientation' => SettingConstant::ORIENTATION,
            'file' => SettingConstant::FILE,
            'portal' => SettingConstant::PORTAL,
            'preorientation' => SettingConstant::PREORIENTATION,
            'survey' => SettingConstant::SURVEY,
            'quicklink' => SettingConstant::QUICKLINK,
        ])->save();
    }
}
