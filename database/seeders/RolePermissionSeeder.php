<?php

namespace Database\Seeders;

use App\Constants\RoleConstant;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\Permission\PermissionRegistrar;

class RolePermissionSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->roleSeeder();
        $this->permissionSeeder();
    }

    public function permissionSeeder()
    {
        $rolePermissions = [];
        // level 1 permission
        $level1Permissions = [
            ['uuid' => uniqueUuidByModel(new Permission()), 'title' => $title = 'Admin', 'name' => $this->nameRolePermissions($title, $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]),  'guard_name' => 'web', 'weight' => 1],
            ['uuid' => uniqueUuidByModel(new Permission()), 'title' => $title = 'Student', 'name' => $this->nameRolePermissions($title, $rolePermissions, [RoleConstant::ROLE_STUDENT, RoleConstant::ROLE_ORIENTATION_LEADER]),  'guard_name' => 'web', 'weight' => 1],
        ];
        Permission::upsertUniqueBy($level1Permissions, ['name', 'guard_name']);
        $level1Permissions = Permission::where('weight', 1)->get();
        $level1PermissionIds['student'] = $level1Permissions->where('name', 'student')->first()->id;
        $level1PermissionIds['admin'] = $level1Permissions->where('name', 'admin')->first()->id;
        // level 2 permission
        $level2Permissions = [
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['student'], 'title' => $title = 'Orientation Leader', 'name' => $this->nameRolePermissions($title, $rolePermissions, [RoleConstant::ROLE_ORIENTATION_LEADER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'User Management', 'name' => $this->nameRolePermissions('Manage Users', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Role Management', 'name' => $this->nameRolePermissions('Manage Roles', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Resources', 'name' => $this->nameRolePermissions('Manage Resources', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'News & Announcements', 'name' => $this->nameRolePermissions('Manage Announcements', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Pre-Orientation', 'name' => $this->nameRolePermissions('Manage Pre-Orientations', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Mini Quiz Maker', 'name' => $this->nameRolePermissions('Manage Quizzes', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Orientation', 'name' => $this->nameRolePermissions('Manage Orientations', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Activity Approval', 'name' => $this->nameRolePermissions('Manage Activity Approval', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Student List', 'name' => $this->nameRolePermissions('Manage Student Lists', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Student Login Details', 'name' => $this->nameRolePermissions('Access Report Login', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Audit Trails', 'name' => $this->nameRolePermissions('Access Report Audit', $rolePermissions, [RoleConstant::ROLE_ADMIN]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Student Attendance', 'name' => $this->nameRolePermissions('Access Report Attendance', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Student Registration', 'name' => $this->nameRolePermissions('Access Report Registration', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Student Activity', 'name' => $this->nameRolePermissions('Access Report Activity', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Student Survey', 'name' => $this->nameRolePermissions('Access Report Survey', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Student Gamification', 'name' => $this->nameRolePermissions('Access Report Gamification', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF, RoleConstant::ROLE_ACTIVITY_MANAGER]), 'guard_name' => 'web', 'weight' => 2],
            ['uuid' => uniqueUuidByModel(new Permission()), 'parent_id' => $level1PermissionIds['admin'], 'title' => 'Settings', 'name' => $this->nameRolePermissions('Manage Settings', $rolePermissions, [RoleConstant::ROLE_ADMIN, RoleConstant::ROLE_STAFF]), 'guard_name' => 'web', 'weight' => 2],
        ];
        Permission::upsertUniqueBy($level2Permissions, ['name', 'guard_name']);
        app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
        $roles[RoleConstant::ROLE_ADMIN] = Role::findByName(RoleConstant::ROLE_ADMIN)->syncPermissions($rolePermissions[RoleConstant::ROLE_ADMIN]);
        $roles[RoleConstant::ROLE_STAFF] = Role::findByName(RoleConstant::ROLE_STAFF)->syncPermissions($rolePermissions[RoleConstant::ROLE_STAFF]);
        $roles[RoleConstant::ROLE_ACTIVITY_MANAGER] = Role::findByName(RoleConstant::ROLE_ACTIVITY_MANAGER)->syncPermissions($rolePermissions[RoleConstant::ROLE_ACTIVITY_MANAGER]);
        $roles[RoleConstant::ROLE_STUDENT] = Role::findByName(RoleConstant::ROLE_STUDENT)->syncPermissions($rolePermissions[RoleConstant::ROLE_STUDENT]);
        $roles[RoleConstant::ROLE_ORIENTATION_LEADER] = Role::findByName(RoleConstant::ROLE_ORIENTATION_LEADER)->syncPermissions($rolePermissions[RoleConstant::ROLE_ORIENTATION_LEADER]);
        app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
    }

    protected function nameRolePermissions($name, &$rolePermissions = [], $roles = [])
    {
        $name = Str::slug($name, '_');
        foreach ($roles as $role) {
            $rolePermissions[$role] = [...$rolePermissions[$role] ?? [], $name] ?? [$name];
        }
        return $name;
    }

    public function roleSeeder()
    {
        $roles = [
            [
                'uuid' => uniqueUuidByModel(new Role()), 'name' => RoleConstant::ROLE_ADMIN, 'title' => 'Admin',
                'is_active' => true, 'guard_name' => 'web'
            ],
            [
                'uuid' => uniqueUuidByModel(new Role()), 'name' => RoleConstant::ROLE_STAFF, 'title' => 'Staff',
                'is_active' => true, 'guard_name' => 'web'
            ],
            [
                'uuid' => uniqueUuidByModel(new Role()), 'name' => RoleConstant::ROLE_ACTIVITY_MANAGER, 'title' => 'Activity Manager',
                'is_active' => true, 'guard_name' => 'web'
            ],
            [
                'uuid' => uniqueUuidByModel(new Role()), 'name' => RoleConstant::ROLE_STUDENT, 'title' => 'Student',
                'is_active' => true, 'guard_name' => 'web'
            ],
            [
                'uuid' => uniqueUuidByModel(new Role()), 'name' => RoleConstant::ROLE_ORIENTATION_LEADER, 'title' => 'Orientation Leader',
                'is_active' => true, 'guard_name' => 'web'
            ],
        ];
        Role::upsertUniqueBy($roles, ['name', 'guard_name']);
        return Role::all();
    }
}
