<?php

namespace Database\Seeders;

use App\Constants\ActivityTypeConstant;
use App\Models\Activity;
use App\Models\ActivityType;
use App\Models\Programme;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

class ActivitySeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->activityTypeSeeder();
        $this->activitySeeder();
    }

    public function activityTypeSeeder()
    {
        $types = [];
        foreach (ActivityTypeConstant::getAvailableOptions() as $key => $value) {
            array_push($types, [
                'id' => $key, 'uuid' => uniqueUuidByModel(new ActivityType()), 'name' => Str::slug($value), 'title' => $value
            ]);
        }
        ActivityType::upsertUniqueBy($types, ['id']);
    }

    public function activitySeeder()
    {
        $faker = Factory::create();
        $totalProgramme = Programme::count();
        $totalActivityType = ActivityType::count();
        $activities = [];
        for ($i = 0; $i < 20; $i++) {
            $registrationStartAt = Carbon::now()->subDays(rand(8, 12))->subMinutes(rand(1, 60));
            $registrationCloseAt = $registrationStartAt->copy()->addDays(rand(3, 5));
            $activityStartAt = $registrationCloseAt->copy()->addDays(rand(5, 10));
            $activityCloseAt = $activityStartAt->copy()->addDays(rand(0, 3));
            array_push($activities, [
                'uuid' => uniqueUuidByModel(new Activity()),
                "activity_type_id" => rand(1, $totalActivityType),
                "name" => $faker->sentence,
                "description" => $faker->text,
                "registration_start_at" => $registrationStartAt,
                "registration_close_at" => $registrationCloseAt,
                "start_at" => $activityStartAt,
                "end_at" => $activityCloseAt,
                "location" => $faker->address,
                "programme_id" => rand(1, $totalProgramme),
            ]);
        }
        Activity::upsertUniqueBy($activities, ['name']);
    }
}
