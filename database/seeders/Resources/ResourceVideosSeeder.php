<?php

namespace Database\Seeders\Resources;

use App\Models\Resource;
use Illuminate\Database\Seeder;

class ResourceVideosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(...$params)
    {
        $user = $params[0];
        $pathToFile = 'media/others/sample.mp4';
        $totalOfResourceVideos = Resource::select('id')->type(Resource::TYPE_VIDEO)->count();
        
        if ($totalOfResourceVideos < 10) {
            for ($i = 0; $i < (10 - $totalOfResourceVideos); $i++) {
                Resource::factory()->create([
                    'created_by' => $user,
                    'updated_by' => null,
                    'type' => Resource::TYPE_VIDEO,
                    'value' => asset($pathToFile),
                ]);
            }
        }
    }
}
