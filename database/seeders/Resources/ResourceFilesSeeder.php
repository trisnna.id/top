<?php

namespace Database\Seeders\Resources;

use App\Models\Resource;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ResourceFilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(...$params)
    {
        $user = $params[0];
        $faker = Faker::create();
        $totalOfResourceFiles = Resource::select('id')->type(Resource::TYPE_FILE)->count();
        $pathToFiles = [
            'media/others/sample.docx',
            'media/others/sample.pdf',
        ];
        
        if ($totalOfResourceFiles < 10) {
            for ($i = 0; $i < (10 - $totalOfResourceFiles); $i++) {
                Resource::factory()->create([
                    'created_by' => $user,
                    'updated_by' => $faker->randomElement([null, $user]),
                    'type' => Resource::TYPE_FILE,
                    'value' => asset($faker->randomElement($pathToFiles))
                ]);
            }
        }
    }
}
