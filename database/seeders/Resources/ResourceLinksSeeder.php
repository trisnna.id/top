<?php

namespace Database\Seeders\Resources;

use App\Models\Resource;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class ResourceLinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(...$params)
    {
        $user = $params[0];
        $totalOfResourceLinks = Resource::select('id')->type(Resource::TYPE_LINK)->count();
        
        if ($totalOfResourceLinks < 15) {
            for ($i = 0; $i < (15 - $totalOfResourceLinks); $i++) {
                Resource::factory()->create([
                    'created_by' => $user,
                    'updated_by' => null,
                    'type' => Resource::TYPE_LINK,
                    'value' => Arr::random(['https://www.youtube.com/embed/PAPPoANdeS0', 'https://university.taylors.edu.my/en.html']),
                ]);
            }
        }
    }
}
