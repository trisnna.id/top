<?php

namespace Database\Seeders;

use App\Models\ResourceType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ResourceSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->resourceTypeSeeder();
    }

    public function resourceTypeSeeder()
    {
        $types = [];
        foreach (getConstant('ResourceTypeConstant') as $key => $value) {
            array_push($types, [
                'id' => $key, 'uuid' => uniqueUuidByModel(new ResourceType()), 'name' => Str::slug($value['name']), 'title' => $value['title']
            ]);
        }
        ResourceType::upsert($types, ['id']);
    }
}
