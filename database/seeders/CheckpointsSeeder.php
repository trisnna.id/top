<?php

namespace Database\Seeders;

use App\Models\Checkpoint;
use Illuminate\Database\Seeder;

class CheckpointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 6; $i++) { 
            Checkpoint::factory()->create([
                'label' => sprintf("%s {$i}", config('view.description.checkpoint', 'Checkpoint')),
                'value' => sprintf("%s {$i}", config('view.description.checkpoint', 'Checkpoint')),
            ]);
        }
    }
}
