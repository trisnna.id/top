<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-layouts.toolbar :page="__('announcements')" :title="$title" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl z-index-2 mt-10">
        <div class="row mb-xl-10 mb-5">
            @foreach ($announcements as $announcement)
                <div class="col-md-4 col-lg-4 col-xl-4 col-xxl-4 mb-md-5 mb-xl-10" id="ann-container">
                    <div class="card ann-card rounded border shadow" style="border-radius: 10% !important;">
                        <img class="card-img-top rounded-4"
                            src="{{ asset($announcement->thumbnail) == config('app.url') . '/' ? 'https://dummyimage.com/1920x1080/000/fff' : asset($announcement->thumbnail) }}" />
                        <div class="card-body d-flex justify-content-center flex-column">
                            <h3 class="card-title text-center"
                                style="font-family: Antic, sans-serif;color: rgb(81,87,94);" align="center">
                                {{ $announcement->name }}</h3>
                            {{-- <p class="card-text text-left">{!! substr(preg_replace('/<[^>]*>/', '', html_entity_decode($announcement->content)), 0, 50) . '...' !!}</p> --}}
                            <div class="d-flex justify-content-center mt-auto">
                                <a class="btn btn-sm btn-secondary me-2 btn-view-detail hover-scale"
                                    href="{{ route('announcement.show', ['slug' => $announcement->uuid]) }}">
                                    {{ __('View Detail') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            {{ $announcements->links() }}
        </div>
    </div>

    <style>
        .card-img-top {
            max-height: 200px;
            min-height: 200px;
            object-fit: cover;
        }

        #ann-container {
            border-color: inherit !important;
            border-width: inherit !important;
            border-style: inherit !important;
            text-transform: inherit !important;
            font-weight: inherit !important;
            font-size: inherit !important;
            color: inherit !important;
            height: inherit !important;
            min-height: inherit !important;
        }

        #ann-container>.card {
            height: 100% !important;
        }
    </style>

    @push('style')
        <style>
            .pagination>li>a,
            .pagination>li>span {
                color: #FF0000; // use your own color here
            }

            .pagination>.active>a,
            .pagination>.active>a:focus,
            .pagination>.active>a:hover,
            .pagination>.active>span,
            .pagination>.active>span:focus,
            .pagination>.active>span:hover {
                background-color: #FF0000;
                border-color: #FF0000;
            }
        </style>
    @endpush
</x-layouts.app>
