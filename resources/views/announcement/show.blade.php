<x-layouts.app :title="__('Resources')" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-layouts.toolbar :page="__('resources')" :title="$title" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-lg-20 pb-lg-0">
                        <!--begin::Layout-->
                        <div class="d-flex flex-column">
                            <!--begin::Content-->
                            <div class="flex-lg-row-fluid me-xl-15">
                                <!--begin::Post content-->
                                <div class="mb-17">
                                    <!--begin::Wrapper-->
                                    <div class="mb-8">
                                        <!--begin::Info-->
                                        <div class="d-flex mb-6 flex-wrap">
                                            <!--begin::Item-->
                                            <div class="me-9 my-1">
                                                <!--begin::Icon-->
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                                <span class="svg-icon svg-icon-primary svg-icon-2 me-1"><svg
                                                        width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <rect x="2" y="2" width="9"
                                                            height="9" rx="2" fill="currentColor"></rect>
                                                        <rect opacity="0.3" x="13" y="2"
                                                            width="9" height="9" rx="2"
                                                            fill="currentColor"></rect>
                                                        <rect opacity="0.3" x="13" y="13"
                                                            width="9" height="9" rx="2"
                                                            fill="currentColor"></rect>
                                                        <rect opacity="0.3" x="2" y="13"
                                                            width="9" height="9" rx="2"
                                                            fill="currentColor"></rect>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                                <!--end::Icon-->

                                                <!--begin::Label-->
                                                <span
                                                    class="fw-bold text-gray-400">{{ $announcement->publish_date }}</span>
                                                <!--end::Label-->
                                            </div>
                                            <!--end::Item-->

                                            <!--begin::Item-->
                                            <div class="me-9 my-1">
                                                <!--begin::Icon-->
                                                <!--SVG file not found: icons/duotune/finance/fin006.svgFolder.svg-->
                                                <!--end::Icon-->

                                                <!--begin::Label-->
                                                <span class="fw-bold text-gray-400">Announcements</span>
                                                <!--begin::Label-->
                                            </div>
                                            <!--end::Item-->

                                            <!--begin::Item-->
                                            {{-- <div class="my-1">
                                                <!--begin::Icon-->
                                                <!--begin::Svg Icon | path: icons/duotune/communication/com003.svg-->
                                                <span class="svg-icon svg-icon-primary svg-icon-2 me-1"><svg
                                                        width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path opacity="0.3"
                                                            d="M2 4V16C2 16.6 2.4 17 3 17H13L16.6 20.6C17.1 21.1 18 20.8 18 20V17H21C21.6 17 22 16.6 22 16V4C22 3.4 21.6 3 21 3H3C2.4 3 2 3.4 2 4Z"
                                                            fill="currentColor"></path>
                                                        <path
                                                            d="M18 9H6C5.4 9 5 8.6 5 8C5 7.4 5.4 7 6 7H18C18.6 7 19 7.4 19 8C19 8.6 18.6 9 18 9ZM16 12C16 11.4 15.6 11 15 11H6C5.4 11 5 11.4 5 12C5 12.6 5.4 13 6 13H15C15.6 13 16 12.6 16 12Z"
                                                            fill="currentColor"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                                <!--end::Icon-->

                                                <!--begin::Label-->
                                                <span class="fw-bold text-gray-400">XX Comments</span>
                                                <!--end::Label-->
                                            </div> --}}
                                            <!--end::Item-->
                                        </div>
                                        <!--end::Info-->

                                        <!--begin::Title-->
                                        <p class="text-dark fs-2 fw-bold">
                                            {{ $title }}

                                            {{-- <span class="fw-bold text-muted fs-5 ps-1">5 mins read</span> --}}
                                        </p>
                                        <!--end::Title-->

                                        <!--begin::Container-->
                                        <div class="overlay mt-8">
                                            <!--begin::Image-->
                                            {{-- <div class="bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-350px"
                                                style="background-image:url('{{ asset('media/stock/1600x800/img-1.jpg') }}')">
                                            </div> --}}
                                            <!--end::Image-->

                                            {{-- <!--begin::Links-->
                                            <div class="overlay-layer card-rounded bg-dark bg-opacity-25">
                                                <a href="/metronic8/demo1/../demo1/pages/about.html"
                                                    class="btn btn-primary">About
                                                    Us</a>
                                                <a href="/metronic8/demo1/../demo1/pages/careers/apply.html"
                                                    class="btn btn-light-primary ms-3">Join Us</a>
                                            </div>
                                            <!--end::Links--> --}}
                                        </div>
                                        <!--end::Container-->
                                    </div>
                                    <!--end::Wrapper-->

                                    <!--begin::Description-->
                                    <div id="announcementContent" class="fs-5 fw-semibold text-gray-600">
                                        <!--begin::Text-->
                                        {!! $announcement->content !!}
                                        <!--end::Text-->
                                    </div>
                                    <!--end::Description-->
                                </div>
                                <!--end::Post content-->
                            </div>
                            <!--end::Content-->

                            <!--begin::Sidebar-->
                            <div class="flex-column flex-lg-row-auto w-100 w-xl-300px mb-10">
                                <div class="mb-16">
                                    <h4 class="text-dark mb-7">Other Links</h4>
                                    @forelse (optional($announcement)->content_links as $contentLink)
                                        <div class="card-xl-stretch me-md-6">
                                            <a class="video-thumbnail d-block bgi-no-repeat bgi-size-cover bgi-position-center card-rounded position-relative min-h-175px mb-5"
                                                style="background-image:url('{{ $contentLink->thumbnail }}')"
                                                data-fslightbox="lightbox-video-tutorial"
                                                href="{{ $contentLink->link }}">

                                                <img src="{{ asset('media/svg/misc/video-play.svg') }}"
                                                    class="position-absolute top-50 start-50 translate-middle"
                                                    alt="">
                                            </a>
                                            <div class="m-0">
                                                <a href="/metronic8/demo1/../demo1/pages/user-profile/overview.html"
                                                    class="fs-4 text-dark fw-bold text-hover-primary text-dark lh-base">
                                                </a>
                                                <div class="fw-semibold fs-5 text-dark my-4 text-gray-600">
                                                    {!! $contentLink->text !!}
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="d-flex flex-stack fw-semibold fs-5 text-muted mb-4">
                                            <div class="m-0">
                                                <p class="text-muted">There is no other links</p>
                                            </div>
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                            <!--end::Sidebar-->
                        </div>
                        <!--end::Layout-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
