<div class="vh-100 dashboard-welcome bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom landing-dark-bg cover mb-0"
    style="background-image: url(media/taylors/bg-taylors3.svg);">
    <div class="landing-header" data-kt-sticky="false" data-kt-sticky-name="landing-header"
        data-kt-sticky-offset="{default: '50px'}">
        <div class="container mt-1">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center flex-equal">
                    <a class="brand-logo" href="#"
                        style="background-color: transparent !important; box-shadow: none !important;">
                        <img alt="Logo" src="{{ asset('media/logos/default-white.png') }}"
                            class="h-80px app-sidebar-logo-default p-2" />
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column flex-center w-100 min-h-350px min-h-lg-500px px-9">
        <div class="row justify-content-center container mb-20" style="position: absolute;bottom: 0;">
            <div class="col-7">
                <h1 class="lh-base fw-bold fs-2x fs-lg-3x text-center text-white m-0" style="font-family: 'Century Gothic','CenturyGothic','AppleGothic',sans-serif; ">{{ __('portal-heading') }}</h1>
                <h4 class="lh-base fs-1 text-white text-center" style="font-family: 'ArialNarrow', sans-serif;">{{ __('portal-subheading') }}</h4>
                @auth
                    <a href="{{ route('dashboard.index') }}" class="btn btn-danger btn-dashboard w-100">DASHBOARD<i
                            class="fa-solid fa-arrow-right" style="float: right;margin-top: 4px;"></i></a>
                @else
                    <x-swal.upsert :id="$instance['forgotPassword']['swal']['id']" :swalSettings="$instance['forgotPassword']['swal']['settings']" :route="route('password.email')">
                        @include('portal.index.welcome.forgot-password', ['id' => 'forgotPassword'])
                        <x-slot name="onOpen">
                            @include('portal.index.welcome.forgot-password-script', [
                                'id' => 'forgotPassword',
                            ])
                        </x-slot>
                    </x-swal.upsert>
                    <x-swal.upsert :id="$instance['help']['swal']['id']" :swalSettings="$instance['help']['swal']['settings']">
                        @include('portal.index.welcome.help', ['id' => 'help'])
                    </x-swal.upsert>
                    @include('portal.index.welcome.swal-login')
                    <a href="#" class="btn btn-danger btn-login upsert-swal-popup w-100" dusk="loginButton"
                        data-swal="{{ json_encode($instance['login']['swal']) }}">
                        <b>LOGIN</b><i class="fa-solid fa-right-to-bracket" style="float: right;margin-top: 4px;"></i>
                    </a>
                @endauth
            </div>
        </div>
    </div>
</div>
