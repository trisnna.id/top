<div class="mb-5 z-index-2">
    <div class="container">
        <div class="text-center mb-17">
            <h3 class="fs-2hx text-dark mb-5" id="how-it-works" data-kt-scroll-offset="{default: 100, lg: 150}">
                Shared services
            </h3>
            <div class="fs-5 fw-bold">
                {{fake()->text}}
            </div>
        </div>
        <div class="row g-5 g-xl-9 mb-5 mb-xl-9">
            <div class="col-sm-6 mb-6 mb-sm-0">
                <div class="m-0">
                    <div class="card-rounded position-relative mb-5 h-200px">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 mb-6 mb-sm-0">
                <div class="m-0">
                    <div class="card-rounded position-relative mb-5 h-200px">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/_e1doZGEzV0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>