<x-swal.upsert :id="$instance['login']['swal']['id']" :swalSettings="$instance['login']['swal']['settings']" :route="route('login')">
    <div class="bg-dark mb-10 mt-10 text-center text-white">
        <h2 class="m-0 py-2"
            style="color: rgba(var(--bs-white-rgb),var(--bs-text-opacity))!important; font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif;">
            Log in to
            {{ config('app.name') }}</h2>
    </div>
    <div class="fv-row mb-10">
        <div class="d-flex flex-stack mb-2">
            <label class="form-label fw-bolder text-dark fs-5 mb-0">Login using:</label>
        </div>
        <div class="form-check form-check-custom form-check-solid mb-3">
            <input class="form-check-input" name="type" type="radio" value="1" id="type1" checked />
            <label class="form-check-label text-black" for="type1">
                TOPAS Account
            </label>
        </div>
        @if (config('services.taylors.ldap.enabled'))
            <div class="form-check form-check-custom form-check-solid mb-3">
                <input class="form-check-input" name="type" type="radio" value="2" id="type2" />
                <label class="form-check-label text-black" for="type2">
                    Taylor’s Account
                </label>
            </div>
        @endif
        <div class="form-check form-check-custom form-check-solid">
            <input class="form-check-input" name="type" type="radio" value="3" id="type3" />
            <label class="form-check-label text-black" for="type3">
                Guest Account
            </label>
        </div>
    </div>
    <div class="fv-row mb-10">
        <label class="form-label fs-5 fw-bolder text-dark" for="username_email">User ID / Taylor’s Email</label>
        <input required class="form-control" type="email" name="username_email" autocomplete="off"
            placeholder="User ID / Taylor’s Email" />
    </div>
    <div class="fv-row mb-3">
        <div class="d-flex flex-stack mb-2">
            <label class="form-label fw-bolder text-dark fs-5 mb-0">Password</label>
        </div>
        <div class="input-group">
            <input required class="form-control" type="password" name="password" autocomplete="off"
            placeholder="**********" aria-describedby="password-eye" />
            <span style="cursor: pointer;" class="input-group-text" id="password-eye">
                <i class="fas fa-eye-slash"></i>
            </span>
        </div>
    </div>
    <div class="d-flex fs-base fw-semibold justify-content-end mb-8 flex-wrap gap-3">
        <div></div>
        <span id="forgetPasswordContainer">
            <a href="#" class="link-primary upsert-swal-popup"
                data-swal="{{ json_encode($instance['forgotPassword']['swal']) }}">Forgot Password</a> |
        </span>
        <a href="#" class="link-primary upsert-swal-popup"
            data-swal="{{ json_encode($instance['help']['swal']) }}">Need Help</a>
    </div>
    <x-slot name="didOpen">
        modules.portal.index.swalLogin('form#login');
        $('#password-eye').click(function () {
            const pass = $('input[name=password]');
            if (pass.attr('type') === 'password') {
                pass.removeAttr('type')
                pass.siblings()
                    .find('i')
                    .removeClass('fa-eye-slash')
                    .addClass('fa-eye')
            } else {
                pass.attr('type', 'password')
                pass.siblings()
                    .find('i')
                    .removeClass('fa-eye')
                    .addClass('fa-eye-slash')
            }
        })
    </x-slot>
</x-swal.upsert>
