<div class="mb-10 text-center">
    <h1 class="text-dark mb-3">Need Help?</h1>
</div>
<div class="fv-row mb-10">
    <span>
        Hey there!&nbsp;Need a hand?&nbsp;If you&#39;re having any troubles with your account,&nbsp;we&#39;ve got you
        covered.
    </span>
    <br><br>
    <span>
        Reach out to us at&nbsp;<a href="mailto:orientation@taylor.edu.my">orientation@taylor.edu.my</a>&nbsp;and
        we&#39;ll be on
        it in
        no time!&nbsp;If you&#39;re looking for more info,&nbsp;check out our user manual for a detailed guide or hit up
        our FAQs list.
    </span>
</div>
