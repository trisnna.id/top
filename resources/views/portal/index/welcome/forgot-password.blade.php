<div class="mb-10 mt-10 text-center">
    <h1 class="text-light bg-dark mb-3">Forgotten your Password ?</h1>
    <div class="text-dark fw-bold fs-5">Let&#39;s get you back in!&nbsp;&nbsp;Drop in your email to reset your password.
    </div>
</div>
<div class="fv-row mb-10">
    <label class="form-label fw-bolder fs-6 text-gray-900">Email</label>
    <input required class="form-control form-control-solid" type="email" name="email" autocomplete="off"
        placeholder="Email" />
</div>
