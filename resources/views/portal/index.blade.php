<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
        <title>{{config('app.name')}}</title>
		<x-layouts.app.meta :ogTitle="$title.' | '.config('app.name')"/>
        <x-layouts.app.styles />
	</head>
    <body data-kt-name="metronic" id="kt_body" data-bs-spy="scroll" data-bs-target="#kt_landing_menu" class="bg-white position-relative app-blank">
		<div class="d-flex flex-column flex-root" id="kt_app_root">
            @include('portal.index.welcome')
            <x-layouts.app.body.footer />
		</div>
		<x-layouts.app.body.scripts />
	</body>
</html>