<x-layouts.app :title="__('Resources')" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-white fw-bold fs-3 flex-column justify-content-center my-0">
                    {{$title}}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-white text-hover-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('dashboard.index') }}" class="text-white text-hover-white">Dashboard</a>
                    </li>
                </ul>
            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ $resource->title }}</h3>
            </div>
            <div class="card-body">
                <div class="d-flex flex-column">
                    <div class="flex-lg-row-fluid me-xl-15">
                        <div class="mb-17">
                            <div>
                                {!! json_decode($resource->description_encode) !!}
                            </div>
                        </div>
                    </div>
                    <div class="flex-column flex-lg-row-auto w-100 w-xl-300px">
                        @if (count($attachmentLinks) > 0)
                            <div class="mb-5">
                                <h4 class="text-dark">Other Links</h4>
                                <div class="d-flex flex-column content-justify-center flex-row-fluid">
                                    @foreach ($attachmentLinks as $attachmentLink)
                                        <div class="d-flex fw-semibold align-items-center">
                                            <div class="bullet w-8px h-3px rounded-2 bg-primary me-3"></div>
                                            <div class="flex-grow-1 me-4">{{ $attachmentLink['instance']['name'] }}
                                            </div>
                                            <div class="fw-bolder text-xxl-end">
                                                <a target="_blank" href="{{ $attachmentLink['instance']['url'] }}"
                                                    class="text-muted text-hover-primary pe-2">
                                                    <span class="svg-icon svg-icon-active svg-icon-2hx text-info">
                                                        <u>Click Here</u>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        @if (count($attachmentDocuments) > 0)
                            <div class="mb-5">
                                <h4 class="text-dark">Files &amp; Downloads</h4>
                                <div class="d-flex flex-column content-justify-center flex-row-fluid">
                                    @foreach ($attachmentDocuments as $attachmentDocument)
                                        <div class="d-flex fw-semibold align-items-center">
                                            <div class="bullet w-8px h-3px rounded-2 bg-primary me-3"></div>
                                            <div class="flex-grow-1 me-4">
                                                {{ $attachmentDocument['instance']->getCustomProperty('name') ?? $attachmentDocument['instance']->file_name }}
                                            </div>
                                            <div class="fw-bolder text-xxl-end">
                                                <a target="_blank" href="{{ $attachmentDocument['url'] }}"
                                                    class="text-muted text-hover-primary pe-2">
                                                    <span class="svg-icon svg-icon-muted svg-icon-2hx text-info"><svg
                                                            width="24" height="24" viewBox="0 0 24 24"
                                                            fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path opacity="0.3"
                                                                d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z"
                                                                fill="currentColor"></path>
                                                            <path
                                                                d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z"
                                                                fill="currentColor"></path>
                                                            <path opacity="0.3"
                                                                d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z"
                                                                fill="currentColor"></path>
                                                        </svg>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                @if (count($attachmentMedias) > 0)
                    <div class="mb-5">
                        <div class="d-flex flex-stack mb-5">
                            <h3 class="text-dark">Media</h3>
                        </div>
                        <div class="separator separator-dashed mb-9"></div>
                        <div class="row g-10">
                            @php
                                if(count($attachmentMedias) == 1){
                                    $bootstrapSize = 12;
                                }elseif(count($attachmentMedias) == 2){
                                    $bootstrapSize = 6;
                                }else{
                                    $bootstrapSize = 4;
                                }   
                            @endphp
                            @foreach ($attachmentMedias as $attachmentMedia)
                                <div class="col-md-{{$bootstrapSize}}">
                                    <div class="card-xl-stretch me-md-6">
                                        <div class="d-block position-relative min-h-175px mb-5">
                                            @if (urlTypeMedia($attachmentMedia['url']) == 'youtube')
                                                <iframe class="position-absolute top-50 start-50 translate-middle"
                                                    src="{{ $attachmentMedia['url'] }}"
                                                    title="{{ $attachmentMedia['name'] }}" frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                                    allowfullscreen style="max-height: 175px;width:100%;"></iframe>
                                            @elseif(urlTypeMedia($attachmentMedia['url']) == 'video')
                                                <video class="position-absolute top-50 start-50 translate-middle"
                                                    controls style="max-height: 175px;width:100%;">
                                                    <source src="{{ $attachmentMedia['url'] }}">
                                                </video>
                                            @endif
                                        </div>
                                        <div class="m-0">
                                            <a href="#"
                                                class="fs-4 text-dark fw-bold text-hover-primary text-dark lh-base">
                                                {{ $attachmentMedia['name'] }}
                                            </a>
                                            <div class="fw-semibold fs-5 text-gray-600 text-dark my-4">
                                                {{ $attachmentMedia['description'] }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</x-layouts.app>
