<div class=" d-flex flex-column gap-5 gap-md-7 py-5 px-5 border-gray-300 border-dotted"
    id="kt_ecommerce_edit_order_shipping_form">
    <div class="fs-3 fw-bold mb-n2">Additional Documents</div>
    <div id="resourceDocs" class="d-flex flex-column flex-md-row gap-5">
        <div class="fv-row flex-row-fluid">
            <label class="form-label">{{ __('Description') }}</label>
            <input class="form-control" name="file[][description]" placeholder="File description" value="">
        </div>
        <div class="fv-row flex-row-fluid">
            <label class="form-label">{{ __('File') }}</label>
            <input type="file"
                accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
            text/plain, application/pdf"
                class="form-control" name="file[][text]">
        </div>
        <div class="fv-row flex-row-end" style="justify-content: center; align-self: flex-end;">
            <a id="file" href="#" class="btn btn-sm btn-flex btn-light-danger" onclick="alert('Comming soon.')">
                <!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
                <span class="svg-icon svg-icon-3">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5"
                            fill="currentColor"></rect>
                        <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                            transform="rotate(-90 10.8891 17.8033)" fill="currentColor"></rect>
                        <rect x="6.01041" y="10.9247" width="12" height="2" rx="1"
                            fill="currentColor"></rect>
                    </svg>
                </span>
                <!--end::Svg Icon--> Add file
            </a>
        </div>
    </div>
    <div class="d-flex flex-column flex-md-row gap-5">
        <div class="fv-row flex-row-fluid">
            <div class="card">
                <div class="card-header collapsible cursor-pointer rotate" data-bs-toggle="collapse"
                    data-bs-target="#kt_docs_card_collapsible">
                    <h3 class="card-title">{{ __('File uploaded') }}</h3>
                    <div class="card-toolbar rotate-180">
                        <span class="svg-icon svg-icon-1"><svg width="24" height="24" viewBox="0 0 24 24"
                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                    fill="currentColor"></path>
                            </svg>
                        </span>
                    </div>
                </div>
                <div id="kt_docs_card_collapsible" class="collapse show">
                    <div class="row g-6 g-xl-9 mb-6 mb-xl-9">


                        <!--begin::Col-->
                        <div class="col-md-6 col-lg-4 col-xl-3">
                            <!--begin::Card-->
                            <div class="card h-100 ">
                                <!--begin::Card body-->
                                <div class="card-body d-flex justify-content-center text-center flex-column p-8">
                                    <!--begin::Name-->
                                    <a href="#" class="text-gray-800 text-hover-primary d-flex flex-column">
                                        <!--begin::Image-->
                                        <div class="symbol symbol-60px mb-5">
                                            <img src="{{ asset('media/svg/files/pdf.svg') }}" class="theme-light-show"
                                                alt="">
                                            <img src="{{ asset('media/svg/files/pdf-dark.svg') }}"
                                                class="theme-dark-show" alt="">

                                        </div>
                                        <!--end::Image-->

                                        <!--begin::Title-->
                                        <div class="fs-5 fw-bold mb-2">
                                            A map of Taylor's Lakeside Campus </div>
                                        <!--end::Title-->
                                    </a>
                                    <!--end::Name-->

                                    <!--begin::Description-->
                                    <div class="fs-7 fw-semibold text-gray-400">
                                        <a href="#" class="btn btn-secondary btn-sm position-relative">
                                            Download <span
                                                class="position-absolute top-100 start-100 translate-middle badge badge-circle badge-danger svg-icon svg-icon-2x svg-icon-white">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="currentColor"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.3"
                                                        d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z"
                                                        fill="currentColor" />
                                                    <path
                                                        d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z"
                                                        fill="currentColor" />
                                                    <path opacity="0.3"
                                                        d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z"
                                                        fill="currentColor" />
                                                </svg>
                                            </span>
                                        </a>
                                    </div>
                                    <!--end::Description-->
                                </div>
                                <!--end::Card body-->
                            </div>
                            <!--end::Card-->
                        </div>
                        <!--end::Col-->

                        <!--begin::Col-->
                        <div class="col-md-6 col-lg-4 col-xl-3">
                            <!--begin::Card-->
                            <div class="card h-100 ">
                                <!--begin::Card body-->
                                <div class="card-body d-flex justify-content-center text-center flex-column p-8">
                                    <!--begin::Name-->
                                    <a href="#" class="text-gray-800 text-hover-primary d-flex flex-column">
                                        <!--begin::Image-->
                                        <div class="symbol symbol-60px mb-5">
                                            <img src="{{ asset('media/svg/files/doc.svg') }}" class="theme-light-show"
                                                alt="">
                                            <img src="{{ asset('media/svg/files/dark/doc.svg') }}"
                                                class="theme-dark-show" alt="">

                                        </div>
                                        <!--end::Image-->

                                        <!--begin::Title-->
                                        <div class="fs-5 fw-bold mb-2">
                                            Taylor's Bus Schedule </div>
                                        <!--end::Title-->
                                    </a>
                                    <!--end::Name-->

                                    <!--begin::Description-->
                                    <div class="fs-7 fw-semibold text-gray-400 position-relative">
                                        <a href="#" class="btn btn-secondary btn-sm position-relative">
                                            Download <span
                                                class="position-absolute top-100 start-100 translate-middle badge badge-circle badge-danger svg-icon svg-icon-2x svg-icon-white">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="currentColor"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path opacity="0.3"
                                                        d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z"
                                                        fill="currentColor" />
                                                    <path
                                                        d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z"
                                                        fill="currentColor" />
                                                    <path opacity="0.3"
                                                        d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z"
                                                        fill="currentColor" />
                                                </svg>
                                            </span>
                                        </a>
                                    </div>
                                    <!--end::Description-->
                                </div>
                                <!--end::Card body-->
                            </div>
                            <!--end::Card-->
                        </div>
                        <!--end::Col-->
                    </div>
                    {{-- <div class="card-body">
                        <div class="row">
                            @for ($i = 0; $i < 3; $i++)
                            <div class="col-lg-4">
                                <!--begin::Card-->
                                <div class="card overlay overlay">
                                    <div class="card-body p-0">
                                        <div class="overlay-wrapper">
                                            <img src="{{ asset('media/stock/600x400/img-1.jpg') }}" alt=""
                                                class="w-100 card-rounded">
                                        </div>
                                        <div class="overlay-layer card-rounded bg-dark bg-opacity-25">
                                            <img src="{{ asset('media/icons/duotune/general/gen034.svg') }}"
                                                style="cursor: pointer;" class="h-60px" alt="" data-bs-toggle="tooltip" data-bs-placement="top" title="Remove">
                                        </div>
                                    </div>
                                </div>
                                <!--end::Card-->
                            </div>
                            @endfor
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
