<div class=" d-flex flex-column gap-5 gap-md-7" id="kt_ecommerce_edit_order_shipping_form">
    <div class="fs-3 fw-bold mb-n2">Media</div>
    <div class="d-flex flex-column flex-md-row gap-2">
        <div class="fv-row flex-row-fluid">
            <label class="form-label">{{ __('External Link') }}</label>
            <input class="form-control" name="link_youtube" placeholder="e.g: https://youtube.com" value="">
        </div>
        <div class="fv-row flex-row-end" style="justify-content: center; align-self: flex-end;">
            <a id="link" href="#" class="btn btn-sm btn-flex btn-light-danger" onclick="alert('Coming soon.')">
                <!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
                <span class="svg-icon svg-icon-3">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5"
                            fill="currentColor"></rect>
                        <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                            transform="rotate(-90 10.8891 17.8033)" fill="currentColor"></rect>
                        <rect x="6.01041" y="10.9247" width="12" height="2" rx="1"
                            fill="currentColor"></rect>
                    </svg>
                </span>
                <!--end::Svg Icon--> Add link
            </a>
        </div>
    </div>
    <div class="d-flex flex-column flex-md-row">
        <div class="row g-10 my-2">
            <!--begin::Col-->
            <div class="col-md-4">
                <!--begin::Feature post-->
                <div class="card-xl-stretch me-md-6">
                    <!--begin::Image-->
                    <a class="d-block bgi-no-repeat bgi-size-cover bgi-position-center card-rounded position-relative min-h-175px mb-5"
                        style="background-image:url('{{ asset('media/stock/600x400/img-73.jpg') }}')"
                        data-fslightbox="lightbox-video-tutorials" href="https://www.youtube.com/embed/btornGtLwIo">

                        <img src="{{ asset('media/svg/misc/video-play.svg') }}"
                            class="position-absolute top-50 start-50 translate-middle" alt="">
                    </a>
                    <!--end::Image-->

                    <!--begin::Body-->
                    <div class="m-0">
                        <!--begin::Title-->
                        <a href="/metronic8/demo1/../demo1/pages/user-profile/overview.html"
                            class="fs-4 text-dark fw-bold text-hover-primary text-dark lh-base">
                            Admin Panel - How To Started the Dashboard Tutorial 
                        </a>
                        <!--end::Title-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Feature post-->



            </div>
            <!--end::Col-->

            <!--begin::Col-->
            <div class="col-md-4">
                <!--begin::Feature post-->
                <div class="card-xl-stretch mx-md-3">
                    <!--begin::Image-->
                    <a class="d-block bgi-no-repeat bgi-size-cover bgi-position-center card-rounded position-relative min-h-175px mb-5"
                        style="background-image:url('{{ asset('media/stock/600x400/img-74.jpg') }}')"
                        data-fslightbox="lightbox-video-tutorials" href="https://www.youtube.com/embed/btornGtLwIo">

                        <img src="{{ asset('media/svg/misc/video-play.svg') }}"
                            class="position-absolute top-50 start-50 translate-middle" alt="">
                    </a>
                    <!--end::Image-->

                    <!--begin::Body-->
                    <div class="m-0">
                        <!--begin::Title-->
                        <a href="/metronic8/demo1/../demo1/pages/user-profile/overview.html"
                            class="fs-4 text-dark fw-bold text-hover-primary text-dark lh-base">
                            Admin Panel - How To Started the Dashboard Tutorial </a>
                        <!--end::Title-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Feature post-->



            </div>
            <!--end::Col-->

            <!--begin::Col-->
            <div class="col-md-4">
                <!--begin::Feature post-->
                <div class="card-xl-stretch ms-md-6">
                    <!--begin::Image-->
                    <a class="d-block bgi-no-repeat bgi-size-cover bgi-position-center card-rounded position-relative min-h-175px mb-5"
                        style="background-image:url('{{ asset('media/stock/600x400/img-47.jpg') }}')"
                        data-fslightbox="lightbox-video-tutorials" href="https://www.youtube.com/embed/TWdDZYNqlg4">

                        <img src="{{ asset('media/svg/misc/video-play.svg') }}"
                            class="position-absolute top-50 start-50 translate-middle" alt="">
                    </a>
                    <!--end::Image-->

                    <!--begin::Body-->
                    <div class="m-0">
                        <!--begin::Title-->
                        <a href="/metronic8/demo1/../demo1/pages/user-profile/overview.html"
                            class="fs-4 text-dark fw-bold text-hover-primary text-dark lh-base">
                            Admin Panel - How To Started the Dashboard Tutorial </a>
                        <!--end::Title-->
                    </div>
                </div>
                <!--end::Feature post-->



            </div>
            <!--end::Col-->
        </div>
    </div>
    <div class="d-flex flex-column flex-md-row gap-2">
        <div class="fv-row flex-row-fluid">
            <div class="separator separator-content my-5">Or upload from your local computer.</div>
        </div>
    </div>
    <div id="resourceLink" class="d-flex flex-column flex-md-row gap-2">
        <div class="fv-row flex-row-fluid">
            <div class="dropzone" id="resourceMedia">
                <div class="dz-message needsclick">
                    <i class="bi bi-file-earmark-arrow-up text-primary fs-3x"></i>
                    <div class="ms-4">
                        <h3 class="fs-5 fw-bold text-gray-900 mb-1">Drop files here or click to
                            upload.</h3>
                        <span class="fs-7 fw-semibold text-gray-400">Upload up to 10
                            files</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
