<div class=" d-flex flex-column gap-5 gap-md-7 py-5 px-5 border-gray-300 border-dotted" id="resourceLinkContainer">
    <div class="fs-3 fw-bold mb-n2">Resource Links</div>
    <div id="resourceLink" class="d-flex flex-column flex-md-row gap-5">
        <div class="fv-row flex-row-fluid">
            <label class="form-label">{{ __('Description') }}</label>
            <input class="form-control resource-link" name="link.description" placeholder="Link description"
                value="">
        </div>
        <div class="fv-row flex-row-fluid">
            <label class="form-label">{{ __('Text to Display') }}</label>
            <input class="form-control resource-link" name="link.text" placeholder="Link display" value="">
        </div>
        <div class="fv-row flex-row-fluid">
            <label class="form-label">{{ __('Url') }}</label>
            <input class="form-control resource-link" name="link.url" placeholder="e.g: https://google.com"
                value="">
        </div>
        <div class="fv-row flex-row-fluid" style="justify-content: center; align-self: flex-end;">
            <a id="link" href="#" class="btn btn-sm btn-flex btn-light-danger">
                <!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
                <span class="svg-icon svg-icon-3">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5"
                            fill="currentColor"></rect>
                        <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                            transform="rotate(-90 10.8891 17.8033)" fill="currentColor"></rect>
                        <rect x="6.01041" y="10.9247" width="12" height="2" rx="1"
                            fill="currentColor"></rect>
                    </svg>
                </span>
                <!--end::Svg Icon--> Add link
            </a>
        </div>
    </div>
    <div id="previewContainer" class="d-flex flex-column flex-md-row gap-5">
        <div class="fv-row flex-row-fluid" style="display: none;">
            <div class="card">
                <div class="card-header collapsible cursor-pointer rotate" data-bs-toggle="collapse"
                    data-bs-target="#linkCollapsiable">
                    <h3 class="card-title">{{ __('Links Preview') }}</h3>
                    <div class="card-toolbar rotate-180">
                        <span class="svg-icon svg-icon-1"><svg width="24" height="24" viewBox="0 0 24 24"
                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z"
                                    fill="currentColor"></path>
                            </svg>
                        </span>
                    </div>
                </div>
                <div id="linkCollapsiable" class="collapse show">
                    <div id="linkPreviewContainer" class="card-body pt-0">
                        {{-- Handled by javascript --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
