<x-layouts.app :title="__('Resources')" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-layouts.toolbar :page="__('resources')" :title="__('Resources')" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card card-flush py-4">
            <!--begin::Card header-->
            <div class="card-header">
                <div class="card-title">
                    <h2>{{ __('Resource Form') }}</h2>
                </div>
            </div>
            <!--end::Card header-->

            <!--begin::Card body-->
            <div class="card-body pt-0">
                <form action="form fv-plugins-bootstrap5 fv-plugins-framework" method="post">
                    <div class="d-flex flex-column gap-5 gap-md-7 mb-5">
                        <div class="fs-3 fw-bold mb-n2">Description</div>
                        <div id="title" class="fv-row fv-plugins-icon-container">
                            <label class="required form-label">Title</label>
                            <input name="title" class="form-control" placeholder="{{ __('This field is required') }}" value="{{ $resource->name }}">
                        </div>
                        <div id="content" class="fv-row fv-plugins-icon-container">
                            <label class="required form-label">Content</label>
                            <textarea name="content" id="contentEditor" class="form-control tox-target"
                                placeholder="{{ __('This field is required') }}">{!! $resource->content !!}</textarea>
                        </div>
                        @include('resource.inputs.links', ['links' => $resource->links])
                        @include('resource.inputs.documents', ['documents' => $resource->documents])
                        @include('resource.inputs.media', ['medias' => $resource->medias])
                    </div>
                    <div class="d-flex d-fle gap-md-3 justify-content-end">
                        <!--begin::Button-->
                        <button type="reset" data-kt-contacts-type="cancel" class="btn btn-light me-3">
                            Cancel
                        </button>
                        <!--end::Button-->

                        <!--begin::Button-->
                        <button type="submit" data-kt-contacts-type="submit" class="btn btn-danger" onclick="alert('Coming soon.')">
                            <span class="indicator-label">
                                Save
                            </span>
                            <span class="indicator-progress">
                                Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                            </span>
                        </button>
                        <!--end::Button-->
                    </div>
                </form>
            </div>
        </div>
    </div>

    @push('scripts')
        <script type="text/javascript" src="{{ asset(mix('js/modules/resource.min.js')) }}"></script>
    @endpush
</x-layouts.app>
