<x-layouts.app :title="__('Resources')" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-layouts.toolbar :page="__('resources')" :title="__('Resources')" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        {{ $content }}
    </div>

    @push('scripts')
        {{ $scripts }}
        <script type="text/javascript" src="{{ asset(mix('js/modules/resource.min.js')) }}"> </script>
    @endpush
</x-layouts.app>
