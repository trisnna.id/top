<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-layouts.toolbar :page="__('resources')" :title="$title" />
    </x-slot>
    @php
        $navItems = [
            [
                'aClass' => 'active',
                'name' => 'video',
                'tabClass' => 'show active',
                'title' => 'Videos',
                'iconClass' => 'fa-solid fa-video',
                'search' => 'Search videos',
                'addButton' => 'Add Video',
                'color' => 'red',
            ],
            [
                'aClass' => '',
                'name' => 'link',
                'tabClass' => '',
                'title' => 'Links',
                'iconClass' => 'fa-solid fa-link',
                'search' => 'Search links',
                'addButton' => 'Add Link',
                'color' => '#0095e8'
            ],
            [
                'aClass' => '',
                'name' => 'file',
                'tabClass' => '',
                'title' => 'Files',
                'iconClass' => 'fa-solid fa-file',
                'search' => 'Search files',
                'addButton' => 'Add File',
                'color' => '#ffc700',
            ],
            [
                'aClass' => '',
                'name' => 'faq',
                'tabClass' => '',
                'title' => 'FAQ',
                'iconClass' => 'fa-solid fa-question',
                'search' => 'Search faqs',
                'addButton' => 'Add Faq',
                'color' => '#7239ea'
            ],
        ];
    @endphp
    <ul class="nav nav-pills nav-pills-custom mb-3 d-flex flex-center">
        @foreach ($navItems as $key => $navItem)
            <li class="nav-item mb-3 me-3 me-lg-6">
                <a class="nav-link btn btn-outline btn-flex btn-color-muted btn-active-color-primary flex-column overflow-hidden w-150px h-85px pt-5 pb-2 bg-white {{ $navItem['aClass'] }}"
                    id="tab_link_{{ $navItem['name'] }}" data-bs-toggle="pill" href="#tab_{{ $navItem['name'] }}">
                    <i class="{{ $navItem['iconClass'] }} fs-2qx mb-2" style="color: {{ $navItem['color'] }};"></i>
                    <span class="nav-text text-gray-800 fw-bold fs-6 lh-1">{{ $navItem['title'] }}</span>
                    <span class="bullet-custom position-absolute bottom-0 w-100 h-4px bg-primary"></span>
                </a>
            </li>
        @endforeach
    </ul>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="tab-content" id="resourceTab">
            @foreach ([$videoDt, $linkDt, $fileDt, $faqDt] as $i => $dt)
                <div class="tab-pane fade {{ $navItems[$i]['tabClass'] }}" id="tab_{{ $navItems[$i]['name'] }}"
                    role="tabpanel">
                    <div class="card">
                        <div class="card-header border-0 pt-6">
                            <div class="d-flex align-items-center position-relative my-1">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546"
                                            height="2" rx="1" transform="rotate(45 17.0365 15.1223)"
                                            fill="currentColor" />
                                        <path
                                            d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                            fill="currentColor" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <input type="text" data-kt-user-table-filter="search"
                                    class="form-control form-control-solid w-250px ps-14"
                                    placeholder="{{ $navItems[$i]['search'] }}" />
                            </div>
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                    @if ($i === 0)
                                        <x-adminform-resource.video  />
                                    @elseif ($i === 1)
                                        <x-adminform-resource.link  />
                                    @elseif ($i === 2)
                                        <x-adminform-resource.file  />
                                    @elseif ($i === 3)
                                        <x-adminform-resource.faq  />
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-body py-4">
                            <x-datatables.card.buttons.legend :gap="70" :include="['edit', 'delete', 'banned']"/>
                            {!! $dt->html()->table([
                                'class' =>
                                    'table table-bordered table-striped align-middle table-row-dashed fs-6 gy-5 th-text-uppercase th-fw-bold w-100',
                            ]) !!}
                            @push('script')
                                dtx.initOption("{{ $navItems[$i]['name'] }}Dt");
                                {!! $dt->html()->generateScripts() !!}
                            @endpush
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</x-layouts.app>