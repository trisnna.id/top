<x-layouts.app :title="__('Resources')" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-layouts.toolbar :page="__('resources')" :title="__($user->hasRole('student') ? 'Arrivals' : 'Resources')" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="d-flex align-items-center position-relative my-1">
                    <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                    <span class="svg-icon svg-icon-1 position-absolute ms-6">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
                                rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                            <path
                                d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <input type="text" data-kt-user-table-filter="search"
                        class="form-control form-control-solid w-250px ps-14"
                        placeholder="{{ __('Search resources') }}" />
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                        <button type="button" class="btn btn-light-danger me-3" data-kt-menu-trigger="click"
                            data-kt-menu-placement="bottom-end">
                            <span class="svg-icon svg-icon-2">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                        fill="currentColor" />
                                </svg>
                            </span>
                            Filter
                        </button>
                        <div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true">
                            <div class="px-7 py-5">
                                <div class="fs-5 text-dark fw-bold">Filter Options</div>
                            </div>
                            <div class="separator border-gray-200"></div>
                            <div class="px-7 py-5">
                                <div class="row mb-5">
                                    <div class="col-md-12 fv-row fv-plugins-icon-container">
                                        <label class="form-label fw-semibold">Source Type:</label>
                                        <select name="filter_source_type" class="form-select form-select2"
                                            id="filter_source_type" data-hide-search="true" data-allow-clear="true"
                                            data-placeholder="-- Choose source type --">
                                            <option></option>
                                            <option value="file">File</option>
                                            <option value="link">Link</option>
                                            <option value="video">Video</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <x-filter-status :options="['active', 'inactive']" />
                                </div>
                                <div class="row">
                                    <div class="d-flex justify-content-end">
                                        <button type="reset"
                                            class="btn btn-sm btn-light btn-active-light-primary me-2"
                                            data-kt-menu-dismiss="true">Reset</button>
                                        <button type="submit" class="btn btn-sm btn-primary"
                                            data-kt-menu-dismiss="true">Apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($user->hasRole('admin'))
                            <x-datatables.card.buttons.export color="danger" />
                            <a href="{{ route('admin.resources.create') }}" type="button" class="btn btn-danger">
                                <span class="svg-icon svg-icon-2">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2"
                                            rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor" />
                                        <rect x="4.36396" y="11.364" width="16" height="2" rx="1"
                                            fill="currentColor" />
                                    </svg>
                                </span>
                                {{ __('Add Resource') }}
                            </a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body py-4">
                @if ($user->hasRole('admin'))
                    <x-datatables.card.buttons.legend :gap="70" :include="['edit', 'show', 'banned', 'delete']" />
                @endif
                {{ $resourceDT->table(['class' => 'table table-bordered table-striped align-middle table-row-dashed fs-6 gy-5 th-text-uppercase th-fw-bold w-100']) }}
            </div>
        </div>
    </div>

    @push('scripts')
        {!! $resourceDT->scripts() !!}
        <script type="text/javascript">
            $(function() {
                window.renderSwalDialog = function(resource, title) {
                    Swal.fire({
                        title: title || 'Add Resource',
                        html: `
                        <form id="resourceForm" class="form theme-form" action="#">
                            <div class="d-flex flex-column mb-5 fv-row">
                                <label for="resourceType" class="required form-label">
                                    Resource Type
                                </label>
                                <select name="type" id="resourceType" class="form-select form-control form-control-solid swal2-select2" data-control="select2" data-placeholder="-- Choose one --">
                                    <option></option>
                                    <option value="file">File</option>
                                    <option value="link">Link</option>
                                    <option value="video">Video</option>
                                </select>
                            </div>
                            <div class="d-flex flex-column mb-5 fv-row">
                                <x-inputs.title :grouping="true" name="name" required id="videoTitle" />
                            </div>
                            <div class="d-flex flex-column mb-5 fv-row">
                                <x-inputs.description :grouping="true" id="resourceDescription" />
                            </div>
                            <div id="sourceContainer" class="d-flex flex-column mb-5 fv-row">
                                
                            </div>
                        </form>
                        `,
                        buttonsStyling: false,
                        showCloseButton: true,
                        showCancelButton: true,
                        showConfirmButton: true,
                        showDenyButton: false,
                        confirmButtonText: '<i class="fa fa-save"></i> Save',
                        cancelButtonText: 'Cancel',
                        denyButtonText: '<i class="fa fa-trash"></i> Delete',
                        showLoaderOnConfirm: true,
                        backdrop: true,
                        allowOutsideClick: false,
                        heightAuto: false,
                        customClass: {
                            popup: 'w-300px w-md-500px',
                            validationMessage: 'm-0 p-1',
                            content: 'text-left px-2 px-sm-4',
                            confirmButton: "btn btn-info",
                            cancelButton: "btn btn-secondary",
                        },
                        didOpen: function() {
                            $(".swal2-modal .select2-hidden-accessible").attr('style',
                                'position: inherit !important;');
                            $(".swal2-modal .select2-container").attr('style',
                                'width: 100%;font-size: 1rem;text-align: initial;');
                            // $("form#resourceForm").attr('action', swalUrl);
                            // $("form#resourceForm").attr('method', swalMethod);
                            $("form#resourceForm" + " .swal2-select2").select2({
                                theme: "bootstrap5",
                                allowClear: true,
                                dropdownParent: $('.swal2-modal')
                            });
                            $("form#resourceForm" + " .swal2-flatpickr-date").flatpickr();
                            tinymce.init({
                                selector: `#resourceDescription`,
                                height: "300",
                                plugins: 'image',
                                toolbar: 'undo redo | bold italic | alignment | image',
                                file_picker_callback: function(callback, value, meta) {
                                    var input = document.createElement('input');
                                    input.setAttribute('type', 'file');
                                    input.setAttribute('accept', 'image/*');

                                    input.click();
                                }
                            });

                            $('#resourceType').on('change', function() {
                                const val = $(this).val()
                                const sourceContainer = $('#resourceForm > #sourceContainer')

                                if (val === 'link') {
                                    sourceContainer.children().remove()
                                    sourceContainer.append(`
                                        <label for="value" class="required form-label">{{ __('Source') }}</label>
                                        <input name="value" type="text" class="form-control" id="value" aria-describedby="value" />
                                    `)
                                } else {
                                    sourceContainer.children().remove()
                                    sourceContainer.append(`
                                        <label for="value" class="required form-label">{{ __('Source') }}</label>
                                        <input name="value" type="file" class="form-control" id="value" aria-describedby="value" />
                                    `)
                                }
                            })
                        },
                        preConfirm: function() {

                        }
                    });
                }
                $('#addNewResource').on('click', function() {
                    renderSwalDialog()
                })
            })
        </script>
    @endpush
</x-layouts.app>
