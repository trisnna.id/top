<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <title>{{ 'Orientation Timetable' }}</title>
</head>
<style>
    @font-face {
        font-family: "CenturyGothic";
        src: url("{{ url('plugins/global/fonts/taylors/CenturyGothic.woff') }}") format("woff");
        font-display: fallback;
    }

    @font-face {
        font-family: "CenturyGothic-Bold";
        src: url("{{ url('plugins/global/fonts/taylors/CenturyGothic-Bold.woff') }}") format("woff");
        font-display: fallback;
    }

    body,
    html {
        font-family: 'CenturyGothic';
        font-size: 13px !important;
        -webkit-font-smoothing: antialiased;
        font-weight: 400;
        /* letter-spacing: 0.4mm !important; font-kerning: normal; text-rendering: optimize-speed; */
    }

    .text-center {
        text-align: center !important;
    }

    .h-100-a4 {
        height: 1015px;
    }

    .h1,
    .h2,
    .h3,
    .h4,
    .h5,
    .h6,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        margin-top: 0;
        margin-bottom: .5rem;
        font-weight: 600;
        line-height: 1.2;
        color: #181c32
    }

    .h1,
    h1 {
        font-size: calc(1.3rem + .6vw)
    }

    @media (min-width:1200px) {

        .h1,
        h1 {
            font-size: 1.75rem
        }
    }

    .h2,
    h2 {
        font-size: calc(1.275rem + .3vw)
    }

    @media (min-width:1200px) {

        .h2,
        h2 {
            font-size: 1.5rem
        }
    }

    .h3,
    h3 {
        font-size: calc(1.26rem + .12vw)
    }

    @media (min-width:1200px) {

        .h3,
        h3 {
            font-size: 1.35rem
        }
    }

    .h4,
    h4 {
        font-size: 1.25rem
    }

    .h5,
    h5 {
        font-size: 1.15rem
    }

    .h6,
    h6 {
        font-size: 1.075rem
    }

    p {
        margin-top: 0;
        margin-bottom: 1rem
    }

    .text-delft-blue {
        color: #1d3354 !important
    }

    .bg-delft-blue {
        background-color: #1d3354
    }

    .text-poppy {
        color: #d64045 !important
    }

    .text-orange {
        color: #eb9800 !important;
    }

    .text-white {
        color: #fff !important
    }

    .w-100 {
        width: 100% !important;
    }

    table.dataTable.table-striped>tbody>tr.even>* {
        box-shadow: inset 0 0 0 9999px rgba(0, 0, 0, .05);
    }

    .mb-3 {
        margin-bottom: 0.75rem !important;
    }

    table,
    th,
    td {
        border: 1px solid;
        border-color: black;
    }

    table td,
    table th {
        padding: 0.25rem;
    }

    table {
        border-collapse: collapse;
    }

    .fs-6 {
        font-size: 1.075rem !important;
    }
</style>

<body>
    <section class="main-content">
        <div class="text-center">
            <h3 class="mb-3"><b>ORIENTATION TIMETABLE</b></h3>
        </div>
        <table class="dataTable table-striped w-100" style="margin-bottom: 2.25rem;">
            <thead class="bg-delft-blue text-white">
                <tr>
                    <th><h4 class="text-white">Type</h4></th>
                    <th><h4 class="text-white">Date</h4></th>
                    <th><h4 class="text-white">From</h4></th>
                    <th><h4 class="text-white">To</h4></th>
                    <th><h4 class="text-white">Activity</h4></th>
                    <th><h4 class="text-white">Venue</h4></th>
                    <th><h4 class="text-white">Presenter</h4></th>
                    <th><h4 class="text-white">Livestream Link</h4></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($data as $key => $row)
                    <tr @if ($loop->iteration % 2 == 0) class="even" @else class="odd" @endif>
                        @foreach ($row as $key => $value)
                            @if ($key === 'Type')
                                <td style="background-color: {{ $value === 'Complementary' ? '#ECF8F4 !important;' : '#FEE8EF !important;' }}"><p>{{ $value }}</p></td>
                            @else
                                <td><p>{{ $value }}</p></td>
                            @endif
                        @endforeach
                    </tr>
                @empty
                    <tr>
                        <td colspan="8"><th><h4 class="text-white">There are no activities</h4></th></td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </section>
</body>

</html>
