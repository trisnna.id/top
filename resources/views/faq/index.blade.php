<x-layouts.app :title="'FAQ'" toolbarClass="py-3 py-lg-6 cover mb-5"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center me-3 flex-wrap">
                <h1 class="page-heading d-flex fw-bold fs-3 flex-column justify-content-center my-0 text-white">
                    FAQ</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-hover-white text-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet w-5px h-2px bg-gray-400"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('faqs.index') }}" class="text-hover-white text-white">FAQ</a>
                    </li>
                </ul>
            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-body py-4">
                <div class="d-flex flex-stack mb-5 flex-wrap">
                    <x-datatables.search id="faqDt" placeholder="Search FAQ" />
                </div>
                {!! $faqDt->html()->table() !!}
                @push('script')
                    dtx.initOption('faqDt');
                    {!! $faqDt->html()->generateScripts() !!}
                @endpush
                @push('scripts')
                    <script>
                        // if in url has parameter q, then get the value and set to search input, then trigger search
                        if (window.location.href.indexOf('q=') > -1) {
                            var q = window.location.href.split('q=')[1];
                            $('#faqDt_search').val(q);
                            $('#faqDt_search').trigger('keyup');
                        }
                    </script>
                @endpush
            </div>
        </div>
    </div>
</x-layouts.app>
