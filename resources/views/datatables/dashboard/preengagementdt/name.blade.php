<strong class="upsert-swal-popup text-info" data-swal="{{json_encode($instance['activity']['swal'])}}" data-data="{{json_encode($instance['activity']['data'])}}">{{$instance->name}}</strong><br>
{{$instance->location}}<br>
{{$instance->activity_start_close_at_readable}}<br>
<strong>Attendance</strong>: <a href="#" class="btn btn-success btn-sm upsert-swal-popup" data-swal="{{json_encode($instance['attendance']['swal'])}}" data-data="{{json_encode($instance['attendance']['data'])}}"><i class="fa-solid fa-qrcode"></i>Scan Here</a>