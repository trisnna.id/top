<div class="card-header">
    <h3 class="card-title align-items-start flex-column w-75">
        <span class="card-label fw-bold text-gray-800">{{ $instance->name }}</span>
        <span class="text-gray-400 mt-1 fw-semibold fs-6">{{ $instance->club_society_category_name }}</span>
    </h3>

    <div class="card-toolbar">
        <!--begin::Menu-->
        <button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end"
            data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
            <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
            <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4"
                        fill="currentColor"></rect>
                    <rect x="11" y="11" width="2.6" height="2.6" rx="1.3"
                        fill="currentColor"></rect>
                    <rect x="15" y="11" width="2.6" height="2.6" rx="1.3"
                        fill="currentColor"></rect>
                    <rect x="7" y="11" width="2.6" height="2.6" rx="1.3"
                        fill="currentColor"></rect>
                </svg>
            </span>
            <!--end::Svg Icon-->
        </button>
        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px"
            data-kt-menu="true" style="">
            <!--begin::Menu item-->
            <div class="menu-item px-3">
                <div class="menu-content fs-6 text-dark fw-bold px-3 py-4">Quick Actions</div>
            </div>
            <!--end::Menu item-->
            <!--begin::Menu separator-->
            <div class="separator mb-3 opacity-75"></div>
            <!--end::Menu separator-->
            <!--begin::Menu item-->
            <div class="menu-item px-3">
                <a href="#" class="menu-link px-3">New Ticket</a>
            </div>
            <!--end::Menu item-->
            <!--begin::Menu item-->
            <div class="menu-item px-3">
                <a href="#" class="menu-link px-3">New Customer</a>
            </div>
            <!--end::Menu item-->
            <!--begin::Menu item-->
            <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
                <!--begin::Menu item-->
                <a href="#" class="menu-link px-3">
                    <span class="menu-title">New Group</span>
                    <span class="menu-arrow"></span>
                </a>
                <!--end::Menu item-->
                <!--begin::Menu sub-->
                <div class="menu-sub menu-sub-dropdown w-175px py-4" style="">
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">Admin Group</a>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">Staff Group</a>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">Member Group</a>
                    </div>
                    <!--end::Menu item-->
                </div>
                <!--end::Menu sub-->
            </div>
            <!--end::Menu item-->
            <!--begin::Menu item-->
            <div class="menu-item px-3">
                <a href="#" class="menu-link px-3">New Contact</a>
            </div>
            <!--end::Menu item-->
            <!--begin::Menu separator-->
            <div class="separator mt-3 opacity-75"></div>
            <!--end::Menu separator-->
            <!--begin::Menu item-->
            <div class="menu-item px-3">
                <div class="menu-content px-3 py-3">
                    <a class="btn btn-primary btn-sm px-4" href="#">Generate Reports</a>
                </div>
            </div>
            <!--end::Menu item-->
        </div>
        <!--begin::Menu 2-->

        <!--end::Menu 2-->
        <!--end::Menu-->
    </div>
</div>
<div class="card-body">
    {{ $instance->description }}
    <div class="fw-bold mt-5">Contact Us:-</div>
    <a class="d-flex align-items-center fs-7 fw-bold mb-2" href="mailto:{{ $instance->email }}">
        <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path opacity="0.3"
                    d="M21 18H3C2.4 18 2 17.6 2 17V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V17C22 17.6 21.6 18 21 18Z"
                    fill="currentColor" />
                <path
                    d="M11.4 13.5C11.8 13.8 12.3 13.8 12.6 13.5L21.6 6.30005C21.4 6.10005 21.2 6 20.9 6H2.99998C2.69998 6 2.49999 6.10005 2.29999 6.30005L11.4 13.5Z"
                    fill="currentColor" />
            </svg>
        </span>
        {{ $instance->email }}
    </a>
    @if(!empty($instance->contact_number) && $instance->contact_number !== '0')
        <a class="d-flex align-items-center fs-7 fw-bold mb-2" href="tel:{{ $instance->contact_number }}">
            <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z" fill="currentColor"/>
                    <path opacity="0.3" d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z" fill="currentColor"/>
                    </svg>
            </span>
            {{ $instance->contact_number }}
        </a>
    @endif
    @if(!empty($instance->media_social))
        <div class="d-flex flex-center">
            @foreach (explode('||',$instance->media_social) as $mediaSocial)
                @php
                    $type = null;
                    $icon = null;
                    $url = null;
                    list($type, $icon, $url) = urlIconMediaSocial($mediaSocial);
                @endphp
                <a href="{{$url}}" class="mx-4">
                    <img src="{{ $icon }}" class="h-20px my-2">
                </a>
            @endforeach
        </div>
    @endif
</div>
<div class="card-footer">
    <div class="text-center mb-1">
        <a class="btn btn-sm btn-primary me-2" href="#">Sign Up</a>
        <a class="btn btn-sm btn-danger" href="#">Exit</a>
    </div>
</div>