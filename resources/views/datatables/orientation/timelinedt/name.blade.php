@if(!is_null($instance->orientation_attendance_updated_at))
    <i class="icon icon-check fa-solid fa-check bg-success text-white p-2"></i>
@else
    <i class="icon fas fa-dot-circle text-{{$instance->orientation_type_constant['color']['class']}}"></i>
@endif
<div class="arrow-container">
    <div class="mt-3 arrow-head mb-3 bg-d6dce5 position-relative">
        <span class="arrow-text m-12 text-black fs-6">{{strtoupper($instance->start_end_date_dFY)}}</span>
        <span class="position-absolute top-0 start-50 translate-middle badge badge-d6dce5" style="border: 2px solid white;">{{strtoupper($instance->orientationTypeConstant['title'])}}</span>
    </div>
</div>
<div class="card bg-{{$instance->orientation_type_constant['color']['class']}} accordion accordion-icon-toggle" style="background-color: {{ strtolower($instance->orientationTypeConstant['title']) !== 'complementary' ? '#FEE8EF' : '#ECF8F4' }} !important">
    <div class="card-header p-6 border-0">
        <div class="accordion-header d-flex collapsed" data-bs-toggle="collapse" data-bs-target="#accordian_{{$instance->uuid}}_item_1">
            <h3 class="fs-4 fw-semibold mb-0 font-family-century-gothic">
                <span class="card-label fw-bold fs-5 uppercase text-002060 mb-1" style="display: inline-block;">{{$instance->name_uppercase}}</span><br>
                <span class="fw-bold fs-6 text-002060 mb-3" style="display: inline-block; color: #C00000 !important;">Time: {{$instance->start_time_readable}} to {{$instance->end_time_readable}}</span><br>
                @if(!empty($instance->venue ?? null))
                    <span class="fw-bold fs-6 text-002060">{{$instance->venue}}</span><br>
                @endif
                @if(!empty($instance->notes ?? null) && $instance->notes !== '""')
                    <span class="pt-2 fw-semibold fs-6">{{$instance->notes ?? null}}</span>
                @endif
            </h3>
        </div>
    </div>
    <div  class="card-body pt-0 p-6">
        <div id="accordian_{{$instance->uuid}}_item_1" class="mb-3 collapse" data-bs-parent="#accordian_{{$instance->uuid}}">
            @if(!empty($instance->synopsis ?? null) && $instance->synopsis !== '""')
                <span class="mb-8 d-block font-family-century-gothic">{!! !empty($instance->synopsis) ? json_decode($instance->synopsis_encode) : null !!}</span>
            @endif
        </div>
        <div class="text-center">
            @if(!empty($instance['rate'] ?? []))
                <a id="btn-rate-{{$instance->uuid}}" href="#" class="btn btn-sm btn-warning me-2 upsert-swal-popup fw-bold" data-swal="{{json_encode($instance['rate']['swal'] ?? [])}}" data-data="{{json_encode($instance['rate']['data'] ?? [])}}">Please Rate</a>
            @endif
            @if(is_null($instance->orientation_rsvp_is_go) && !empty($instance['rsvp'] ?? []))
                @if($instance->is_rsvp_full)
                    <span class="btn btn-sm btn-info me-2 fw-bold">RSVP Fully Occupied</span>
                @else
                    <span class="btn btn-sm btn-info me-2 fw-bold upsert-swal-popup" data-swal="{{json_encode($instance['rsvp']['swal'] ?? [])}}" data-data="{{json_encode($instance['rsvp']['data'] ?? [])}}">RSVP Here</span>
                @endif
            @elseif(!is_null($instance->orientation_rsvp_is_go))
                <span class="btn btn-sm btn-secondary me-2 text-{{$instance->orientation_rsvp_is_go ? 'green' : 'red'}} fw-bold upsert-swal-popup" data-swal="{{json_encode($instance['rsvp']['swal'] ?? [])}}" data-data="{{json_encode($instance['rsvp']['data'] ?? [])}}">RSVP - {{$instance->orientation_rsvp_is_go ? 'Yes' : 'No'}}</span>
            @endif
            <a href="#" class="btn btn-sm btn-info me-2 upsert-swal-popup fw-bold" data-swal="{{json_encode($instance['orientation']['swal'])}}" data-data="{{json_encode($instance['orientation']['data'])}}">View Detail</a>
            @if(!empty($instance['video'] ?? []))
                <a href="#" class="btn btn-sm btn-primary me-2 upsert-swal-popup fw-bold" data-swal="{{json_encode($instance['video']['swal'])}}" data-data="{{json_encode($instance['video']['data'])}}">View Recording Video</a>
            @endif
        </div>
    </div>
</div>
