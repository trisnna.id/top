<p class="text-center">
    <strong class="upsert-swal-popup text-info" data-swal="{{json_encode($instance['orientation']['swal'])}}" data-data="{{json_encode($instance['orientation']['data'])}}">{{$instance->name}}</strong>
</p>
<div class="row">
    <label class="col-md-3 fw-bold form-label m-0">
        Date
    </label>
    <div class="col-md-9">: {{$instance->start_date_dFY}}</div>
</div>
<div class="row">
    <label class="col-md-3 fw-bold form-label m-0">
        Time
    </label>
    <div class="col-md-9">: {{$instance->start_time_readable}} to {{$instance->end_time_readable}}</div>
</div>
@if(!empty($instance->orientation_attendance_updated_at))
    <div class="row">
        <label class="col-md-3 fw-bold form-label m-0">
            Venue
        </label>
        <div class="col-md-9">: {{$instance->venue}}</div>
    </div>
    <div class="row mb-4">
        <label class="col-md-3 fw-bold form-label m-0">
            Attended
        </label>
        <div class="col-md-9">: {{$instance->orientation_attendance_updated_at}}</div>
    </div>
@else
    <div class="row mb-4">
        <label class="col-md-3 fw-bold form-label m-0">
            Venue
        </label>
        <div class="col-md-9">: {{$instance->venue}}</div>
    </div>
@endif
@if($instance->orientation_type_id == 1)
    @if(empty($instance->orientation_attendance_updated_at) && request()->route()->getName() !== "admin.students.show-orientation" && !empty($instance['attendance'] ?? null))
        <div class="d-flex justify-content-center">    
            <a href="#" class="btn btn-{{$instance->orientation_type_constant['color']['class']}} btn-sm upsert-swal-popup" data-swal="{{json_encode($instance['attendance']['swal'])}}" data-data="{{json_encode($instance['attendance']['data'])}}"><i class="fa-solid fa-qrcode"></i><strong>Scan here to mark attendance</strong></a>
        </div>
    @endif
@elseif($instance->orientation_type_id == 2)
    @if(empty($instance->orientation_attendance_updated_at) && request()->route()->getName() !== "admin.students.show-orientation" && !empty($instance['attendance'] ?? null))
        @if(is_null($instance->orientation_rsvp_is_go) && !empty($instance['rsvp'] ?? []))
            <div class="d-flex justify-content-center">
                @if($instance->is_rsvp_full)
                    <span class="btn btn-sm btn-info me-2 fw-bold">RSVP Fully Occupied</span>
                @else
                    <span class="btn btn-sm btn-info me-2 fw-bold upsert-swal-popup" data-swal="{{json_encode($instance['rsvp']['swal'] ?? [])}}" data-data="{{json_encode($instance['rsvp']['data'] ?? [])}}">RSVP Here</span>
                @endif
            </div>
        @elseif(!is_null($instance->orientation_rsvp_is_go))
            <div class="d-flex justify-content-center">  
                <span class="btn btn-sm btn-secondary me-2 text-{{$instance->orientation_rsvp_is_go ? 'green' : 'red'}} fw-bold upsert-swal-popup" data-swal="{{json_encode($instance['rsvp']['swal'] ?? [])}}" data-data="{{json_encode($instance['rsvp']['data'] ?? [])}}">RSVP - {{$instance->orientation_rsvp_is_go ? 'Yes' : 'No'}}</span>
                @if($instance->orientation_rsvp_is_go)
                    <a href="#" class="btn btn-{{$instance->orientation_type_constant['color']['class']}} btn-sm upsert-swal-popup" data-swal="{{json_encode($instance['attendance']['swal'])}}" data-data="{{json_encode($instance['attendance']['data'])}}"><i class="fa-solid fa-qrcode"></i><strong>Scan here to mark attendance</strong></a>
                @endif
            </div>
        @endif
    @endif
@endif






