<div class="card h-100 rounded border" style="border-radius: 10% !important;">
    <img class="card-img-top rounded-4" src="{{ $instance->getFirstMediaUrl('thumbnail') }}">
    <div class="card-body pb-0">
        <h3 class="mb-0 card-title text-center" style="font-family: Antic, sans-serif;color: rgb(81,87,94);" align="center">
            {{ $instance->name }}
        </h3>
    </div>
    <div class="card-footer border-0">
        <div class="d-flex justify-content-center">
            <a href="#" class="btn btn-sm btn-info upsert-swal-popup"
                data-swal="{{ json_encode($instance['orientation']['swal']) }}"
                data-data="{{ json_encode($instance['orientation']['data']) }}">View Detail</a>
        </div>
    </div>
</div>
