<div class="card shadow-md min-h-250px">
    <div class="card-header">
        <h3 class="card-title align-items-start flex-column w-50">
            <span class="card-label fw-bold text-gray-800">{{ $instance->title }}</span>
        </h3>
        <div class="card-toolbar align-items-end flex-column">
            @php
                $color = [
                    'done' => 'success',
                    'ongoing' => 'warning',
                    'todo' => 'info',
                    'locked' => 'secondary',
                ];
                $status = $instance->status === 'completed' ? 'done' : $instance->checkpoint_status;
            @endphp
            <div class="fw-bold mt-5 mb-2">
                <span
                    class="badge badge-{{ $color[$status] }}">{{ sprintf('Checkpoint %s', $instance->checkpoint) }}</span>
            </div>
        </div>
    </div>
    <div class="card-body card-scroll h-250px">
        <img src="{{ asset('media/stock/1600x800/img-1.jpg') }}" width="100%">
        <p class="text-justify mt-3">
            <span class="d-inline-block" style="max-width: 250px;">
                {{ $instance->description }}
            </span>
        </p>
        {{-- <div class="fw-bold">Category:-</div>
        <span class="d-flex align-items-center fs-7 fw-bold mb-2">
            <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.3"
                        d="M21 18H3C2.4 18 2 17.6 2 17V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V17C22 17.6 21.6 18 21 18Z"
                        fill="currentColor" />
                    <path
                        d="M11.4 13.5C11.8 13.8 12.3 13.8 12.6 13.5L21.6 6.30005C21.4 6.10005 21.2 6 20.9 6H2.99998C2.69998 6 2.49999 6.10005 2.29999 6.30005L11.4 13.5Z"
                        fill="currentColor" />
                </svg>
            </span>
            {{ $instance->get('categories') }}
        </span>
        <div class="fw-bold">Nationality:-</div>
        <span class="d-flex align-items-center fs-7 fw-bold mb-2">
            <span class="svg-icon svg-icon-6 svg-icon-gray-600 me-2">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.3"
                        d="M21 18H3C2.4 18 2 17.6 2 17V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V17C22 17.6 21.6 18 21 18Z"
                        fill="currentColor" />
                    <path
                        d="M11.4 13.5C11.8 13.8 12.3 13.8 12.6 13.5L21.6 6.30005C21.4 6.10005 21.2 6 20.9 6H2.99998C2.69998 6 2.49999 6.10005 2.29999 6.30005L11.4 13.5Z"
                        fill="currentColor" />
                </svg>
            </span>
            {{ $instance->get('nationality') }}
        </span> --}}
    </div>
    <div class="card-footer">
        <div class="text-center mb-1">
            <button data-sweetalert-form="{{ sprintf('preorientation%s', $instance->id) }}"
                class="btn btn-sm {{ $instance->status === 'completed' ? ' btn-secondary' : 'btn-light sweet-notify' }} me-2 btn-detail"
                data-completed={{ $instance->status }}>View Detail</button>
        </div>
    </div>
</div>

<x-form-preorientationquiz :orientation="$instance" />
