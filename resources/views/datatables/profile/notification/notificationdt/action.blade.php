@php
    $iconConstants = getConstant('IconConstant');
@endphp
<div class="btn-group" role="group">
    @if(!empty($instance['show'] ?? []))
        <a href="{{$instance['show']['redirect'] ?? null}}" 
            data-id="{{$instance->id}}"
            @if(empty($instance->read_at ?? null)) onclick="event.preventDefault();modules.profile.notification.index.updateRead(this)" @endif title="{{ $instance['show']['swal']['settings']['title'] ?? 'View Detail' }}" class="btn btn-icon btn-{{$iconConstants['btn_show']['color']['class']}} btn-sm">
            <i class="{{$iconConstants['btn_show']['icon_class']}}"></i>
        </a>
    @endif
</div>