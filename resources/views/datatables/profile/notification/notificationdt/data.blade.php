@if (empty($instance->read_at))
    <span class="badge badge-danger fw-bold fs-9 px-2 py-1 me-1">New</span>
@endif
{{ $content ?? null }}
