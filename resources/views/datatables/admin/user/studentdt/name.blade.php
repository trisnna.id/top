<div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
    <div class="symbol-label fs-3 bg-light-{{$instance->users_meta_color}} text-{{$instance->users_meta_color}}">{{upperSubstr($instance->name)}}</div>
</div>
<div class="d-flex flex-column">
    <span class="text-gray-800 text-hover-primary mb-1">{{$instance->name}}</span>
</div>