<div class="btn-group" role="group">
    <a href="#" title="View Detail" class="btn btn-icon btn-info btn-sm upsert-swal-dt" data-data="{{json_encode($instance['profile']['data'])}}" data-swal="{{json_encode($instance['profile']['swal'])}}">
        <i class="fas fa-eye"></i>
    </a>
    <a href="{{$instance['orientation']['url']}}" title="Orientation Activity" target="_blank" class="btn btn-icon btn-{{getConstant('IconConstant','colors','ORIENTATION')[1]['class']}} btn-sm">
        <i class="{{getConstant('IconConstant','icon_class','ORIENTATION')}}"></i>
    </a>
    <a href="#" title="Activity Point" class="btn btn-icon btn-{{getConstant('IconConstant','color','POINT')['class']}} btn-sm upsert-swal-dt" data-data="{{json_encode($instance['point']['data'])}}" data-swal="{{json_encode($instance['point']['swal'])}}">
        <i class="{{getConstant('IconConstant','icon_class','POINT')}}"></i>
    </a>
</div>