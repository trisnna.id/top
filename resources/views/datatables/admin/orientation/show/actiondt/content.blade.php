<div class="pe-3 mb-5">
    <div class="fs-5 fw-semibold mb-2">
        {{ $actionLogConstant['log']['title'] }}&nbsp;<strong>{{ !empty($instance->created_at ?? null) ? $instance->created_at->format(config('app.date_format.php') . ' ' . config('app.time_format.php')) : null }}</strong>
    </div>
    <div class="d-flex align-items-center mt-1 fs-6">
        <div class="d-flex align-items-center justify-content-center mb-3">
            <div class="symbol symbol-30px symbol-circle me-3">
                <div class="symbol-label fs-3 bg-light-success text-success">
                    {{ upperSubstr($instance->user->name) }}
                </div>
            </div>
            <div class="m-0">
                <span class="d-block fs-8">
                    By <strong>{{ $instance->user->name }}</strong>
                </span>
            </div>
        </div>
    </div>
    @if (!empty($instance->note ?? null))
        <div class="notice d-flex bg-light-primary rounded border-primary border border-dashed flex-shrink-0 p-6">
            <div class="d-flex flex-stack flex-grow-1 flex-wrap flex-md-nowrap">
                <div class="mb-3 mb-md-0 fw-semibold">
                    <h4 class="text-gray-900 fw-bold">Comment/Note</h4>
                    <div class="fs-6 text-gray-700 pe-7">
                        {{ $instance->note }}
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
