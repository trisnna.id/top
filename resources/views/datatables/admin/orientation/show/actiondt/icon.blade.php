<div class="symbol-label bg-light">
    <i class="{{ $icon['icon_class'] }} fs-2x text-{{ $icon['color']['class'] }}"></i>
</div>
