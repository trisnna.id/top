<div class="d-flex align-items-center">
    @if ($type === 'name')
        <span class="mb-1">{{$instance->name}}</span>
    @else
        <div class="symbol symbol-50px overflow-hidden me-3">
            @if($instance->getFirstMedia() && in_array($instance->getFirstMedia()->extension,['png','jpg']))
                {{$instance->getFirstMedia()}}
            @elseif($instance->getFirstMedia())
                <a target="_blank" href="{{$instance->getFirstMediaUrl()}}">
                    <i class="fa-solid {{fileIconExtension($instance->getFirstMedia()->extension)}} fs-3x text-primary"></i>
                </a>
            @endif
        </div>
        <div class="d-flex flex-column">
            @if ($type === 'file_name')
                @if($instance->getFirstMedia())
                    <a target="_blank" href="{{$instance->getFirstMediaUrl()}}" class="text-gray-800 text-hover-primary">{{$instance->getFirstMedia()->file_name}}</a>
                @endif
            @endif
        </div>
    @endif
</div>