@if ($resource->type !== 'video')
    @if ($resource->type === 'file')
        <a href="{{ $resource->value }}" download>{{ __('Download') }}</a>
    @elseif ($resource->type === 'link' && str_contains($resource->value, 'youtube')) 
        <iframe
            width="100%"
            height="100%"
            src="{{ $resource->value }}"
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen="">
        </iframe>
    @else
        <a href="{{ $resource->value }}" target="_blank">{{ __('Visit link') }}</a>
    @endif
@else
    <video width="100%" height="100%" controls>
        <source src="{{ $resource->value }}">
    </video>
@endif

