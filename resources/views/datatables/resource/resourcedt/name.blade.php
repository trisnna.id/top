<div class="card min-h-250px rounded border" style="border-radius: 10% !important;"><img
    class="card-img-top rounded-4" src="{{$instance->getFirstMediaUrl('thumbnail')}}">
<div class="card-body d-flex justify-content-center flex-column">
    <h3 class="card-title text-center" style="font-family: Antic, sans-serif;color: rgb(81,87,94);" align="center">
        {{$instance->title}}</h3>
    <div class="d-flex justify-content-center">
        <a href="{{$instance['show']['redirect']}}" class="btn btn-sm btn-info me-2">View Detail</a>
    </div>
</div>
</div>
