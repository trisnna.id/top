@push('student.styles')
    <style>
        .btn-secondary.week-link span,
        .btn-secondary.week-link i {
            color: #000000 !important;
        }

        .week-link span,
        .week-link i {
            color: #f3f3f3 !important;
        }

        .btn-orange,
        .btn-orange:hover,
        .btn-orange:active,
        .btn-orange:focus,
        .btn-orange:visited {
            background-color: #FF6600;
        }

        .menu-link .menu-link:hover:not(.disabled):not(.active):not(.here) {
            background-color: #FF6600 !important;
        }

        .tooltip-arrow,
        .tooltip>.tooltip-inner {
            /* font-weight: bold; */
            /* font-style: italic; */
        }
    </style>
@endpush

<div class="card shadow-sm">
    <div class="card-body">
        <div id="checkpointContainer"
            class="menu menu-rounded menu-column menu-title-gray-700 menu-icon-gray-400 menu-arrow-gray-400 menu-bullet-gray-400 menu-arrow-gray-400 menu-state-bg fw-semibold"
            data-kt-menu="true">
            @foreach ($milestones as $key => $milestone)
                @if ($milestone->preOrientations->count() < 1)
                    @continue
                @endif
                @php
                    $studentMilestone =
                        $studentActivities->first(function ($item) use ($milestone) {
                            return $item->get('milestone') === $milestone->id;
                        }) ?? collect([]);
                    $studentMilestoneCurrent =
                        $studentActivities->first(function ($item) {
                            return !$item->get('completed');
                        }) ?? collect([]);

                    if ($studentMilestone->get('completed') && $studentMilestone->get('countActivities') >= 1) {
                        $title = 'Completed';
                    } elseif ($studentMilestone->get('completed') && $studentMilestone->get('countActivities') < 1) {
                        $title = 'There are no activites for this milestone';
                    } else {
                        $title = 'Incomplete';
                    }
                @endphp
                <div class="menu-item btn-checkpoint py-2">
                    <a href="#" data-checkpoint="{{ $milestone->id }}"
                        data-student-milestone-current="{{ $studentMilestoneCurrent->toJson() }}"
                        data-student-milestone="{{ $studentMilestone->toJson() }}"
                        class="menu-link btn fw-semibold week-link py-3 py-2" data-bs-toggle="tooltip"
                        data-bs-placement="right" title="{{ $title }}">
                        <span class="menu-icon">
                            <i class="bi bi-calendar-event fs-3" style="color: rgb(0,0,0) !important;"></i>
                        </span>
                        <span class="menu-title" style="color: rgb(0,0,0) !important;">{{ $milestone->label }}</span>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
