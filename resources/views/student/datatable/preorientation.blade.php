{{-- <div class="card min-h-250px shadow-md">
    <div class="card-header">
        <h3 class="card-title align-items-start flex-column w-50">
            <span class="card-label fw-bold text-gray-800">{{ $entity->activity_name }}</span>
        </h3>
        <div class="card-toolbar align-items-end flex-column">
            <div class="fw-bold mt-5 mb-2">
                <span class="badge badge-secondary">{{ $entity->checkpoint->label }}</span>
            </div>
        </div>
    </div>
    <div class="card-body h-250px" style="">
        <img src="{{ asset($entity->thumbnail) }}" class="img img-fluid"
            style="display: block; width: 100%; height: auto;">
    </div>
    <div class="card-footer">
        <div class="mb-1 text-center">
            <button
                class="btn btn-sm btn-{{ $entity->students->isEmpty() ? 'secondary' : 'success' }} me-2 btn-view-detail hover-scale"
                data-milestone="{{ $entity->checkpoint_id }}">
                <input id="preOrientation" type="hidden" value="{{ json_encode($entity) }}"
                    data-csrf="{{ csrf_token() }}" data-url-submit-quiz="{{ route('api.preorientation.submitmcq') }}">
                {{ __('View Detail') }}
            </button>
        </div>
    </div>
</div> --}}
<div class="card min-h-250px preor-card rounded border shadow" style="border-radius: 10% !important;"><img
        class="card-img-top card-img-preor rounded-4" src="{{ asset($entity->thumbnail) }}" />
    <div class="card-body d-flex justify-content-center flex-column">
        <h3 class="card-title text-center" style="font-family: Antic, sans-serif;color: rgb(81,87,94);" align="center">
            {{ $entity->activity_name }}</h3>
        <div class="d-flex justify-content-center">
            <button class="btn btn-sm me-2 btn-view-detail hover-scale text-white"
                data-milestone="{{ $entity->checkpoint_id }}" style="background-color: #467599 !important">
                <input id="preOrientation" type="hidden" value="{{ json_encode($entity) }}"
                    data-csrf="{{ csrf_token() }}" data-url-submit-quiz="{{ route('api.preorientation.submitmcq') }}">
                {{ __('View Detail') }}
            </button>
        </div>
    </div>
</div>

<x-form-preorientationquiz :entity="$entity" />
