<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center me-3 flex-wrap">
                <h1 class="page-heading d-flex fw-bold fs-3 flex-column justify-content-center my-0 text-white">
                    {{ $title }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-hover-white text-white">Home</a>
                    </li>
                    @yield('student.breadcrumb')
                </ul>
            </div>
            {{-- <div class="d-flex align-items-center gap-2 gap-lg-3">
            </div> --}}
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        @yield('student.content')
    </div>

    @push('styles')
        @stack('student.styles')
        <style>
            .toast-milestone {
                background-color: #5C7099 !important;
            }
        </style>
    @endpush

    @push('scripts')
        @stack('student.scripts')
    @endpush
</x-layouts.app>
