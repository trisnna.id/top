<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center me-3 flex-wrap">
                <h1 class="page-heading d-flex fw-bold fs-3 flex-column justify-content-center my-0 text-white">
                    Post Orientation Survey</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-hover-white text-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet w-5px h-2px bg-gray-400"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('orientations.index2') }}" class="text-hover-white text-white">Orientation
                            Activities</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet w-5px h-2px bg-gray-400"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('orientations.post-survey') }}" class="text-hover-white text-white">Post
                            Survey</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center gap-lg-3 gap-2">
            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">

        <form action="{{ route('orientations.post-survey') }}" method="post"
            class="card d-flex flex-column gap-10 px-5 py-5 pt-5">
            @csrf
            <div id="q1">
                <h4 class="mb-6">1. How useful was the information in the videos you watched on the 'Pre-Orientation'
                    page of Taylor’s Orientation Portal?</h4>

                <div id="ans1" class="d-flex flex-column gap-3 px-5">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q1" value="Extremely useful"
                            @if ($is_answered) @if ($answer['q1']['answer'] == 'Extremely useful')
                                checked @endif
                            disabled @endif
                        >
                        <label class="form-check-label text-dark">
                            Extremely useful
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q1" value="Somewhat useful"
                            @if ($is_answered) @if ($answer['q1']['answer'] == 'Somewhat useful')
                                checked @endif
                            disabled @endif>
                        <label class="form-check-label text-dark">
                            Somewhat Useful
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q1" value="Neutral"
                            @if ($is_answered) @if ($answer['q1']['answer'] == 'Neutral')
                                checked @endif
                            disabled @endif>
                        <label class="form-check-label text-dark">
                            Neutral
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q1" value="Somewhat not useful"
                            @if ($is_answered) @if ($answer['q1']['answer'] == 'Somewhat not useful')
                                checked @endif
                            disabled @endif>
                        <label class="form-check-label text-dark">
                            Somewhat not useful
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q1" value="Extremely not useful"
                            @if ($is_answered) @if ($answer['q1']['answer'] == 'Extremely not useful')
                                checked @endif
                            disabled @endif>
                        <label class="form-check-label text-dark">
                            Extremely not useful
                        </label>
                    </div>
                </div>
            </div>
            <div id="q2">
                <h4 class="mb-6">2. Rate your experience using Taylor's Orientation Portal?</h4>

                <div id="ans2" class="d-flex flex-column gap-3 px-5">
                    <div class="d-flex justify-content-start ratings mb-2" id="ratings2"
                        @if ($is_answered) data-rating="{{ $answer['q2']['answer'] }}" @endif>
                    </div>
                    <input type="hidden" name="q2">
                </div>
            </div>
            <div id="q3">
                <h4 class="mb-6">3. How would you rate the level of support you received from the Orientation
                    Leaders
                    ?</h4>

                <div id="ans3" class="d-flex flex-column gap-3 px-5">
                    <div class="d-flex justify-content-start ratings mb-2" id="ratings3"
                        @if ($is_answered) data-rating="{{ $answer['q3']['answer'] }}" @endif>
                    </div>
                    <input type="hidden" name="q3">
                </div>
            </div>
            <div id="q4">
                <h4 class="mb-6">4. Please indicate your level of concern for each of the following topics related
                    to
                    your transition into Taylor's after attending Orientation.</h4>

                <div id="ans4" class="d-flex flex-column gap-3 px-5" style="overflow: auto hidden !important;">
                    <table class="table-stripped table">
                        <tbody>
                            <tr>
                                <td></td>
                                <td class="text-center">Not at all concerned</td>
                                <td class="text-center">Slightly concerned</td>
                                <td class="text-center">Somewhat concerned</td>
                                <td class="text-center">Moderately concerned</td>
                                <td class="text-center">Extremely concerned</td>
                            </tr>
                            <tr>
                                <td>Academic Support</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-1"
                                        value="Not at all concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-1']['answer'] == 'Not at all concerned')
                                                checked @endif
                                        disabled @endif
                                    >
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-1"
                                        value="Slightly concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-1']['answer'] == 'Slightly concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-1"
                                        value="Somewhat concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-1']['answer'] == 'Somewhat concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-1"
                                        value="Moderately concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-1']['answer'] == 'Moderately concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-1"
                                        value="Extremely concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-1']['answer'] == 'Extremely concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                            </tr>
                            <tr>
                                <td>Making New Friends</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-2"
                                        value="Not at all concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-2']['answer'] == 'Not at all concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-2"
                                        value="Slightly concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-2']['answer'] == 'Slightly concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-2"
                                        value="Somewhat concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-2']['answer'] == 'Somewhat concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-2"
                                        value="Moderately concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-2']['answer'] == 'Moderately concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-2"
                                        value="Extremely concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-2']['answer'] == 'Extremely concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                            </tr>
                            <tr>
                                <td>Personal Well-being</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-3"
                                        value="Not at all concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-3']['answer'] == 'Not at all concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-3"
                                        value="Slightly concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-3']['answer'] == 'Slightly concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-3"
                                        value="Somewhat concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-3']['answer'] == 'Somewhat concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-3"
                                        value="Moderately concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-3']['answer'] == 'Moderately concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-3"
                                        value="Extremely concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-3']['answer'] == 'Extremely concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                            </tr>
                            <tr>
                                <td>Campus Facilities and Services</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-4"
                                        value="Not at all concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-4']['answer'] == 'Not at all concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-4"
                                        value="Slightly concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-4']['answer'] == 'Slightly concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-4"
                                        value="Somewhat concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-4']['answer'] == 'Somewhat concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-4"
                                        value="Moderately concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-4']['answer'] == 'Moderately concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-4"
                                        value="Extremely concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-4']['answer'] == 'Extremely concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                            </tr>
                            <tr>
                                <td>Support from Staff</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-5"
                                        value="Not at all concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-5']['answer'] == 'Not at all concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-5"
                                        value="Slightly concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-5']['answer'] == 'Slightly concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-5"
                                        value="Somewhat concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-5']['answer'] == 'Somewhat concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-5"
                                        value="Moderately concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-5']['answer'] == 'Moderately concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q4-5"
                                        value="Extremely concerned"
                                        @if ($is_answered) @if ($answer['q4']['answer']['q4-5']['answer'] == 'Extremely concerned')
                                                checked @endif
                                        disabled @endif>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="q5">
                <h4 class="mb-6">5. Please share your thoughts and experiences about your overall orientation as
                    a
                    freshman.</h4>

                <div id="ans5" class="d-flex flex-column gap-3 px-5">
                    <textarea class="form-control" name="q5" id="q5" cols="30" rows="10"
                        @if ($is_answered) readonly @endif>
@if ($is_answered) {{ $answer['q5']['answer'] }} @endif
</textarea>
                </div>
            </div>
            <div id="q6">
                <h4 class="mb-6">6. How likely are you to recommend Taylor's to a friend or colleague?
                </h4>

                <div id="ans6" class="d-flex flex-column w-md-50 w-100 gap-3 px-5">
                    <table border="1">
                        <tbody>
                            <tr class="rating-1-10">
                                <td @if ($is_answered) @if ($answer['q6']['answer'] == '1')
                                        class="q6-selected" @endif
                                    @endif
                                    >1</td>
                                <td @if ($is_answered) @if ($answer['q6']['answer'] == '2')
                                        class="q6-selected" @endif
                                    @endif
                                    >2</td>
                                <td @if ($is_answered) @if ($answer['q6']['answer'] == '3')
                                        class="q6-selected" @endif
                                    @endif
                                    >3</td>
                                <td @if ($is_answered) @if ($answer['q6']['answer'] == '4')
                                        class="q6-selected" @endif
                                    @endif
                                    >4</td>
                                <td @if ($is_answered) @if ($answer['q6']['answer'] == '5')
                                        class="q6-selected" @endif
                                    @endif
                                    >5</td>
                                <td @if ($is_answered) @if ($answer['q6']['answer'] == '6')
                                        class="q6-selected" @endif
                                    @endif
                                    >6</td>
                                <td @if ($is_answered) @if ($answer['q6']['answer'] == '7')
                                        class="q6-selected" @endif
                                    @endif
                                    >7</td>
                                <td @if ($is_answered) @if ($answer['q6']['answer'] == '8')
                                        class="q6-selected" @endif
                                    @endif
                                    >8</td>
                                <td @if ($is_answered) @if ($answer['q6']['answer'] == '9')
                                        class="q6-selected" @endif
                                    @endif
                                    >9</td>
                                <td @if ($is_answered) @if ($answer['q6']['answer'] == '10')
                                        class="q6-selected" @endif
                                    @endif
                                    >10</td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="q6">
                </div>
            </div>

            @if (!$is_answered)
                <div class="buttons w-100 d-flex justify-content-end">
                    <button type="submit" class="btn btn-red">Submit</button>
                </div>
            @endif
        </form>

    </div>


    @push('styles')
        <link rel="stylesheet"
            href="https://cdn.jsdelivr.net/gh/nashio/star-rating-svg@master/src/css/star-rating-svg.css">

        <style>
            .rating-1-10>td {
                border: 0.0625rem solid #000000;
                color: #000000;
                width: 8%;
                text-align: center;

                cursor: pointer;
            }

            .rating-1-10>td:hover {
                background-color: #cacaca;
            }

            .rating-1-10>td.q6-selected {
                background-color: #ff0000;
                color: #ffffff;
            }

            .rating-1-10 {
                height: 3.125rem;
            }
        </style>
    @endpush
    @push('scripts')
        <script src="{{ asset('js/vendor/jquery-star-rating.js') }}"></script>
        <script>
            let readOnly = {{ $is_answered ? 'true' : 'false' }};
            let disabled = {{ $is_answered ? 'true' : 'false' }};
            $("#ratings2").starRating({
                totalStars: 5,
                starSize: 40,
                emptyColor: '#ffffff',
                activeColor: '#8e562e',
                useGradient: false,
                readOnly: readOnly,
                starShape: 'rounded',
                useFullStars: true,
                disableAfterRate: disabled,
                callback: function(currentRating, $el) {
                    $('input[name="q2"]').val(currentRating);
                }
            });
            $("#ratings3").starRating({
                totalStars: 5,
                starSize: 40,
                emptyColor: '#ffffff',
                activeColor: '#8e562e',
                useGradient: false,
                readOnly: readOnly,
                starShape: 'rounded',
                useFullStars: true,
                disableAfterRate: disabled,
                callback: function(currentRating, $el) {
                    $('input[name="q3"]').val(currentRating);
                }
            });
        </script>
        @if (!$is_answered)
            <script>
                // every td on .rating-1-10 on click, set the value of the input to the index of the td, then set background color of the td to #ff0000 and text color to #ffffff
                $('.rating-1-10 td').click(function() {
                    $('input[name="q6"]').val($(this).index() + 1);
                    $(this).parent().find('td').css('background-color', '#ffffff');
                    $(this).parent().find('td').css('color', '#000000');
                    $(this).css('background-color', '#ff0000');
                    $(this).css('color', '#ffffff');
                });
            </script>
        @endif
        <script>
            // make all input required. if submit button is clicked, check if all inputs are filled. if not, alert the user
            $('form').submit(function(e) {
                e.preventDefault();
                let isFilled = true;

                var radios = $('.card input[type="radio"][class="form-check-input"]');

                // get all radio button. group them by name. check if any of them in the group is checked. if not, set isFilled to false
                radios.each(function() {
                    if ($('input[name="' + $(this).attr('name') + '"]:checked').length == 0) {
                        isFilled = false;
                    }
                });
                $('.card input[type="hidden"]').each(function() {
                    if ($(this).val() == '') {
                        isFilled = false;
                    }
                });
                $('.card textarea#q5').each(function() {
                    if ($(this).val() == '') {
                        isFilled = false;
                    }
                });
                if (isFilled) {
                    $(this).unbind('submit').submit();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Please fill up all questions!',
                    });
                }
            });
        </script>
    @endpush
    @push('styles')
        <style>
            /* make all radio button more contrast, because the background is white */
            input[type="radio"] {
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                background-color: #aaaaaa;
                border: 1px solid #e2e8f0;
                border-radius: 0.25rem;
                height: 1.5rem;
                width: 1.5rem;
                transition: all 0.15s ease-in-out 0s;
            }
        </style>
    @endpush
</x-layouts.app>
