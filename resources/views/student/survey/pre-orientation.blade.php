<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center me-3 flex-wrap">
                <h1 class="page-heading d-flex fw-bold fs-3 flex-column justify-content-center my-0 text-white">
                    Pre Orientation Survey</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-hover-white text-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet w-5px h-2px bg-gray-400"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('orientations.index2') }}" class="text-hover-white text-white">Orientation
                            Activities</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet w-5px h-2px bg-gray-400"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('student.preorientation.survey') }}" class="text-hover-white text-white">Pre
                            Orientation
                            Survey</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center gap-lg-3 gap-2">
            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">

        <form action="{{ route('student.preorientation.survey') }}" method="post"
            class="card d-flex flex-column gap-10 px-5 py-5 pt-5">
            @csrf
            {{-- <div id="q1">
                <h4 class="mb-6">1. Taylor's Student ID number (the alternative is your IC/Passport number)</h4>

                <div id="ans1" class="d-flex flex-column gap-3 px-5">
                    <input type="text" name="q1" class="form-control"
                        @if ($is_answered) value="{{ $answer['q1']['answer'] }}"
                        readonly @endif>
                </div>
            </div> --}}
            <div id="q2">
                <h4 class="mb-6">1. Nationality</h4>

                <div id="ans2" class="d-flex flex-column gap-3 px-5">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q1" value="International"
                            @if ($is_answered && $answer['q1']['answer'] == 'International') checked @endif
                            @if ($is_answered) disabled @endif>
                        <label class="form-check-label text-dark">
                            International
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q1" value="Malaysian"
                            @if ($is_answered && $answer['q1']['answer'] == 'Malaysian') checked @endif
                            @if ($is_answered) disabled @endif>
                        <label class="form-check-label text-dark">
                            Malaysian
                        </label>
                    </div>
                </div>
            </div>
            {{-- <div id="q3">
                <h4 class="mb-6">3. Faculty</h4>

                <div id="ans3" class="d-flex flex-column gap-3 px-5">
                    <select class="form-select select2" name="q3" id="faculty">
                        <option selected disabled>Select One</option>
                        @foreach ($faculties as $faculty)
                            <option value="{{ $faculty->id }}" @if ($is_answered && $answer['q3']['answer'] == $faculty->id) selected @endif
                                @if ($is_answered) disabled @endif>
                                {{ $faculty->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div id="q4">
                <h4 class="mb-6">4. Programme</h4>

                <div id="ans4" class="d-flex flex-column gap-3 px-5">
                    <select class="form-select select2" name="q4" id="programme">
                        <option selected disabled>Select One</option>
                        @foreach ($programmes as $programme)
                            <option value="{{ $programme->id }}" @if ($is_answered && $answer['q4']['answer'] == $programme->id) selected @endif
                                @if ($is_answered) disabled @endif>{{ $programme->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div> --}}
            <div id="q5">
                <h4 class="mb-6">2. Please indicate your level of concern for each of the following topics related to
                    your transition into Taylor's.</h4>
                <div id="ans5" class="d-flex flex-column gap-3 px-5" style="overflow: auto hidden !important;">
                    <table class="table-stripped table">
                        <tbody>
                            <tr>
                                <td></td>
                                <td class="text-center">Not at all concerned</td>
                                <td class="text-center">Slightly concerned</td>
                                <td class="text-center">Somewhat concerned</td>
                                <td class="text-center">Moderately concerned</td>
                                <td class="text-center">Extremely concerned</td>
                            </tr>
                            <tr class="malaysian">
                                <td>Academic Support</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-1"
                                        value="Not at all concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-1']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-1"
                                        value="Slightly concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-1']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-1"
                                        value="Somewhat concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-1']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-1"
                                        value="Moderately concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-1']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-1"
                                        value="Extremely concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-1']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="malaysian">
                                <td>Making New Friends</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-2"
                                        value="Not at all concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-2']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-2"
                                        value="Slightly concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-2']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-2"
                                        value="Somewhat concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-2']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-2"
                                        value="Moderately concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-2']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-2"
                                        value="Extremely concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-2']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="malaysian">
                                <td>Personal Well-being</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-3"
                                        value="Not at all concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-3']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-3"
                                        value="Slightly concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-3']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-3"
                                        value="Somewhat concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-3']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-3"
                                        value="Moderately concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-3']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-3"
                                        value="Extremely concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-3']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="malaysian">
                                <td>Support from staff</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-4"
                                        value="Not at all concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-4']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-4"
                                        value="Slightly concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-4']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-4"
                                        value="Somewhat concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-4']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-4"
                                        value="Moderately concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-4']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-4"
                                        value="Extremely concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-4']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="malaysian">
                                <td>Campus Facilities and Services</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-5"
                                        value="Not at all concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-5']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-5"
                                        value="Slightly concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-5']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-5"
                                        value="Somewhat concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-5']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-5"
                                        value="Moderately concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-5']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-m-5"
                                        value="Extremely concerned" @if ($is_answered && $answer['q2']['answer']['q2-m-5']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="international">
                                <td>Academic Support</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-1"
                                        value="Not at all concerned" @if ($is_answered && $answer['q2']['answer']['q2-1']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-1"
                                        value="Slightly concerned" @if ($is_answered && $answer['q2']['answer']['q2-1']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-1"
                                        value="Somewhat concerned" @if ($is_answered && $answer['q2']['answer']['q2-1']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-1"
                                        value="Moderately concerned" @if ($is_answered && $answer['q2']['answer']['q2-1']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-1"
                                        value="Extremely concerned" @if ($is_answered && $answer['q2']['answer']['q2-1']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="international">
                                <td>Making New Friends</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-2"
                                        value="Not at all concerned" @if ($is_answered && $answer['q2']['answer']['q2-2']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-2"
                                        value="Slightly concerned" @if ($is_answered && $answer['q2']['answer']['q2-2']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-2"
                                        value="Somewhat concerned" @if ($is_answered && $answer['q2']['answer']['q2-2']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-2"
                                        value="Moderately concerned" @if ($is_answered && $answer['q2']['answer']['q2-2']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-2"
                                        value="Extremely concerned" @if ($is_answered && $answer['q2']['answer']['q2-2']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="international">
                                <td>Personal Well-being</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-3"
                                        value="Not at all concerned" @if ($is_answered && $answer['q2']['answer']['q2-3']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-3"
                                        value="Slightly concerned" @if ($is_answered && $answer['q2']['answer']['q2-3']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-3"
                                        value="Somewhat concerned" @if ($is_answered && $answer['q2']['answer']['q2-3']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-3"
                                        value="Moderately concerned" @if ($is_answered && $answer['q2']['answer']['q2-3']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-3"
                                        value="Extremely concerned" @if ($is_answered && $answer['q2']['answer']['q2-3']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="international">
                                <td>Support from staff</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-4"
                                        value="Not at all concerned" @if ($is_answered && $answer['q2']['answer']['q2-4']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-4"
                                        value="Slightly concerned" @if ($is_answered && $answer['q2']['answer']['q2-4']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-4"
                                        value="Somewhat concerned" @if ($is_answered && $answer['q2']['answer']['q2-4']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-4"
                                        value="Moderately concerned" @if ($is_answered && $answer['q2']['answer']['q2-4']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-4"
                                        value="Extremely concerned" @if ($is_answered && $answer['q2']['answer']['q2-4']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="international">
                                <td>Campus Facilities and Services</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-5"
                                        value="Not at all concerned" @if ($is_answered && $answer['q2']['answer']['q2-5']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-5"
                                        value="Slightly concerned" @if ($is_answered && $answer['q2']['answer']['q2-5']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-5"
                                        value="Somewhat concerned" @if ($is_answered && $answer['q2']['answer']['q2-5']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-5"
                                        value="Moderately concerned"
                                        @if ($is_answered && $answer['q2']['answer']['q2-5']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-5"
                                        value="Extremely concerned" @if ($is_answered && $answer['q2']['answer']['q2-5']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="international">
                                <td>Language and Communication</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-6"
                                        value="Not at all concerned"
                                        @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-6']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-6"
                                        value="Slightly concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-6']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-6"
                                        value="Somewhat concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-6']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-6"
                                        value="Moderately concerned"
                                        @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-6']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-6"
                                        value="Extremely concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-6']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="international">
                                <td>Cultural Adjusment</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-7"
                                        value="Not at all concerned"
                                        @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-7']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-7"
                                        value="Slightly concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-7']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-7"
                                        value="Somewhat concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-7']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-7"
                                        value="Moderately concerned"
                                        @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-7']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-7"
                                        value="Extremely concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-7']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="international">
                                <td>Visa and Legal Matters</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-8"
                                        value="Not at all concerned"
                                        @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-8']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-8"
                                        value="Slightly concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-8']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-8"
                                        value="Somewhat concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-8']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-8"
                                        value="Moderately concerned"
                                        @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-8']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-8"
                                        value="Extremely concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-8']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="international">
                                <td>Managing Finance</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-9"
                                        value="Not at all concerned"
                                        @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-9']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-9"
                                        value="Slightly concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-9']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-9"
                                        value="Somewhat concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-9']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-9"
                                        value="Moderately concerned"
                                        @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-9']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-9"
                                        value="Extremely concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-9']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                            <tr class="international">
                                <td>Accomodation</td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-10"
                                        value="Not at all concerned"
                                        @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-10']['answer'] == 'Not at all concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-10"
                                        value="Slightly concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-10']['answer'] == 'Slightly concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-10"
                                        value="Somewhat concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-10']['answer'] == 'Somewhat concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-10"
                                        value="Moderately concerned"
                                        @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-10']['answer'] == 'Moderately concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" name="q2-10"
                                        value="Extremely concerned" @if (
                                            $is_answered &&
                                                $answer['q1']['answer'] == 'International' &&
                                                $answer['q2']['answer']['q2-10']['answer'] == 'Extremely concerned') checked @endif
                                        @if ($is_answered) disabled @endif>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- <div id="q6">
                <h4 class="mb-6">6. Do you have any other concerns related to adapting to Taylor's that is not listed
                    above.</h4>

                <div id="ans6" class="d-flex flex-column gap-3 px-5">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q6" value="Yes"
                            @if ($is_answered && $answer['q6']['answer'] == 'Yes') checked @endif
                            @if ($is_answered) disabled @endif>
                        <label class="form-check-label text-dark">
                            Yes
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="q6" value="No"
                            @if ($is_answered && $answer['q6']['answer'] == 'No') checked @endif
                            @if ($is_answered) disabled @endif>
                        <label class="form-check-label text-dark">
                            No
                        </label>
                    </div>
                </div>
            </div> --}}

            @if (!$is_answered)
                <div class="buttons w-100 d-flex justify-content-end">
                    <button type="submit" class="btn btn-red">Submit</button>
                </div>
            @endif
        </form>
    </div>

    @push('scripts')
        @if (!$is_answered)
            <script>
                $(() => {
                    $('#q5').addClass('d-none')
                })
            </script>

            <script>
                var isInternational = false;
                $('input[name="q1"]').on('change', function() {
                    $('#q5').removeClass('d-none')

                    if ($(this).val() == 'International') {
                        $('.international').removeClass('d-none');
                        $('.malaysian').addClass('d-none');
                        isInternational = true;
                    } else if ($(this).val() == 'Malaysian') {
                        $('.international').addClass('d-none');
                        $('.malaysian').removeClass('d-none');
                        isInternational = false;
                    }
                });
            </script>
        @else
            @if ($answer['q1']['answer'] == 'International')
                <script>
                    $(() => {
                        $('.international').removeClass('d-none')
                        $('.malaysian').addClass('d-none')
                    })
                </script>
            @elseif ($answer['q1']['answer'] == 'Malaysian')
                <script>
                    $(() => {
                        $('.international').addClass('d-none')
                        $('.malaysian').removeClass('d-none')
                    })
                </script>
            @endif
        @endif

        <script>
            // make all input required. if submit button is clicked, check if all inputs are filled. if not, alert the user
            $('form').submit(function(e) {
                e.preventDefault();
                let isFilled = true;

                if (isInternational) {
                    var radios = $('#q5 .international input[type="radio"][class="form-check-input"]');
                } else if (!isInternational) {
                    var radios = $('#q5 .malaysian input[type="radio"][class="form-check-input"]');
                }

                // get all radio button. group them by name. check if any of them in the group is checked. if not, set isFilled to false
                radios.each(function() {
                    if ($('input[name="' + $(this).attr('name') + '"]:checked').length == 0) {
                        isFilled = false;
                    }
                });
                if (isFilled) {
                    $(this).unbind('submit').submit();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Please fill up all questions!',
                    });
                }
            });
        </script>
    @endpush
    @push('styles')
        <style>
            /* make all radio button more contrast, because the background is white */
            input[type="radio"] {
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                background-color: #aaaaaa;
                border: 1px solid #e2e8f0;
                border-radius: 0.25rem;
                height: 1.5rem;
                width: 1.5rem;
                transition: all 0.15s ease-in-out 0s;
            }
        </style>
    @endpush
</x-layouts.app>
