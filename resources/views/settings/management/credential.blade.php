<x-setting-layout :searchPlaceholder="__('Search password')">
    <x-slot name="form">
        <x-swal.upsert :id="'upsert'" :swalSettings="[
            'title' => 'Master Password',
            'showCancelButton' => true,
            'showConfirmButton' => true,
            'customClass' => [
                'popup' => 'w-300px w-md-500px',
            ]
        ]">
            <x-slot name="didOpen">
                
            </x-slot>
        </x-swal.upsert>
    </x-slot>
    <x-slot name="table">
       
    </x-slot>
</x-setting-layout>
