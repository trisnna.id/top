<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <x-slot name="form">
        <x-swal.upsert :id="'upsert'" :swalSettings="[
            'title' => 'Rule Form',
            'showCancelButton' => true,
            'showConfirmButton' => true,
            'customClass' => [
                'popup' => 'w-300px w-md-500px',
            ]
        ]">
            <div class="fv-row mb-10">
                <label class="required fw-semibold fs-6 mb-2">
                    File Size
                </label>
                <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder=""
                    value="" />
            </div>
            <div class="fv-row mb-10">
                <label class="required fw-semibold fs-6 mb-2">
                    File Types
                </label>
                <select name="file_types" class="form-select swal2-select2" id="fileTypes"
                    data-hide-search="true" data-allow-clear="true" data-placeholder="-- Please Select / All --" multiple>
                    <option></option>
                    <option value="jpg">JPG/JPEG</option>
                    <option value="png">PNG</option>
                    <option value="svg">SVG</option>
                </select>
            </div>
            <x-slot name="didOpen">
                
            </x-slot>
        </x-swal.upsert>
        {{-- <form id="kt_docs_formvalidation_text" class="form" action="#" autocomplete="off">
            <div class="fv-row mb-10">
                <label class="required fw-semibold fs-6 mb-2">
                    File Size
                </label>
                <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder=""
                    value="" />
            </div>
            <div class="fv-row mb-10">
                <label class="required fw-semibold fs-6 mb-2">
                    File Types
                </label>
                <select name="file_types" class="form-select form-select2" id="fileTypes"
                    data-hide-search="true" data-allow-clear="true" data-placeholder="-- Please Select / All --" multiple>
                    <option></option>
                    <option value="exchange_student">Exchange Student</option>
                    <option value="study_abroad_student">Study Abroad Student</option>
                </select>
            </div>
            <button id="kt_docs_formvalidation_text_submit" type="submit" class="btn btn-primary">
                <span class="indicator-label">
                    Validation Form
                </span>
                <span class="indicator-progress">
                    Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
            </button>
        </form> --}}
    </x-slot>
    <x-slot name="table">
        <form id="kt_docs_formvalidation_text" class="form" action="#" autocomplete="off">
            <div class="fv-row mb-10">
                <label class="required fw-semibold fs-6 mb-2">
                    File Size
                </label>
                <input type="text" name="text_input" class="form-control form-control-solid mb-3 mb-lg-0" placeholder=""
                    value="" />
            </div>
            <div class="fv-row mb-10">
                <label class="required fw-semibold fs-6 mb-2">
                    File Types
                </label>
                <select name="file_types" class="form-select form-select2" id="fileTypes"
                    data-hide-search="true" data-allow-clear="true" data-placeholder="-- Please select file types --" multiple>
                    <option></option>
                    <option value="jpg">JPG/JPEG</option>
                    <option value="png">PNG</option>
                    <option value="svg">SVG</option>
                </select>
            </div>
            <button id="kt_docs_formvalidation_text_submit" type="submit" class="btn btn-primary">
                <span class="indicator-label">
                    Validation Form
                </span>
                <span class="indicator-progress">
                    Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
            </button>
        </form>
        {{-- {{ $dataTable->table([
            'class' =>'table table-bordered table-striped align-middle table-row-dashed fs-6 gy-5 th-text-uppercase th-fw-bold w-100'
            ]) }} --}}
    </x-slot>
</x-setting-layout>

@push('script')
    {{ $dataTable->scripts() }}
@endpush
