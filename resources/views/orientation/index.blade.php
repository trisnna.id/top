<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-white fw-bold fs-3 flex-column justify-content-center my-0">
                    {{ $title }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-white text-hover-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('orientations.index') }}" class="text-white text-hover-white">{{$title}}</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center gap-2 gap-lg-3">
            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        @if($student->intake->is_timetable_active ?? false)
            @php
                $navItems = [
                    [
                        'aClass' => 'active',
                        'name' => 'timeline',
                        'tabClass' => 'show active',
                        'title' => 'Timetable',
                        'iconImg' => url('media/icons/taylors/timeline.png'),
                        'search' => 'Search',
                        'addButton' => 'Add Admin / Staff',
                        'color' => getConstant('IconConstant', 'color', 'TIMELINE')['class'],
                        'data' => [
                            'include1' => 'orientation.index.tab-timeline',
                        ],
                    ],
                    [
                        'aClass' => '',
                        'name' => 'timetable',
                        'tabClass' => '',
                        'title' => 'Calendar',
                        'iconImg' => url('media/icons/taylors/calendar-2.png'),
                        'search' => 'Search',
                        'addButton' => 'Add Student',
                        'color' => getConstant('IconConstant', 'color', 'TIMETABLE')['class'],
                        'data' => [
                            'include1' => 'orientation.index.tab-timetable',
                        ],
                    ],
                ];
            @endphp
            <div class="d-flex flex-stack flex-wrap">
                <div class="d-flex justify-content-start">
                    <div class="tab-content">
                        <div class="tab-pane active show fs-1 fw-bold" id="title_timeline">
                            <h3 class="fw-bold fs-1 my-2 text-gray-800">
                                My Orientation Activities
                                <x-tooltip.basic title='tooltip-timetable-my-orientation-activities'></x-tooltip.basic>
                            </h3>
                        </div>
                        <div class="tab-pane fs-1 fw-bold" id="title_timetable">
                            <h3 class="fw-bold fs-1 my-2 text-gray-800">
                                Orientation Calendar
                                <x-tooltip.basic title='tooltip-timetable-calendar-orientation-calendar'></x-tooltip.basic>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <ul class="nav nav-pills nav-pills-custom mb-3 d-flex flex-center">
                        @foreach ($navItems as $key => $navItem)
                            <li class="nav-item mb-3 me-3 me-lg-6">
                                <a class="nav-link btn btn-outline btn-flex btn-color-muted btn-active-color-primary flex-column overflow-hidden w-150px h-85px pt-5 pb-2 bg-white {{ $navItem['aClass'] }}"
                                    id="tab_link_{{ $navItem['name'] }}" data-bs-toggle="pill" href="#tab_{{ $navItem['name'] }}" data-href-title="#title_{{ $navItem['name'] }}" onclick="fullcalendars['orientationCalendar'].updateSize();fullcalendars['orientationCalendarMonth'].updateSize();">
                                    <img class="w-35px mb-1" src="{{ $navItem['iconImg'] ?? null }}">
                                    <span class="nav-text text-gray-800 fw-bold fs-6 lh-1">{{ $navItem['title'] }}</span>
                                    <span class="bullet-custom position-absolute bottom-0 w-100 h-4px bg-red"></span>
                                </a>
                            </li>
                        @endforeach
                        <li class="nav-item mb-3">
                            <a href="{{ route('orientations.export-timetable', ['student_id' => $student->uuid]) }}" class="nav-link btn btn-outline btn-flex btn-color-muted btn-active-color-primary flex-column overflow-hidden w-150px h-85px pt-5 pb-2 bg-white">
                                <img class="w-35px mb-1" src="{{ url('media/icons/taylors/download-2.png') }}">
                                <span class="nav-text text-gray-800 fw-bold fs-6 lh-1">Download</span>
                                <span class="bullet-custom position-absolute bottom-0 w-100 h-4px bg-red"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab_timeline">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                @include('orientation.index.tab-timeline-summary')
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="card">
                                @include('orientation.index.tab-timeline')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab_timetable">
                    <div class="card">
                        @include('orientation.index.tab-timetable')
                    </div>
                </div>
            </div>
        @else
            <div class="card mb-xl-10 mb-5">
                <div class="card-header">
                    <div class="py-5 w-100">
                        <div class="d-flex align-items-center bg-light-primary rounded py-5 px-5">
                            <span class="svg-icon svg-icon-3x svg-icon-primary me-5"><svg width="24" height="24"
                                    viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect opacity="0.3" x="2" y="2" width="20" height="20"
                                        rx="10" fill="currentColor"></rect>
                                    <rect x="11" y="14" width="7" height="2" rx="1"
                                        transform="rotate(-90 11 14)" fill="currentColor"></rect>
                                    <rect x="11" y="17" width="2" height="2" rx="1"
                                        transform="rotate(-90 11 17)" fill="currentColor"></rect>
                                </svg>
                            </span>
                            <div class="fw-bold fs-6 text-gray-700">{!! $student->intake->timetable_message ?? null !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    @push('script')
        modules.orientation.index.init();
    @endpush
</x-layouts.app>
