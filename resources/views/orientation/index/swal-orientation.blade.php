<x-swal.upsert :id="$instance['orientation']['swal']['id']" :swalSettings="$instance['orientation']['swal']['settings']">
    <div class="row mb-5 m-0">
        <label class="fw-bold">Activity</label>
        <span class="fs-6" data-model="name"></span>
    </div>
    <div class="row mb-5 m-0">
        <label class="fw-bold">Date</label>
        <span class="fs-6" data-model="date"></span>
    </div>
    <div class="row mb-5 m-0">
        <label class="fw-bold">Time</label>
        <span class="fs-6" data-model="time"></span>
    </div>
    <div class="row mb-5 m-0" id="venueContainer">
        <label class="fw-bold">Venue</label>
        <span class="fs-6" data-model="venue"></span>
    </div>
    <div class="row mb-5 m-0" id="livestreamContainer">
        <label class="fw-bold">Livestream Link</label>
        <a class="fs-6" href="" data-model="livestream"></a>
    </div>
    <div class="row mb-5 m-0" id="attendanceContainer">
        <label class="fw-bold">Attendance</label>
        <span class="fs-6" data-model="attendance"></span>
    </div>
    <div class="row mb-5 m-0" id="ratingContainer">
        <label class="fw-bold">Rating</label>
        <span class="fs-6" data-model="rating"></span>
    </div>
    <div class="row mb-5 m-0" id="presenterContainer">
        <label class="fw-bold">Presenter</label>
        <span class="fs-6" data-model="presenter"></span>
    </div>
    <div class="row mb-5 m-0" id="pointContainer">
        <label class="fw-bold">Point</label>
        <span class="fs-6" data-model="point"></span>
    </div>
    <div class="row mb-5 m-0" id="agendaContainer">
        <label class="fw-bold">Agenda</label>
        <span class="fs-6" data-model="agenda"></span>
    </div>
    <div class="row mb-5 m-0" id="synopsisContainer">
        <label class="fw-bold">Description</label>
        <span class="fs-6" data-model="synopsis"></span>
    </div>
    <div class="row mb-7 m-0">
        <label class="fw-bold">Attachment(s)</label>
        <input id="orientation_id" type="hidden" name="orientation_id">
        {!! $attachmentDt->html()->table() !!}
    </div>
    <div class="text-center mb-1">
        @if(!empty($instance['attendance'] ?? []) && request()->route()->getName() !== "admin.students.show-orientation")
            <a id="btn-attendance" class="btn btn-sm btn-info upsert-swal-popup me-3" href="#" data-swal="{{json_encode($instance['attendance']['swal'])}}" ><i class="fa-solid fa-qrcode"></i>Scan Attendance</a>
        @endif
        @if(config('services.google.calendar.enabled'))
            <a id="btn-calendar" class="btn btn-sm btn-info" data-calendar="insert" href="#">Add to Calendar</a>
        @endif
    </div>
    <x-slot name="didOpen">
        modules.orientation.index.swalOrientation('form#orientation',data);
        dtx.initOption('attachmentDt');
        {!! $attachmentDt->html()->generateScripts() !!}
    </x-slot>
</x-swal.upsert>