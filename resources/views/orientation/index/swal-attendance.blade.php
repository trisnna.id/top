<x-swal.upsert :id="$instance['attendance']['swal']['id']" :swalSettings="$instance['attendance']['swal']['settings']">
    <input type="hidden" name="student_id">
    <input type="hidden" name="orientation_id">
    <input type="hidden" name="attendance_qrcode">
    <div class="py-5 w-100">
        <div class="d-flex align-items-center bg-light-primary rounded py-5 px-5">
            <span class="svg-icon svg-icon-3x svg-icon-primary me-5"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor"></rect>
                    <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor"></rect>
                    <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor"></rect>
                </svg>
            </span>
            <div class="fw-bold fs-6 text-gray-700">
                Scanning your attendance will give you {{ setting('orientation.point_attendance') }} points
            </div>
        </div>
    </div>
    <div class="row mb-7 m-0">
        <label class="fw-bold">Name</label>
        <span class="fs-6" data-model="name"></span>
    </div>
    <div class="row mb-7 m-0">
        <label class="fw-bold">Scan from Camera</label>
        <div id="video-container">
            <video class="w-100" id="qr-video"></video>
        </div>
    </div>
    <div class="row mb-7 m-0">
        <label class="fw-bold">Scan from File</label>
        <input type="file" id="file-selector">
    </div>
    <x-slot name="didOpen">
        modules.orientation.index.swalAttendance('form#attendance',data);
    </x-slot>
    <x-slot name="didDestroy">
        modules.orientation.index.swalAttendanceDestroy('form#attendance',data);
    </x-slot>
</x-swal.upsert>