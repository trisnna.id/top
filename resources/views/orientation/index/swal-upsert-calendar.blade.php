<x-swal.upsert :id="'upsertCalendar'" :swalSettings="[]">
    <div id="eventsCalendarContainer"></div>
    <div class="text-center mb-1">
        <a id="btn-calendar" class="btn btn-sm btn-info" data-calendar="insert" href="#" data-data="{}">Add to Calendar</a>
    </div>
    <x-slot name="didOpen">
        modules.orientation.index.swalUpsertCalendar('form#upsertCalendar',data);
    </x-slot>
</x-swal.upsert>