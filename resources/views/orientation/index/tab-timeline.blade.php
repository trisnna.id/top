<div class="card-header border-0 pt-6">
    <h3 class="card-title align-items-start flex-column"></h3>
    <div class="card-toolbar">
        <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
            <button type="button" class="btn btn-light-primary me-2" onclick='$(".accordion-header").addClass("collapsed");$(".collapse").removeClass("show");' data-kt-menu-dismiss="true"><strong>Summary View</strong></button>
            <button type="button" class="btn btn-light-primary me-2" onclick='$(".accordion-header").removeClass("collapsed");$(".collapse").addClass("show");' data-kt-menu-dismiss="true"><strong>Detailed View</strong></button>
        </div>
    </div>
</div>
<div class="card-body py-4">
    {!! $timelineDt->html()->table() !!}
    @push('script')
        dtx.initOption('timelineDt');
        {!! $timelineDt->html()->generateScripts() !!}
    @endpush
</div>