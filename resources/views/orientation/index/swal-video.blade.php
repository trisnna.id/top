<x-swal.upsert id="video" :swalSettings="[]">
    <div class="row mb-7 m-0">
        <label class="fw-semibold">Name</label>
        <span class="fs-6" data-model="name"></span>
    </div>
    <div id="videoContainer"></div>
    <div class="row mb-7 m-0">
        <label class="fw-semibold">Attachment(s)</label>
        <input id="orientation_id" type="hidden" name="orientation_id">
        {!! $orientationRecordingDt->html()->table() !!}
    </div>
    <x-slot name="didOpen">
        modules.orientation.index.swalVideo('form#video',data);
        dtx.initOption('orientationRecordingDt');
        {!! $orientationRecordingDt->html()->generateScripts() !!}
    </x-slot>
</x-swal.upsert>
