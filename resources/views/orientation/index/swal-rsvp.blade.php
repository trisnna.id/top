<x-swal.upsert :id="'rsvp'" :swalSettings="[]">
    @method('PUT')
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">RSVP</span>
        </label>
        {{ Form::select('is_go', ['' => '', 0 => 'No', 1 => 'Yes'], null, ['required' => 'required', 'id' => 'is_go', 'class' => 'form-select swal2-select2', 'data-placeholder' => '-- Please Select --']) }}
    </div>
    <x-slot name="didOpen">
        modules.orientation.index.swalRsvp('form#rsvp',data);
    </x-slot>
</x-swal.upsert>
