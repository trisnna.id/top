<div class="card-body">
    <div class="row g-5 g-xl-9 mb-xl-9 mb-5">
        <div class="col-md-4">
            <div id='orientationCalendarMonth' class="fc-toolbar-bootstrap fc-month-mini mb-6"></div>
            <h3 class="align-items-start flex-column">
                <span class="card-label fw-bold text-dark">Today's Activities</span><br />
                <span class="fw-semibold fs-6 mt-1 text-gray-400">Do not miss your activity & attendance</span>
            </h3>
            <div class="accordion accordion-icon-toggle" id="kt_accordion_2">
                <div class="mb-5">
                    <div class="accordion-header collapsed bg-{{ getConstant('IconConstant')['compulsory_core']['color']['class'] }} rounded p-3 py-3 text-center"
                        data-bs-toggle="collapse" data-bs-target="#kt_accordion_2_item_2">
                        <div class="d-flex justify-content-center">
                            <h3 class="fs-4 ms-4 fw-bold mb-0 text-black">
                                Core Activities
                                <x-tooltip.basic title='tooltip-timetable-calendar-core-activities'></x-tooltip.basic>
                            </h3>
                            @if ($count['compulsoryCoreActivity'] > 0)
                                <span
                                    class="badge badge-light-info fw-bold fs-9 ms-1 px-2 py-1">{{ $count['compulsoryCoreActivity'] }}</span>
                            @endif
                        </div>
                    </div>
                    <div id="kt_accordion_2_item_2" class="fs-6 collapse mt-5" data-bs-parent="#kt_accordion_2">
                        {!! $compulsoryCoreDt->html()->table() !!}
                        @push('script')
                            {!! $compulsoryCoreDt->html()->generateScripts() !!}
                        @endpush
                    </div>
                </div>
                <div class="mb-5">
                    <div class="accordion-header collapsed bg-{{ getConstant('IconConstant')['complementary']['color']['class'] }} rounded p-3 py-3 text-center"
                        data-bs-toggle="collapse" data-bs-target="#kt_accordion_2_item_3">
                        <div class="d-flex justify-content-center">
                            <h3 class="fs-4 fw-bold ms-4 mb-0 text-black">
                                Complementary Activities
                                <x-tooltip.basic title='tooltip-timetable-calendar-complementary-activities'>
                                </x-tooltip.basic>
                            </h3>
                            @if ($count['complementaryActivity'] > 0)
                                <span
                                    class="badge badge-light-info fw-bold fs-9 ms-1 px-2 py-1">{{ $count['complementaryActivity'] }}</span>
                            @endif
                        </div>
                    </div>
                    <div id="kt_accordion_2_item_3" class="fs-6 collapse mt-5" data-bs-parent="#kt_accordion_2">
                        {!! $complementaryDt->html()->table() !!}
                        @push('script')
                            {!! $complementaryDt->html()->generateScripts() !!}
                        @endpush
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="d-flex justify-content-end mb-6" data-kt-user-table-toolbar="base">
                <x-modules.orientation.index.calendar.filter id="orientationCalendar" />
                @if (config('services.google.calendar.enabled'))
                    <button type="button" class="btn btn-light-primary upsert-swal-popup"
                        data-swal="{{ json_encode([
                            'id' => 'upsertCalendar',
                            'settings' => [
                                'title' => 'Are you sure to add selected records to Calendar?',
                                'icon' => 'warning',
                                'showCancelButton' => false,
                                'showConfirmButton' => false,
                            ],
                        ]) }}"
                        data-data="{{ json_encode([
                            'student_id' => $student->uuid,
                            'calendar' => 'orientationCalendar',
                        ]) }}">
                        <strong>Add to Calendar</strong>
                    </button>
                    @include('orientation.index.swal-upsert-calendar')
                @endif
            </div>
            <div class="m-0">
                @include('orientation.index.swal-orientation')
                @include('orientation.index.swal-video')
                @include('orientation.index.swal-attendance')
                @include('orientation.index.swal-rate')
                @include('orientation.index.swal-rsvp')
                <div id='orientationCalendar' class="fc-toolbar-bootstrap"></div>
                @push('script')
                    modules.orientation.index.calendar("orientationCalendar","{{ $student->uuid }}");
                @endpush
                @push('styles')
                    {{-- <style>
                        .fc-scroller {
                            overflow-y: hidden !important;
                        }
                    </style> --}}
                    <style>
                        #orientationCalendar {
                            /* {{-- make overflow y increasing it's parent element height --}} */
                            height: 100%;
                        }
                    </style>
                @endpush
            </div>
        </div>
    </div>
</div>
