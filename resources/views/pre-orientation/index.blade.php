<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-white fw-bold fs-3 flex-column justify-content-center my-0">
                    {{ $title }}
                </h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-white text-hover-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('dashboard.index') }}" class="text-white text-hover-white">Dashboard</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center gap-2 gap-lg-3">
            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="row">
            <div class="col-md-3">
                <div class="card shadow-sm mb-5">
                    <div class="card-body">
                        <x-completion-progress :value="__('50')" :title="__('pre-orientation progress')" />
                    </div>
                </div>
                @include('pre-orientation.includes.milestones', [
                    'checkpoints' => ['done', 'done', 'ongoing', 'todo', 'locked', 'locked'],
                ])
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="d-flex align-items-center position-relative my-1">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                            <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
                                        rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                    <path
                                        d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                        fill="currentColor" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                            <input type="text" data-kt-user-table-filter="search"
                                class="form-control form-control-solid w-250px ps-14"
                                placeholder="{{ __('Search pre-orientation') }}" />
                        </div>
                        <div class="card-toolbar">
                            <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                <button type="button" class="btn btn-light-danger me-3" data-kt-menu-trigger="click"
                                    data-kt-menu-placement="bottom-end">
                                    <span class="svg-icon svg-icon-2">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                                fill="currentColor" />
                                        </svg>
                                    </span>
                                    Filter
                                </button>
                                <div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true">
                                    <div class="px-7 py-5">
                                        <div class="fs-5 text-dark fw-bold">Filter Options</div>
                                    </div>
                                    <div class="separator border-gray-200"></div>
                                    <div class="px-7 py-5">
                                        <div class="row mb-5">
                                            <div class="col-md-12 fv-row fv-plugins-icon-container">
                                                <label class="form-label fw-semibold">Checkpoints:</label>
                                                <select name="filterCheckpoint" class="form-select form-select2"
                                                    id="filterCheckpoint" data-hide-search="true" multiple
                                                    data-allow-clear="true" data-placeholder="-- No checkpoints choosen --">
                                                    <option></option>
                                                    @foreach (['done', 'done', 'ongoing', 'todo', 'locked', 'locked'] as $i => $checkpoint)
                                                        <option value="{{ $loop->iteration }}" {{ in_array($checkpoint, ['todo', 'locked']) ? 'disabled' : '' }}>{{ __("Checkpoint {$loop->iteration}") }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row mb-5">
                                            <x-filter-status :options="['active', 'inactive']" />
                                        </div>
                                        <div class="row">
                                            <div class="d-flex justify-content-end">
                                                <button id="resetFilter" type="reset"
                                                    class="btn btn-sm btn-light btn-active-light-danger me-2"
                                                    data-kt-menu-dismiss="true">Reset</button>
                                                <button id="applyFilter" type="button" class="btn btn-sm btn-danger"
                                                    data-kt-menu-dismiss="true">Apply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <x-datatables.card.buttons.export color="danger" /> --}}
                            </div>
                        </div>
                    </div>
                </div>
                {!! $preOrientationDt->html()->table(['class' => 'thead-none w-100 tbody-row tr-col-md-4 td-card td-card-bordered tr-mb-3']) !!}
                @push('script')
                    {!! $preOrientationDt->html()->generateScripts() !!}
                @endpush
            </div>
        </div>
    </div>

    @push('scripts')
        <script src="{{ asset(mix('js/modules/preorientation.min.js')) }}"></script>
    @endpush
</x-layouts.app>
