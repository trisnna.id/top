<form method="post" action="{{ $route }}" class="form theme-form" id="preOrientationForm" autocomplete="off"
    enctype="multipart/form-data">
    @csrf
    @method($method)
    <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            <span class="required">Activity Name</span>
        </label>
        <input class="form-control {{ $errors->has('activity_name') ? 'is-invalid' : '' }}" id="activityName"
            type="text" name="activity_name" placeholder="This field is required"
            autocomplete="{{ optional($preOrientation)->activity_name }}"
            value="{{ old('activity_name', optional($preOrientation)->activity_name) }}">
        <x-inputs.error :name="__('activity_name')" />
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <div class="mb-0">
                    <label class="form-label" for="effective_date">Effective Date</label>
                    <div class="input-group" id="effectiveDateGroup">
                        <input class="form-control {{ $errors->has('effective_date') ? 'is-invalid' : '' }}"
                            id="effectiveDate" name="effective_date"
                            data-old="{{ old('effective_date', optional($preOrientation)->effective_date) }}">
                        <span class="input-group-text">
                            <i class="fas fa-calendar"></i>
                        </span>
                        <x-inputs.error :name="__('effective_date')" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <label class="fs-5 fw-bold form-label mb-2">
                    <span class="required">Activity Checkpoint</span>
                </label>
                <select class="form-select pre-orientation-select2 {{ $errors->has('category') ? 'is-invalid' : '' }}"
                    id="category" name="category"
                    data-old="{{ old('category', optional($preOrientation)->checkpoint_id) }}">
                    <option></option>
                    @foreach ($checkpoints as $checkpoint)
                        <option value="{{ $checkpoint->id }}">{{ $checkpoint->label }}</option>
                    @endforeach
                </select>
                <x-inputs.error :name="__('category')" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <label class="fs-5 fw-bold form-label mb-2">
                    <span class="required">Intake</span>
                </label>
                @php
                    $originalIntake = optional(optional($preOrientation)->intakes)['original'];
                @endphp
                <select
                    class="form-select pre-orientation-select2 {{ $errors->has('intake') ? 'is-invalid' : '' }} all-support"
                    name="intake[]" id="intake" multiple="multiple"
                    data-old="{{ is_array(old('intake', $originalIntake)) ? implode(',', old('intake', $originalIntake)) : old('intake', $originalIntake) }}">
                    <option value="all">{{ __('All') }}</option>
                    @foreach ($intakes as $intake)
                        <option value="{{ $intake->id }}">{{ $intake->name }}
                        </option>
                    @endforeach
                </select>
                <x-inputs.error :name="__('intake')" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <label class="fs-5 fw-bold form-label mb-2">
                    <span class="required">Faculty</span>
                </label>
                @php
                    $originalFaculty = optional(optional($preOrientation)->faculties)['original'];
                @endphp
                <select
                    class="form-select pre-orientation-select2 {{ $errors->has('faculty') ? 'is-invalid' : '' }} all-support"
                    name="faculty[]" id="faculty" multiple="multiple"
                    data-old="{{ is_array(old('faculty', $originalFaculty)) ? implode(',', old('faculty', $originalFaculty)) : old('faculty', $originalFaculty) }}">
                    <option value="all">{{ __('All') }}</option>
                    @foreach ($faculties as $faculty)
                        <option value="{{ $faculty->id }}">{{ $faculty->name }}
                        </option>
                    @endforeach
                </select>
                <x-inputs.error :name="__('faculty')" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <label class="fs-5 fw-bold form-label mb-2">
                    <span class="required">School</span>
                </label>
                @php
                    $originalSchool = optional(optional($preOrientation)->schools)['original'];
                @endphp
                <select
                    class="form-select pre-orientation-select2 {{ $errors->has('school') ? 'is-invalid' : '' }} all-support"
                    name="school[]" id="school" multiple="multiple"
                    data-old="{{ is_array(old('school', $originalSchool)) ? implode(',', old('school', $originalSchool)) : old('school', $originalSchool) }}">
                    <option value="all">{{ __('All') }}</option>
                    @foreach ($schools as $school)
                        <option value="{{ $school->id }}">{{ $school->name }}
                        </option>
                    @endforeach
                </select>
                <x-inputs.error :name="__('school')" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <label class="fs-5 fw-bold form-label mb-2">
                    <span class="required">Programme</span>
                </label>
                @php
                    $originalProgramme = optional(optional($preOrientation)->programmes)['original'];
                @endphp
                <select
                    class="form-select pre-orientation-select2 {{ $errors->has('programme') ? 'is-invalid' : '' }} all-support"
                    name="programme[]" id="programme" multiple="multiple"
                    data-old="{{ is_array(old('programme', $originalProgramme)) ? implode(',', old('programme', $originalProgramme)) : old('programme', $originalProgramme) }}">
                    <option value="all">{{ __('All') }}</option>
                    @foreach ($programmes as $programme)
                        <option value="{{ $programme->id }}">{{ $programme->name }}
                        </option>
                    @endforeach
                </select>
                <x-inputs.error :name="__('programme')" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <label class="fs-5 fw-bold form-label mb-2">
                    <span class="required">Mobility</span>
                </label>
                @php
                    $originalMobility = optional(optional($preOrientation)->mobilities)['original'];
                @endphp
                <select
                    class="form-select pre-orientation-select2 {{ $errors->has('mobility') ? 'is-invalid' : '' }} all-support"
                    name="mobility[]" id="mobility" multiple="multiple"
                    data-old="{{ is_array(old('mobility', $originalMobility)) ? implode(',', old('mobility', $originalMobility)) : old('mobility', $originalMobility) }}">
                    <option value="all">{{ __('All') }}</option>
                    @foreach ($mobilities as $mobility)
                        <option value="{{ $mobility->id }}">{{ $mobility->name }}
                        </option>
                    @endforeach
                </select>
                <x-inputs.error :name="__('mobility')" />
            </div>
        </div>
        <div class="col-md-6">
            <x-inputs.nationality :current="optional($preOrientation)->localities" :moduleName="'pre-orientation'" :containerClassName="__('d-flex flex-column mb-5 fv-row fv-plugins-icon-container')" />
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <div class="mb-0">
                    <label class="fs-5 fw-bold form-label mb-2">
                        <span class="required">Thumbnail</span>
                    </label>
                    <div class="fv-row col-12 mb-5">
                        <div class="image-input image-input-outline image-placeholder" data-kt-image-input="true"
                            style="background-image: url({{ url('https://dummyimage.com/600x400/000/fff') }})">
                            <div class="image-input-wrapper w-300px h-200px image-placeholder"
                                style="background-image: url({{ !empty($preOrientation ?? null) && $preOrientation->thumbnail != null ? url($preOrientation->thumbnail) : url('https://dummyimage.com/600x400/000/fff') }})">
                            </div>
                            <label
                                class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                data-kt-image-input-action="change" data-bs-toggle="tooltip" data-bs-dismiss="click"
                                title="Change Thumbnail">
                                <i class="bi bi-pencil-fill fs-7"></i>
                                <input class="" type="file" name="thumbnail"
                                    accept="{{ settingExtensionFormat('jpg jpeg png webp') }}" />
                                <input class="" type="hidden" name="logo_remove" />
                                <input type="hidden" name="thumbnail_hex">
                            </label>
                            <span
                                class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                data-kt-image-input-action="cancel" data-bs-toggle="tooltip" data-bs-dismiss="click"
                                title="Cancel Thumbnail">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                                data-kt-image-input-action="remove" data-bs-toggle="tooltip"
                                title="Remove Thumbnail">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                        </div>
                        <div class="form-text text-black">Allowed file types:
                            {{ settingExtensionFormat('jpg jpeg png webp', '') }}. Max size:
                            {{ settingExtensionFormat('2', '') }} MB</div>
                        <x-inputs.error :name="__('thumbnail_hex')" />
                        <div class="modal fade" tabindex="-1" id="cropper-modal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Crop Thumbnail</h5>
                                    </div>
                                    <div class="modal-body" id="cropper-content">
                                        <img id="cropper-image" style="max-width: 100%; display: block;"
                                            width="100%">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" id="cropper-save">Save
                                            changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            <span class="required">Content</span>
        </label>
        <textarea class="form-control tox-target {{ $errors->has('content') ? 'is-invalid' : '' }}" id="contentEditor"
            name="content" placeholder="This field is required">{!! old('content', optional($preOrientation)->content) !!}</textarea>
        <x-inputs.error :name="__('content')" />
    </div>
    <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
        <label class="fs-5 fw-bold form-label {{ $errors->has('quiz') ? 'is-invalid' : '' }} mb-2">
            <span id="inputQuiz" class="required"
                data-old="{{ old('quiz', optional($preOrientation)->content) }}">Quiz</span>
        </label>
        <div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" id="quiz" name="quiz" type="radio" value="1" @checked((int) old('quiz') === 1)>
                <label class="form-check-label" for="quiz">Yes</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" id="quiz" name="quiz" type="radio" value="0" @checked((int) old('quiz') === 0)>
                <label class="form-check-label" for="quiz">No</label>
            </div>
        </div>
        <x-inputs.error :name="__('quiz')" />
    </div>
    <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
        @include('admin.preorientation.mcq')
    </div>
</form>
