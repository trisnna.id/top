<div class="btn-group" role="group">
    <a class="btn btn-icon btn-info btn-sm" href="{{ route('admin.preorientation.show', ['uuid' => $uuid]) }}">
        <i class="fas fa-eye"></i>
    </a>
    <a data-key="edit-preorientation" href="{{ route('admin.preorientation.edit', ['uuid' => $uuid]) }}"
        class="btn btn-icon btn-black btn-sm">
        <i class="fas fa-edit"></i>
    </a>
    <a class="btn btn-icon btn-red btn-sm" href="{{ route('admin.preorientation.delete', ['uuid' => $uuid]) }}"
        data-method="delete" data-token="{{ csrf_token() }}" {{ $isUnpublished || $isPublished ? 'hidden' : '' }}>
        <i class="fas fa-trash"></i>
    </a>
    <a data-cloned="{{ $uuid }}" data-route="{{ route('api.preorientation.clone') }}" data-token="{{ csrf_token() }}"  href="#clone" title="Clone" class="btn btn-icon btn-sanguine-brown btn-sm clone-preor">
        <i class="fas fa-copy"></i>
    </a>
    <a class="btn btn-icon btn-success btn-sm" href="#publish" data-route="{{ route('api.preorientation.publish') }}"
        data-token="{{ csrf_token() }}" data-target="{{ $uuid }}" {{ $isPublished ? 'hidden' : '' }}>
        <i class="fas fa-cloud-upload"></i>
    </a>
    <a class="btn btn-icon btn-poppy btn-sm" href="#unpublish" data-route="{{ route('api.preorientation.unpublish') }}"
        data-token="{{ csrf_token() }}" data-target="{{ $uuid }}"
        {{ $isUnpublished || !$isPublished ? 'hidden' : '' }}>
        <i class="fas fa-cloud-download"></i>
    </a>
</div>
