<div id="quiz_stepper_container" style="display: none;" data-title="{{ $title }}"
    data-has-quiz="{{ $mcq->count() < 1 ? 'false' : 'true' }}">
    <div id="quiz_stepper" class="stepper stepper-links d-flex flex-column">
        <div class="stepper-nav my-5" style="display: none;">
            @for ($i = 1; $i <= $mcq->count(); $i++)
                <div class="stepper-item current" data-kt-stepper-element="nav">
                    <h3 class="stepper-title">{{ sprintf('Question %s', $i) }}</h3>
                </div>
            @endfor
        </div>
        <form id="quiz_form" class="w-100 mx-auto" novalidate="novalidate">
            @foreach ($mcq as $i => $quiz)
                <div class="flex-column {{ $i <= 0 ? 'current' : '' }}" data-kt-stepper-element="content">
                    <div class="fv-row fs-5 fw-semibold mb-2 text-gray-600">
                        <p class="mb-8">
                            {{ $quiz->question }}
                        </p>
                    </div>
                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <div class="rounded border p-10">
                            <div class="row">
                                @foreach ($quiz->shuffledOptions as $option)
                                    <div class="col-md-6">
                                        <div class="mb-5">
                                            <div class="form-check">
                                                <input
                                                    class="form-check-input {{ $option->isCorrect() ? 'is-valid' : '' }}"
                                                    type="radio" value="" id="flexCheckDefault1" name="radio2">
                                                <label class="form-check-label" for="flexCheckDefault1">
                                                    {{ $option->getTitleKey() }} . {{ $option->value }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="d-flex flex-stack">
                <div class="mr-2">
                    <button type="button" class="btn btn-lg btn-light-primary me-3" data-kt-stepper-action="previous">
                        <span class="svg-icon svg-icon-4 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="6" y="11" width="13" height="2"
                                    rx="1" fill="currentColor" />
                                <path
                                    d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z"
                                    fill="currentColor" />
                            </svg>
                        </span>
                        Previous
                    </button>
                </div>
                <div>
                    {{-- <button type="button" class="btn btn-lg btn-primary me-3" data-precallfn="dataFormBuilder"
                        data-kt-stepper-action="submit">
                        <span class="indicator-label">
                            Submit
                            <span class="svg-icon svg-icon-3 ms-2 me-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="18" y="13" width="13" height="2"
                                        rx="1" transform="rotate(-180 18 13)" fill="currentColor" />
                                    <path
                                        d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
                                        fill="currentColor" />
                                </svg>
                            </span>
                        </span>
                    </button> --}}
                    <button type="button" class="btn btn-lg btn-primary me-3" data-precallfn="dataFormBuilder"
                        data-kt-stepper-action="submit" onclick="window.location.reload()">
                        <span class="indicator-label">
                            Close
                            <span class="svg-icon svg-icon-3 ms-2 me-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                    fill="currentColor" class="bi bi-x" viewBox="0 0 16 16">
                                    <path
                                        d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                                </svg>
                            </span>
                        </span>
                    </button>
                    <button type="button" class="btn btn-lg btn-primary" data-kt-stepper-action="next">
                        Next
                        <span class="svg-icon svg-icon-4 ms-1 me-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none">
                                <rect opacity="0.5" x="18" y="13" width="13" height="2"
                                    rx="1" transform="rotate(-180 18 13)" fill="currentColor" />
                                <path
                                    d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
                                    fill="currentColor" />
                            </svg>
                        </span>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
