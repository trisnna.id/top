<x-layouts.app :title="__('TOP Management')" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    @push('styles')
        <style>
            .is-invalid .select2-selection,
            .needs-validation~span>.select2-dropdown {
                border-color: red !important;
            }
        </style>
    @endpush
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="__('description.title.preor')" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl has-notif"
        data-message="{{ session('message') }}">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card body-->
            <div class="card-header border-0 pt-6">
                <!--begin::Card title-->
                <div class="card-title">
                    {{-- Bulk action --}}
                    <div class="me-2">
                        <button class="btn btn-icon btn-black" data-kt-menu-trigger="click"
                            data-kt-menu-placement="bottom-end">
                            <i class="fa-solid fa-bars fs-3"></i>
                        </button>
                        <div class="menu menu-sub fs-6 menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3"
                            data-kt-menu="true" style="">
                            <div class="menu-item px-3">
                                <div class="menu-content text-muted fs-7 text-uppercase px-3 pb-2">Bulk Action</div>
                            </div>
                            <div class="menu-item px-3">
                                <a href="#bulkPublish" class="menu-link px-3" data-token="{{ csrf_token() }}"
                                    data-route="{{ route('api.preorientation.bulk.publish') }}">
                                    Publish Selected
                                </a>
                            </div>
                            <div class="menu-item px-3">
                                <a href="#bulkUnpublish" class="menu-link px-3" data-token="{{ csrf_token() }}"
                                    data-route="{{ route('api.preorientation.bulk.unpublish') }}">
                                    Unpublish Selected
                                </a>
                            </div>
                            <div class="menu-item px-3">
                                <a href="#resetCheckbox" class="menu-link px-3">Reset Selected</a>
                            </div>
                        </div>
                    </div>


                    <!--begin::Search-->
                    <div class="d-flex align-items-center position-relative my-1">
                        <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                        <span class="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
                                    rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                <path
                                    d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                    fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <input type="text" data-kt-user-table-filter="search" name="search"
                            class="form-control form-control-solid w-250px ps-14"
                            placeholder="Search pre-orientation activity" />
                    </div>
                </div>
                <!--begin::Card title-->
                <!--begin::Card toolbar-->
                <div class="card-toolbar">
                    <!--begin::Toolbar-->
                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                        <!--begin::Filter-->
                        <button type="button" class="btn btn-light-primary me-3" data-kt-menu-trigger="click"
                            data-kt-menu-placement="bottom-end">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                        fill="currentColor" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->Filter
                        </button>
                        <div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true">
                            <div class="px-7 py-5">
                                <div class="fs-5 text-dark fw-bold">Filter Options</div>
                            </div>
                            <div class="separator border-gray-200"></div>
                            <div class="px-7 py-5">
                                <div class="row mb-5">
                                    <x-inputs.filters.nationality></x-inputs.filters.nationality>
                                </div>
                                <div class="row mb-5">
                                    <x-inputs.filters.checkpoint></x-inputs.filters.checkpoint>
                                </div>
                                <div class="row mb-5">
                                    <x-inputs.filters.announcement-status></x-inputs.filters.announcement-status>
                                </div>
                                <div class="row">
                                    <div class="d-flex justify-content-end">
                                        <button type="reset"
                                            class="btn btn-sm btn-light btn-active-light-primary me-2" id="resetFilter"
                                            data-kt-menu-dismiss="true">Reset</button>
                                        <button type="submit" class="btn btn-sm btn-primary" id="applyFilter"
                                            data-kt-menu-dismiss="true">Apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <x-datatables.card.buttons.export :color="__('info')" /> --}}
                        <div>
                            <x-datatables.export id="preorientationTable" colorClass="primary" />
                        </div>
                        <a href="{{ route('admin.preorientation.create') }}" id="createPreorientation"
                            class="btn btn-sm fw-bold btn-info d-flex align-items-center">
                            <i class="fas fa-plus fa-sm"></i>
                            Add Pre-Orientation Activity
                        </a>
                    </div>
                </div>
                <!--end::Card toolbar-->
            </div>
            <div class="card-body py-4">
                @csrf
                <x-datatables.card.buttons.legend :gap="50" :exclude="['banned', 'submit']" />

                {{-- Bulk --}}
                {{-- <div class="buttons d-flex justify-content-center gap-3 text-white" id="bulkButtons">
                    <a href="#bulkPublish" class="btn btn-sm fw-bold btn-success" data-token="{{ csrf_token() }}"
                        data-route="{{ route('api.preorientation.bulk.publish') }}">
                        Publish Selected
                    </a>
                    <a href="#bulkUnpublish" class="btn btn-sm fw-bold btn-info" style="background: #ffa500 !important;"
                        data-token="{{ csrf_token() }}" data-route="{{ route('api.preorientation.bulk.unpublish') }}">
                        Unpublish Selected
                    </a>
                </div> --}}

                {{ $dataTable->table([
                    'class' =>
                        'table table-bordered table-striped align-middle table-row-dashed fs-6 gy-5 th-text-uppercase th-fw-bold w-100',
                ]) }}
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>
    @push('scripts')
        {{ $dataTable->scripts() }}
        <script src="{{ asset(mix('js/modules/preorientation.min.js')) }}"></script>
    @endpush
</x-layouts.app>
