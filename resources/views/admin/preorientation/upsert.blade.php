<x-layouts.app :title="__('TOP Management')" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    @push('styles')
        <style>
            .is-invalid .select2-selection,
            .needs-validation~span>.select2-dropdown {
                border-color: red !important;
            }
        </style>
    @endpush
    <x-slot name="toolbar">
        <x-share.breadcrumb
            :title="__('description.action.create', ['x' => __('description.title.preor')])"
            :links="[['url' => route('admin.preorientation.index'), 'text' => __('description.title.preor')]]"
        />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ $title }}</h3>
                        <div class="card-toolbar">
                            <a href="{{ route('admin.preorientation.index') }}" class="btn btn-red btn-sm">
                                <i class="fas fa-arrow-left me-2"></i>{{ __('description.labels.back_to_list', ['x' => __('description.title.preor')]) }}
                            </a>
                        </div>
                    </div>
                    <!--end::Card body-->
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body py-4">
                        @include('admin.preorientation.form')
                    </div>
                    <div class="card-footer">
                        <div class="d-flex flex-end">
                            <div class="d-flex align-items-center gap-2 gap-lg-3" data-select2-id="select2-data-123-l8qd">    
                                <button type="submit" role="button" class="btn btn-info btn-sm hover-elevate-up">
                                    <i class="bi bi-save fs-4 me-2"></i>
                                    Save
                                </button>
                                <button type="reset" role="button" class="btn btn-secondary btn-sm hover-elevate-down">
                                    <i class="bi bi-bootstrap-reboot" fs-4 me-2"></i>
                                    Reset
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Card-->
    </div>
    @push('scripts')
        <script src="{{ asset(mix('js/modules/preorientation.min.js')) }}" defer></script>
    @endpush
</x-layouts.app>
