<x-layouts.app :title="__('TOP Management')" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    @push('styles')
        <style>
            .is-invalid .select2-selection,
            .needs-validation~span>.select2-dropdown {
                border-color: red !important;
            }
        </style>
    @endpush
    <x-slot name="toolbar">
        <x-share.breadcrumb
            :title="$title"
            :links="[['url' => route('admin.preorientation.index'), 'text' => __('description.title.preor')]]"
        />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl has-notif"
        data-message="{{ session('message') }}">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <!--begin::Card body-->
                    <div class="card-header border-0 pt-6">
                        <!--begin::Card title-->
                        <div class="card-title">
                            <h3>{{ $preOrientation->activity_name }}</h3>
                            <div class="d-flex align-items-center gap-lg-3 gap-2">

                            </div>
                            <!--end::Search-->
                        </div>
                        <!--begin::Card title-->
                        <!--begin::Card toolbar-->
                        <div class="card-toolbar">
                            <div class="buttons">
                                <a href="{{ route('admin.preorientation.index') }}" class="btn btn-red btn-sm">
                                    <i class="fas fa-arrow-left me-2"></i>{{ __('description.labels.back_to_list', ['x' => __('description.title.preor')]) }}
                                </a>
                                @if ($preOrientation->students()->count() == 0)
                                    <a href="{{ route('admin.preorientation.edit', ['uuid' => $preOrientation->uuid]) }}"
                                        class="btn btn-warning btn-sm">
                                        <i class="bi bi-pencil-square fs-4 me-2"></i>
                                        Edit
                                    </a>
                                @endif
                                <a href="#viewQuiz" class="btn btn-success btn-sm hover-scale">
                                    <i class="bi bi-list-task fs-4 me-2"></i>
                                    View Quiz
                                </a>
                            </div>
                        </div>
                        <!--end::Card toolbar-->
                    </div>
                    <div class="card-body py-4">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="">
                                    <h3 class="text-dark">Thumbnail</h3>
                                    <div class="separator separator-dashed mb-9"></div>
                                    <img src="{{ asset($preOrientation->thumbnail) }}" class="img-fluid rounded"
                                        alt="">
                                </div>
                            </div>
                            <div class="col">
                                <div class="">
                                    <h3 class="text-dark">Description</h3>
                                    <div class="separator separator-dashed mb-9"></div>
                                    <div id="quizContent">
                                        {!! $preOrientation->content !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if ($preOrientation->content_links->count() > 0)
                        <div class="card-footer">
                            <!--begin::Section Video-->
                            <div class="mb-17">
                                <!--begin::Content-->
                                <div class="d-flex flex-stack mb-5">
                                    <!--begin::Title-->
                                    <h3 class="text-dark">Media</h3>
                                    <!--end::Title-->
                                </div>
                                <!--end::Content-->

                                <!--begin::Separator-->
                                <div class="separator separator-dashed mb-9"></div>
                                <!--end::Separator-->

                                <!--begin::Row-->
                                <div class="row g-10">
                                    @forelse ($preOrientation->content_links as $contentLink)
                                        <div
                                            class="{{ sprintf('col-md-%s', round(12 / $preOrientation->content_links->count())) }}">
                                            <div class="card-xl-stretch me-md-6">
                                                <a class="video-thumbnail d-block bgi-no-repeat bgi-size-cover bgi-position-center card-rounded position-relative min-h-175px mb-5"
                                                    style="background-image:url('{{ $contentLink->thumbnail }}')"
                                                    data-fslightbox="lightbox-video-tutorial"
                                                    href="{{ $contentLink->link }}">

                                                    <img src="{{ asset('media/svg/misc/video-play.svg') }}"
                                                        class="position-absolute top-50 start-50 translate-middle"
                                                        alt="">
                                                </a>
                                                <div class="m-0">
                                                    <a href="/metronic8/demo1/../demo1/pages/user-profile/overview.html"
                                                        class="fs-4 text-dark fw-bold text-hover-primary text-dark lh-base">
                                                    </a>
                                                    <div class="fw-semibold fs-5 text-dark my-4 text-gray-600">
                                                        {!! $contentLink->text !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                        <div>
                                            <p class="text-muted">There are no media's.</p>
                                        </div>
                                    @endforelse
                                </div>
                                <!--end::Row-->
                            </div>
                            <!--end::Section Video-->
                        </div>
                        <!--end::Card body-->
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('admin.preorientation.quiz', [
                    'mcq' => $preOrientationQuizses,
                    'title' => $preOrientation->activity_name,
                ])
            </div>
        </div>
    </div>
    @push('scripts')
        <script src="{{ asset(mix('js/modules/preorientation.min.js')) }}"></script>
    @endpush
</x-layouts.app>
