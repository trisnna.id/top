<div id="mcqContainer">
    <input id="MCQ" type="text" value="{{ json_encode(optional($preOrientation)->quizs) }}" hidden>
    <label class="fs-5 fw-bold form-label mb-2">
        <span class="required">Pre-Orientation MCQ</span>
    </label>
    <div id="mcqWrapper">
        <div class="mb-5 hover-scroll-x">
            <div class="d-grid">
                <ul id="tabControl" class="nav nav-tabs flex-nowrap text-nowrap">
                    <li id="addQuestionContainer" class="nav-item">
                        <a id="addQuestion"
                            class="nav-link btn btn-active-light btn-color-gray-800 btn-active-color-danger rounded-bottom-0"
                            data-bs-toggle="tab" href="#">
                            <span class="svg-icon svg-icon-muted svg-icon-2x"><svg width="24" height="24"
                                    viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <rect opacity="0.3" x="2" y="2" width="20" height="20"
                                        rx="5" fill="currentColor" />
                                    <rect x="10.8891" y="17.8033" width="12" height="2" rx="1"
                                        transform="rotate(-90 10.8891 17.8033)" fill="currentColor" />
                                    <rect x="6.01041" y="10.9247" width="12" height="2" rx="1"
                                        fill="currentColor" />
                                </svg>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="tab-content" id="mcqTabContent">
        </div>

        {{-- handled by JS @see public/js/modules/preorientation.js --}}
    </div>
</div>
