<div class="card">
    <div class="card-body">
        <div class="d-flex flex-stack mb-5 flex-wrap">
            <div class="d-flex flex-row">
                <div class="me-2">
                    <button class="btn btn-icon btn-black" data-kt-menu-trigger="click"
                        data-kt-menu-placement="bottom-end">
                        <i class="fa-solid fa-bars fs-3"></i>
                    </button>
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3"
                        data-kt-menu="true" style="">
                        <div class="menu-item px-3">
                            <div class="menu-content text-muted fs-7 text-uppercase px-3 pb-2">Bulk Action</div>
                        </div>
                        <div class="menu-item px-3">
                            <a href="#" class="menu-link upsert-swal-popup px-3"
                                data-swal="{{ json_encode([
                                    'id' => 'bulkActiveFaq',
                                    'axios' => [
                                        'url' => route('admin.faqs.update-bulk-active'),
                                        'method' => 'post',
                                    ],
                                ]) }}">Active
                                Selected</a>
                            @include('admin.resource.index.swal-bulk-active-faq')
                        </div>
                        <div class="menu-item px-3">
                            <a href="#" class="menu-link upsert-swal-popup px-3"
                                data-swal="{{ json_encode([
                                    'id' => 'bulkInactiveFaq',
                                    'axios' => [
                                        'url' => route('admin.faqs.update-bulk-inactive'),
                                        'method' => 'post',
                                    ],
                                ]) }}">Inactive
                                Selected</a>
                            @include('admin.resource.index.swal-bulk-inactive-faq')
                        </div>
                        <div class="menu-item px-3">
                            <a href="#" class="menu-link checkbox-reset px-3" data-table="faqDt">Reset
                                Selected</a>
                        </div>
                    </div>
                </div>
                <x-datatables.search id="faqDt" placeholder="Search FAQ" />
            </div>
            <div class="d-flex justify-content-end">
                <button type="button" class="btn fw-bold btn-info upsert-swal-popup"
                    data-swal="{{ json_encode([
                        'id' => 'upsertFaq',
                        'axios' => [
                            'url' => route('admin.faqs.store'),
                            'method' => 'post',
                        ],
                    ]) }}">
                    <i class="fas fa-plus fa-sm"></i>
                    {{ __('Add FAQ') }}
                </button>
                @include('admin.resource.index.swal-upsert-faq')
            </div>
        </div>
        <x-datatables.card.buttons.legend :gap="50" :include="['edit', 'delete', 'clone']" />
        {!! $faqDt->html()->table() !!}
        @push('script')
            dtx.initOption('faqDt');
            {!! $faqDt->generateMinifyScripts() !!}
        @endpush
    </div>
</div>
