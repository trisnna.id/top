<x-swal.upsert :id="'upsertFaq'" :swalSettings="[
    'title' => 'FAQ',
    'customClass' => [
        'popup' => 'w-md-500px',
    ],
]">
    @method('PUT')
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Question</span>
        </label>
        <input required type="text" class="form-control" name="question" id="question" placeholder="Question">
    </div>
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Answer</span>
        </label>
        <textarea required class="form-control" name="answer" id="answer" placeholder="Answer"></textarea>
    </div>

    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Intake</span>
        </label>
        <input type="hidden" name="filter_all[intake]" value="0">
        {{ Form::select('filters[intake][]', [(count($options['student_intake_editable']) > 0 ? 'All' : 'Please check under settings') => $options['student_intake_editable']], null, [
            'id' => 'filter_intake',
            'multiple',
            'required',
            'class' => 'form-select swal2-select2-multiple-optgroup',
            'data-total' => $count = count($options['student_intake_editable']),
            'data-placeholder' => $count > 0 ? '-- Please Select --' : '-- Nothing to Select --',
            'data-input-all' => 'intake',
        ]) }}
    </div>

    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Institution</span>
        </label>
        <input type="hidden" name="filter_all[campus]" value="0">
        {{ Form::select('filters[campus][]', [(count($options['student_campus_editable']) > 0 ? 'All' : 'Please check under settings') => $options['student_campus_editable']], null, [
            'id' => 'filter_campus',
            'multiple',
            'required',
            'class' => 'form-select swal2-select2-multiple-optgroup',
            'data-total' => $count = count($options['student_campus_editable']),
            'data-placeholder' => $count > 0 ? '-- Please Select --' : '-- Nothing to Select --',
            'data-input-all' => 'campus',
        ]) }}
    </div>
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Programme</span>
        </label>
        <input type="hidden" name="filter_all[programme]" value="0">
        {{ Form::select('filters[programme][]', [(count($options['student_programme_editable']) > 0 ? 'All' : 'Please check under settings') => $options['student_programme_editable']], null, [
            'id' => 'filter_programme',
            'multiple',
            'required',
            'class' => 'form-select swal2-select2-multiple-optgroup',
            'data-total' =>  $count = count($options['student_programme_editable']),
            'data-placeholder' => $count > 0 ? '-- Please Select --' : '-- Nothing to Select --',
            'data-input-all' => 'programme',
        ]) }}
    </div>
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
            <input id="is_active" class="form-check-input" type="checkbox" value="true" name="is_active" checked>
            <span class="fw-bold form-check-label">
                Make it Active?
            </span>
        </label>
    </div>
    <x-slot name="didOpen">
        modules.resource.admin.index.swalUpsertFaq("form#upsertFaq",data);
    </x-slot>
</x-swal.upsert>
