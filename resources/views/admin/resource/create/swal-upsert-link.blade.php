<x-swal.upsert :id="'upsertLink'" :swalSettings="[
    'title' => 'Link',
]">
    @method('PUT')
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span>Text to Display</span>
        </label>
        <input type="text" class="form-control" name="name" id="name" placeholder="Text to Display">
    </div>
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">URL</span>
        </label>
        <textarea class="form-control" name="url" id="url" placeholder="URL"></textarea>
    </div>
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Description</span>
        </label>
        <textarea class="form-control" name="description" id="description" placeholder="Description"></textarea>
    </div>
    <x-slot name="didOpen">
        modules.resource.admin.create.swalUpsertLink('form#upsertLink',data);
    </x-slot>
</x-swal.upsert>
