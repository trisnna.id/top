<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        <span class="required">Title</span>
    </label>
    <input type="text" class="form-control" name="title" id="title" placeholder="Title"
        value="{{ $resource->title ?? null }}">
</div>
<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        <span class="required">Description</span>
    </label>
    <textarea name="description" id="description" class="form-control" placeholder="Description">{!! !empty($resource->description) ? json_decode($resource->description_encode) : null !!}</textarea>
</div>
<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        <span class="required">Thumbnail</span>
    </label>
    <div class="fv-row col-12 mb-5">
        <div class="image-input image-input-outline image-placeholder" data-kt-image-input="true"
            style="background-image: url({{ url('https://dummyimage.com/600x400/000/fff') }})">
            <div class="image-input-wrapper w-300px h-200px image-placeholder"
                style="background-image: url('{{ !empty($resource ?? null) ? $resource->getFirstMediaUrl('thumbnail') : url(setting('file.thumbnail_fallback_url')) }}')">
            </div>
            <label class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="change" data-bs-toggle="tooltip" data-bs-dismiss="click"
                title="Change Thumbnail">
                <i class="bi bi-pencil-fill fs-7"></i>
                <input class="" type="file" name="thumbnail"
                    accept="{{ settingExtensionFormat('jpg jpeg png webp') }}" />
                <input class="" type="hidden" name="logo_remove" />
                <input type="hidden" name="thumbnail_hex">
            </label>
            <span class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="cancel" data-bs-toggle="tooltip" data-bs-dismiss="click"
                title="Cancel Thumbnail">
                <i class="bi bi-x fs-2"></i>
            </span>
            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove Thumbnail">
                <i class="bi bi-x fs-2"></i>
            </span>
        </div>
        <div class="form-text text-black">Allowed file types:
            {{ settingExtensionFormat('jpg jpeg png webp', '') }}. Max size:
            {{ settingExtensionFormat('2', '') }} MB</div>
        <div class="modal fade" tabindex="-1" id="cropper-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Crop Thumbnail</h5>
                    </div>
                    <div class="modal-body" id="cropper-content">
                        <img id="cropper-image" style="max-width: 100%; display: block;" width="100%">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="cropper-save">Save
                            changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        <span class="required">Attachments</span>
    </label>
    <select name="attachment" class="form-select form-select2" id="attachment" data-hide-search="true"
        data-allow-clear="true" data-placeholder="-- Please Select --" multiple="multiple">
        <option></option>
        <option data-accept="{{ settingExtensionFormat(setting('file.arrival_resource_extension')) }}"
            data-accept-info="{{ settingExtensionFormat(setting('file.arrival_resource_extension'), '') }}"
            data-size="{{ settingExtensionFormat(setting('file.arrival_resource_size'), '') }}" value="files">Files
        </option>
        <option value="links">Links</option>
    </select>
</div>
<div id="attachmentContainer"></div>
@if (!empty($attachmentDt ?? null))
    @include('admin.resource.create.swal-upsert-link')
    @include('admin.resource.create.swal-upsert-file')
    {!! $attachmentDt->html()->table() !!}
    @push('script')
        {!! $attachmentDt->generateMinifyScripts() !!}
    @endpush
@endif
<div class="d-flex flex-column fv-row mb-5">
    <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
        {{ Form::checkbox('is_active', null, $resource->is_active ?? true, ['id' => 'is_active', 'class' => 'form-check-input']) }}
        <span class="fw-bold form-check-label">
            Make it Active?
        </span>
    </label>
</div>


@push('scripts')
    <script src="{{ asset(mix('js/modules/resource.min.js')) }}"></script>
@endpush
