<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-white fw-bold fs-3 flex-column justify-content-center my-0">
                    {{ $title }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-white text-hover-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('dashboard.index') }}" class="text-white text-hover-white">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ $routeIndex }}" class="text-white text-hover-white">{{$titleIndex}}</a>
                    </li>
                </ul>
            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card py-4">
            <div class="card-header">
                <h3 class="card-title">{{$title}}</h3>
                <div class="card-toolbar">
                    <a href="{{$routeIndex}}" class="btn btn-red btn-sm">
                        <i class="fas fa-arrow-left me-2"></i>Back to {{$titleIndex}} List
                    </a>
                </div>
            </div>
            <form id="upsert" class="form card-body">
                @method($instance['upsert']['swal']['axios']['method'])
                @include('admin.resource.create.form-resource')
            </form>
            <div class="card-footer d-flex flex-stack py-6 px-9">
                @if(!empty($resource ?? null))
                    <a href="{{$routeShow}}" class="btn btn-info px-6 me-2">
                        <i class="fas fa-eye"></i>Preview
                    </a>
                @else
                    <div></div>
                @endif
                <button class="btn btn-dark px-6 upsert-swal-inline"
                    data-swal="{{ json_encode($instance['upsert']['swal']) }}">
                    <i class="fas fa-save"></i>Save Changes
                </button>
            </div>
        </div>
    </div>
    @push('script')
        modules.resource.admin.create.swalUpsert('form#upsert');
    @endpush
</x-layouts.app>
