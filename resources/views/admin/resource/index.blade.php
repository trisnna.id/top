<x-layouts.app :title="__('FAQ')" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="__('FAQ')" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        @include('admin.resource.index.tab-faq')
    </div>
</x-layouts.app>
