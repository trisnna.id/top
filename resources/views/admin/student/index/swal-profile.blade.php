<x-swal.upsert id="profile" :swalSettings="['customClass' => [
    'popup' => 'w-md-50 w-lg-40',
]]">
    @include('profile.index.form-student',['inputDisabled'=>true])
    <x-slot name="didOpen">
        modules.student.index.swalProfile('form#profile',data);
    </x-slot>
</x-swal.upsert>
