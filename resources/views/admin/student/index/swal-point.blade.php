<x-swal.upsert id="point" :swalSettings="['customClass' => [
    'popup' => 'container',
]]">
    <div id="points-chart" class="w-100 overflow-auto" style="height: 350px;"></div>
    <x-slot name="didOpen">
        modules.student.index.swalPoint('form#point',data);
    </x-slot>
</x-swal.upsert>