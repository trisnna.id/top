$("form#{{$id}} [data-model='name']").html(data.name);
$("form#{{$id}} [data-model='id_number']").html(data.id_number);
$("form#{{$id}} [data-model='faculty_name']").html(data.faculty_name);
$("form#{{$id}} [data-model='intake_number']").html(data.intake_number);
$("form#{{$id}} [data-model='taylors_email']").html(data.taylors_email);
$("form#{{$id}} [data-model='personal_email']").html(data.personal_email);