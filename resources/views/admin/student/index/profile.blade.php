<div class="row mb-7 m-0">
    <label class="fw-semibold">Name</label>
    <span class="fs-6" data-model="name"></span>
</div>
<div class="row mb-7 m-0">
    <label class="fw-semibold">Student ID</label>
    <span class="fs-6" data-model="id_number"></span>
</div>
<div class="row mb-7 m-0">
    <label class="fw-semibold">School</label>
    <span class="fs-6" data-model="faculty_name"></span>
</div>
<div class="row mb-7 m-0">
    <label class="fw-semibold">Programme</label>
    <span class="fs-6" data-model="programme"></span>
</div>
<div class="row mb-7 m-0">
    <label class="fw-semibold">Intake</label>
    <span class="fs-6" data-model="intake_number"></span>
</div>
<div class="row mb-7 m-0">
    <label class="fw-semibold">Student Level</label>
    <span class="fs-6" data-model="level_of_study"></span>
</div>
<div class="row mb-7 m-0">
    <label class="fw-semibold">Email (Taylor’s email)</label>
    <span class="fs-6" data-model="taylors_email"></span>
</div>
<div class="row mb-7 m-0">
    <label class="fw-semibold">Email (Personal email)</label>
    <span class="fs-6" data-model="personal_email"></span>
</div>
<div class="row mb-7 m-0">
    <label class="fw-semibold">Institution</label>
    <span class="fs-6" data-model="tu_tc"></span>
</div>
<div class="row mb-7 m-0">
    <label class="fw-semibold">Nationality</label>
    <span class="fs-6" data-model="nationality"></span>
</div>
<div class="row mb-7 m-0">
    <label class="fw-semibold">Mobility</label>
    <span class="fs-6" data-model="mobility"></span>
</div>