<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover mb-5"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="$title" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            @include('admin.student.index.swal-profile')
            @include('admin.student.index.swal-point')
            <div class="card-body">
                <div class="d-flex flex-stack mb-5 flex-wrap">
                    <x-datatables.search id="studentDt" placeholder="Search Student" />
                    <div class="d-flex justify-content-end">
                        <x-modules.admin.student.index.filter id="studentDt" />
                        <div>
                            <x-datatables.export id="studentDt" />
                        </div>
                    </div>
                </div>
                <div class="w-md-80" style="margin: 0 auto;">
                    <div class="d-flex justify-content-around mb-3 rounded border border-gray-200 p-3 pt-6">
                        <div class="d-flex flex-column flex-center w-150px">
                            <span class="badge badge-info">
                                <i class="fas fa-eye fs-6"></i>
                            </span>
                            <div class="fw-semibold py-2">Detail</div>
                        </div>
                        <div class="d-flex flex-column flex-center w-150px">
                            <span class="badge badge-primary">
                                <i class="fa-solid fa-calendar fs-6"></i>
                            </span>
                            <div class="fw-semibold py-2">Orientation Activity</div>
                        </div>
                        <div class="d-flex flex-column flex-center w-150px">
                            <span class="badge badge-warning">
                                <i class="fas fa-star fs-6"></i>
                            </span>
                            <div class="fw-semibold py-2">Activity Point</div>
                        </div>
                    </div>
                </div>
                <div id="studentDtScrollbarX" class="table-scrollbar-x" data-parentEl="#studentDt_wrapper .table-responsive">
                    <div class="scrollbar-x">&nbsp;</div>
                </div>
                {!! $studentDt->html()->table() !!}
                <div id="studentDtScrollbarXEnd"></div>
                @push('script')
                    dtx.initOption('studentDt');{!! $studentDt->generateMinifyScripts() !!}
                @endpush
            </div>
        </div>
    </div>
</x-layouts.app>
