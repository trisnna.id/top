<x-layouts.app :title="__('TOP Management')" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    @push('styles')
        <style>
            .is-invalid .select2-selection,
            .needs-validation~span>.select2-dropdown {
                border-color: red !important;
            }
        </style>
    @endpush
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="__($title)" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl has-notif"
        data-message="{{ session('message') }}">
        <!--begin::Card-->
        <div class="card">
            <!--begin::Card body-->
            <div class="card-header border-0 pt-6">
                <!--begin::Card title-->
                <div class="card-title">
                    {{-- Bulk action --}}

                    <!--begin::Search-->
                    <div class="d-flex align-items-center position-relative my-1">
                        <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                        <span class="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
                                    rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                <path
                                    d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                    fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <input type="text" data-kt-user-table-filter="search" name="search"
                            class="form-control form-control-solid w-250px ps-14" placeholder="Search student" />
                    </div>
                </div>
                <!--begin::Card title-->
                <!--begin::Card toolbar-->
                <div class="card-toolbar">
                    <!--begin::Toolbar-->
                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                        <button type="button" class="btn btn-light-primary me-3" data-bs-toggle="modal"
                            data-bs-target="#studentPointsFilter">
                            <span class="svg-icon svg-icon-2">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                                        fill="currentColor" />
                                </svg>
                            </span>
                            Filter
                        </button>
                        <div class="modal fade" id="studentPointsFilter" data-bs-backdrop="static">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Filter Options</h5>
                                        <button type="button" class="btn close p-0" data-bs-dismiss="modal"
                                            aria-label="Close"><i class="fas fa-times"></i></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row mb-5">
                                            <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                                                <label class="form-label fw-semibold">Faculties:</label>
                                                {!! Form::select('filter_faculty', $options['faculty'], null, [
                                                    'id' => "filter_faculty",
                                                    'multiple',
                                                    'class' => 'form-select form-select2 reset',
                                                    'data-hide-search' => 'true',
                                                    'data-allow-clear' => 'true',
                                                    'data-placeholder' => '-- Please Select / All --',
                                                ]) !!}
                                            </div>
                                            <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                                                <label class="form-label fw-semibold">School:</label>
                                                {!! Form::select('filter_school', $options['school'], null, [
                                                    'id' => "filter_school",
                                                    'multiple',
                                                    'class' => 'form-select form-select2 reset',
                                                    'data-hide-search' => 'true',
                                                    'data-allow-clear' => 'true',
                                                    'data-placeholder' => '-- Please Select / All --',
                                                ]) !!}
                                            </div>
                                            <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                                                <label class="form-label fw-semibold">Programme:</label>
                                                {!! Form::select('filter_programme', $options['programme'], null, [
                                                    'id' => "filter_programme",
                                                    'multiple',
                                                    'class' => 'form-select form-select2 reset',
                                                    'data-hide-search' => 'true',
                                                    'data-allow-clear' => 'true',
                                                    'data-placeholder' => '-- Please Select / All --',
                                                ]) !!}
                                            </div>
                                            <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                                                <label class="form-label fw-semibold">Intake:</label>
                                                {!! Form::select('filter_intake', $options['intake'], null, [
                                                    'id' => "filter_intake",
                                                    'multiple',
                                                    'class' => 'form-select form-select2 reset',
                                                    'data-hide-search' => 'true',
                                                    'data-allow-clear' => 'true',
                                                    'data-placeholder' => '-- Please Select / All --',
                                                ]) !!}
                                            </div>
                                            <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                                                <label class="form-label fw-semibold">Level of Study:</label>
                                                {!! Form::select('filter_level_study', $options['level_study'], null, [
                                                    'id' => "filter_level_study",
                                                    'multiple',
                                                    'class' => 'form-select form-select2 reset',
                                                    'data-hide-search' => 'true',
                                                    'data-allow-clear' => 'true',
                                                    'data-placeholder' => '-- Please Select / All --',
                                                ]) !!}
                                            </div>
                                            <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                                                <label class="form-label fw-semibold">Institution:</label>
                                                {!! Form::select('filter_campus', ['' => ''] + $options['campus']->toArray(), null, [
                                                    'id' => "filter_campus",
                                                    'class' => 'form-select form-select2 reset',
                                                    'data-hide-search' => 'true',
                                                    'data-allow-clear' => 'true',
                                                    'data-placeholder' => '-- Please Select / All --',
                                                ]) !!}
                                            </div>
                                            <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                                                <label class="form-label fw-semibold">Mobility:</label>
                                                {!! Form::select('filter_mobility', $options['mobility'], null, [
                                                    'id' => "filter_mobility",
                                                    'multiple',
                                                    'class' => 'form-select form-select2 reset',
                                                    'data-hide-search' => 'true',
                                                    'data-allow-clear' => 'true',
                                                    'data-placeholder' => '-- Please Select / All --',
                                                ]) !!}
                                            </div>
                                            <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                                                <label class="form-label fw-semibold">Locality:</label>
                                                {!! Form::select('filter_locality', ['' => ''] + $options['locality']->toArray(), null, [
                                                    'id' => "filter_locality",
                                                    'class' => 'form-select form-select2 reset',
                                                    'data-hide-search' => 'true',
                                                    'data-allow-clear' => 'true',
                                                    'data-placeholder' => '-- Please Select / All --',
                                                ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-light-primary font-weight-bold"
                                            id="resetFilter">Reset</button>
                                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal"
                                            id="applyFilter">Apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <x-datatables.export id="studentPoints" colorClass="primary" />
                        </div>
                    </div>
                </div>
                <!--end::Card toolbar-->
            </div>
            <div class="card-body py-4">
                <div id="studentPointsScrollbarX" class="table-scrollbar-x" data-parentEl="#studentPoints_wrapper .table-responsive">
                    <div class="scrollbar-x">&nbsp;</div>
                </div>
                {{ $dataTable->table([
                    'class' =>
                        'table table-bordered table-striped align-middle table-row-dashed fs-6 gy-5 th-text-uppercase th-fw-bold w-100',
                ]) }}
                <div id="studentPointsScrollbarXEnd"></div>
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>
    @push('scripts')
        {{ $dataTable->scripts() }}
        <script src="{{ asset(mix('js/modules/studentpoint.min.js')) }}"></script>
    @endpush
</x-layouts.app>
