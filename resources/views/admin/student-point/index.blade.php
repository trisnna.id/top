<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover mb-5"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="$title" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-stack mb-5 flex-wrap">
                    <x-datatables.search id="studentPointDt" placeholder="Search Student Point" />
                    <div class="d-flex justify-content-end">
                        <x-modules.admin.student.index.filter id="studentPointDt" />
                        <div>
                            <x-datatables.export id="studentPointDt" />
                        </div>
                    </div>
                </div>
                <div id="studentPointDtScrollbarX" class="table-scrollbar-x" data-parentEl="#studentPointDt_wrapper .table-responsive">
                    <div class="scrollbar-x">&nbsp;</div>
                </div>
                {!! $studentPointDt->html()->table() !!}
                <div id="studentPointDtScrollbarXEnd"></div>
                @push('script')
                    dtx.initOption('studentPointDt');{!! $studentPointDt->generateMinifyScripts() !!}
                @endpush
            </div>
        </div>
    </div>
</x-layouts.app>
