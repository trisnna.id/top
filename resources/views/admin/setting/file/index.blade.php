<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <div class="card">
        <form id="upsert" class="card-body">
            @include('admin.setting.file.index.form-file')
        </form>
        <div class="card-footer d-flex justify-content-end">
            <button type="button" class="btn btn-primary upsert-swal-inline"
                data-swal="{{ json_encode([
                    'id' => 'upsert',
                    'axios' => [
                        'method' => 'post',
                        'url' => route('admin.settings.files.store'),
                    ],
                ]) }}">
                Save Changes
            </button>
        </div>
    </div>
</x-setting-layout>