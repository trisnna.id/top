<h3 class="text-uppercase">Arrival & Resource File Attachments</h3>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>File Size (MB)</span>
    </label>
    <input type="number" min="1" name="file[arrival_resource_size]" class="form-control" placeholder="File Size (MB)"
        value="{{ setting('file.arrival_resource_size') }}" />
</div>
<div class="d-flex flex-column mb-10 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>File Extension</span>
    </label>
    <input type="text" name="file[arrival_resource_extension]" class="form-control"
        placeholder="{!! __('text-helper-file-extension') !!}"
        value="{{ setting('file.arrival_resource_extension') }}" />
    <div class="form-text text-black">{!! __('text-helper-file-extension') !!}</div>
</div>
<h3 class="text-uppercase">Orientation File Attachments</h3>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>File Size (MB)</span>
    </label>
    <input type="number" min="1" name="file[orientation_attachment_size]" class="form-control"
        placeholder="File Size (MB)" value="{{ setting('file.orientation_attachment_size') }}" />
</div>
<div class="d-flex flex-column mb-10 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>File Extension</span>
    </label>
    <input type="text" name="file[orientation_attachment_extension]" class="form-control"
        placeholder="{!! __('text-helper-file-extension') !!}"
        value="{{ setting('file.orientation_attachment_extension') }}" />
        <div class="form-text text-black">{!! __('text-helper-file-extension') !!}</div>
</div>
<h3 class="text-uppercase">Orientation File Recording & Attachments</h3>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>File Size (MB)</span>
    </label>
    <input type="number" min="1" name="file[orientation_recording_size]" class="form-control"
        placeholder="File Size (MB)" value="{{ setting('file.orientation_recording_size') }}" />
</div>
<div class="d-flex flex-column mb-10 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>File Extension</span>
    </label>
    <input type="text" name="file[orientation_recording_extension]" class="form-control"
        placeholder="{!! __('text-helper-file-extension') !!}"
        value="{{ setting('file.orientation_recording_extension') }}" />
        <div class="form-text text-black">{!! __('text-helper-file-extension') !!}</div>
</div>
<h3 class="text-uppercase">Profile Avatar</h3>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>File Size (MB)</span>
    </label>
    <input type="number" min="1" name="file[profile_avatar_size]" class="form-control"
        placeholder="File Size (MB)" value="{{ setting('file.profile_avatar_size') }}" />
</div>
<div class="d-flex flex-column fv-row">
    <label class="fw-bold form-label mb-2">
        <span>File Extension</span>
    </label>
    <input type="text" name="file[profile_avatar_extension]" class="form-control"
        placeholder="{!! __('text-helper-file-extension') !!}"
        value="{{ setting('file.profile_avatar_extension') }}" />
        <div class="form-text text-black">{!! __('text-helper-file-extension') !!}</div>
</div>
