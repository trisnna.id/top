<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <div class="card">
        <div class="card-body">
            @include('admin.setting.page.index.swal-upsert')
            {!! $pageDt->html()->table() !!}
            @push('script')
                dtx.initOption('pageDt');
                {!! $pageDt->generateMinifyScripts() !!}
            @endpush
        </div>
    </div>
</x-setting-layout>