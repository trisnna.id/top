<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <div class="card">
        <div class="card-header">
            <div class="py-5">
                <div class="d-flex align-items-center rounded py-5 px-5 bg-light-primary ">
                    <span class="svg-icon svg-icon-3x svg-icon-primary me-5"><svg width="24" height="24"
                            viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.3" x="2" y="2" width="20" height="20"
                                rx="10" fill="currentColor"></rect>
                            <rect x="11" y="14" width="7" height="2" rx="1"
                                transform="rotate(-90 11 14)" fill="currentColor"></rect>
                            <rect x="11" y="17" width="2" height="2" rx="1"
                                transform="rotate(-90 11 17)" fill="currentColor"></rect>
                        </svg>
                    </span>
                    <div class="text-gray-700 fw-bold fs-6">
                        {!! __('note-settings-cms') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="d-flex flex-stack flex-wrap mb-5">
                <div class="d-flex flex-row">
                    <div class="me-2">
                        <button class="btn btn-icon btn-black" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                            <i class="fa-solid fa-bars fs-3"></i>
                        </button>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3" data-kt-menu="true" style="">
                            <div class="menu-item px-3">
                                <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Bulk Action</div>
                            </div>
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3 upsert-swal-popup" data-swal="{{ json_encode([
                                    'id' => 'bulkActive',
                                    'axios' => [
                                        'url' => route('admin.settings.programmes.update-bulk-active'),
                                        'method' => 'post',
                                    ],
                                ]) }}">Active Selected</a>
                                @include('admin.setting.programme.index.swal-bulk-active')
                            </div>
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3 upsert-swal-popup" data-swal="{{ json_encode([
                                    'id' => 'bulkInactive',
                                    'axios' => [
                                        'url' => route('admin.settings.programmes.update-bulk-inactive'),
                                        'method' => 'post',
                                    ],
                                ]) }}">Inactive Selected</a>
                                @include('admin.setting.programme.index.swal-bulk-inactive')
                            </div>
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3 checkbox-reset" data-table="programmeDt">Reset Selected</a>
                            </div>
                        </div>
                    </div>
                    <x-datatables.search id="programmeDt" placeholder="Search Programme" />
                </div>
            </div>
            {!! $programmeDt->html()->table() !!}
            @push('script')
                dtx.initOption('programmeDt');
                {!! $programmeDt->generateMinifyScripts() !!}
            @endpush
        </div>
    </div>
</x-setting-layout>