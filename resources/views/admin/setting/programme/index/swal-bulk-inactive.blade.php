<x-swal.upsert :id="'bulkInactive'" :swalSettings="[
    'title' => 'Are you sure to INACTIVE selected records?',
    'icon' => 'warning',
]">
    <input type="hidden" name="checkbox">
    <p class="text-center">
        You select&nbsp;<span id="selectedCount"></span>&nbsp;record(s).<br />
        You can change it back later!
    </p>
    <p class="text-justify">
        {!! __('note-swal-bulk-active') !!}
    </p>
    <x-slot name="didOpen">
        dtx.swalInputCheckbox('form#bulkInactive','programmeDt');
    </x-slot>
</x-swal.upsert>
