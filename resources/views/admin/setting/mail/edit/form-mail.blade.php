<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span class="required">Name</span>
    </label>
    <input type="text" class="form-control bs-maxlength" name="title" id="title" placeholder="Name"
        value="{{ $mail->title ?? null }}" maxlength="100" required>
</div>
<div class="d-flex flex-column mb-3 fv-row">
    <label class="fw-bold form-label">
        <span class="required">Subject</span>
    </label>
    <input id="subject" type="text" class="form-control bs-maxlength" name="subject"
        value="{{ $mail->subject ?? null }}" maxlength="100" required>
</div>
<div class="d-flex flex-column mb-3 fv-row">
    <label class="fw-bold form-label">
        <span class="required">Content</span>
    </label>
    <input id="content" type="hidden" name="content" value="{{ $mail->content ?? null }}">
    <textarea class="form-control" name="content_editor" id="content_editor">{!! json_decode($mail->content ?? null) ?? null !!}</textarea>
</div>
@push('script-ready')
    modules.setting.admin.mail.edit.upsert();
@endpush
