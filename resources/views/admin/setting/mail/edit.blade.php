<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <div class="d-flex justify-content-center">
        <ul class="nav nav-pills nav-pills-custom mb-3 d-flex flex-center">
            <li class="nav-item mb-3 me-3 me-lg-6">
                <a class="nav-link btn btn-outline btn-flex btn-color-muted btn-active-color-primary flex-column overflow-hidden w-150px h-85px pt-5 pb-2 bg-white active"
                    id="tab_link_1" data-bs-toggle="pill" href="#tab_1">
                    <img class="w-35px mb-1" src="{{ url('media/icons/taylors/email.png') }}">
                    <span class="nav-text text-gray-800 fw-bold fs-6 lh-1">Details</span>
                    <span class="bullet-custom position-absolute bottom-0 w-100 h-4px bg-red"></span>
                </a>
            </li>
            @if(!$mail->is_default)
                <li class="nav-item mb-3 me-3 me-lg-6">
                    <a class="nav-link btn btn-outline btn-flex btn-color-muted btn-active-color-primary flex-column overflow-hidden w-150px h-85px pt-5 pb-2 bg-white"
                        id="tab_link_2" data-bs-toggle="pill" href="#tab_2">
                        <img class="w-35px mb-1" src="{{ url('media/icons/taylors/orientation.png') }}">
                        <span class="nav-text text-gray-800 fw-bold fs-6 lh-1">Recipients</span>
                        <span class="bullet-custom position-absolute bottom-0 w-100 h-4px bg-red"></span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="tab_1">
            <div class="card mb-5">
                <form id="upsert" class="card-body">
                    @method('PUT')
                    @include('admin.setting.mail.edit.form-mail')
                </form>
                <div class="card-footer d-flex justify-content-between">
                    @include('admin.setting.mail.index.swal-show')
                    <button id="preview" type="button"
                        title="{{ $instance['show']['swal']['settings']['title'] ?? 'View Detail' }}"
                        class="btn btn-info upsert-swal-popup" data-data="{{ json_encode($instance['show']['data']) }}"
                        data-swal="{{ json_encode($instance['show']['swal']) }}">
                        Preview
                    </button>
                    <button type="button" class="btn btn-primary upsert-swal-inline"
                        data-swal="{{ json_encode([
                            'id' => 'upsert',
                            'axios' => [
                                'url' => route('admin.settings.mails.update', $mail->uuid),
                            ],
                        ]) }}">
                        Save Changes
                    </button>
                </div>
            </div>
        </div>
        <div class="tab-pane fade">
            
        </div>
    </div>
    
    
</x-setting-layout>
