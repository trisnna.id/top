<x-swal.upsert :id="'upsert'" :swalSettings="[
    'title' => 'Mail',
    'customClass' => [
        'popup' => 'container',
    ],
]">
    @method('PUT')
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Name</span>
        </label>
        <input type="text" class="form-control" name="title" id="title" placeholder="Name">
    </div>

    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Subject</span>
        </label>
        <textarea class="form-control" name="subject" id="subject" placeholder="Subject"></textarea>
    </div>
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Content</span>
        </label>
        <textarea class="form-control" name="content" id="content" placeholder="Content"></textarea>
    </div>
    <x-slot name="didOpen">
        modules.setting.admin.mail.index.swalUpsert('form#upsert',data);
    </x-slot>
</x-swal.upsert>
