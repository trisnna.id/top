<x-swal.upsert :id="'show'" :swalSettings="[
    'showConfirmButton' => false,
    'showCancelButton' => true,
    'cancelButtonText' => 'Close',
    'customClass'=>[
        'popup'=>'container-xxl'
    ]
]">
    <div id="show">
        <iframe id="container" class="w-100"></iframe>
    </div>
    <x-slot name="didOpen">
        modules.setting.admin.mail.index.swalShow('div#show',data);
    </x-slot>
</x-swal.upsert>
