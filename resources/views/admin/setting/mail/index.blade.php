<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <div class="card">
        <div class="card-body">
            {{-- <div class="d-flex flex-stack mb-5 flex-wrap">
                <x-datatables.search id="mailDt" placeholder="Search Email Management" />
                <div class="d-flex justify-content-end">
                    <a href="{{ route('admin.settings.mails.create') }}" class="btn fw-bold btn-info">
                        <i class="fas fa-plus fa-sm"></i>
                        Add Email
                    </a>
                </div>
            </div> --}}
            @include('admin.setting.mail.index.swal-show')
            {!! $mailDt->html()->table() !!}
            @push('script')
                dtx.initOption('mailDt');
                {!! $mailDt->generateMinifyScripts() !!}
            @endpush
        </div>
    </div>
</x-setting-layout>
