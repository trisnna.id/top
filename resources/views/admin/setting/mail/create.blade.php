<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <div class="card mb-5">
        <form id="upsert" class="card-body">
            @include('admin.setting.mail.edit.form-mail')
        </form>
        <div class="card-footer d-flex justify-content-end">
            <button type="button" class="btn btn-primary upsert-swal-inline"
                data-swal="{{ json_encode([
                    'id' => 'upsert',
                    'axios' => [
                        'url' => route('admin.settings.mails.store'),
                    ]
                ]) }}">
                Save Changes
            </button>
        </div>
    </div>
</x-setting-layout>