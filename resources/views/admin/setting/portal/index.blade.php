<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <div class="card">
        <form id="upsert" class="card-body">
            @include('admin.setting.portal.index.form-portal')
        </form>
        <div class="card-footer d-flex justify-content-end">
            <button type="button" class="btn btn-primary upsert-swal-inline"
                data-swal="{{ json_encode([
                    'id' => 'upsert',
                    'axios' => [
                        'method' => 'post',
                        'url' => route('admin.settings.portal.store'),
                    ],
                ]) }}">
                Save Changes
            </button>
        </div>
    </div>
</x-setting-layout>