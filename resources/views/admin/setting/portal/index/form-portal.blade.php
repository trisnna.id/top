<h3 class="text-uppercase">External Link</h3>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>Taylor’s Mobile App</span>
    </label>
    <input type="text" name="portal[link_mobile]" class="form-control" placeholder="Taylor’s Mobile App"
        value="{{ setting('portal.link_mobile') }}" />
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>TIMeS</span>
    </label>
    <input type="text" name="portal[link_times]" class="form-control" placeholder="TIMeS"
        value="{{ setting('portal.link_times') }}" />
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>Campus Central</span>
    </label>
    <input type="text" name="portal[link_campus]" class="form-control" placeholder="Campus Central"
        value="{{ setting('portal.link_campus') }}" />
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>Virtual Campus Tour Link</span>
    </label>
    <input type="text" name="portal[link_virtual]" class="form-control"
        placeholder="Virtual Campus Tour Link" value="{{ setting('portal.link_virtual') }}" />
</div>
<h3 class="text-uppercase">Media Social</h3>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span><img src="{{url('media/svg/social-logos/youtube.svg')}}" class="h-20px"> Youtube</span>
    </label>
    <input type="text" name="portal[sosmed_youtube]" class="form-control"
        placeholder="Youtube" value="{{ setting('portal.sosmed_youtube') }}" />
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span><img src="{{url('media/svg/social-logos/twitter.svg')}}" class="h-20px"> Twitter</span>
    </label>
    <input type="text" name="portal[sosmed_twitter]" class="form-control"
        placeholder="Twitter" value="{{ setting('portal.sosmed_twitter') }}" />
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span><img src="{{url('media/svg/social-logos/instagram.svg')}}" class="h-20px"> Instagram</span>
    </label>
    <input type="text" name="portal[sosmed_instagram]" class="form-control"
        placeholder="Instagram" value="{{ setting('portal.sosmed_instagram') }}" />
</div>
<div class="d-flex flex-column fv-row">
    <label class="fw-bold form-label mb-2">
        <span><img src="{{url('media/svg/social-logos/facebook.svg')}}" class="h-20px"> Facebook</span>
    </label>
    <input type="text" name="portal[sosmed_facebook]" class="form-control"
        placeholder="Facebook" value="{{ setting('portal.sosmed_facebook') }}" />
</div>