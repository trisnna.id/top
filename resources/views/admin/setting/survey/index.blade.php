<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <div class="card">
        <div class="card-body">
            <form id="upsert">
                @include('admin.setting.survey.index.form-survey')
            </form>
            <div class="d-none">
                <h3 class="text-uppercase">Upload Survey Response</h3>
                <div class="d-flex flex-stack mb-5 flex-wrap">
                    <div class="d-flex flex-row">

                    </div>
                    <div class="d-flex justify-content-end">
                        <button type="button" class="btn fw-bold btn-black upsert-swal-popup"
                            data-swal="{{ json_encode([
                                'id' => 'upload',
                                'axios' => [
                                    'url' => route('admin.settings.survey.upload'),
                                    'method' => 'post',
                                    'contentType' => 'application/x-www-form-urlencoded',
                                ],
                            ]) }}">
                            <i class="fas fa-plus fa-sm"></i>
                            {{ __('Upload') }}
                        </button>
                        @include('admin.setting.survey.index.swal-upload')
                    </div>
                </div>
                {!! $surveyMigrationDt->html()->table() !!}
                @push('script')
                    dtx.initOption('surveyMigrationDt');{!! $surveyMigrationDt->generateMinifyScripts() !!}
                @endpush
            </div>

        </div>
        <div class="card-footer d-flex justify-content-end">
            <button type="button" class="btn btn-primary upsert-swal-inline"
                data-swal="{{ json_encode([
                    'id' => 'upsert',
                    'axios' => [
                        'method' => 'post',
                        'url' => route('admin.settings.survey.store'),
                    ],
                ]) }}">
                Save Changes
            </button>
        </div>
    </div>
</x-setting-layout>
