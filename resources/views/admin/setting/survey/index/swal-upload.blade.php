<x-swal.upsert :id="'upload'" :swalSettings="[
    'title' => 'Upload',
]">
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Filename</span>
        </label>
        <input required type="file" class="form-control" name="file" id="file" accept="{{settingExtensionFormat(setting('survey.upload_extension'))}}">
        <div class="form-text text-black">Allowed file types: {{settingExtensionFormat(setting('survey.upload_extension'),'')}}. Max size: {{settingExtensionFormat(setting('survey.upload_size'),'')}} MB</div>
    </div>
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Type</span>
        </label>
        <select class="form-select swal2-select2" id="type_id" name="type_id"
            data-placeholder="-- Please Select --" required>
            <option></option>
            @foreach (getConstant('SurveyMigrationTypeConstant','title') as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
    </div>
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Field Mapping to Student ID</span>
        </label>
        <input required type="text" class="form-control" name="field_map_student_id" id="field_map_student_id" value="student_id">
    </div>

    <x-slot name="didOpen">
        {{-- modules.resource.admin.index.swalUpsertFaq("form#upsertFaq",data); --}}
    </x-slot>
</x-swal.upsert>
