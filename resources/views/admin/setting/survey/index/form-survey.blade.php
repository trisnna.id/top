<h3 class="text-uppercase">Pre Orientation Survey</h3>
<div class="d-flex flex-column fv-row d-none mb-5">
    <label class="fw-bold form-label mb-2">
        <span class="required">Link </span>
    </label>
    <textarea required name="survey[link_pre_orientation_survey]" class="form-control" placeholder="Link">{{ setting('survey.link_pre_orientation_survey') }}</textarea>
</div>
<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        <span class="required">Point </span>
    </label>
    <input required type="number" min="0" name="survey[point_pre_orientation_survey]" class="form-control"
        placeholder="Point" value="{{ setting('survey.point_pre_orientation_survey') }}" />
</div>
<h3 class="text-uppercase">Post Orientation Survey</h3>
<div class="d-flex flex-column fv-row d-none mb-5">
    <label class="fw-bold form-label mb-2">
        <span class="required">Link </span>
    </label>
    <textarea required name="survey[link_post_orientation_survey]" class="form-control" placeholder="Link">{{ setting('survey.link_post_orientation_survey') }}</textarea>
</div>
<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        <span class="required">Point </span>
    </label>
    <input required type="number" min="0" name="survey[point_post_orientation_survey]" class="form-control"
        placeholder="Point" value="{{ setting('survey.point_post_orientation_survey') }}" />
</div>
