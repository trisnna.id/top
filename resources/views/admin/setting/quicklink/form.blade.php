<h3 class="text-uppercase">Quick Link Content</h3>

{{-- Flame --}}
<div class="d-flex flex-column fv-row mb-10">
    <label class="fw-bold form-label mb-2">
        <span>Flame</span>
    </label>
    <textarea class="form-control tox-target editor" name="quicklink[flame]">{{ setting('quicklink.flame') }}</textarea>
</div>

{{-- Clubs & Societies --}}
<div class="d-flex flex-column fv-row mb-10">
    <label class="fw-bold form-label mb-2">
        <span>Clubs & Societies</span>
    </label>
    <textarea class="form-control tox-target editor" name="quicklink[clubs_n_societies]">{{ setting('quicklink.clubs_n_societies') }}</textarea>
</div>

{{-- Student ID --}}
<div class="d-flex flex-column fv-row mb-10">
    <label class="fw-bold form-label mb-2">
        <span>Student ID</span>
    </label>
    <textarea class="form-control tox-target editor" name="quicklink[student_id]">{{ setting('quicklink.student_id') }}</textarea>
</div>

{{-- Campus Central --}}
<div class="d-flex flex-column fv-row mb-10">
    <label class="fw-bold form-label mb-2">
        <span>Campus Central</span>
    </label>
    <textarea class="form-control tox-target editor" name="quicklink[campus_central]">{{ setting('quicklink.campus_central') }}</textarea>
</div>

{{-- International Students --}}
<div class="d-flex flex-column fv-row mb-10">
    <label class="fw-bold form-label mb-2">
        <span>International Students</span>
    </label>
    <textarea class="form-control tox-target editor" name="quicklink[international_students]">{{ setting('quicklink.international_students') }}</textarea>
</div>

{{-- Unigym --}}
<div class="d-flex flex-column fv-row mb-10">
    <label class="fw-bold form-label mb-2">
        <span>Unigym</span>
    </label>
    <textarea class="form-control tox-target editor" name="quicklink[unigym]">{{ setting('quicklink.unigym') }}</textarea>
</div>

{{-- Missed Orientation --}}
<div class="d-flex flex-column fv-row mb-10">
    <label class="fw-bold form-label mb-2">
        <span>Missed Orientation</span>
    </label>
    <textarea class="form-control tox-target editor" name="quicklink[missed_orientation]">{{ setting('quicklink.missed_orientation') }}</textarea>
</div>


@push('scripts')
    <script>
        tinymce.init({
            selector: ".editor",
            height: "350",
            plugins: "image lists link",
            // menubar: false,
            // toolbar:
            //     "fontselect | bold italic strikethrough underline | bullist numlist | alignleft aligncenter alignright | image link ",
            menubar: "file edit view insert format", // hanya menampilkan menu yang diinginkan
            menu: {
                format: {
                    title: "Format",
                    items: "bold italic underline strikethrough superscript subscript code | formats blockformats fontsizes align lineheight | removeformat",
                },
            },
            link_title: false,
            link_quicklink: false,
            target_list: false,
            file_picker_types: "image",
        });
    </script>

    <script>
        $('.upsert-swal-inline-tinymce').on('click', (e) => {
            const data = $(e.currentTarget).data('swal');

            tinymce.triggerSave();
            const formData = new FormData(document.getElementById(data.id));

            axios({
                method: data.axios.method,
                url: data.axios.url,
                data: formData,
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then((response) => {
                toastr.success(response.data.message);
            }).catch((error) => {
                toastr.error(error.response.data.message);
            })
        })
    </script>
@endpush
