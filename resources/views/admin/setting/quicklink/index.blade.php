<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <div class="card">
        <form id="upsert" class="card-body">
            @include('admin.setting.quicklink.form')
        </form>
        <div class="card-footer d-flex justify-content-end">
            <button type="button" class="btn btn-primary upsert-swal-inline-tinymce"
                data-swal="{{ json_encode([
                    'id' => 'upsert',
                    'axios' => [
                        'method' => 'post',
                        'url' => route('admin.settings.quicklink.store'),
                    ],
                ]) }}">
                Save Changes
            </button>
        </div>
    </div>
</x-setting-layout>
