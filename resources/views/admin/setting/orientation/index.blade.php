<x-setting-layout :searchPlaceholder="__('Search rules')" :addBtnTitle="__('Add Rule')">
    <div class="card mb-5">
        <form id="upsert" class="card-body">
            <h3 class="text-uppercase mb-5">Orientation</h3>
            <div class="d-flex flex-column mb-5 fv-row">
                <label class="fw-bold form-label mb-2">
                    <span class="required">Attendance Extension Time </span>
                </label>
                <input required type="number" min="1" name="orientation[extension_scan_attendance]" class="form-control" placeholder="Attendance Extension Time" value="{{setting('orientation.extension_scan_attendance')}}" />
                <span class="form-text text-black">The attendance will be close after "end + extension" time. (Minute)</span>
            </div>

            <div class="d-flex flex-column mb-5 fv-row">
                <label class="fw-bold form-label mb-2">
                    <span class="required">Point Attendance </span>
                </label>
                <input required type="number" min="0" name="orientation[point_attendance]" class="form-control" placeholder="Point Attendance" value="{{setting('orientation.point_attendance')}}" />
            </div>

            <div class="d-flex flex-column mb-5 fv-row">
                <label class="fw-bold form-label mb-2">
                    <span class="required">Point Rating </span>
                </label>
                <input required type="number" min="0" name="orientation[point_rating]" class="form-control" placeholder="Point Rating" value="{{setting('orientation.point_rating')}}" />
            </div>
        </form>
        <div class="card-footer d-flex justify-content-end">
            <button type="button" class="btn btn-primary upsert-swal-inline"
                data-swal="{{ json_encode([
                    'id' => 'upsert',
                    'axios' => [
                        'method' => 'post',
                        'url' => route('admin.settings.orientation.store'),
                    ],
                ]) }}">
                Save Changes
            </button>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h3 class="text-uppercase mb-5">Orientation Modules by Intake</h3>
            <div class="d-flex flex-stack flex-wrap mb-5">
                <div class="d-flex flex-row">
                    <x-datatables.search id="intakeDt" placeholder="Search Intake" />
                </div>
            </div>
            <x-swal.upsert :id="'upsertIntake'" :swalSettings="[
                'title' => 'Orientation Modules by Intake',
            ]">
                @method('PUT')
                @include('admin.setting.orientation.index.form', ['id' => 'upsertIntake'])
                <x-slot name="didOpen">
                    modules.setting.admin.orientation.index.upsertIntake('form#upsertIntake',data);
                </x-slot>
            </x-swal.upsert>
            {!! $intakeDt->html()->table() !!}
            @push('script')
                dtx.initOption('intakeDt');
                {!! $intakeDt->generateMinifyScripts() !!}
            @endpush
        </div>
    </div>
</x-setting-layout>