<div class="d-flex flex-column mb-5 fv-row">
    <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
        <input id="is_orientation_activities_active" class="form-check-input" type="checkbox" value="true" name="is_orientation_activities_active">
        <span class="fw-bold form-check-label">
            Student Orientation Activities modules
        </span>
    </label>
</div>
<div id="orientation_activities_message_container" style="display: none;">
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Student Orientation Activities message</span>
        </label>
        <textarea class="form-control" name="orientation_activities_message" id="orientation_activities_message" placeholder="Student Orientation Activities message"></textarea>
    </div>
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
        <input id="is_timetable_active" class="form-check-input" type="checkbox" value="true" name="is_timetable_active">
        <span class="fw-bold form-check-label">
            Student Timetable modules
        </span>
    </label>
</div>
<div id="timetable_message_container" style="display: none;">
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Student Timetable message</span>
        </label>
        <textarea class="form-control" name="timetable_message" id="timetable_message" placeholder="Student Timetable message"></textarea>
    </div>
</div>