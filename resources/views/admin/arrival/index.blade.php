<x-layouts.app :title="'Pre-Arrival'" toolbarClass="py-3 py-lg-6 cover mb-5"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="__('description.title.arrival')" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-stack flex-wrap mb-5">
                    <div class="d-flex flex-row">

                        <div class="me-2">
                            <button class="btn btn-icon btn-black" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                <i class="fa-solid fa-bars fs-3"></i>
                            </button>
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3"
                                data-kt-menu="true" style="">
                                <div class="menu-item px-3">
                                    <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Bulk Action</div>
                                </div>
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3 upsert-swal-popup"
                                        data-swal="{{ json_encode([
                                            'id' => 'bulkActive',
                                            'axios' => [
                                                'url' => route('admin.arrivals.update-bulk-active'),
                                                'method' => 'post',
                                            ],
                                        ]) }}">Active
                                        Selected</a>
                                    @include('admin.arrival.index.swal-bulk-active')
                                </div>
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3 upsert-swal-popup"
                                        data-swal="{{ json_encode([
                                            'id' => 'bulkInactive',
                                            'axios' => [
                                                'url' => route('admin.arrivals.update-bulk-inactive'),
                                                'method' => 'post',
                                            ],
                                        ]) }}">Inactive
                                        Selected</a>
                                        @include('admin.arrival.index.swal-bulk-inactive')
                                </div>
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3 checkbox-reset" data-table="faqDt">Reset Selected</a>
                                </div>
                            </div>
                        </div>
                        <x-datatables.search id="arrivalDt" placeholder="Search Pre-Arrival" />
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{ route('admin.arrivals.create') }}" type="button" class="btn btn-sm fw-bold btn-info d-flex align-items-center">
                            <i class="fas fa-plus fa-sm"></i>
                            {{ __('Add Pre-Arrival') }}
                        </a>
                    </div>
                </div>
                <x-datatables.card.buttons.legend :gap="50" :include="['show','edit','delete','clone']" />
                {!! $arrivalDt->html()->table() !!}
                @push('script')dtx.initOption('arrivalDt');{!! $arrivalDt->generateMinifyScripts() !!}@endpush
            </div>
        </div>
    </div>
</x-layouts.app>
