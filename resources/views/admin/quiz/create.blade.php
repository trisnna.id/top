<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-white fw-bold fs-3 flex-column justify-content-center my-0">
                    {{ $title }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-white text-hover-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('dashboard.index') }}" class="text-white text-hover-white">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-white">Mini Quiz Maker</li>
                </ul>
            </div>
            <div class="d-flex align-items-center gap-2 gap-lg-3">

            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <!--begin::Card-->
        <div class="card">
            <div class="card-body py-4">
                <div class="row mb-6">
                    <label class="col-lg-3 col-form-label required fw-bold fs-6">Title</label>
                    <div class="col-lg-9 fv-row">
                        <input id="title" type="text" name="title" class="form-control form-control-lg form-control-solid" value="{{$form->title ?? null}}"/>
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-3 col-form-label fw-bold fs-6">Intro/Description</label>
                    <div class="col-lg-9 fv-row">
                        <textarea id="title" name="description" class="form-control form-control-lg form-control-solid">{{$form->description ?? null}}</textarea>
                    </div>
                </div>
                <div class="w-100">
                    <input type="hidden" name="form">
                    <div id="editor"></div>
                    @push('style')
                        .form-wrap.form-builder .frmb .field-actions .btn {
                            padding: 0 6px !important;
                            border-radius: 0 !important;
                            border-color: #c5c5c5 !important;
                            border-width: inherit;
                            border-width: 0 0 1px 1px !important;
                        }
                        .form-wrap.form-builder .frmb {
                            min-height: 200px !important;
                        }
                    @endpush
                    @push('script')
                        $('#editor').formBuilder({
                            showActionButtons: false,
                            disabledAttrs: ["name", "className"],
                            disableFields: ['autocomplete',
                                'button',
                                'checkbox-group',
                                'date',
                                'file',
                                'header',
                                'hidden',
                                'number',
                                'paragraph',
                                'radio-group',
                                'starRating',
                                'text',
                                'textarea',
                            ]
                        });
                    @endpush
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end py-6 px-9">
                <button type="button" class="btn btn-primary">
                    Save
                </button>
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>
</x-layouts.app>
