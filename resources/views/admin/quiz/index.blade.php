<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-white fw-bold fs-3 flex-column justify-content-center my-0">
                    {{ $title }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-white text-hover-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('dashboard.index') }}" class="text-white text-hover-white">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-white">Mini Quiz Maker</li>
                </ul>
            </div>
            <div class="d-flex align-items-center gap-2 gap-lg-3">

            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <!--begin::Card-->
        <div class="card">
            <div class="card-header border-0 pt-6">
                <!--begin::Card title-->
                <div class="card-title">
                    <!--begin::Search-->
                    <div class="d-flex align-items-center position-relative my-1">
                        <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                        <span class="svg-icon svg-icon-1 position-absolute ms-6">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2"
                                    rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                <path
                                    d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z"
                                    fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <input type="text" data-kt-user-table-filter="search"
                            class="form-control form-control-solid w-250px ps-14" placeholder="Search" />
                    </div>
                    <div class="d-flex align-items-center gap-2 gap-lg-3">

                        {{-- <x-swal.upsert :id="'upsert'" :swalSettings="['title' => 'User']">
                            @method('PUT')
                            @include('admin.user.index.form-user', ['id' => 'upsert'])
                            <x-slot name="didOpen">
                                @include('admin.user.index.form-user-script', ['id' => 'upsert'])
                            </x-slot>
                        </x-swal.upsert> --}}

                    </div>
                    <!--end::Search-->
                </div>
                <!--begin::Card title-->
                <!--begin::Card toolbar-->
                <div class="card-toolbar">
                    <!--begin::Toolbar-->
                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                        <a href="{{route('admin.quizzes.create')}}" class="btn btn-sm fw-bold btn-info">
                            <i class="fas fa-plus fa-sm"></i>
                            Add Mini Quiz
                        </a>
                    </div>
                </div>
                <!--end::Card toolbar-->
            </div>
            <!--end::Card header-->
            <!--begin::Card body-->
            <div class="card-body py-4">
                <x-swal.upsert :id="'upsert'" :swalSettings="[
                    'title' => 'Mini Quiz',
                    'customClass' => [
                        'popup' => 'w-300px w-md-500px',
                    ],
                ]">
                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="fs-5 fw-bold form-label mb-2">
                            <span class="required">Title</span>
                        </label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                    </div>
                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="fs-5 fw-bold form-label mb-2">
                            <span class="required">Content</span>
                        </label>
                        <input id="content" type="hidden" name="content" value="">
                        <textarea class="form-control" name="content_editor" id="content_editor"></textarea>
                    </div>

                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="fs-5 fw-bold form-label mb-2">
                            <span class="required">Nationality</span>
                        </label>
                        <select name="nationality" class="form-select swal2-select2" id="nationality"
                            data-hide-search="true" data-allow-clear="true" data-placeholder="-- Please Select --"
                            multiple="multiple">
                            <option></option>
                            <option value="local">Local</option>
                            <option value="international">International</option>
                        </select>
                    </div>
                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="fs-5 fw-bold form-label mb-2">
                            <span class="required">Intake</span>
                        </label>
                        <select name="intake" class="form-select swal2-select2" id="intake" data-hide-search="true"
                            data-allow-clear="true" data-placeholder="-- Please Select --" multiple="multiple">
                            <option></option>
                            <option value="local">Local</option>
                            <option value="international">International</option>
                        </select>
                    </div>

                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="fs-5 fw-bold form-label mb-2">
                            <span class="required">Week</span>
                        </label>
                        <select class="form-select swal2-select2" name="week" id="week" data-hide-search="true"
                            data-allow-clear="true" data-placeholder="-- Please Select --" multiple="multiple">
                            <option></option>
                            <option value="1">Week 1</option>
                            <option value="2">Week 2</option>
                            <option value="3">Week 3</option>
                            <option value="4">Week 4</option>
                            <option value="5">Week 5</option>
                            <option value="6">Week 6</option>
                        </select>
                    </div>

                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="fs-5 fw-bold form-label mb-2">
                            <span class="required">Programme</span>
                        </label>
                        <select class="form-select swal2-select2" name="programme" id="programme"
                            data-hide-search="true" data-allow-clear="true" data-placeholder="-- Please Select --"
                            multiple="multiple">
                            <option></option>
                            <option value="1">Bachelor of Arts (Honours) Accounting and Finance</option>
                            <option value="2">Bachelor of Business (Honours) Banking and Finance</option>
                            <option value="3">Bachelor of International Tourism Management (Honours) (Events
                                Management)</option>
                        </select>
                    </div>

                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="fs-5 fw-bold form-label mb-2">
                            <span class="required">Institution</span>
                        </label>
                        <select class="form-select swal2-select2" name="campus" id="campus"
                            data-hide-search="true" data-allow-clear="true" data-placeholder="-- Please Select --"
                            multiple="multiple">
                            <option></option>
                            <option value="*">Both</option>
                            <option value="tu">TU</option>
                            <option value="tc">TC</option>
                        </select>
                    </div>
                    <x-slot name="didOpen">
                        
                    </x-slot>
                </x-swal.upsert>
                {!! $quizDt->html()->table([
                    'class' =>
                        'table table-bordered table-striped align-middle table-row-dashed fs-6 gy-5 th-text-uppercase th-fw-bold',
                ]) !!}
                @push('script')
                    {!! $quizDt->html()->generateScripts() !!}
                @endpush
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card-->
    </div>
</x-layouts.app>
