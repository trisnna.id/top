<div class="row g-5 g-xl-10 mb-5 mb-sm-10">
    <div class="col-sm-6">
        <div class="card card-flush h-lg-100">
            <div class="card-header pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-dark text-uppercase">Resources Summary</span>
                </h3>
            </div>
            <div class="card-body pt-6">
                <div id="kt_apexcharts_1" style="height: 350px;"></div>
                @push('script')
                    modules.dashboard.admin.index.renderResourceChart('kt_apexcharts_1','{!! json_encode($charts['resources']) !!}');
                @endpush
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card card-flush border-0">
            <div class="card-header pt-5">
                <h3 class="card-title">
                    <span class="fs-3 fw-bold me-2 text-uppercase">Total No. of Orientation Activities</span>
                </h3>
                <div class="card-toolbar">
                    <div class="card-toolbar" style="width: auto;overflow: unset;">
                        <x-modules.admin.dashboard.index.card.filter id="orientationTypeDt"
                            applyOnClick="modules.dashboard.admin.index.countOrientationTypeDt();" />
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                <div class="row">
                    <div class="col fv-row fv-plugins-icon-container">
                        <select name="orientationActivity" class="form-select form-select2" id="orientationActivity"
                            data-hide-search="true" data-allow-clear="true" data-placeholder="-- Please Select --">
                            <option selected value="1">Compulsory Core and Complementary Activities</option>
                            <option value="2">Intake No. / Cohort</option>
                            <option value="3">Programme</option>
                        </select>
                    </div>
                </div>
                <div id="orientationTypeDtContainer">
                    {!! $orientationTypeDt->html()->table() !!}
                    @push('script')
                        {!! $orientationTypeDt->generateMinifyScripts() !!}
                    @endpush
                </div>
                <div id="orientationIntakeDtContainer">
                    {!! $orientationIntakeDt->html()->table() !!}
                    @push('script')
                        {!! $orientationIntakeDt->generateMinifyScripts() !!}
                    @endpush
                </div>
                <div id="orientationProgrammeDtContainer">
                    {!! $orientationProgrammeDt->html()->table() !!}
                    @push('script')
                        {!! $orientationProgrammeDt->generateMinifyScripts() !!}
                    @endpush
                </div>
                @push('script')
                    modules.dashboard.admin.index.orientation();
                @endpush
            </div>
        </div>
    </div>
</div>
