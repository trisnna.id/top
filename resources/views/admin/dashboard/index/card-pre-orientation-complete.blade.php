<div class="card h-100">
    <div class="card-header pt-5 border-0">
        <h3 class="card-title align-items-start flex-column">
            <div class="m-0">
                <i class="{{ getConstant('IconConstant', 'icon_class', 'PRE_ORIENTATION') }} text-black fs-5x"></i>
            </div>
        </h3>
        <div class="card-toolbar" style="width: auto;overflow: unset;">
            <x-modules.admin.dashboard.index.card.filter id="preOrientationComplete"
                applyOnClick="modules.dashboard.admin.index.countPreOrientationComplete();" />
        </div>
    </div>
    <div class="card-body d-flex justify-content-between align-items-start flex-column pt-2">
        <div class="d-flex flex-column">
            <div class="d-flex">
                <span id="preOrientationCompleteTotal"
                    class="fw-semibold fs-3x text-gray-800 lh-1 ls-n2 text-orange">0</span>
                <span class="fw-semibold fs-3x text-gray-800 lh-1 ls-n2 text-orange">%</span>
            </div>
            @push('script')
                modules.dashboard.admin.index.countPreOrientationComplete();
            @endpush
            <div class="m-0">
                <span class="fw-semibold fs-3">Pre-Orientation Activities Completion by Students</span>
            </div>
        </div>
    </div>
</div>
