<div class="card h-100">
    <div class="card-header pt-5 border-0">
        <h3 class="card-title align-items-start flex-column">
            <div class="m-0">
                <i class="{{ getConstant('IconConstant', 'icon_class', 'STUDENT') }} text-black fs-5x"></i>
            </div>
        </h3>
        <div class="card-toolbar" style="width: auto;overflow: unset;">
            <x-modules.admin.dashboard.index.card.filter id="activeStudent" applyOnClick="modules.dashboard.admin.index.countActiveStudent();" />
        </div>
    </div>
    <div class="card-body d-flex justify-content-between align-items-start flex-column pt-2">
        <div class="d-flex flex-column">
            <span id="activeStudentTotal" class="fw-semibold fs-3x text-gray-800 lh-1 ls-n2 text-orange">0</span>
            @push('script')
                modules.dashboard.admin.index.countActiveStudent();
            @endpush
            <div class="m-0">
                <span class="fw-semibold fs-3">Active Students</span>
            </div>
        </div>
        <div class="d-flex flex-column content-justify-center flex-row-fluid">
            <div class="d-flex fw-semibold align-items-center">
                <div class="bullet w-8px h-3px rounded-2 bg-success me-3"></div>
                <div class="flex-grow-1 me-4">Local</div>
                <div id="activeStudentLocal" class="fw-bolder text-xxl-end">0</div>
            </div>
            <div class="d-flex fw-semibold align-items-center my-3">
                <div class="bullet w-8px h-3px rounded-2 bg-primary me-3"></div>
                <div class="flex-grow-1 me-4">International</div>
                <div id="activeStudentInternational" class="fw-bolder text-xxl-end">0</div>
            </div>
        </div>
    </div>
</div>
