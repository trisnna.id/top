<div class="row">
    <div class="col">
        <div class="card card-flush h-xl-100">
            <div class="card-header pt-7">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-dark text-uppercase">Orientation Calendar</span>
                </h3>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                        @include('admin.orientation.index.calendar-filter')
                    </div>
                </div>
            </div>
            <div class="card-body pt-7">
                <div class="row g-5 g-xl-9 mb-5 mb-xl-9">
                    <div class="col-md-4 mb-6 mb-md-0">
                        <h3 class="align-items-start flex-column">
                            <span class="card-label fw-bold text-dark">Today's Activities</span><br/>
                            <span class="text-gray-400 mt-1 fw-semibold fs-6">Do not miss your activity & attendance</span>
                        </h3>
                        <div class="accordion accordion-icon-toggle" id="kt_accordion_2">
                            <div class="mb-5">
                                <div class="accordion-header py-3 d-flex collapsed bg-{{getConstant('IconConstant')['compulsory_core']['color']['class']}} rounded p-3" data-bs-toggle="collapse" data-bs-target="#kt_accordion_2_item_2">
                                    <span class="accordion-icon">
                                        <span class="svg-icon svg-icon-4 text-black">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor" />
                                                <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor" />
                                            </svg>
                                        </span>
                                    </span>
                                    <h3 class="fs-4 fw-semibold mb-0 ms-4 text-black">Compulsory Core Activities</h3>
                                    @if($count['compulsoryCoreActivity'] > 0)
                                        <span class="badge badge-light-info fw-bold fs-9 px-2 py-1 ms-1">{{$count['compulsoryCoreActivity']}}</span>
                                    @endif
                                </div>
                                <div id="kt_accordion_2_item_2" class="fs-6 collapse mt-5" data-bs-parent="#kt_accordion_2">
                                    {!! $compulsoryCoreDt->html()->table(['class' =>'table thead-none table-bordered table-striped align-middle table-row-dashed fs-6 gy-5 th-text-uppercase th-fw-bold w-100']) !!}
                                    @push('script')
                                        {!! $compulsoryCoreDt->generateMinifyScripts() !!}
                                    @endpush
                                </div>
                            </div>
                            <div class="mb-5">
                                <div class="accordion-header py-3 d-flex collapsed bg-{{getConstant('IconConstant')['complementary']['color']['class']}} rounded p-3" data-bs-toggle="collapse" data-bs-target="#kt_accordion_2_item_3">
                                    <span class="accordion-icon">
                                        <span class="svg-icon svg-icon-4 text-black">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor" />
                                                <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor" />
                                            </svg>
                                        </span>
                                    </span>
                                    <h3 class="fs-4 fw-semibold mb-0 ms-4 text-black">Complementary Activities</h3>
                                    @if($count['complementaryActivity'] > 0)
                                        <span class="badge badge-light-info fw-bold fs-9 px-2 py-1 ms-1">{{$count['complementaryActivity']}}</span>
                                    @endif
                                </div>
                                <div id="kt_accordion_2_item_3" class="fs-6 collapse mt-5" data-bs-parent="#kt_accordion_2">
                                    {!! $complementaryDt->html()->table(['class' =>'table thead-none table-bordered table-striped align-middle table-row-dashed fs-6 gy-5 th-text-uppercase th-fw-bold w-100']) !!}
                                    @push('script')
                                        {!! $complementaryDt->generateMinifyScripts() !!}
                                    @endpush
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 mb-6 mb-md-0">
                        <div class="m-0">
                            @include('admin.orientation.index.calendar')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>