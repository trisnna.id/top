<div class="row g-5 g-xl-10 mb-5 mb-sm-10">
    <div class="col">
        <div class="card card-flush">
            <div class="card-header pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-dark text-uppercase">Total No. of RSVP for Orientation Activities</span>
                </h3>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                        <div>
                            <x-modules.admin.dashboard.index.graph.filter id="rsvpOrientationActivityDt" applyOnClick="modules.dashboard.admin.index.countRsvpOrientationActivityDt();" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0">
                {!! $rsvpOrientationActivityDt->html()->table() !!}
                @push('script')
                    {!! $rsvpOrientationActivityDt->generateMinifyScripts() !!}
                @endpush
            </div>
        </div>
    </div>
</div>