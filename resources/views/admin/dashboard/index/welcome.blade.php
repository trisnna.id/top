<div class="row g-5 g-xl-10 mb-5 mb-sm-10">
    <div class="col-sm-3">
        <div class="card h-100">
            <div class="card-body d-flex align-items-center">
                <div class="m-0">
                    <h1 class="fw-semibold text-center lh-lg mb-9">
                        <span class="text-uppercase fw-bolder">
                            Hi {{ auth()->user()->name }} !
                        </span>
                    </h1>
                    <p class="fw-semibold lh-lg mb-2">
                        Welcome to Taylor’s Orientation Portal.<br>It’s great to meet you.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="row mb-4">
            <div class="col-sm-4 mb-5 mb-sm-0">
                @include('admin.dashboard.index.card-active-student')
            </div>
            <div class="col-sm-4 mb-5 mb-sm-0">
                @include('admin.dashboard.index.card-pre-orientation-complete')
            </div>
            <div class="col-sm-4 mb-5 mb-sm-0">
                @include('admin.dashboard.index.card-student-login')
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 mb-5 mb-sm-0">
                @include('admin.dashboard.index.card-attendance-orientation')
            </div>
            <div class="col-sm-4 mb-5 mb-sm-0">
                @include('admin.dashboard.index.card-attendance-orientation-academic')
            </div>
            <div class="col-sm-4 mb-5 mb-sm-0">
                <div class="card h-100">
                    <div class="card-header pt-5 border-0">
                        <h3 class="card-title align-items-start flex-column">
                            <div class="m-0">
                                <i class="fas fa-poll-h text-black fs-5x"></i>
                            </div>
                        </h3>
                    </div>
                    <div class="card-body d-flex justify-content-between align-items-start flex-column pt-2">
                        <div class="d-flex flex-column">
                            <span id="counter_announcement"
                                class="fw-semibold fs-3x text-gray-800 lh-1 ls-n2 text-orange">0</span>
                            @push('script')
                                new countUp.CountUp('counter_announcement', 5).start();
                            @endpush
                            <div class="m-0">
                                <span class="fw-semibold fs-3">Survey Activity Completion</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
