<div class="card h-100">
    <div class="card-header pt-5 border-0">
        <h3 class="card-title align-items-start flex-column">
            <div class="m-0">
                <i class="fas fa-sign-in-alt text-black fs-5x"></i>
            </div>
        </h3>
        <div class="card-toolbar" style="width: auto;overflow: unset;">
            <x-modules.admin.dashboard.index.card.filter id="studentLogin" applyOnClick="modules.dashboard.admin.index.countStudentLogin();" />
        </div>
    </div>
    <div class="card-body d-flex justify-content-between align-items-start flex-column pt-2">
        <div class="d-flex flex-column">
            <span id="studentLoginTotal" class="fw-semibold fs-3x text-gray-800 lh-1 ls-n2 text-orange">0</span>
            @push('script')
                modules.dashboard.admin.index.countStudentLogin();
            @endpush
            <div class="m-0">
                <span class="fw-semibold fs-3">Total No of Student's Login to Portal</span>
            </div>
        </div>
    </div>
</div>
