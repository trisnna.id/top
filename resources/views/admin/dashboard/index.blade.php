<x-layouts.app :title="$title">
    <div id="kt_app_content_container" class="app-container container-xxl z-index-2 mt-10">
        @include('admin.dashboard.index.welcome')
        @include('admin.dashboard.index.resource')
        @include('admin.dashboard.index.attendance')
        @include('admin.dashboard.index.rsvp')
        @include('admin.dashboard.index.preorientation')
    </div>
    <div class="app-container container-xxl">
        @include('admin.dashboard.index.timetable')
    </div>
</x-layouts.app>
