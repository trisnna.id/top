<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="__('description.title.management', ['x' => 'Role'])"/>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-stack flex-wrap mb-5">
                    <div class="d-flex flex-row">
                        <div class="me-2">
                            <button class="btn btn-icon btn-black" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                <i class="fa-solid fa-bars fs-3"></i>
                            </button>
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3"
                                data-kt-menu="true" style="">
                                <div class="menu-item px-3">
                                    <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Bulk Action</div>
                                </div>
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3 upsert-swal-popup"
                                        data-swal="{{ json_encode([
                                            'id' => 'bulkActive',
                                            'axios' => [
                                                'url' => route('admin.roles.update-bulk-active'),
                                                'method' => 'post',
                                            ],
                                        ]) }}">Active
                                        Selected</a>
                                    @include('admin.role.index.swal-bulk-active')
                                </div>
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3 upsert-swal-popup"
                                        data-swal="{{ json_encode([
                                            'id' => 'bulkInactive',
                                            'axios' => [
                                                'url' => route('admin.roles.update-bulk-inactive'),
                                                'method' => 'post',
                                            ],
                                        ]) }}">Inactive
                                        Selected</a>
                                        @include('admin.role.index.swal-bulk-inactive')
                                </div>
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3 checkbox-reset" data-table="roleDt">Reset Selected</a>
                                </div>
                            </div>
                        </div>
                        <x-datatables.search id="roleDt" placeholder="Search Role" />
                    </div>
                    <div class="d-flex justify-content-end">
                        <button class="btn fw-bold btn-black upsert-swal-popup"
                            data-swal="{{ json_encode([
                                'id' => 'upsert',
                                'axios' => [
                                    'url' => route('admin.roles.store'),
                                    'method' => 'post',
                                ],
                            ]) }}">
                            <i class="fas fa-plus fa-sm"></i>
                            Add Role
                        </button>
                        <x-swal.upsert :id="'upsert'" :swalSettings="[
                            'title' => 'Role',
                        ]">
                            @method('PUT')
                            @include('admin.role.index.form-role', ['id' => 'upsert'])
                            <x-slot name="didOpen">
                                modules.role.upsert('form#upsert',data);
                            </x-slot>
                        </x-swal.upsert>
                    </div>
                </div>
                {!! $roleDt->html()->table() !!}
                @push('script')dtx.initOption('roleDt');{!! $roleDt->generateMinifyScripts() !!}@endpush
            </div>
        </div>
    </div>
</x-layouts.app>
