<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span class="required">Name of Role</span>
    </label>
    <input type="text" class="form-control" name="title" id="title" placeholder="Name of Role">
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        Description
    </label>
    <textarea class="form-control" name="description" id="description" placeholder="Description"></textarea>
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
        <input id="is_active" class="form-check-input" type="checkbox" value="true" name="is_active" checked>
        <span class="fw-bold form-check-label">
            Make it Active?
        </span>
    </label>
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        Permissions
    </label>
    <div class="table-responsive">
        <table class="table align-middle table-row-dashed gy-5">
            <tbody class="text-gray-600 fw-semibold">
                <tr style="border: none;">
                    <td class="text-gray-800">User&nbsp;&&nbsp;Role Management</td>
                    <td></td>
                </tr>
                <tr style="border: none;">
                    <td class="text-gray-800 pt-0" style="padding-left: 1.25rem;">User Management</td>
                    <td class="pt-0">
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_manage_users" class="form-check-input" type="checkbox"
                                    value="manage_users" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800 pt-0" style="padding-left: 1.25rem;">Role Management</td>
                    <td class="pt-0">
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_manage_roles" class="form-check-input" type="checkbox"
                                    value="manage_roles" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800">Announcements</td>
                    <td>
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_manage_announcements" class="form-check-input" type="checkbox"
                                    value="manage_announcements" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800">Arrival&nbsp;&&nbsp;FAQ</td>
                    <td>
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_manage_resources" class="form-check-input" type="checkbox"
                                    value="manage_resources" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800">Pre-Orientation</td>
                    <td>
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_manage_pre_orientations" class="form-check-input" type="checkbox"
                                    value="manage_pre_orientations" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800">Orientation Activity</td>
                    <td>
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_view_orientations" class="form-check-input" type="checkbox"
                                    value="view_orientations" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800">Orientation Timetable</td>
                    <td>
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_manage_orientations" class="form-check-input" type="checkbox"
                                    value="manage_orientations" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800">Orientation Approval</td>
                    <td>
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_manage_activity_approval" class="form-check-input"
                                    type="checkbox" value="manage_activity_approval" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800">Student List</td>
                    <td>
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_manage_student_lists" class="form-check-input" type="checkbox"
                                    value="manage_student_lists" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr style="border: none;">
                    <td class="text-gray-800">Reports</td>
                    <td></td>
                </tr>
                <tr style="border: none;">
                    <td class="text-gray-800 pt-0" style="padding-left: 1.25rem;">Audit Trails</td>
                    <td class="pt-0">
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_access_report_audit" class="form-check-input" type="checkbox"
                                    value="access_report_audit" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr style="border: none;">
                    <td class="text-gray-800 pt-0" style="padding-left: 1.25rem;">Student Login Details</td>
                    <td class="pt-0">
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_access_report_login" class="form-check-input" type="checkbox"
                                    value="access_report_login" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800 pt-0" style="padding-left: 1.25rem;">Student Attendance</td>
                    <td class="pt-0">
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_access_report_attendance" class="form-check-input"
                                    type="checkbox" value="access_report_attendance" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800 pt-0" style="padding-left: 1.25rem;">Student Registration</td>
                    <td class="pt-0">
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_access_report_registration" class="form-check-input"
                                    type="checkbox" value="access_report_registration" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800 pt-0" style="padding-left: 1.25rem;">Student Activity</td>
                    <td class="pt-0">
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_access_report_activity" class="form-check-input"
                                    type="checkbox" value="access_report_activity" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800 pt-0" style="padding-left: 1.25rem;">Student Survey</td>
                    <td class="pt-0">
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_access_report_survey" class="form-check-input" type="checkbox"
                                    value="access_report_survey" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800 pt-0" style="padding-left: 1.25rem;">Student Gamification</td>
                    <td class="pt-0">
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_access_report_gamification" class="form-check-input"
                                    type="checkbox" value="access_report_gamification" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-gray-800">Settings</td>
                    <td>
                        <div class="d-flex">
                            <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                <input id="permission_manage_settings" class="form-check-input" type="checkbox"
                                    value="manage_settings" name="permissions[]">
                                <span class="form-check-label">
                                    Access
                                </span>
                            </label>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
