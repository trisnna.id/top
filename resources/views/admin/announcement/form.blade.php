<form method="post" action="{{ $route }}" class="form theme-form" id="announcementForm" autocomplete="off"
    enctype="multipart/form-data">
    @csrf
    @method($method)
    <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
        <div class="mb-0">
            <label class="fs-5 fw-bold form-label mb-2">
                <span>{{ __('description.labels.sequence') }}</span>
            </label>
            <input type="number" min="1" class="form-control" name="sequence" id="sequence"
                placeholder="{{ __('description.placeholder.sequence') }}"
                value="{{ old('sequence', optional($announcement)->sequence) }}">
            <x-inputs.error :name="__('sequence')" />
        </div>
    </div>
    {{-- @if ($errors->any())
        {{ dd($errors) }}
    @endif --}}
    <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
        <div class="mb-0">
            <label class="fs-5 fw-bold form-label mb-2">
                <span class="required">Title</span>
            </label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Title"
                value="{{ old('title', optional($announcement)->name) }}">
            <x-inputs.error :name="__('title')" />
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <div class="mb-0">
                    <label class="fs-5 fw-bold form-label mb-2">
                        <span class="required">Thumbnail</span>
                    </label>
                    <div class="fv-row col-12 mb-5">
                        <div class="image-input image-input-outline image-placeholder" data-kt-image-input="true"
                            style="background-image: url({{ url('https://dummyimage.com/600x400/000/fff') }})">
                            <div class="image-input-wrapper w-300px h-200px image-placeholder"
                                style="background-image: url({{ !empty($announcement ?? null) && $announcement->thumbnail != null ? url($announcement->thumbnail) : url('https://dummyimage.com/600x400/000/fff') }})">
                            </div>
                            <label
                                class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                data-kt-image-input-action="change" data-bs-toggle="tooltip" data-bs-dismiss="click"
                                title="Change Thumbnail">
                                <i class="bi bi-pencil-fill fs-7"></i>
                                <input class="" type="file" name="thumbnail"
                                    accept="{{ settingExtensionFormat('jpg jpeg png webp') }}" />
                                <input class="" type="hidden" name="logo_remove" />
                                <input type="hidden" name="thumbnail_hex">
                            </label>
                            <span
                                class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                                data-kt-image-input-action="cancel" data-bs-toggle="tooltip" data-bs-dismiss="click"
                                title="Cancel Thumbnail">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                                data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove Thumbnail">
                                <i class="bi bi-x fs-2"></i>
                            </span>
                        </div>
                        <div class="form-text text-black">Allowed file types:
                            {{ settingExtensionFormat('jpg jpeg png webp', '') }}. Max size:
                            {{ settingExtensionFormat('2', '') }} MB</div>
                        <div class="modal fade" tabindex="-1" id="cropper-modal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Crop Thumbnail</h5>
                                    </div>
                                    <div class="modal-body" id="cropper-content">
                                        <img id="cropper-image" style="max-width: 100%; display: block;" width="100%">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" id="cropper-save">Save
                                            changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <label class="fs-5 fw-bold form-label mb-2">
                    <span class="required">Intake</span>
                </label>
                @php
                    $originalIntake = optional(optional($announcement)->intakes)['original'];
                @endphp
                <select
                    class="form-select announcement-select2 {{ $errors->has('intake') ? 'is-invalid' : '' }} all-support"
                    name="intake[]" id="intake" multiple="multiple"
                    data-old="{{ is_array(old('intake', $originalIntake)) ? implode(',', old('intake', $originalIntake)) : old('intake', $originalIntake) }}">
                    <option value="all">{{ __('All') }}</option>
                    @foreach ($intakes as $intake)
                        <option value="{{ $intake->id }}">{{ $intake->name }}
                        </option>
                    @endforeach
                </select>
                <x-inputs.error :name="__('intake')" />
            </div>
        </div>
        <div class="col-md-6">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <label class="fs-5 fw-bold form-label mb-2">
                    <span class="required">Campus</span>
                </label>
                @php
                    $originalCampus = optional(optional($announcement)->campuses)['original'];
                @endphp
                <select
                    class="form-select announcement-select2 {{ $errors->has('campus') ? 'is-invalid' : '' }} all-support"
                    name="campus[]" id="campus" multiple="multiple"
                    data-old="{{ is_array(old('campus', $originalCampus)) ? implode(',', old('campus', $originalCampus)) : old('campus', $originalCampus) }}">
                    <option value="all">{{ __('All') }}</option>
                    @foreach ($campuses as $campus)
                        <option value="{{ $campus->id }}">{{ $campus->name }}
                        </option>
                    @endforeach
                </select>
                <x-inputs.error :name="__('campus')" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            @php
                $originalNationality = optional(optional($announcement)->localities)['original'];
            @endphp
            <x-inputs.nationality :current="old('nationality', $originalNationality)" :moduleName="'announcement'" :containerClassName="__('d-flex flex-column mb-5 fv-row fv-plugins-icon-container')" />
        </div>
        <div class="col-md-6">
            <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
                <label class="fs-5 fw-bold form-label mb-2">
                    <span class="required">Student Programme</span>
                </label>
                @php
                    $originalProgramme = optional(optional($announcement)->programmes)['original'];
                @endphp
                <select
                    class="form-select announcement-select2 {{ $errors->has('programme') ? 'is-invalid' : '' }} all-support"
                    name="programme[]" id="programme" multiple="multiple"
                    data-old="{{ is_array(old('programme', $originalProgramme)) ? implode(',', old('programme', $originalProgramme)) : old('programme', $originalProgramme) }}">
                    <option value="all">{{ __('All') }}</option>
                    @foreach ($programmes as $programme)
                        <option value="{{ $programme->id }}">{{ $programme->name }}</option>
                    @endforeach
                </select>
                <x-inputs.error :name="__('programme')" />
            </div>
        </div>
    </div>
    <div class="d-flex flex-column fv-row fv-plugins-icon-container mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            <span class="required">Content</span>
        </label>
        <textarea class="form-control tox-target {{ $errors->has('content') ? 'is-invalid' : '' }}" id="contentEditor"
            name="content" placeholder="This field is required">{!! old('content', optional($announcement)->content) !!}</textarea>
        <x-inputs.error :name="__('content')" />
    </div>
</form>
