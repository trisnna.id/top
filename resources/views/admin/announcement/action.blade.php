<div class="btn-group" role="group">
    <input type="hidden" name="key" value="{{ $uuid }}">
    <a class="btn btn-icon btn-info btn-sm" href="{{ route('admin.announcements.show', ['uuid' => $uuid]) }}">
        <i class="fas fa-eye"></i>
    </a>
    <a data-key="edit-announcement" href="{{ route('admin.announcements.edit', ['uuid' => $uuid]) }}"
        class="btn btn-icon btn-black btn-sm" {{ $isPublished ? 'hidden' : '' }}>
        <i class="fas fa-edit"></i>
    </a>
    <a class="btn btn-icon btn-red btn-sm" href="{{ route('admin.announcements.delete', ['uuid' => $uuid]) }}"
        data-method="delete" data-token="{{ csrf_token() }}" {{ $isPublished ? 'hidden' : '' }}>
        <i class="fas fa-trash"></i>
    </a>
    <a class="btn btn-icon btn-success btn-sm" href="#publish" data-route="{{ route('api.announcements.publish') }}"
        data-token="{{ csrf_token() }}" {{ $isPublished ? 'hidden' : '' }}>
        <i class="fas fa-cloud-upload"></i>
    </a>
    <a class="btn btn-icon btn-poppy btn-sm" href="#unpublish" data-route="{{ route('api.announcements.unpublish') }}"
        data-token="{{ csrf_token() }}" {{ $isUnpublished || !$isPublished ? 'hidden' : '' }}>
        <i class="fas fa-cloud-download"></i>
    </a>
</div>
