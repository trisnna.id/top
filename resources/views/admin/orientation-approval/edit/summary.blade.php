@php
    if($orientation->is_publish && $orientation->is_type_activity){
        $colMd = 8;
    }else{
        $colMd = 12;
    }
@endphp

<div class="card mb-5">
    <div class="card-body">
        <div class="row">
            <div class="col-md-{{$colMd}}">
                <div class="row">
                    <label class="col-md-4 fw-semibold text-info">Activity</label>
                    <div class="col-md-8 fv-row"><span class="fs-6">{{ $orientation->name }}</span></div>
                </div>
                @if(!empty($orientation->venue ?? null))
                    <div class="row">
                        <label class="col-md-4 fw-semibold text-info">Venue</label>
                        <div class="col-md-8 fv-row"><span class="fs-6">{{ $orientation->venue }}</span></div>
                    </div>
                @endif
                <div class="row">
                    <label class="col-md-4 fw-semibold text-info">Datetime of Activity</label>
                    <div class="col-md-8 fv-row"><span class="fs-6">{{ $orientation->start_end_datetime_dFY }}</span></div>
                </div>
                <div class="row">
                    <label class="col-md-4 fw-semibold text-info">Status</label>
                    <div class="col-md-8 fv-row"><span class="fs-6">{{ $orientation->status_name }}</span></div>
                </div>
                <div class="row">
                    <label class="col-md-4 fw-semibold text-info">Publish</label>
                    <div class="col-md-8 fv-row"><span class="fs-6">{{ $orientation->is_publish_yes_no }}</span></div>
                </div>
                @if($orientation->is_publish && $orientation->is_type_activity)
                    <div class="row">
                        <label class="col-md-4 fw-semibold text-info">Scan Availability</label>
                        <div class="col-md-8 fv-row"><span class="fs-6">{{ $orientation->start_end_datetime_dFY_scan }}</span></div>
                    </div>
                @endif
            </div>
            @if($orientation->is_publish && $orientation->is_type_activity)
                <div class="col-md-4">
                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="fw-bold form-label mb-2 text-info">
                            <span>Scan Attendance</span>
                        </label>
                        <canvas class="w-100 h-100" id="attendance-qr-code" style="max-width: 150px;max-height: 150px;"></canvas>
                    </div>
                    @push('script')
                        modules.orientation.generateAttendanceQRCode('attendance-qr-code',{!! json_encode($attendanceQrCode) !!});
                    @endpush
                </div>
            @endif
        </div>
    </div>
</div>
