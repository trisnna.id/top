<div class="card mb-5 shadow-none" style="background-color: var(--kt-app-bg-color);">
    <div class="card-body p-0">
        <div class="form-group">
            <div class="d-flex justify-content-center">
                <div class="btn-group">
                    @if (!empty($instance['publish'] ?? null))
                        <button type="button" class="btn btn-{{$instance['publish']['data']['color_class'] ?? null}} upsert-swal-popup"
                            data-swal="{{json_encode($instance['publish']['swal'] ?? [])}}">
                            {{$instance['publish']['data']['status'] ?? []}}
                        </button>
                    @endif
                    @if (!empty($instance['approve'] ?? null))
                        <button type="button" class="btn btn-success upsert-swal-popup"
                            data-swal="{{ json_encode($instance['approve']['swal']) }}">
                            Approve & Publish
                        </button>
                        <x-swal.upsert :id="$instance['approve']['swal']['id']" :swalSettings="$instance['approve']['swal']['settings']">
                            <span class="d-flex flex-column mb-5 fv-row text-center">
                                This action will APPROVE the orientation activity request
                            </span>
                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="fw-bold form-label mb-2">
                                    Comment/Note
                                </label>
                                <textarea class="form-control" name="note" id="note" placeholder="Comment/Note"></textarea>
                            </div>
                        </x-swal.upsert>
                    @endif

                    @if (!empty($instance['notify'] ?? null))
                        <button type="button" class="btn btn-orange upsert-swal-popup"
                            data-swal="{{ json_encode($instance['notify']['swal']) }}">
                            Notify Student
                        </button>
                        <x-swal.upsert :id="$instance['notify']['swal']['id']" :swalSettings="$instance['notify']['swal']['settings']">
                            <span class="d-flex flex-column mb-5 fv-row text-center">
                                This action will NOTIFY the student for a changes.
                            </span>
                        </x-swal.upsert>
                    @endif

                    @if (!empty($instance['correction'] ?? null))
                        <button type="button" class="btn btn-warning upsert-swal-popup"
                            data-swal="{{ json_encode($instance['correction']['swal']) }}">
                            Require Correction
                        </button>
                        <x-swal.upsert :id="$instance['correction']['swal']['id']" :swalSettings="$instance['correction']['swal']['settings']">
                            <span class="d-flex flex-column mb-5 fv-row text-center">
                                This action will return back the orientation activity request to user for CORRECTION
                            </span>
                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="fw-bold form-label mb-2">
                                    Comment/Note
                                </label>
                                <textarea class="form-control" name="note" id="note" placeholder="Comment/Note"></textarea>
                            </div>
                            <x-slot name="didOpen">

                            </x-slot>
                        </x-swal.upsert>
                    @endif
                    @if (!empty($instance['cancel'] ?? null))
                        <button type="button" class="btn btn-danger upsert-swal-popup"
                            data-swal="{{ json_encode($instance['cancel']['swal']) }}">
                            Cancel & Unpublish
                        </button>
                        <x-swal.upsert :id="$instance['cancel']['swal']['id']" :swalSettings="$instance['cancel']['swal']['settings']">
                            <span class="d-flex flex-column mb-5 fv-row text-center">
                                This action will CANCEL the orientation activity request
                            </span>
                            <div class="d-flex flex-column mb-5 fv-row">
                                <label class="fw-bold form-label mb-2">
                                    Comment/Note
                                </label>
                                <textarea class="form-control" name="note" id="note" placeholder="Comment/Note"></textarea>
                            </div>
                        </x-swal.upsert>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
