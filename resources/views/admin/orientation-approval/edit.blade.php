<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="$title" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        @include('admin.orientation-approval.edit.action')
        @include('admin.orientation-approval.edit.summary')
        <div class="card">
            <ul class="card-header nav nav-tabs nav-line-tabs min-h-auto text-nowrap mt-3 flex-nowrap overflow-y-auto">
                <li class="nav-item card-title mb-0">
                    <a class="nav-link active text-black" data-bs-toggle="tab" href="#details">
                        Orientation Activity Details
                    </a>
                </li>
                @if ($orientation->is_status_approved)
                    @if ($orientation->is_rsvp)
                        <li class="nav-item card-title mb-0">
                            <a class="nav-link text-black" data-bs-toggle="tab" href="#rsvp-tab">
                                RSVP
                            </a>
                        </li>
                    @endif
                    @if ($orientation->is_type_activity)
                        <li class="nav-item card-title mb-0">
                            <a class="nav-link text-black" data-bs-toggle="tab" href="#attendance-tab">
                                Attendance
                            </a>
                        </li>
                    @endif
                    <li class="nav-item card-title mb-0">
                        <a class="nav-link text-black" data-bs-toggle="tab" href="#recording-tab">
                            Recording & Attachment
                        </a>
                    </li>
                @endif
                <li class="nav-item card-title mb-0">
                    <a class="nav-link text-black" data-bs-toggle="tab" href="#log-tab">
                        Log History
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active pt-3" id="details" role="tabpanel"
                    style="background-color: var(--kt-app-bg-color);">
                    @include('admin.orientation.create.stepper')
                </div>
                @if ($orientation->is_status_approved)
                    @if ($orientation->is_rsvp)
                        @include('admin.orientation.show.tab-pane-rsvp')
                    @endif
                    @if ($orientation->is_type_activity)
                        @include('admin.orientation.show.tab-pane-attendance')
                    @endif
                    @include('admin.orientation.show.tab-pane-recording')
                @endif
                <div class="tab-pane fade" id="log-tab" role="tabpanel">
                    <div class="card-body">
                        <div class="table-responsive">
                            {!! $actionDt->html()->table() !!}
                            @push('script')
                                {!! $actionDt->generateMinifyScripts() !!}
                            @endpush
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layouts.app>
