<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="$title" :links="[
            [
                'url' => route('admin.orientations.index'),
                'text' => __('description.title.orientation', ['x' => 'Timeable']),
            ],
        ]" />
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        @include('admin.orientation.create.stepper')
    </div>
</x-layouts.app>
