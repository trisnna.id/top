<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="$title" />
    </x-slot>
    <ul class="nav nav-pills nav-pills-custom d-flex flex-center mb-3">
        <li class="nav-item me-3 me-lg-6 mb-3">
            <a class="nav-link btn btn-outline btn-flex btn-color-muted btn-active-color-primary flex-column w-150px h-85px active overflow-hidden pt-5 pb-2"
                id="tab_link_1" data-bs-toggle="pill" href="#tab_1">
                <img class="w-35px mb-1" src="{{ url('media/icons/taylors/detail.png') }}">
                <span class="nav-text fw-bold fs-6 lh-1 text-gray-800">Details</span>
                <span class="bullet-custom position-absolute w-100 h-4px bg-red bottom-0"></span>
            </a>
        </li>
        <li class="nav-item me-3 me-lg-6 mb-3">
            <a class="nav-link btn btn-outline btn-flex btn-color-muted btn-active-color-primary flex-column w-150px h-85px overflow-hidden pt-5 pb-2"
                id="tab_link_2" data-bs-toggle="pill" href="#tab_2"
                onclick="fullcalendars['orientationCalendar'].updateSize()">
                <img class="w-35px mb-1" src="{{ url('media/icons/taylors/calendar.png') }}">
                <span class="nav-text fw-bold fs-6 lh-1 text-gray-800">Calendar</span>
                <span class="bullet-custom position-absolute w-100 h-4px bg-red bottom-0"></span>
            </a>
        </li>
    </ul>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tab_1">
                @include('admin.orientation.index.tab-detail')
            </div>
            <div class="tab-pane fade" id="tab_2">
                @include('admin.orientation.index.tab-calendar')
            </div>
        </div>
    </div>
</x-layouts.app>
