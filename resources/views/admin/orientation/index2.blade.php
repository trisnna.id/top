<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-white fw-bold fs-3 flex-column justify-content-center my-0">
                    {{ $title }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-white text-hover-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('dashboard.index') }}" class="text-white text-hover-white">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <span class="text-primary">{{ $title }}</span>
                    </li>
                </ul>
            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-stack flex-wrap mb-5">
                    <x-datatables.search id="viewOrientationDt" placeholder="Search Orientation (Details)" />
                    <div class="d-flex justify-content-end">
                        <x-modules.admin.orientation.index.detail.filter id="viewOrientationDt" />
                        <div>
                            <x-datatables.export id="viewOrientationDt" />
                        </div>
                    </div>
                </div>
                <div id="viewOrientationDtScrollbarX" class="table-scrollbar-x" data-parentEl="#viewOrientationDt_wrapper .table-responsive">
                    <div class="scrollbar-x">&nbsp;</div>
                </div>
                {!! $viewOrientationDt->html()->table() !!}
                <div id="viewOrientationDtScrollbarXEnd"></div>
                @push('script')
                    dtx.initOption('viewOrientationDt');
                    {!! $viewOrientationDt->generateMinifyScripts() !!}
                @endpush
            </div>
        </div>
        
    </div>
</x-layouts.app>