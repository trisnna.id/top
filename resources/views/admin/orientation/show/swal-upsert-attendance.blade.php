<x-swal.upsert :id="'upsertAttendance'" :swalSettings="['title' => 'Attendance']">
    <div class="d-flex flex-column mb-5 fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Student ID</span>
        </label>
        <textarea name="id_number" class="form-control" placeholder="Student ID. For multiple Student ID, seperate by comma (,)."></textarea>
    </div>
    <x-slot name="didOpen">
    </x-slot>
</x-swal.upsert>