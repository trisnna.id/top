<div class="stepper stepper-pills stepper-column first" id="stepper">
    <div class="row">
        <div class="col-md-4 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="stepper-nav">
                        <div class="stepper-item current" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                            <div class="stepper-wrapper">
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">1</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">Orientation Activity</h3>
                                </div>
                            </div>
                            <div class="stepper-line h-40px"></div>
                        </div>
                        <div class="stepper-item" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                            <div class="stepper-wrapper">
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">2</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">Activity Filters</h3>
                                </div>
                            </div>
                            <div class="stepper-line h-40px"></div>
                        </div>
                        <div class="stepper-item" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                            <div class="stepper-wrapper">
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">3</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">RSVP Settings</h3>
                                </div>
                            </div>
                            <div class="stepper-line h-40px"></div>
                        </div>
                        <div class="stepper-item" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                            <div class="stepper-wrapper">
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">4</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">Activity Points</h3>
                                </div>
                            </div>
                            <div class="stepper-line h-40px"></div>
                        </div>
                        <div class="stepper-item" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                            <div class="stepper-wrapper">
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">5</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">Attachments</h3>
                                </div>
                            </div>
                            @if(!$orientation->is_status_approved)
                                <div class="stepper-line h-40px"></div>
                            @endif
                        </div>
                        @if(!$orientation->is_status_approved)
                            <div class="stepper-item" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <div class="stepper-wrapper">
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">6</span>
                                    </div>
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Log History</h3>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-lg-9">
            <div class="card">
                <form id="stepper-form" novalidate="novalidate" class="card-body">
                    <div class="current" data-kt-stepper-element="content">
                        <div class="w-100">
                            <div>
                                <h2 class="fw-bold d-flex align-items-center text-dark">
                                    Orientation Activity
                                </h2>
                                <div class="separator my-5"></div>
                            </div>
                            <div class="fv-row">
                                @include('admin.orientation.create.form-orientation')
                            </div>
                        </div>
                    </div>
                    <div data-kt-stepper-element="content">
                        <div class="w-100">
                            <div>
                                <h2 class="fw-bold d-flex align-items-center text-dark">
                                    Activity Filters
                                </h2>
                                <div class="fw-semibold">These will help in filtering and showing the activity to a
                                    specific audience</div>
                                <div class="separator my-5"></div>
                            </div>
                            <div class="fv-row">
                                @include('admin.orientation.create.form-orientation-filter')
                            </div>
                        </div>
                    </div>
                    <div data-kt-stepper-element="content">
                        <div class="w-100">
                            <div>
                                <h2 class="fw-bold d-flex align-items-center text-dark">
                                    RSVP Settings
                                </h2>
                                <div class="separator my-5"></div>
                            </div>
                            <div class="fv-row">
                                @include('admin.orientation.create.form-orientation-rsvp')
                            </div>
                        </div>
                    </div>
                    <div data-kt-stepper-element="content">
                        <div class="w-100">
                            <div>
                                <h2 class="fw-bold d-flex align-items-center text-dark">
                                    Activity Points
                                </h2>
                                <div class="separator my-5"></div>
                            </div>
                            <div class="fv-row">
                                @include('admin.orientation.create.form-orientation-point')
                            </div>
                        </div>
                    </div>
                    <div data-kt-stepper-element="content">
                        <div class="w-100">
                            <div>
                                <h2 class="fw-bold d-flex align-items-center text-dark">
                                    Attachments
                                </h2>
                                <div class="separator my-5"></div>
                            </div>
                            <div class="fv-row">
                                @include('admin.orientation.create.form-orientation-attachment')
                            </div>
                        </div>
                    </div>
                    @if(!$orientation->is_status_approved)
                        <div data-kt-stepper-element="content">
                            <div class="w-100">
                                <div class="pb-5">
                                    <h2 class="fw-bold d-flex align-items-center text-dark">
                                        Log History
                                    </h2>
                                </div>
                                <div class="fv-row">
                                    {!! $actionDt->html()->table() !!}
                                    @push('script')
                                        {!! $actionDt->generateMinifyScripts() !!}
                                    @endpush
                                </div>
                            </div>
                        </div>
                    @endif
                </form>
                <div class="card-footer d-flex flex-stack pt-10">
                    <div class="mr-2">
                        <button type="button" class="btn btn-lg btn-light-primary"
                            data-kt-stepper-action="previous">
                            <i class="{{ getConstant('IconConstant', 'icon_class', 'PREVIOUS') }} me-3"></i>Back
                        </button>
                    </div>
                    <div>
                        <button type="button" class="btn btn-lg btn-primary"
                            data-kt-stepper-action="next">
                            Next<i class="{{ getConstant('IconConstant', 'icon_class', 'NEXT') }} ms-3"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
    modules.orientation.stepper('stepper',{{ $step ?? '' }});
    modules.orientation.showForm('form#stepper-form',{!! !empty($orientation ?? null) ? json_encode($orientationOnly) : '{}' !!});
@endpush
