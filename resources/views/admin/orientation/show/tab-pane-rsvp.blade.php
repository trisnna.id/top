<div class="tab-pane fade" id="rsvp-tab" role="tabpanel">
    <div class="card-header border-0">
        <x-datatables.search id="orientationRsvpDt" placeholder="Search RSVP" />
    </div>
    <div class="card-body pt-0">
        <div class="table-responsive">
            {!! $orientationRsvpDt->html()->table() !!}
            @push('script')
                dtx.initOption('orientationRsvpDt');
                {!! $orientationRsvpDt->generateMinifyScripts() !!}
            @endpush
        </div>
    </div>
</div>