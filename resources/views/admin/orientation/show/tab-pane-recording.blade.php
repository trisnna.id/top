<div class="tab-pane fade" id="recording-tab" role="tabpanel">
    <div class="card-header border-0">
        <div class="d-flex align-items-center position-relative my-1 mb-2 mb-md-0"></div>
        <div class="card-toolbar">
            <div class="d-flex justify-content-end">
                @can('manage_orientations')
                    <button class="btn btn-info fw-bold upsert-swal-popup" data-swal="{{ json_encode([
                            'id' => 'upsertRecording',
                            'axios' => [
                                'url' => route('admin.orientations.store-recording',$orientation->uuid),
                                'method' => 'post',
                                'contentType' => 'application/x-www-form-urlencoded',
                            ],
                        ]) }}">
                        <i class="fas fa-plus fa-sm"></i>
                        Add Recording & Attachment
                    </button>
                    @include('admin.orientation.show.swal-upsert-recording')
                @endcan
            </div>
        </div>
    </div>
    <div class="card-body pt-0">
        <div class="table-responsive">
            {!! $orientationRecordingDt->html()->table() !!}
            @push('script')
                {!! $orientationRecordingDt->generateMinifyScripts() !!}
            @endpush
        </div>
    </div>
</div>