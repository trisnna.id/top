<x-swal.upsert :id="'upsertRecording'" :swalSettings="['title' => 'Recording & Attachment']">
    <div class="d-flex flex-column mb-5 fv-row">
        <select name="attachment_recording[]" class="form-select swal2-select2" id="attachment_recording" data-hide-search="true"
            data-allow-clear="true" data-placeholder="-- Please Select --" multiple="multiple">
            <option></option>
            <option value="files" data-accept="{{settingExtensionFormat(setting('file.orientation_recording_extension'))}}" data-accept-info="{{settingExtensionFormat(setting('file.orientation_recording_extension'),'')}}" data-size="{{settingExtensionFormat(setting('file.orientation_recording_size'),'')}}">Files</option>
            <option value="links">Links</option>
        </select>
    </div>
    <div id="attachmentRecordingContainer"></div>
    <x-slot name="didOpen">
        window.modules.orientation.admin.show.swalUpsertRecording('form#upsertRecording',data);
    </x-slot>
</x-swal.upsert>

