<div class="tab-pane fade" id="attendance-tab" role="tabpanel">
    <div class="card-header border-0">
        <x-datatables.search id="orientationAttendanceDt" placeholder="Search Attendance" />
        <div class="card-toolbar">
            <div class="d-flex justify-content-end">
                @can('manage_orientations')
                    <button class="btn btn-info fw-bold upsert-swal-popup" data-swal="{{ json_encode([
                            'id' => 'upsertAttendance',
                            'axios' => [
                                'url' => route('admin.orientations.store-attendance',$orientation->uuid),
                                'method' => 'post',
                            ],
                        ]) }}">
                        <i class="fas fa-plus fa-sm"></i>
                        Add Attendances
                    </button>
                    @include('admin.orientation.show.swal-upsert-attendance')
                @endcan
            </div>
        </div>
    </div>
    <div class="card-body pt-0">
        <div class="table-responsive">
            {!! $orientationAttendanceDt->html()->table() !!}
            @push('script')
                dtx.initOption('orientationAttendanceDt');
                {!! $orientationAttendanceDt->generateMinifyScripts() !!}
            @endpush
        </div>
    </div>
</div>