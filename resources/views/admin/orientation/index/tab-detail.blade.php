<div class="card">
    <div class="card-body">
        <div class="d-flex flex-stack mb-5 flex-wrap">
            <x-datatables.search id="orientationDt" placeholder="Search Orientation (Details)" />
            <div class="d-flex justify-content-end">
                <x-modules.admin.orientation.index.detail.filter id="orientationDt" />
                <div>
                    <x-datatables.export id="orientationDt" />
                </div>
                <a href="{{ route('admin.orientations.create') }}" class="btn fw-bold btn-info">
                    <i class="fas fa-plus fa-sm"></i>
                    Add Orientation Activity
                </a>
            </div>
        </div>
        <x-datatables.card.buttons.legend :gap="50" :include="['show', 'edit', 'delete', 'clone', 'approval', 'submit', 'publish', 'unpublish']" />
        <div id="orientationDtScrollbarX" class="table-scrollbar-x" data-parentEl="#orientationDt_wrapper .table-responsive">
            <div class="scrollbar-x">&nbsp;</div>
        </div>
        {!! $orientationDt->html()->table() !!}
        <div id="orientationDtScrollbarXEnd"></div>
        @push('script')
            dtx.initOption('orientationDt');
            {!! $orientationDt->generateMinifyScripts() !!}
        @endpush
    </div>
</div>
