<div class="card">
    <div class="card-body">

        <div class="d-flex flex-stack flex-wrap mb-5">
            <div></div>
            <div class="d-flex justify-content-end">
                @include('admin.orientation.index.calendar-filter')
            </div>
        </div>
        <div class="w-md-80" style="margin: 0 auto;">
            <div class="rounded border border-gray-200 d-flex justify-content-around p-3 mb-3">
                <div class="d-flex flex-column flex-center">
                    <span class="badge badge-e9b7ce">
                        Compulsory Core
                    </span>
                </div>
                <div class="d-flex flex-column flex-center">
                    <span class="badge badge-d3f3f1">
                        Complementary
                    </span>
                </div>
            </div>
        </div>
        
        @include('admin.orientation.index.calendar')
    </div>
</div>
