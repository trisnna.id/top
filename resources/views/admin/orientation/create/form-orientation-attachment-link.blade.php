<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>Title</span>
    </label>
    <input type="text" class="form-control" name="name" id="name" placeholder="Title">
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span class="required">URL</span>
    </label>
    <textarea class="form-control" name="url" id="url" placeholder="URL"></textarea>
</div>
