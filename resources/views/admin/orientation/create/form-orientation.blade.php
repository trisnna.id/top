<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        <span class="required">Activity</span>
    </label>
    <input type="text" class="form-control" name="name" id="name" placeholder="Activity"
        value="{{ $orientation->name ?? null }}">
</div>
<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        <span>Venue</span>
    </label>
    <input type="text" class="form-control" name="venue" id="venue" placeholder="Venue"
        value="{{ $orientation->venue ?? null }}">
</div>
<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        <span>Livestream Link</span>
    </label>
    <input type="text" class="form-control" name="livestream" id="livestream" placeholder="Livestream Link"
        value="{{ $orientation->livestream ?? null }}">
</div>
<div class="d-flex flex-column fv-row mb-5">
    <div class="row">
        <div class="col-md-6 fv-row fv-plugins-icon-container">
            <label class="fw-bold form-label mb-2">
                <span class="required">Start Date of Activity</span>
            </label>
            <input type="date" class="form-control" name="start_date" id="start_date"
                value="{{ $orientation->start_date ?? null }}">
        </div>
        <div class="col-md-6 fv-row fv-plugins-icon-container">
            <label class="fw-bold form-label mb-2">
                <span class="required">End Date of Activity</span>
            </label>
            <input type="date" class="form-control" name="end_date" id="end_date"
                value="{{ $orientation->end_date ?? null }}">
        </div>
    </div>
</div>
<div class="d-flex flex-column fv-row mb-5">
    <div class="row">
        <div class="col-md-6 fv-row fv-plugins-icon-container">
            <label class="fw-bold form-label mb-2">
                <span class="required">Start Time of Activity</span>
            </label>
            <input type="time" class="form-control" name="start_time" id="start_time"
                value="{{ $orientation->start_time ?? null }}">
        </div>
        <div class="col-md-6 fv-row fv-plugins-icon-container">
            <label class="fw-bold form-label mb-2">
                <span class="required">End Time of Activity</span>
            </label>
            <input type="time" class="form-control" name="end_time" id="end_time"
                value="{{ $orientation->end_time ?? null }}">
        </div>
    </div>
</div>
<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        Speakers/Presenter Name
    </label>
    <input type="text" class="form-control" name="presenter" id="presenter" placeholder="Speakers/Presenter Name"
        value="{{ $orientation->presenter ?? null }}">
</div>
<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        Agenda
    </label>
    <textarea class="form-control" name="agenda" id="agenda" placeholder="Agenda">{!! !empty($orientation ?? null) ? json_decode($orientation->agenda_encode) : null !!}</textarea>
</div>
<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        Description
    </label>
    <textarea class="form-control" name="synopsis" id="synopsis" placeholder="Description">{!! !empty($orientation ?? null) ? json_decode($orientation->synopsis_encode) : null !!}</textarea>
</div>

<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        Notes
    </label>
    <textarea class="form-control" name="notes" id="notes" placeholder="Notes" maxlength="150">{!! $orientation->notes ?? null !!}</textarea>
</div>
<div class="d-flex flex-column fv-row mb-5">
    <label class="fw-bold form-label mb-2">
        <span class="required">Thumbnail</span>
    </label>
    <div class="fv-row col-12 mb-5">
        <div class="image-input image-input-outline image-placeholder" data-kt-image-input="true"
            style="background-image: url({{ url('https://dummyimage.com/600x400/000/fff') }})">
            <div class="image-input-wrapper w-300px h-200px image-placeholder"
                style="background-image: url('{{ !empty($orientation ?? null) ? $orientation->getFirstMediaUrl('thumbnail') : url(setting('file.thumbnail_fallback_url')) }}')">
            </div>
            <label class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="change" data-bs-toggle="tooltip" data-bs-dismiss="click"
                title="Change Thumbnail">
                <i class="bi bi-pencil-fill fs-7"></i>
                <input class="" type="file" name="thumbnail"
                    accept="{{ settingExtensionFormat('jpg jpeg png webp') }}" />
                <input class="" type="hidden" name="logo_remove" />
                <input type="hidden" name="thumbnail_hex">
            </label>
            <span class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="cancel" data-bs-toggle="tooltip" data-bs-dismiss="click"
                title="Cancel Thumbnail">
                <i class="bi bi-x fs-2"></i>
            </span>
            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove Thumbnail">
                <i class="bi bi-x fs-2"></i>
            </span>
        </div>
        <div class="form-text text-black">Allowed file types:
            {{ settingExtensionFormat('jpg jpeg png webp', '') }}. Max size:
            {{ settingExtensionFormat('2', '') }} MB</div>
        <x-inputs.error :name="__('thumbnail_hex')" />
        <div class="modal fade" tabindex="-1" id="cropper-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Crop Thumbnail</h5>
                    </div>
                    <div class="modal-body" id="cropper-content">
                        <img id="cropper-image" style="max-width: 100%; display: block;" width="100%">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="cropper-save">Save
                            changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script src="{{ asset(mix('js/modules/orientation.min.js')) }}"></script>
    @endpush
</div>
