<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        Does the activity need an RSVP flow?
    </label>
    <div>
        <div class="form-check form-check-inline">
            <input readonly name="is_rsvp" class="form-check-input" type="radio" id="is_rsvp1" value="1">
            <label class="form-check-label" for="is_rsvp1">Yes</label>
        </div>
        <div class="form-check form-check-inline">
            <input readonly name="is_rsvp" class="form-check-input" type="radio" id="is_rsvp2" checked value="0">
            <label class="form-check-label" for="is_rsvp2">No</label>
        </div>
    </div>
    <div class="form-text text-black">RSVP option to be default as NO for activity type 'compulsory core'.</div>
</div>
<div id="isRsvpContainer"></div>