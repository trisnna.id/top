<div class="row">
    <label class="col-md-2 fw-bold form-label mb-2">
        Attendance Point
    </label>
    <div class="col-md-10">: <span data-model="point_attendance">{{ $orientation->point_attendance ?? (setting('orientation.point_attendance') ?? 0) }}</span></div>
    <input type="hidden" name="point_attendance" data-default="{{ $orientation->point_attendance ?? (setting('orientation.point_attendance') ?? 0) }}" value="{{ $orientation->point_attendance ?? (setting('orientation.point_attendance') ?? 0) }}">
</div>
<div class="row">
    <label class="col-md-2 fw-bold form-label mb-2">
        Rating Point
    </label>
    <div class="col-md-10">: <span data-model="point_rating">{{ $orientation->point_rating ?? (setting('orientation.point_rating') ?? 0) }}</span></div>
    <input type="hidden" name="point_rating" data-default="{{ $orientation->point_rating ?? (setting('orientation.point_rating') ?? 0) }}" value="{{ $orientation->point_rating ?? (setting('orientation.point_rating') ?? 0) }}">
</div>