<div class="d-flex flex-column mb-5 fv-row">
    <select name="attachment" class="form-select form-select2" id="attachment" data-hide-search="true"
        data-allow-clear="true" data-placeholder="-- Please Select --" multiple="multiple">
        <option></option>
        <option data-accept="{{settingExtensionFormat(setting('file.orientation_attachment_extension'))}}" data-accept-info="{{settingExtensionFormat(setting('file.orientation_attachment_extension'),'')}}" data-size="{{settingExtensionFormat(setting('file.orientation_attachment_size'),'')}}" value="files">Files</option>
        <option value="links">Links</option>
    </select>
</div>
<div id="attachmentContainer"></div>
@if(!empty($attachmentDt ?? null))
    <x-swal.upsert :id="'upsertLink'" :swalSettings="[
        'title' => 'Link',
    ]">
        @method('PUT')
        @include('admin.orientation.create.form-orientation-attachment-link', ['id' => 'upsertLink'])
        <x-slot name="didOpen">
            modules.orientation.upsertLink('form#upsertLink',data);
        </x-slot>
    </x-swal.upsert>
    {!! $attachmentDt->html()->table() !!}
    @push('script')
        {!! $attachmentDt->generateMinifyScripts() !!}
    @endpush
@endif