<div class="row mb-5">
    <div class="col-md-6 fv-row fv-plugins-icon-container">
        <label class="required fw-semibold mb-2">Activity Type</label>
        {{ Form::select('orientation_type_id', $options['orientation_type'], 1, [
            'id' => 'orientation_type_id',
            'class' => 'form-select form-select2',
            'data-placeholder' => '-- Please Select --',
            'data-search' => false,
        ]) }}
    </div>
    <div class="col-md-6 fv-row fv-plugins-icon-container">
        <label class="required fw-semibold mb-2">Intake</label>
        <input type="hidden" name="filter_all[intake]" value="0">
        {{ Form::select('filters[intake][]', [(count($options['student_intake_editable']) > 0 ? 'All' : 'Please check under settings') => $options['student_intake_editable']], null, [
            'id' => 'filter_intake',
            'multiple',
            'class' => 'form-select swal2-select2-multiple-optgroup',
            'data-total' => $count = count($options['student_intake_editable']),
            'data-placeholder' => $count > 0 ? '-- Please Select --' : '-- Nothing to Select --',
            'data-input-all' => 'intake',
            'data-allow-clear' => 'true',
        ]) }}
    </div>
</div>
<div class="row mb-5">
    <div class="col-md-6 fv-row fv-plugins-icon-container">
        <label class="required fw-semibold mb-2">Faculty</label>
        <input type="hidden" name="filter_all[faculty]" value="1">
        {{ Form::select(
            'filters[faculty][]',
            [(count($options['student_faculty_editable']) > 0 ? 'All' : 'Please check under settings') => $options['student_faculty_editable']],
            null,
            [
                'id' => 'filter_faculty',
                'multiple',
                'class' => 'form-select swal2-select2-multiple-optgroup',
                'data-total' => $count = count($options['student_faculty_editable']),
                'data-placeholder' => $count > 0 ? '-- Please Select --' : '-- Nothing to Select --',
                'data-input-all' => 'faculty',
                'data-allow-clear' => 'true',
            ],
        ) }}
    </div>
    <div class="col-md-6 fv-row fv-plugins-icon-container">
        <label class="required fw-semibold mb-2">School</label>
        <input type="hidden" name="filter_all[school]" value="1">
        {{ Form::select(
            'filters[school][]',
            [(count($options['student_school_editable']) > 0 ? 'All' : 'Please check under settings') => $options['student_school_editable']],
            null,
            [
                'id' => 'filter_school',
                'multiple',
                'class' => 'form-select swal2-select2-multiple-optgroup',
                'data-total' => $count = count($options['student_school_editable']),
                'data-placeholder' => $count > 0 ? '-- Please Select --' : '-- Nothing to Select --',
                'data-input-all' => 'school',
                'data-allow-clear' => 'true',
            ],
        ) }}
    </div>
</div>
<div class="row mb-5">
    <div class="col-md-6 fv-row fv-plugins-icon-container">
        <label class="required fw-semibold mb-2">Programmes</label>
        <input type="hidden" name="filter_all[programme]" value="1">
        {{ Form::select(
            'filters[programme][]',
            [(count($options['student_school_editable']) > 0 ? 'All' : 'Please check under settings') => $options['student_programme_editable']],
            null,
            [
                'id' => 'filter_programme',
                'multiple',
                'class' => 'form-select swal2-select2-multiple-optgroup',
                'data-total' => $count = count($options['student_programme_editable']),
                'data-placeholder' => $count > 0 ? '-- Please Select --' : '-- Nothing to Select --',
                'data-input-all' => 'programme',
                'data-allow-clear' => 'true',
            ],
        ) }}
    </div>
    <div class="col-md-6 fv-row fv-plugins-icon-container">
        <label class="required fw-semibold mb-2">Mobility Programme</label>
        <input type="hidden" name="filter_all[mobility]" value="1">
        {{ Form::select(
            'filters[mobility][]',
            [(count($options['student_school_editable']) > 0 ? 'All' : 'Please check under settings') => $options['student_mobility_editable']],
            null,
            [
                'id' => 'filter_mobility',
                'multiple',
                'class' => 'form-select swal2-select2-multiple-optgroup',
                'data-total' => $count = count($options['student_mobility_editable']),
                'data-placeholder' => $count > 0 ? '-- Please Select --' : '-- Nothing to Select --',
                'data-input-all' => 'mobility',
                'data-allow-clear' => 'true',
            ],
        ) }}
    </div>
</div>
<div class="row mb-5">
    <div class="col-md-6 fv-row fv-plugins-icon-container">
        <label class="required fw-semibold mb-2">Student Level</label>
        <input type="hidden" name="filter_all[level_study]" value="0">
        {{ Form::select('filters[level_study][]', [(count($options['student_school_editable']) > 0 ? 'All' : 'Please check under settings') => $options['student_level_study_editable']], null, [
            'id' => 'filter_level_study',
            'multiple',
            'class' => 'form-select swal2-select2-multiple-optgroup',
            'data-total' => $count = count($options['student_level_study_editable']),
            'data-placeholder' => $count > 0 ? '-- Please Select --' : '-- Nothing to Select --',
            'data-input-all' => 'level_study',
            'data-allow-clear' => 'true',
        ]) }}
    </div>
    <div class="col-md-6 fv-row fv-plugins-icon-container">
        <label class="required fw-semibold mb-2">Number of Students</label>
        <input type="text" class="form-control bg-white" name="total_student" id="total_student"
            placeholder="Auto Calculate" disabled readonly>
        <div class="form-text text-black">Number of students based on intake, faculty, school, programme, mobility and
            student level.</div>
    </div>
</div>
<div class="row mb-5">
    <div class="col-md-6 d-flex flex-column mb-5 fv-row">
        <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
            {{ Form::checkbox('is_academic', true, $orientation->is_academic ?? true, ['id' => 'is_academic', 'class' => 'form-check-input']) }}
            <span class="form-check-label">
                This is an Academic Activity
            </span>
        </label>
    </div>
</div>
