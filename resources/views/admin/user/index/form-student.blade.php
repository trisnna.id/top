<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span class="required">Email</span>
    </label>
    <input type="text" class="form-control" name="email" id="email" placeholder="Email">
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span>Name</span>
    </label>
    <input type="text" class="form-control" name="name" id="id_number" placeholder="Name">
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="fw-bold form-label mb-2">
        <span class="required">Role</span>
    </label>
    <select class="form-select swal2-select2" id="role_name" name="role_name" data-hide-search="true"
        data-placeholder="-- Please Select --" required>
        <option></option>
        @foreach ($roleStudents as $role)
            <option value="{{ $role->name }}">{{ $role->title }}</option>
        @endforeach
    </select>
</div>
<div class="d-flex flex-column mb-5 fv-row">
    <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
        <input id="is_active" class="form-check-input" type="checkbox" value="true" name="is_active" checked>
        <span class="fw-bold form-check-label">
            Make it Active?
        </span>
    </label>
</div>