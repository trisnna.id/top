<div class="card-body">
    <div class="d-flex flex-stack flex-wrap mb-5">
        <div class="d-flex flex-row">
            <div class="me-2">
                <button class="btn btn-icon btn-black" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                    <i class="fa-solid fa-bars fs-3"></i>
                </button>
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3"
                    data-kt-menu="true" style="">
                    <div class="menu-item px-3">
                        <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Bulk Action</div>
                    </div>
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3 upsert-swal-popup"
                            data-swal="{{ json_encode([
                                'id' => 'bulkActiveStudent',
                                'axios' => [
                                    'url' => route('admin.users.update-bulk-active'),
                                    'method' => 'post',
                                ],
                            ]) }}">Active
                            Selected</a>
                        @include('admin.user.index.swal-bulk-active-student')
                    </div>
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3 upsert-swal-popup"
                            data-swal="{{ json_encode([
                                'id' => 'bulkInactiveStudent',
                                'axios' => [
                                    'url' => route('admin.users.update-bulk-inactive'),
                                    'method' => 'post',
                                ],
                            ]) }}">Inactive
                            Selected</a>
                        @include('admin.user.index.swal-bulk-inactive-student')
                    </div>

                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3 upsert-swal-popup"
                            data-swal="{{ json_encode([
                                'id' => 'bulkUpdateRoleStudent',
                                'axios' => [
                                    'url' => route('admin.users.update-bulk-role'),
                                    'method' => 'post',
                                ],
                            ]) }}">Update Role Selected</a>
                        @include('admin.user.index.swal-bulk-update-role-student')
                    </div>

                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3 checkbox-reset" data-table="studentDt">Reset Selected</a>
                    </div>
                </div>
            </div>
            <x-datatables.search id="studentDt" placeholder="Search Student" />
        </div>
    </div>
    <x-swal.upsert :id="'upsertStudent'" :swalSettings="['title' => 'Student']">
        @method('PUT')
        @include('admin.user.index.form-student', ['id' => 'upsertStudent'])
        <x-slot name="didOpen">
            modules.user.admin.index.swalUpdateAdmin('form#upsertStudent',data);
        </x-slot>
    </x-swal.upsert>
    <div id="studentDtScrollbarX" class="table-scrollbar-x" data-parentEl="#studentDt_wrapper .table-responsive">
        <div class="scrollbar-x">&nbsp;</div>
    </div>
    {!! $studentDt->html()->table() !!}
    <div id="studentDtScrollbarXEnd"></div>
    @push('script')dtx.initOption('studentDt');{!! $studentDt->generateMinifyScripts() !!}@endpush
</div>