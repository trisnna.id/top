<div class="card-body">
    <div class="d-flex flex-stack flex-wrap mb-5">
        <div class="d-flex flex-row">
            <div class="me-2">
                <button class="btn btn-icon btn-black" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                    <i class="fa-solid fa-bars fs-3"></i>
                </button>
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3"
                    data-kt-menu="true" style="">
                    <div class="menu-item px-3">
                        <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Bulk Action</div>
                    </div>
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3 upsert-swal-popup"
                            data-swal="{{ json_encode([
                                'id' => 'bulkActiveUser',
                                'axios' => [
                                    'url' => route('admin.users.update-bulk-active'),
                                    'method' => 'post',
                                ],
                            ]) }}">Active
                            Selected</a>
                        @include('admin.user.index.swal-bulk-active-user')
                    </div>
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3 upsert-swal-popup"
                            data-swal="{{ json_encode([
                                'id' => 'bulkInactiveUser',
                                'axios' => [
                                    'url' => route('admin.users.update-bulk-inactive'),
                                    'method' => 'post',
                                ],
                            ]) }}">Inactive
                            Selected</a>
                        @include('admin.user.index.swal-bulk-inactive-user')
                    </div>
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3 upsert-swal-popup"
                            data-swal="{{ json_encode([
                                'id' => 'bulkUpdateRoleUser',
                                'axios' => [
                                    'url' => route('admin.users.update-bulk-role'),
                                    'method' => 'post',
                                ],
                            ]) }}">Update Role Selected</a>
                        @include('admin.user.index.swal-bulk-update-role-user')
                    </div>
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3 checkbox-reset" data-table="userDt">Reset Selected</a>
                    </div>
                </div>
            </div>
            <x-datatables.search id="userDt" placeholder="Search Admin / Staff" />
        </div>
        <div class="d-flex justify-content-end">
            @if (config('services.taylors.ldap.enabled'))
                <button class="btn fw-bold btn-black upsert-swal-popup"
                    data-swal="{{ json_encode([
                        'id' => 'storeAdmin',
                        'axios' => [
                            'url' => route('admin.users.store-admin'),
                            'method' => 'post',
                        ],
                    ]) }}">
                    <i class="fas fa-plus fa-sm"></i>
                    Add Admin / Staff
                </button>
                @include('admin.user.index.swal-store-admin')
            @endif
            @include('admin.user.index.swal-update-admin')
        </div>
    </div>
    <div id="userDtScrollbarX" class="table-scrollbar-x" data-parentEl="#userDt_wrapper .table-responsive">
        <div class="scrollbar-x">&nbsp;</div>
    </div>
    {!! $userDt->html()->table() !!}
    <div id="userDtScrollbarXEnd"></div>
    @push('script')dtx.initOption('userDt');{!! $userDt->generateMinifyScripts() !!}@endpush
</div>
