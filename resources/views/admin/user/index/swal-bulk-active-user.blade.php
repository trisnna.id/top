<x-swal.upsert :id="'bulkActiveUser'" :swalSettings="[
    'title' => 'Are you sure to ACTIVE selected records?',
    'icon' => 'warning',
]">
    <input type="hidden" name="checkbox">
    <p class="text-center">
        You select&nbsp;<span id="selectedCount"></span>&nbsp;record(s).<br />
        You can change it back later!
    </p>
    <x-slot name="didOpen">
        dtx.swalInputCheckbox('form#bulkActiveUser','userDt');
    </x-slot>
</x-swal.upsert>
