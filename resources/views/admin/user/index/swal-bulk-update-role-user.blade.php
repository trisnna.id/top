<x-swal.upsert :id="'bulkUpdateRoleUser'" :swalSettings="[
    'title' => 'Update Role Selected',
]">
    <input type="hidden" name="checkbox">
    <p class="text-center">
        You select&nbsp;<span id="selectedCount"></span>&nbsp;record(s).<br />
    </p>
    <div class="d-flex flex-column fv-row">
        <label class="fw-bold form-label mb-2">
            <span class="required">Role</span>
        </label>
        <select class="form-select swal2-select2" id="role_name" name="role_name" data-hide-search="true"
            data-placeholder="-- Please Select --" required>
            <option></option>
            @foreach ($roleUsers as $role)
                <option value="{{ $role->name }}">{{ $role->title }}</option>
            @endforeach
        </select>
    </div>
    <x-slot name="didOpen">
        dtx.swalInputCheckbox('form#bulkUpdateRoleUser','userDt');
    </x-slot>
</x-swal.upsert>
