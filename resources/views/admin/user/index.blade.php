<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <x-share.breadcrumb :title="__('description.title.management', ['x' => 'User'])" :current="'User Management ?>'" />
    </x-slot>
    @php
        $navItems = [
            [
                'aClass' => 'active',
                'name' => 'admin-staff',
                'tabClass' => 'show active',
                'title' => 'Admin / Staff',
                'iconImg' => getConstant('IconConstant', 'icon_img', 'STAFF'),
                'search' => 'Search',
                'addButton' => 'Add Admin / Staff',
                'color' => getConstant('IconConstant', 'color', 'STAFF')['class'],
                'data' => [
                    'include1' => 'admin.user.index.tab-user',
                ],
            ],
            [
                'aClass' => '',
                'name' => 'student',
                'tabClass' => '',
                'title' => 'Student',
                'iconImg' => getConstant('IconConstant', 'icon_img', 'STUDENT'),
                'search' => 'Search',
                'addButton' => 'Add Student',
                'color' => getConstant('IconConstant', 'color', 'STUDENT')['class'],
                'data' => [
                    'include1' => 'admin.user.index.tab-student',
                ],
            ],
        ];
    @endphp
    <ul class="nav nav-pills nav-pills-custom d-flex flex-center mb-3">
        @foreach ($navItems as $key => $navItem)
            <li class="nav-item me-3 me-lg-6 mb-3">
                <a class="nav-link btn btn-outline btn-flex btn-color-muted btn-active-color-primary flex-column w-150px h-85px {{ $navItem['aClass'] }} overflow-hidden bg-white pt-5 pb-2"
                    id="tab_link_{{ $navItem['name'] }}" data-bs-toggle="pill" href="#tab_{{ $navItem['name'] }}">
                    <img class="w-35px mb-1" src="{{ $navItem['iconImg'] ?? null }}">
                    <span class="nav-text fw-bold fs-6 lh-1 text-gray-800">{{ $navItem['title'] }}</span>
                    <span class="bullet-custom position-absolute w-100 h-4px bg-red bottom-0"></span>
                </a>
            </li>
        @endforeach
    </ul>
    <div id="kt_app_content_container" class="app-container container-xxl">
        <div class="tab-content">
            @foreach ($navItems as $key => $navItem)
                <div class="tab-pane fade {{ $navItem['tabClass'] }}" id="tab_{{ $navItem['name'] }}" role="tabpanel">
                    <div class="card">
                        @include($navItem['data']['include1'], ['navItem' => $navItem])
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</x-layouts.app>
