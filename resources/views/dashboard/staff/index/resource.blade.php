<div class="row g-5 g-xl-10 mb-5 mb-sm-10">
    <!--begin::Col-->
    <div class="col-sm-5">
        <!--begin::Card widget 19-->
        <div class="card card-flush h-lg-100">
            <!--begin::Header-->
            <div class="card-header pt-5">
                <!--begin::Title-->
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-dark text-uppercase">Resources Summary</span>
                </h3>
                <!--end::Title-->
            </div>
            <!--end::Header-->
            <!--begin::Card body-->
            <div class="card-body pt-6">
                <!--begin::Row-->
                <div id="kt_apexcharts_1" style="height: 350px;"></div>
                @push('script')
                    var element = document.getElementById('kt_apexcharts_1');

                    var height = parseInt(KTUtil.css(element, 'height'));
                    var labelColor = KTUtil.getCssVariableValue('--kt-gray-500');
                    var borderColor = KTUtil.getCssVariableValue('--kt-gray-200');
                    var baseColor = KTUtil.getCssVariableValue('--kt-primary');
                    var secondaryColor = KTUtil.getCssVariableValue('--kt-gray-300');
                    
                    if (!element) {
                        console.log(element);
                    }
                    
                    var options = {
                        series: [44, 55, 41],
                        chart: {
                            type: 'donut',
                        },
                        dataLabels: {
                            formatter: function (val, opts) {
                                return opts.w.config.series[opts.seriesIndex]
                            },
                          },
                        labels: ['Videos', 'Files', 'Links'],
                        legend: {
                            position: 'bottom',
                        },
                        plotOptions: {
                            pie: {
                              donut: {
                                labels: {
                                  show: true,
                                  name: {
                                    show: true,
                                    fontSize: '22px',
                                    fontFamily: 'Rubik',
                                    color: '#dfsda',
                                    offsetY: -10
                                  },
                                  value: {
                                    show: true,
                                    fontSize: '16px',
                                    fontFamily: 'Helvetica, Arial, sans-serif',
                                    color: undefined,
                                    offsetY: 16,
                                    formatter: function (val) {
                                      return val
                                    }
                                  },
                                  total: {
                                    show: true,
                                    label: 'Total',
                                    color: '#373d3f',
                                    formatter: function (w) {
                                      return w.globals.seriesTotals.reduce((a, b) => {
                                        return a + b
                                      }, 0)
                                    }
                                  }
                                }
                              }
                            }
                          },
                        responsive: [{
                            breakpoint: 480,
                            options: {
                            chart: {
                                width: 200
                            },
                            legend: {
                                position: 'bottom'
                            }
                            }
                        }]
                      };
                    
                    var chart = new ApexCharts(element, options);
                    chart.render();
                @endpush
                <!--end::Row-->
            </div>
            <!--end::Card body-->
        </div>
        <!--end::Card widget 19-->
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-sm-7">

        <div class="card card-flush border-0 h-xl-100" data-theme="light" style="background-color: #000000">
            <!--begin::Header-->
            <div class="card-header pt-2">
                <!--begin::Title-->
                <h3 class="card-title">
                    <span class="text-white fs-3 fw-bold me-2">Orientation Activities</span>
                </h3>
                <!--end::Title-->
                <!--begin::Toolbar-->
            </div>
            <!--end::Header-->
            <!--begin::Body-->
            <div class="card-body d-flex justify-content-between flex-column pt-1 px-0 pb-0">
                <!--begin::Wrapper-->
                <div class="d-flex flex-wrap px-9 mb-5">
                    <!--begin::Stat-->
                    <div class="rounded min-w-125px py-3 px-4 my-1 me-6" style="border: 1px dashed rgba(255, 255, 255, 0.15)">
                        <!--begin::Number-->
                        <div class="d-flex align-items-center">
                            <div class="text-white fs-2 fw-bold counted" data-kt-countup="true" data-kt-countup-value="4368" data-kt-countup-prefix="$" data-kt-initialized="1">20</div>
                            &nbsp;<span class="badge badge-success">Active</span>
                        </div>
                        <!--end::Number-->
                        <!--begin::Label-->
                        <div class="fw-semibold fs-6 text-white">Compulsory Core Activity</div>
                        <!--end::Label-->
                    </div>
                    <!--end::Stat-->
                    <!--begin::Stat-->
                    <div class="rounded min-w-125px py-3 px-4 my-1" style="border: 1px dashed rgba(255, 255, 255, 0.15)">
                        <!--begin::Number-->
                        <div class="d-flex align-items-center">
                            <div class="text-white fs-2 fw-bold counted" data-kt-countup="true" data-kt-countup-value="120,000" data-kt-initialized="1">40</div>
                            &nbsp;<span class="badge badge-success">Active</span>
                        </div>
                        <!--end::Number-->
                        <!--begin::Label-->
                        <div class="fw-semibold fs-6 text-white">Complementary Activity</div>
                        <!--end::Label-->
                    </div>
                    <!--end::Stat-->
                </div>
                <!--end::Wrapper-->
                <!--begin::Chart-->
                <div id="kt_card_widget_activity_chart" data-kt-chart-color="primary" style="height: 300px"></div>
                @push('script-ready')
                    var element = document.getElementById("kt_card_widget_activity_chart");
            
                    if (!element) {
                        console.log(element);
                    }
            
                    var color = element.getAttribute('data-kt-chart-color');
                    
                    var height = parseInt(KTUtil.css(element, 'height'));
                    var labelColor = KTUtil.getCssVariableValue('--kt-gray-500');         
                    var baseColor = KTUtil.isHexColor(color) ? color : KTUtil.getCssVariableValue('--kt-' + color);
                    var secondaryColor = KTUtil.getCssVariableValue('--kt-gray-300');        
            
                    var options = {
                        series: [{
                            name: 'Compulsory Core Activity',
                            data: [44, 55, 41, 64, 22, 43, 21],
                            margin: {
                                left: 5,
                                right: 5
                            }  
                        }, {
                            name: 'Complementary Activity',
                            data: [53, 32, 33, 52, 13, 44, 32],
                            margin: {
                                left: 5,
                                right: 5
                            }  
                        }],
                        chart: {
                            fontFamily: 'inherit',
                            type: 'bar',
                            height: height,
                            toolbar: {
                                show: false
                            },
                            sparkline: {
                                enabled: true
                            }
                        },
                        plotOptions: {
                            bar: {
                                horizontal: false,
                                columnWidth: ['35%'],
                                borderRadius: 6
                            }
                        },
                        legend: {
                            show: false
                        },
                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            show: true,
                            width: 4,
                            colors: ['transparent']
                        },
                        xaxis: {                
                            axisBorder: {
                                show: false,
                            },
                            axisTicks: {
                                show: false
                            },
                            labels: {
                                show: false,
                                style: {
                                    colors: labelColor,
                                    fontSize: '12px'
                                }
                            },               
                            crosshairs: {
                                show: false
                            }
                        },
                        yaxis: {
                            labels: {
                                show: false,
                                style: {
                                    colors: labelColor,
                                    fontSize: '12px'
                                }
                            }
                        },
                        fill: {
                            type: 'solid'
                        },
                        states: {
                            normal: {
                                filter: {
                                    type: 'none',
                                    value: 0
                                }
                            },
                            hover: {
                                filter: {
                                    type: 'none',
                                    value: 0
                                }
                            },
                            active: {
                                allowMultipleDataPointsSelection: false,
                                filter: {
                                    type: 'none',
                                    value: 0
                                }
                            }
                        },
                        tooltip: {
                            style: {
                                fontSize: '12px'
                            },
                            x: {
                                formatter: function (val) {
                                    return "Feb: " + val
                                }
                            },
                            y: {
                                formatter: function (val) {
                                    return val
                                }
                            }
                        },
                        colors: [baseColor, secondaryColor],
                        grid: {
                            borderColor: false,
                            strokeDashArray: 4,
                            yaxis: {
                                lines: {
                                    show: true
                                }
                            },
                            padding: {
                                top: 10,
                                left: 25,
                                right: 25     
                            }               
                        }
                    };
            
                    // Set timeout to properly get the parent elements width
                    var chart = new ApexCharts(element, options);
                    setTimeout(function() {
                        chart.render();   
                    }, 300);  
                @endpush

                <!--end::Chart-->
            </div>
            <!--end::Body-->
        </div>
    </div>
    <!--end::Col-->
</div>