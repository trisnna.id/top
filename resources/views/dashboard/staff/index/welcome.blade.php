<div class="row gy-5 g-xl-10">
    <!--begin::Col-->
    <div class="col-sm-6 col-md-3 mb-md-10">
        <!--begin::Engage widget 3-->
        <div class="card h-md-100" style="background-color: var(--app-header-color) !important;">
            <!--begin::Body-->
            <div class="card-body d-flex flex-column pt-13 pb-14">
                <!--begin::Heading-->
                <div class="m-0">
                    <!--begin::Title-->
                    <h1 class="fw-semibold text-white text-center lh-lg mb-9">
                        <span class="fw-bolder">
                            Welcome
                        </span>
                    </h1>
                    <h2 class="fw-semibold text-white text-center lh-lg mb-9">
                        <span class="fw-bolder">
                            {{auth()->user()->name}}
                        </span>
                    </h2>
                    <!--end::Title-->
                    <!--begin::Illustration-->
                    <!--end::Illustration-->
                </div>
                <!--end::Heading-->
                <!--begin::Links-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Engage widget 3-->
    </div>
    <div class="col-sm-2 col-md-3 mb-md-10">
        <!--begin::Card widget 2-->
        <div class="card h-lg-100">
            <!--begin::Body-->
            <div class="card-body d-flex justify-content-between align-items-start flex-column">
                <!--begin::Icon-->
                <div class="m-0">
                    <i class="{{getConstant('IconConstant','icon_class','PRE_ORIENTATION')}} text-{{getConstant('IconConstant','color','PRE_ORIENTATION')['class']}} fs-5x"></i>
                </div>
                <!--end::Icon-->
                <!--begin::Section-->
                <div class="d-flex flex-column my-7">
                    <!--begin::Number-->
                    <span id="counter_pre_orientation" class="fw-semibold fs-3x text-gray-800 lh-1 ls-n2 text-orange">0</span>
                    @push('script')
                        new countUp.CountUp('counter_pre_orientation', 2000).start();
                    @endpush
                    <!--end::Number-->
                    <!--begin::Follower-->
                    <div class="m-0">
                        <span class="fw-semibold fs-3">Pre-orientation<br>Activities</span>
                    </div>
                    <!--end::Follower-->
                </div>
                <!--end::Section-->
                <!--begin::Badge-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card widget 2-->
    </div>
    <div class="col-sm-2 col-md-3 mb-md-10">
        <!--begin::Card widget 2-->
        <div class="card h-lg-100">
            <div class="card-header pt-5 border-0">
                <!--begin::Title-->
                <h3 class="card-title align-items-start flex-column">
                    <div class="m-0">
                        <i class="{{getConstant('IconConstant','icon_class','STUDENT')}} text-{{getConstant('IconConstant','color','STUDENT')['class']}} fs-5x"></i>
                    </div>
                </h3>
                <!--end::Title-->
                <!--begin::Toolbar-->
                <div class="card-toolbar">
                    <!--begin::Menu-->
                    <button class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-overflow="true">
                        <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                        <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="currentColor"></rect>
                                <rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                <rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                <rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </button>
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-550px w-md-625px" data-kt-menu="true" style="">
                        <!--begin::Menu item-->
                        <div class="menu-item px-3">
                            <div class="menu-content fs-6 text-dark fw-bold px-3 py-4">Filter Options</div>
                        </div>
                        <!--end::Menu item-->
                        <!--begin::Menu separator-->
                        <div class="separator mb-3 opacity-75"></div>
                        <!--end::Menu separator-->
                        <div class="px-7 py-5">
                            <div class="row mb-5">
                                <div class="col fv-row fv-plugins-icon-container">
                                    <label class="form-label fw-semibold">Intakes:</label>
                                    <select name="activeStudentfilterIntakes" class="form-select form-select2" id="activeStudentfilterIntakes" data-hide-search="true" data-allow-clear="true" data-placeholder="-- Please Select / All --">
                                        <option></option>
                                        <option selected value="1">202203</option>
                                        <option value="2">202103</option>
                                    </select>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                <button type="reset" class="btn btn-sm btn-light btn-active-light-primary me-2"
                                    data-kt-menu-dismiss="true">Reset</button>
                                <button type="submit" class="btn btn-sm btn-primary"
                                    data-kt-menu-dismiss="true">Apply</button>
                            </div>
                        </div>
                        <!--end::Menu item-->
                    </div>
                    <!--begin::Menu 2-->
                    
                    <!--end::Menu 2-->
                    <!--end::Menu-->
                </div>
                <!--end::Toolbar-->
            </div>
            <div class="card-body d-flex justify-content-between align-items-start flex-column pt-0">
                <!--begin::Icon-->
                
                <!--end::Icon-->
                <!--begin::Section-->
                <div class="d-flex flex-column my-7">
                    <!--begin::Number-->
                    <span id="counter_new" class="fw-semibold fs-3x text-gray-800 lh-1 ls-n2 text-orange">0</span>
                    @push('script')
                        new countUp.CountUp('counter_new', 7234).start();
                    @endpush
                    <!--end::Number-->
                    <!--begin::Follower-->
                    <div class="m-0">
                        <span class="fw-semibold fs-3">Active<br>Students</span>
                    </div>
                    <!--end::Follower-->
                </div>
                <!--end::Section-->
                <!--begin::Badge-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card widget 2-->
    </div>
    <div class="col-sm-2 col-md-3 mb-md-10">
        <!--begin::Card widget 2-->
        <div class="card h-lg-100">
            <!--begin::Body-->
            <div class="card-body d-flex justify-content-between align-items-start flex-column">
                <!--begin::Icon-->
                <div class="m-0">
                    <i class="{{getConstant('IconConstant','icon_class','ANNOUNCEMENT')}} text-{{getConstant('IconConstant','color','ANNOUNCEMENT')['class']}} fs-5x"></i>
                </div>
                <!--end::Icon-->
                <!--begin::Section-->
                <div class="d-flex flex-column my-7">
                    <span id="counter_announcement" class="fw-semibold fs-3x text-gray-800 lh-1 ls-n2 text-orange">0</span>
                    @push('script')
                        new countUp.CountUp('counter_announcement', 150).start();
                    @endpush
                    <!--end::Number-->
                    <!--begin::Follower-->
                    <div class="m-0">
                        <span class="fw-semibold fs-3">News<br>Announcements</span>
                    </div>
                    <!--end::Follower-->
                </div>
                <!--end::Section-->
                <!--begin::Badge-->
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card widget 2-->
    </div>
    <!--end::Col-->
    <!--begin::Col-->
</div>
