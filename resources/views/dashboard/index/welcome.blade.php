<div class="mb-0 dashboard-welcome">
    <!--begin::Wrapper-->
    <div class="bgi-no-repeat bgi-size-contain bgi-position-x-center bgi-position-y-bottom landing-dark-bg cover"
        style="background-image: url(media/taylors/bg-taylors3.svg)">
        <!--begin::Landing hero-->
        <div class="d-flex flex-column flex-center w-100 min-h-350px min-h-lg-500px px-9">
            <!--begin::Heading-->
            <div class="text-center mb-5 mb-lg-10 py-10 py-lg-20">
                <!--begin::Title-->
                <h1 class="text-white lh-base fw-bold fs-2x fs-lg-3x mb-15">
                    Welcome message<br />with
                    <span
                        style="background: linear-gradient(to right, #12CE5D 0%, #FFD80C 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">
                        <span id="kt_landing_hero_text">TOP's Highlights</span>
                    </span>
                </h1>
                <div class="fs-2 fw-semibold text-white text-center mb-10">
                    <span class="fs-1 lh-1 text-muted">“</span>When you care about your topic, you’ll write about it in a
                    <br />
                    <span class="text-muted me-1">more powerful</span>, emotionally expressive way
                    <span class="fs-1 lh-1 text-muted">“</span>
                </div>
                <!--end::Title-->
                <!--begin::Action-->
                <div class="container">
                    <!--begin::Heading-->
                    <div class="tns tns-default">
                        <!--begin::Wrapper-->
                        <div data-tns="true" data-tns-loop="true" data-tns-swipe-angle="false" data-tns-speed="2000" data-tns-autoplay="true" data-tns-autoplay-timeout="18000" data-tns-controls="true" data-tns-nav="false" data-tns-items="1" data-tns-center="false" data-tns-dots="false" data-tns-prev-button="#kt_team_slider_prev" data-tns-next-button="#kt_team_slider_next" data-tns-responsive="{1200: {items: 2}, 992: {items: 1}}">
                            <!--begin::Item-->
                            <div class="text-center px-5 pt-5 pt-lg-10 px-lg-10">
                                <a class="d-block overlay" data-fslightbox="lightbox-basic" href="https://dummyimage.com/1350x560/000/fff">
                                    <!--begin::Image-->
                                    <img src="https://dummyimage.com/720x300/000/fff" class="card-rounded shadow mw-100" alt="" />
                                    <!--end::Image-->
                                    <!--begin::Action-->
                                    <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                        <i class="bi bi-eye-fill text-white fs-3x"></i>
                                    </div>
                                    <!--end::Action-->
                                </a>
                            </div>
                            <!--end::Item-->
                            <!--begin::Item-->
                            <div class="text-center px-5 pt-5 pt-lg-10 px-lg-10">
                                <a class="d-block overlay" data-fslightbox="lightbox-basic" href="https://dummyimage.com/1350x560/ff0000/fff">
                                    <!--begin::Image-->
                                    <img src="https://dummyimage.com/720x300/ff0000/fff" class="card-rounded shadow mw-100" alt="" />
                                    <!--end::Image-->
                                    <!--begin::Action-->
                                    <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                        <i class="bi bi-eye-fill text-white fs-3x"></i>
                                    </div>
                                    <!--end::Action-->
                                </a>
                            </div>
                            <!--end::Item-->
                            <!--begin::Item-->
                            <div class="text-center px-5 pt-5 pt-lg-10 px-lg-10">
                                <a class="d-block overlay" data-fslightbox="lightbox-basic" href="https://dummyimage.com/1350x560/0000ff/fff">
                                    <!--begin::Image-->
                                    <img src="https://dummyimage.com/720x300/0000ff/fff" class="card-rounded shadow mw-100" alt="" />
                                    <!--end::Image-->
                                    <!--begin::Action-->
                                    <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                        <i class="bi bi-eye-fill text-white fs-3x"></i>
                                    </div>
                                    <!--end::Action-->
                                </a>
                            </div>
                            <!--end::Item-->
                            <!--begin::Item-->
                            <div class="text-center px-5 pt-5 pt-lg-10 px-lg-10">
                                <a class="d-block overlay" data-fslightbox="lightbox-basic" href="https://dummyimage.com/1350x560/00ff00/fff">
                                    <!--begin::Image-->
                                    <img src="https://dummyimage.com/720x300/00ff00/fff" class="card-rounded shadow mw-100" alt="" />
                                    <!--end::Image-->
                                    <!--begin::Action-->
                                    <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                        <i class="bi bi-eye-fill text-white fs-3x"></i>
                                    </div>
                                    <!--end::Action-->
                                </a>
                            </div>
                            <!--end::Item-->
                            <!--begin::Item-->
                            <div class="text-center px-5 pt-5 pt-lg-10 px-lg-10">
                                <a class="d-block overlay" data-fslightbox="lightbox-basic" href="https://dummyimage.com/1350x560/f000f0/fff">
                                    <!--begin::Image-->
                                    <img src="https://dummyimage.com/720x300/f000f0/fff" class="card-rounded shadow mw-100" alt="" />
                                    <!--end::Image-->
                                    <!--begin::Action-->
                                    <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                        <i class="bi bi-eye-fill text-white fs-3x"></i>
                                    </div>
                                    <!--end::Action-->
                                </a>
                            </div>
                        </div>
                        <!--end::Wrapper-->
                        <!--begin::Button-->
                        <button class="btn btn-icon btn-active-color-primary" id="kt_team_slider_prev">
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr074.svg-->
                            <span class="svg-icon svg-icon-3x">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11.2657 11.4343L15.45 7.25C15.8642 6.83579 15.8642 6.16421 15.45 5.75C15.0358 5.33579 14.3642 5.33579 13.95 5.75L8.40712 11.2929C8.01659 11.6834 8.01659 12.3166 8.40712 12.7071L13.95 18.25C14.3642 18.6642 15.0358 18.6642 15.45 18.25C15.8642 17.8358 15.8642 17.1642 15.45 16.75L11.2657 12.5657C10.9533 12.2533 10.9533 11.7467 11.2657 11.4343Z" fill="currentColor" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </button>
                        <!--end::Button-->
                        <!--begin::Button-->
                        <button class="btn btn-icon btn-active-color-primary" id="kt_team_slider_next">
                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr071.svg-->
                            <span class="svg-icon svg-icon-3x">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12.6343 12.5657L8.45001 16.75C8.0358 17.1642 8.0358 17.8358 8.45001 18.25C8.86423 18.6642 9.5358 18.6642 9.95001 18.25L15.4929 12.7071C15.8834 12.3166 15.8834 11.6834 15.4929 11.2929L9.95001 5.75C9.5358 5.33579 8.86423 5.33579 8.45001 5.75C8.0358 6.16421 8.0358 6.83579 8.45001 7.25L12.6343 11.4343C12.9467 11.7467 12.9467 12.2533 12.6343 12.5657Z" fill="currentColor" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </button>
                        <!--end::Button-->
                    </div>
                    <!--end::Slider-->
                </div>
                <!--end::Action-->
            </div>
            <!--end::Heading-->
            <!--begin::Clients-->
            <div class="d-flex flex-center flex-wrap position-relative px-5">
                <!--begin::Client-->
                <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Fujifilm">
                    <img src="{{ asset('media/svg/brand-logos/fujifilm.svg') }}" class="mh-30px mh-lg-40px"
                        alt="" />
                </div>
                <!--end::Client-->
                <!--begin::Client-->
                <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Vodafone">
                    <img src="{{ asset('media/svg/brand-logos/vodafone.svg') }}" class="mh-30px mh-lg-40px"
                        alt="" />
                </div>
                <!--end::Client-->
                <!--begin::Client-->
                <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="KPMG International">
                    <img src="{{ asset('media/svg/brand-logos/kpmg.svg') }}" class="mh-30px mh-lg-40px"
                        alt="" />
                </div>
                <!--end::Client-->
                <!--begin::Client-->
                <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Nasa">
                    <img src="{{ asset('media/svg/brand-logos/nasa.svg') }}" class="mh-30px mh-lg-40px"
                        alt="" />
                </div>
                <!--end::Client-->
                <!--begin::Client-->
                <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Aspnetzero">
                    <img src="{{ asset('media/svg/brand-logos/aspnetzero.svg') }}" class="mh-30px mh-lg-40px"
                        alt="" />
                </div>
                <!--end::Client-->
                <!--begin::Client-->
                <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="AON - Empower Results">
                    <img src="{{ asset('media/svg/brand-logos/aon.svg') }}" class="mh-30px mh-lg-40px"
                        alt="" />
                </div>
                <!--end::Client-->
                <!--begin::Client-->
                <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Hewlett-Packard">
                    <img src="{{ asset('media/svg/brand-logos/hp-3.svg') }}" class="mh-30px mh-lg-40px"
                        alt="" />
                </div>
                <!--end::Client-->
                <!--begin::Client-->
                <div class="d-flex flex-center m-3 m-md-6" data-bs-toggle="tooltip" title="Truman">
                    <img src="{{ asset('media/svg/brand-logos/truman.svg') }}" class="mh-30px mh-lg-40px"
                        alt="" />
                </div>
                <!--end::Client-->
            </div>
            <!--end::Clients-->
        </div>
        <!--end::Landing hero-->
    </div>
    <!--end::Wrapper-->
    <!--begin::Curve bottom-->
    <div class="landing-curve landing-dark-color mb-5">
        <svg viewBox="15 12 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M0 11C3.93573 11.3356 7.85984 11.6689 11.7725 12H1488.16C1492.1 11.6689 1496.04 11.3356 1500 11V12H1488.16C913.668 60.3476 586.282 60.6117 11.7725 12H0V11Z"
                fill="currentColor"></path>
        </svg>
    </div>
    <!--end::Curve bottom-->
</div>