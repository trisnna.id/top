<div class="mt-sm-n20">
    <!--begin::Curve top-->
    <div class="landing-curve landing-dark-color z-index-n1">
        <svg viewBox="15 -1 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M1 48C4.93573 47.6644 8.85984 47.3311 12.7725 47H1489.16C1493.1 47.3311 1497.04 47.6644 1501 48V47H1489.16C914.668 -1.34764 587.282 -1.61174 12.7725 47H1V48Z"
                fill="currentColor"></path>
        </svg>
    </div>
    <!--end::Curve top-->
    <!--begin::Wrapper-->
    <div class="pb-15 pt-18 landing-dark-bg">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Heading-->
            <div class="text-center mt-15 mb-18" id="achievements" data-kt-scroll-offset="{default: 100, lg: 150}">
                <!--begin::Title-->
                <h3 class="fs-2hx text-white fw-bold mb-5">Resource Bank</h3>
                <!--end::Title-->
                <!--begin::Description-->
                <div class="fs-5 text-gray-700 fw-bold">Save thousands to millions of bucks by using single tool
                    <br />for different amazing and great useful admin
                </div>
                <!--end::Description-->
            </div>
            <!--end::Heading-->
            <!--begin::Statistics-->
            <div class="d-flex flex-center">
                <!--begin::Items-->
                <div class="d-flex flex-wrap flex-center justify-content-lg-between mb-15 mx-auto w-xl-900px">
                    <!--begin::Item-->
                    <x-swal.upsert :id="$instance['faq']['swal']['id']" :swalSettings="$instance['faq']['swal']['settings']">
                        {!! $faqDt->html()->table(['class' => 'thead-none w-100 tbody-row td-d-block']) !!}
                        <x-slot name="didOpen">
                            {!! $faqDt->html()->generateScripts() !!}
                        </x-slot>
                    </x-swal.upsert>
                    <x-swal.upsert :id="$instance['resource']['swal']['id']" :swalSettings="$instance['resource']['swal']['settings']">
                        {!! $resourceDt->html()->table(['class' => 'table table-bordered table-striped align-middle table-row-dashed fs-6 gy-5 th-text-uppercase th-fw-bold w-100']) !!}
                        <x-slot name="didOpen">
                            {!! $resourceDt->html()->generateScripts() !!}
                        </x-slot>
                    </x-swal.upsert>



                    <div class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain upsert-swal-popup"
                        style="background-image: url('media/svg/misc/octagon.svg')" data-swal="{{ json_encode($instance['resource']['swal']) }}">
                        <!--begin::Symbol-->
                        <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->

                        <!--begin::Svg Icon | path: C:/wamp64/www/keenthemes/core/html/src/media/icons/duotune/files/fil012.svg-->
                        <span class="svg-icon svg-icon-2tx svg-icon-white mb-3">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.3" d="M10 4H21C21.6 4 22 4.4 22 5V7H10V4Z" fill="currentColor" />
                                <path
                                    d="M9.2 3H3C2.4 3 2 3.4 2 4V19C2 19.6 2.4 20 3 20H21C21.6 20 22 19.6 22 19V7C22 6.4 21.6 6 21 6H12L10.4 3.60001C10.2 3.20001 9.7 3 9.2 3Z"
                                    fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <!--end::Svg Icon-->
                        <!--end::Symbol-->
                        <!--begin::Info-->
                        <div class="mb-0 text-center">
                            <!--begin::Value-->
                            <div class="fs-lg-2hx fs-2x fw-bold text-white d-flex flex-center">
                                <div class="min-w-70px">Resources</div>
                            </div>
                            <!--end::Value-->
                            <!--begin::Label-->
                            <span class="text-gray-600 fw-semibold fs-5 lh-0">Download the material<br />you need
                                here</span>
                            <!--end::Label-->
                        </div>
                        <!--end::Info-->
                    </div>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <div class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain upsert-swal-popup"
                        style="background-image: url('media/svg/misc/octagon.svg')" data-swal="{{ json_encode($instance['faq']['swal']) }}">
                        <!--begin::Symbol-->
                        <!--begin::Svg Icon | path: icons/duotune/graphs/gra008.svg-->

                        <!--begin::Svg Icon | path: C:/wamp64/www/keenthemes/core/html/src/media/icons/duotune/communication/com012.svg-->
                        <span class="svg-icon svg-icon-2tx svg-icon-white mb-3">
                            <svg width="24" height="24"
                                viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.3"
                                    d="M20 3H4C2.89543 3 2 3.89543 2 5V16C2 17.1046 2.89543 18 4 18H4.5C5.05228 18 5.5 18.4477 5.5 19V21.5052C5.5 22.1441 6.21212 22.5253 6.74376 22.1708L11.4885 19.0077C12.4741 18.3506 13.6321 18 14.8167 18H20C21.1046 18 22 17.1046 22 16V5C22 3.89543 21.1046 3 20 3Z"
                                    fill="currentColor" />
                                <rect x="6" y="12" width="7" height="2" rx="1"
                                    fill="currentColor" />
                                <rect x="6" y="7" width="12" height="2" rx="1"
                                    fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <!--end::Svg Icon-->
                        <!--end::Symbol-->
                        <!--begin::Info-->
                        <div class="mb-0 text-center">
                            <!--begin::Value-->
                            <div class="fs-lg-2hx fs-2x fw-bold text-white d-flex flex-center">
                                <div class="min-w-70px">FAQ</div>
                            </div>
                            <!--end::Value-->
                            <!--begin::Label-->
                            <span class="text-gray-600 fw-semibold fs-5 lh-0">Frequently Asked<br />Question</span>
                            <!--end::Label-->
                        </div>
                        <!--end::Info-->
                    </div>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <div class="d-flex flex-column flex-center h-200px w-200px h-lg-250px w-lg-250px m-3 bgi-no-repeat bgi-position-center bgi-size-contain"
                        style="background-image: url('media/svg/misc/octagon.svg')">
                        <!--begin::Symbol-->
                        <!--begin::Svg Icon | path: icons/duotune/ecommerce/ecm002.svg-->

                        <!--begin::Svg Icon | path: C:/wamp64/www/keenthemes/core/html/src/media/icons/duotune/communication/com002.svg-->
                        <span class="svg-icon svg-icon-2tx svg-icon-white mb-3"><svg width="24" height="24"
                                viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path opacity="0.3"
                                    d="M21 18H3C2.4 18 2 17.6 2 17V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V17C22 17.6 21.6 18 21 18Z"
                                    fill="currentColor" />
                                <path
                                    d="M11.4 13.5C11.8 13.8 12.3 13.8 12.6 13.5L21.6 6.30005C21.4 6.10005 21.2 6 20.9 6H2.99998C2.69998 6 2.49999 6.10005 2.29999 6.30005L11.4 13.5Z"
                                    fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <!--end::Svg Icon-->
                        <!--end::Symbol-->
                        <!--begin::Info-->
                        <div class="mb-0 text-center">
                            <!--begin::Value-->
                            <div class="fs-lg-2hx fs-2x fw-bold text-white d-flex flex-center">
                                <div class="min-w-70px">Inquiry</div>
                            </div>
                            <!--end::Value-->
                            <!--begin::Label-->
                            <span class="text-gray-600 fw-semibold fs-5 lh-0">Send To Us<br>Your Question</span>
                            <!--end::Label-->
                        </div>
                        <!--end::Info-->
                    </div>
                    <!--end::Item-->
                </div>
                <!--end::Items-->
            </div>
            <!--end::Statistics-->
            <!--begin::Testimonial-->
            <div class="fs-2 fw-semibold text-muted text-center mb-3">
                <span class="fs-1 lh-1 text-gray-700">“</span>When you care about your topic, you’ll write about it in a
                <br />
                <span class="text-gray-700 me-1">more powerful</span>, emotionally expressive way
                <span class="fs-1 lh-1 text-gray-700">“</span>
            </div>
            <!--end::Testimonial-->
            <!--begin::Author-->
            <div class="fs-2 fw-semibold text-muted text-center">
                <a href="../../demo1/dist/account/security.html" class="link-primary fs-4 fw-bold">Marcus Levy,</a>
                <span class="fs-4 fw-bold text-gray-600">KeenThemes CEO</span>
            </div>
            <!--end::Author-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Wrapper-->
    <!--begin::Curve bottom-->
    <div class="landing-curve landing-dark-color">
        <svg viewBox="15 12 1470 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M0 11C3.93573 11.3356 7.85984 11.6689 11.7725 12H1488.16C1492.1 11.6689 1496.04 11.3356 1500 11V12H1488.16C913.668 60.3476 586.282 60.6117 11.7725 12H0V11Z"
                fill="currentColor"></path>
        </svg>
    </div>
    <!--end::Curve bottom-->
</div>
