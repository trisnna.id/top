<div class="row g-5 g-xl-10">
    
    <div class="col-md-8">
        
        <div class="card card-flush h-xl-100">
            
            <div class="card-header pt-7">
                
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-dark">Video/Live stream</span>
                    <span class="text-gray-400 mt-1 fw-semibold fs-6">Updated 37 minutes ago</span>
                </h3>
                
                
                <div class="card-toolbar">
                    <a href="../../demo1/dist/account/statements.html" class="btn btn-sm btn-light">History</a>
                </div>
                
            </div>
            
            
            <div class="card-body pt-7">
                
                <div class="row g-5 g-xl-9 mb-5 mb-xl-9">
                    
                    <div class="col-sm-6 mb-6 mb-sm-0">
                        
                        <div class="m-0">
                            
                            <div class="card-rounded position-relative mb-5 h-200px">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-sm-6 mb-6 mb-sm-0">
                        
                        <div class="m-0">
                            
                            <div class="card-rounded position-relative mb-5 h-200px">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/_e1doZGEzV0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-sm-6 mb-6 mb-sm-0">
                        
                        <div class="m-0">
                            
                            <div class="card-rounded position-relative mb-5 h-200px">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/TFPZQrDZFbc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-sm-6 mb-6 mb-sm-0">
                        
                        <div class="m-0">
                            
                            <div class="card-rounded position-relative mb-5 h-200px">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/SYuhimEx8wA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-sm-6 mb-6 mb-sm-0">
                        
                        <div class="m-0">
                            
                            <div class="card-rounded position-relative mb-5 h-200px">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/bQTMhJqgawg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
                
            </div>
            
        </div>
        
    </div>
    
    
    <div class="col-md-4">
        <div class="card h-xl-100" id="kt_list_widget_24">
            
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-gray-800">My Info</span>
                </h3>
                <x-swal.upsert :id="$instance['stepbystep']['swal']['id']" :swalSettings="$instance['stepbystep']['swal']['settings']">
                    <div class="stepper stepper-pills" id="kt_stepper_example_basic">
                        
                        <div class="stepper-nav flex-center flex-wrap mb-10">
                            
                            <div class="stepper-item mx-8 my-4 current" data-kt-stepper-element="nav">
                                
                                <div class="stepper-wrapper d-flex align-items-center">
                                    
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">1</span>
                                    </div>
                                    
                                    
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 1</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    
                                </div>
                                
                                
                                <div class="stepper-line h-40px"></div>
                                
                            </div>
                            
                            
                            <div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav">
                                
                                <div class="stepper-wrapper d-flex align-items-center">
                                    
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">2</span>
                                    </div>
                                    
                                    
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 2</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    
                                </div>
                                
                                
                                <div class="stepper-line h-40px"></div>
                                
                            </div>
                            
                            
                            <div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav">
                                
                                <div class="stepper-wrapper d-flex align-items-center">
                                    
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">3</span>
                                    </div>
                                    
                                    
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 3</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    
                                </div>
                                
                                
                                <div class="stepper-line h-40px"></div>
                                
                            </div>
                            
                            
                            <div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav">
                                
                                <div class="stepper-wrapper d-flex align-items-center">
                                    
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">4</span>
                                    </div>
                                    
                                    
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 4</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    
                                </div>
                                
                                
                                <div class="stepper-line h-40px"></div>
                                
                            </div>
                            
                        </div>
                        
                        
                        <form class="form w-lg-500px mx-auto" novalidate="novalidate">
                            
                            <div class="mb-5">
                                
                                <div class="flex-column current" data-kt-stepper-element="content">
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label">Example Label 1</label>
                                        
                                        
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        
                                    </div>
                                    
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label">Example Label 2</label>
                                        
                                        
                                        <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                        
                                    </div>
                                    
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label">Example Label 3</label>
                                        
                                        
                                        <label class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" checked="checked" value="1" />
                                            <span class="form-check-label">Switch</span>
                                        </label>
                                        
                                    </div>
                                    
                                </div>
                                
                                
                                <div class="flex-column" data-kt-stepper-element="content">
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label">Example Label 1</label>
                                        
                                        
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        
                                    </div>
                                    
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label">Example Label 2</label>
                                        
                                        
                                        <textarea class="form-control form-control-solid" rows="3" name="input2" placeholder=""></textarea>
                                        
                                    </div>
                                    
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label">Example Label 3</label>
                                        
                                        
                                        <label class="form-check form-check-custom form-check-solid">
                                            <input class="form-check-input" checked="checked" type="checkbox" value="1" />
                                            <span class="form-check-label">Checkbox</span>
                                        </label>
                                        
                                    </div>
                                    
                                </div>
                                
                                
                                <div class="flex-column" data-kt-stepper-element="content">
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label d-flex align-items-center">
                                            <span class="required">Input 1</span>
                                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"></i>
                                        </label>
                                        
                                        
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        
                                    </div>
                                    
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label">Input 2</label>
                                        
                                        
                                        <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                        
                                    </div>
                                    
                                </div>
                                
                                
                                <div class="flex-column" data-kt-stepper-element="content">
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label d-flex align-items-center">
                                            <span class="required">Input 1</span>
                                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"></i>
                                        </label>
                                        
                                        
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        
                                    </div>
                                    
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label">Input 2</label>
                                        
                                        
                                        <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                        
                                    </div>
                                    
                                    
                                    <div class="fv-row mb-10">
                                        
                                        <label class="form-label">Input 3</label>
                                        
                                        
                                        <input type="text" class="form-control form-control-solid" name="input3" placeholder="" value="" />
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                            
                            <div class="d-flex flex-stack">
                                
                                <div class="me-2">
                                    <button type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous">Back</button>
                                </div>
                                
                                
                                <div>
                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="submit">
                                        <span class="indicator-label">Submit</span>
                                        <span class="indicator-progress">Please wait...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="next">Continue</button>
                                </div>
                                
                            </div>
                            
                        </form>
                        
                    </div>
                    <x-slot name="didOpen">
                        KTGeneralStepperDemos.init();
                    </x-slot>
                </x-swal.upsert>
                <div class="card-toolbar">
                    <a href="#" class="btn btn-sm btn-light upsert-swal-popup" data-swal="{{ json_encode($instance['stepbystep']['swal']) }}">Step-by-step</a>
                </div>
                <div class="d-flex align-items-center w-100">
                    
                    <div class="d-flex align-items-center flex-shrink-0 w-100">
                        
                        <span class="fs-7 text-gray-700 fw-bold pe-4 d-none d-md-block">Progress:</span>
                        
                        <div class="progress w-100 h-25px bg-light-success">
                            <div class="progress-bar rounded bg-success fs-7 fw-bold" role="progressbar" style="width: 72%;" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100">72%</div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
            
            <div class="card-body pt-0">
                
                <div class="separator separator-dashed my-5"></div>
                
                
                <div class="d-flex flex-stack mb-3">
                    
                    <div class="symbol symbol-circle symbol-60px me-4">
                        <div class="symbol-label fs-3 bg-light-{{auth()->user()->getMeta('color')}} text-{{auth()->user()->getMeta('color')}}">{{upperSubstr(auth()->user()->name)}}</div>
                    </div>
                    
                    
                    <div class="d-flex align-items-center flex-row-fluid flex-wrap">
                        
                        <div class="flex-grow-1 me-2">
                            <span class="text-gray-800 text-hover-primary fs-5 fw-bolder">{{auth()->user()->name ?? 'Guest'}}</span>
                            <span class="text-black fw-semibold d-block fs-6">Bachelor of Biomedical Science (Honours)</span>
                        </div>
                        
                    </div>
                    
                </div>
                <span class="fw-semibold fs-6 mb-8 d-block">
                    I will Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ornare congue neque id congue. Nulla rutrum rutrum viverra.
                </span>
                
                
                <div class="separator separator-dashed my-5"></div>
                
                
                <div class="d-flex flex-stack">
                    <div class="d-flex align-items-center flex-row-fluid flex-wrap mb-2">
                        <div class="flex-grow-1 me-2">
                            <span class="text-gray-800 text-hover-primary fs-5 fw-bolder">School</span>
                            <span class="fw-semibold d-block fs-6">
                                <div class="bullet bullet-dot w-8px h-7px bg-success me-2"></div>School of Biosciences (SBS)
                            </span>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-stack">
                    <div class="d-flex align-items-center flex-row-fluid flex-wrap mb-2">
                        <div class="flex-grow-1 me-2">
                            <span class="text-gray-800 text-hover-primary fs-5 fw-bolder">Intake</span>
                            <span class="fw-semibold d-block fs-6">
                                <div class="bullet bullet-dot w-8px h-7px bg-success me-2"></div>202010
                            </span>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-stack">
                    <div class="d-flex align-items-center flex-row-fluid flex-wrap mb-2">
                        <div class="flex-grow-1 me-2">
                            <span class="text-gray-800 text-hover-primary fs-5 fw-bolder">Email</span>
                            <span class="fw-semibold d-block fs-6">
                                <div class="bullet bullet-dot w-8px h-7px bg-success me-2"></div>email@sd.taylors.edu.my / email@gmail.com
                            </span>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-stack">
                    <div class="d-flex align-items-center flex-row-fluid flex-wrap mb-2">
                        <div class="flex-grow-1 me-2">
                            <span class="text-gray-800 text-hover-primary fs-5 fw-bolder">Other Info from Step-by-step</span>
                            <span class="fw-semibold d-block fs-6">
                                <div class="bullet bullet-dot w-8px h-7px bg-success me-2"></div>Lorem ipsum dolor sit amet
                            </span>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-stack">
                    <div class="d-flex align-items-center flex-row-fluid flex-wrap mb-2">
                        <div class="flex-grow-1 me-2">
                            <span class="text-gray-800 text-hover-primary fs-5 fw-bolder">Other Info from Step-by-step</span>
                            <span class="fw-semibold d-block fs-6">
                                <div class="bullet bullet-dot w-8px h-7px bg-success me-2"></div>Lorem ipsum dolor sit amet
                            </span>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-stack">
                    <div class="d-flex align-items-center flex-row-fluid flex-wrap mb-2">
                        <div class="flex-grow-1 me-2">
                            <span class="text-gray-800 text-hover-primary fs-5 fw-bolder">Other Info from Step-by-step</span>
                            <span class="fw-semibold d-block fs-6">
                                <div class="bullet bullet-dot w-8px h-7px bg-success me-2"></div>Lorem ipsum dolor sit amet
                            </span>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-stack">
                    <div class="d-flex align-items-center flex-row-fluid flex-wrap mb-2">
                        <div class="flex-grow-1 me-2">
                            <span class="text-gray-800 text-hover-primary fs-5 fw-bolder">Other Info from Step-by-step</span>
                            <span class="fw-semibold d-block fs-6">
                                <div class="bullet bullet-dot w-8px h-7px bg-success me-2"></div>Lorem ipsum dolor sit amet
                            </span>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-stack">
                    <div class="d-flex align-items-center flex-row-fluid flex-wrap mb-2">
                        <div class="flex-grow-1 me-2">
                            <span class="text-gray-800 text-hover-primary fs-5 fw-bolder">Other Info from Step-by-step</span>
                            <span class="fw-semibold d-block fs-6">
                                <div class="bullet bullet-dot w-8px h-7px bg-success me-2"></div>Lorem ipsum dolor sit amet
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
