<x-layouts.app :title="$title">
    <div id="kt_app_content_container" class="app-container container-xxl z-index-2 mt-10">
        @include('dashboard.student.index')
    </div>
    @if ($student->pre_orientation_percentage >= 100 || setting('orientation.skip_completed_pre_orientation') || in_array($student->taylors_email, ['student1@top', 'student2@top', 'student3@top']))
        <div class="app-container container-xxl" id="calendar">
            <div class="px-8">
                <div class="d-flex align-items-center justify-content-between flex-wrap">
                    <span class="fs-1 fw-bold pe-2">
                        <h3 class="fw-bold fs-1 my-2 text-gray-800"
                            style="font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; ">
                            Orientation Calendar
                            <x-tooltip.basic title='tooltip-timetable-calendar-orientation-calendar'></x-tooltip.basic>
                        </h3>
                    </span>
                </div>
                @include('dashboard.index.timetable')
            </div>
        </div>
    @endif
    <div class="app-container container-xxl">
        <x-dashboard.quick-links></x-dashboard.quick-links>
    </div>

    {{-- <div id="fab-container" class="d-flex justify-content-center">
        <a href="#calendar" class="btn btn-red go-to-calendar">Go to orientation calendar</a>
    </div> --}}
    @push('styles')
        <style>
            .go-to-calendar {
                position: fixed;
                bottom: 30px;
                left: auto;
                right: auto;
                display: block;
            }

            #fab-container {
                position: fixed;
                bottom: 30px;
                left: 0;
                right: 0;
                margin: 0 auto;
                text-align: center;
                z-index: 999;
            }
        </style>
    @endpush
    @push('scripts')
        <script>
            function isInViewport(element) {
                const rect = element.getBoundingClientRect();
                return (
                    rect.left >= 0 &&
                    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) + ((window.innerHeight ||
                        document.documentElement.clientHeight) / 2) &&
                    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
                );
            }


            function toggleGoToCalendar() {
                const calendar = document.querySelector('#calendar');
                const goToCalendar = document.querySelector('.go-to-calendar');

                if (isInViewport(calendar)) {
                    goToCalendar.style.display = 'none';
                } else {
                    goToCalendar.style.display = 'block';
                }

                if (isInViewport(calendar.parentNode)) {
                    goToCalendar.style.display = 'none';
                }

                goToCalendar.addEventListener('click', (e) => {
                    e.preventDefault();
                    goToCalendar.style.display = 'none';
                    calendar.scrollIntoView({
                        behavior: 'smooth'
                    });
                });
            }

            function renderQuickLink() {
                $('span.quick-link').each(function(_i, el) {
                    $(el).click(function() {
                        const div = $(this).find('div');

                        const title = div.attr('data-title');
                        const children = div.children();

                        Swal.fire({
                            title: title,
                            html: children.length ? children : ''
                        })
                    })
                })
            }
            renderQuickLink()

            // window.addEventListener('scroll', function() {
            //     window.requestAnimationFrame(toggleGoToCalendar);
            // });
            // toggleGoToCalendar();
        </script>
    @endpush
</x-layouts.app>
