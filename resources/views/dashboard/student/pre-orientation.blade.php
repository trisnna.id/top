
{{-- <div class="d-flex align-items-center w-100">
    <div class="d-flex align-items-center flex-shrink-0 w-100">
        <span class="fs-7 text-gray-700 fw-bold pe-4 d-none d-md-block">Progress:</span>
        <div class="progress w-100 h-25px bg-light-success">
            <div class="progress-bar rounded bg-success fs-7 fw-bold" role="progressbar" style="width: 72%;"
                aria-valuenow="72" aria-valuemin="0" aria-valuemax="100">72%
            </div>
        </div>
    </div>
</div> --}}

<div class="d-flex flex-wrap flex-stack">
    <!--begin::Wrapper-->
    <div class="d-flex flex-column flex-grow-1 pe-8">
        <!--begin::Stats-->
        <div class="d-flex flex-wrap">
            <!--begin::Stat-->
            <div class="border border-gray-300 border-dashed rounded min-w-100px py-3 mb-3">
                <!--begin::Number-->
                <div class="d-flex align-items-center">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
                    <span class="svg-icon svg-icon-3 svg-icon-success me-2"><svg width="24" height="24"
                            viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="13" y="6" width="13" height="2"
                                rx="1" transform="rotate(90 13 6)" fill="currentColor"></rect>
                            <path
                                d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                                fill="currentColor"></path>
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <div class="fs-2 fw-bold counted" data-kt-countup="true" data-kt-countup-value="4500"
                        data-kt-countup-prefix="$" data-kt-initialized="1">$4,500</div>
                </div>
                <!--end::Number-->

                <!--begin::Label-->
                <div class="fw-semibold fs-6 text-gray-400">Earnings</div>
                <!--end::Label-->
            </div>
            <!--end::Stat-->

            <!--begin::Stat-->
            <div class="border border-gray-300 border-dashed rounded min-w-100px py-3 mb-3">
                <!--begin::Number-->
                <div class="d-flex align-items-center">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr065.svg-->
                    <span class="svg-icon svg-icon-3 svg-icon-danger me-2"><svg width="24" height="24"
                            viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="11" y="18" width="13" height="2"
                                rx="1" transform="rotate(-90 11 18)" fill="currentColor"></rect>
                            <path
                                d="M11.4343 15.4343L7.25 11.25C6.83579 10.8358 6.16421 10.8358 5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75L11.2929 18.2929C11.6834 18.6834 12.3166 18.6834 12.7071 18.2929L18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25C17.8358 10.8358 17.1642 10.8358 16.75 11.25L12.5657 15.4343C12.2533 15.7467 11.7467 15.7467 11.4343 15.4343Z"
                                fill="currentColor"></path>
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <div class="fs-2 fw-bold counted" data-kt-countup="true" data-kt-countup-value="80"
                        data-kt-initialized="1">80</div>
                </div>
                <!--end::Number-->

                <!--begin::Label-->
                <div class="fw-semibold fs-6 text-gray-400">Projects</div>
                <!--end::Label-->
            </div>
            <!--end::Stat-->

            <!--begin::Stat-->
            <div class="border border-gray-300 border-dashed rounded min-w-100px py-3 mb-3">
                <!--begin::Number-->
                <div class="d-flex align-items-center">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
                    <span class="svg-icon svg-icon-3 svg-icon-success me-2"><svg width="24" height="24"
                            viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="13" y="6" width="13" height="2"
                                rx="1" transform="rotate(90 13 6)" fill="currentColor"></rect>
                            <path
                                d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z"
                                fill="currentColor"></path>
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <div class="fs-2 fw-bold counted" data-kt-countup="true" data-kt-countup-value="60"
                        data-kt-countup-prefix="%" data-kt-initialized="1">%60</div>
                </div>
                <!--end::Number-->

                <!--begin::Label-->
                <div class="fw-semibold fs-6 text-gray-400">Success Rate</div>
                <!--end::Label-->
            </div>
            <!--end::Stat-->
        </div>
        <!--end::Stats-->
    </div>
    <!--end::Wrapper-->

    <!--begin::Progress-->
    <div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
        <div class="d-flex justify-content-between w-100 mt-auto mb-2">
            <span class="fw-semibold fs-6 text-gray-400">Profile Compleation</span>
            <span class="fw-bold fs-6">50%</span>
        </div>

        <div class="h-5px mx-3 w-100 bg-light mb-3">
            <div class="bg-success rounded h-5px" role="progressbar" style="width: 50%;" aria-valuenow="50"
                aria-valuemin="0" aria-valuemax="100"></div>
        </div>
    </div>
    <!--end::Progress-->
</div>
