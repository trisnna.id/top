<div id="faq-search">
    <div class="card card-shadow" style="background-color: transparent !important;">
        <div class="card-body">
            <h2 class="card-title" style="margin-bottom: 20px !important">
                <span class="card-label fw-bold text-dark fs-1"
                    style="font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; ">Frequently
                    Asked
                    Questions</span>

                <x-tooltip.basic title='Key in your questions and unravel the answers to your most pressing questions.'>
                </x-tooltip.basic>
            </h2>
            <div class="position-relative">
                <div id="searchFaq" class="input-group flex-nowrap">
                    <span class="input-group-text" id="addon-wrapping">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-search" viewBox="0 0 16 16">
                            <path
                                d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                        </svg>
                    </span>
                    <input id="faqDt_search" type="text" class="form-control" placeholder="Find your needs"
                        aria-label="Find your needs" aria-describedby="addon-wrapping" data-kt-search-element="input">

                </div>
                <div data-kt-search-element="content"
                    class="position-absolute top-100 z-index-2 menu menu-sub menu-sub-dropdown ps-7 pe-5 py-7"
                    style="left: 0;right: 0;">
                    <div data-kt-search-element="wrapper">
                        <div data-kt-search-element="results">
                            {!! $faqDt->html()->table() !!}
                            @push('script')
                                {!! $faqDt->html()->generateScripts() !!}
                            @endpush
                        </div>
                        <div class="d-flex justify-content-center pt-5">
                            <button data-kt-search-element="close" class="btn btn-sm btn-secondary me-3">Close</button>
                            <a href="{{ route('faqs.index') }}" class="btn btn-sm btn-red">See All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        // FAQ Search, on user press enter
        $('#faq-search input').on('keyup', function(e) {
            if (e.key === 'Enter' || e.keyCode === 13) {
                window.location.href = "{{ route('faqs.index') }}?q=" + $(this).val();
            }
        });
    </script>
@endpush
