<!--begin::User menu-->
<div class="card mb-5 mb-xl-8">
    <!--begin::Body-->
    <div class="card-body pt-0 px-0">
        <!--begin::Member-->
        <div class="d-flex flex-column text-center mb-9 px-9">
            {{-- <div class="symbol symbol-80px symbol-lg-150px mb-4">
                <img src="{{ asset('media/avatars/300-3.jpg') }}" class="" alt="" />
            </div> --}}
            <!--begin::Info-->
            {{-- <div class="text-center">
                <!--begin::Name-->
                <a href="{{ route('dashboard.index') }}"
                    class="text-gray-800 fw-bold text-hover-primary fs-4">{{ $user->profile->name }}</a>
                <!--end::Name-->
                <!--begin::Position-->
                <span class="text-muted d-block fw-semibold">{{ $user->profile->programme->name }}</span>
                <!--end::Position-->
            </div> --}}
        </div>
        <!--end::Member-->
        <!--begin::Row-->
        <div class="row px-9 mb-4">
            <!--begin::Col-->
            <div class="col-md-4 text-center">
                <div class="text-gray-800 fw-bold fs-3">
                    <span class="m-0" data-kt-countup="true" data-kt-countup-value="300">0</span>/
                    <span class="m-0" data-kt-countup="true" data-kt-countup-value="500">0</span>
                </div>
                <span class="text-gray-500 fs-8 d-block fw-bold">Pre-orientation</span>
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-md-4 text-center">
                <div class="text-gray-800 fw-bold fs-3">
                    <span class="m-0" data-kt-countup="true" data-kt-countup-value="400">0</span>/
                    <span class="m-0" data-kt-countup="true" data-kt-countup-value="500">0</span>
                </div>
                <span class="text-gray-500 fs-8 d-block fw-bold">Orientation</span>
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-md-4 text-center">
                <div class="text-gray-800 fw-bold fs-3">
                    <span class="m-0" data-kt-countup="true" data-kt-countup-value="500">0</span>/
                    <span class="m-0" data-kt-countup="true" data-kt-countup-value="500">0</span>
                </div>
                <span class="text-gray-500 fs-8 d-block fw-bold">Surveys/Others</span>
            </div>
            <!--end::Col-->
        </div>
        <!--end::Row-->
    </div>
    <!--end::Body-->
</div>
<!--end::User menu-->
