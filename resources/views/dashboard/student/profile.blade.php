@foreach ([
    'Student ID' => $user->profile->id_number,
    'Faculty Name' => $user->profile->faculty_name,
    'School Name' => '',
    'Intake Number' => $user->profile->intake_number,
    'Contact Number' => $user->profile->contact_number,
    'Programme Name' => $user->profile->programme->name,
    'Student Level' => '',
    'Taylor\'s Email' => $user->profile->taylors_email,
    'Personal Email' => $user->profile->personal_email,
    'Mentor Name' => $user->profile->mentor->name,
    'Campus' => '',
    'Nationality' => '',
    'Mobility' => '',
] as $label => $value)
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-4 fw-semibold text-muted">{{ $label }}</label>
        <!--end::Label-->

        <!--begin::Col-->
        <div class="col-lg-8">                    
            <span class="fw-bold fs-6 text-gray-800">{{ $value }}</span>                
        </div>
        <!--end::Col-->
    </div>
@endforeach
