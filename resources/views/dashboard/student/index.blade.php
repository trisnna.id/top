<div class="row g-5 g-xl-10 mb-md-0 mb-5">
    {{-- <div class="col-md-8 col-lg-8 col-xl-8 col-xxl-8 mb-md-5 mb-xl-10">
        @include('dashboard.student.banner')
        @include('dashboard.student.announcement')
        <x-announcement.carousel></x-announcement.carousel>
        @include('dashboard.student.resources')
    </div>
    <div class="col-md-4 col-lg-4 col-xl-4 col-xxl-4 mb-md-5 mb-xl-10">
        <div class="card card-shadow">
            <div class="card-header pt-7">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-red">Activities Points</span>
                </h3>
                <div class="card-toolbar">
                </div>
            </div>
            <!--end::Header-->

            <!--begin::Body-->
            <div class="card-body">
                <h4 class="align-items-start flex-column text-red">
                    <span class="fs-1">200</span> <span class="fs-5">Points</span>
                </h4>

                <div class="fw-bold mt-5">
                    My Rewards <i class="fa-solid fa-gift"></i>
                </div>
                <!--begin::Table container-->
                <div class="table-responsive">
                    <!--begin::Table-->
                    <table class="table-row-dashed my-0 table align-middle">
                        <tbody>
                            @foreach ([['STARBUCKS VOUCHER RM10', '=', '100 points'], ['FAMILYMART VOUCHER RM10', '=', '100 points'], ['CELEBRITY FITNESS 1-MONTH', '=', '200 points'], ['KFC VOUCHER RM10', '=', '100 points'], ['TNG EWALLET RM10', '=', '100 points']] as $item)
                                <tr class="hover-elevate-up parent-hover shadow-sm">
                                    <td class="">
                                        <div class="symbol symbol-50px">
                                            <img src="{{ asset('media/others/touchngo.png') }}" class=""
                                                alt="">
                                        </div>
                                    </td>
                                    <td class="">
                                        <div class="d-flex align-items-center text-gray-800">
                                            <figure>
                                                <blockquote>
                                                    <b>{{ $item[0] }}</b>
                                                </blockquote>
                                                <figcaption class="blockquote-footer">
                                                    <cite title="{{ $item[2] }}">{{ $item[2] }}</cite>
                                                </figcaption>
                                            </figure>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <a href="#" role="button" class="btn btn-light-danger btn-sm">
                                                <div class="d-flex align-item-center">
                                                    <span>Claim</span>
                                                </div>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="card card-shadow my-3">
            <div class="card-header border-0 pt-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-dark">TOP Points</span>
                </h3>
            </div>
            <div class="card-body pt-0">
                <div class="card card-bordered">
                    <div class="card-body">
                        <div id="preOrientationDonutChart"></div>
                    </div>
                </div>
                @include('dashboard.student.avatar')
            </div>
        </div>

        <div class="card card-shadow">
            <div class="card-header">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-dark">Pre-Orientation Tracking</span>
                    <span class="fw-semibold fs-6 mt-1 text-gray-400">Step-by-step</span>
                </h3>
            </div>
            <div class="card-body">
                <div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
                    <x-completion.pre-orientation-progress></x-completion.pre-orientation-progress>
                </div>

                <div class="separator separator-dashed my-5"></div>

                <div class="scroll-y me-n7 pe-7" style="height: 200px">
                    <x-pre-orientation.tracking></x-pre-orientation.tracking>
                </div>
            </div>
        </div>
    </div> --}}

    {{-- New dashboard --}}
    <div class="col-md-4 col-lg-4 col-xl-4 col-xxl-4 mb-md-8 mb-xl-10">
        <div id="pre-orientation-tracking">
            <div class="card card-shadow" style="background-color: transparent !important;">
                <div class="card-body">
                    <h2 class="card-title align-items-start flex-column" style="margin-bottom: 18px !important">
                        <span class="card-label fw-bold text-dark fs-1"
                            style="font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; ">Pre-Orientation
                            Progress</span>

                        <x-tooltip.basic
                            title='Check your Pre-Orientation progress here and unlock the next level after completing each checkpoint.'>
                        </x-tooltip.basic>
                    </h2>
                    <div class="scroll-y me-n7 pe-7">
                        <x-pre-orientation.tracking></x-pre-orientation.tracking>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-xl-8 col-xxl-8 mb-md-8 mb-xl-10">
        <div class="row">
            <div class="col-12">
                @include('dashboard.student.search')
            </div>
        </div>
        <div class="row">
            <div class="container mb-4">
                <div class="row">
                    <div class="col-md-6 mb-md-0 mb-4" style="min-height: 235px !important; max-height: 235px !important;">
                        <x-dashboard.attendance-chart></x-dashboard.attendance-chart>
                    </div>
                    <div class="col-md-6">
                        <x-dashboard.survey-chart></x-dashboard.survey-chart>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container py-2" style="min-height: 235px !important; max-height: 235px !important;">
                <div class="d-flex">
                    <div class="col-12">
                        <x-dashboard.achievement-stars></x-dashboard.achievement-stars>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row g-5 g-xl-10 mt-md-0 mb-2 mt-10">
    <div class="col-12 px-13">
        <x-announcement.carousel></x-announcement.carousel>
    </div>
</div>
