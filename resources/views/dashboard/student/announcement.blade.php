<div id="carouselContainer" style="position: relative;">
    <div id="kt_carousel_2_carousel" class="carousel carousel-custom slide" data-bs-ride="carousel" data-bs-interval="8000">
        <!--begin::Heading-->
        <div class="d-flex align-items-center justify-content-between flex-wrap">
            <!--begin::Label-->
            <span class="fs-4 fw-bold pe-2">
                <h3 class="text-gray-800 fw-bold my-2">
                    {{ __('Announcements') }}
                    <span class="fs-6 text-gray-400 fw-semibold ms-1">(6)</span>
                </h3>
            </span>
            <!--end::Label-->

            <!--begin::Carousel Indicators-->
            <ol class="p-0 m-0 carousel-indicators carousel-indicators-bullet">
                <li data-bs-target="#kt_carousel_2_carousel" data-bs-slide-to="0" class="ms-1 active"></li>
                <li data-bs-target="#kt_carousel_2_carousel" data-bs-slide-to="1" class="ms-1"></li>
            </ol>
            <!--end::Carousel Indicators-->
        </div>
        <!--end::Heading-->

        <!--begin::Carousel-->
        <div class="carousel-inner pt-8">
            <div class="carousel-item active">
                <div class="row g-6 mb-6 g-xl-9 mb-xl-9">
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
                <div class="row g-6 mb-6 g-xl-9 mb-xl-9">
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
            <div class="carousel-item">
                <div class="row g-6 mb-6 g-xl-9 mb-xl-9">
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
                <div class="row g-6 mb-6 g-xl-9 mb-xl-9">
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 col-xxl-4">
                        <div class="card mb-5 mb-xl-8">
                            <!--begin::Body-->
                            <div class="card-body pb-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-45px me-5">
                                            <img src="{{ asset('media/avatars/300-23.jpg') }}" alt="">
                                        </div>
                                        <!--end::Avatar-->

                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <a href="#" class="text-gray-900 text-hover-primary fs-6 fw-bold">
                                                {{ __('Title') }}
                                            </a>

                                            <span class="text-gray-400 fw-bold">
                                                {{ 'Student programme, Intake | Category' }}
                                            </span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>

                                <div class="d-flex align-items-center mb-5">
                                    <!--begin::User-->
                                    <div class="d-flex align-items-center flex-grow-1">
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <span class="text-gray-800 fw-bold">{{ __('Nationality, Campus') }}</span>
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Header-->

                                <!--begin::Post-->
                                <div class="mb-5">
                                    <!--begin::Text-->
                                    <p class="text-gray-800 fw-normal mb-5">
                                        Outlines keep you honest. They stop you from indulging in
                                        poorly thought-out metaphors about driving and keep you
                                        focused on the overall structure of your post
                                    </p>
                                    <!--end::Text-->
                                </div>
                                <!--end::Post-->
                            </div>
                            <!--end::Body-->
                        </div>
                    </div>
                    <!--end::Col-->
                </div>
            </div>
        </div>
        <!--end::Carousel-->
    </div>
    <a href="{{ route('announcement.index') }}" class="btn btn-sm btn-danger hover-elevate-up" style="position: absolute; bottom: 3%; right: 0;">See all</a>
</div>
