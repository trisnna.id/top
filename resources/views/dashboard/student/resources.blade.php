<div id="resources-carousel" class="carousel carousel-custom slide" data-bs-ride="carousel" data-bs-interval="8000">
    <div class="d-flex align-items-center justify-content-between flex-wrap">
        <span class="fs-4 fw-bold pe-2">
            <h3 class="text-gray-800 fw-bold my-2">
                {{ __('Resources') }}
                <span class="fs-6 text-gray-400 fw-semibold ms-1">(9)</span>
            </h3>
        </span>
        <ol class="p-0 m-0 carousel-indicators carousel-indicators-bullet">
            <li data-bs-target="#resources-carousel" data-bs-slide-to="0" class="ms-1 active"></li>
            <li data-bs-target="#resources-carousel" data-bs-slide-to="1" class="ms-1"></li>
            <li data-bs-target="#resources-carousel" data-bs-slide-to="2" class="ms-1"></li>
        </ol>
    </div>
    <div class="carousel-inner pt-8">
        <div class="carousel-item active">
            <div class="row g-6 mb-6 g-xl-9 mb-xl-9">
                <div class="col-md-4 col-xxl-4">
                    <!--begin::Card-->
                    <div class="card">
                        <div class="card-header" style="padding: 0 !important;">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="card-body">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, maxime possimus! Inventore, doloribus perspiciatis amet dolore, autem vel iste accusamus, voluptas iusto cupiditate ratione pariatur minus eligendi labore quae recusandae?
                        </div>
                        <div class="card-footer">
                            <p class="fs-6 text-gray-700">
                                <span class="fw-bold">Taylor's College Malaysia</span> on Jan 17 2023
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xxl-4">
                    <div class="card">
                        <div class="card-header" style="padding: 0 !important;">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="card-body">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, maxime possimus! Inventore, doloribus perspiciatis amet dolore, autem vel iste accusamus, voluptas iusto cupiditate ratione pariatur minus eligendi labore quae recusandae?
                        </div>
                        <div class="card-footer">
                            <p class="fs-6 text-gray-700">
                                <span class="fw-bold">Taylor's College Malaysia</span> on Jan 17 2023
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xxl-4">
                    <div class="card">
                        <div class="card-header" style="padding: 0 !important;">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="card-body">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, maxime possimus! Inventore, doloribus perspiciatis amet dolore, autem vel iste accusamus, voluptas iusto cupiditate ratione pariatur minus eligendi labore quae recusandae?
                        </div>
                        <div class="card-footer">
                            <p class="fs-6 text-gray-700">
                                <span class="fw-bold">Taylor's College Malaysia</span> on Jan 17 2023
                            </p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="carousel-item">
            <div class="row g-6 mb-6 g-xl-9 mb-xl-9">
                <div class="col-md-4 col-xxl-4">
                    <!--begin::Card-->
                    <div class="card">
                        <div class="card-header" style="padding: 0 !important;">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="card-body">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, maxime possimus! Inventore, doloribus perspiciatis amet dolore, autem vel iste accusamus, voluptas iusto cupiditate ratione pariatur minus eligendi labore quae recusandae?
                        </div>
                        <div class="card-footer">
                            <p class="fs-6 text-gray-700">
                                <span class="fw-bold">Taylor's College Malaysia</span> on Jan 17 2023
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xxl-4">
                    <div class="card">
                        <div class="card-header" style="padding: 0 !important;">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="card-body">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, maxime possimus! Inventore, doloribus perspiciatis amet dolore, autem vel iste accusamus, voluptas iusto cupiditate ratione pariatur minus eligendi labore quae recusandae?
                        </div>
                        <div class="card-footer">
                            <p class="fs-6 text-gray-700">
                                <span class="fw-bold">Taylor's College Malaysia</span> on Jan 17 2023
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xxl-4">
                    <div class="card">
                        <div class="card-header" style="padding: 0 !important;">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="card-body">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, maxime possimus! Inventore, doloribus perspiciatis amet dolore, autem vel iste accusamus, voluptas iusto cupiditate ratione pariatur minus eligendi labore quae recusandae?
                        </div>
                        <div class="card-footer">
                            <p class="fs-6 text-gray-700">
                                <span class="fw-bold">Taylor's College Malaysia</span> on Jan 17 2023
                            </p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="carousel-item">
            <div class="row g-6 mb-6 g-xl-9 mb-xl-9">
                <div class="col-md-4 col-xxl-4">
                    <!--begin::Card-->
                    <div class="card">
                        <div class="card-header" style="padding: 0 !important;">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="card-body">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, maxime possimus! Inventore, doloribus perspiciatis amet dolore, autem vel iste accusamus, voluptas iusto cupiditate ratione pariatur minus eligendi labore quae recusandae?
                        </div>
                        <div class="card-footer">
                            <p class="fs-6 text-gray-700">
                                <span class="fw-bold">Taylor's College Malaysia</span> on Jan 17 2023
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xxl-4">
                    <div class="card">
                        <div class="card-header" style="padding: 0 !important;">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="card-body">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, maxime possimus! Inventore, doloribus perspiciatis amet dolore, autem vel iste accusamus, voluptas iusto cupiditate ratione pariatur minus eligendi labore quae recusandae?
                        </div>
                        <div class="card-footer">
                            <p class="fs-6 text-gray-700">
                                <span class="fw-bold">Taylor's College Malaysia</span> on Jan 17 2023
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xxl-4">
                    <div class="card">
                        <div class="card-header" style="padding: 0 !important;">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/PAPPoANdeS0"
                                    title="YouTube video player" frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="card-body">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit, maxime possimus! Inventore, doloribus perspiciatis amet dolore, autem vel iste accusamus, voluptas iusto cupiditate ratione pariatur minus eligendi labore quae recusandae?
                        </div>
                        <div class="card-footer">
                            <p class="fs-6 text-gray-700">
                                <span class="fw-bold">Taylor's College Malaysia</span> on Jan 17 2023
                            </p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
