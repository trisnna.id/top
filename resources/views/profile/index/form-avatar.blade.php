<div class="mb-5 fv-row col-12 text-center">
    <div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url('{{ url(setting('file.profile_avatar_fallback_url')) }}')">
        <div class="image-input-wrapper w-125px h-125px" style="background-image: url('{{ !empty($user ?? null) ? $user->getFirstMediaUrl('avatar') : url(setting('file.profile_avatar_fallback_url')) }}')">
        </div>
        @if(request()->route()->getName() == "profile.index")
            <label class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="change" data-bs-toggle="tooltip" data-bs-dismiss="click" title="Change avatar">
                <i class="bi bi-pencil-fill fs-7"></i>
                <input class="avatar" type="file" name="avatar" accept="{{settingExtensionFormat(setting('file.profile_avatar_extension'))}}" />
                <input class="avatar" type="hidden" name="logo_remove" />
            </label>
            <span class="btn btn-icon btn-circle btn-color-muted btn-active-color-primary w-25px h-25px bg-body shadow"
                data-kt-image-input-action="cancel" data-bs-toggle="tooltip" data-bs-dismiss="click" title="Cancel avatar">
                <i class="bi bi-x fs-2"></i>
            </span>
            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" 
                data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                <i class="bi bi-x fs-2"></i>
            </span>
        @endif
    </div>
    @if(request()->route()->getName() == "profile.index")
        <div class="form-text text-black">Allowed file types: {{settingExtensionFormat(setting('file.profile_avatar_extension'),'')}}. Max size: {{settingExtensionFormat(setting('file.profile_avatar_size'),'')}} MB</div>
    @endif
</div>