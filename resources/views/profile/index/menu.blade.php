<div class="card mb-5 mb-xl-10">
    <div class="card-body py-0">
        <ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bold">
            <li class="nav-item mt-2">
                <a class="nav-link text-active-primary ms-0 me-10 py-5 {{ routeSlugActive('profile.index', 'active') }}"
                    href="{{ route('profile.index') }}">
                    My Profile
                </a>
            </li>
            <li class="nav-item mt-2">
                <a class="nav-link text-active-primary ms-0 me-10 py-5 {{ routeSlugActive('profile.notification.index', 'active') }}"
                    href="{{ route('profile.notification.index') }}">
                    Notification
                </a>
            </li>
        </ul>
    </div>
</div>