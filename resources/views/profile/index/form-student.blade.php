<div class="row w-100 m-0">
    @include('profile.index.form-avatar')
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Student ID
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="id_number">{{ $student->id_number ?? null }}</span>
        @else
            <input type="text" name="id_number" class="form-control" placeholder="Student ID"
                value="{{ $student->id_number ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Name
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="name">{{ $student->name ?? null }}</span>
        @else
            <input type="text" name="name" class="form-control" placeholder="Name"
                value="{{ $student->name ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Taylor’s Email
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="taylors_email">{{ $student->taylors_email ?? null }}</span>
        @else
            <input type="text" name="taylors_email" class="form-control" placeholder="Taylor’s email"
                value="{{ $student->taylors_email ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Personal Email
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="personal_email">{{ $student->personal_email ?? null }}</span>
        @else
            <input type="text" name="personal_email" class="form-control" placeholder="Personal email"
            value="{{ $student->personal_email ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Contact Number
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="contact_number">{{ $student->contact_number ?? null }}</span>
        @else
            <input type="text" name="contact_number" class="form-control" placeholder="Personal email"
            value="{{ $student->contact_number ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Nationality
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="nationality">{{ $student->nationality ?? null }}</span>
        @else
            <input type="text" name="nationality" class="form-control" placeholder="Nationality"
            value="{{ $student->nationality ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Intake Number
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="intake_number">{{ $student->intake_number ?? null }}</span>
        @else
            <input type="text" name="intake_number" class="form-control" placeholder="Intake Number"
            value="{{ $student->intake_number ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Faculty Name
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="faculty_name">{{ $student->faculty_name ?? null }}</span>
        @else
            <input type="text" name="faculty_name" class="form-control" placeholder="Faculty Name"
            value="{{ $student->faculty_name ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            School Name
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="school_name">{{ $student->school_name ?? null }}</span>
        @else
            <input type="text" name="school_name" class="form-control" placeholder="School Name"
            value="{{ $student->school_name ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Programme Name
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="programme_name">{{ $student->programme_name ?? null }}</span>
        @else
            <input type="text" name="programme_name" class="form-control" placeholder="Programme Name"
            value="{{ $student->programme_name ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Student Level
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="level_of_study">{{ $student->level_of_study ?? null }}</span>
        @else
            <input type="text" name="level_of_study" class="form-control" placeholder="Student Level"
            value="{{ $student->level_of_study ?? null }}">
        @endif
    </div>
    @if (!auth()->user()->can('student'))
        <div class="d-flex flex-column fv-row col-md-6 mb-5">
            <label class="fs-5 fw-bold form-label mb-2">
                Institution
            </label>
            @if($inputDisabled ?? false)
                <span class="fs-6" data-model="campus">{{ $student->campus ?? null }}</span>
            @else
                <input type="text" name="campus" class="form-control" placeholder="Institution"
                value="{{ $student->campus ?? null }}">
            @endif
        </div>
    @endif
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            Mobility Programme
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="mobility">{{ $student->mobility ?? null }}</span>
        @else
            <input type="text" name="mobility" class="form-control" placeholder="Mobility"
                value="{{ $student->mobility ?? null }}">
        @endif
    </div>
    <div class="d-flex flex-column fv-row col-md-6 mb-5">
        <label class="fs-5 fw-bold form-label mb-2">
            FLAME Mentor Name
        </label>
        @if($inputDisabled ?? false)
            <span class="fs-6" data-model="flame_mentor_name">{{ $student->flame_mentor_name ?? null }}</span>
        @else
            <input type="text" name="flame_mentor_name" class="form-control" placeholder="FLAME Mentor Name"
            value="{{ $student->flame_mentor_name ?? null }}">
        @endif
    </div>
</div>
