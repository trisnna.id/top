<div class="row w-100 m-0">
    @include('profile.index.form-avatar')
    <div class="d-flex flex-column mb-5 fv-row col-md-6">
        <label class="fs-5 fw-bold form-label mb-2">
            Name
        </label>
        <input type="text" name="name" class="form-control" placeholder="Name" value="{{ $staff->name ?? null }}">
    </div>
    <div class="d-flex flex-column mb-5 fv-row col-md-6">
        <label class="fs-5 fw-bold form-label mb-2">
            Staff ID
        </label>
        <input type="text" name="name" class="form-control" placeholder="Staff ID" value="{{ $staff->id_number ?? null }}">
    </div>
    <div class="d-flex flex-column mb-5 fv-row col-md-6">
        <label class="fs-5 fw-bold form-label mb-2">
            Role
        </label>
        <input type="text" name="name" class="form-control" placeholder="Role" value="{{ $user->role->title ?? null }}">
    </div>
    <div class="d-flex flex-column mb-5 fv-row col-md-6">
        <label class="fs-5 fw-bold form-label mb-2">
            Email Address
        </label>
        <input type="text" name="name" class="form-control" placeholder="Email Address" value="{{ $staff->taylors_email ?? null }}">
    </div>
</div>
