<x-layouts.app :title="$title" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <h1 class="page-heading d-flex text-white fw-bold fs-3 flex-column justify-content-center my-0">
                    {{ $title }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-white text-hover-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('profile.notification.index') }}" class="text-white text-hover-white">{{ $title }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        @include('profile.index.menu')
        <div class="card mb-5 mb-xl-10">
            <div class="card-body border-top p-9">
                {!! $notificationDt->html()->table() !!}
                @push('script')dtx.initOption('notificationDt');{!! $notificationDt->generateMinifyScripts() !!}@endpush
            </div>
        </div>
    </div>
</x-layouts.app>
