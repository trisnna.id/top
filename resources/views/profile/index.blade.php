<x-layouts.app :title="'My Profile'" toolbarClass="py-3 py-lg-6 cover"
    toolbarStyle="background-image: url('{{ randomBackgroundImage() }}');">
    <x-slot name="toolbar">
        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
            <div class="page-title d-flex flex-column justify-content-center me-3 flex-wrap">
                <h1 class="page-heading d-flex fw-bold fs-3 flex-column justify-content-center my-0 text-white">
                    My {{ $title }}</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ url('/') }}" class="text-hover-white text-white">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet w-5px h-2px bg-gray-400"></span>
                    </li>
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ route('profile.index') }}" class="text-hover-white text-white">My {{ $title }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </x-slot>
    <div id="kt_app_content_container" class="app-container container-xxl">
        @include('profile.index.menu')
        <div class="card mb-xl-10 mb-5">
            <div class="card-header">
                <div class="py-5 w-100">
                    <div class="d-flex align-items-center bg-light-primary rounded py-5 px-5">
                        <span class="svg-icon svg-icon-3x svg-icon-primary me-5"><svg width="24" height="24"
                                viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20"
                                    rx="10" fill="currentColor"></rect>
                                <rect x="11" y="14" width="7" height="2" rx="1"
                                    transform="rotate(-90 11 14)" fill="currentColor"></rect>
                                <rect x="11" y="17" width="2" height="2" rx="1"
                                    transform="rotate(-90 11 17)" fill="currentColor"></rect>
                            </svg>
                        </span>
                        <div class="fw-bold fs-6 text-gray-700">
                            Please click <a href="https://campuscentral.taylors.edu.my/myadmin/MyProfile/SitePages/Home.aspx">HERE</a>
                            to update your personal details. To update personal email and mobile no., please update from
                            your Taylor’s Mobile app.
                        </div>
                    </div>
                </div>
            </div>
            <form id="upsert" class="form">
                <div class="card-body border-top p-9">
                    @if (!empty($student ?? null))
                        <div id="student">
                            @include('profile.index.form-student')
                        </div>
                        @push('script')
                            modules.profile.index.student("#student");
                        @endpush
                    @elseif(!empty($staff ?? null))
                        <div id="staff">
                            @include('profile.index.form-staff')
                        </div>
                        @push('script')
                            modules.profile.index.staff("#staff");
                        @endpush
                    @else
                        <div id="admin">
                            @include('profile.index.form-admin')
                        </div>
                        @push('script')
                            modules.profile.index.admin("#admin");
                        @endpush
                    @endif
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button class="btn btn-primary upsert-swal-inline px-6"
                        data-swal="{{ json_encode([
                            'id' => 'upsert',
                            'axios' => [
                                'method' => 'post',
                                'url' => route('profile.store'),
                            ],
                        ]) }}">
                        Save Avatar Changes
                    </button>
                </div>
            </form>
        </div>
    </div>
</x-layouts.app>
