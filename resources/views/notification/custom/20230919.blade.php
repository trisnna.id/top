@section('subject')
    You're Invited to Luminous: Orientation Party!
@endsection

@section('content')
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">Dear Freshmen,&nbsp;</span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">We hope this email finds you excited and ready to embark on your journey at Taylor&rsquo;s University. To kickstart your university experience, we are thrilled to invite you to our much-anticipated Freshmen Orientation Party - Luminous!&nbsp;</span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">🌟 <strong>Event Details</strong> 🌟&nbsp;</span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;"><strong><span style="color: rgb(0, 32, 96);">Date: 22nd of September, Friday&nbsp;<br>Time: 5pm - 10pm&nbsp;<br>Location: Taylor&rsquo;s Grand Hall </span></strong></span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">Luminous is the perfect opportunity for you to meet your fellow freshmen, get acquainted with the campus, and start creating unforgettable memories. Our team has put together an exciting program filled with games, music, and delicious food that will light up your night.&nbsp;</span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;"><strong>To secure your spot at this unforgettable event, please click the registration link below: </strong><br>[<a href="https://www.eventbrite.co.uk/e/713642512967?aff=oddtdtcreator" target="_blank" rel="noopener noreferrer">Registration Link</a>]&nbsp;</span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">Requirements:&nbsp;</span></p>
    <ol>
        <li style='font-family: "Trebuchet MS", Helvetica, sans-serif;'>Please use your full name as per IC or as per Passport&nbsp;</li>
        <li style='font-family: "Trebuchet MS", Helvetica, sans-serif;'>You will need to prepare your Taylor&rsquo;s Mobile App Timetable&nbsp;</li>
    </ol>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">Don&apos;t miss out on this chance to shine bright at Luminous and make new friends before classes even begin. We can&apos;t wait to celebrate the start of your academic journey with you!&nbsp;</span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">If you have any questions or need further information, feel free to reach out to us at @orientation.tlc or email us at ol.taylors@gmail.com 😎&nbsp;</span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">We look forward to seeing you at Luminous and officially welcoming you to the Taylor&rsquo;s University family.&nbsp;</span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;"><strong>Sincerely,&nbsp;<br>Taylor&rsquo;s Orientation Leaders </strong></span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">&lsquo;&lsquo;Lead, Love, Serve&rsquo;&rsquo;&nbsp;</span></p>
    <p><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;"><a href="https://www.facebook.com/Orientation.TLC/" target="_blank" rel="noopener noreferrer">Facebook </a><br></span><a href="https://www.instagram.com/orientation.tlc/" target="_blank" rel="noopener noreferrer"><span style="font-family: 'Trebuchet MS', Helvetica, sans-serif;">Instagram</span></a></p>
@endsection