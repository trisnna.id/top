@section('subject')
    Congratulations! Your countdown to arrival starts now - See you at Taylor's!
@endsection

@section('content')
    <p><a href="{{url('/')}}" target="_blank"><img src="{{url('media/poster/poster-20230912.png')}}" /></a></p>   
@endsection