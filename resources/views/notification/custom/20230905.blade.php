@section('subject')
    Get Ready to Ace Your Academic Journey at Taylor's: Checkpoint 4 is Here!
@endsection

@section('content')
    <p><a href="{{url('/')}}" target="_blank"><img src="{{url('media/poster/poster-20230905.png')}}" /></a></p>   
@endsection