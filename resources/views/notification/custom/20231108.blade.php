@section('subject')
    Welcome to Taylor's. Get ready for an amazing journey!
@endsection

@section('content')
    <p><a href="{{url('/')}}" target="_blank"><img width="500" style="display: block; width: 100%; max-width: 100%;" src="{{url('media/poster/poster-20231108.png')}}" /></a></p>
    <p><strong><u>Additional Information</u></strong></p>
    <ol>
        <li><a style="color:#c00000;" href="https://www.youtube.com/watch?v=XOkRKmpQTqw" target="_blank" rel="noopener noreferrer">Click here for Taylor&rsquo;s Orientation Portal Guide</a></li>
        <li>For more information on Orientation, you can find support through any of the following channels:<ul>
                <li>Email at <a style="color:#c00000;" href="mailto:orientation@taylors.edu.my">orientation@taylors.edu.my</a></li>
                <li>For any other information or support, please use the <a style="color:#c00000;" href="https://campuscentral.taylors.edu.my/HelpMe/GL/SitePages/Home.aspx" target="_blank" rel="noopener noreferrer">Online Enquiry Form</a> on Campus Central Portal. You may also use the Live chat function on the <a style="color:#c00000;" href="https://campuscentral.taylors.edu.my/sitePages/Home.aspx">portal</a> or the <a style="color:#c00000;" href="https://play.google.com/store/apps/details?id=my.edu.taylors.mobilecampus&hl=en&gl=US&pli=1">Mobile app</a>.</li>
            </ul>
        </li>
    </ol>
@endsection