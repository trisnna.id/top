<div class="d-flex flex-stack flex-wrap">
    @php
        function textColor(string $value): string
        {
            $value = (int) $value;
        
            if ($value <= 25) {
                return 'bg-danger';
            } elseif ($value > 25 && $value <= 50) {
                return 'bg-warning';
            } else if ($value > 50 && $value < 100) {
                return 'bg-dark-success';
            }
        
            return 'bg-success';
        }
    @endphp

    <style>
        .bg-progress-max {
            background-color: #0070c0 !important;
        }

        .bg-progress-min {
            background-color: #FF0000 !important;
        }

        .text-progress-dark {
            color: #0070c0 !important;
        }
    </style>

    <div class="d-flex align-items-center {{ $widthClass }} flex-column mt-3">
        <div class="d-flex justify-content-between w-100 mt-auto mb-2">
            <span class="fw-semibold fs-6 text-gray-900">{{ $title }}</span>
            <span class="fw-bold fs-6" style="color: {{ $value < 50 ? '#FF0000;' : '#0070C0;' }}">{{ __("{$value}%") }}</span>
        </div>

        <div class="h-5px w-100 bg-light mx-3 mb-3">
            <div class="{{ $value < 50 ? 'bg-progress-min' : 'bg-progress-max' }} h-5px rounded" role="progressbar" style="width: {{ $value }}%;"
                aria-valuenow="{{ __("{$value}%") }}" aria-valuemin="{{ $min }}"
                aria-valuemax="{{ $max }}"></div>
        </div>
    </div>
</div>
