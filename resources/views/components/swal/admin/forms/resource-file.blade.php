<button type="button" class="btn btn-light-warning me-3" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
    <span class="svg-icon svg-icon-2">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                fill="currentColor" />
        </svg>
    </span>
    Filter
</button>
<div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true">
    <div class="px-7 py-5">
        <div class="fs-5 text-dark fw-bold">Filter Options</div>
    </div>
    <div class="separator border-gray-200"></div>
    <div class="px-7 py-5">
        <div class="row mb-5">
            <div class="col-md-12 fv-row fv-plugins-icon-container">
                <label class="form-label fw-semibold">File Type:</label>
                <select name="filter_file_type" class="form-select form-select2" id="filter_file_type"
                    data-hide-search="true" data-allow-clear="true" data-placeholder="-- Choose file type --">
                    <option></option>
                    <option value="image">Image</option>
                    <option value="document">Doument</option>
                </select>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-12 fv-row fv-plugins-icon-container">
                <label class="form-label fw-semibold">Title:</label>
                <input id="filterTitle" name="filter_title" type="text" class="form-control" placeholder="eg, Link Title">
            </div>
        </div>
        <div class="row mb-5">
            <x-filter-status :options="['active', 'inactive']" />
        </div>
        <div class="row">
            <div class="d-flex justify-content-end">
                <button type="reset"
                    class="btn btn-sm btn-light btn-active-light-primary me-2"
                    data-kt-menu-dismiss="true">Reset</button>
                <button type="submit" class="btn btn-sm btn-primary"
                    data-kt-menu-dismiss="true">Apply</button>
            </div>
        </div>
    </div>
</div>
<x-datatables.card.buttons.export color="warning" />
@if (auth()->user()->hasRole('admin'))
    <button type="button" class="btn btn-warning upsert-swal-popup" data-swal="{{ json_encode($swalSettings()) }}">
        <span class="svg-icon svg-icon-2">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1"
                    transform="rotate(-90 11.364 20.364)" fill="currentColor" />
                <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor" />
            </svg>
        </span>
        {{ $toolbarTitle }}
    </button>
    <x-swal.upsert :id="$id" :swalSettings="$swalSettings()">
        <!--begin::Form-->
        <x-inputs.title :grouping="true" required id="linkTitle" />
        <div class="col-md-12 mb-3 fv-row fv-plugins-icon-container">
            <label class="required fs-5 fw-semibold mb-2">Description:</label>
            <input id="content" type="hidden" name="description" value="">
            <textarea class="form-control" name="content_editor" id="content_editor"></textarea>
        </div>
        <div class="mb-10">
            <label for="basic-url" class="required form-label">{{ __('File') }}</label>
            <input name="value" type="file" class="form-control" id="source" aria-describedby="videoSource" />
        </div>
        <!--end::Form-->
        <x-slot name="didOpen">
            tinymcex.init("content_editor", "#content")
            if (data.id) {
                $('#videoTitle').val(data.name).change()
                $('#videoDescription').val(data.description).change()
    
                if (data.resourceType.name.includes('file')) {
                    $('#sourceType').val('file').change()
                }
            }
        </x-slot>
    </x-swal.upsert>
@endif