window.swalSettings['{{$id}}Settings'] = $.extend({}, {!! $swalSettings !!}, {
    html: `<form id="{{$id}}" class="form theme-form" autocomplete="off" onkeydown="return event.key != 'Enter';">{!! $slot !!}</form>`,
    didOpen: (data) => { {!! $didOpen ?? null !!} },
    didDestroy: (data) => { {!! $didDestroy ?? null !!} },
    preConfirm: () => { {!! $preConfirm ?? null !!} },
    axios:{headersAccept: '{{$accept}}',url: (data) => {return `{!! $route !!}`;},afterValidate: (data) => { {!! $afterValidate ?? null !!} },afterSuccessResponse: (data) => { {!! $afterSuccessResponse ?? null !!} }},
});