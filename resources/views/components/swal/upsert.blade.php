@php
    $slot = \JShrink\Minifier::minify($slot ?? '');
    $compact = [
        'id' => $id,
        'swalSettings' => $swalSettings,
        'slot' => $slot,
        'didOpen' => $didOpen ?? null,
        'didDestroy' => $didDestroy ?? null,
        'preConfirm' => $preConfirm ?? null,
        'accept' => $accept ?? null,
        'route' => $route ?? null,
        'afterValidate' => $afterValidate ?? null,
        'afterSuccessResponse' => $afterSuccessResponse ?? null,
    ];
    $script = \JShrink\Minifier::minify(view('components.swal.upsert-script', $compact)->render());
@endphp

@push('script'){!! $script !!}@endpush
