<div id="carouselContainer" style="position: relative;">
    <div id="kt_carousel_2_carousel" class="carousel carousel-custom slide" data-bs-ride="carousel" data-bs-interval="8000">
        <!--begin::Heading-->
        <div class="d-flex align-items-center justify-content-between flex-wrap">
            <!--begin::Label-->
            <span class="fs-1 fw-bold pe-2">
                <h3 class="fw-bold fs-1 my-2 text-gray-800"
                    style="font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; ">
                    {{ __('Announcements') }}

                    <x-tooltip.basic
                        title='Keep an eye on your Orientation email showcased here for orientation updates and exciting announcements!'>
                    </x-tooltip.basic>

                    <span
                        class="fs-6 fw-semibold ms-1 text-gray-400">({{ $announcementsLg->collapse()->count() }})</span>
                </h3>
            </span>
            <!--end::Label-->

            <!--begin::Carousel Indicators-->
            <ol class="carousel-indicators carousel-indicators-bullet d-none d-sm-block d-xs-block d-md-none m-0 p-0">
                @foreach ($announcementsSm as $announcement)
                    <li data-bs-target="#kt_carousel_2_carousel" data-bs-slide-to="{{ $loop->index }}"
                        class="{{ $loop->first ? 'active' : '' }} ms-1"></li>
                @endforeach
            </ol>
            <ol class="carousel-indicators carousel-indicators-bullet d-none d-md-block d-lg-none m-0 p-0">
                @foreach ($announcementsMd as $announcement)
                    <li data-bs-target="#kt_carousel_2_carousel" data-bs-slide-to="{{ $loop->index }}"
                        class="{{ $loop->first ? 'active' : '' }} ms-1"></li>
                @endforeach
            </ol>
            <ol
                class="carousel-indicators carousel-indicators-bullet d-block d-xs-none d-sm-none d-md-none d-lg-block m-0 p-0">
                @foreach ($announcementsLg as $announcement)
                    <li data-bs-target="#kt_carousel_2_carousel" data-bs-slide-to="{{ $loop->index }}"
                        class="{{ $loop->first ? 'active' : '' }} ms-1"></li>
                @endforeach
            </ol>
            <!--end::Carousel Indicators-->
        </div>
        <!--end::Heading-->

        <!--begin::Carousel-->
        <div class="carousel-inner d-block pt-8">
            @foreach ($announcementsLg as $announcement)
                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                    <div class="row g-6 gx-xl-9 mb-xl-9 row-cols-ann mb-6">
                        @foreach ($announcement as $ann)
                            <!--begin::Col-->
                            <div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10 car-conn">
                                <div class="carousel-container">
                                    <div class="card ann-card rounded border shadow"
                                        style="border-radius: 10% !important;">
                                        <img class="card-img-top rounded-4"
                                            src="{{ asset($ann->thumbnail) == config('app.url') . '/' ? 'https://dummyimage.com/1920x1080/000/fff' : asset($ann->thumbnail) }}" />
                                        <div class="card-body d-flex justify-content-center flex-column">
                                            <h3 class="card-title text-center"
                                                style="font-family: Antic, sans-serif;color: rgb(81,87,94);"
                                                align="center">
                                                {{ $ann->name }}</h3>
                                            {{-- <p class="card-text text-left">{!! substr(preg_replace('/<[^>]*>/', '', html_entity_decode($ann->content)), 0, 50) . '...' !!}</p> --}}
                                            <div class="d-flex justify-content-center mt-auto">
                                                <a class="btn btn-sm btn-secondary btn-view-detail hover-scale btn-ann me-2 text-white"
                                                    href="{{ route('announcement.show', ['slug' => $ann->uuid]) }}">
                                                    {{ __('View Detail') }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end::Col-->
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
        <!--end::Carousel-->
    </div>
    <div class="w-100 d-flex justify-content-end">
        <a href="{{ route('announcement.index') }}" class="btn btn-sm btn-danger btn-ann ml-auto mt-1">View
            all</a>
    </div>
</div>

@push('styles')
    <style>
        .car-conn {
            border-color: inherit !important;
            border-width: inherit !important;
            border-style: inherit !important;
            text-transform: inherit !important;
            font-weight: inherit !important;
            font-size: inherit !important;
            color: inherit !important;
            height: inherit !important;
            min-height: inherit !important;
        }

        .carousel-container {
            height: 100% !important;
        }

        .carousel-container>.card {
            height: 100% !important;
        }

        .card-img-top {
            max-height: 130px;
            min-height: 130px;
            object-fit: cover;
        }

        .btn-ann {
            background-color: #467599 !important;
        }
    </style>
@endpush
@push('styles')
    <style>
        /* media query for sm, md, and lg each */
        @media (max-width: 767px) {
            #carouselContainer {
                margin-top: 4rem !important;
            }

            .row-cols-ann>* {
                width: 100% !important;
            }
        }

        @media (min-width: 768px and max-width: 991px) {
            .row-cols-ann>* {
                width: 50% !important;
            }
        }

        @media (min-width: 992px) {
            .row-cols-ann>* {
                width: 20% !important;
            }
        }
    </style>
@endpush
