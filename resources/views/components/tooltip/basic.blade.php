<i class="fa fa-info-circle fs-3 text-1e4a99" style="color: {{ $color }} !important" data-bs-toggle="tooltip"
    data-bs-placement="{{ $placement }}" title="{!! $title !!}"></i>
