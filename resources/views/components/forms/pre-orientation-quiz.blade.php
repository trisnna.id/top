<div id="{{ $entity->uuid }}" style="display: none;"
    @if (optional($entity->quizs)->count() != null && optional($entity->quizs)->count() > 0) data-has-quiz="true" @else data-has-quiz="false @endif">
    <div class="row detail_container_{{ $entity->uuid }}">
        <div class="col-12" id="content_container_{{ $entity->uuid }}" style="height: 100% !important;">
            <div class="card detail_card shadow-md">
                <div class="card-header">
                    <h3 class="card-title">
                        <div class="fw-bold mb-2 mt-5">
                            <span class="badge badge-secondary">{{ $entity->checkpoint->label }}</span>
                        </div>
                    </h3>
                    <div class="card-toolbar">
                        @if (
                            !$entity->students->isNotEmpty() &&
                                (optional($entity->quizs)->count() == null || optional($entity->quizs)->count() == 0))
                            <button id="markComplete" type="button" value="{{ $entity->id }}"
                                class="btn btn-sm btn-success"
                                data-route="{{ route('api.preorientation.markcomplete') }}"
                                data-csrf="{{ csrf_token() }}" data-uuid="{{ $entity->uuid }}"
                                data-render-url="{{ route('api.preorientation.update-component') }}">
                                <i class="bi bi-check fs-4 me-2"></i>
                                {{ __('Mark as complete') }}
                            </button>
                        @elseif ($entity->students->isNotEmpty())
                            <span class="badge badge-success">{{ __('Completed') }}</span>
                        @endif
                    </div>
                </div>
                <div class="card-body p-lg-5 pb-lg-0">
                    @if (optional($entity->content_links)->isNotEmpty())
                        <!--begin::Section Video-->
                        <div class="mb-5">
                            <!--begin::Content-->
                            <div class="d-flex flex-stack mb-5">
                                <!--begin::Title-->
                                <h3 class="text-dark">Video/Activity</h3>
                                <!--end::Title-->
                            </div>
                            <!--end::Content-->

                            <!--begin::Separator-->
                            <div class="separator separator-dashed mb-9"></div>
                            <!--end::Separator-->

                            <!--begin::Row-->
                            <div class="row g-5 justify-content-center">
                                @foreach ($entity->content_links as $contentLink)
                                    <div class="{{ sprintf('col-md-4') }}">
                                        <div class="card-xl-stretch me-md-6">
                                            {{-- <a class="video-thumbnail d-block bgi-no-repeat bgi-size-cover bgi-position-center card-rounded position-relative min-h-175px mb-5"
                                                style="background-image:url('{{ $contentLink->thumbnail }}')"
                                                data-fslightbox="lightbox-video-tutorial"
                                                href="{{ $contentLink->link }}">

                                                <img src="{{ asset('media/svg/misc/video-play.svg') }}"
                                                    class="position-absolute top-50 start-50 translate-middle"
                                                    alt="">
                                            </a> --}}
                                            @php
                                                $link = $contentLink->link;
                                                $link = str_replace('watch?v=', 'embed/', $link);
                                                $placeholder = '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100% 100%"><text fill="#FF0000" x="50%" y="50%" font-family="\'Lucida Grande\', sans-serif" font-size="24" text-anchor="middle">PLACEHOLDER</text></svg>';
                                            @endphp
                                            <div class="ratio ratio-16x9">
                                                <div class="video">
                                                    <iframe class="w-100" src="{{ $link }}"
                                                        {{-- title="${data.video_name}" --}} frameborder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                                        allowfullscreen
                                                        style="background: url('data:image/svg+xml;charset=utf-8,{{ $placeholder }}') 0px 0px no-repeat;"></iframe>
                                                    {{-- <div class="video__youtube" data-youtube>
                                                        <img src="{{ $contentLink->thumbnail }}"
                                                            class="video__placeholder" />
                                                        <button class="video__button"
                                                            data-youtube-button="{{ $link }}"></button>
                                                    </div> --}}
                                                </div>
                                            </div>
                                            <div class="m-0">
                                                <a href="/metronic8/demo1/../demo1/pages/user-profile/overview.html"
                                                    class="fs-4 text-dark fw-bold text-hover-primary text-dark lh-base">
                                                </a>
                                                <div class="fw-semibold fs-5 text-dark my-4 text-gray-600">
                                                    {!! $contentLink->text !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <!--end::Row-->
                        </div>
                        <!--end::Section Video-->
                    @endif

                    <div class="d-flex flex-column">
                        <div class="flex-lg-row-fluid">
                            <div class="mb-17">
                                <div id="quizContent">
                                    {!! $entity->content !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if (optional($entity->quizs)->count() != null && optional($entity->quizs)->count() > 0)
                    <div class="d-flex justify-content-center mb-3">
                        <button class="btn btn-red" id="btn_next_{{ $entity->uuid }}">Next</button>
                    </div>
                @endif
            </div>
        </div>
        @if (optional($entity->quizs)->count() != null && optional($entity->quizs)->count() > 0)
            <div class="col-12 col-md-6 d-none" id="quiz_container_{{ $entity->uuid }}">
                <div class="quiz-content">
                    <x-pre-orientation.quiz :key="$entity->uuid" :target="$entity->id" :milestone="$entity->checkpoint->label" :title="$entity->activity_name" />
                </div>
                <quiz-component key="{{ $entity->uuid }}" target="{{ $entity->id }}"
                    milestone="{{ $entity->checkpoint->label }}" title="{{ $entity->activity_name }}"
                    data-render-url="{{ route('api.preorientation.update-component') }}"
                    token="{{ csrf_token() }}" />
            </div>
        @endif
    </div>

    <style>
        .detail_container_{{ $entity->uuid }} {
            /* remove horizontal scroll */
            width: 100%;
        }

        .ratio::before {
            padding-top: 5%;
        }
    </style>
</div>
