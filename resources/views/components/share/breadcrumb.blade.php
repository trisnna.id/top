@props(['title' => null])

<div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
    <div class="page-title d-flex flex-column justify-content-center me-3 flex-wrap">
        <h1 class="page-heading d-flex fw-bold fs-3 flex-column justify-content-center my-0 text-white">
            {!! $title !!}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            @foreach ($defaultLinks as $link)
                <li class="breadcrumb-item text-muted">
                    <a href="{{ url($link->get('url')) }}"
                        class="text-primary text-hover-blue">{!! $link->get('text') !!}</a>
                </li>
                @if ($loop->iteration < $defaultLinks->count())
                    <li class="breadcrumb-item">
                        <span class="bullet w-5px h-2px bg-gray-400"></span>
                    </li>
                @endif
            @endforeach

            @if ($title !== null)
                <li class="breadcrumb-item">
                    <span class="bullet w-5px h-2px bg-gray-400"></span>
                </li>
                <li class="breadcrumb-item text-muted">
                    <span class="text-primary">{!! $title !!}</span>
                </li>
            @endif
        </ul>
    </div>
    <div class="d-flex align-items-center gap-lg-3 gap-2">
        {{ $slot }}
    </div>
</div>
