<div class="d-flex align-items-center">
    <div class="d-flex flex-column">
        @if($instance)
            @if(urlTypeMedia($instance->url) == 'youtube')
                <iframe src="{{$instance->url}}" title="{{$instance->name}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen style="max-width: 170px;max-height: 170px;"></iframe>
            @elseif(urlTypeMedia($instance->url) == 'video')
                <video controls style="max-width: 170px;max-height: 170px;">
                    <source src="{{$instance->url}}">
                </video>
            @else
                <a href="{{$instance->url}}" target="_blank" class="text-gray-800 text-hover-primary">{{$instance->name}}</a>
            @endif
        @endif
    </div>
</div>