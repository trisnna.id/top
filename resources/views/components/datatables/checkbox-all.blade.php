<label class="form-check form-check-sm form-check-custom form-check-solid form-checkbox-label">
    <input class="form-check-input form-checkbox-input checkbox-input-all" {{$check ?? null}} data-checkboxId="{{$checkboxId}}" type="checkbox">
</label>
<input id="{{$checkboxId}}" data-count="0" type="hidden">