@if (!empty($instance['upsert'] ?? null))
    <span class="badge badge-sm px-4 badge-{{ $instance['upsert']['data']['color_class'] ?? null }} upsert-swal-dt"
        data-swal="{{ json_encode($instance['upsert']['swal'] ?? []) }}">{{ $instance['upsert']['data']['status'] ?? [] }}</span>
@else
    <span class="badge badge-sm px-4 badge-{{ $colorClass ?? null }}">{{ $label ?? null }}</span>
@endif
