@php
    $iconConstants = getConstant('IconConstant');
@endphp
<div class="btn-group" role="group">
    @if(!empty($instance['show'] ?? []))
        @if(!empty($instance['show']['redirect'] ?? []))
            <a href="{{$instance['show']['redirect'] ?? null}}" title="{{ $instance['show']['swal']['settings']['title'] ?? 'View Detail' }}" class="btn btn-icon btn-{{$iconConstants['btn_show']['color']['class']}} btn-sm">
                <i class="{{$iconConstants['btn_show']['icon_class']}}"></i>
            </a>
        @else
            <a href="#" title="{{ $instance['show']['swal']['settings']['title'] ?? 'View Detail' }}" class="btn btn-icon btn-{{$iconConstants['btn_show']['color']['class']}} btn-sm upsert-swal-dt" data-data="{{json_encode($instance['show']['data'])}}" data-swal="{{json_encode($instance['show']['swal'])}}">
                <i class="{{$iconConstants['btn_show']['icon_class']}}"></i>
            </a>
        @endif
    @endif
    @if(!empty($instance['upsert'] ?? []))
        @if(!empty($instance['upsert']['redirect'] ?? []))
            <a href="{{$instance['upsert']['redirect'] ?? null}}" title="{{ $instance['upsert']['swal']['settings']['title'] ?? 'Edit' }}" class="btn btn-icon btn-{{$iconConstants['btn_edit']['color']['class']}} btn-sm">
                <i class="{{$iconConstants['btn_edit']['icon_class']}}"></i>
            </a>
        @else
            <a href="#" title="{{ $instance['upsert']['swal']['settings']['title'] ?? 'Edit' }}" class="btn btn-icon btn-{{$iconConstants['btn_edit']['color']['class']}} btn-sm upsert-swal-dt" data-data="{{json_encode($instance['upsert']['data'])}}" data-swal="{{json_encode($instance['upsert']['swal'])}}">
                <i class="{{$iconConstants['btn_edit']['icon_class']}}"></i>
            </a>
        @endif
    @endif
    @if(!empty($instance['delete'] ?? []))
        <a href="#" title="{{ $instance['delete']['swal']['settings']['title'] ?? 'Delete' }}" class="btn btn-icon btn-{{$iconConstants['btn_delete']['color']['class']}} btn-sm upsert-swal-dt" data-swal="{{json_encode($instance['delete']['swal'])}}">
            <i class="{{$iconConstants['btn_delete']['icon_class']}}"></i>
        </a>
    @endif
    @if(!empty($instance['clone'] ?? []))
        <a href="#" title="{{ $instance['clone']['swal']['settings']['title'] ?? 'Clone' }}" class="btn btn-icon btn-{{ $iconConstants['btn_clone']['color']['class'] }} btn-sm upsert-swal-dt" data-swal="{{json_encode($instance['clone']['swal'])}}">
            <i class="{{ $iconConstants['btn_clone']['icon_class'] }}"></i>
        </a>
    @endif
    @if(!empty($instance['status'] ?? []))
        <a href="#" title="{{ $instance['status']['swal']['settings']['title'] ?? 'Status' }}" class="btn btn-icon btn-warning btn-sm upsert-swal-dt" data-swal="{{json_encode($instance['status']['swal'])}}">
            <i class="fas fa-ban"></i>
        </a>
    @endif
    @if(!empty($instance['approval'] ?? []))
        @if(!empty($instance['approval']['redirect'] ?? []))
            <a href="{{$instance['approval']['redirect'] ?? null}}" title="{{ $instance['approval']['swal']['settings']['title'] ?? 'Approval' }}" class="btn btn-icon btn-{{$iconConstants['btn_approval']['color']['class']}} btn-sm">
                <i class="{{ $iconConstants['btn_approval']['icon_class'] }}"></i>
            </a>
        @else
            <a href="#" title="{{ $instance['approval']['swal']['settings']['title'] ?? 'Approval' }}" class="btn btn-icon btn-{{$iconConstants['btn_approval']['color']['class']}} btn-sm upsert-swal-dt" data-data="{{json_encode($instance['approval']['data'])}}" data-swal="{{json_encode($instance['approval']['swal'])}}">
                <i class="{{ $iconConstants['btn_approval']['icon_class'] }}"></i>
            </a>
        @endif
    @endif
    @if(!empty($instance['submit'] ?? []))
        <a href="#" title="{{ $instance['submit']['swal']['settings']['title'] ?? 'Submit' }}" class="btn btn-icon btn-sm btn-{{ $iconConstants['btn_submit']['color']['class'] }} upsert-swal-dt" data-swal="{{json_encode($instance['submit']['swal'])}}">
            <i class="{{ $iconConstants['btn_submit']['icon_class'] }}"></i>
        </a>
    @endif
    @if(!empty($instance['publish'] ?? []))
        <a href="#" title="{{ $instance['publish']['swal']['settings']['title'] ?? 'Publish' }}" class="btn btn-icon btn-{{$instance['publish']['data']['color_class'] ?? $iconConstants['btn_publish']['color']['class']}} btn-sm upsert-swal-dt" data-swal="{{json_encode($instance['publish']['swal'])}}">
            <i class="{{$instance['publish']['data']['icon_class'] ?? $iconConstants['btn_publish']['icon_class']}}"></i>
        </a>
    @endif
    @if(!empty($instance['unpublish'] ?? []))
        <a href="#" title="{{ $instance['unpublish']['swal']['settings']['title'] ?? 'Unpublish' }}" class="btn btn-icon btn-sm btn-{{ $iconConstants['btn_unpublish']['color']['class'] }} upsert-swal-dt" data-swal="{{json_encode($instance['unpublish']['swal'])}}" style="background: #ffa500;">
            <i class="{{ $iconConstants['btn_unpublish']['icon_class'] }}"></i>
        </a>
    @endif
    
</div>