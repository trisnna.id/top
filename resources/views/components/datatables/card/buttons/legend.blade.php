<div class="w-md-80" style="margin: 0 auto;">
    <div class="rounded border border-gray-200 d-flex sm-justify-content-around p-3 pt-6 mb-3 overflow-x-scroll">
        @foreach ($legends as $key => $value)
            @if ($isShouldRender($key))
                <div class="d-flex flex-column flex-center w-{{ $gap }}px px-3">
                    @if ($value['badge'][0] !== '#')
                        <span class="badge badge-{{ $value['badge'] }}">
                            <i class="{{ $value['icon'] }} fs-6"></i>
                        </span>
                    @else
                        <span class="badge" style="background: {{ $value['badge'] }};">
                            <i class="{{ $value['icon'] }} fs-6"></i>
                        </span>
                    @endif
                    <div class="fw-semibold py-2">{{ $value['label'] }}</div>
                </div>
            @endif
        @endforeach
    </div>
</div>
