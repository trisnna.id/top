<label class="form-check form-check-sm form-check-custom form-check-solid form-checkbox-label">
    <input class="form-check-input form-checkbox-input checkbox-input-single" data-checkboxId="{{$checkboxId}}" {{$checked ?? null}} type="checkbox" value="{{$id}}">
</label>