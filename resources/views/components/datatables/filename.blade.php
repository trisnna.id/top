<div class="d-flex align-items-center">
    <div class="symbol symbol-50px overflow-hidden me-3">
        @if(extensionTypeMedia($instance->extension) == 'picture')
            {{$instance}}
        @elseif(extensionTypeMedia($instance->extension) == 'video')
            <video controls style="max-width: 170px;max-height: 170px;">
                <source src="{{$instance->original_url}}">
            </video>
        @else
            <a target="_blank">
                <i class="{{fileIconExtension($instance->extension)['icon_class']}} fs-3x text-{{fileIconExtension($instance->extension)['color']['class']}}"></i>
            </a>
        @endif
    </div>
    <div class="d-flex flex-column">
        <span class="mb-1">{{$instance->custom_properties['title'] ?? ''}}</span>
        @if($instance)
            <a href="{{$instance->original_url}}" download="" class="text-gray-800 text-hover-primary">{{$instance->file_name}}</a> ({{$instance->humanReadableSize}})
        @endif
    </div>
</div>