<div>
    @foreach ($checkpoints as $i => $item)
        <div class="mb-2 rounded bg-white py-7 px-4">
            <div class="fw-bold fs-6 text-gray-700">
                {{ $item->label }}
            </div>
            <div class="fw-bold fs-6 align-middle text-gray-700" style="width: 100%">
                <div class="row">
                    <div class="col-9 align-self-center">
                        @php
                            // get all pre-orientation on this checkpoint, then compare it with the pre-orienation that has been completed by the student
                            $student = auth()->user()->student;
                            $preOrientationTemp = $preOrientation->where('checkpoint_id', $item->id);
                            $studentCompleted = $studentPreOrientation
                                ->where('student_id', $student->id)
                                ->whereIn('pre_orientation_id', $preOrientationTemp->pluck('id')->toArray())
                                ->where('completed', 1)
                                ->unique('pre_orientation_id')->values();
                            
                            // get the percentage of the pre-orientation that has been completed by the student
                            $value = 0;
                            if ($preOrientationTemp->count() !== 0 && $studentCompleted->count() !== 0) {
                                $value = round(($studentCompleted->count() / $preOrientationTemp->count()) * 100);
                            }
                        @endphp
                        <div class="progress h-5px" style="background-color: #f2f2f2 !important;">
                            <div class="{{ $color($value) }} h-5px progress-bar rounded" role="progressbar"
                                style="width: {{ $value }}%;" aria-valuenow="{{ __("{$value}%") }}"
                                aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="col-3" style="color: {{ $textColor($value) }} !important">
                        {{ $value }}%
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
