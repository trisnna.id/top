<div id="{{ sprintf('quiz_%s', $key) }}" class="card quiz_card shadow-md" data-questions="{{ $questions->toJson() }}"
    data-questions-count="{{ $questions->count() }}" data-quiz-student="{{ json_encode($studentQuiz ?? []) }}">
    <div class="card-header">
        <h3 class="card-title">
            <div class="fw-bold mt-5 mb-2">
                Quiz
            </div>
        </h3>
        <div class="card-toolbar">
            <span class="badge badge-secondary">{{ $milestone }}</span>
        </div>
    </div>
    <div class="card-body p-lg-5 pb-lg-0">
        <div id="quiz_stepper" class="d-flex flex-column">
            <form id="quiz_form" class="w-100 mx-auto" novalidate="novalidate">
                @foreach ($questions as $i => $quiz)
                    @php
                        $quiz = (object) $quiz;
                    @endphp
                    <div class="flex-column {{ $i <= 0 ? 'current' : '' }}">
                        <div class="fv-row fs-5 fw-semibold mb-2 text-gray-600">
                            <p class="mb-8">
                                {{ $quiz->question }}
                            </p>
                        </div>
                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <div class="rounded border p-10">
                                <div class="row">
                                    @php
                                        try {
                                            $options = $quiz->shuffledOptions;
                                            if (!is_null($studentQuiz)) {
                                                $options = $quiz->options;
                                            }
                                        } catch (\Throwable $th) {
                                            $options = $quiz->options;
                                        }
                                    @endphp
                                    @foreach ($options as $option)
                                        @php
                                            $option = (object) $option;
                                        @endphp
                                        <div class="col-md-6">
                                            <div class="mb-5">
                                                <div class="form-check">
                                                    @if (!is_null($studentQuiz))
                                                        <input id="flexCheckDefault1"
                                                            name="{{ $quiz->id }}.{{ $option->key }}"
                                                            class="quiz-answer form-check-input {{ $renderIsValid($option->isCorrect()) }} {{ !is_null($studentQuiz) ? ($option->isCorrect() ? 'correct' : 'incorrect') : 'unanswered' }} {{ $shouldChecked($quiz->id, $option) == 'checked' ? 'myAnswer' : '' }}"
                                                            type="checkbox" value="{{ $option->value }}"
                                                            {{ !is_null($studentQuiz) ? 'disabled' : '' }}
                                                            {{ $shouldChecked($quiz->id, $option) }}
                                                            data-isAnswered={{ !is_null($studentQuiz) ? 'true' : 'false' }}
                                                            data-isCorrect={{ !is_null($studentQuiz) ? ($option->isCorrect() ? 'true' : 'false') : 'false' }}>
                                                        <label for="flexCheckDefault1"
                                                            class="form-check-label {{ $option->isCorrect() ? 'text-dark' : 'text-danger' }} {{ !is_null($studentQuiz) ? ($option->isCorrect() ? 'fw-bold' : '') : '' }}"
                                                            title="{{ $renderTitle($quiz->id, $option) }}"
                                                            data-bs-toggle="tooltip" data-bs-placement="top">
                                                            {{ $option->getTitleKey() }} . {{ $option->value }}
                                                        </label>
                                                    @else
                                                        {{-- radio button --}}
                                                        <input id="flexCheckDefault1" name="{{ $quiz->id }}"
                                                            class="quiz-answer form-check-input {{ $renderIsValid($option->isCorrect()) }} {{ !is_null($studentQuiz) ? ($option->isCorrect() ? 'correct' : 'incorrect') : 'unanswered' }} {{ $shouldChecked($quiz->id, $option) == 'checked' ? 'myAnswer' : '' }}"
                                                            type="radio" value="{{ $option->value }}"
                                                            {{ !is_null($studentQuiz) ? 'disabled' : '' }}
                                                            {{ $shouldChecked($quiz->id, $option) }}
                                                            data-isAnswered={{ !is_null($studentQuiz) ? 'true' : 'false' }}
                                                            data-isCorrect={{ !is_null($studentQuiz) ? ($option->isCorrect() ? 'true' : 'false') : 'false' }}>
                                                        <label for="flexCheckDefault1" class="form-check-label">
                                                            {{ $option->getTitleKey() }} . {{ $option->value }}
                                                        </label>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="d-flex flex-stack justify-content-center" style="width: 100% !important;">
                    <div>
                        <button id="cancelQuiz" type="button" class="btn btn-lg btn-secondary me-3">
                            <span class="indicator-label">
                                <span class="svg-icon svg-icon-3 ms-2 me-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                        fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                                    </svg>
                                </span>
                                Back
                            </span>
                        </button>
                        @if (is_null($studentQuiz))
                            <button id="submitQuiz" type="button" class="btn btn-lg btn-primary me-3"
                                style="position: absolute; right: 0;" data-precallfn="dataFormBuilder">
                                <span class="indicator-label">
                                    Submit
                                    <span class="svg-icon svg-icon-3 ms-2 me-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="18" y="13" width="13"
                                                height="2" rx="1" transform="rotate(-180 18 13)"
                                                fill="currentColor" />
                                            <path
                                                d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
                                                fill="currentColor" />
                                        </svg>
                                    </span>
                                </span>
                            </button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (!is_null($studentQuiz))
        <div class="card-footer">
            <div class="fw-bold">
                {{ __('Your score for this activity is:') }}
                <span class="badge badge-info">
                    {{ $studentQuiz->score }}
                </span>
            </div>
        </div>
    @endif
</div>
