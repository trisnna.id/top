<div id="points-overview" class="{{ $isVisible() }} mb-lg-0 mb-3">
    <div class="card card-shadow" style="background-color: transparent !important;">
        <div class="card-body">
            <h2 class="card-title align-items-start flex-column">
                <span class="card-label fw-bold text-dark fs-1"
                    style="font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; ">Points Overview</span>
            </h2>

            <div class="card rounded" style="background-color: #e5ebf7 !important; border-radius: 3rem !important;">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4" style="padding: 0px;">
                        <div
                            style="background-image: url('{{ asset('media/logos/points-image.png') }}');height: 20vh;background-repeat: no-repeat;background-size: contain;background-position: center;">
                        </div>
                    </div>
                    <div class="col" style="padding: 0px;">
                        <div class="card rounded"
                            style="background-color: transparent !important; border-radius: 3rem !important;">
                            <div class="card-body">
                                <div class="container" style="height: 100%">
                                    <div class="row align-items-center" style="height: 100%">
                                        <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                            <h4 class="fw-bold">Congratulations, {{ auth()->user()->name }}</h4>
                                            <p>You've successfully completed
                                                the Pre-Orientation and
                                                Orientation activities.</p>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                            <div class="card" style="background-color: #dae3f3 !important;">
                                                <div class="card-body">
                                                    <div class="text-center">
                                                        <h2 class="fs-1">250</h2>
                                                        <h4 style="color: #4291cf;">Points</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
