<div class="card card-shadow" style="background-color: #467599 !important;">
    <div class="card-body d-none d-md-block">
        <div class="row align-items-center mb-3">
            <div class="col-md-8 col-lg-8 col-xl-8 col-12 d-flex justify-content-center">
                <div data-rating="{{ $stars }}" class="d-flex justify-content-center ratings" id="ratings1"></div>
            </div>
            <div class="col-md-4 col-lg-4 col-xl-4 col-12 d-flex justify-content-center">
                <div id="gauge1"></div>
            </div>
            <style>
                .ratings {
                    width: 100% !important;
                }

                .jq-star svg {
                    stroke-width: 0% !important;
                }
            </style>
        </div>
        <div style="width: 100%;" class="d-flex align-items-center mt-md-n13 gap-1">
            <h2 class="text-white" style="font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; ">
                Total Points Collected</h2>

            <x-tooltip.basic
                title='Unlock rewards with every quiz, attendance and survey completed. Earn points, convert them to stars, and win incredible prizes!'
                color='white'>
            </x-tooltip.basic>
        </div>
    </div>
    <div class="card-body d-block d-md-none">
        <div class="row" style="height: 100% !important;">
            <div class="col-4 align-self-end" style="word-wrap: normal;">
            </div>
            <div class="col-8">
                <div id="gauge2"></div>
            </div>
        </div>
        <div class="row" style="height: 100% !important;">
            <div class="col" style="word-wrap: normal;">
                <div data-rating="{{ $stars }}" class="d-flex justify-content-start ratings mb-2" id="ratings2">
                </div>
                <div class="d-flex align-items-center gap-2">
                    <h2 class="text-white" style="font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; ">
                        Total Points Collected</h2>

                    <x-tooltip.basic
                        title='Unlock rewards with every quiz, attendance and survey completed. Earn points, convert them to stars, and win incredible prizes!'
                        color='white'>
                    </x-tooltip.basic>
                </div>
            </div>
        </div>
    </div>
</div>

@push('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/nashio/star-rating-svg@master/src/css/star-rating-svg.css">
@endpush
@push('scripts')
    <script src="https://cdn.jsdelivr.net/gh/nashio/star-rating-svg@master/dist/jquery.star-rating-svg.min.js"></script>
    <script>
        // let starSize is a #ratings width divided by 5
        let starSize1 = $("#ratings1").width() * (15 / 100);
        $("#ratings1").starRating({
            totalStars: 5,
            starSize: starSize1,
            emptyColor: '#f2f2f2',
            activeColor: '#ffff99',
            useGradient: false,
            readOnly: true,
        });
        // let starSize is a #ratings width divided by 5
        let starSize2 = $("#ratings2").width() * (13 / 100);
        $("#ratings2").starRating({
            totalStars: 5,
            starSize: starSize2,
            emptyColor: '#f2f2f2',
            activeColor: '#ffff99',
            useGradient: false,
            readOnly: true,
        });
    </script>
    <script>
        if (parseInt('{{ $gauge }}') == 0) {
            var series = [
                100
            ]
            var colors = ['#ffffff']
        } else {
            var series = [
                parseInt('{{ $gauge }}'),
                parseInt('{{ 100 - $gauge }}')
            ]
            var colors = ['#FFFFFF', '#4e84ac']
        }

        var options = {
            series: series,
            colors: colors,
            chart: {
                type: 'donut',
            },
            dataLabels: {
                enabled: false,
            },
            labels: labels,
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        show: false,
                        position: 'bottom'
                    }
                }
            }],
            legend: {
                show: false,
                position: 'bottom'
            },
            yaxis: {
                labels: {
                    formatter: function(value) {
                        return value;
                    },
                },
            },
            @if ($gauge == 0)
                tooltip: {
                    enabled: false
                },
            @endif

            @if ($gauge != 0)
                tooltip: {
                    y: {
                        formatter: function(value) {
                            return value + "%";
                        },
                        title: {
                            formatter: function(seriesName) {
                                return '';
                            }
                        }
                    },
                },
            @endif
            plotOptions: {
                pie: {
                    donut: {
                        labels: {
                            show: false,
                            value: {
                                show: false,
                                offsetY: 0,
                                fontSize: '9px',
                            },
                            total: {
                                show: false,
                                showAlways: false,
                                label: 'Total',
                                fontSize: '12px',
                                formatter: function(w) {
                                    return '1200/1500'
                                }
                            }
                        }
                    }
                }
            }
        }

        var chart = new ApexCharts(document.querySelector("#gauge1"), options);
        chart.render();

        let chart2 = new ApexCharts(document.querySelector("#gauge2"), options);
        chart2.render();
    </script>
@endpush
