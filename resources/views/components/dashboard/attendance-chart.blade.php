<div class="card card-shadow" style="background-color: #9ed8db !important; height: 100%;">
    <div class="card-body">
        <div class="row" style="height: 100% !important;">
            <div class="col-4 align-self-end" style="word-wrap: normal;">
                <h2 class="text-white" style="font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; ">
                    Orientation Attendance</h2>
            </div>
            <div class="col-8">
                <div id="attendance"></div>
            </div>
        </div>
    </div>
</div>

@push('styles')
    <style>
        .apexcharts-tooltip span {
            color: #000000;
        }
    </style>
@endpush
@push('scripts')
    <script type="text/javascript">
        var element = document.getElementById('attendance');

        var height = parseInt(KTUtil.css(element, 'height'));
        var labelColor = KTUtil.getCssVariableValue('--kt-gray-500');
        var borderColor = KTUtil.getCssVariableValue('--kt-gray-200');
        var baseColor = KTUtil.getCssVariableValue('--kt-primary');
        var secondaryColor = KTUtil.getCssVariableValue('--kt-gray-300');

        if (parseInt('{{ $completed }}') == 0) {
            var series = [
                100
            ]
            var colors = ['#dbf0f1']
            var labels = ['% Not Attended']
        } else {
            var series = [
                parseInt('{{ $completed }}'),
                parseInt('{{ 100 - $completed }}')
            ]
            var colors = ['#FFFFFF', '#dbf0f1']
            var labels = ['% Attended', '% Not Attended']
        }

        var options = {
            series: series,
            colors: colors,
            chart: {
                type: 'donut',
            },
            dataLabels: {
                enabled: false,
            },
            labels: labels,
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        show: false,
                        position: 'bottom'
                    }
                }
            }],
            legend: {
                show: false,
                position: 'bottom'
            },
            yaxis: {
                labels: {
                    formatter: function(value) {
                        return value;
                    },
                },
            },
            @if ($completed == 0)
                tooltip: {
                    enabled: false
                },
            @endif

            @if ($completed != 0)
                tooltip: {
                    y: {
                        formatter: function(value) {
                            return value + "%";
                        },
                        title: {
                            formatter: function(seriesName) {
                                return '';
                            }
                        }
                    },
                },
            @endif
            plotOptions: {
                pie: {
                    donut: {
                        labels: {
                            show: false,
                            value: {
                                show: false,
                                offsetY: 0,
                                fontSize: '9px',
                            },
                            total: {
                                show: false,
                                showAlways: false,
                                label: 'Total',
                                fontSize: '12px',
                                formatter: function(w) {
                                    return '1200/1500'
                                }
                            }
                        }
                    }
                }
            }
        }

        var chart = new ApexCharts(element, options);
        chart.render();
    </script>
@endpush
