<div class="row g-5 g-xl-10 mb-xl-10 mb-5">
    <div class="@if (!$isVisible) col-md-12 col-lg-12 col-xl-12 col-xxl-12 @else col-lg-8 col-12 @endif">
        {{-- <x-dashboard.points-overview></x-dashboard.points-overview> --}}

        <x-dashboard.points-graph></x-dashboard.points-graph>
    </div>
    <div id="quick-links"
        class="@if ($isVisible) d-block col-md-4 col-lg-4 col-xl-4 col-xxl-4 mb-md-8 mb-xl-10 @else d-none @endif">
        <div class="card card-shadow" style="background-color: transparent !important;">
            <div class="card-body">
                <h2 class="card-title align-items-start flex-column">
                    <span class="card-label fw-bold text-dark fs-1"
                        style="font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; ">Quick Links</span>
                </h2>

                <div id="links">
                    @php
                        $links = [
                            [
                                'href' => '',
                                'img' => 'flame.png',
                                'title' => 'FLAME',
                                'id' => 'flame',
                            ],
                            [
                                'href' => '',
                                'img' => 'clubs_and_societies.png',
                                'title' => 'CLUBS & SOCIETIES',
                                'id' => 'clubs_n_societies',
                            ],
                            [
                                'href' => '',
                                'img' => 'student_id.png',
                                'title' => 'STUDENT ID',
                                'id' => 'student_id',
                            ],
                            [
                                'href' => '',
                                'img' => 'campus_central.png',
                                'title' => 'CAMPUS CENTRAL',
                                'id' => 'campus_central',
                            ],
                            [
                                'href' => '',
                                'img' => 'international_students.png',
                                'title' => 'INTERNATIONAL STUDENTS',
                                'id' => 'international_students',
                            ],
                            [
                                'href' => '',
                                'img' => 'unigym.png',
                                'title' => 'UNIGYM',
                                'id' => 'unigym',
                            ],
                            [
                                'href' => '',
                                'img' => 'missed_orientation.png',
                                'title' => 'MISSED ORIENTATION?',
                                'id' => 'missed_orientation',
                            ],
                        ];
                    @endphp

                    @foreach ($links as $link)
                        <div class="card card-shadow" style="background-color: transparent !important;">
                            <div class="card-body d-flex align-items-center flex-row gap-5">
                                <div style="height: 5vh !important;">
                                    <img src="{{ asset('media/icons/taylors/quicklinks/' . $link['img']) }}"
                                        alt="" height="100%">
                                </div>
                                <span class="quick-link" style="cursor: pointer;">
                                    <div data-title="{{ $link['title'] }}'" hidden>{!! setting('quicklink.' . $link['id']) !!}</div>
                                    <h4 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bold text-dark fs-5">{{ $link['title'] }}</span>
                                    </h4>
                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</div>
