<div id="points-graph">
    <div class="card card-shadow" style="background-color: transparent !important;">
        <div class="card-body">
            <h2 class="card-title align-items-start flex-column">
                <span class="card-label fw-bold text-dark fs-1"
                    style="font-family: Century Gothic,CenturyGothic,AppleGothic,sans-serif; ">Points
                    Stats</span>

                <x-tooltip.basic
                    title='Point Scorecard: Check your points earned from pre-orientation and orientation activities. Each completed activity counts!'>
                </x-tooltip.basic>
            </h2>

            <div id="points-chart">
            </div>

            <style>
                .apexcharts-text {
                    font-family: Century Gothic, CenturyGothic, AppleGothic, sans-serif;
                    font-weight: bolder;
                }
            </style>
        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript" defer>
        var element = document.getElementById('points-chart');

        var height = parseInt(KTUtil.css(element, 'height'));
        var labelColor = KTUtil.getCssVariableValue('--kt-gray-500');
        var borderColor = KTUtil.getCssVariableValue('--kt-gray-200');
        var baseColor = KTUtil.getCssVariableValue('--kt-primary');
        var secondaryColor = KTUtil.getCssVariableValue('--kt-gray-300');

        var options = {
            series: [{
                name: "Points",
                data: [
                    {{ optional($studentPoints->where('name', 'pre_orientation_survey')->first())->point ?? 0 }},
                    {{ optional($studentPoints->where('name', 'pre_orientation_quiz')->first())->point ?? 0 }},
                    {{ optional($studentPoints->where('name', 'orientation_attendance')->first())->point ?? 0 }},
                    {{ optional($studentPoints->where('name', 'orientation_rating')->first())->point ?? 0 }},
                    {{ optional($studentPoints->where('name', 'post_orientation_survey')->first())->point ?? 0 }},
                ],
            }],
            colors: ['#264653', '#2a9d8f', '#e9c46a', '#f4a261', '#e76f51'],
            chart: {
                height: 350,
                type: 'bar',
                fontFamily: 'Century Gothic, CenturyGothic, AppleGothic, sans-serif',
                fontWeight: 'bold',
            },
            plotOptions: {
                bar: {
                    columnWidth: '45%',
                    distributed: true,
                }
            },
            legend: {
                show: false,
            },
            xaxis: {
                categories: ['Pre-Orientation Survey', 'Pre-Orientation Quiz', 'Orientation Attendance',
                    'Orientation Ratings', 'Post Orientation Survey'
                ],
            },
        };

        var chart = new ApexCharts(element, options);
        chart.render();
    </script>
@endpush

@push('styles')
    <style>
        /* if devices is small */
        @media (max-width: 767px) {
            #points-chart .apexcharts-xaxis-label {
                font-size: 8px !important;
            }
        }
    </style>
@endpush
