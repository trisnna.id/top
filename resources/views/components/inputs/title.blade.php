@if ($grouping)
    <div class="mb-10">
        <label
            for="{{ $attributes['id'] }}"
            class="{{ $isRequired($attributes['required']) ? 'required' : '' }} form-label"
        >
            {{ $attributes['label'] ?? __('Title') }}
        </label>
        <input
            id="{{ $attributes['id'] }}"
            name="{{ $attributes['name'] ?? 'title' }}"
            type="{{ $attributes['type'] ?? 'text' }}"
            class="form-control form-control-solid"
            placeholder="{{ $attributes['placeholder'] }}"
            value="{{ $attributes['value'] }}"
        />
    </div>
@endif
