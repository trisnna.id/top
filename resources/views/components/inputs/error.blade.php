@forelse ($errors->get($name) as $message)
    <div class="fv-plugins-message-container invalid-feedback d-block">
        {{ $message }}
    </div>
@empty
    {{-- don't render anything --}}
@endforelse
