<div>
    <label class="form-label fw-semibold">Programme:</label>
    <select name="filterProgrammes" class="form-select form-select2" id="filterProgrammes" data-hide-search="true"
        data-allow-clear="true" data-placeholder="-- Please Select / All --" multiple="multiple">
        @foreach ($options as $programme)
            <option value="{{ $programme->id }}">{{ $programme->name }}</option>
        @endforeach
    </select>
</div>
