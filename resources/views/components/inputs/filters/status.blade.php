<div>
    <label class="form-label fw-semibold">Status:</label>
    <select class="form-select form-select-solid form-select2" data-kt-select2="true"
        data-placeholder="-- Choose status --"
        data-allow-clear="true">
        <option></option>
        @foreach ($options as $option)
            <option value="{{ $option->get('value') }}">{{ $option->get('label') }}</option>
        @endforeach
    </select>
</div>
