<label class="form-label fw-semibold">Schools:</label>
<select multiple class="form-select form-select2" data-hide-search="true" data-allow-clear="true"
    data-placeholder="-- Please Select / All --">
    <option></option>
    <option value="1">Taylor's Business School</option>
    <option value="2">School of Liberal Arts and Sciences</option>
</select>
