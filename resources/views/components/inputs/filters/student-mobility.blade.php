<label class="form-label fw-semibold">Mobilities:</label>
<select multiple class="form-select form-select2" data-hide-search="true" data-allow-clear="true"
    data-placeholder="-- Please Select / All --">
    <option></option>
    <option value="1">None</option>
    <option value="2">Exchange Student</option>
    <option value="3">Study Abroad Student</option>
</select>
