<div class="col-md-12 fv-row fv-plugins-icon-container">
    <label class="form-label fw-semibold">Nationality:</label>
    <select name="filterNationality" class="form-select form-select2" id="filterNationality" data-hide-search="true"
        data-allow-clear="true" data-placeholder="-- Please Select / All --" multiple="multiple">
        @foreach ($options as $locality)
            <option value="{{ $locality->id }}">{{ $locality->name }}</option>
        @endforeach
    </select>
</div>
