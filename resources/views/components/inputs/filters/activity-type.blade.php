<label class="form-label fw-semibold">Activity Types:</label>
<select multiple class="form-select form-select2" data-hide-search="true" data-allow-clear="true"
    data-placeholder="-- Please Select / All --">
    <option></option>
    <option value='1'>Compulsory Core</option>
    <option value='2'>Complementary</option>
</select>
