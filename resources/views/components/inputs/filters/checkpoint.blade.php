<div>
    <label class="form-label fw-semibold">Milestone:</label>
    <select name="filterWeeks" class="form-select form-select2" id="filterWeeks" data-hide-search="true"
        data-allow-clear="true" data-placeholder="-- Please Select / All --" multiple="multiple">
        @foreach ($options as $checkpoint)
            <option value="{{ $checkpoint->id }}">{{ $checkpoint->label }}</option>
        @endforeach
    </select>
</div>
