<div>
    <label class="form-label fw-semibold">Status:</label>
    <select name="filterStatuses" class="form-select form-select2" id="filterStatuses" data-hide-search="true"
        data-allow-clear="true" data-placeholder="-- Please Select / All --" multiple="multiple">
        <option value="draft">Draft</option>
        <option value="published">Published</option>
        <option value="unpublished">Unpublished</option>
    </select>
</div>
