<label class="form-label fw-semibold">Faculties:</label>
<select multiple class="form-select form-select2" data-hide-search="true" data-allow-clear="true"
    data-placeholder="-- Please Select / All --">
    <option></option>
    <option value="1">Faculty of Health & Medical Sciences</option>
    <option value="2">Faculty of Social Sciences & Leisure Management</option>
</select>
