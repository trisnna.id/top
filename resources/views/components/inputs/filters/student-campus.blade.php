<div>
    <label class="form-label fw-semibold">Campus:</label>
    <select name="filterCampuses" class="form-select form-select2" id="filterCampuses" data-hide-search="true"
        data-allow-clear="true" data-placeholder="-- Please Select / All --" multiple="multiple">
        @foreach ($options as $campus)
            <option value="{{ $campus->id }}">{{ $campus->name }}</option>
        @endforeach
    </select>
</div>
