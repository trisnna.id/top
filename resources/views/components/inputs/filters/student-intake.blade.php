<div class="col-md-12 fv-row fv-plugins-icon-container">
    <label class="form-label fw-semibold">Intakes:</label>
    <select name="filterIntakes" class="form-select form-select2" id="filterIntakes" data-hide-search="true"
        data-allow-clear="true" data-placeholder="-- Please Select / All --" multiple="multiple">
        @foreach ($options as $intake)
            <option value="{{ $intake->id }}">{{ $intake->name }}</option>
        @endforeach
    </select>
</div>
