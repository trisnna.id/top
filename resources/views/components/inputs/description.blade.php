@if ($grouping)
    <div class="mb-10">
        <label
            for="{{ $attributes['id'] }}"
            class="{{ $isRequired($attributes['required']) ? 'required' : '' }} form-label"
        >
            {{ $attributes['label'] ?? __('Description') }}
        </label>
        <textarea
            id="{{ $attributes['id'] }}"
            name="{{ $attributes['name'] ?? 'description' }}"
            type="{{ $attributes['type'] ?? 'text' }}"
            class="form-control form-control-solid"
            placeholder="{{ $attributes['placeholder'] }}"
            value="{{ $attributes['value'] }}"
        >{{ $attributes['value'] }}</textarea>
    </div>
@endif