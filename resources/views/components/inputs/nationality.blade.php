<div class="{{ $containerClassName }}">
    <label class="fs-5 fw-bold form-label mb-2">
        <span class="required">{{ __('Nationality') }}</span>
    </label>
    <select class="form-select {{ $moduleName }}-select2 {{ $renderError($errors->has($name)) }} all-support"
        id="nationality" name="nationality[]" multiple="multiple" data-old="{{ $value }}">
        <option value="all">{{ __('All') }}</option>
        @foreach ($options as $locality)
            <option value="{{ $locality->id }}">{{ $locality->name }}</option>
        @endforeach
    </select>
</div>
<x-inputs.error :name="$name" />
