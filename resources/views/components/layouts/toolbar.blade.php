<div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
        <h1 class="page-heading d-flex text-white fw-bold fs-3 flex-column justify-content-center my-0">
            {{ $title }}</h1>
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
            @forelse ($navs as $nav)
                @if ($nav->has('link'))
                    <li class="breadcrumb-item text-muted">
                        <a href="{{ $nav->get('link') }}" class="text-white text-hover-white">
                            {{ $nav->get('label') }}
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                @else
                    <li class="breadcrumb-item text-white">{{ $nav->get('label') }}</li>
                @endif
            @empty
                {{ __('Page is not registered') }}
            @endforelse
        </ul>
    </div>
</div>
