<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-theme="light">
	<head>
        <title>{{"{$title} | ".config('app.name')}}</title>
		<x-layouts.app.meta :ogTitle="$title.' | '.config('app.name')"/>
        <x-layouts.app.styles />
	</head>
    <x-layouts.app.body :title="$title" :toolbar="$toolbar" :toolbarStyle="$toolbarStyle" :toolbarClass="$toolbarClass" :showFooter="$showFooter">
        {!! $slot !!}
    </x-layouts.app.body>
</html>