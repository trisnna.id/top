<div id="kt_app_header" class="app-header">
    <div class="app-container container-xxl d-flex align-items-stretch justify-content-between">
        <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0 me-lg-15">
            <a class="" href="{{ url('/') }}">
                <img alt="Logo" src="{{ asset('media/logos/default-small-white.png') }}"
                    class="h-50px app-sidebar-logo-default" />
            </a>
        </div>
        <div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1" id="kt_app_header_wrapper">
            <div class="app-header-menu app-header-mobile-drawer align-items-stretch" data-kt-drawer="true"
                data-kt-drawer-name="app-header-menu" data-kt-drawer-activate="{default: true, lg: false}"
                data-kt-drawer-overlay="true" data-kt-drawer-width="225px" data-kt-drawer-direction="end"
                data-kt-drawer-toggle="#kt_app_header_menu_toggle" data-kt-swapper="true"
                data-kt-swapper-mode="{default: 'append', lg: 'prepend'}"
                data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}">
                <x-layouts.app.body.header.menu />
            </div>
            <div class="app-navbar flex-shrink-0">
                <style>
                    #faqMenu:hover #faqText {
                        color: #f00 !important;
                    }
                </style>
                <x-layouts.app.body.header.notification />
                <div class="app-navbar-item ms-1 ms-md-3">
                    <a title="FAQ" href="{{ route('faqs.index') }}" id='faqMenu' data-bs-toggle="tooltip"
                        data-bs-placement="bottom" data-bs-custom-class="custom-tooltip" data-bs-title="FAQ"
                        class="btn btn-icon btn-custom btn-icon-white btn-active-light btn-active-color-danger w-40px h-40px position-relative {{ routeSlugActive('faqs.index', 'show menu-dropdown') }}">
                        <i class="{{ getConstant('IconConstant', 'icon_class', 'FAQ') }} fs-2"></i>
                        {{-- <span class="position-absolute start-50 translate-middle fs-10 top-80 text-white"
                            id='faqText'>FAQ</span> --}}


                    </a>
                </div>
                <div class="app-navbar-item ms-1 ms-md-3">
                    <a title="Contact Us" target="_blank"
                        href="https://forms.office.com/Pages/ResponsePage.aspx?id=E-45CidcDEKwr45lxpKQVTtxbPPS2AtCqjeyHy1jajtUNEhENUVDWDRQMVJKT05SOUhXOVQ3UU0yNi4u"
                        id='contactUsMenu' data-bs-toggle="tooltip" data-bs-placement="bottom"
                        data-bs-custom-class="custom-tooltip" data-bs-title="Contact Us"
                        class="btn btn-icon btn-custom btn-icon-white btn-active-light btn-active-color-danger w-40px h-40px position-relative">
                        <i class="{{ getConstant('IconConstant', 'icon_class', 'CONTACT_US') }} fs-2"></i>
                    </a>
                </div>
                <div class="app-navbar-item ms-1 ms-lg-3" id="kt_header_user_menu_toggle" dusk="userMenu">
                    <div class="symbol symbol-35px symbol-md-40px cursor-pointer" data-kt-menu-trigger="click"
                        data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                        @if (
                            !empty(auth()->user()->getFirstMedia('avatar')
                            ))
                            <img src="{{ auth()->user()->getFirstMediaUrl('avatar') }}">
                        @else
                            <div
                                class="symbol-label fs-3 bg-light-{{ session()->get('profile-color', 'success') }} text-{{ session()->get('profile-color', 'success') }}">
                                {{ upperSubstr(auth()->user()->name) }}</div>
                        @endif
                    </div>
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold fs-6 w-275px py-4"
                        data-kt-menu="true">
                        <div class="menu-item px-3">
                            <div class="menu-content d-flex align-items-center px-3">
                                <div class="symbol symbol-50px me-5">
                                    @if (
                                        !empty(auth()->user()->getFirstMedia('avatar')
                                        ))
                                        <img src="{{ auth()->user()->getFirstMediaUrl('avatar') }}">
                                    @else
                                        <div
                                            class="symbol-label fs-3 bg-light-{{ session()->get('profile-color', 'success') }} text-{{ session()->get('profile-color', 'success') }}">
                                            {{ upperSubstr(auth()->user()->name) }}</div>
                                    @endif
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fw-bold d-flex align-items-center fs-5">
                                        {{ auth()->user()->name ?? 'Guest' }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="separator my-2"></div>
                        <div class="menu-item px-5">
                            <a href="{{ route('profile.index') }}" class="menu-link px-5">My Profile</a>
                        </div>
                        @if (auth()->user()->can('admin') &&
                                auth()->user()->can('student'))
                            <div class="menu-item px-5">
                                <a href="{{ route('admin.dashboard.index') }}" class="menu-link px-5">Go to Dashboard
                                    Admin</a>
                            </div>
                        @endif
                        <div class="separator my-2"></div>
                        <div class="menu-item px-5">
                            <a id="logoutButton" dusk="logoutButton" href="{{ route('logout') }}"
                                class="menu-link px-5"
                                onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
                <div class="app-navbar-item d-lg-none ms-2 me-n3" title="Show header menu">
                    <div class="btn btn-icon w-35px h-35px" id="kt_app_header_menu_toggle">
                        <i class="fa-solid fa-bars fs-1 text-white"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('styles')
    <style>
        .app-navbar-item>a>.custom-tooltip>.tooltip>.tooltip-inner {
            background-color: #f1f1f1 !important;
        }

        .app-navbar-item>a>.custom-tooltip>.tooltip {
            font-family: Century Gothic, CenturyGothic, AppleGothic, sans-serif !important;
            font-weight: bolder !important;
        }
    </style>
@endpush
