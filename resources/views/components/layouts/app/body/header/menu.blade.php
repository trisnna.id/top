<div class="menu menu-rounded menu-column menu-lg-row my-lg-0 align-items-stretch fw-semibold px-lg-0 my-5 px-2"
    id="kt_app_header_menu" data-kt-menu="true">

    <style>
        .app-header-menu .menu .menu-item .menu-link .menu-title {
            color: var(--kt-white) !important;
        }

        /* from sm device until md device, duplicate above rule, but with var --kt-black */
        @media (min-width: 1px) and (max-width: 991px) {
            .app-header-menu .menu .menu-item .menu-link .menu-title {
                color: var(--kt-black) !important;
            }
        }

        .app-header-menu .menu .here .menu-link .menu-title {
            color: var(--kt-black) !important;
        }

        .app-header-menu .menu .menu-item .menu-link:hover .menu-title {
            color: var(--kt-black) !important;
        }

        .header-student {
            color: #fff;
        }
    </style>

    <div class="menu-item {{ routeSlugActive('dashboard.index', 'here ') }} show">
        <a href="{{ route('dashboard.index') }}" class="menu-link">
            <span class="menu-title">
                <b>Dashboard</b>
            </span>
        </a>
    </div>

    <div class="menu-item {{ routeSlugActive('student.preorientation.index', 'here ') }} show text-center">
        <a href="{{ route('student.preorientation.index') }}" class="menu-link">
            <span class="menu-title">
                <b>{{ __('Pre-Orientation') }}</b>
            </span>
        </a>
    </div>
    <div class="menu-item {{ routeSlugActive('orientations.index2', 'here ') }} show text-center">
        <a href="{{ route('orientations.index2') }}" class="menu-link">
            <span class="menu-title">
                <b>Orientation Activities</b>
            </span>
        </a>
    </div>
    <div class="menu-item {{ routeSlugActive('orientations.index', 'here ') }} show text-center">
        <a href="{{ route('orientations.index') }}" class="menu-link">
            <span class="menu-title">
                <b>Timetable</b>
            </span>
        </a>
    </div>
    <div class="menu-item {{ routeSlugActive('arrivals.*', 'here ') }} show text-center">
        <a href="{{ route('arrivals.index') }}" class="menu-link">
            <span class="menu-title">
                <b>{{ __('Pre-Arrival') }}</b>
            </span>
        </a>
    </div>
</div>
