<div class="app-navbar-item ms-1 ms-lg-3">
    <div title="Notification"
        class="btn btn-icon btn-icon-white btn-custom btn-active-light btn-active-color-danger w-40px h-40px position-relative"
        data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end"
        data-bs-toggle="tooltip" data-bs-placement="bottom" data-bs-custom-class="custom-tooltip"
        data-bs-title="Notification">
        <i class="fas fa-bell fs-2"></i>
        @if ($countNotificationsByReadAtNull)
            <span
                class="position-absolute start-80 translate-middle fs-9 top-20">{{ $countNotificationsByReadAtNull ?? 0 }}</span>
        @endif
    </div>
    <div class="menu menu-sub menu-sub-dropdown menu-column w-350px w-lg-375px" data-kt-menu="true">
        <div class="d-flex flex-column bgi-no-repeat rounded-top"
            style="background-image:url('{{ url('media/misc/pattern-1.jpg') }}')">
            <h3 class="fw-semibold mt-10 mb-6 px-9 text-white">
                Notification
            </h3>
        </div>
        <div>
            <div class="scroll-y mh-325px my-5 px-8">
                @forelse ($notifications as $notification)
                    <div class="d-flex flex-stack py-4">
                        <div class="d-flex align-items-center">
                            <div class="me-2 mb-0">
                                @if (!empty($notification->url))
                                    @if (empty($notification->read_at))
                                        <a href="{{ $notification->url }}" class="text-black"
                                            data-id="{{ $notification->id }}"
                                            onclick="event.preventDefault();modules.profile.notification.index.updateRead(this)">{!! $notification->html !!}</a>
                                    @else
                                        <a class="text-black"
                                            href="{{ $notification->url }}">{!! $notification->html !!}</a>
                                    @endif
                                @else
                                    <div>{!! $notification->html !!}</div>
                                @endif
                            </div>
                        </div>
                        <span class="badge badge-light">{{ $notification->created_at->diffForHumans() }}</span>
                    </div>
                @empty
                    <div class="d-flex flex-stack py-4">
                        <div class="d-flex align-items-center">
                            <div class="me-2 mb-0">
                                <div>No Notification Available</div>
                            </div>
                        </div>
                    </div>
                @endforelse
            </div>
            <div class="border-top py-3 text-center">
                <a href="{{ route('profile.notification.index') }}"
                    class="btn btn-color-gray-600 btn-active-color-primary">View All
                    <span class="svg-icon svg-icon-5">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="18" y="13" width="13" height="2"
                                rx="1" transform="rotate(-180 18 13)" fill="currentColor"></rect>
                            <path
                                d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z"
                                fill="currentColor"></path>
                        </svg>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
