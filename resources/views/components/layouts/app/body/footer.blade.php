@if (request()->route()->getName() == 'portal.index')
    <div class="app-footer-link">
        <div class="app-container container-xxl py-6">
            <div class="row mb-sm-3 mb-md-0">
                <div class="col-md-8">
                    <h1 class="title fw-bold">External Links</h1>
                    <div class="row">
                        @if (!empty(setting('portal.link_mobile')))
                            <li class="col-md-6 d-flex align-items-center py-2">
                                <span class="bullet me-5"></span> <a href="{{ setting('portal.link_mobile') }}"
                                    class="fw-semibold fs-6 me-2 text-black">Taylor’s Mobile App</a>
                            </li>
                        @endif
                        @if (!empty(setting('portal.link_times')))
                            <li class="col-md-6 d-flex align-items-center py-2">
                                <span class="bullet me-5"></span> <a href="{{ setting('portal.link_times') }}"
                                    class="fw-semibold fs-6 me-2 text-black">myTIMeS</a>
                            </li>
                        @endif
                        @if (!empty(setting('portal.link_campus')))
                            <li class="col-md-6 d-flex align-items-center py-2">
                                <span class="bullet me-5"></span> <a href="{{ setting('portal.link_campus') }}"
                                    class="fw-semibold fs-6 me-2 text-black">Campus Central</a>
                            </li>
                        @endif
                        @if (!empty(setting('portal.link_virtual')))
                            <li class="col-md-6 d-flex align-items-center py-2">
                                <span class="bullet me-5"></span> <a href="{{ setting('portal.link_virtual') }}"
                                    class="fw-semibold fs-6 me-2 text-black">Virtual Campus Tour Link</a>
                            </li>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <h1 class="title fw-bold">Follow Us</h1>
                    <ul
                        class="menu menu-gray-600 menu-hover-primary fw-semibold align-items-center mb-md-0 order-1 mb-3">
                        @if (!empty(setting('portal.sosmed_youtube')))
                            <li class="menu-item">
                                <a href="{{ setting('portal.sosmed_youtube') }}" target="_blank" class="menu-link px-3">
                                    <img src="{{ asset('media/svg/social-logos/youtube.svg') }}" class="h-20px">
                                </a>
                            </li>
                        @endif
                        @if (!empty(setting('portal.sosmed_twitter')))
                            <li class="menu-item">
                                <a href="{{ setting('portal.sosmed_twitter') }}" target="_blank" class="menu-link px-3">
                                    <img src="{{ asset('media/svg/social-logos/twitter.svg') }}" class="h-20px">
                                </a>
                            </li>
                        @endif
                        @if (!empty(setting('portal.sosmed_instagram')))
                            <li class="menu-item">
                                <a href="{{ setting('portal.sosmed_instagram') }}" target="_blank"
                                    class="menu-link px-3">
                                    <img src="{{ asset('media/svg/social-logos/instagram.svg') }}" class="h-20px">
                                </a>
                            </li>
                        @endif
                        @if (!empty(setting('portal.sosmed_facebook')))
                            <li class="menu-item">
                                <a href="{{ setting('portal.sosmed_facebook') }}" target="_blank"
                                    class="menu-link px-3">
                                    <img src="{{ asset('media/svg/social-logos/facebook.svg') }}" class="h-20px">
                                </a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="app-footer">
    <div class="app-container container-fluid d-flex flex-column flex-md-row flex-center flex-md-stack py-3">
        <div class="text-white">
            <span class="fw-semibold me-1">Copyright 2023©</span>
            <span>{{ config('app.name') }}. All right reserved</span>
        </div>
        <ul class="menu menu-gray-600 menu-hover-primary fw-semibold order-1">
            <li class="menu-item">
                <a href="{{ route('page.policy') }}" class="menu-link px-2 text-white">Privacy Policy</a>
            </li>
            <li class="menu-item">
                <a href="{{ route('page.disclaimer') }}" class="menu-link px-2 text-white">Disclaimers</a>
            </li>
        </ul>
    </div>
</div>
