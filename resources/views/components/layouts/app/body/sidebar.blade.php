<div class="app-sidebar flex-column" data-kt-drawer="true" data-kt-drawer-name="app-sidebar"
    data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="225px"
    data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle">
    <div class="app-sidebar-logo px-6" id="kt_app_sidebar_logo">
        <a class="sidebar-brand" href="{{ url('/') }}">
            <img alt="Logo" src="{{ asset('media/logos/default-white.png') }}" class="h-55px app-sidebar-logo-default" />
            <img alt="Logo" src="{{ asset('media/logos/default-small-white.png') }}" class="h-50px app-sidebar-logo-minimize" />
        </a>
        <div id="kt_app_sidebar_toggle"
            class="app-sidebar-toggle btn btn-icon btn-shadow btn-sm btn-color-muted btn-active-color-primary body-bg h-30px w-30px position-absolute top-150 start-100 translate-middle rotate"
            data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body"
            data-kt-toggle-name="app-sidebar-minimize">
            <span class="svg-icon svg-icon-2 rotate-180">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.5"
                        d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z"
                        fill="currentColor" />
                    <path
                        d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z"
                        fill="currentColor" />
                </svg>
            </span>
        </div>
    </div>
    <div class="app-sidebar-menu overflow-hidden flex-column-fluid bg-delft-blue">
        <div id="kt_app_sidebar_menu_wrapper" class="app-sidebar-wrapper hover-scroll-overlay-y my-5"
            data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto"
            data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer"
            data-kt-scroll-wrappers="#kt_app_sidebar_menu" data-kt-scroll-offset="5px" data-kt-scroll-save-state="true">
            <div class="menu menu-column menu-rounded menu-sub-indention px-3" id="#kt_app_sidebar_menu"
                data-kt-menu="true" data-kt-menu-expand="false">
                <li class="menu-item {{ routeSlugActive('dashboard.index', 'here show') }}">
                    <a class="menu-link" href="{{ auth()->user()->can('admin') && auth()->user()->can('student') ? route('admin.dashboard.index') : route('dashboard.index') }}">
                        <i class="fas fa-home menu-icon"></i>
                        <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                @canany(['manage_users', 'manage_roles'])
                    <div data-kt-menu-trigger="click"
                        class="menu-item menu-accordion {{ routeSlugActive(['admin.roles.index', 'admin.users.index'], 'here show') }}">
                        <span class="menu-link">
                            <i class="fas fa-user-friends menu-icon"></i>
                            <span class="menu-title">User & Role Management</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-accordion">
                            @can('manage_users')
                                <div class="menu-item {{ routeSlugActive('admin.users.index', 'here show') }}">
                                    <a href="{{ route('admin.users.index') }}" class="menu-link">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">
                                            User Management
                                        </span>
                                    </a>
                                </div>
                            @endcan
                            @canany('manage_roles')
                                <div class="menu-item {{ routeSlugActive('admin.roles.index', 'here show') }}">
                                    <a href="{{ route('admin.roles.index') }}" class="menu-link">
                                        <span class="menu-bullet">
                                            <span class="bullet bullet-dot"></span>
                                        </span>
                                        <span class="menu-title">
                                            Role Management
                                        </span>
                                    </a>
                                </div>
                            @endcan
                        </div>
                    </div>
                @endcanany
                @can('manage_announcements')
                    <li class="menu-item {{ routeSlugActive('admin.announcements.index', 'here show') }}"
                        aria-haspopup="true">
                        <a href="{{ route('admin.announcements.index') }}" class="menu-link ">
                            <i class="{{ getConstant('IconConstant', 'icon_class', 'ANNOUNCEMENT') }} menu-icon"></i>
                            <span class="menu-title">
                                {{ __('Announcements') }}
                            </span>
                        </a>
                    </li>
                @endcan
                @can('manage_resources')
                    <li class="menu-item {{ routeSlugActive('admin.arrivals.*', 'here show') }}" aria-haspopup="true">
                        <a href="{{ route('admin.arrivals.index') }}" class="menu-link ">
                            <i class="{{ getConstant('IconConstant', 'icon_class', 'RESOURCE') }} menu-icon"></i>
                            <span class="menu-title">
                                {{ __('Pre-Arrival') }}
                            </span>
                        </a>
                    </li>
                @endcan
                @can('manage_pre_orientations')
                    <li class="menu-item {{ routeSlugActive('admin.preorientation.index', 'here show') }}"
                        aria-haspopup="true">
                        <a href="{{ route('admin.preorientation.index') }}" class="menu-link ">
                            <i class="{{ getConstant('IconConstant', 'icon_class', 'PRE_ORIENTATION') }} menu-icon"></i>
                            <span class="menu-title">
                                {{ __('Pre-Orientation') }}
                            </span>
                        </a>
                    </li>
                @endcan
                @canany(['view_orientations'])
                    <li class="menu-item {{ routeSlugActive(['admin.orientations.index2','admin.orientations.show2'], 'here show') }}" aria-haspopup="true">
                        <a href="{{ route('admin.orientations.index2') }}" class="menu-link ">
                            <i class="{{ getConstant('IconConstant', 'icon_class', 'ORIENTATION') }} menu-icon"></i>
                            <span class="menu-title">
                                {{ __('Orientation Activity') }}
                            </span>
                        </a>
                    </li>
                @endcanany
                @canany(['manage_orientations', 'manage_activity_approval'])
                    <li class="menu-item {{ !in_array(request()->route()->getName(),["admin.orientations.index2","admin.orientations.show2"]) ? routeSlugActive('admin.orientations.*', 'here show') : null }}" aria-haspopup="true">
                        <a href="{{ route('admin.orientations.index') }}" class="menu-link ">
                            <i class="{{ getConstant('IconConstant', 'icon_class', 'ORIENTATION') }} menu-icon"></i>
                            <span class="menu-title">
                                {{ __('Orientation Timetable') }}
                            </span>
                        </a>
                    </li>
                @endcanany
                @can('manage_student_lists')
                    <li class="menu-item {{ routeSlugActive('admin.students.*', 'here show') }}" aria-haspopup="true">
                        <a href="{{ route('admin.students.index') }}" class="menu-link ">
                            <i class="{{ getConstant('IconConstant', 'icon_class', 'STUDENT') }} menu-icon"></i>
                            <span class="menu-title">
                                {{ __('Student List') }}
                            </span>
                        </a>
                    </li>
                    @can('admin')
                    <li class="menu-item {{ routeSlugActive('admin.point.students', 'here show') }}" aria-haspopup="true">
                        <a href="{{ route('admin.point.students') }}" class="menu-link ">
                            <i class="{{ getConstant('IconConstant', 'icon_class', 'POINT') }} menu-icon"></i>
                            <span class="menu-title">
                                {{ __('Student Points') }}
                            </span>
                        </a>
                    </li>
                    @endcan
                @endcan
                @can('manage_resources')
                    <li class="menu-item {{ routeSlugActive('admin.resources.*', 'here show') }}" aria-haspopup="true">
                        <a href="{{ route('admin.resources.index') }}" class="menu-link ">
                            <i class="{{ getConstant('IconConstant', 'icon_class', 'RESOURCE') }} menu-icon"></i>
                            <span class="menu-title">
                                {{ __('FAQ') }}
                            </span>
                        </a>
                    </li>
                @endcan
                @canany(['access_report_login','access_report_attendance','access_report_registration','access_report_activity','access_report_survey', 'access_report_gamification'])
                    <div data-kt-menu-trigger="click"
                        class="menu-item menu-accordion {{ routeSlugActive('admin.reports.*', 'here show') }}">
                        <span class="menu-link">
                            <i class="fa-solid fa-chart-pie menu-icon"></i>
                            <span class="menu-title">{{ __('Reports') }}</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-accordion">
                            @foreach ([
                                ['title' => 'Audit Trails', 'routeName' => 'admin.reports.audit-trail.index', 'permissionName' => 'access_report_audit'],
                                ['title' => 'Student Login Details', 'routeName' => 'admin.reports.student-login.index', 'permissionName' => 'access_report_login'],
                                ['title' => 'Student Attendance', 'routeName' => 'admin.reports.student-attendance.index', 'permissionName' => 'access_report_attendance'],
                                ['title' => 'Student Registration', 'routeName' => 'admin.reports.student-registration.index', 'permissionName' => 'access_report_registration'],
                                ['title' => 'Student Activity Log', 'routeName' => 'admin.reports.student-activity.index', 'permissionName' => 'access_report_activity'],
                                ['title' => 'Student Survey Results', 'routeName' => 'admin.reports.student-survey.index', 'permissionName' => 'access_report_survey'],
                                ['title' => 'No of View', 'routeName' => 'admin.reports.view-page.index', 'permissionName' => ''],
                                // ['title' => 'Gamification Data', 'routeName' => 'admin.reports.gamification.index', 'permissionName' => 'access_report_gamification'],
                            ] as $item)
                                @if(!empty($item['permissionName'] ?? null) && auth()->user()->can($item['permissionName']))
                                    <div class="menu-item {{ routeSlugActive($item['routeName'], 'here show') }}">
                                        <a href="{{ route($item['routeName']) }}" class="menu-link">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                            <span class="menu-title">
                                                {{ $item['title'] }}
                                            </span>
                                        </a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endcanany
                @can('manage_settings')
                    <li class="menu-item {{ routeSlugActive('admin.settings.*', 'here show') }}" aria-haspopup="true">
                        <a href="{{ route('admin.settings.mails.index') }}" class="menu-link ">
                            <i class="fa-solid fa-gear menu-icon"></i>
                            <span class="menu-title">
                                {{ __('Settings') }}
                            </span>
                        </a>
                    </li>
                @endcan
            </div>
        </div>
    </div>
</div>
