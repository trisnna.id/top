<script>
    var hostUrl = "app/";
    window.ConfigApp = {
        "timezone": "{{ config('app.timezone') }}",
        "dateFormat": 'DD/MM/YYYY',
        "timeFormat": 'h:mm a',
        "calendarClientId": "{{ config('services.google.calendar.client_id') }}",
        "calendarApiKey": "{{ config('services.google.calendar.api_key') }}",
    };
</script>
<!--begin::Global Javascript Bundle(mandatory for all pages)-->
<script src="{{ asset(mix('plugins/global/plugins.bundle.js')) }}"></script>
<script src="{{ asset('js/scripts.bundle.js') }}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Vendors Javascript(used for this page only)-->
<script src="{{ asset(mix('plugins/custom/fullcalendar/fullcalendar.bundle.js')) }}"></script>
<script src="{{ asset(mix('plugins/custom/fslightbox/fslightbox.bundle.js')) }}"></script>
<script src="{{ asset(mix('plugins/custom/datatables/datatables.bundle.js')) }}"></script>
<script src="{{ asset(mix('plugins/custom/prismjs/prismjs.bundle.js')) }}"></script>
<script src="{{ asset(mix('plugins/custom/tinymce/tinymce.bundle.js')) }}"></script>
<script src="{{ asset(mix('plugins/custom/cropper/cropper.bundle.js')) }}"></script>
<script src="https://apis.google.com/js/api.js"></script>
<script src="https://accounts.google.com/gsi/client"></script>
<script src="{{ asset(mix('js/app.js')) }}"></script>
{{-- <script async defer src="https://apis.google.com/js/api.js" onload="modules.integration.google.calendar.gapiLoaded()"></script>
<script async defer src="https://accounts.google.com/gsi/client" onload="modules.integration.google.calendar.gisLoaded()"></script> --}}

<!--end::Vendors Javascript-->
<!--begin::Custom Javascript(used for this page only)-->
<script src="{{ asset('js/widgets.bundle.js') }}"></script>
{{-- <script src="{{ asset('js/custom/widgets.js') }}"></script> --}}
{{-- <script src="{{ asset('js/custom/apps/chat/chat.js') }}"></script> --}}
{{-- <script src="{{ asset('js/custom/utilities/modals/upgrade-plan.js') }}"></script> --}}
{{-- <script src="{{ asset('js/custom/utilities/modals/create-app.js') }}"></script> --}}
{{-- <script src="{{ asset('js/custom/utilities/modals/new-target.js') }}"></script> --}}
{{-- <script src="{{ asset('js/custom/documentation/general/stepper.js') }}"></script> --}}
{{-- <script src="{{ asset('js/custom/utilities/modals/users-search.js') }}"></script> --}}
<script src="{{ asset('vendor/videojs-playlist/dist/videojs-playlist.min.js') }}"></script>
<script src="{{ asset('vendor/videojs-playlist-ui/dist/videojs-playlist-ui.min.js') }}"></script>
<!--end::Custom Javascript-->
<!--end::Javascript-->
<script>
    $(document).ready(function() {
        @stack('script-ready')
        @if (Session::has('toastr-success'))
            toastr.success("{!! Session::get('toastr-success') !!}");
        @elseif (Session::has('toastr-info'))
            toastr.info("{!! Session::get('toastr-info') !!}");
        @elseif (Session::has('toastr-warning'))
            toastr.warning("{!! Session::get('toastr-warning') !!}");
        @elseif (Session::has('toastr-danger') || Session::has('toastr-error'))
            toastr.error("{!! Session::get('toastr-danger') !!}{!! Session::get('toastr-error') !!}");
        @endif
        var params = new window.URLSearchParams(window.location.search);
        if (params.get('swal_action') !== null && params.get('swal_action').length) {
            let paramSwalAction = params.get('swal_action');
            var btnSwalAction = $(`.${paramSwalAction}`);
            if (btnSwalAction.length) {
                swalx.upsertPopup(btnSwalAction);
            }
        }
        $('select.form-select2').select2({
            theme: "bootstrap5",
            width: '100%'
        });
    });
    @stack('script')
</script>
@stack('scripts')
