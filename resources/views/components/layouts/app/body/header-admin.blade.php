<div id="kt_app_header" class="app-header">
    <div class="app-container container-fluid d-flex align-items-stretch justify-content-between">
        <div class="d-flex align-items-center d-lg-none ms-n2" title="Show sidebar menu">
            <div class="btn btn-icon w-35px h-35px" id="kt_app_sidebar_mobile_toggle">
                <i class="fa-solid fa-bars text-white fs-1"></i>
            </div>
        </div>
        <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
            <a href="{{url('/')}}" class="d-lg-none">
                <img alt="Logo" src="{{ asset('media/logos/default-small-white.png') }}" class="h-50px" />
            </a>
        </div>
        <div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1" id="kt_app_header_wrapper">
            <div class="app-header-menu app-header-mobile-drawer align-items-stretch">
            </div>
            <div class="app-navbar flex-shrink-0">
                <x-layouts.app.body.header.notification />
                <div class="app-navbar-item ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
                    <div class="cursor-pointer symbol symbol-35px symbol-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                        @if(!empty(auth()->user()->getFirstMedia('avatar')))
                            <img src="{{auth()->user()->getFirstMediaUrl('avatar')}}">
                        @else
                            <div class="symbol-label fs-3 bg-light-{{session()->get('profile-color', 'success')}} text-{{session()->get('profile-color', 'success')}}">{{upperSubstr(auth()->user()->name)}}</div>
                        @endif
                    </div>
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-color fw-semibold py-4 fs-6 w-275px" data-kt-menu="true">
                        <div class="menu-item px-3">
                            <div class="menu-content d-flex align-items-center px-3">
                                <div class="symbol symbol-50px me-5">
                                    @if(!empty(auth()->user()->getFirstMedia('avatar')))
                                        <img src="{{auth()->user()->getFirstMediaUrl('avatar')}}">
                                    @else
                                        <div class="symbol-label fs-3 bg-light-{{session()->get('profile-color', 'success')}} text-{{session()->get('profile-color', 'success')}}">{{upperSubstr(auth()->user()->name)}}</div>
                                    @endif
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="fw-bold d-flex align-items-center fs-5">
                                        {{ auth()->user()->name }}
                                    </div>
                                    <span class="badge badge-light-success fw-bold fs-8">{{ optional(auth()->user()->role)->display_name }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="separator my-2"></div>
                        <div class="menu-item px-5">
                            <a href="{{ route('profile.index') }}" class="menu-link px-5">My Profile</a>
                        </div>
                        @if(auth()->user()->can('admin') && auth()->user()->can('student'))
                            <div class="menu-item px-5">
                                <a href="{{ route('dashboard.index') }}" class="menu-link px-5">Go to Dashboard Student</a>
                            </div>
                        @endif
                        <div class="separator my-2"></div>
                        <div class="menu-item px-5">
                            <a href="{{ route('logout') }}" class="menu-link px-5" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>