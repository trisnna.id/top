<link rel="canonical" href="{{ url('/') }}" />
<link rel="shortcut icon" href="{{ asset('media/logos/favicon.ico') }}" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
<link href="{{ asset(mix('plugins/custom/fullcalendar/fullcalendar.bundle.css')) }}" rel="stylesheet" type="text/css" />
<link href="{{ asset(mix('plugins/custom/prismjs/prismjs.bundle.css')) }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('vendor/select2-optgroup-select/dist/select2.optgroupSelect.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ asset(mix('plugins/custom/datatables/datatables.bundle.css')) }}" rel="stylesheet" type="text/css" />
<link href="{{ asset(mix('plugins/global/plugins.bundle.css')) }}" rel="stylesheet" type="text/css" />
<link href="{{ asset(mix('plugins/custom/cropper/cropper.bundle.css')) }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet" type="text/css" />
<link href="https://fonts.cdnfonts.com/css/mv-boli" rel="stylesheet">
@stack('styles')
<style>
    :root {
        --app-header-color: #ff0000;
        --kt-text-danger: #ff0000;
        --kt-danger: #ff0000;
        --kt-danger-active: #ff3333;
        --kt-success: #15b960;
    }

    /* set selected and hovered select2 option background color as #000000 */
    .select2-results__option[aria-selected=true],
    .select2-results__option:hover,
    .select2-results__option--selected,
    .select2-results__option--highlighted {
        background-color: #000000 !important;
    }

    #announcementContent p img,
    #quizContent p img {
        max-width: 100% !important;
        height: auto;
    }

    @stack('style')
</style>
