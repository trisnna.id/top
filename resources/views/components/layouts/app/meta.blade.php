<meta charset="utf-8" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="description" content="" />
<meta name="keywords" content="form builder" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta property="og:locale" content="{{ str_replace('_', '-', app()->getLocale()) }}" />
<meta property="og:site_name" content="{{config('app.name')}}" />
<meta property="og:title" content="{{$ogTitle ?? null}}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{request()->url()}}" />