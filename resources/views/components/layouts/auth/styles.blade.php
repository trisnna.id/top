<link rel="canonical" href="{{url('/')}}" />
<link rel="shortcut icon" href="{{ asset('media/logos/favicon.ico') }}" />
<!--begin::Fonts(mandatory for all pages)-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
<!--end::Fonts-->
<!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
<link href="{{ asset('plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/auth.css') }}" rel="stylesheet" type="text/css" />
<!--end::Global Stylesheets Bundle-->
@stack('styles')
<style>
    @stack('style')
</style>