<script>
    var hostUrl = "app/";
    window.ConfigApp = {
        "timezone": "{{ config('app.timezone') }}",
        "dateFormat": 'DD/MM/YYYY',
        "timeFormat": 'h:mm a'
    };
</script>
<script src="{{ asset('plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('js/scripts.bundle.js') }}"></script>
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/custom/authentication/sign-in/general.js') }}"></script>
@stack('scripts')
<script>
    $(document).ready(function() {
        @stack('script-ready')
        var params = new window.URLSearchParams(window.location.search);
        if (params.get('swal_action').length) {
            let paramSwalAction = params.get('swal_action');
            var btnSwalAction = $(`.${paramSwalAction}`);
            if (btnSwalAction.length) {
                swalx.upsertPopup(btnSwalAction);
            }
        }
    });
    @stack('script')
</script>
