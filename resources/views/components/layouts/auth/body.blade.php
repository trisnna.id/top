<body data-kt-name="metronic" id="kt_body" class="app-blank app-blank">
    <!--begin::Theme mode setup on page load-->
    <!--end::Theme mode setup on page load-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root" id="kt_app_root">
        <!--begin::Authentication - Sign-in -->
        <div class="d-flex flex-column flex-lg-row flex-column-fluid">
            <!--begin::Body-->
            <div class="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
                {!! $slot !!}
            </div>
            <!--end::Body-->
            <div class="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center order-1 order-lg-2" style="background-image: url({{asset('media/misc/auth-bg.png')}})">
                <div class="d-flex flex-column flex-center py-15 px-5 px-md-15 w-100">
                    <a href="{{url('/')}}" class="mb-12">
                        <img alt="Logo" src="{{ asset('media/logos/default.png') }}" class="h-75px" />
                    </a>
                    <h1 class="text-white fs-2qx fw-bolder text-center mb-7">Taylor's Orientation Portal</h1>
                    <div class="text-white fs-base text-center">
                        Your Future Starts Now!
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-layouts.auth.body.scripts />
</body>