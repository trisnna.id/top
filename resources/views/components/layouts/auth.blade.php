<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
        <title>{{"{$title} | ".config('app.name')}}</title>
		<x-layouts.app.meta :ogTitle="$title.' | '.config('app.name')"/>
        <x-layouts.auth.styles />
	</head>
    <x-layouts.auth.body :title="$title">
        {!! $slot !!}
    </x-layouts.auth.body>
</html>