<button type="button" class="btn btn-color-gray-400 btn-active-color-primary justify-content-end px-0" data-bs-toggle="modal" data-bs-target="#{{ "{$id}FilterModal" }}">
    <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <rect opacity="0.3" x="2" y="2" width="20" height="20"
                rx="4" fill="currentColor"></rect>
            <rect x="11" y="11" width="2.6" height="2.6" rx="1.3"
                fill="currentColor"></rect>
            <rect x="15" y="11" width="2.6" height="2.6" rx="1.3"
                fill="currentColor"></rect>
            <rect x="7" y="11" width="2.6" height="2.6" rx="1.3"
                fill="currentColor"></rect>
        </svg>
    </span>
</button>
<div class="modal fade" id="{{ "{$id}FilterModal" }}" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filter Options</h5>
                <button type="button" class="btn close p-0" data-bs-dismiss="modal" aria-label="Close"><i
                        class="fas fa-times"></i></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 mb-5 fv-row fv-plugins-icon-container">
                        <label class="form-label fw-semibold">Intakes:</label>
                        {!! Form::select('filter_intake', $options['intake'], $options['intake_default'], [
                            'id' => "{$id}_filter_intake",
                            'multiple',
                            'class' => 'form-select form-select2 reset',
                            'data-hide-search' => 'true',
                            'data-allow-clear' => 'true',
                            'data-placeholder' => '-- Please Select / All --',
                        ]) !!}
                    </div>
                    <div class="col-12 fv-row fv-plugins-icon-container">
                        <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                            {{ Form::checkbox('apply_all', null, false, ['id' => "{$id}_apply_all", 'class' => 'form-check-input']) }}
                            <span class="fw-bold form-check-label">
                                Apply to All?
                            </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-red font-weight-bold" onclick="inputx.reset('#{{ $id }}FilterModal')">Reset</button>
                <button type="button" class="btn btn-black" data-bs-dismiss="modal"
                        onclick="{!! $applyOnClick !!}">Apply</button>
            </div>
        </div>
    </div>
</div>