<button type="button" class="btn btn-light-primary me-3" data-bs-toggle="modal" data-bs-target="#{{ "{$id}FilterModal" }}">
    <span class="svg-icon svg-icon-2">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path
                d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                fill="currentColor" />
        </svg>
    </span>Filter
</button>
<div class="modal fade" id="{{ "{$id}FilterModal" }}" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filter Options</h5>
                <button type="button" class="btn close p-0" data-bs-dismiss="modal" aria-label="Close"><i
                        class="fas fa-times"></i></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 mb-5 fv-row fv-plugins-icon-container">
                        <label class="form-label fw-semibold">Intakes:</label>
                        {!! Form::select('filter_intake', $options['intake'], $options['intake_default'], [
                            'id' => "{$id}_filter_intake",
                            'multiple',
                            'class' => 'form-select form-select2 reset',
                            'data-hide-search' => 'true',
                            'data-allow-clear' => 'true',
                            'data-placeholder' => '-- Please Select / All --',
                        ]) !!}
                    </div>
                    <div class="col-12 fv-row fv-plugins-icon-container">
                        <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                            {{ Form::checkbox('apply_all', null, false, ['id' => "{$id}_apply_all", 'class' => 'form-check-input']) }}
                            <span class="fw-bold form-check-label">
                                Apply to All?
                            </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-red font-weight-bold" onclick="inputx.reset('#{{ $id }}FilterModal')">Reset</button>
                <button type="button" class="btn btn-black" data-bs-dismiss="modal"
                        onclick="{!! $applyOnClick !!}">Apply</button>
            </div>
        </div>
    </div>
</div>