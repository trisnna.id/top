<button type="button" class="btn btn-light-primary me-3" data-bs-toggle="modal" data-bs-target="#{{ "{$id}FilterModal" }}">
    <span class="svg-icon svg-icon-2">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z"
                fill="currentColor" />
        </svg>
    </span>
    Filter
</button>
<div class="modal fade" id="{{ "{$id}FilterModal" }}" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Filter Options</h5>
                <button type="button" class="btn close p-0" data-bs-dismiss="modal" aria-label="Close"><i
                        class="fas fa-times"></i></button>
            </div>
            <div class="modal-body">
                <div class="row mb-5">
                    <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                        <label class="form-label fw-semibold">Faculties:</label>
                        {!! Form::select('filter_faculty', $options['faculty'], null, [
                            'id' => "{$id}_filter_faculty",
                            'multiple',
                            'class' => 'form-select form-select2 reset',
                            'data-hide-search' => 'true',
                            'data-allow-clear' => 'true',
                            'data-placeholder' => '-- Please Select / All --',
                        ]) !!}
                    </div>
                    <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                        <label class="form-label fw-semibold">School:</label>
                        {!! Form::select('filter_school', $options['school'], null, [
                            'id' => "{$id}_filter_school",
                            'multiple',
                            'class' => 'form-select form-select2 reset',
                            'data-hide-search' => 'true',
                            'data-allow-clear' => 'true',
                            'data-placeholder' => '-- Please Select / All --',
                        ]) !!}
                    </div>
                    <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                        <label class="form-label fw-semibold">Programme:</label>
                        {!! Form::select('filter_programme', $options['programme'], null, [
                            'id' => "{$id}_filter_programme",
                            'multiple',
                            'class' => 'form-select form-select2 reset',
                            'data-hide-search' => 'true',
                            'data-allow-clear' => 'true',
                            'data-placeholder' => '-- Please Select / All --',
                        ]) !!}
                    </div>
                    <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                        <label class="form-label fw-semibold">Intake:</label>
                        {!! Form::select('filter_intake', $options['intake'], null, [
                            'id' => "{$id}_filter_intake",
                            'multiple',
                            'class' => 'form-select form-select2 reset',
                            'data-hide-search' => 'true',
                            'data-allow-clear' => 'true',
                            'data-placeholder' => '-- Please Select / All --',
                        ]) !!}
                    </div>
                    <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                        <label class="form-label fw-semibold">Level of Study:</label>
                        {!! Form::select('filter_level_study', $options['level_study'], null, [
                            'id' => "{$id}_filter_level_study",
                            'multiple',
                            'class' => 'form-select form-select2 reset',
                            'data-hide-search' => 'true',
                            'data-allow-clear' => 'true',
                            'data-placeholder' => '-- Please Select / All --',
                        ]) !!}
                    </div>
                    <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                        <label class="form-label fw-semibold">Institution:</label>
                        {!! Form::select('filter_campus', ['' => ''] + $options['campus']->toArray(), null, [
                            'id' => "{$id}_filter_campus",
                            'class' => 'form-select form-select2 reset',
                            'data-hide-search' => 'true',
                            'data-allow-clear' => 'true',
                            'data-placeholder' => '-- Please Select / All --',
                        ]) !!}
                    </div>
                    <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                        <label class="form-label fw-semibold">Mobility:</label>
                        {!! Form::select('filter_mobility', $options['mobility'], null, [
                            'id' => "{$id}_filter_mobility",
                            'multiple',
                            'class' => 'form-select form-select2 reset',
                            'data-hide-search' => 'true',
                            'data-allow-clear' => 'true',
                            'data-placeholder' => '-- Please Select / All --',
                        ]) !!}
                    </div>
                    <div class="col-md-6 mb-5 fv-row fv-plugins-icon-container">
                        <label class="form-label fw-semibold">Locality:</label>
                        {!! Form::select('filter_locality', ['' => ''] + $options['locality']->toArray(), null, [
                            'id' => "{$id}_filter_locality",
                            'class' => 'form-select form-select2 reset',
                            'data-hide-search' => 'true',
                            'data-allow-clear' => 'true',
                            'data-placeholder' => '-- Please Select / All --',
                        ]) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-light-primary font-weight-bold" onclick="inputx.reset('#{{ $id }}FilterModal')">Reset</button>
                <button type="button" class="btn btn-primary" data-bs-dismiss="modal"
                    onclick="dtx.reloadSingleDt('{{ $id }}')">Apply</button>
            </div>
        </div>
    </div>
</div>
