<div class="d-flex flex-align-center gap-5">
    @foreach ($localities as $locality)
        <span class="badge badge-sm px-4 badge-secondary">{{ $locality->name }}</span>
    @endforeach
</div>
