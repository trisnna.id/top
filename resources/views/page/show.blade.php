<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<title>{{ config('app.name') }}</title>
		<x-layouts.app.meta :ogTitle="$title . ' | ' . config('app.name')" />
		<x-layouts.app.styles />
	</head>
	<body data-kt-name="metronic" id="kt_app_body" data-kt-app-layout="dark-header" data-kt-app-header-fixed="true"
		data-kt-app-header-fixed-mobile="true" data-kt-app-toolbar-enabled="true" class="app-default">
		<div class="d-flex flex-column flex-root" id="kt_app_root">
			<div class="app-page flex-column flex-column-fluid" id="kt_app_page">
				<div id="kt_app_header" class="app-header">
					<div class="app-container container-xxl d-flex align-items-stretch justify-content-between">
						<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0 me-lg-15">
							<a class="sidebar-brand" href="{{ url('/') }}">
								<img src="{{ asset('media/logos/default-white.png') }}"
									class="h-55px app-sidebar-logo-default" />
							</a>
						</div>
					</div>
				</div>
				<div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
					<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
						<div class="d-flex flex-column flex-column-fluid">
							<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6 cover mb-5"
								style="background-image: url('{{ randomBackgroundImage() }}');">
								<div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
									<div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
										<h1
											class="text-uppercase page-heading d-flex text-white fw-bold fs-3 flex-column justify-content-center my-0">
											{{ $page->title }}</h1>
									</div>
								</div>
							</div>
							<div id="kt_app_content" class="app-content flex-column-fluid pb-5">
								<div id="kt_app_content_container" class="app-container container-xxl z-index-2">
									<div class="card">
										<div class="card-body">
											{!! json_decode($page->content_encode) !!}
										</div>
									</div>
								</div>
							</div>
						</div>
						<x-layouts.app.body.footer />
					</div>
				</div>
			</div>
		</div>
		<x-layouts.app.body.scripts />
	</body>
</html>
