(function ($, W, Swal) {
    const ICONS = ['info', 'success', 'warning', 'error'];

    W.notify = function (message, title = 'Notification', icon = 'info') {
        const btn = $(`.sweet-notify`)
        btn.each(function (i, elm) {
            const self = $(elm);
            self.on('click', function (e) {
                e.preventDefault();
                Swal.fire({
                    icon: ICONS.includes(icon) ? icon : 'info',
                    title: title,
                    text: message,
                })
            })
        })
    }
})(jQuery, window, Swal);
