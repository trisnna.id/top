window.route = require("~ziggy").default;
window.Ziggy = require("./plugins/ziggy.js").Ziggy;
require("./plugins/input");
require("./plugins/datatable.js");
require("./plugins/fullcalendar.js");
require("./plugins/swal.js");
require("./plugins/fbuilder.js");
require("./plugins/tinymce.js");
require("./plugins/stepper.js");
require("./globals/sweetalert");
require("./globals/youtube");
require("./modules/preorientation");
require("./modules/role");
require("./modules/user");
require("./modules/orientation");
require("./modules/profile");
require("./modules/student");
require("./modules/dashboard");
require("./modules/setting");
require("./modules/integration");
require("./modules/portal");
require("./modules/resource");
require("./modules/report");
