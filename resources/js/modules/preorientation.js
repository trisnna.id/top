(function (W, $) {
    const PreOrientation = Function;

    PreOrientation.prototype.handleViewDetail = function (triggeredClassName = '.btn-detail') {
        $(triggeredClassName).each(function (i, elm) {
            $(elm).on('click', function () {
                const self = $(this)
                if (self.attr('class').includes('btn-light')) {
                    W.notify('Please complete the previous activities first', 'Wait!', 'warning')
                } else if (self.attr('class').includes('btn-secondary')) {
                    const formTarget = $(this).attr('data-sweetalert-form')
                    const detailContainer = $(`#${formTarget}`)
                    const quizContainer = $(`#quiz_stepper_container`)
                    Swal.fire({
                        title: detailContainer.attr('data-sweetalert-title'),
                        html: detailContainer.html(),
                        confirmButtonText: 'See quiz',
                        showConfirmButton: self.attr('data-completed') == false,
                        confirmButtonColor: '#ff0000',
                        customClass: {
                            popup: 'w-300px w-md-500px'
                        },
                        didOpen: function () {
                            detailContainer.css('display', 'auto')
                            var player = videojs('video');
                        },
                        didDestroy: function () {
                            detailContainer.css('display', 'none')
                        }, 
                    }).then(function (result) {
                        if (result.isConfirmed) {
                            Swal.fire({
                                title: detailContainer.attr('data-sweetalert-title') + ' MCQ',
                                html: quizContainer.html(),
                                showConfirmButton: false,
                                customClass: {
                                    popup: 'w-300px w-md-500px'
                                },
                                didOpen: function () {
                                    stepperx.init('#swal2-html-container #quiz_stepper','#swal2-html-container #quiz_form');
                                },
                            })
                        }
                    })
                }
            })
        })
    }

    W['PreOrientation'] = PreOrientation
})(window, jQuery)