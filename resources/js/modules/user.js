(function ($, window) {
    "use strict";
    window.modules = window.modules || {};
    window.modules.user = window.modules.user || {};
    window.modules.user.admin = window.modules.user.admin || {};
    window.modules.user.admin.index = function () {
        return {
            swalUpdateAdmin: function (elInput, data) {
                if (data.id === undefined) {
                    $(`${elInput} [name='_method']`).val('POST').change();
                } else {
                    if (data.role_type === 'admin') {
                        $(`${elInput} [name='id_number']`).val(data.id_number).change();
                        window.inputx.disabledReadonly(`${elInput} [name='id_number']`);
                    } else if (data.role_type === 'student') {
                        $(`${elInput} [name='email']`).val(data.email).change();
                        window.inputx.disabledReadonly(`${elInput} [name='email']`);
                    }
                    $(`${elInput} [name='name']`).val(data.name).change();
                    $(`${elInput} [name='role_name']`).val(data.role_name).change();
                    $(`${elInput} [name='is_active']`).prop('checked', data.is_active);
                    window.inputx.disabledReadonly(`${elInput} [name='name']`);
                }
            },
        };
    }();
})(jQuery, window);