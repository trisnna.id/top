(function ($, window) {
    "use strict";
    window.steppers = window.steppers || {};
    window.modules = window.modules || {};
    window.modules.orientation = function () {
        return {
            generateAttendanceQRCode: function (id, data) {
                var canvas = document.getElementById(id);
                QRCode.toCanvas(canvas, JSON.stringify(data), {
                    width: 200,
                    margin: 0,
                });
            },
            stepperOrientationNext: function (elId) {
                var step = window.steppers[elId].getCurrentStepIndex();
                if (step == 5) {
                    $(`form#stepper-form select[name=attachment]`).val([]).change();
                    $(`form#stepper-form #attachmentContainer`).find('.attachment-input-files').remove();
                    $(`form#stepper-form #attachmentContainer`).find('.attachment-input-links').remove();
                }
                window.steppers[elId].goNext();
            },
            stepperOrientationNoNext: function (elId) {
                var step = window.steppers[elId].getCurrentStepIndex();
                if (step == 5) {
                    $(`form#stepper-form select[name=attachment]`).val([]).change();
                    $(`form#stepper-form #attachmentContainer`).find('.attachment-input-files').remove();
                    $(`form#stepper-form #attachmentContainer`).find('.attachment-input-links').remove();
                }
            },
            stepper: function (elId, step) {
                var element = document.querySelector(`#${elId}`);
                var stepper = new KTStepper(element);
                if (typeof step !== "undefined") {
                    stepper.goTo(step);
                }
                stepper.on("kt.stepper.click", function (stepper) {
                    var step = stepper.getClickedStepIndex();
                    $(`#${elId} [name='step']`).val(step).change();
                    stepper.goTo(step);
                })
                stepper.on("kt.stepper.previous", function (stepper) {
                    stepper.goPrevious();
                });
                stepper.on("kt.stepper.next", function (stepper) {
                    if (!$(stepper.btnNext).hasClass('upsert-swal-inline')) {
                        stepper.goNext();
                    }
                });
                window.steppers[elId] = stepper;
            },
            selectFilterAll: function (name, formEl, values) {
                if (typeof values !== "undefined" && values.length == 1 && values.includes('all')) {
                    $(`${formEl} [name='filter_all[${name}]']`).val(1).change();
                    var options = $(`${formEl} [name='filters[${name}][]'] option`);
                    values = $.map(options, e => $(e).val())
                } else {
                    $(`${formEl} [name='filter_all[${name}]']`).val(0).change();
                }
                $(`${formEl} [name='filters[${name}][]']`).val(values).change();
            },
            getCalculateStudent: function (formEl) {
                const params = {
                    filter_all: {
                        faculty: $(`${formEl} [name='filter_all[faculty]']`).val() ?? null,
                        intake: $(`${formEl} [name='filter_all[intake]']`).val() ?? null,
                        level_study: $(`${formEl} [name='filter_all[level_study]']`).val() ?? null,
                        mobility: $(`${formEl} [name='filter_all[mobility]']`).val() ?? null,
                        school: $(`${formEl} [name='filter_all[school]']`).val() ?? null,
                        programme: $(`${formEl} [name='filter_all[programme]']`).val() ?? null,
                    },
                    filters: {
                        faculty: $(`${formEl} [name='filters[faculty][]']`).val() ?? null,
                        intake: $(`${formEl} [name='filters[intake][]']`).val() ?? null,
                        level_study: $(`${formEl} [name='filters[level_study][]']`).val() ?? null,
                        mobility: $(`${formEl} [name='filters[mobility][]']`).val() ?? null,
                        school: $(`${formEl} [name='filters[school][]']`).val() ?? null,
                        programme: $(`${formEl} [name='filters[programme][]']`).val() ?? null,
                    },
                    scope: ['active_filter']
                };
                return axios.get(route('admin.orientations.calculate-student'), { params })
                    .then(response => {
                        const value = response.data.data.total;
                        $(`${formEl} [name='total_student']`).val(value).change();
                    }).catch(error => {
                    });
            },
            showForm: function (formEl, data) {
                this.createForm(formEl, data);
                $.each($(`${formEl} input`), function (index, value) {
                    inputx.disabledReadonly(value);
                });
                $.each($(`${formEl} textarea`), function (index, value) {
                    inputx.disabledReadonly(value);
                });
                $.each($(`${formEl} select`), function (index, value) {
                    inputx.disabledReadonly(value);
                });
                var sliders = $(`${formEl} #pointSlider`);
                if (sliders.length) {
                    sliders.each(function (index, slider) {
                        slider.noUiSlider.disable();
                    })
                }
            },
            createForm: function (formEl, data) {
                $(`${formEl} select[name=orientation_type_id]`).on('change', function (e) {
                    e.preventDefault();
                    const value = $(this).val(),
                        valuePointAttendance = $(`${formEl} input[name=point_attendance]`).attr('data-default') || 0,
                        valuePointRating = $(`${formEl} input[name=point_rating]`).attr('data-default') || 0;
                    if (value == 1 || value == 3) { //compulsory core or break
                        $(`${formEl} input#is_rsvp2`).prop('checked', true);
                        $(`${formEl} input#is_rsvp1`).attr('readonly', 'readonly');
                        $(`${formEl} input#is_rsvp2`).attr('readonly', 'readonly');
                        $(`${formEl} #isRsvpContainer`).find('.is-rsvp-input').remove();
                    } else {
                        $(`${formEl} input#is_rsvp1`).removeAttr('readonly');
                        $(`${formEl} input#is_rsvp2`).removeAttr('readonly');
                    }
                    if (value == 3) { //break
                        $(`${formEl} [data-model=point_attendance]`).html(0);
                        $(`${formEl} [data-model=point_rating]`).html(0);
                        $(`${formEl} input[name=point_attendance]`).val(0).change();
                        $(`${formEl} input[name=point_rating]`).val(0).change();
                    } else {
                        $(`${formEl} [data-model=point_attendance]`).html(valuePointAttendance);
                        $(`${formEl} [data-model=point_rating]`).html(valuePointRating);
                        $(`${formEl} input[name=point_attendance]`).val(valuePointAttendance).change();
                        $(`${formEl} input[name=point_rating]`).val(valuePointRating).change();
                    }
                });
                $(`${formEl} input[name=is_rsvp]`).on('change', function (e) {
                    e.preventDefault();
                    const value = $(this).val();
                    if (value == '1') {
                        $(`${formEl} #isRsvpContainer`).find('.is-rsvp-input').remove();
                        $(`${formEl} #isRsvpContainer`).append(`
                            <div class="d-flex flex-column mb-5 fv-row is-rsvp-input">
                                <label class="required fw-bold mb-2">Capacity / Vacancies</label>
                                <input type="number" class="form-control" name="rsvp_capacity" id="rsvp_capacity" placeholder="Capacity / vacancies" minimum="1">
                            </div>
                        `);
                        if (typeof data.uuid !== "undefined") {
                            $(`${formEl} [name='rsvp_capacity']`).val(data.rsvp_capacity).change();
                        }
                    } else {
                        $(`${formEl} #isRsvpContainer`).find('.is-rsvp-input').remove()
                    }
                });
                if (typeof data.uuid !== "undefined") {
                    $(`${formEl} [name='name']`).val(data.name).change();
                    $(`${formEl} [name='venue']`).val(data.venue).change();
                    $(`${formEl} [name='livestream']`).val(data.livestream).change();
                    $(`${formEl} [name='start_date']`).val(data.start_date).change();
                    $(`${formEl} [name='start_time']`).val(data.start_time).change();
                    $(`${formEl} [name='end_date']`).val(data.end_date).change();
                    $(`${formEl} [name='end_time']`).val(data.end_time).change();
                    $(`${formEl} [name='presenter']`).val(data.presenter).change();
                    $(`${formEl} [name='orientation_type_id']`).val(data.orientation_type_id).change();
                    $(`${formEl} [name='total_student']`).val(data.total_student).change();
                    this.selectFilterAll('faculty', formEl, data.filters.faculty);
                    this.selectFilterAll('intake', formEl, data.filters.intake);
                    this.selectFilterAll('level_study', formEl, data.filters.level_study);
                    this.selectFilterAll('mobility', formEl, data.filters.mobility);
                    this.selectFilterAll('school', formEl, data.filters.school);
                    this.selectFilterAll('programme', formEl, data.filters.programme);
                    if (data.is_rsvp) {
                        $(`${formEl} [name='is_rsvp'][value='1']`).prop('checked', true).change();
                    } else {
                        $(`${formEl} [name='is_rsvp'][value='0']`).prop('checked', true).change();
                    }
                    if (data.is_point) {
                        $(`${formEl} [name='is_point'][value='1']`).prop('checked', true).change();
                    } else {
                        $(`${formEl} [name='is_point'][value='0']`).prop('checked', true).change();
                    }
                }

                function startTimeOnChange(value) {
                    let startDateValue = $(`${formEl} input[name=start_date]`).val(),
                        endDateValue = $(`${formEl} input[name=end_date]`).val();
                    if (startDateValue == endDateValue) {
                        $(`${formEl} input[name=end_time]`).attr('min', value);
                    } else {
                        $(`${formEl} input[name=end_time]`).removeAttr('min');
                    }
                }
                $(`${formEl} input[name=start_date]`).on('change', function (e) {
                    e.preventDefault();
                    const value = $(this).val(),
                        startTimeValue = $(`${formEl} input[name=start_time]`).val();
                    $(`${formEl} input[name=end_date]`).attr('min', value);
                    startTimeOnChange(startTimeValue);
                });
                $(`${formEl} input[name=end_date]`).on('change', function (e) {
                    e.preventDefault();
                    const value = $(this).val(),
                        startTimeValue = $(`${formEl} input[name=start_time]`).val();
                    $(`${formEl} input[name=start_date]`).attr('max', value);
                    startTimeOnChange(startTimeValue);
                });
                $(`${formEl} input[name=start_time]`).on('change', function (e) {
                    e.preventDefault();
                    const value = $(this).val();
                    startTimeOnChange(value);
                });
                $(`${formEl} select[name=attachment]`).on('select2:select', function (e) {
                    e.preventDefault();
                    const value = e.params.data.id;
                    if (value == 'files') {
                        const accept = $(e.params.data.element).data('accept'),
                            acceptInfo = $(e.params.data.element).data('accept-info'),
                            size = $(e.params.data.element).data('size');
                        $(`${formEl} #attachmentContainer`).append(`
                            <div class="d-flex flex-column mb-5 fv-row attachment-input-files">
                                <input multiple type="file" class="form-control" name="attachment_files[]" id="attachment_files" placeholder="Media File" accept="${accept}">
                                <div class="form-text text-black">Allowed file types: ${acceptInfo}. Max size: ${size} MB</div>
                            </div>
                        `);
                    } else if (value == 'links') {
                        $(`${formEl} #attachmentContainer`).append(`
                            <div class="d-flex flex-column mb-5 fv-row attachment-input-links">
                                <textarea class="form-control" name="attachment_links" id="attachment_links" placeholder="Media link. For multiple links, separate by comma (,)."></textarea>
                                <div class="form-text text-black">i.e.: Youtube link: https://www.youtube.com/embed/{ID}</div>
                            </div>
                        `);
                    }
                }).on('select2:unselect', function (e) {
                    e.preventDefault();
                    const value = e.params.data.id;
                    if (value == 'files') {
                        $(`${formEl} #attachmentContainer`).find('.attachment-input-files').remove();
                    } else if (value == 'links') {
                        $(`${formEl} #attachmentContainer`).find('.attachment-input-links').remove();
                    }
                });
                $.fn.select2.amd.require(["optgroup-data", "optgroup-results"], function (OptgroupData, OptgroupResults) {
                    $(`${formEl} .swal2-select2-multiple-optgroup`).select2({
                        dropdownCssClass: "select2-multiple-optgroup",
                        dataAdapter: OptgroupData,
                        resultsAdapter: OptgroupResults,
                    });
                });
                $(`${formEl} select[name='filters[faculty][]'], ${formEl} select[name='filters[intake][]'], ${formEl} select[name='filters[level_study][]'], ${formEl} select[name='filters[mobility][]'], ${formEl} select[name='filters[school][]'], ${formEl} select[name='filters[programme][]']`).on('select2:unselect', function (e) {
                    e.preventDefault();
                    modules.orientation.getCalculateStudent(formEl);
                }).on('change', function (e) {
                    e.preventDefault();
                    modules.orientation.getCalculateStudent(formEl);
                });
                tinymcex.init("agenda", "#agenda");
                tinymcex.init("synopsis", "#synopsis");
                $(`${formEl} [name='notes']`).maxlength({
                    warningClass: "badge badge-warning",
                    limitReachedClass: "badge badge-success"
                });
            },
            upsertLink: function (elInput, data) {
                if (typeof data.id === undefined) {
                    $(`${elInput} [name='_method']`).val('POST').change();
                } else {
                    $(`${elInput} [name='name']`).val(data.name).change();
                    $(`${elInput} [name='url']`).val(data.url).change();
                }
            },

        };
    }();
    window.modules.orientation.admin = window.modules.orientation.admin || {};
    window.modules.orientation.admin.index = function () {
        var filter = function (calendarId) {
            return {
                'faculty': $(`#${calendarId}_filter_faculty`).val(),
                'school': $(`#${calendarId}_filter_school`).val(),
                'programme': $(`#${calendarId}_filter_programme`).val(),
                'intake': $(`#${calendarId}_filter_intake`).val(),
                'level_study': $(`#${calendarId}_filter_level_study`).val(),
                'mobility': $(`#${calendarId}_filter_mobility`).val(),
                'orientation_type': $(`#${calendarId}_filter_orientation_type`).val(),
                'is_rsvp': $(`#${calendarId}_filter_is_rsvp`).val(),
                'status': $(`#${calendarId}_filter_status`).val(),
            }
        };
        return {
            calendar: function (calendarId) {
                fullcalendarx.default(calendarId, {
                    events: function (info, successCallback, failureCallback) {
                        return axios.get(route('admin.orientations.index', {
                            'calendar': calendarId,
                            'start_date': info.startStr,
                            'end_date': info.endStr,
                            'filter': filter(calendarId),
                        })).then((response) => {
                            return response.data.data;
                        });
                    },
                    eventDidMount: function (info) {
                        const id = 'calendar_' + info.event.extendedProps['data-data']['id'];
                        $(info.el).addClass('upsert-swal-popup');
                        $(info.el).attr('data-swal', JSON.stringify(info.event.extendedProps['data-swal']));
                        $(info.el).attr('data-data', JSON.stringify(info.event.extendedProps['data-data']));
                        $(info.el).attr('id', id);
                        swalx.init(`#${id}`);
                    },
                });
            },
            exportCalendar: function (calendarId, action) {
                var calendar = fullcalendars[calendarId],
                    url = route('admin.orientations.index', {
                        'calendar': calendarId,
                        'start_date': calendar.view.currentStart.toISOString(),
                        'end_date': calendar.view.currentEnd.toISOString(),
                        'filter': filter(calendarId),
                        'action': action,
                    });
                window.location = url;
            }
        }
    }();
    window.modules.orientation.admin.show = function () {
        return {
            swalUpsertRecording: function (formEl, data) {
                $(`${formEl} select#attachment_recording`).on('select2:select', function (e) {
                    e.preventDefault();
                    const value = e.params.data.id;
                    if (value == 'files') {
                        const accept = $(e.params.data.element).data('accept'),
                            acceptInfo = $(e.params.data.element).data('accept-info'),
                            size = $(e.params.data.element).data('size');
                        $(`${formEl} #attachmentRecordingContainer`).append(`
                            <div class="d-flex flex-column mb-5 fv-row container-attachment-recording-files">
                                <input multiple type="file" class="form-control" name="attachment_recording_files[]" id="attachment_recording_files" placeholder="Media File" accept="${accept}">
                                <div class="form-text text-black">Allowed file types: ${acceptInfo}. Max size: ${size} MB</div>
                            </div>
                        `);
                    } else if (value == 'links') {
                        $(`${formEl} #attachmentRecordingContainer`).append(`
                            <div class="d-flex flex-column mb-5 fv-row container-attachment-recording-links">
                                <textarea class="form-control" name="attachment_recording_links" id="attachment_recording_links" placeholder="Media link. For multiple links, separate by comma (,)."></textarea>
                                <div class="form-text text-black">i.e.: Youtube link: https://www.youtube.com/embed/{ID}</div>
                            </div>
                        `);
                    }
                }).on('select2:unselect', function (e) {
                    e.preventDefault();
                    const value = e.params.data.id;
                    if (value == 'files') {
                        $(`${formEl} #attachmentRecordingContainer`).find('.container-attachment-recording-files').remove();
                    } else if (value == 'links') {
                        $(`${formEl} #attachmentRecordingContainer`).find('.container-attachment-recording-links').remove();
                    }
                });
            },
        }
    }();
    window.modules.orientation = window.modules.orientation || {};
    window.modules.orientation.index2 = function () {
        return {
            script: function () {
                let orientationDtBtnCore = $('#orientationDt_btn_core'),
                    orientationDtBtnComplementary = $('#orientationDt_btn_complementary'),
                    orientationDtFilterOrientationType = $('#orientationDt_filter_orientation_type');
                if (orientationDtBtnCore.length) {
                    orientationDtBtnCore.click(function (e) {
                        e.preventDefault();
                        orientationDtBtnCore.addClass('border');
                        orientationDtBtnComplementary.removeClass('border');
                        orientationDtFilterOrientationType.val(1).change();
                    });
                }
                if (orientationDtBtnComplementary.length) {
                    orientationDtBtnComplementary.click(function (e) {
                        e.preventDefault();
                        orientationDtBtnComplementary.addClass('border');
                        orientationDtBtnCore.removeClass('border');
                        orientationDtFilterOrientationType.val(2).change();
                    });
                }
                if (orientationDtFilterOrientationType.length) {
                    orientationDtFilterOrientationType.change(function (e) {
                        e.preventDefault();
                        dtx.reloadSingleDt('orientationDt');
                    });
                }
            },
        };
    }();
    window.modules.orientation.index = function () {
        var filter = function (calendarId) {
            return {
                'orientation_type': $(`#${calendarId}_filter_orientation_type`).val(),
                'is_rsvp': $(`#${calendarId}_filter_is_rsvp`).val(),
            }
        };
        return {
            timeline: function (settings, dtId) {
                let weightEls = $('#timelineDt .weight'),
                    weightNames = [],
                    cardTimelineSummary = $('.card-timeline-summary'),
                    ul = $(document.createElement("ul")).addClass('nav nav-pills nav-pills-custom flex-column border-transparent fs-5 fw-bold'),
                    data = settings.json.data || [];
                $.each(data, function (index, value) {
                    let sameDay = value.DT_RowAttr['data-same-date'],
                        toEndDate = '';
                    if (!sameDay) {
                        toEndDate = `- ${value.DT_RowAttr['data-end-date-day']} ${value.DT_RowAttr['data-end-date-month']}`;
                    }
                    ul.append(`<li class="nav-item m-0 border" data-weight-name="${value.DT_RowAttr['data-weight-name']}">
                        <span class="nav-link text-active-primary ms-0 py-2 ps-5 border-0 active ms-5">
                            <span class="d-flex flex-column">
                                <span><span class="fs-2 text-c00000 me-2">${value.DT_RowAttr['data-date-day']}</span><span class="text-c00000 text-uppercase">${value.DT_RowAttr['data-date-month']} ${toEndDate}</span></span>
                                <span class="text-black fs-6">${value.DT_RowAttr['data-title']}</span>
                            </span>
                            <span class="bullet-custom position-absolute start-0 top-0 w-3px h-100 bg-${value.DT_RowAttr['data-color']} rounded-end"></span>
                        </span>
                    </li>`);
                });
                if (data.length == 0) {
                    ul.append(`<li class="nav-item m-0">
                        <span class="nav-link text-active-primary ms-0 py-2 ps-5 border-0 active ms-5">
                            <span>No data available</span>
                        </span>
                    </li>`);
                }
                cardTimelineSummary.html(ul);
                $.each(weightEls, function (index, value) {
                    let that = $(value),
                        weightName = that.attr('data-weight-name');
                    if (weightNames.indexOf(weightName) === -1) {
                        weightNames.push(weightName);
                    }
                });
                $.each(weightNames, function (index, value) {
                    let weightEl = $(`#timelineDt .weight[data-weight-name="${value}"]`)[0],
                        summaryWeightEl = $(`.card-timeline-summary .nav-item[data-weight-name="${value}"]`)[0],
                        classMargin = 'm-15';
                    if (index == 0) {
                        classMargin = 'mb-15';
                    }
                    $(`<tr class='d-flex flex-center'><td><div class="${classMargin}"><div class="mt-3 arrow-head mb-3 bg-0070c0 position-relative"><span class="arrow-text m-12 text-white fs-6">${value}</span></div></div></td></tr>`).insertBefore(weightEl);
                    $(`<li class="nav-item m-0">
                        <span class="nav-link ps-5 border-0">
                            <div class="mt-3 arrow-head mb-3 bg-0070c0 position-relative"><span class="arrow-text m-12 text-white fs-6">${value}</span></div>
                        </span>
                    </li>`).insertBefore(summaryWeightEl);
                });
            },
            swalUpsertCalendar: async function (formEl, data) {
                let studentId = data.student_id || 0,
                    calendarId = data.calendar || 'orientationCalendar',
                    events = await axios.get(route('orientations.index', {
                        'student_id': studentId,
                        'calendar': calendarId,
                    })).then((response) => {
                        return response.data.data;
                    }),
                    calendarEvents = {},
                    accessTokenRequired = true,
                    btnCalendarData = { access_token_required: accessTokenRequired, events: calendarEvents };
                $.each(events, function (index, value) {
                    $(`${formEl} #eventsCalendarContainer`).append(`<div class="d-flex flex-column mb-5 fv-row">
                        <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                            <input class="form-check-input" data-model="event" data-id="${value.id}" type="checkbox" value="true" name="events[${value.id}]" checked="" />
                            <span class="fw-bold form-check-label">${value.title}</span>
                        </label>
                    </div>`);
                    accessTokenRequired = value['extendedProps']['data-data']['access_token_required'];
                    calendarEvents[value['extendedProps']['data-data'].id] = {
                        'summary': value['extendedProps']['data-data'].name,
                        'location': value['extendedProps']['data-data'].venue,
                        'start': {
                            'dateTime': value['extendedProps']['data-data'].start_datetime_iso_8601,
                        },
                        'end': {
                            'dateTime': value['extendedProps']['data-data'].end_datetime_iso_8601,
                        },
                    };
                });

                function updateCalendarEvents() {
                    let inputEvents = $(`${formEl} [data-model='event']`);
                    $.each(inputEvents, function (index, that) {
                        let id = $(that).attr('data-id');
                        calendarEvents[id]['check'] = $(that).prop("checked");
                    });
                }
                updateCalendarEvents();
                $(`${formEl} #btn-calendar`).attr('data-data', JSON.stringify(btnCalendarData));
                modules.integration.google.calendar.initBtnAddCalendars(formEl);
                $(`${formEl} [data-model='event']`).on("change", function (e) {
                    e.preventDefault();
                    
                    updateCalendarEvents();
                    $(`${formEl} #btn-calendar`).attr('data-data', JSON.stringify(btnCalendarData));
                });
            },
            calendar: function (calendarId, studentId) {
                fullcalendarx.init(calendarId, {
                    events: function (info, successCallback, failureCallback) {
                        return axios.get(route('orientations.index', {
                            'student_id': studentId,
                            'calendar': calendarId,
                            'start_date': info.startStr,
                            'end_date': info.endStr,
                            'filter': filter(calendarId),
                        })).then((response) => {
                            return response.data.data;
                        });
                    },
                    eventDidMount: function (info) {
                        const id = 'calendar_' + info.event.extendedProps['data-data']['id'];
                        $(info.el).addClass('upsert-swal-popup');
                        $(info.el).attr('data-swal', JSON.stringify(info.event.extendedProps['data-swal']));
                        $(info.el).attr('data-data', JSON.stringify(info.event.extendedProps['data-data']));
                        $(info.el).attr('id', id);
                        swalx.init(`#${id}`);
                    },
                });
            },
            swalVideo: function (formEl, data) {
                $(`${formEl} [data-model='name']`).html(data.name);
                $(`${formEl} [name='orientation_id']`).val(data.id).change();
                if (data.video_type == 'file') {
                    $(`${formEl} #videoContainer`).addClass('row mb-7 m-0');
                    $(`${formEl} #videoContainer`).append(`
                        <video controls class="w-100">
                            <source src="${data.video_url}">
                        </video>
                    `);
                } else if (data.video_type == 'link') {
                    $(`${formEl} #videoContainer`).addClass('row mb-7 m-0');
                    $(`${formEl} #videoContainer`).append(`
                        <iframe class="w-100" src="${data.video_url}" title="${data.video_name}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                    `);
                }
            },
            swalAttendanceDestroy: function (formEl, data) {
                scanner.stop();
            },
            swalAttendance: function (formEl, data) {
                $(`${formEl} [name='orientation_id']`).val(data.id).change();
                $(`${formEl} [name='student_id']`).val(data.student_id).change();
                $(`${formEl} [data-model='name']`).html(data.name);
                const video = document.getElementById('qr-video');
                const videoContainer = document.getElementById('video-container');
                const fileSelector = document.getElementById('file-selector');
                function setResult(result) {
                    try {
                        JSON.parse(result.data);
                    } catch (e) {
                        Swal.showValidationMessage('Not a valid format.');
                        return false;
                    }
                    Swal.resetValidationMessage();
                    $(`${formEl} [name='attendance_qrcode']`).val(result.data).change();
                    scanner.stop();
                    Swal.clickConfirm();
                    return true;
                }
                const scanner = new QrScanner(video, result => setResult(result), {
                    highlightScanRegion: true,
                    highlightCodeOutline: true,
                });
                window.scanner = scanner;
                scanner.start().then(() => {
                    QrScanner.listCameras(true).then(cameras => cameras.forEach(camera => {
                        const option = document.createElement('option');
                        option.value = camera.id;
                        option.text = camera.label;
                    }));
                });
                fileSelector.addEventListener('change', event => {
                    const file = fileSelector.files[0];
                    if (!file) {
                        return;
                    }
                    QrScanner.scanImage(file, { returnDetailedScanResult: true })
                        .then(result => setResult(result))
                        .catch(e => setResult({ data: e || 'No QR code found.' }));
                });
            },
            swalOrientation: function (formEl, data) {
                $('.swal2-title').addClass('my-2');
                if (data.orientation_type_id == 1) {
                    $('.swal2-title').html('<span class="bg-e9b7ce p-3 my-2">Compulsory Core Activity</span>');
                } else if (data.orientation_type_id == 2) {
                    $('.swal2-title').html('<span class="bg-d3f3f1 p-3 my-2">Complementary Activity</span>');
                } else if (data.orientation_type_id == 3) {
                    $('.swal2-title').html('<span class="bg-pale-red p-3 my-2">Break</span>');
                }
                $(`${formEl} [name='orientation_id']`).val(data.id).change();
                $(`${formEl} [data-calendar='insert']`).attr('data-data', JSON.stringify(data));
                modules.integration.google.calendar.initBtnAddCalendar(formEl);
                if (data.attendance == null && data.rule_can_scan_attendance == true) {
                    $(`${formEl} #btn-attendance`).show();
                    $(`${formEl} #btn-attendance`).attr('data-data', JSON.stringify(data));
                } else {
                    $(`${formEl} #btn-attendance`).hide();
                }
                if (!data.is_type_activity) {
                    $(`${formEl} #btn-calendar`).hide();
                }
                $(`${formEl} [data-model='name']`).html(data.name);
                $(`${formEl} [data-model='date']`).html(data.start_end_date_dFY);
                $(`${formEl} [data-model='time']`).html(data.start_end_time_readable);
                if (inputx.notEmpty(data.venue)) {
                    $(`${formEl} #venueContainer`).show();
                    $(`${formEl} [data-model='venue']`).html(data.venue);
                } else {
                    $(`${formEl} #venueContainer`).hide();
                }
                if (inputx.notEmpty(data.attendance)) {
                    $(`${formEl} #attendanceContainer`).show();
                    $(`${formEl} [data-model='attendance']`).html(data.attendance);
                    if (inputx.notEmpty(data.rating)) {
                        $(`${formEl} #ratingContainer`).show();
                        $(`${formEl} [data-model='rating']`).html(data.rating_html);
                    } else {
                        $(`${formEl} #ratingContainer`).hide();
                    }
                } else {
                    $(`${formEl} #attendanceContainer`).hide();
                    $(`${formEl} #ratingContainer`).hide();
                }
                if (inputx.notEmpty(data.is_point)) {
                    if (inputx.notEmpty(data.point)) {
                        $(`${formEl} #pointContainer`).show();
                        $(`${formEl} [data-model='point']`).html(data.point);
                    } else {
                        $(`${formEl} #pointContainer`).hide();
                    }
                } else {
                    $(`${formEl} #pointContainer`).hide();
                }
                if (inputx.notEmpty(data.livestream)) {
                    $(`${formEl} #livestreamContainer`).show();
                    $(`${formEl} [data-model='livestream']`).html(data.livestream);
                    $(`${formEl} [data-model='livestream']`).attr('href', data.livestream);
                } else {
                    $(`${formEl} #livestreamContainer`).hide();
                }
                if (inputx.notEmpty(data.agenda) && data.agenda !== '""') {
                    $(`${formEl} #agendaContainer`).show();
                    try {
                        $(`${formEl} [data-model='agenda']`).html(JSON.parse(data.agenda));
                    } catch (err) {
                        $(`${formEl} [data-model='agenda']`).html(data.agenda);
                    }
                } else {
                    $(`${formEl} #agendaContainer`).hide();
                }
                if (inputx.notEmpty(data.synopsis) && data.synopsis !== '""') {
                    $(`${formEl} #synopsisContainer`).show();
                    try {
                        $(`${formEl} [data-model='synopsis']`).html(JSON.parse(data.synopsis));
                    } catch (err) {
                        $(`${formEl} [data-model='synopsis']`).html(data.synopsis);
                    }
                } else {
                    $(`${formEl} #synopsisContainer`).hide();
                }
                if (inputx.notEmpty(data.presenter)) {
                    $(`${formEl} #presenterContainer`).show();
                    $(`${formEl} [data-model='presenter']`).html(data.presenter);
                } else {
                    $(`${formEl} #presenterContainer`).hide();
                }
                swalx.init()
            },
            swalRate: function (formEl, data) {
                $(`${formEl} [data-model='name']`).html(data.name);
            },
            swalRsvp: function (formEl, data) {
                $(`${formEl} [name='is_go']`).val(data.orientation_rsvp_is_go).change();
            },
            clickSwalRate: function (formEl, id) {
                $(`${formEl} #btn-rate-${id}`).click();
            },
            init: function (data = {}) {
                let tabLinkTimelineEl = document.querySelector('#tab_link_timeline'),
                    timelineHrefTitle = $(tabLinkTimelineEl).data("href-title") || null,
                    tabLinkTimetableEl = document.querySelector('#tab_link_timetable'),
                    timetableHrefTitle = $(tabLinkTimetableEl).data("href-title") || null;
                if (!tabLinkTimelineEl) {
                    return;
                } else {
                    tabLinkTimelineEl.addEventListener('shown.bs.tab', function (event) {
                        $(timelineHrefTitle).show();
                    });
                    tabLinkTimelineEl.addEventListener('hidden.bs.tab', function (event) {
                        $(timelineHrefTitle).hide();
                    });
                }
                if (!tabLinkTimetableEl) {
                    return;
                } else {
                    tabLinkTimetableEl.addEventListener('shown.bs.tab', function (event) {
                        $(timetableHrefTitle).show();
                    });
                    tabLinkTimetableEl.addEventListener('hidden.bs.tab', function (event) {
                        $(timetableHrefTitle).hide();
                    });
                }
            }
        };
    }();
})(jQuery, window);