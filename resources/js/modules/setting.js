(function ($, window) {
    "use strict";
    window.modules = window.modules || {};
    window.modules.setting = window.modules.setting || {};
    window.modules.setting.admin = window.modules.setting.admin || {};
    window.modules.setting.admin.faculty = (function () {
        return {
            index: {
                swalBulkActive: function (elInput, data) {
                    var checkboxAll = $('input#facultyDtCbx'),
                        selectedCount = $(`${elInput} #selectedCount`),
                        count = checkboxAll.attr('data-count') || 0,
                        value = $('input#facultyDtCbx').val();
                    selectedCount.html(count);
                    $(`${elInput} [name='checkbox']`).val(value).change();
                },
            }
        }
    })();
    window.modules.setting.admin.orientation = (function () {
        return {
            index: {
                upsertIntake: function (elInput, data) {
                    if (data.id === undefined) {
                        $(`${elInput} [name='_method']`).val('POST').change();
                    } else {
                        $(`${elInput} [name='orientation_activities_message']`).val(data.orientation_activities_message).change();
                        $(`${elInput} [name='timetable_message']`).val(data.timetable_message).change();
                        $(`${elInput} [name='is_orientation_activities_active']`).prop('checked', data.is_orientation_activities_active);
                        $(`${elInput} [name='is_timetable_active']`).prop('checked', data.is_timetable_active);
                    }
                    if (!data.is_orientation_activities_active) {
                        $(`${elInput} [id='orientation_activities_message_container']`).show();
                        $(`${elInput} [name='orientation_activities_message']`).attr('required', true);
                    } else {
                        $(`${elInput} [id='orientation_activities_message_container']`).hide();
                        $(`${elInput} [name='orientation_activities_message']`).val('').change();
                        $(`${elInput} [name='orientation_activities_message']`).removeAttr('required');
                    }
                    if (!data.is_timetable_active) {
                        $(`${elInput} [id='timetable_message_container']`).show();
                        $(`${elInput} [name='timetable_message']`).attr('required', true);
                    } else {
                        $(`${elInput} [id='timetable_message_container']`).hide();
                        $(`${elInput} [name='timetable_message']`).val('').change();
                        $(`${elInput} [name='timetable_message']`).removeAttr('required');
                    }
                    $(`${elInput} [name='is_orientation_activities_active']`).change(function () {
                        if (!this.checked) {
                            $(`${elInput} [id='orientation_activities_message_container']`).show();
                            $(`${elInput} [name='orientation_activities_message']`).attr('required', true);
                        } else {
                            $(`${elInput} [id='orientation_activities_message_container']`).hide();
                            $(`${elInput} [name='orientation_activities_message']`).val('').change();
                            $(`${elInput} [name='orientation_activities_message']`).removeAttr('required');
                        }
                    });
                    $(`${elInput} [name='is_timetable_active']`).change(function () {
                        if (!this.checked) {
                            $(`${elInput} [id='timetable_message_container']`).show();
                            $(`${elInput} [name='timetable_message']`).attr('required', true);
                        } else {
                            $(`${elInput} [id='timetable_message_container']`).hide();
                            $(`${elInput} [name='timetable_message']`).val('').change();
                            $(`${elInput} [name='timetable_message']`).removeAttr('required');
                        }
                    });
                }
            }
        }
    })();
    window.modules.setting.admin.mail = (function () {
        return {
            edit: {
                upsert: function () {
                    tinymcex.init("content_editor", "#content");
                    // swalx.init('#preview');
                    // dtx.upsertSwalDt('preview');
                }
            },
            index: {
                swalShow: function (elInput, data) {
                    let frame = $(`${elInput} #container`).contents().find('body');
                    frame.html(data.body);
                    let height = $(`iframe#container`)[0].contentWindow.document.body.scrollHeight;
                    document.getElementById('container').height = height;
                },
                swalUpsert: function (elInput, data) {
                    $(`${elInput} [name='title']`).val(data.title).change();
                    $(`${elInput} [name='subject']`).val(data.subject).change();
                    $(`${elInput} [name='content']`).val(data.content).change();
                    tinymcex.init("content", "#content")
                },
            }
        }
    })();
})(jQuery, window);