(function ($, window) {
    "use strict";
    window.modules = window.modules || {};
    window.modules['role'] = function () {
        return {
            upsert: function (elInput, data) {
                if (data.id === undefined) {
                    $(`${elInput} [name='_method']`).val('POST').change();
                } else {
                    $(`${elInput} [name='title']`).val(data.title).change();
                    $(`${elInput} [name='description']`).val(data.description).change();
                    $(`${elInput} [name='is_active']`).prop('checked', data.is_active);
                    $.each(data.permissions || [], function (index, value) {
                        $(`${elInput} [id='permission_${value}']`).prop('checked', true);
                    });
                }
            },
        };
    }();
})(jQuery, window);