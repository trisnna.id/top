(function ($, window) {
    "use strict";
    window.modules = window.modules || {};
    window.modules.student = window.modules.student || {};
    window.modules.student.index = function () {
        return {
            swalProfile: function (formEl, data) {
                $(`${formEl} [name='name']`).val(data.name).change();
                $(`${formEl} [data-model='name']`).html(data.name);

                $(`${formEl} [name='id_number']`).val(data.id_number).change();
                $(`${formEl} [data-model='id_number']`).html(data.id_number);

                $(`${formEl} [name='taylors_email']`).val(data.taylors_email).change();
                $(`${formEl} [data-model='taylors_email']`).html(data.taylors_email);

                $(`${formEl} [name='personal_email']`).val(data.personal_email).change();
                $(`${formEl} [data-model='personal_email']`).html(data.personal_email);

                $(`${formEl} [name='nationality']`).val(data.nationality).change();
                $(`${formEl} [data-model='nationality']`).html(data.nationality);

                $(`${formEl} [name='intake_number']`).val(data.intake_number).change();
                $(`${formEl} [data-model='intake_number']`).html(data.intake_number);

                $(`${formEl} [name='faculty_name']`).val(data.faculty_name).change();
                $(`${formEl} [data-model='faculty_name']`).html(data.faculty_name);

                $(`${formEl} [name='school_name']`).val(data.school_name).change();
                $(`${formEl} [data-model='school_name']`).html(data.school_name);

                $(`${formEl} [name='programme_name']`).val(data.programme_name).change();
                $(`${formEl} [data-model='programme_name']`).html(data.programme_name);

                $(`${formEl} [name='level_of_study']`).val(data.level_of_study).change();
                $(`${formEl} [data-model='level_of_study']`).html(data.level_of_study);

                $(`${formEl} [name='campus']`).val(data.campus).change();
                $(`${formEl} [data-model='campus']`).html(data.campus);

                $(`${formEl} [name='mobility']`).val(data.mobility).change();
                $(`${formEl} [data-model='mobility']`).html(data.mobility);

                $(`${formEl} [name='contact_number']`).val(data.contact_number).change();
                $(`${formEl} [data-model='contact_number']`).html(data.contact_number);

                $(`${formEl} [name='flame_mentor_name']`).val(data.flame_mentor_name).change();
                $(`${formEl} [data-model='flame_mentor_name']`).html(data.flame_mentor_name || '-');

                $(`${formEl} .image-input-wrapper`).attr('style', `background-image: url(${data.avatar_url})`);
                $.each($(`${formEl} input`), function (index, value) {
                    inputx.disabledReadonly(value);
                });
            },
            swalPoint: function (formEl, data) {
                let element = document.getElementById('points-chart');
                if (!element) {
                    return;
                }
                let height = parseInt(KTUtil.css(element, 'height')),
                    labelColor = KTUtil.getCssVariableValue('--kt-gray-500'),
                    borderColor = KTUtil.getCssVariableValue('--kt-gray-200'),
                    baseColor = KTUtil.getCssVariableValue('--kt-primary'),
                    secondaryColor = KTUtil.getCssVariableValue('--kt-gray-300'),
                    orientationAttendance = data.orientation_attendance || 0,
                    orientationRating = data.orientation_rating || 0,
                    postOrientationSurvey = data.post_orientation_survey || 0,
                    preOrientationQuiz = data.pre_orientation_quiz || 0,
                    preOrientationSurvey = data.pre_orientation_survey || 0,
                    options = {
                        series: [{
                            name: "Points",
                            data: [preOrientationSurvey, preOrientationQuiz, orientationAttendance, orientationRating, postOrientationSurvey],
                        }],
                        colors: ['#264653', '#2a9d8f', '#e9c46a', '#f4a261', '#e76f51'],
                        chart: {
                            height: height,
                            type: 'bar',
                        },
                        plotOptions: {
                            bar: {
                                columnWidth: '45%',
                                distributed: true,
                            }
                        },
                        legend: {
                            show: false,
                        },
                        xaxis: {
                            categories: [
                                'Pre-Orientation Survey', 
                                'Pre-Orientation Quiz', 
                                'Orientation Attendance',
                                'Orientation Ratings', 
                                'Post Orientation Survey'
                            ],
                        },
                    },
                    chart = new ApexCharts(element, options);
                chart.render();
            },
        };
    }();
})(jQuery, window);