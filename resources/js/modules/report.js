(function ($, window) {
    "use strict";
    window.modules = window.modules || {};
    window.modules.report = (function () {
        return {
            studentRegistration: {
                index: {
                    filter: function (id) {
                        $(`#${id}_filter_updated_at`).daterangepicker({
                            "timePicker": true,
                            "showDropdowns": true,
                            "minDate": new Date(moment('2023-01-01').format('YYYY-MM-DD')),
                            "maxDate": new Date(),
                            "autoUpdateInput": false,
                            "locale": {
                                "format": 'DD-MM-YYYY h:mm a'
                            }
                        }).on("apply.daterangepicker", function (e, picker) {
                            picker.element.val(picker.startDate.format(picker.locale.format) + ' - ' + picker.endDate.format(picker.locale.format));
                        });
                    },
                }
            },
        };
    })();
})(jQuery, window);
