(function ($, window) {
    "use strict";
    window.modules = window.modules || {};
    window.modules.dashboard = window.modules.dashboard || {};
    window.modules.dashboard.admin = window.modules.dashboard.admin || {};
    window.modules.dashboard.admin.index = (function () {
        var filter = function (id) {
            return {
                intake: $(`#${id}_filter_intake`).val(),
            };
        };
        var setFilterIntake = function (id, value) {
            $(`#${id}_filter_intake`).val(value).change();
        };
        return {
            orientation: function () {
                $("#orientationIntakeDtContainer").hide();
                $("#orientationProgrammeDtContainer").hide();
                $("#orientationActivity").on("change", function (e) {
                    e.preventDefault();
                    if (this.value == 1) {
                        dtx.reloadSingleDt("orientationTypeDt");
                        $("#orientationTypeDtContainer").show();
                        $("#orientationIntakeDtContainer").hide();
                        $("#orientationProgrammeDtContainer").hide();
                    } else if (this.value == 2) {
                        dtx.reloadSingleDt("orientationIntakeDt");
                        $("#orientationTypeDtContainer").hide();
                        $("#orientationIntakeDtContainer").show();
                        $("#orientationProgrammeDtContainer").hide();
                    } else if (this.value == 3) {
                        dtx.reloadSingleDt("orientationProgrammeDt");
                        $("#orientationTypeDtContainer").hide();
                        $("#orientationIntakeDtContainer").hide();
                        $("#orientationProgrammeDtContainer").show();
                    }
                });
            },
            renderResourceChart: function (elId, data) {
                data = JSON.parse(data);
                var element = document.getElementById(elId);
                if (!element) {
                    console.log(element);
                }
                var options = {
                    series: data.series,
                    colors: ["#d64045", "#9ed8db", "#467599", "#1d3354"],
                    chart: {
                        type: "donut",
                    },
                    dataLabels: {
                        formatter: function (val, opts) {
                            return opts.w.config.series[opts.seriesIndex];
                        },
                    },
                    labels: data.labels,
                    legend: {
                        position: "bottom",
                    },
                    plotOptions: {
                        pie: {
                            donut: {
                                labels: {
                                    show: true,
                                    name: {
                                        show: true,
                                        fontSize: "22px",
                                        fontFamily: "Rubik",
                                        color: "#dfsda",
                                        offsetY: -10,
                                    },
                                    value: {
                                        show: true,
                                        fontSize: "16px",
                                        fontFamily:
                                            "Helvetica, Arial, sans-serif",
                                        color: undefined,
                                        offsetY: 16,
                                        formatter: function (val) {
                                            return val;
                                        },
                                    },
                                    total: {
                                        show: true,
                                        label: "Total",
                                        color: "#373d3f",
                                        formatter: function (w) {
                                            return w.globals.seriesTotals.reduce(
                                                (a, b) => {
                                                    return a + b;
                                                },
                                                0
                                            );
                                        },
                                    },
                                },
                            },
                        },
                    },
                    responsive: [
                        {
                            breakpoint: 480,
                            options: {
                                chart: {
                                    width: 200,
                                },
                                legend: {
                                    position: "bottom",
                                },
                            },
                        },
                    ],
                };
                var chart = new ApexCharts(element, options);
                chart.render();
            },
            orientationTypeDtRenderChart: function (
                setting,
                dtId,
                chartId,
                tableName
            ) {
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}" height="400"></div>`
                );
                new ApexCharts(document.querySelector(`#${chartId}`), {
                    chart: { height: 400, type: "bar" },
                    series: [{ data: [] }],
                    xaxis: { categories: [] },
                }).render();
                var jsonData = setting.json.data;
                var category = $.map(jsonData, function (e) {
                    return e["name"];
                });
                var colorCode = $.map(jsonData, function (e) {
                    return e["color_code"];
                });
                var series = $.map(jsonData, function (e) {
                    return parseFloat(e["total"]);
                });
                var width = $(`#${dtId}_wrapper .custom-dt-loader`).width();
                let animation = true;
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}"></div>`
                );
                var chartEl = document.querySelector(`#${chartId}`),
                    chartConfig = {
                        series: series,
                        colors: colorCode,
                        chart: {
                            type: "donut",
                        },
                        dataLabels: {
                            formatter: function (val, opts) {
                                return opts.w.config.series[opts.seriesIndex];
                            },
                        },
                        labels: category,
                        legend: {
                            position: "bottom",
                        },
                        plotOptions: {
                            pie: {
                                donut: {
                                    labels: {
                                        show: true,
                                        name: {
                                            show: true,
                                            fontSize: "22px",
                                            fontFamily: "Rubik",
                                            color: "#dfsda",
                                            offsetY: -10,
                                        },
                                        value: {
                                            show: true,
                                            fontSize: "16px",
                                            fontFamily:
                                                "Helvetica, Arial, sans-serif",
                                            color: undefined,
                                            offsetY: 16,
                                            formatter: function (val) {
                                                return val;
                                            },
                                        },
                                        total: {
                                            show: true,
                                            label: "Total",
                                            color: "#373d3f",
                                            formatter: function (w) {
                                                return w.globals.seriesTotals.reduce(
                                                    (a, b) => {
                                                        return a + b;
                                                    },
                                                    0
                                                );
                                            },
                                        },
                                    },
                                },
                            },
                        },
                        responsive: [
                            {
                                breakpoint: 480,
                                options: {
                                    chart: {
                                        width: 200,
                                    },
                                    legend: {
                                        position: "bottom",
                                    },
                                },
                            },
                        ],
                    };
                if (typeof chartEl !== undefined && chartEl !== null) {
                    var chart = new ApexCharts(chartEl, chartConfig);
                    chart.render();
                }
            },
            orientationIntakeDtRenderChart: function (
                setting,
                dtId,
                chartId,
                tableName
            ) {
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}" height="400"></div>`
                );
                new ApexCharts(document.querySelector(`#${chartId}`), {
                    chart: { height: 400, type: "bar" },
                    series: [{ data: [] }],
                    xaxis: { categories: [] },
                }).render();
                var jsonData = setting.json.data;
                var category = $.map(jsonData, function (e) {
                    return e["name"];
                });
                var colorCode = ["#d64045", "#9ed8db", "#467599", "#1d3354"];
                var series = $.map(jsonData, function (e) {
                    return parseFloat(e["total"]);
                });
                var width = $(`#${dtId}_wrapper .custom-dt-loader`).width();
                let animation = true;
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}"></div>`
                );
                var chartEl = document.querySelector(`#${chartId}`),
                    chartConfig = {
                        series: series,
                        colors: colorCode,
                        chart: {
                            type: "donut",
                        },
                        dataLabels: {
                            formatter: function (val, opts) {
                                return opts.w.config.series[opts.seriesIndex];
                            },
                        },
                        labels: category,
                        legend: {
                            position: "bottom",
                        },
                        plotOptions: {
                            pie: {
                                donut: {
                                    labels: {
                                        show: true,
                                        name: {
                                            show: true,
                                            fontSize: "22px",
                                            fontFamily: "Rubik",
                                            color: "#dfsda",
                                            offsetY: -10,
                                        },
                                        value: {
                                            show: true,
                                            fontSize: "16px",
                                            fontFamily:
                                                "Helvetica, Arial, sans-serif",
                                            color: undefined,
                                            offsetY: 16,
                                            formatter: function (val) {
                                                return val;
                                            },
                                        },
                                        total: {
                                            show: true,
                                            label: "Total",
                                            color: "#373d3f",
                                            formatter: function (w) {
                                                return w.globals.seriesTotals.reduce(
                                                    (a, b) => {
                                                        return a + b;
                                                    },
                                                    0
                                                );
                                            },
                                        },
                                    },
                                },
                            },
                        },
                        responsive: [
                            {
                                breakpoint: 480,
                                options: {
                                    chart: {
                                        width: 200,
                                    },
                                    legend: {
                                        position: "bottom",
                                    },
                                },
                            },
                        ],
                    };
                if (typeof chartEl !== undefined && chartEl !== null) {
                    var chart = new ApexCharts(chartEl, chartConfig);
                    chart.render();
                }
            },
            orientationProgrammeDtRenderChart: function (
                setting,
                dtId,
                chartId,
                tableName
            ) {
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}" height="400"></div>`
                );
                new ApexCharts(document.querySelector(`#${chartId}`), {
                    chart: { height: 400, type: "bar" },
                    series: [{ data: [] }],
                    xaxis: { categories: [] },
                }).render();
                var jsonData = setting.json.data;
                var category = $.map(jsonData, function (e) {
                    return e["name"];
                });
                var colorCode = ["#d64045", "#9ed8db", "#467599", "#1d3354"];
                var total = $.map(jsonData, function (e) {
                    return parseFloat(e["total"]);
                });

                var series = [
                    {
                        name: "No. of Orientation Activities",
                        data: total,
                    },
                ];
                var width = $(`#${dtId}_wrapper .custom-dt-loader`).width();
                var height = total.length * 40 + 115;
                let animation = true;
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}" height="${height}"></div>`
                );
                var barChartEl = document.querySelector(`#${chartId}`),
                    barChartConfig = {
                        chart: {
                            height: height,
                            width: width,
                            type: "bar",
                            animations: {
                                enabled: animation,
                            },
                            toolbar: {
                                show: false,
                            },
                        },
                        plotOptions: {
                            bar: {
                                horizontal: true,
                                barHeight: "55%",
                                distributed: true,
                            },
                        },
                        fill: {
                            colors: colorCode,
                        },
                        grid: {
                            strokeDashArray: 1,
                            row: {
                                colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
                                opacity: 0.5,
                            },
                            xaxis: {
                                lines: {
                                    show: false,
                                },
                            },
                        },
                        stroke: {
                            show: true,
                            curve: "smooth",
                        },
                        dataLabels: {
                            enabled: false,
                        },
                        legend: {
                            show: false,
                        },
                        series: series,
                        xaxis: {
                            categories: category,
                        },
                    };
                if (typeof barChartEl !== undefined && barChartEl !== null) {
                    var barChart = new ApexCharts(barChartEl, barChartConfig);
                    barChart.render();
                }
            },
            renderAttendanceOrientationActivityChart: function (
                setting,
                dtId,
                chartId,
                tableName
            ) {
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}" height="400"></div>`
                );
                new ApexCharts(document.querySelector(`#${chartId}`), {
                    chart: { height: 400, type: "bar" },
                    series: [{ data: [] }],
                    xaxis: { categories: [] },
                }).render();
                var jsonData = setting.json.data;
                var category = $.map(jsonData, function (e) {
                    return e["name"];
                });
                var colorCode = $.map(jsonData, function (e) {
                    return e["color_code"];
                });
                var total = $.map(jsonData, function (e) {
                    return parseFloat(e["total"]);
                });
                var series = [
                    {
                        name: "No. of Attendance",
                        data: total,
                    },
                ];
                var width = $(`#${dtId}_wrapper .custom-dt-loader`).width();
                var height = total.length * 40 + 115;
                let animation = true;
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}" height="${height}"></div>`
                );
                var barChartEl = document.querySelector(`#${chartId}`),
                    barChartConfig = {
                        chart: {
                            height: height,
                            width: width,
                            type: "bar",
                            animations: {
                                enabled: animation,
                            },
                            toolbar: {
                                show: false,
                            },
                        },
                        fill: {
                            colors: colorCode,
                        },
                        plotOptions: {
                            bar: {
                                horizontal: true,
                                barHeight: "55%",
                                distributed: true,
                            },
                        },
                        grid: {
                            strokeDashArray: 1,
                            row: {
                                colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
                                opacity: 0.5,
                            },
                            xaxis: {
                                lines: {
                                    show: false,
                                },
                            },
                        },
                        stroke: {
                            show: true,
                            curve: "smooth",
                        },
                        dataLabels: {
                            enabled: false,
                        },
                        legend: {
                            show: false,
                        },
                        series: series,
                        xaxis: {
                            categories: category,
                        },
                    };
                if (typeof barChartEl !== undefined && barChartEl !== null) {
                    var barChart = new ApexCharts(barChartEl, barChartConfig);
                    barChart.render();
                }
            },
            renderRsvpOrientationActivityChart: function (
                setting,
                dtId,
                chartId,
                tableName
            ) {
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}" height="400"></div>`
                );
                new ApexCharts(document.querySelector(`#${chartId}`), {
                    chart: { height: 400, type: "bar" },
                    series: [{ data: [] }],
                    xaxis: { categories: [] },
                }).render();
                var jsonData = setting.json.data;
                var category = $.map(jsonData, function (e) {
                    return e["name"];
                });
                var totalYes = $.map(jsonData, function (e) {
                    return parseFloat(e["total_yes"]);
                });
                var totalNo = $.map(jsonData, function (e) {
                    return parseFloat(e["total_no"]);
                });
                var series = [
                    {
                        name: "RSVP Yes",
                        data: totalYes,
                    },
                    {
                        name: "RSVP No",
                        data: totalNo,
                    },
                ];
                var width = $(`#${dtId}_wrapper .custom-dt-loader`).width();
                var height = totalYes.length * 40 + 115;
                let animation = true;
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}" height="${height}"></div>`
                );
                var barChartEl = document.querySelector(`#${chartId}`),
                    barChartConfig = {
                        colors: ["#9ed8db", "#d64045"],
                        chart: {
                            height: height,
                            width: width,
                            type: "bar",
                            animations: {
                                enabled: animation,
                            },
                            toolbar: {
                                show: false,
                            },
                        },
                        plotOptions: {
                            bar: {
                                horizontal: true,
                                barHeight: "55%",
                            },
                        },
                        grid: {
                            strokeDashArray: 1,
                            row: {
                                colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
                                opacity: 0.5,
                            },
                            xaxis: {
                                lines: {
                                    show: false,
                                },
                            },
                        },
                        stroke: {
                            show: true,
                            curve: "smooth",
                        },
                        dataLabels: {
                            enabled: false,
                        },
                        series: series,
                        xaxis: {
                            categories: category,
                        },
                    };
                if (typeof barChartEl !== undefined && barChartEl !== null) {
                    var barChart = new ApexCharts(barChartEl, barChartConfig);
                    barChart.render();
                }
            },
            renderPreOrientationCompletionChart: function (
                setting,
                dtId,
                chartId,
                tableName
            ) {
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}" height="400"></div>`
                );
                new ApexCharts(document.querySelector(`#${chartId}`), {
                    chart: { height: 400, type: "bar" },
                    series: [{ data: [] }],
                    xaxis: { categories: [] },
                }).render();
                var jsonData = setting.json.data;
                var category = $.map(jsonData, function (e) {
                    return e["name"];
                });
                var percentage = $.map(jsonData, function (e) {
                    return parseFloat(e["percentage"]);
                });
                var totalCompleteStudents = $.map(jsonData, function (e) {
                    return parseFloat(e["complete"]);
                });
                var series = [
                    {
                        name: "Completion",
                        data: percentage,
                    },
                ];
                var width = $(`#${dtId}_wrapper .custom-dt-loader`).width();
                var height = percentage.length * 40 + 115;
                let animation = true;
                $(`#${dtId}_wrapper .custom-table`).html(
                    `<div id="${chartId}" height="${height}"></div>`
                );
                var barChartEl = document.querySelector(`#${chartId}`),
                    barChartConfig = {
                        colors: [
                            "#345b80",
                            "#c3ebeb",
                            "#d44444",
                            "#e4bcbc",
                            "#9ed8db",
                            "#6796b9",
                        ],
                        chart: {
                            height: height,
                            width: width,
                            type: "bar",
                            animations: {
                                enabled: animation,
                            },
                            toolbar: {
                                show: false,
                            },
                        },
                        plotOptions: {
                            bar: {
                                distributed: true,
                                barHeight: "55%",
                            },
                        },
                        grid: {
                            strokeDashArray: 1,
                            row: {
                                colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
                                opacity: 0.5,
                            },
                            xaxis: {
                                lines: {
                                    show: false,
                                },
                            },
                        },
                        stroke: {
                            show: true,
                            curve: "smooth",
                        },
                        dataLabels: {
                            enabled: false,
                        },
                        series: series,
                        xaxis: {
                            categories: category,
                        },
                        tooltip: {
                            y: {
                                formatter: function (value, tooltip) {
                                    return `${value}% | ${totalCompleteStudents[tooltip.dataPointIndex]} Students`
                                }
                            }
                        },
                        legend: {
                            show: false,
                        },
                    };
                if (typeof barChartEl !== undefined && barChartEl !== null) {
                    var barChart = new ApexCharts(barChartEl, barChartConfig);
                    barChart.render();
                }
            },
            applyToAll: function (id, filterValue) {
                if (id !== "activeStudent") {
                    setFilterIntake("activeStudent", filterValue.intake);
                    this.countActiveStudent();
                }
                if (id !== "studentLogin") {
                    setFilterIntake("studentLogin", filterValue.intake);
                    this.countStudentLogin();
                }
                if (id !== "attendanceOrientation") {
                    setFilterIntake(
                        "attendanceOrientation",
                        filterValue.intake
                    );
                    this.countAttendanceOrientation();
                }
                if (id !== "attendanceOrientationAcademic") {
                    setFilterIntake(
                        "attendanceOrientationAcademic",
                        filterValue.intake
                    );
                    this.countAttendanceOrientationAcademic();
                }
                if (id !== "preOrientationComplete") {
                    setFilterIntake(
                        "preOrientationComplete",
                        filterValue.intake
                    );
                    this.countPreOrientationComplete();
                }
                if (id !== "orientationTypeDt") {
                    setFilterIntake("orientationTypeDt", filterValue.intake);
                    this.countOrientationTypeDt();
                }
                if (id !== "attendanceOrientationActivityDt") {
                    setFilterIntake(
                        "attendanceOrientationActivityDt",
                        filterValue.intake
                    );
                    this.countAttendanceOrientationActivityDt();
                }
                if (id !== "rsvpOrientationActivityDt") {
                    setFilterIntake(
                        "rsvpOrientationActivityDt",
                        filterValue.intake
                    );
                    this.countRsvpOrientationActivityDt();
                }
                if (id !== "preOrientationCompletionDt") {
                    setFilterIntake(
                        "preOrientationCompletionDt",
                        filterValue.intake
                    );
                    this.countPreOrientationCompletionDt();
                }
                if (id !== "orientationCalendar") {
                    setFilterIntake("orientationCalendar", filterValue.intake);
                    this.filterOrientationCalendar();
                }
            },
            countActiveStudent: function () {
                const filterValue = filter("activeStudent");
                if ($(`#activeStudent_apply_all`).is(":checked")) {
                    this.applyToAll("activeStudent", filterValue);
                    $(`#activeStudent_apply_all`).prop("checked", false);
                }
                return axios
                    .get(
                        route("dashboard.index", {
                            count: "activeStudent",
                            filter: filterValue,
                        })
                    )
                    .then((response) => {
                        const total = response.data.data.total || 0,
                            local = response.data.data.local || 0,
                            international =
                                response.data.data.international || 0;
                        new countUp.CountUp(
                            "activeStudentTotal",
                            total
                        ).start();
                        new countUp.CountUp(
                            "activeStudentLocal",
                            local
                        ).start();
                        new countUp.CountUp(
                            "activeStudentInternational",
                            international
                        ).start();
                    });
            },
            countStudentLogin: function () {
                const filterValue = filter("studentLogin");
                if ($(`#studentLogin_apply_all`).is(":checked")) {
                    this.applyToAll("studentLogin", filterValue);
                    $(`#studentLogin_apply_all`).prop("checked", false);
                }
                return axios
                    .get(
                        route("dashboard.index", {
                            count: "studentLogin",
                            filter: filterValue,
                        })
                    )
                    .then((response) => {
                        const total = response.data.data.total || 0;
                        new countUp.CountUp("studentLoginTotal", total).start();
                    });
            },
            countAttendanceOrientation: function () {
                const filterValue = filter("attendanceOrientation");
                if ($(`#attendanceOrientation_apply_all`).is(":checked")) {
                    this.applyToAll("attendanceOrientation", filterValue);
                    $(`#attendanceOrientation_apply_all`).prop(
                        "checked",
                        false
                    );
                }
                return axios
                    .get(
                        route("dashboard.index", {
                            count: "attendanceOrientation",
                            filter: filterValue,
                        })
                    )
                    .then((response) => {
                        const percentage = response.data.data.percentage || 0;
                        new countUp.CountUp(
                            "attendanceOrientationPercentage",
                            percentage
                        ).start();
                    });
            },
            countAttendanceOrientationAcademic: function () {
                const filterValue = filter("attendanceOrientationAcademic");
                if (
                    $(`#attendanceOrientationAcademic_apply_all`).is(":checked")
                ) {
                    this.applyToAll(
                        "attendanceOrientationAcademic",
                        filterValue
                    );
                    $(`#attendanceOrientationAcademic_apply_all`).prop(
                        "checked",
                        false
                    );
                }
                return axios
                    .get(
                        route("dashboard.index", {
                            count: "attendanceOrientationAcademic",
                            filter: filter("attendanceOrientationAcademic"),
                        })
                    )
                    .then((response) => {
                        const percentage = response.data.data.percentage || 0;
                        new countUp.CountUp(
                            "attendanceOrientationAcademicPercentage",
                            percentage
                        ).start();
                    });
            },
            countPreOrientationComplete: function () {
                const filterValue = filter("preOrientationComplete");
                if ($(`#preOrientationComplete_apply_all`).is(":checked")) {
                    this.applyToAll("preOrientationComplete", filterValue);
                    $(`#preOrientationComplete_apply_all`).prop(
                        "checked",
                        false
                    );
                }
                return axios
                    .get(
                        route("dashboard.index", {
                            count: "preOrientationComplete",
                            filter: filterValue,
                        })
                    )
                    .then((response) => {
                        const total = response.data.data.total || 0;
                        new countUp.CountUp(
                            "preOrientationCompleteTotal",
                            total
                        ).start();
                    });
            },
            countOrientationTypeDt: function () {
                const filterValue = filter("orientationTypeDt");
                if ($(`#orientationTypeDt_apply_all`).is(":checked")) {
                    this.applyToAll("orientationTypeDt", filterValue);
                    $(`#orientationTypeDt_apply_all`).prop("checked", false);
                }
                dtx.reloadSingleDt("orientationTypeDt");
                dtx.reloadSingleDt("orientationIntakeDt");
                dtx.reloadSingleDt("orientationProgrammeDt");
            },
            countAttendanceOrientationActivityDt: function () {
                const filterValue = filter("attendanceOrientationActivityDt");
                if (
                    $(`#attendanceOrientationActivityDt_apply_all`).is(
                        ":checked"
                    )
                ) {
                    this.applyToAll(
                        "attendanceOrientationActivityDt",
                        filterValue
                    );
                    $(`#attendanceOrientationActivityDt_apply_all`).prop(
                        "checked",
                        false
                    );
                }
                dtx.reloadSingleDt("attendanceOrientationActivityDt");
            },
            countRsvpOrientationActivityDt: function () {
                const filterValue = filter("rsvpOrientationActivityDt");
                if ($(`#rsvpOrientationActivityDt_apply_all`).is(":checked")) {
                    this.applyToAll("rsvpOrientationActivityDt", filterValue);
                    $(`#rsvpOrientationActivityDt_apply_all`).prop(
                        "checked",
                        false
                    );
                }
                dtx.reloadSingleDt("rsvpOrientationActivityDt");
            },
            countPreOrientationCompletionDt: function () {
                const filterValue = filter("preOrientationCompletionDt");
                if ($(`#preOrientationCompletionDt_apply_all`).is(":checked")) {
                    this.applyToAll("preOrientationCompletionDt", filterValue);
                    $(`#preOrientationCompletionDt_apply_all`).prop(
                        "checked",
                        false
                    );
                }
                dtx.reloadSingleDt("preOrientationCompletionDt");
            },
            filterOrientationCalendar: function () {
                const filterValue = filter("orientationCalendar");
                if ($(`#orientationCalendar_apply_all`).is(":checked")) {
                    this.applyToAll("orientationCalendar", filterValue);
                    $(`#orientationCalendar_apply_all`).prop("checked", false);
                }
                fullcalendars.orientationCalendar.refetchEvents();
            },
        };
    })();
    window.modules.dashboard.student = window.modules.dashboard.student || {};
    window.modules.dashboard.student.index = (function () {
        return {
            search: function () {
                let element = document.querySelector("#faq-search");
                if (!element) {
                    return;
                }
                let closeEl = element.querySelector(
                    '[data-kt-search-element="close"]'
                ),
                    contentEl = element.querySelector(
                        '[data-kt-search-element="content"]'
                    ),
                    inputEl = element.querySelector(
                        '[data-kt-search-element="input"]'
                    );
                $(inputEl).on("keyup", function () {
                    if (this.value.length >= 2) {
                        $(contentEl).show();
                    } else {
                        $(contentEl).hide();
                    }
                    if (this.value.length >= 1) {
                        dtx.reloadSingleDt("faqDt");
                    }
                });
                $(closeEl).on("click", function (e) {
                    $(inputEl).val("").change();
                    $(contentEl).hide();
                });
            },
        };
    })();
})(jQuery, window);
