const CLIENT_ID = ConfigApp.calendarClientId;
const API_KEY = ConfigApp.calendarApiKey;
const DISCOVERY_DOC = process.env.MIX_GOOGLE_CALENDAR_DISCOVERY_DOC;
const SCOPES = process.env.MIX_GOOGLE_CALENDAR_SCOPES;

(function ($, window) {
    "use strict";
    window.modules = window.modules || {};
    window.modules.integration = window.modules.integration || {};
    window.modules.integration.google = window.modules.integration.google || {};
    window.modules.integration.google.calendar = function () {
        let tokenClient;
        let gapiInited = false;
        let gisInited = false;

        function gapiLoaded() {
            gapi.load('client', initializeGapiClient);
        }

        async function initializeGapiClient() {
            await gapi.client.init({
                apiKey: API_KEY,
                discoveryDocs: [DISCOVERY_DOC],
            });
            gapiInited = true;
        }

        function gisLoaded() {
            tokenClient = google.accounts.oauth2.initTokenClient({
                client_id: CLIENT_ID,
                scope: SCOPES,
                callback: '', // defined later
            });
            gisInited = true;
        }

        function handleAuthInsert(el) {
            var data = JSON.parse($(el).attr("data-data") || "{}");
            tokenClient.callback = async (resp) => {
                if (resp.error !== undefined) {
                    throw (resp);
                }
                const token = gapi.client.getToken(),
                    event = {
                        'summary': data.name,
                        'location': data.venue,
                        'start': {
                            'dateTime': data.start_datetime_iso_8601,
                        },
                        'end': {
                            'dateTime': data.end_datetime_iso_8601,
                        },
                    };
                return axios.post(route('google-accounts.share-event'), { token, event })
                    .then(response => {
                        swalx.swalResponse(response);
                        swal.close();
                    }).catch(error => {
                        // console.log(error);
                    });
            };
            if (data.access_token_required) {
                tokenClient.requestAccessToken({ prompt: 'consent' });
            } else {
                tokenClient.requestAccessToken({ prompt: '' });
            }
        }
        function handleAuthInserts(el) {
            let data = JSON.parse($(el).attr("data-data") || "{}");
            tokenClient.callback = async (resp) => {
                if (resp.error !== undefined) {
                    throw (resp);
                }
                const token = gapi.client.getToken(),
                    events = data.events || {};
                return axios.post(route('google-accounts.share-events'), { token, events })
                    .then(response => {
                        swalx.swalResponse(response);
                        swal.close();
                    }).catch(error => {
                        // console.log(error);
                    });
            };
            if (data.access_token_required) {
                tokenClient.requestAccessToken({ prompt: 'consent' });
            } else {
                tokenClient.requestAccessToken({ prompt: '' });
            }
        }

        /**
         *  Sign out the user upon button click.
         */
        function handleSignoutClick() {
            const token = gapi.client.getToken();
            if (token !== null) {
                google.accounts.oauth2.revoke(token.access_token);
                gapi.client.setToken('');
                document.getElementById('content').innerText = '';
                document.getElementById('authorize_button').innerText = 'Authorize';
                document.getElementById('signout_button').style.visibility = 'hidden';
            }
        }
        return {
            init: function () {
                if (inputx.notEmpty(CLIENT_ID)) {
                    gapiLoaded();
                    gisLoaded();
                }
            },
            initBtnAddCalendar(formEl) {
                $(`${formEl} [data-calendar='insert']`).unbind();
                $(`${formEl} [data-calendar='insert']`).on("click", function (e) {
                    e.preventDefault();
                    handleAuthInsert(this);
                });
            },
            initBtnAddCalendars(formEl) {
                $(`${formEl} [data-calendar='insert']`).unbind();
                $(`${formEl} [data-calendar='insert']`).on("click", function (e) {
                    e.preventDefault();
                    handleAuthInserts(this);
                });
            },
            handleSignoutClick: function () {
                handleSignoutClick();
            },
        };
    }();
    modules.integration.google.calendar.init();
})(jQuery, window);