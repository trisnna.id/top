(function ($, window) {
    "use strict";
    window.modules = window.modules || {};
    window.modules.resource = window.modules.resource || {};
    window.modules.resource.admin = window.modules.resource.admin || {};
    window.modules.resource.admin.index = (function () {
        return {
            swalUpsertFaq: function (formEl, data) {
                if (data.id === undefined) {
                    $(`${formEl} [name='_method']`).val("POST").change();
                } else {
                    $(`${formEl} [name='question']`)
                        .val(data.question)
                        .change();
                    $(`${formEl} [name='answer']`).val(data.answer).change();
                    $(`${formEl} [name='is_active']`).prop(
                        "checked",
                        data.is_active
                    );
                    $(`${formEl} [name='filters[campus][]']`)
                        .val(data.filters.campus)
                        .change();
                    $(`${formEl} [name='filters[intake][]']`)
                        .val(data.filters.intake)
                        .change();
                    $(`${formEl} [name='filters[programme][]']`)
                        .val(data.filters.programme)
                        .change();
                }
                tinymcexfaq.init("answer", "#answer");
                $.fn.select2.amd.require(
                    ["optgroup-data", "optgroup-results"],
                    function (OptgroupData, OptgroupResults) {
                        $(`${formEl} .swal2-select2-multiple-optgroup`).select2(
                            {
                                dropdownParent: $(".swal2-modal"),
                                dropdownCssClass: "select2-multiple-optgroup",
                                dataAdapter: OptgroupData,
                                resultsAdapter: OptgroupResults,
                            }
                        );
                    }
                );
            },
        };
    })();
    window.modules.resource.admin.create = (function () {
        return {
            swalUpsert: function (formEl, data) {
                tinymcex.init("description", "#description");
                $(`${formEl} select[name=attachment]`)
                    .on("select2:select", function (e) {
                        e.preventDefault();
                        const value = e.params.data.id;
                        if (value == "files") {
                            const accept = $(e.params.data.element).data(
                                    "accept"
                                ),
                                acceptInfo = $(e.params.data.element).data(
                                    "accept-info"
                                ),
                                size = $(e.params.data.element).data("size");
                            $(`${formEl} #attachmentContainer`).append(`
                            <div class="d-flex flex-column mb-5 fv-row attachment-input-files">
                                <input multiple type="file" class="form-control" name="attachment_files[]" id="attachment_files" placeholder="Media File" accept="${accept}">
                                <div class="form-text text-black">Allowed file types: ${acceptInfo}. Max size: ${size} MB</div>
                            </div>
                        `);
                        } else if (value == "links") {
                            $(`${formEl} #attachmentContainer`).append(`
                            <div class="d-flex flex-column mb-5 fv-row attachment-input-links">
                                <textarea class="form-control" name="attachment_links" id="attachment_links" placeholder="Media link. For multiple links, separate by comma (,)."></textarea>
                                <div class="form-text text-black">i.e.: Youtube link: https://www.youtube.com/embed/{ID}</div>
                            </div>
                        `);
                        }
                    })
                    .on("select2:unselect", function (e) {
                        e.preventDefault();
                        const value = e.params.data.id;
                        if (value == "files") {
                            $(`${formEl} #attachmentContainer`)
                                .find(".attachment-input-files")
                                .remove();
                        } else if (value == "links") {
                            $(`${formEl} #attachmentContainer`)
                                .find(".attachment-input-links")
                                .remove();
                        }
                    });
            },
            swalUpsertSuccess: function (formEl, data) {
                $(`${formEl} select[name=attachment]`).val("").change();
                const files = $(`${formEl} #attachmentContainer`).find(
                        ".attachment-input-files"
                    ),
                    links = $(`${formEl} #attachmentContainer`).find(
                        ".attachment-input-links"
                    );
                if (files.length) {
                    files.remove();
                }
                if (links.length) {
                    links.remove();
                }
            },
            swalUpsertLink: function (elInput, data) {
                if (typeof data.id === undefined) {
                    $(`${elInput} [name='_method']`).val("POST").change();
                } else {
                    $(`${elInput} [name='name']`).val(data.name).change();
                    $(`${elInput} [name='url']`).val(data.url).change();
                    $(`${elInput} [name='description']`)
                        .val(data.description)
                        .change();
                }
            },
            swalUpsertFile: function (elInput, data) {
                if (typeof data.id === undefined) {
                    $(`${elInput} [name='_method']`).val("POST").change();
                } else {
                    $(`${elInput} [name='name']`).val(data.name).change();
                    $(`${elInput} [name='description']`)
                        .val(data.description)
                        .change();
                }
            },
        };
    })();
})(jQuery, window);
