(function ($, window) {
    "use strict";
    window.modules = window.modules || {};
    window.modules.portal = window.modules.portal || {};
    window.modules.portal.index = function () {
        return {
            swalLogin: function (formEl) {
                function changeLabel(value) {
                    const label = $(`${formEl} label[for='username_email']`),
                        input = $(`${formEl} input[name='username_email']`),
                        forgetPasswordContainer = $(`${formEl} #forgetPasswordContainer`);
                    if (value == 1) {
                        label.html('Email');
                        input.attr('placeholder', 'Email');
                        input.attr('type', 'email');
                        forgetPasswordContainer.hide();
                    } else if (value == 2) {
                        label.html("Student ID / Staff ID");
                        input.attr('placeholder', "Student ID / Staff ID");
                        input.attr('type', 'text');
                        forgetPasswordContainer.hide();
                    } else {
                        label.html('Email');
                        input.attr('placeholder', 'Email');
                        input.attr('type', 'email');
                        forgetPasswordContainer.show();
                    }
                };
                changeLabel($(`${formEl} [name='type']`).val());
                $(`${formEl} [name='type']`).on('change', function (e) {
                    e.preventDefault();
                    changeLabel(this.value);
                })
                swalx.init();
            },
        };
    }();
})(jQuery, window);