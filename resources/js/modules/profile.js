(function ($, window) {
    "use strict";
    window.modules = window.modules || {};
    window.modules.profile = window.modules.profile || {};
    window.modules.profile.index = function () {
        return {
            student: function (formEl) {
                $.each($(`${formEl} input:not(.avatar)`), function (index, value) {
                    inputx.disabledReadonly(value);
                });
            },
            staff: function (formEl) {
                $.each($(`${formEl} input:not(.avatar)`), function (index, value) {
                    inputx.disabledReadonly(value);
                });
            },
            admin: function (formEl) {
                $.each($(`${formEl} input:not(.avatar)`), function (index, value) {
                    inputx.disabledReadonly(value);
                });
            },
        };
    }();
    window.modules.profile.notification = window.modules.profile.notification || {};
    window.modules.profile.notification.index = function () {
        return {
            updateRead: function (el) {
                var that = $(el);
                const url = that.attr('href'),
                    id = that.attr('data-id');
                return axios.post(route('profile.notification-read.update', id), { url })
                    .then(response => {
                        swalx.swalResponse(response);
                    }).catch(error => {
                        // console.log(error);
                    });
            },
        };
    }();
})(jQuery, window);