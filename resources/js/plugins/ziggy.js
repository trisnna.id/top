const Ziggy = {"url":"","port":null,"defaults":{},"routes":{"portal.index":{"uri":"\/","methods":["GET","HEAD"]},"dashboard.index":{"uri":"dashboard","methods":["GET","HEAD"]},"profile.notification-read.update":{"uri":"profile\/notification\/read\/{read}","methods":["POST"]},"google-accounts.share-event":{"uri":"google-accounts\/share-event","methods":["POST"]},"google-accounts.share-events":{"uri":"google-accounts\/share-events","methods":["POST"]},"orientations.index":{"uri":"timetables","methods":["GET","HEAD"]},"admin.orientations.calculate-student":{"uri":"admin\/orientations\/calculate-student","methods":["GET","HEAD"]},"admin.orientations.index":{"uri":"admin\/orientations","methods":["GET","HEAD"]}}};

if (typeof window !== 'undefined' && typeof window.Ziggy !== 'undefined') {
    Object.assign(Ziggy.routes, window.Ziggy.routes);
}

export { Ziggy };
