(function ($, window) {
    "use strict";
    window.fullcalendars = window.fullcalendars || {};
    window.fullcalendarx = function () {
        var containerHeight = function (size) {
            var body = window.innerHeight,
                header = $('.app-header').height() || 0,
                footer = $('.app-footer').height() || 0,
                container = body - header - footer - (size || 360);
            return container;
        };
        return {
            init: function (elId, setting) {
                let calendarEl = document.getElementById(elId),
                    elMonthId = `${elId}Month`,
                    calendarMonthEl = document.getElementById(elMonthId);
                if (!calendarEl) {
                    return;
                }
                let calendar = new FullCalendar.Calendar(calendarEl, {
                    headerToolbar: { left: "prev,next today", center: "title", right: "timeGridWeek,timeGridDay" },
                    initialView: 'timeGridWeek',
                    events: setting.events,
                    slotMinTime: "07:00:00",
                    slotMaxTime: "23:00:00",
                    height: containerHeight(1),
                    eventDidMount: setting.eventDidMount || this.eventDidMount,
                    eventClick: setting.eventClick || this.eventClick,
                });
                calendar.render();
                window.fullcalendars[elId] = calendar;
                if (!calendarMonthEl) {
                    return;
                }
                let calendarMonth = new FullCalendar.Calendar(calendarMonthEl, {
                    headerToolbar: { left: "prev", center: "title", right: "next" },
                    initialView: 'dayGridMonth',
                });
                calendarMonth.render();
                window.fullcalendars[elMonthId] = calendarMonth;
            },
            default: function (elId, setting) {
                let calendarEl = document.getElementById(elId);
                if (!calendarEl) {
                    return;
                }
                let calendar = new FullCalendar.Calendar(calendarEl, {
                    headerToolbar: { left: "prev,next today", center: "title", right: "dayGridMonth,timeGridWeek,timeGridDay" },
                    initialView: 'dayGridMonth',
                    events: setting.events,
                    slotMinTime: "07:00:00",
                    slotMaxTime: "23:00:00",
                    height: containerHeight(1),
                    eventDidMount: setting.eventDidMount || this.eventDidMount,
                    eventClick: setting.eventClick || this.eventClick,
                });
                calendar.render();
                window.fullcalendars[elId] = calendar;
            },
        };
    }();
})(jQuery, window);