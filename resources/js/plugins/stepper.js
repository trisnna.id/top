(function ($, window) {
    "use strict";
    window.stepperx = function () {
        var t, i, o, s, r;
        return {
            init: function (elStepper, elForm) {
                t = document.querySelector(elStepper);
                i = t.querySelector(elForm);
                o = t.querySelector('[data-kt-stepper-action="submit"]');
                s = t.querySelector('[data-kt-stepper-action="next"]');
                (r = new KTStepper(t)).on("kt.stepper.next", (function () {
                    r.goNext();
                    KTUtil.scrollTop();
                })), r.on("kt.stepper.previous", (function () {
                    r.goPrevious();
                    KTUtil.scrollTop();
                }));
            }
        };
    }();
})(jQuery, window);