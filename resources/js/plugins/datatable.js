(function ($, window, DataTable) {
    "use strict";
    $.fn.hasScrollBar = function () {
        var e = this.get(0);
        return {
            vertical: e.scrollHeight > e.clientHeight,
            horizontal: e.scrollWidth > e.clientWidth
        };
    }
    $.fn.dataTable.ext.errMode = function (settings, helpPage, message) {
        let errorStatus = settings.jqXHR.status || null,
            errorMessage = settings.jqXHR.responseJSON?.message || settings.jqXHR.responseJSON?.error || '';
        if (!errorStatus) {
            return;
        }
        Swal.fire('Oops...', errorStatus + ": " + errorMessage, 'error');
    };
    window.dtx = function () {
        var buildUrl = function (dt, action) {
            let url = dt.ajax.url() || '',
                params = dt.ajax.params();
            params.action = action;
            if (url.indexOf('?') > -1) {
                return url + '&' + $.param(params);
            }
            return url + '?' + $.param(params);
        };
        var updateCheckboxesDt = function (el, dtId) {
            let hiddenCheckboxId = $(`#${dtId} #${$(el).attr("data-checkboxId")}`),
                checkboxIds = [];
            if (hiddenCheckboxId.val()) {
                let checkboxIdValues = hiddenCheckboxId.val().split(",");
                checkboxIds = $.grep(checkboxIdValues, function (item, index) {
                    return index == $.inArray(item, checkboxIdValues);
                });
            }
            let id = $(el).val(),
                arrPos = checkboxIds.indexOf(id);
            if (arrPos > -1 && !$(el).prop("checked")) {
                checkboxIds.splice(arrPos, 1);
            } else if ($(el).prop("checked")) {
                if ($.inArray(id, checkboxIds) == -1) {
                    checkboxIds.push(id);
                }
            }
            hiddenCheckboxId.val(checkboxIds)
            hiddenCheckboxId.attr('data-count', checkboxIds.length || 0);
            return true;
        };
        var isOnScreen = function (element) {
            var elementOffsetTop = element.offset().top;
            var elementHeight = element.height();
            var screenScrollTop = $(window).scrollTop();
            var screenHeight = $(window).height();
            var scrollIsAboveElement = elementOffsetTop + elementHeight - screenScrollTop >= 0;
            var elementIsVisibleOnScreen = screenScrollTop + screenHeight - elementOffsetTop >= 0;
            return scrollIsAboveElement && elementIsVisibleOnScreen;
        };
        var extendScrollbarX = function (dtId) {
            let tableEl = document.querySelector(`table#${dtId}`),
                tableWidth = $(tableEl).width(),
                tableScrollbarXEl = document.querySelector(`#${dtId}ScrollbarX`),
                tableScrollbarXEndEl = document.querySelector(`#${dtId}ScrollbarXEnd`),
                parentEl = $(tableScrollbarXEl).attr('data-parentEl'),
                parentWidth = $(parentEl).width(),
                scrollbarXEl = document.querySelector(`#${dtId}ScrollbarX .scrollbar-x`);
            if (!tableScrollbarXEl) {
                return;
            }
            let bodyHasScrollYBar = $('html').hasScrollBar().vertical || false,
                parentHasScrollXBar = $(parentEl).hasScrollBar().horizontal || false;
            if (parentHasScrollXBar && !isOnScreen($(tableScrollbarXEndEl))) {
                if (!isOnScreen($(tableEl))) {
                    $(tableScrollbarXEl).fadeTo(0, 0);
                } else {
                    $(tableScrollbarXEl).fadeTo(0, 1);
                }
            } else {
                $(tableScrollbarXEl).fadeTo(0, 0);
            }
            if (bodyHasScrollYBar && !isOnScreen($(tableScrollbarXEndEl))) {
                if (!isOnScreen($(tableEl))) {
                    $(tableScrollbarXEl).fadeTo(0, 0);
                } else {
                    $(tableScrollbarXEl).fadeTo(0, 1);
                }
            } else {
                $(tableScrollbarXEl).fadeTo(0, 0);
            }
            $(tableScrollbarXEl).width(parentWidth);
            $(scrollbarXEl).width(tableWidth);
            $(tableScrollbarXEl).on('scroll', function (e) {
                $(parentEl).scrollLeft($(tableScrollbarXEl).scrollLeft());
            });
            $(parentEl).on('scroll', function (e) {
                $(tableScrollbarXEl).scrollLeft($(parentEl).scrollLeft());
            });
            $(window).on('scroll', function (e) {
                if (isOnScreen($(tableScrollbarXEndEl)) || !isOnScreen($(tableEl))) {
                    $(tableScrollbarXEl).fadeTo(0, 0); //hide
                } else {
                    $(tableScrollbarXEl).fadeTo(0, 1); //show
                }
            });
            $(window).on("resize", function () {
                $(tableScrollbarXEl).width($(parentEl).width());
                $(scrollbarXEl).width($(`table#${dtId}`).width());
            });
        }
        return {
            checkboxReset: function (elInput, dtId) {
                let that = this;
                $(`.checkbox-reset`).unbind().click(function (e) {
                    e.preventDefault();
                    let dtId = $(this).attr('data-table'),
                        hiddenCheckboxId = $(`#${dtId}Cbx`),
                        checkboxInputAll = $(`#${dtId} .checkbox-input-all`),
                        checkboxInputSingle = $(`#${dtId} .checkbox-input-single`);
                    hiddenCheckboxId.val('').change();
                    hiddenCheckboxId.attr('data-count', 0);
                    checkboxInputAll.prop('checked', false);
                    checkboxInputSingle.prop('checked', false);
                    that.reloadSingleDt(dtId);
                });
            },
            swalInputCheckbox: function (elInput, dtId) {
                let checkboxAll = $(`input#${dtId}Cbx`),
                    selectedCount = $(`${elInput} #selectedCount`),
                    count = checkboxAll.attr('data-count') || 0,
                    value = checkboxAll.val();
                selectedCount.html(count);
                $(`${elInput} [name='checkbox']`).val(value).change();
            },
            exportDt: function (dtId, type) {
                let dt = window.LaravelDataTables[dtId],
                    url = buildUrl(dt, type);
                window.location = url;
            },
            containerHeight: function (size) {
                let body = window.innerHeight,
                    header = $('.app-header').height() || 0,
                    toolbar = $('.app-toolbar').height() || 0,
                    footer = $('.app-footer').height() || 0,
                    container = body - header - toolbar - footer - (size || 360);
                return container;
            },
            adjustColumnSingleDt: function (dtId) {
                setTimeout(function () {
                    window.LaravelDataTables[dtId].columns.adjust();
                }, 300);
            },
            upsertSwalDt: function (dtId) {
                $(`#${dtId} .upsert-swal-dt`).click(function (e) {
                    e.preventDefault();
                    swalx.upsertPopup(this);
                });
            },
            drawCallback: function (settings, dtId) {
                this.checkboxInputSingle(dtId);
                this.checkboxInputAll(dtId);
                this.upsertSwalDt(dtId);
                this.checkboxReset();
                extendScrollbarX(dtId);
                swalx.init();
            },
            initComplete: function (settings, json, dtId) {

            },
            initOption: function (dtId, options) {
                let that = this;
                $(`select#${dtId}_length`).change(function (e) {
                    e.preventDefault();
                    window.LaravelDataTables[dtId].page.len(this.value).draw();
                });
                $(`input#${dtId}_search`).on('input', function (e) {
                    e.preventDefault();
                    that.reloadSingleDt(dtId);
                });
            },
            checkboxInputAll: function (dtId) {
                $(`#${dtId} .checkbox-input-all`).unbind().change(function (e) {
                    e.preventDefault();
                    let that = $(this);
                    $('.checkbox-input-single[data-checkboxid="' + $(that).attr("data-checkboxId") + '"]').each(function (index, value) {
                        if ($(that).prop("checked") && !$(value).prop('checked')) {
                            $(value).prop('checked', true);
                            updateCheckboxesDt(value, dtId);
                        } else if (!$(that).prop("checked") && $(value).prop('checked')) {
                            $(value).prop('checked', false);
                            updateCheckboxesDt(value, dtId);
                        }
                    });
                });
            },
            checkboxInputSingle: function (dtId) {
                $(`#${dtId} .checkbox-input-single`).unbind().change(function (e) {
                    e.preventDefault();
                    updateCheckboxesDt(this, dtId);
                });
            },
            reloadSingleDt: function (dtId) {
                window.LaravelDataTables[dtId].ajax.reload();
            },
            infiniteScrollSingleDt: function (dtId) {
                $(window).off("scroll");
                $(`#${dtId}_processing`).hide();
                $(window).on("scroll", function () {
                    if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                        $(window).off("scroll");
                        $(`#${dtId}_processing`).show();
                        window.LaravelDataTables[dtId].ajax.infiniteScroll();
                    }
                });
            },
        }
    }();
})(jQuery, window, jQuery.fn.dataTable);
$('.dataTables_filter .form-control').removeClass('form-control-sm');
$('.dataTables_length .form-select').removeClass('form-select-sm').removeClass('form-control-sm');