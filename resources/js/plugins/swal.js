(function ($, window) {
    "use strict";
    window.upsertSettings = window.upsertSettings || {};
    window.swalSettings = window.swalSettings || {};
    window.swalx = function () {
        var actionResponse = function (response, data = null, swalComponent = null) {
            if (response.data.links) {
                if (response.data.links.redirect && response.data.links.redirect !== '') {
                    window.location = response.data.links.redirect;
                } else if (response.data.links.reload && response.data.links.reload !== '') {
                    location.reload();
                }
            }
            $.each(window.LaravelDataTables, function (key, value) {
                window.LaravelDataTables[key].ajax.reload(null, false);
            });
            $.each(window.fullcalendars, function (key, value) {
                window.fullcalendars[key].refetchEvents();
            });
            if (response.data.meta && response.data.meta.function) {
                if (response.data.meta.function) {
                    if (response.data.meta.function.name && response.data.meta.function.name !== '') {
                        setTimeout(function () {
                            var call = eval(response.data.meta.function.name);
                        }, 1000);

                    }
                }
            }
            if (inputx.notEmpty(data)) {
                data.response = response;
                swalComponent.axios.afterSuccessResponse(data);
            }
        };
        var swalResponse = function (response, data = null, swalComponent = null) {
            if (response.status === 200) {
                let alert = response.data.meta.alert || {};
                if (alert) {
                    var title = alert.title || '',
                        text = alert.text || '',
                        type = alert.type || '';
                    toastr.options.closeButton = true;
                    if (alert.component !== undefined && alert.component.name === 'toastr') {
                        if (title.length && text.length) {
                            if (type === 'success') {
                                toastr.success(text, title);
                            } else if (type === 'info') {
                                toastr.info(text, title);
                            } else if (type === 'warning') {
                                toastr.warning(text, title);
                            } else if (type === 'danger' || type === 'error') {
                                toastr.error(text, title);
                            }
                        } else if (!title.length && text.length) {
                            if (type === 'success') {
                                toastr.success(text);
                            } else if (type === 'info') {
                                toastr.info(text);
                            } else if (type === 'warning') {
                                toastr.warning(text);
                            } else if (type === 'danger' || type === 'error') {
                                toastr.error(text);
                            }
                        } else if (title.length && !text.length) {
                            if (type === 'success') {
                                toastr.success(title);
                            } else if (type === 'info') {
                                toastr.info(title);
                            } else if (type === 'warning') {
                                toastr.warning(title);
                            } else if (type === 'danger' || type === 'error') {
                                toastr.error(title);
                            }
                        }
                        actionResponse(response, data, swalComponent);
                    } else {
                        let timer = 2000;
                        if (alert.component !== undefined && alert.component.timer !== undefined && alert.component.timer !== '') {
                            timer = alert.component.timer;
                        }
                        Swal.fire({
                            title: title,
                            text: text,
                            icon: response.data.type,
                            timer: timer
                        }).then(() => {
                            actionResponse(response, data, swalComponent);
                        });
                    }
                } else {
                    actionResponse(response, data, swalComponent);
                }
            } else {
                Swal.showValidationMessage(response.status + ": " + response.data.message);
            }
        };
        var upsertPopup = function (el, deny) {
            var data = JSON.parse($(el).attr("data-data") || "{}"),
                swalEl = JSON.parse($(el).attr("data-swal") || "{}"),
                swalComponentName = swalEl.id + 'Settings',
                swalComponent = swalSettings[swalComponentName] || {},
                swalUrl, swalMethod = 'post', swalContentType = "application/json";
            if (swalEl.settings) {
                swalComponent = $.extend({}, swalComponent, swalEl.settings);
            }
            if (inputx.notEmpty(swalEl.axios)) {
                swalUrl = swalEl.axios.url;
                if (inputx.notEmpty(swalEl.axios.method)) {
                    swalMethod = swalEl.axios.method;
                }
                if (inputx.notEmpty(swalEl.axios.contentType)) {
                    swalContentType = swalEl.axios.contentType;
                }
            } else {
                swalUrl = swalComponent.axios.url(data);
            }
            var extendSwalComponent = {
                didOpen: () => {
                    $(".swal2-modal .select2-hidden-accessible").attr('style', 'position: inherit !important;');
                    $(".swal2-modal .select2-container").attr('style', 'width: 100%;font-size: 1rem;text-align: initial;');
                    $("form#" + swalEl.id).attr('action', swalUrl);
                    $("form#" + swalEl.id).attr('method', swalMethod);
                    if (swalComponent.didOpen !== undefined) {
                        swalComponent.didOpen(data);
                    }
                    $("form#" + swalEl.id + " .swal2-select2").select2({
                        theme: "bootstrap5",
                        allowClear: true,
                        dropdownParent: $('.swal2-modal')
                    });
                    $("form#" + swalEl.id + " .swal2-flatpickr-date").flatpickr();
                },
                didDestroy: () => {
                    if (swalComponent.didDestroy !== undefined) {
                        swalComponent.didDestroy();
                    }
                },
                preConfirm: () => {
                    if (swalComponent.preConfirm !== undefined) {
                        swalComponent.preConfirm();
                    }
                    var form = $(`form#${swalEl.id}`);
                    if (form.length) {
                        var validate = form[0].checkValidity();
                        form[0].reportValidity();
                    }
                    var formData = new FormData(form[0]) || {};
                    if (validate || form.length === 0) {
                        if (swalComponent.axios.afterValidate !== undefined) {
                            swalComponent.axios.afterValidate(data);
                        }
                        return axios({
                            url: swalUrl,
                            method: swalMethod,
                            headers: {
                                "content-type": swalContentType,
                                "accept": 'application/json',
                            },
                            data: formData,
                        }).then(response => {
                            swalResponse(response, data, swalComponent);
                        }).catch(error => {
                            if (error.response.status === 422) {
                                let responseError = '';
                                var responseErrors = $.map(error.response.data.errors, function (value, index) {
                                    return [value[0]];
                                });
                                responseErrors.forEach(function (item, index) {
                                    responseError += item + "<br>";
                                });
                                Swal.showValidationMessage(responseError);
                            } else {
                                Swal.showValidationMessage(error.response.status + ": " + error.response.data.message);
                            }
                        });
                    } else {
                        return false;
                    }
                },
                preDeny: () => {
                    if (deny.deny) {
                        upsertPopup(deny, deny.deny);
                    } else {
                        upsertPopup(deny);
                    }
                },
            };
            Swal.fire($.extend({}, swalComponent, extendSwalComponent));
        };
        var upsertInline = function (el) {
            var swalEl = JSON.parse($(el).attr("data-swal") || "{}"),
                form = null,
                validate = false,
                swalUrl;
            if (swalEl.id) {
                form = $("form#" + swalEl.id);
            } else {
                form = $(swalEl.el);
            }
            validate = form[0].checkValidity();
            let formData = new FormData(form[0]);

            if (typeof swalEl.axios !== "undefined") {
                swalUrl = swalEl.axios.url;
            } else {
                swalUrl = form.attr('action');
            }
            if (validate) {
                Swal.fire({
                    title: 'Loading... Please Wait!',
                    html: '',
                    backdrop: true,
                    allowOutsideClick: () => !Swal.isLoading(),
                    heightAuto: false,
                    willOpen: () => {
                        Swal.showLoading();
                    }
                });
                return axios.post(swalUrl, formData, {
                    headers: {
                        "content-type": "multipart/form-data",
                        "accept": 'application/json',
                    },
                }).then(response => {
                    swal.close();
                    if (response.status === 200) {
                        let alert = response.data.meta.alert || {};
                        if (alert) {
                            var title = alert.title || '',
                                text = alert.text || '',
                                type = alert.type || '';
                            toastr.options.closeButton = true;
                            if (alert.component !== undefined && alert.component.name === 'toastr') {
                                if (title.length && text.length) {
                                    if (type === 'success') {
                                        toastr.success(text, title);
                                    } else if (type === 'info') {
                                        toastr.info(text, title);
                                    } else if (type === 'warning') {
                                        toastr.warning(text, title);
                                    } else if (type === 'danger' || type === 'error') {
                                        toastr.error(text, title);
                                    }
                                } else if (!title.length && text.length) {
                                    if (type === 'success') {
                                        toastr.success(text);
                                    } else if (type === 'info') {
                                        toastr.info(text);
                                    } else if (type === 'warning') {
                                        toastr.warning(text);
                                    } else if (type === 'danger' || type === 'error') {
                                        toastr.error(text);
                                    }
                                }
                                actionResponse(response);
                            } else {
                                let timer = 2000;
                                if (alert.component !== undefined && alert.component.timer !== undefined && alert.component.timer !== '') {
                                    timer = alert.component.timer;
                                }
                                Swal.fire({
                                    title: title,
                                    text: text,
                                    icon: response.data.type,
                                    timer: timer
                                }).then(() => {
                                    actionResponse(response);
                                });
                            }
                        } else {
                            actionResponse(response);
                        }
                    } else {
                        Swal.fire('Oops...', response.status + ": " + response.data.message, 'error');
                    }
                }).catch(error => {
                    if (typeof error.response !== "undefined") {
                        if (error.response.status === 422) {
                            let responseError = '';
                            var responseErrors = $.map(error.response.data.errors, function (value, index) {
                                return [value[0]];
                            });
                            responseErrors.forEach(function (item, index) {
                                responseError += item + "<br>";
                            });
                            Swal.fire(error.response.data.message, responseError, 'error');
                        } else {
                            Swal.fire('Oops...', error.response.status + ": " + error.response.data.message, 'error');
                        }
                    }
                });
            } else {
                form[0].reportValidity();
            }
        };
        return {
            init: function (elId = null) {
                var upsertSwalPopup, upsertSwalInline;
                if (inputx.notEmpty(elId)) {
                    upsertSwalPopup = $(`${elId}.upsert-swal-popup`);
                    upsertSwalInline = $(`${elId}.upsert-swal-inline`);
                } else {
                    upsertSwalPopup = $(`.upsert-swal-popup`);
                    upsertSwalInline = $(`.upsert-swal-inline`);
                }
                upsertSwalPopup.unbind();
                upsertSwalPopup.on("click", function (e) {
                    e.preventDefault();
                    upsertPopup(this);
                });
                upsertSwalInline.unbind();
                upsertSwalInline.on("click", function (e) {
                    e.preventDefault();
                    var precallfn = $(this).data('precallfn');
                    if (precallfn) {
                        window[precallfn]();
                    }
                    upsertInline(this);
                });
            },
            upsertPopup: function (el, deny) {
                upsertPopup(el, deny);
            },
            upsertInline: function (el) {
                upsertInline(el);
            },
            swalResponse: function (response, data = null, swalComponent = null) {
                swalResponse(response, data = null, swalComponent = null)
            }
        };
    }();
    swalx.init();
})(jQuery, window);