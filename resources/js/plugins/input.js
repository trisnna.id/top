(function ($, window) {
    "use strict";
    window.inputx = function () {
        return {
            disabledReadonly: function (el) {
                $(el).prop('disabled', true);
                $(el).prop('readonly', true);
            },
            notEmpty: function (value) {
                if (typeof value !== "undefined" && value !== '' && value !== null) {
                    return true;
                } else {
                    return false;
                }
            },
            reset: function (el) {
                $(`${el} .reset`).val('').change();
            },
        };
    }();
})(jQuery, window);