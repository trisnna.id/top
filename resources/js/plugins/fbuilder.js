(function (window, document, $) {
    'use strict';
    window.fbuilderx = function () {
        return {
            init: function (el) {
                return $(el).formBuilder({
                    showActionButtons: false,
                    disabledAttrs: ["name", "className"],
                    disableFields: ['autocomplete','button']
                });
            }
        };
    }();
})(window, document, jQuery);