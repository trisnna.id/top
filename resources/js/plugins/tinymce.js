(function (window, $) {
    "use strict";
    window.tinymcex = (function () {
        var viewSwalEditor = function (elId) {
            var options = {
                selector: `textarea#${elId}`,
                readonly: 1,
                toolbar: false,
                menubar: false,
                statusbar: false,
                resize: false,
                height: "70vh",
            };
            tinymce.init(options);
        };
        var init = function (elId, elInput) {
            var input = $(elInput);
            var options = {
                selector: `textarea#${elId}`,
                height: "300",
                plugins: "image,link",
                menubar: "file edit view insert format",
                menu: {
                    format: {
                        title: "Format",
                        items: "bold italic underline strikethrough superscript subscript code | formats blockformats fontsizes align lineheight | removeformat",
                    },
                },
                file_picker_types: "image",
                file_picker_callback: function (callback, value, meta) {
                    if (meta.filetype == "image") {
                        var input = document.createElement("input");
                        input.setAttribute("type", "file");
                        input.setAttribute("accept", "image/*");
                        input.onchange = function () {
                            var file = this.files[0];
                            var reader = new FileReader();
                            reader.onload = function () {
                                var id = "blobid" + new Date().getTime();
                                var blobCache =
                                    tinymce.activeEditor.editorUpload.blobCache;
                                var base64 = reader.result.split(",")[1];
                                var blobInfo = blobCache.create(
                                    id,
                                    file,
                                    base64
                                );
                                blobCache.add(blobInfo);
                                callback(blobInfo.blobUri(), {
                                    title: file.name,
                                });
                            };
                            reader.readAsDataURL(file);
                        };

                        input.click();
                    }
                },
                setup: function (editor) {
                    if (input.length) {
                        editor.on("change", function (e) {
                            e.preventDefault();
                            input
                                .val(JSON.stringify(editor.getContent()))
                                .change();
                        });
                        editor.on("keyup", function (e) {
                            e.preventDefault();
                            input
                                .val(JSON.stringify(editor.getContent()))
                                .change();
                        });
                    }
                },
            };
            tinymce.remove(`textarea#${elId}`);
            tinymce.init(options);
        };
        return {
            init: function (elId, elInput) {
                return init(elId, elInput);
            },
            viewSwalEditor: function (elId) {
                return viewSwalEditor(elId);
            },
        };
    })();
    window.tinymcexfaq = (function () {
        var viewSwalEditor = function (elId) {
            var options = {
                selector: `textarea#${elId}`,
                readonly: 1,
                toolbar: false,
                menubar: false,
                statusbar: false,
                resize: false,
                height: "70vh",
            };
            tinymce.init(options);
        };
        var init = function (elId, elInput) {
            var input = $(elInput);
            var options = {
                selector: `textarea#${elId}`,
                height: "300",
                plugins: "image,link",
                menubar: "file edit view insert format", // hanya menampilkan menu yang diinginkan
                // menu: {
                //     format: {
                //         title: "Format",
                //         items: "bold italic underline strikethrough superscript subscript code | formats blockformats fontsizes align lineheight | removeformat",
                //     },
                // },
                file_picker_types: "image",
                file_picker_callback: function (callback, value, meta) {
                    if (meta.filetype == "image") {
                        var input = document.createElement("input");
                        input.setAttribute("type", "file");
                        input.setAttribute("accept", "image/*");
                        input.onchange = function () {
                            var file = this.files[0];
                            var reader = new FileReader();
                            reader.onload = function () {
                                var id = "blobid" + new Date().getTime();
                                var blobCache =
                                    tinymce.activeEditor.editorUpload.blobCache;
                                var base64 = reader.result.split(",")[1];
                                var blobInfo = blobCache.create(
                                    id,
                                    file,
                                    base64
                                );
                                blobCache.add(blobInfo);
                                callback(blobInfo.blobUri(), {
                                    title: file.name,
                                });
                            };
                            reader.readAsDataURL(file);
                        };

                        input.click();
                    }
                },
                setup: function (editor) {
                    if (input.length) {
                        editor.on("change", function (e) {
                            e.preventDefault();
                            input
                                .val(JSON.stringify(editor.getContent()))
                                .change();
                        });
                        editor.on("keyup", function (e) {
                            e.preventDefault();
                            input
                                .val(JSON.stringify(editor.getContent()))
                                .change();
                        });
                    }
                },
            };
            tinymce.remove(`textarea#${elId}`);
            tinymce.init(options);
        };
        return {
            init: function (elId, elInput) {
                return init(elId, elInput);
            },
            viewSwalEditor: function (elId) {
                return viewSwalEditor(elId);
            },
        };
    })();
})(window, jQuery);
